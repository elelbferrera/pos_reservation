<?php
  class appointment extends template{
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();

    }
	  
	public function reminder()
	{		
	   $actions = array("set_reminder");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
            
        }

        $params = array(
            'session_id' => $_SESSION['sessionid'],
        );

        $response = lib::getWsResponse(API_URL, 'get_appointment_reminder', $params);
    
        if($response['respcode'] == 911 ){
            $reminder = array(
                'is_notification_on' =>'',
                'subject' => '',
                'html_template' => '',
                'frequency_value' => '',
                'frequency_option' => ''
                );
        }else{
            $response = $response['respmsg'];
            $reminder = $response;
        }
        

        $this->view->assign('reminder', $reminder);
	}

    private function set_reminder()
    {
        // print_r($_POST['txtTemplate']); die();
         $params = array(
            'session_id' => $_SESSION['sessionid'],
            'merchant_id' => $_SESSION['merchant_id'],
            'is_notification_on' => trim($_POST['txtNotification']),
            'subject' => trim($_POST['txtSubject']),
            'html_template' => $_POST['txtTemplate'],
            'frequency_value' => trim($_POST['txtValue']),
            'frequency_option' => trim(strtolower($_POST['txtOption']))
        );

        $merchant_id = $_SESSION['merchant_id'];
        $response = lib::getWsResponse(API_URL, 'set_appointment_reminder', $params);
        // print_r($response); die();

        if (!(isset($response['respcode'], $response['respmsg']))) {
            // $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
            // $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }else{
            $response = $response['respmsg'];
            
            $this->response = array(
                'success' => true,
                'message' => $response,
                'redirect' => WEBROOT."/appointment/reminder",
            );
        }
    }

  }


