<?php
class defaultcontroller extends template {
    public function index() {
        if(!isset($_SESSION['sessionid'])){
            unset($_SESSION);
            session_destroy();
            return $this->redirect('log/in/');
        }else{
            return $this->redirect('reservation/management/');
        }
    }
    public function accessdenied($params = null) {
        // $this->meta['menu'] = array('home');
        // if (isset($params[0]) && $params[0] == 'db_error') {
        //     $this->showError('Error: Unable to load database.');
        // }

        if(!isset($_SESSION['sessionid'])){
            unset($_SESSION);
            session_destroy();
            return $this->redirect('log/in/');
        } else if ($_SESSION['user_type'] != 1) {
            return $this->redirect('reservation/management/');
        } else {
            unset($_SESSION);
            session_destroy();
            return $this->redirect('log/in/');
        }
    }
}