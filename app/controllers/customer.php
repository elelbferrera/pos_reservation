<?php
    class customer extends template {
      	protected $response;
  		protected $merchant_id;
		
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			$this->merchant_id = $_SESSION['user_info']['reference_id'];
        }
		
		public function management()
        {

            $actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
            $this->backend->get_customers($this->merchant_id); //
            $customers = $this->backend->get_response();
            $customers = $customers['ResponseMessage'];
            
            $this->view->assign('customers', $customers);   
		}
		
		
		//merchant_id, last_name, first_name, middle_name, mobile, telephone, state, city, zip, country, email, address, udf1, udf2, udf3	
		protected function register()
		{
			$params = array(
				'merchant_id' => $this->merchant_id,
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'mobile' => $_POST['txtMobile'],
				'telephone' => $_POST['txtTelephone'],
				'state' => $_POST['txtState'],
				'city' => $_POST['txtCity'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'address' => $_POST['txtAddress'],
				'udf1' => $_POST['txtUDF1'],
				'udf2' => $_POST['txtUDF2'],
				'udf3' => $_POST['txtUDF3'],
				'password' => $_POST['txtPassword']
			);
			
		  $result = $this->backend->register_customer($params);
          $response = $this->backend->get_response();
                                                             
          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
          } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
          } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "New Customer has been registered.",
                );
          }
		}
		
		
		//id,merchant_id, last_name, first_name, middle_name, mobile, telephone, state, city, zip, country, email, address, udf1, udf2, udf3 
		protected function update()
		{
			$params = array(
				'id' => $_POST['txtCustomerId'],
				'merchant_id' => $_POST['txtMerchantId'],
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'mobile' => $_POST['txtMobile'],
				'telephone' => $_POST['txtTelephone'],
				'state' => $_POST['txtState'],
				'city' => $_POST['txtCity'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'address' => $_POST['txtAddress'],
				'udf1' => $_POST['txtUDF1'],
				'udf2' => $_POST['txtUDF2'],
				'udf3' => $_POST['txtUDF3'],
			);
			
			  $result = $this->backend->update_customer($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Customer has been updated.",
	                );
	          }
		}
		
		protected function delete()
		{
			$id = $_GET['id'];
            $result = $this->backend->delete_customer($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
		}
		
	}
?>