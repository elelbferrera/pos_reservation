<?php
class user extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function management()
    {
        
    }
    
    public function registration() {
        $actions = array('get_agents');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
                            
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        if($row[0]['description']=="Super Agent")
        {
            $user_types = array(
                3 => 'Agent',
                5 => 'Customer',
            );
        }
        else if($row[0]['description']=="Agent")
        {
            $user_types = array(
                5 => 'Customer',
            );                  
        }
        else if($row[0]['description']=="Admin")
        {
            $user_types = array(
                2 => 'Staff',
                3 => 'Agent',
                4 => 'Super Agent',
                5 => 'Customer',
            );                  
        }
        else{
            $user_types = array(
                1 => 'Admin',
                2 => 'Staff',
                3 => 'Agent',
                4 => 'Super Agent',
                5 => 'Customer',
                6 => 'Super User'
            );
        }
        $this->view->assign('user_types', $user_types);
        //JR-2014/03/26
        //$agents=$this->backend->get_agents_for_users(0, 'pair');
        //$this->view->assign('agents', $agents);
        $this->view->assign('agents', -1); //default value just in case on change was not triggered
        
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $_POST['username'], $_POST['user_type']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email_address']) != '' && trim($_POST['username']) != '' && trim($_POST['user_type']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
                $reg_params = array(
                    'fname' => $_POST['first_name'],
                    'lname' => $_POST['last_name'],
                    'email_address' => $_POST['email_address'],
                    'mobile_number' => $_POST['mobile_number'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    'user_type' => $_POST['user_type'],
                    'created_by' => $_SESSION['username'],
                    //JR-2014/03/26 
                    'agent_id' => $_POST['agent'], 
                    'customers_id' => -1, //default value
                    'isSMS' => 0, //default value
                    'isEmail' => 1 //default value
                    
                 );
                $result = $this->backend->register_user($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'],
                    );
                    //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Created new user '. $_POST['first_name']. ' '. $_POST['last_name']. ' with user type id#'. $_POST['user_type'],'User Registration',$datetime,$_SESSION['username'],'');

                }
            }
        }
    }
    
    //JR-2014/03/26
    protected function get_agents(){
         $isAgent = $_GET['isAgent'];
         //echo $isAgent;
        // die();
         $agent = $this->backend->get_agents_for_users(0,'');
         
         if ($isAgent=='true')
         { 
            if (count($agent) > 0){
                 $option = "";
                 foreach($agent as $row){
                    $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
                    //print_r($row['id']);
                    //print_r($row['agent_name']);
                 }
                 //print_r($option);
                $this->response = array(
                    'success'   => true,
                    'data'      => $option,
                );
             }
             else{
                 $this->response = array(
                    'success'   => false,
                    'data'      => "No Result"
                );
             }
         }else{
                  $this->response = array(
                    'success'   => false,
                    'data'      => "No Result"
                    );
             
         }
         
    }
    
    protected function reset_password()
    {
        if (!(isset($_GET['id']) && trim($_GET['id']) != '')) {
            $this->response['message'] = 'Invalid user ID';
        } else {
            $user_id = $_GET['id'];
            $email = $_GET['email'];
            $username = $_GET['username'];
            
            $params = array('user_id' => $user_id
            ,'username' =>  $username
            ,'email_address' => $email
            );
            
            $result = $this->backend->reset_user_password($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
                );
                //JR-2014/04/02 logs
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Reset Password was performed for user '. $username ,'User Management',$datetime,$_SESSION['username'],'');

            }
        }

    }
    
    
    
    public function updateuser()
    {
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';
            /* //JR-2014/03/26 use this just in case they switch to auto assigning of agent_id via mobile_number
            $agent_id = $this->backend->get_agent_id_by_mobile($_POST['mobile_number']);
            if ($_POST['user_type'] == 3 || $_POST['user_type'] == 4){ //3 - Agent 4 - Super Agent
                $agent_id = $this->backend->get_agent_id_by_mobile($_POST['mobile_number']);                
            }else{
                $agent_id = -1;
            }
            */
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $_POST['user_type']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email_address']) != '' && trim($_POST['user_type']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
              
              if(!isset($_POST['agent']))
              {$agentid = -1;}
              else
              {$agentid = $_POST['agent'];}
                $reg_params = array(
                    'fname' => $_POST['first_name'],
                    'lname' => $_POST['last_name'],
                    'email_address' => $_POST['email_address'],
                    'mobile_number' => $_POST['mobile_number'],
                    'address' => $_POST['address'],
                    'user_type' => $_POST['user_type'],
                    'user_id' => $_POST['user_id'],
                    //JR-2014/03/26
                  //  'agent_id' => $_POST['agent'],
                  'agent_id' =>$agentid ,
                  'customers_id' => -1, //default value
                  'status' => $_POST['userStatus']
                 );
                $result = $this->backend->update_user($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                   //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('User '.$_POST['first_name'].' '.$_POST['last_name']. ' was successfully updated' ,'User Management',$datetime,$_SESSION['username'],'');

                }
            }
         }
    }
    
    
    public function management() {
        $actions = array('delete_user', 'reset_password','load_agents');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        } else {
            
            $user_types = array(
                1 => 'Admin',
                2 => 'Staff',
                3 => 'Agent',
                4 => 'Super Agent',
                5 => 'Customer',
                6 => 'Super User'
            );
            $this->view->assign('user_types', $user_types);
            //------------ Loading of agent starts here
            //JR-2014/03/26
            $agents=$this->backend->get_agents_for_users(0,'pair');
          
            $this->view->assign('agents', $agents);
           
//            if (count($agents) > 0){
//                 $option = "";
//                 foreach($agents as $row){
//                    $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
//                 }
//            }
//            $this->view->assign('optiontext', $option);
            //------------ Loading of agent ends here 
            
            $this->backend->get_users_detailed();
            
            $response = $this->backend->get_response();
            $users = $response['ResponseMessage'];
            $this->view->assign('users', $users);
            

        }
    }
	  //JR-2014/03/24
    protected function load_agents(){
         //$id = $_GET['id'];
         //echo count('test');
         $agents=$this->backend->get_agents_for_users(0,'');
        //print_r(count($agents));
         if (count($agents) > 0){
             $option = "";
             foreach($agents as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
                
             }
             //print_r($option);
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             $this->response = array(
                'success'   => false,
                'data'      => "Error"
            );
         }
         
    }
    
    protected function delete_user() {
        if (!(isset($_GET['id']) && trim($_GET['id']) != '')) {
            $this->response['message'] = 'Invalid user ID';
        } else {
            $user_id = $_GET['id'];
            $user_id = substr($user_id, 2);
            if (!(is_numeric($user_id) && $user_id > 0)) {
                $this->response['message'] = 'Invalid user ID';
            } else {
                $params = array('user_id' => $user_id);
                $result = $this->backend->delete_user($params);
                $response = $this->backend->get_response();
                if (!$result) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                    //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('UserId #'. $user_id . ' was successfully deleted.' ,'User Management',$datetime,$_SESSION['username'],'');

                }
            }
        }
    }
}
