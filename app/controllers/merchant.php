<?php
  class merchant extends template {
    protected $response;
	protected $merchant_id;
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
	
	public function my_profile()
	{
		$actions = array("update_my_profile");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        }
		
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        );
		$response = lib::getWsResponse(API_URL, 'get_countries', $params);
		$countries = $response['respmsg'];
		
		$this->view->assign('countries', $countries);
		
		// $params = array(
        	// 'merchant_id' => $_SESSION['merchant_id'],
        // );
		$response = lib::getWsResponse(API_URL, 'get_merchant_profile', $params);
		
		$response = $response['respmsg'];
		
		// print_r($response);
		// die();
		
		$b_hours="";
		
		foreach($response['business_hours'] as $s)
		{
			$this->view->assign(strtolower("{$s['day']}_is_close"), $s['is_close']);
			$this->view->assign(strtolower("{$s['day']}_start_time"), $s['start_time']);
			$this->view->assign(strtolower("{$s['day']}_end_time"), $s['end_time']);
		}
		
		$profile = $response;
		
		
		$this->view->assign('profile', $profile);
	}
	
	private function update_my_profile()
	{
		$sTempFileName="";
		if($_FILES)
        {
			if (! $_FILES['file']['error'] && $_FILES['file']['size'] < (1024*2) * 1024) {
	                if (is_uploaded_file($_FILES['file']['tmp_name'])) {
	
	                    $aSize = getimagesize($_FILES['file']['tmp_name']); // try to obtain image info
	                    if (!$aSize) {
	                        @unlink($sTempFileName);
	                        return;
	                    }
	    
	                    // check for image type
	                    switch($aSize[2]) {
	                        case IMAGETYPE_JPEG:
	                            $sExt = '.jpg';
	                            break;
	                        case IMAGETYPE_PNG:
	                            $sExt = '.png';
	                            break;
	                        default:
	                            @unlink($sTempFileName);
	                            return;
	                    }
	                    
	                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
	                    $sTempFileName = 'images/uploads/' .time().rand()."{$sExt}";
	
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['file']['tmp_name'], $sTempFileName);
	                    
	                    $sTempFileName = WEBROOT."/public/{$sTempFileName}";
	                  }
			}else{
				if(($_FILES['file']['tmp_name']!=""))
				{
					$this->response['success'] = false;
	            	$this->response['message'] = $_FILES['file']['tmp_name'];
					return;
				}
				
			}
                  
		}

		$monday_is_close = isset($_POST['chkMonday']) ? 1 : 0;
		$monday_start_time = "";
		$monday_end_time ="";

		if($monday_is_close==0)
		{
			$time = $_POST['txtMondayOpeningTime'];
			$date = DateTime::createFromFormat('g:i a', $time);
			$monday_start_time = $date->format('H:i');
			 
			$time = $_POST['txtMondayClosingTime'];
		 	$date = DateTime::createFromFormat('g:i a', $time);
		 	$monday_end_time = $date->format('H:i');
		}
		 
		 
		$tuesday_is_close = isset($_POST['chkTuesday']) ? 1 : 0;
		$tuesday_start_time = "";
		$tuesday_end_time ="";

		if($tuesday_is_close==0)
		{		 
			 $time = $_POST['txtTuesdayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $tuesday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtTuesdayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $tuesday_end_time = $date->format('H:i');
		}

	  	$wednesday_is_close = isset($_POST['chkWednesday']) ? 1 : 0;
		$wednesday_start_time = "";
		$wednesday_end_time ="";

		if($wednesday_is_close==0)
		{
			 $time = $_POST['txtWednesdayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $wednesday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtWednesdayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $wednesday_end_time = $date->format('H:i');
		}
		
	  	$thursday_is_close = isset($_POST['chkThursday']) ? 1 : 0;
		$thursday_start_time = "";
		$thursday_end_time ="";

		if($thursday_is_close==0)
		{		
			 $time = $_POST['txtThursdayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $thursday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtThursdayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $thursday_end_time = $date->format('H:i');
		}
		
	  	$friday_is_close = isset($_POST['chkFriday']) ? 1 : 0;
		$friday_start_time = "";
		$friday_end_time ="";

		if($friday_is_close==0)
		{		
			 $time = $_POST['txtFridayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $friday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtFridayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $friday_end_time = $date->format('H:i');
		}
		
		$saturday_is_close = isset($_POST['chkSaturday']) ? 1 : 0;
		$saturday_start_time = "";
		$saturday_end_time ="";

		if($saturday_is_close==0)
		{	
			 $time = $_POST['txtSaturdayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $saturday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtSaturdayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $saturday_end_time = $date->format('H:i');
		}
		
		$sunday_is_close = isset($_POST['chkSunday']) ? 1 : 0;
		$sunday_start_time = "";
		$sunday_end_time ="";

		if($sunday_is_close==0)
		{
			 $time = $_POST['txtSundayOpeningTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $sunday_start_time = $date->format('H:i');
			 
			 $time = $_POST['txtSundayClosingTime'];
			 $date = DateTime::createFromFormat('g:i a', $time);
			 $sunday_end_time = $date->format('H:i');			 
		}
		 
		 // $_POST['chkMonday'];
		 // print_r($_POST);
		 // die();
		 
		 
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'logo' => $sTempFileName,
        	'description' => $_POST['txtDescription'],
        	'business_name' => $_POST['txtMerchantName'],
        	'address1' => $_POST['txtAddress'],
			'city' => $_POST['txtCity'],
			'state' => $_POST['txtState'],
			'dba' => $_POST['txtDBA'],
			'mid' => $_POST['txtMID'],
			'processor' => $_POST['txtProcessor'],
			'zip' => $_POST['txtZip'],
			'country' => $_POST['txtCountry'],
			'email' => $_POST['txtEmail'],
			'phone1' => $_POST['txtContactNo'],
			'first_name' => $_POST['txtFirstName'],
			'last_name' => $_POST['txtLastName'],
			'contact_email' => $_POST['txtEmail'], 
			'longitude' => $_POST['txtLongitude'],
			'latitude' => $_POST['txtLatitude'],
			'caption' => $_POST['txtCaption'],
			
			'monday_is_close' => $monday_is_close,
			'monday_start_time' => $monday_start_time,
			'monday_end_time' => $monday_end_time,
			
			'tuesday_is_close' => $tuesday_is_close,
			'tuesday_start_time' => $tuesday_start_time,
			'tuesday_end_time' => $tuesday_end_time,
			
			'wednesday_is_close' => $wednesday_is_close,
			'wednesday_start_time' => $wednesday_start_time,
			'wednesday_end_time' => $wednesday_end_time,
			
			'thursday_is_close' => $thursday_is_close,
			'thursday_start_time' => $thursday_start_time,
			'thursday_end_time' => $thursday_end_time,
			
			'friday_is_close' => $friday_is_close,
			'friday_start_time' => $friday_start_time,
			'friday_end_time' => $friday_end_time,
			
			'saturday_is_close' => $saturday_is_close,
			'saturday_start_time' => $saturday_start_time,
			'saturday_end_time' => $saturday_end_time,
			
			'sunday_is_close' => $sunday_is_close,
			'sunday_start_time' => $sunday_start_time,
			'sunday_end_time' => $sunday_end_time,

         );

		
        //LC-09/11/2013
        // print_r($monday_end_time);
		// die();
//         
        
        $response = lib::getWsResponse(API_URL, 'update_merchant_profile', $params);
		
		// print_r($response);
		// die();
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }

	    $params = array(
        	'session_id' => $_SESSION['sessionid'],
			'merchant_id' => $_SESSION['merchant_id']
		);

	    $response = lib::getWsResponse(API_URL, 'get_merchant_business_hours', $params);
	   
		$today = date("Y-m-d");
		$u_day = strtoupper(date('l', strtotime($today)));

		foreach($response['respmsg']['business_hours'] as $b)
		{
			if($u_day == $b['day'])
			{
				$_SESSION['business_start_time'] = $b['start_time'];
				$_SESSION['business_end_time'] = $b['end_time'];
			}
		}
		
	}
	
	
	private function update_status()
	{
		
		
		$insert_data= array(
			'order_id' => $_POST['txtOrderIdStatus'],
			'status' => $_POST['txtStatus'],
			'create_by' => $_SESSION['username'],
			 'user_id' => $_SESSION['user_info']['user_id'],
		);
		
		
		//order_id, status, update_by
		$result = $this->backend->update_product_order($insert_data);
        $response = $this->backend->get_response();
		// print_r($response);
		// die();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			$this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
			
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Product has been updated.",
                );
        }
	}
	
	private function add_sub_comment()
	{
		
		$comment_id = $_POST['txtParentId'];
		
		$sTempFileName ="";
		if($_FILES)
		{
			
			//2mb
			if(isset($_FILES['file'.$comment_id]['tmp_name']))
			{
				if (! $_FILES['file'.$comment_id]['error'] && $_FILES['file'.$comment_id]['size'] < (1024*2) * 1024) {
			          if (is_uploaded_file($_FILES['file'.$comment_id]['tmp_name'])) {
							
							$sExt = pathinfo($_FILES['file'.$comment_id]['name'], PATHINFO_EXTENSION);
		                    
		                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
		                    $sTempFileName = 'uploads/comment/' .time().rand().".{$sExt}";
		
		                    // move uploaded file into cache folder
		                    move_uploaded_file($_FILES['file'.$comment_id]['tmp_name'], $sTempFileName);
							
							$sTempFileName = WEBROOT."/public/{$sTempFileName}";
							
							// print_r($sTempFileName);
							// die();
	                 		// return $sTempFileName;   
		                }
	            	}
	            }
	     }
		
		$is_internal = 0;
		$is_public = 0;
		
		$insert_data= array(
			'order_id' => $_POST['txtOrderId'],
			'product_id' => $_POST['txtProductId'],
			'partner_id' => $_POST['txtPartnerId'],
			'comment' => $_POST['txtComment'],
			'parent_id' => $_POST['txtParentId'],
			'create_by' => $_SESSION['username'],
			'user_id' => $_SESSION['user_info']['user_id'],
			'attachment' => $sTempFileName
			
		);
		
		$result = $this->backend->create_sub_comment($insert_data);
        $response = $this->backend->get_response();
		// print_r($response);
		// die();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			$this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
			
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Comment has been posted.",
                );
        }
		// print_r($_FILES);
		// die();
	}
	
	private function add_comment()
	{
		
		// print_r($_POST);
		// die();
		
		$order_id = $_POST['txtOrderId'];


		$sTempFileName ="";
		if($_FILES)
		{
			
			//2mb
			if(isset($_FILES['file'.$order_id]['tmp_name']))
			{
				if (! $_FILES['file'.$order_id]['error'] && $_FILES['file'.$order_id]['size'] < (1024*2) * 1024) {
			          if (is_uploaded_file($_FILES['file'.$order_id]['tmp_name'])) {
							
							$sExt = pathinfo($_FILES['file'.$order_id]['name'], PATHINFO_EXTENSION);
							
			                // check for image type
			                // switch($aSize[2]) {
			                    // case IMAGETYPE_JPEG:
			                        // $sExt = '.jpg';
			                        // break;
			                    // case IMAGETYPE_PNG:
			                        // $sExt = '.png';
			                        // break;
			                    // default:
			                        // @unlink($sTempFileName);
			                        // return;
			                // }
		                    
		                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
		                    $sTempFileName = 'uploads/comment/' .time().rand().".{$sExt}";
		
		                    // move uploaded file into cache folder
		                    move_uploaded_file($_FILES['file'.$order_id]['tmp_name'], $sTempFileName);
							
							$sTempFileName = WEBROOT."/public/{$sTempFileName}";
							
							// print_r($sTempFileName);
							// die();
	                 		// return $sTempFileName;   
		                }
	            	}
	            }
	       }
		
		//create_comment
		//order_id, product_id, partner_id, comment, parent_id, create_by, user_id, is_public, attachment
		
		$is_internal = 0;
		$is_public = 0;
		
		if($_POST['comment-type'] =="Public")
		{
			$is_public = 1;
		}elseif($_POST['comment-type'] =="Agent")
		{
			
		}
		else{
			$is_internal = 1;
		}
		
		
		$insert_data= array(
			'order_id' => $_POST['txtOrderId'],
			'product_id' => $_POST['txtProductId'],
			'partner_id' => $_POST['txtPartnerId'],
			'comment' => $_POST['txtComment'],
			'parent_id' => $_POST['txtParentId'],
			'create_by' => $_SESSION['username'],
			'user_id' => $_SESSION['user_info']['user_id'],
			'attachment' => $sTempFileName,
			'is_internal' => $is_internal,
			
		);
		
		$result = $this->backend->create_comment($insert_data);
        $response = $this->backend->get_response();

		
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			$this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
			
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Comment has been posted.",
                );
        }
		// print_r($params);
		// die();
	}
	
	public function application()
	{
		
		$actions = array("add_comment","add_sub_comment", "update_status");
        
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
				
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			// die();
        }
		
		
		
		$parent_id = $this->parent_id;
		
		$is_agent = $_SESSION['user_info']['is_agent'];
		
		if($is_agent > 0)
		{
			$is_admin = 0;
			$this->view->assign('is_admin', FALSE);
		}else{
			$is_admin = 1;
			$this->view->assign('is_admin', TRUE);
		}
		
		
		
		
		$applications = $this->backend->get_applications($parent_id, $is_admin);
		$applications = $this->backend->get_response();
		$applications = $applications['ResponseMessage'];
		$this->view->assign('applications', $applications);
		
		
		$statuses = $this->backend->get_statuses();
		$statuses = $this->backend->get_response();
		$statuses = $statuses['ResponseMessage'];
		
		
		
		$this->view->assign('statuses', $statuses);
		// print_r($applications);
		// die();
		
		// print_r($_SESSION['user_info']);
		// die();
		
		
		// $comments = $this->backend->get_comments($parent_id);
		// $comments = $this->backend->get_response();
		// $comments = $comments['ResponseMessage'];
		
		// print_r($applications);
		// die();
		
		$this->view->assign('comments', $applications);
	}
	
	
	private function send_pdf_merchant()
	{
		//print_r($_GET);
		$partner_id = $_GET['id'];
		$order_id =$_GET['order_id'];
		
		
		
 		
		if($this->generate_pdf_doc(array("{$partner_id}&order_id={$order_id}"), true)=="unsent")
		{
			$this->response = array(
                'success' => false,
                'message' => "Unable to send application.",
            );
		}else{
		
			$product_order = $this->backend->get_product_order_status($order_id);
			
			
			if($product_order['status'] =="PDF Sent")
			{
			 	$this->response = array(
	                'success' => true,
	                'message' => "Information has been emailed.",
	            );
			}else{
			
				$update = array(
					'create_by' => $_SESSION['username'],
					'status' => 'PDF Sent',
					'order_id' => $order_id
				);
				//create_by, status, order_id
				
				$this->backend->update_order_status($update);
				
				
				
				$response = $this->backend->get_response(); 
				
				
				if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
					$this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		        } elseif (!($response['ResponseCode'] == '0000')) {
		            $this->response['success'] = false;
		            $this->response['message'] = $response['ResponseMessage'];
					
		        } else {
		                $this->response = array(
		                    'data'=> array(),
		                    'success' => true,
		                    'message' => "Information has been emailed.",
		                );
		        }
			}
		}
	}
	
	
	public function generate_pdf_doc($id, $send = false)
	{

		$info = explode("&", $id[0]);



		$id = $info[0];
		
		$order_id = str_replace("order_id=", "", $info[1]);
		
		$product = $this->backend->get_product_order_info($order_id);
		
		$result = $this->backend->get_partner_info($id);
		$merchant_info = $this->backend->get_response();
		
		$merchant_info = $merchant_info['ResponseMessage'];
		
				//get parent_partner
		$parent_id = $merchant_info['parent_id'];
		$result = $this->backend->get_partner_info($parent_id);
		$agent = $this->backend->get_response();
		$agent = $agent['ResponseMessage'];
		
		
		if($send)
		{
			
			$email_addresses = "{$merchant_info['email']},{$merchant_info['contact_email']}";
			
			
			$email_content ="Dear {$merchant_info['dba']} – ({$merchant_info['first_name']} {$merchant_info['last_name']}), 
			<br>
			Thank you for applying for our Gift & Rewards application service. To have your application processed.
			<br>
			<br>".
			//Please sign the attached pre-filled application form
			"Please click <a href='".WEBROOT."/sign_application?partner_id={$id}&order_id={$order_id}'> here </a> to sign."	
			."<br>
			<br>
			Reply back to this email with the following attachments:
			<br>
			<br>
			Signed application form (Required)<br>
			Copy of void check (Required)<br>
			Business Logo<br>
			<br>
			This email was sent on behalf of {$agent['first_name']} with contact information below.<br> 
			<br>
			<p>
			Agent Information<br>
			Name: {$agent['first_name']} {$agent['last_name']}<br>
			Email: {$agent['contact_email']}<br>
			Mobile: {$agent['contact_email']}<br>
			</p><br>
			<br>
			Should you have any questions or concerns, please feel free to contact us. 
			<br>
			<br>
			
			
			Tel: 888-377-3818   Fax: 888-406-0777 <br>
			Support Email: support@go3solutions.com <br>
			Chat: http://chat.go3support.com/ <br>
			Website: www.go3solutions.com <br>
			";
			
			// <img src='".WEBROOT."/public/images/logo_go3.png' title='lem'> </img>
			// <br>
			// <br>
			
			//if($this->backend->send_mail_notification($email_content, $email_addresses,"Merchant Application Form", $full_path))
			if($this->backend->send_mail_notification($email_content, $email_addresses,"Merchant Application Form", "")) 
			{
				//add logs
				//change status to PDF Sent
				return "sent";
			}
			return "unsent";

		}
		
		require_once('tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING ="";
        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        // set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks        
        $pdf->SetAutoPageBreak(false, 0);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf->SetFont('Courier', 'B', 12);
        $cnt = 1;
        $page_cnt = 1;
        $row_limit = 200;
        $isAddPage = false;
        // add a page
        $pdf->AddPage();
		
		if($product['product_id'] ==1)
		{  
			$image_path = 'images/template.png';
	        $destination_path = 'images/pdf_applications/';
	        
	        //$datetime=$this->backend->get_server_date();                
	        $y = $pdf->GetY($image_path);
	        $rby = $pdf->getImageRBY($image_path);
			
			
	        $pdf->Image($image_path,7,8,200,$rby-$y);
			
	        // set information font
	        $pdf->SetFont('times', '', 12);    
			
			
			// https://localhost/registration/merchant/generate_pdf_doc/32
			
			$result = $this->backend->get_partner_info($id);
			
			
			$merchant_info = $this->backend->get_response();
			
			$merchant_info = $merchant_info['ResponseMessage'];
			
			
	
			
			$pdf->Text(52.8,38.5,$merchant_info['company_name']); //company name
			$pdf->Text(136,38.5,$merchant_info['merchant_id']); //MID
			
			
			$pdf->Text(30,45.8,$merchant_info['dba']); 
			$pdf->Text(148,45.8,$merchant_info['merchant_processor']); //MID
			
			$pdf->Text(40,52.8,$merchant_info['address1']);
			
			$pdf->Text(100,52.8,$merchant_info['city']);
			
			$pdf->Text(138,52.8,$merchant_info['state']);
			
			$pdf->Text(168,52.8,$merchant_info['zip']);
			
			$pdf->Text(50,60.5,$merchant_info['email']);
			$pdf->Text(151,60.5,$merchant_info['phone1']);
			
			
			$pdf->Text(46,67.5,$merchant_info['fax']);
			$pdf->Text(151,67.5,$merchant_info['phone2']); 
			
			
			$pdf->Text(42,88.8,$merchant_info['first_name']);
			$pdf->Text(140,88.8,$merchant_info['mobile_number']);
			
			
			$pdf->Text(42,95.8,$merchant_info['last_name']);
			$pdf->Text(144,95.8,$merchant_info['office_number']);
			
			$pdf->Text(38,102,$merchant_info['position']);
			
			$pdf->Text(135,102,$merchant_info['contact_fax']);
			
			$pdf->Text(46.5,109,$merchant_info['contact_email']);
		
			//get parent_partner
			$parent_id = $merchant_info['parent_id'];
			$result = $this->backend->get_partner_info($parent_id);
			$agent = $this->backend->get_response();
			$agent = $agent['ResponseMessage'];
			
			
			$pdf->Text(46.5,228.5,$agent['first_name']." ".$agent['last_name']);
			$pdf->Text(127.5,228.5,$agent['merchant_id']);
			$pdf->Text(36.5,235.5,$agent['contact_email']);
			
			$pdf->Text(133,235.5,$agent['mobile_number']);
			
			
			$merchant_fees = $this->backend->get_product_fee($order_id);
			
			$total_fee = 0;
			foreach($merchant_fees as $fee)
			{
				$total_fee = $total_fee + $fee['product_information'];
				if($fee['product_name']== 'MONTHLY FEE')
				{
					$pdf->Text(80,127.5,$fee['product_information']);		
				}
				else{
					$pdf->Text(80,133.5,$fee['product_information']);	
				}
			}
			
			$pdf->Text(80,154,number_format($total_fee,2));	
			
			$pdf->Text(143,127.5,$merchant_info['bank_name']);
			$pdf->Text(143,134.5,$merchant_info['account_name']);
			$pdf->Text(143,141.5,$merchant_info['account_number']);
			$pdf->Text(143,147.5,$merchant_info['routing_number']);
			
			
			$product_sign = $this->backend->get_product_order_info($order_id);
			$data = $product_sign['signature'];
			$destination_path = 'images/pdf_applications/';
			if($data !="")
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
		
				file_put_contents($destination_path.'/'.$order_id.'.png', $data);
				
				$pdf->Image($destination_path.'/'.$order_id.'.png',40,202,60,10);
			}
			$pdf_file =  $id.'.pdf';
	        $full_path = $destination_path . $pdf_file;   
			$pdf->Output($full_path, 'F');
		}
		// $pdf->Image($image_path."hello.jpg");
		//$pdf->Ou
		
		
		// $im = new Imagick();
		// $im->setResolution(300, 300);     //set the resolution of the resulting jpg
		// $im->readImage($full_path);    //[0] for the first page
		// $im->setImageFormat('jpg');
		// header('Content-Type: image/jpeg');
		// echo $im;
		// die();
		if($product['product_id'] ==2) //for online order
		{
			$image_path = 'images/GO2POS.png';
	        $destination_path = 'images/pdf_applications/';
	        
	        //$datetime=$this->backend->get_server_date();                
	        $y = $pdf->GetY($image_path);
	        $rby = $pdf->getImageRBY($image_path);
			
			
	        $pdf->Image($image_path,7,8,200,$rby-$y);
			
	        // set information font
	        $pdf->SetFont('times', '', 9.5);    
			
			
			// https://localhost/registration/merchant/generate_pdf_doc/32
			
			$result = $this->backend->get_partner_info($id);
			
			
			$merchant_info = $this->backend->get_response();
			
			$merchant_info = $merchant_info['ResponseMessage'];
			
			
	
			
			$pdf->Text(45.8,33.0,$merchant_info['company_name']); //company name
			$pdf->Text(100,33.0,$merchant_info['dba']);
			$pdf->Text(160,33.0,$merchant_info['merchant_id']); //MID
			
			$pdf->Text(35,37.8,$merchant_info['address1']);
			$pdf->Text(101,37.8,$merchant_info['city']);
			$pdf->Text(140,37.8,$merchant_info['state']);
			$pdf->Text(174,37.8,$merchant_info['zip']);
			 
			//$pdf->Text(148,45.8,$merchant_info['merchant_processor']); //MID
			$pdf->Text(40,42.5,$merchant_info['phone1']);
			$pdf->Text(88,42.5,$merchant_info['fax']);
			
			$pdf->Text(136,42.5,$merchant_info['email']);
		
			$pdf->Text(40,54.5,$merchant_info['first_name']);
			$pdf->Text(96,54.5,$merchant_info['last_name']);
			$pdf->Text(154,54.5,$merchant_info['position']);
			
			
			
			$pdf->Text(46,59.8,$merchant_info['office_number']);
			$pdf->Text(118.5,59.8,$merchant_info['contact_email']);
			
			
			
			
			
		
			//get parent_partner
			$parent_id = $merchant_info['parent_id'];
			$result = $this->backend->get_partner_info($parent_id);
			$agent = $this->backend->get_response();
			$agent = $agent['ResponseMessage'];
			
			
			// $pdf->Text(46.5,228.5,$agent['first_name']." ".$agent['last_name']);
			// $pdf->Text(127.5,228.5,$agent['merchant_id']);
			// $pdf->Text(36.5,235.5,$agent['contact_email']);
// 			
			// $pdf->Text(133,235.5,$agent['mobile_number']);
			
			
			$merchant_fees = $this->backend->get_product_fee($order_id);
			
			
			
			$other_info = array();
			
			$other_fee= 0;
			$total_fee = 0;
			foreach($merchant_fees as $fee)
			{
				//$total_fee = $total_fee + $fee['product_information'];
				$prod_info = $fee['product_information'];
				
				if($fee['product_name']== 'ONLINE ORDERING')
				{
					$other_info = unserialize($prod_info);
					// print_r($prod_info);
					// die();
					//$pdf->Text(80,127.5,$fee['product_information']);
							
				}
				else{ //other
					$other_fee =$fee['product_information'];
					//$pdf->Text(80,133.5,$fee['product_information']);	
				}
			}
			$total_fee = $other_fee;

			
			$_path = 'images/check_pdf.png';
			if($other_info['order_type'] =="WEBSITE")
			{
				$pdf->Image($_path,21.5,75.8,3,3);
			}else{
				$pdf->Image($_path,56,75.8,3,3);
			}
			if(isset($other_info['domain_name']))
			{
				$pdf->Text(56,78,$other_info['domain_name']);		
			}else{
				$pdf->Text(56,78,"DOMAIN NAME");
			}
			
			$pdf->Text(56,82,$other_info['template_no']);
			if(isset($other_info['domain_name_plugin']))
			{
				$pdf->Text(56,93.8,$other_info['domain_name_plugin']);		
			}else{
				$pdf->Text(56,93.8,"DOMAIN NAME PLUGIN");
			}
			
			// print_r($other_info);
			// die();
			
			if($other_info['is_existing_domain'] == "YES")
			{
				$pdf->Image($_path,79,98.5,3,3);
			}else{
				$pdf->Image($_path,94.5,98.5,3,3);
			}
			
			
			if(isset($other_info['receive_order_by'][0]))
			{
				if($other_info['receive_order_by'][0] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][0] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][0] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			if(isset($other_info['receive_order_by'][1]))
			{
				if($other_info['receive_order_by'][1] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][1] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][1] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			if(isset($other_info['receive_order_by'][2]))
			{
				if($other_info['receive_order_by'][2] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][2] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][2] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			
			if(isset($other_info['accept_by'][0]))
			{
				if($other_info['accept_by'][0] =="Delivery")
				{
					//for PHONE
					$pdf->Image($_path,45,119,3,3);	
					
					$pdf->Text(86,118,$other_info['delivery_estimate_time']);
				}
				if($other_info['accept_by'][0] =="Pickup")
				{
					//for PHONE
					$pdf->Image($_path,70,119,3,3);
					
					$pdf->Text(62,110.5,$other_info['pickup_estimate_time']);
				}
			}
			if(isset($other_info['accept_by'][1]))
			{
				if($other_info['accept_by'][1] =="Delivery")
				{
					//for PHONE
					$pdf->Image($_path,45,119,3,3);	
					
					$pdf->Text(86,118,$other_info['delivery_estimate_time']);
				}
				if($other_info['accept_by'][1] =="Pickup")
				{
					//for PHONE
					$pdf->Image($_path,121.5,119,3,3);
					
					$pdf->Text(165,118,$other_info['pickup_estimate_time']);
				}
			}
			
			
			 
			$pdf->Text(40,124.5,$other_info['delivery_fee']);
			$pdf->Text(90,124.5,$other_info['minimum_delivery']);
			$pdf->Text(160,124.5,$other_info['radius']);
			
			if(isset($other_info['payment_method'][0]))
			{
				if($other_info['payment_method'][0] =="CASH")
				{
					//for PHONE
					$pdf->Image($_path,65.5,129.5,3,3);
					
				}
				if($other_info['payment_method'][0] =="CREDIT CARD")
				{
					$pdf->Image($_path,103.5,129.5,3,3);
				}
			}
			if(isset($other_info['payment_method'][1]))
			{
				if($other_info['payment_method'][1] =="CASH")
				{
					//for PHONE
					$pdf->Image($_path,65.5,129.5,3,3);
					
				}
				if($other_info['payment_method'][1] =="CREDIT CARD")
				{
					$pdf->Image($_path,103.5,129.5,3,3);
				}
			}
			
			
			$pdf->Text(165,128.5,$other_info['sales_tax']);
			$pdf->Text(45,132.5,$other_info['payment_gateway']);
			
			$pdf->Text(45,153,$other_info['coupon_code']);
			$pdf->Text(87,153,$other_info['title']);
			
			if(isset($other_info['discount_type'][0]))
			{
				if($other_info['discount_type'][0] =="Fixed Amount")
				{
					//for PHONE
					$pdf->Image($_path,68.2,157.2,3,3);
					$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
					
					
				}
				if($other_info['discount_type'][0] =="Percentage")
				{
					$pdf->Image($_path,127,157.2,3,3);
					$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			if(isset($other_info['discount_type'][1]))
			{
				if($other_info['discount_type'][1] =="Fixed Amount")
				{
					//for PHONE
					$pdf->Image($_path,68.2,157.2,3,3);
					$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
					
					
				}
				if($other_info['discount_type'][1] =="Percentage")
				{
					$pdf->Image($_path,127,157.2,3,3);
					$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			
			if(isset($other_info['how_many_times'][0]))
			{
				if($other_info['how_many_times'][0] =="One Time")
				{
					//for PHONE
					$pdf->Image($_path,109,161,3,3);
					//$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
				}
				if($other_info['how_many_times'][0] =="Multiple")
				{
					$pdf->Image($_path,127,157.5,3,3);
					//$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			if(isset($other_info['how_many_times'][1]))
			{
				if($other_info['how_many_times'][1] =="One Time")
				{
					//for PHONE
					$pdf->Image($_path,109,161,3,3);
					
					
				}
				if($other_info['how_many_times'][1] =="Multiple")
				{
					$pdf->Image($_path,137,161,3,3);
					$pdf->Text(159,160,$other_info['multiple_count']);
					
				}
			}
			
			$pdf->Text(76.5,171.2,$other_info['fee_design']);
			$pdf->Text(76.5,175.2,$other_info['fee_hosting']);
			$pdf->Text(76.5,179.2,$other_info['fee_setup']);
			$pdf->Text(76.5,182.2,$other_info['fee_service']);
			$pdf->Text(76.5,186.2,$other_fee);
			
			$total_fee = $total_fee + $other_info['fee_design'];
			$total_fee = $total_fee + $other_info['fee_hosting'];
			$total_fee = $total_fee + $other_info['fee_setup'];
			$total_fee = $total_fee + $other_info['fee_service'];
			
			
			// Array ( [order_type] => WEBSITE [template_no] => Template No. 
			// [plugin] => ONLINE ORDERING [STAND ALONE] [is_existing_domain] => YES 
			// [receive_order_by] => Array ( [0] => PHONE ) 
			// [accept_by] => Array ( [0] => Delivery [1] => Pickup ) 
			// [delivery_estimate_time] => 1 [pickup_estimate_time] => 1 
			// [delivery_fee] => 2 [minimum_delivery] => 200 [radius] => 2 
			// [payment_method] => Array ( [0] => CASH ) [sales_tax] => 788 
			// [payment_gateway] => GGE4 [coupon_code] => COUPCODE 
			// [title] => TITLECODE 
			// [discount_type] => Array ( [0] => Fixed Amount [1] => Percentage ) 
			// [fixed_amount] => 2 
			// [percentage] => 5 
			//[how_many_times] => Array ( [0] => One Time [1] => Multiple ) 
			// [multiple_count] => 10 
			// [fee_design] => 10 [fee_hosting] => 10 
			// [fee_setup] => 10 [fee_service] => 10 )
			
			
			
			// $pdf->Text(151,67.5,$merchant_info['phone2']);
			// $pdf->Text(140,88.8,$merchant_info['mobile_number']);
			// $pdf->Text(135,102,$merchant_info['contact_fax']);
			
			//http://localhost/registration/sign_application/generate_pdf_doc?partner_id=43&order_id=23
			
			$pdf->Text(76.5,193.5,number_format($total_fee,2));	
			$pdf->Text(105.5,178,$merchant_info['account_number']);
			$pdf->Text(105.5,185,$merchant_info['routing_number']);
			
			//$pdf->Text(143,127.5,$merchant_info['bank_name']);
			//$pdf->Text(143,134.5,$merchant_info['account_name']);
			//$pdf->Text(143,141.5,$merchant_info['account_number']);
			//$pdf->Text(143,147.5,$merchant_info['routing_number']);
			$pdf->Text(46.5,219.5,$merchant_info['first_name']." ".$merchant_info['last_name']);
			
			$pdf->Text(43.5,243,$agent['first_name']." ".$agent['last_name']);
			$pdf->Text(127.5,243,$agent['merchant_id']);

			$pdf->Text(44,246.3,$agent['contact_email']);

			$pdf->Text(128.5,246.5,$agent['mobile_number']);	
			
			
			$product_sign = $this->backend->get_product_order_info($order_id);
			$data = $product_sign['signature'];
			
			if($data !="")
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
		
				file_put_contents($destination_path.'/'.$order_id.'.png', $data);
				
				$pdf->Image($destination_path.'/'.$order_id.'.png',40,202,60,10);
			}
			$pdf_file =  $id.'.pdf';
	        $full_path = $destination_path . $pdf_file;   
			$pdf->Output($full_path, 'F');
		}

	
		
		
		
		
		// print_r('lems');
		// die();
		
		
		header('Location: '.WEBROOT."/public/".$full_path);
		
	}
	
	public function contact($id)
	{
		$id = $id[0];
		$result = $this->backend->get_partner_info($id);
		$merchant_info = $this->backend->get_response();
		$this->view->assign('merchant', $merchant_info['ResponseMessage']);
		
		$this->view->assign('partner_id', $id);
		
	}
	
	public function profile($id)
	{
		$id = $id[0];
		$result = $this->backend->get_partner_info($id);
		$merchant_info = $this->backend->get_response();
	
		$this->view->assign('merchant', $merchant_info['ResponseMessage']);
		
		$result = $this->backend->api_get_countries();
		$country = $this->backend->get_response();
		$country = $country['ResponseMessage'];
		$this->view->assign('country', $country);

		$this->view->assign('partner_id', $id);
	
		$result = $this->backend->get_partner_attachment($id);
		$this->view->assign('documents', $result);
	}
	
	
	public function summary($id)
	{
		
		$actions = array("register_merchant", "update_merchant", "delete_merchant", "send_pdf_merchant");
        
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
				
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			die();
        }
		
		$id = $id[0];
		
		$result = $this->backend->get_partner_info($id);
		$merchant_info = $this->backend->get_response();
		
		$this->view->assign('merchant', $merchant_info['ResponseMessage']);
		
		$this->backend->get_application_information($id);
		$history = $this->backend->get_response();
		
		$this->view->assign('partner_id', $id);
		
		$this->view->assign('histories', $history['ResponseMessage']);
		
	}
	
	public function add()
	{
		
		$actions = array("register_merchant", "update_merchant", "delete_merchant");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
		
		
		$result = $this->backend->get_partner_types();
		$partner_type = $this->backend->get_response();
		$partner_type = $partner_type['ResponseMessage'];
		
		$this->view->assign('partner_type', $partner_type);
		
		$result = $this->backend->api_get_countries();
		$country = $this->backend->get_response();
		$country = $country['ResponseMessage'];
		$this->view->assign('country', $country);
		
		$result = $this->backend->get_states("");
		$states = $this->backend->get_response();
		$states = $states['ResponseMessage'];
		$this->view->assign('states', $states);
		
		$result = $this->backend->get_available_products("");
		$this->view->assign('products', $result);
		
	}
	
	
    
    public function management()
    {
    	
		// print_r($this->meta['menu']);
		// die();
		
        $actions = array("register_merchant", "update_merchant", "delete_merchant");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $this->backend->get_all_merchants($this->parent_id, $this->is_agent);
        $merchants = $this->backend->get_response();
        $merchants = $merchants['ResponseMessage'];
		$this->view->assign('merchants', $merchants);
		
		
		
		
		
		
		// print_r($merchants);
		// die();
//         
        // $countries = $this->backend->get_countries();
        // $this->view->assign('countries', $countries);   
        // $this->view->assign('merchants', $merchants);   
    }
    
    protected function delete_merchant()
    {
        $id = $_GET['id'];
        $result = $this->backend->delete_merchant($id);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => $response['ResponseMessage'],
                );
        }  
    }
    
    
    
    protected function update_merchant()
    {
        $params = $_POST;
        $insert_data = array(
            'id' => $params['txtMerchantId'],
            'merchant_code' => $params['txtMerchantCode'],
            'merchant_name' => $params['txtMerchantName'],
            // 'merchant_logo' => '',
            //'contact_person' => $params['txtContactPerson'],
            'business_type_id' => -1, //txtBusinessType
            //'address' => '',
            'address' => $params['txtAddress'],
            'city' => isset($params['txtCity']) ? $params['txtCity'] : "",
            'state' => isset($params['txtState']) ? $params['txtState'] : "",
            'zip' => isset($params['txtZipCode']) ? $params['txtZipCode'] : "",
            'country' => $params['txtCountry'],
            'email' => $params['txtEmailAddress'],
            'telephone' => $params['txtPhone'],
            'mobile' => isset($params['txtMobile']) ? $params['txtMobile'] : "",
            'fax' => isset($params['txtFax']) ? $params['txtFax'] : "",
            'last_name' => $params['txtLastName'],
            'first_name' => $params['txtFirstName'],
        );
            
		
        $result = $this->backend->update_merchant($insert_data);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Merchant has been updated.",
                );
        }    
    }
    
    protected function register_merchant()
    {
    	
		// print_r($_FILES);
		// print_r($_POST);
		// die();
		

		
		$sTempFileName="";
		$logo ="";
		if($_FILES)
		{
			//2mb
			
			if(isset($_FILES['fileUploadVoidCheck']['tmp_name']))
			{
				if (! $_FILES['fileUploadVoidCheck']['error'] && $_FILES['fileUploadVoidCheck']['size'] < (1024*2) * 1024) {
	                	if (is_uploaded_file($_FILES['fileUploadVoidCheck']['tmp_name'])) {
	
			                $aSize = getimagesize($_FILES['fileUploadVoidCheck']['tmp_name']); // try to obtain image info
			                if (!$aSize) {
			                    @unlink($sTempFileName);
			                    return;
			                }
			
			                // check for image type
			                switch($aSize[2]) {
			                    case IMAGETYPE_JPEG:
			                        $sExt = '.jpg';
			                        break;
			                    case IMAGETYPE_PNG:
			                        $sExt = '.png';
			                        break;
			                    default:
			                        @unlink($sTempFileName);
			                        return;
			                }
		                    
		                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
		                    $sTempFileName = 'images/' .time().rand()."void"."{$sExt}";
		
		                    // move uploaded file into cache folder
		                    move_uploaded_file($_FILES['fileUploadVoidCheck']['tmp_name'], $sTempFileName);
							
							$sTempFileName = WEBROOT."/public/{$sTempFileName}";
	                 		// return $sTempFileName;   
		                }
	            	}
	            }
				
				if(isset($_FILES['fileUploadLogo']['tmp_name']))
				{
					if (! $_FILES['fileUploadLogo']['error'] && $_FILES['fileUploadLogo']['size'] < (1024*2) * 1024) {
	                	if (is_uploaded_file($_FILES['fileUploadLogo']['tmp_name'])) {
	
			                $aSize = getimagesize($_FILES['fileUploadLogo']['tmp_name']); // try to obtain image info
			                if (!$aSize) {
			                    @unlink($sTempFileName);
			                    return;
			                }
			
			                // check for image type
			                switch($aSize[2]) {
			                    case IMAGETYPE_JPEG:
			                        $sExt = '.jpg';
			                        break;
			                    case IMAGETYPE_PNG:
			                        $sExt = '.png';
			                        break;
			                    default:
			                        @unlink($sTempFileName);
			                        return;
			                }
		                    
		                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
		                    $logo = 'images/' .time().rand()."logo"."{$sExt}";
		
		                    // move uploaded file into cache folder
		                    move_uploaded_file($_FILES['fileUploadLogo']['tmp_name'], $logo);
							
							$logo = WEBROOT."/public/{$logo}";
	                 		// return $sTempFileName;   
		                }
	            	}
	            }
				
		}


		
		// print_r("hello");
		// die();
		//document - void check
		//WEBROOT."/public/{$sTempFileName}";
		//LOGO 
		
		//partner_type_id, email, merchant_id, logo, merchant_processor, 
		//company_name,dba, address1, address2, city state, zip, country, phone1, phone2,
		//create_by, document_image,
		//reward_info = array()
		
		//contact_information
		//last_name, first_name, position, mobile_number, office_number, extension, contact_email_address
		
        $insert_data = array(
        	'partner_id' => $this->parent_id,
        	'partner_type_id' => $_POST['txtPartnerTypeId'],
        	'email' => $_POST['txtEmail'],
        	'merchant_id' => $_POST['txtMerchantID'],
        	'logo' => $logo,
        	'merchant_processor' => $_POST['txtProcessor'],
        	'company_name' => $_POST['txtCompanyName'],
        	'dba' => $_POST['txtDBA'],
        	'address1' => $_POST['txtAddress1'],
        	'address2' => $_POST['txtAddress2'],
        	'city' => $_POST['txtCity'],
        	'state' => $_POST['txtState'],
        	'zip' => $_POST['txtZip'],
        	'country' => $_POST['txtCountry'],
        	'phone1' => $_POST['txtPhone1'],
        	'phone2' => $_POST['txtPhone2'],
        	'create_by' => $_SESSION['username'],
        	'document_image' => $sTempFileName,
        	'product_id' => $_POST['txtProduct'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'position' => $_POST['txtPosition'],
        	'mobile_number' => $_POST['txtMobileNumber'],
        	'office_number' => $_POST['txtOfficeNumber'],
        	'office_number2' => $_POST['txtOfficeNumber2'],
        	'extension' => $_POST['txtExtension'],
        	'contact_email_address' => $_POST['txtContactEmail'],
        	'fax' => $_POST['txtFax'],
        	'contact_fax' => $_POST['txtContactFax'],
        	'monthly_fee' => $_POST['txtMonthlyFee'],
        	'other_fee' => $_POST['txtOtherFee'],
        	
			'business_country' => $_POST['txtCountryBusiness'],
			'business_address' => $_POST['txtCountryBusiness'],
			'business_city' => $_POST['txtCityBusiness'],
			'business_state' => $_POST['txtStateBusiness'],
			'business_zip' => $_POST['txtZipBusiness'],
			'bank_name' => $_POST['txtBankName'],
			'account_name' => $_POST['txtAccountName'],
			'account_number' => $_POST['txtAccountNumber'],
			'routing_number' => $_POST['txtRoutingNumber'],
        	'website' => $_POST['txtWebsite']
        );
		
		
			
		if($_POST['txtProduct'] == 1) //for Rewards
		{
			$reward_info[] = array(
				'type' => 'PUNCHCARD',
				'information' => serialize(array(
					'no_of_holes' => $_POST['txtNoOfHole'],
					'start_date' => $_POST['txtStartDatePC'],
					'end_date' => $_POST['txtEndDatePC'],
					'description' => $_POST['txtDescriptionPC'],
					'freebie_description' => $_POST['txtFreebieDescription'],
					'freebie_hole_position' => $_POST['txtFreebieHolePosition'], 
					)
				)
			);
			$reward_info[] = array(
				'type' => 'PROMOTION',
				'information' => serialize(array(
					'is_unlimited' => isset($_POST['chkIsUnlimited']) ? $_POST['chkIsUnlimited'] : 0,
					'start_date' => $_POST['txtStartDatePromo'],
					'end_date' => $_POST['txtEndDatePromo'],
					'description' => $_POST['txtDescriptionPromo'],
					'required_point' => $_POST['txtRequiredPoints'],
					'required_amount' => $_POST['txtRequiredAmount'], 
					)
				)
			);
			
			$reward_info[] = array(
				'type' => 'LOYALTY',
				'information' => serialize(array(
					'name' => $_POST['txtLoyaltyName'],
					'description' => $_POST['txtLoyaltyDescription'],
					'date_reset' => $_POST['txtDateReset'],
					'description' => $_POST['txtDescriptionPromo'],
					'amount' => $_POST['txtDollarAmount'],
					'point_conversion' => $_POST['txtPointConversion'],
					'points' => $_POST['txtPoints'],
					'amount_converstion' => $_POST['txtAmountConversion'],
					'goal_point' => $_POST['txtGoalPoints'], 
					)
				)
			);
			
			$reward_info[] = array(
				'type' => 'MONTHLY FEE',
				'information' => $_POST['txtMonthlyFee']
			);
		
			
		}

		$reward_info[] = array(
			//'type' => 'OTHER FEE',
			'type' => $_POST['txtOtherFeeName'],
			'information' => $_POST['txtOtherFee']
		);
		
	
		if($_POST['txtProduct'] == 2) //for Website Online Ordering
		{
			$reward_info[] = array(
				'type' => 'ONLINE ORDERING',
				'information' => serialize(array(
					'order_type' => $_POST['txtOrderType'],
					'template_no' => $_POST['txtTemplateNo'],
					'plugin' => $_POST['txtPlugin'],
					'is_existing_domain' => $_POST['txtExistingDomain'],
					'domain_name' => $_POST['txtDomainName'],
					'domain_name_plugin' => $_POST['txtDomainNamePlugin'],
					'receive_order_by' => $_POST['txtReceiveOrderBy'],
					'accept_by' => $_POST['txtAcceptBy'], 
					'delivery_estimate_time' => $_POST['txtDeliveryTime'],
					'pickup_estimate_time' => $_POST['txtPickUpEstimatedTime'],  
					'delivery_fee' => $_POST['txtDeliveryFee'],
					'minimum_delivery' => $_POST['txtDeliveryMinimum'],
					'radius' => $_POST['txtDeliveryRadius'],
					'payment_method' => $_POST['txtPaymentMethod'],
					'sales_tax' => $_POST['txtSalesTax'],
					'payment_gateway' => $_POST['txtPaymentGateway'],
					'coupon_code' => $_POST['txtCouponCode'],
					'title' => $_POST['txtCouponTitle'],
					'discount_type' => $_POST['txtDiscountType'],
					'fixed_amount' => $_POST['txtFixedAmount'],
					'percentage' => $_POST['txtPercentage'],
					'how_many_times' => $_POST['txtHowManyTimes'], 
					'multiple_count' => $_POST['txtMultiple'],
					'fee_design' => $_POST['txtWebsiteDesign'],
					'fee_hosting' => $_POST['txtYearlyHosting'],
					'fee_setup' => $_POST['txtSetup'],
					'fee_service' => $_POST['txtMonthlyService'],
					)
				)
			);
		}
		
		
		$insert_data['reward_info'] = $reward_info;

        
		
		
        $result = $this->backend->create_merchant($insert_data);
        $response = $this->backend->get_response();
		// print_r($response);
		// die();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			$this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
			
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "New Merchant has been registered.",
                );
        }
    }
    
    
  }
?>
