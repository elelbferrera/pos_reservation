<?php
    class waiting_list extends template {
        protected $response;
		protected $merchant_id;
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
        }
        
      	public function management($is_show_all = 1)
        {
	
            $actions = array("add", "update", "delete", "search", "send_sms", "get_salon_specialist", "search_all_customer");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
			$today = date("Y-m-d");
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	'create_date' => $today,
            	// 'is_display_all_status' => 1
            );


            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_waiting_list_per_merchant', $params);

            // print_r($response);
            // die();
			
			if($response['respmsg'] > 0)
			{
				$waiting_lists = $response['respmsg'];
			}else{
				$waiting_lists = array();
			}
			


			$this->view->assign('waiting_lists', $waiting_lists);


			$response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
		 	$services = $response['respmsg'];
			
			$this->view->assign('services', $services);

			$response = lib::getWsResponse(API_URL, 'get_product_categories', $params);
		 	$categories = $response['respmsg'];
		 	$this->view->assign('categories', $categories);
        }

        private function get_salon_specialist()
      {
      		$params = array(
				'session_id' => $_SESSION['sessionid'],
				'services' => $_POST['services'],
				'date' => $_POST['date'],
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant_per_day', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$message ="";
			  		foreach($response['respmsg'] as $r)
			  		{
			  			$id =$r['id'];	
			  			$first_name =$r['first_name'];
			  			$last_name =$r['last_name'];
						$message .= "<div class='col-md-6 col-sm-6'>
	                        <label style='margin-right: 5px; font-size: 14px;'>
	                        	<input type='checkbox' value='{$id}' id='specialist[]' name='specialist[]' class='flat-red'> 
	                        	{$first_name} {$last_name}
	                        </label> 
	                  	</div>";

			  		}
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'message' => $message,
			        );
			  }
      }

      private function search_all_customer() {
      	$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'term' => $_GET['term']
        );

        $response = lib::getWsResponse(API_URL, 'search_customer_waiting_reservation', $params);
	 	$all_customers = $response['respmsg'];
	 	
	 	$list = array();
        
	 	foreach ($all_customers as $value) {
	 		$list[] = $value['last_name'] . "," . $value['first_name'] . "," . $value['email_address'];
	 	}

	 	//var_dump($list);die;

	 	$this->response = $list;
	}

      private function send_sms()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	// 'status' => $_POST['txtStatus'],
        	'last_name' => $_POST['txtMessageLastName'],
        	'first_name' => $_POST['txtMessageFirstName'],
        	'contact_number' => $_POST['txtMessageContactNumber'],
        	'create_date' => date('Y-m-d'),
        	'message' => $_POST['txtMessage'],
         );



        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'send_sms_to_customer', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }

		
	  private function search()
	  {
			$params = array(
				'session_id' => $_SESSION['sessionid'],
				'mobile' => str_replace("-", "", $_POST['txtMobileSearch']) 
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'search_customer_by_mobile', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'last_name' => $resp['last_name'],
			            'first_name' => $resp['first_name'],
			            'mobile' => $resp['mobile'],
			        );
			  }
	  }
	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	// 'status' => $_POST['txtStatus'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'contact_number' => $_POST['txtContactNumber'],
        	'email_address' => $_POST['txtEmailAddress'],
        	'create_date' => date('Y-m-d'),
        	'check_in_time' => date('H:i:s'),
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );

          //LC-09/11/2013
          $response = lib::getWsResponse(API_URL, 'check_in_for_waiting_list', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtCheckInId'],
        	'status' => $_POST['txtStatus'],
        	'update_date' => date('Y-m-d'),
        	'update_time' => date('H:m:s'),
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );
		// die();
	    
        $response = lib::getWsResponse(API_URL, 'update_waiting_status', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	
    protected function delete()
    {
        $id = $_GET['id'];
        $result = $this->backend->delete_branch($id);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => $response['ResponseMessage'],
                );
        }  
    }
        
        
    }
?>
