<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>

<div id="content">
<h3>Bills Payment</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
            <h4 class="heading">Setting Information</h4>
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>
            <a href="{$webroot}/customers/registration">
                <h4 class="floatright">+ Register Customers </h4>
            </a>-->
        </div>                                                                                       
        
        <div class="widget-body">
            <fieldset id="fldSettingSystemConfig";padding:10px"><legend>&nbsp;Search Result&nbsp;</legend>
            <table id="tblSettingSystemConfig" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>       
                    <th width="40%">Setting Name</th>
                    <th width="30%">Value</th>
                    <th width="30%">Action</th>
                </tr>
            </thead>
            <tbody>
            {foreach from=$result item=item}
                <tr>
                    <td>{$item.name}</td>
                    <td>{$item.set_value}</td>
                    <td> <input type="button" class="btn btn-warning" value="Edit" id="{$item.id}" onClick="showEditSettingDialog('{$item.id}','{$item.name}','{$item.set_value}')" />
                    </td>                                                                  
                </tr>
            {/foreach}
        </tbody>
            </table>
            </fieldset>

        </div>
        
        <div id="dlgEditSetting" title="Edit Setting" style="display:none">
        <form id="frmEditSetting" name="frmEditSetting">
        <input type="hidden" name="settingid" id="settingid" maxlength="10" />
        <table width="100%">
            <hr>
            <tbody>
                <tr>
                    <td class="tbl_label">Setting Name:</td>
                        <td>
                            <input type="text" name="settingname" id="settingname" maxlength="18" readonly/>
                        </td>
                </tr>
                <tr>
                    <td class="tbl_label">Value:</td>
                    <td>
                        <input type="text" name="settingvalue" id="settingvalue" maxlength="10" />
                     </td>
                </tr>
            </tbody>
        </table>
        <br>
        <hr>
        <input type="submit" name="btnSave" id="btnSave" value="Save" class="btn btn-danger"/>
        </form>
        </div>
        
    {$_messages_}
    
    
    </div>
</div>
</div>

        
