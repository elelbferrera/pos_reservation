<?php /* Smarty version 2.6.26, created on 2016-12-14 04:08:34
         compiled from service_management.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/service.js"></script>
 <section id="content">
    <section class="vbox">
       <header class="header bg-gray b-b b-light">
                                <div class="row">
                                     <div class="col-md-5 col-xs-4">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">Product and Services Offer</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-4 col-xs-3 ta-right">
                                        <!-- <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div> -->
                                    </div>
                                     <div class="col-md-3 col-xs-5 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New Product and Service" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add_service()">Add New Service</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable wrapper">
        	<ul id="example">
	            <div class="row">
	            	<?php $_from = $this->_tpl_vars['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	            		
	            	
	                    <div class="col-md-4">
	                        <div class="services-holder">
	                            <div class="row">
	                                <div class="col-md-6">
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
cost" name="<?php echo $this->_tpl_vars['item']['id']; ?>
cost"><?php echo $this->_tpl_vars['item']['cost']; ?>
</div>
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_type" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_type"><?php echo $this->_tpl_vars['item']['product_type']; ?>
</div>
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
minutes" name="<?php echo $this->_tpl_vars['item']['id']; ?>
minutes"><?php echo $this->_tpl_vars['item']['minutes_of_product']; ?>
</div>
	                                	
	                                    <div class="title-services" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_name"><?php echo $this->_tpl_vars['item']['name']; ?>
</div>
	                                    <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
quantity" name="<?php echo $this->_tpl_vars['item']['id']; ?>
quantity"><?php echo $this->_tpl_vars['item']['quantity']; ?>
</div>
	                                    <div class="description-services"  id="<?php echo $this->_tpl_vars['item']['id']; ?>
description" name="<?php echo $this->_tpl_vars['item']['id']; ?>
description">
	                                    		<?php echo $this->_tpl_vars['item']['description']; ?>

	                                   	</div>
	                                </div>
	                                <div class="col-md-3 ta-right">
	                                    <div class="price-services" id="<?php echo $this->_tpl_vars['item']['id']; ?>
price">$<?php echo $this->_tpl_vars['item']['price']; ?>
</div>
	                                </div>
	                            </div>
	                            <div class="ta-right">
	                                <button type="button" class="btn btn-sm btn-primary" onclick="view_service('<?php echo $this->_tpl_vars['item']['id']; ?>
')">View</button>
	                                <button type="button" class="btn btn-sm btn-dark" onClick="edit_service('<?php echo $this->_tpl_vars['item']['id']; ?>
')">Edit</button>
	                            </div>
	                        </div>
	                    </div>
	                <?php endforeach; endif; unset($_from); ?>
	            </div>
            </ul>
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>