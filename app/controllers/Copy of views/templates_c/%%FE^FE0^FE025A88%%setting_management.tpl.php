<?php /* Smarty version 2.6.26, created on 2016-12-18 10:49:26
         compiled from setting_management.tpl */ ?>
<!-- .content -->
                      <section id="content">
                                <section class="vbox">  
                                       <header class="header bg-darkblue b-light">
                                        <div class="row">
                                             <div class="col-md-12 col-xs-12">
                                                <div class="breadtitle-holder">
                                                    <div class="breadtitle">
                                                        <i class="fa fa-cog titleFA"></i> 
                                                        <p class="headeerpage-title">Settings</p>
                                                    </div>
                                                </div>
                                             </div>
                                           
                                        </div>
                                    </header>
                                    <section class="scrollable wrapper">
                                       <h3 class="m-b-none price-services">Hi Merchant</h3> <small>this your settings management</small> </div>
                                            <ul class="shortcuts">
                                                 <li class="col-md-3 col-xs-3">
                                                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant_user/management">
                                                        <i class="fa fa-users shortcuts-icon"></i> <br />
                                                        User Account
                                                    </a>
                                                </li>
                                                <li class="col-md-3 col-xs-3">
                                                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/branch/management">
                                                        <i class="fa fa-home shortcuts-icon"></i> <br />
                                                        My Branch
                                                    </a>
                                                </li>
                                                 <li class="col-md-3 col-xs-3">
                                                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/status/management">
                                                        <i class="fa fa-cog shortcuts-icon"></i> <br />
                                                        My Status
                                                    </a>
                                                </li>
                                               
                                              <!--      
                                                <li class="col-md-3 col-xs-3">
                                                    <a href="coupon-management.php">
                                                        <i class="fa fa-tags shortcuts-icon"></i> <br />
                                                        Coupon
                                                    </a>
                                                </li>
                                               -->
                                            </ul>
                                    </section>

                               </section>
                     </section>

                <!-- /.content -->
               
            </section>
        </section>
    </section>