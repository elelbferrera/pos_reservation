<?php /* Smarty version 2.6.26, created on 2016-09-06 04:14:07
         compiled from product_management.tpl */ ?>
	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Products
                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/product/add">
                    	<button class="btn btn-primary" type="button">Add New Product</button>
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Product</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Product List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Description</th>
                                                <th>Create Date</th>
                                                <th>Buy Rate</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	                                            <tr>
	                                            	<td><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/product/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></td>
	                                                <td><?php echo $this->_tpl_vars['item']['description']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['create_date']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['buy_rate']; ?>
</td>
	                                            </tr>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->