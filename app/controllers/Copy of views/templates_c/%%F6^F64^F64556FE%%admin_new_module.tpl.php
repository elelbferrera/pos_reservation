<?php /* Smarty version 2.6.26, created on 2016-08-30 23:00:04
         compiled from admin_new_module.tpl */ ?>
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Module
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/acl"><i class="fa fa-dashboard"></i> Profiles</a></li>
                        <li class="active">Module</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-header with-border">
                                	<!-- <h3 class="box-title"><?php echo $this->_tpl_vars['profile_info']['profile_name']; ?>
 Profile</h3> -->
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="frmAdminAddNewModule" method="post" name="frmAdminAddNewModule">
									<div class="col-md-6">
                                            <div class="form-group">
                                                <label for="processor">Module Name:</label>
                                                <input type="text" class="form-control" id="adminModuleName" name="adminModuleName" size="31" required  placeholder="Module Name">
                                            </div>
											<div class="form-group">
                                                <label for="processor">Value:</label>
                                                <input type="text" class="form-control" id="adminModuleValue" name="adminModuleValue" size="31" required  placeholder="Module Value">
                                            </div>
                                            <div class="form-group">
                                    			<button id="btnSaveProfile" type="submit" class="btn btn-primary">Save Profile</button>
                                    		</div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                    	
                                    	
                                    </div>

                                    <div class="box-body">
                                    	<table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th width="50%">Module Name</th>
												<th width="30%">Value</th>
												<th width="20%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									        <?php $_from = $this->_tpl_vars['result']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                <tr>
									                    <td><?php echo $this->_tpl_vars['item']['description']; ?>
</td>
                    									<td><?php echo $this->_tpl_vars['item']['resource']; ?>
</td>
									                    <td>
									                        <input type="button" class="btn btn-warning" value="Edit" id="<?php echo $this->_tpl_vars['item']['id']; ?>
" onClick="showEditAdminModuleDialog('<?php echo $this->_tpl_vars['item']['id']; ?>
','<?php echo $this->_tpl_vars['item']['description']; ?>
','<?php echo $this->_tpl_vars['item']['resource']; ?>
')" /> 
                    										<input type="button" class="btn btn-danger" value="Delete" id="<?php echo $this->_tpl_vars['item']['id']; ?>
" onClick="deleteModule('<?php echo $this->_tpl_vars['item']['id']; ?>
')" />
									                    </td>
									                </tr>
									        <?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    	</table>
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer">
                                        
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
 

    
<div id="dlgEditAdminModuleDialog" class="modal fade" role="dialog">
	
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Module</h4>
              </div>
              <form id="frmEditAdminModule" name="frmEditAdminModule">
              <div class="modal-body">
              	
        		<input type="hidden" name="admin_module_id" id="admin_module_id" maxlength="10" />
        		<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="processor">Module Name:</label>
                        <input type="text" class="form-control" id="admin_module_name" name="admin_module_name" size="100" required  placeholder="Module Name">
                    </div>
				</div>
				<div class="col-md-12">
                    <div class="form-group">
                        <label for="processor">Module Value:</label>
                        <input type="text" class="form-control" id="admin_module_value" name="admin_module_value" size="100" required  placeholder="Module Name">
                    </div>
				</div>
				</div>
                
              </div>
              <div class="modal-footer">
              	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              	<input type="submit" name="btnSave" id="btnSave" value="Save" class="btn btn-primary"/>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
     
</div>