<?php /* Smarty version 2.6.26, created on 2016-12-19 03:09:59
         compiled from status_management.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/status.js"></script>
<section id="content">
                        <section class="vbox">  


                        <section class="scrollable">


                          <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-user titleFA"></i> 
                                                <p class="headeerpage-title">Reservation Status List</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-9 col-xs-9 ta-right">
                                        <div class="breadtitle-holder2">
                                            <div class="btn-group" onclick="add()">
                                                  <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-user"></i></button>
                                                      <div class="btn-group hidden-nav-xs">
                                                          <button type="button" class="btn btn-sm btn-primary">Create New Status</button>
                                                    </div>
                                            </div>
                                            <!-- <div class="btn-group">
                                                <div class="input-group">
                                                   <input type="text" class="form-control" placeholder="Search">
                                               </div>
                                            </div>
                                             <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary btn-icon" title="New project"><i class="fa fa-search"></i></button>
                                             </div> -->
                                        </div>
                                     </div>
                                </div>
                            </header>

                              <!-- table -->
                              <table id="tbUser" class="waitlist-table">
                                 
                                    <tr class="header bg-gray b-b b-light">
                                      <th class="waitlist-table-td-th">Date Registered</th>
                                      <th class="waitlist-table-td-th">Name</th>
                                      <th class="waitlist-table-td-th">Description</th>
                                      <th class="waitlist-table-td-th">Color</th>
                                      <th class="waitlist-table-td-th">Color Code</th>
                                      <th class="waitlist-table-td-th" style="text-align: center;">Action</th>
                                    </tr>
									<?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
id"><?php echo $this->_tpl_vars['item']['id']; ?>
</div>
									<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
name"><?php echo $this->_tpl_vars['item']['name']; ?>
</div>
									<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
description" name="<?php echo $this->_tpl_vars['item']['id']; ?>
description"><?php echo $this->_tpl_vars['item']['description']; ?>
</div>
									<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
color" name="<?php echo $this->_tpl_vars['item']['id']; ?>
color"><?php echo $this->_tpl_vars['item']['color']; ?>
</div>
                                    <tr>
                                      <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['create_date']; ?>
</td>
                                      <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
                                      <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['description']; ?>
</td>
                                      <td class="waitlist-table-td-th">
                                      	<div style="min-height:20px; width:100%; background-color:<?php echo $this->_tpl_vars['item']['color']; ?>
;">
                                      </td>
                                      <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['color']; ?>
</td>
                                      <td style="text-align: center; font-size:25px;border-bottom: 1px solid #dddddd;"> 
                                      	<a href="#" onclick="edit('<?php echo $this->_tpl_vars['item']['id']; ?>
')"><i class="fa fa-edit"></i> </a> 
                                      	<!-- <a href="" data-toggle="modal" data-target="#question"><i class="fa fa-trash-o"></i> </a></td> -->
                                    </tr>
									<?php endforeach; endif; unset($_from); ?>
                                    
                              </table>
                              <!-- /. table -->

                              </section>



                              
                       </section>
                     </section>

                <!-- /.content -->
               
            </section>
        </section>
    </section>






<!-- Modal -->
<div id="question" class="modal fade" role="dialog">
  <div class="modal-dialog3">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ta-center">
                      <p>Are you sure want to delete?</p>
                      <button type="button" class="btn btn-md btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#updateservices">Yes</button>
                      <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        
  </div>
</div>



<div id="dlgEditStatus" name="dlgEditStatus" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Status Information</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
                   <form role="form" name="frmStatus" id="frmStatus">
                   			<input type="hidden" name="txtStatusId" id="txtStatusId" />
                                               <div class="row">
                                                 <div class="col-md-12 col-xs-12">
                                                      <div class="form-group">
                                                            <label>Status Name</label>
                                                            <input type="text" class="form-control" placeholder="Status Name" id="txtName" name="txtName"> 
                                                       </div>
                                                       <div class="form-group">
                                                            <label>Description</label>
                                                            <input type="text" class="form-control" id="txtDescription" name="txtDescription" placeholder="Description"> 
                                                       </div>
								                       <div class="form-group">
								                          <label>Color Status:</label>
								                          <input type="text" name="txtColor" id="txtColor" class="form-control my-colorpicker1">
								                       </div>
                                                  </div>
                                               </div>
                                              
                                              <!-- <div class="row">
                                                 <div class="col-md-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>User Level</label>
                                                                <select name="txtUserType" id="txtUserType" class="form-control m-b">
                                                                    <option>Admin</option>
                                                                    <option>Staff</option>
                                                                </select>
                                                        </div>
                                                  </div>
                                               </div> -->

                                              
				</form>                      
    
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
           			<button type="button" name="btnSaveStatus" id="btnSaveStatus" class="btn btn-primary" data-dismiss="modal">Save</button>
                   	<button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>



  </div>
</div>