<?php /* Smarty version 2.6.26, created on 2016-09-26 03:42:22
         compiled from agent_edit.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/agent.js"></script>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Update Agent
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/agent/management"><i class="fa fa-dashboard"></i> Agents</a></li>
                        <li class="active">Profile</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <!-- <div class="col-sm-3">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="summary.php">Summary</a></li>
                                    <li><a href="profile.php">Profile</a></li>
                                    <li><a href="#">Contacts</a></li>
                                    <li><a href="#">Payment Info</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">Invoices</a></li>
                                    <li><a href="#">Attachments</a></li>
                                    <li><a href="#">Notes</a></li>
                                    <li><a href="#">Communication</a></li>
                                    <li><a href="#">Log</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <div class="col-sm-12">
                            <div class="box">
                                <!-- <div class="box-header with-border">
                                	<h3 class="box-title">Company Profile</h3>
                                </div> -->
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frmRegisterAgent" id="frmRegisterAgent">
                                	<input type="hidden" name="txtProfileId" id="txtProfileId" value="<?php echo $this->_tpl_vars['agent_id']; ?>
" >
                                    <div class="box-body">
                                    	<div class="row">
                                            <!-- <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label for="processor">Processor:</label>
	                                                <input type="text" class="form-control" id="processor" placeholder="Enter processor">
	                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="merchantID">Merchant ID:</label>
                                                    <input type="text" class="form-control" id="merchantID" placeholder="Enter merchant id">
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12"><h4>Company Profile:</h4></div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Agent ID:</label>
                                                    <input type="text" class="form-control" id="txtAgentId" name="txtAgentId" placeholder="Enter Agent Id" value="<?php echo $this->_tpl_vars['agent']['partner_id_reference']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">Partner:</label>
                                                    <input value="<?php echo $this->_tpl_vars['agent']['agent_partner']; ?>
" type="text" class="form-control" id="txtPartner" name="txtPartner" placeholder="Enter Partner">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Firstname:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['first_name']; ?>
"  class="form-control" id="txtFirstName" name="txtFirstName" placeholder="Enter First Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Last Name:</label>
                                                    <input type="text"  value="<?php echo $this->_tpl_vars['agent']['last_name']; ?>
" class="form-control" id="txtLastName" name="txtLastName" placeholder="Enter Last Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text"  value="<?php echo $this->_tpl_vars['agent']['address1']; ?>
" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['address2']; ?>
" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['city']; ?>
" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text"  value="<?php echo $this->_tpl_vars['agent']['state']; ?>
" class="form-control" id="txtState" name="txtState" placeholder="Enter state">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['zip']; ?>
" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
									                  <?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
" <?php if ($this->_tpl_vars['item']['name'] == $this->_tpl_vars['agent']['country']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" value="<?php echo $this->_tpl_vars['agent']['email']; ?>
" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['mobile_number']; ?>
" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['agent']['other_number']; ?>
" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: none">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <!-- <option selected="selected">Alabama</option>
									                  <option>Alaska</option>
									                  <option>California</option>
									                  <option>Delaware</option>
									                  <option>Tennessee</option>
									                  <option>Texas</option>
									                  <option>Washington</option> -->
									                  <?php $_from = $this->_tpl_vars['partner_type']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"<?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['agent']['partner_type_id']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>

                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer right">
                                        <button type="submit" class="btn btn-primary" id="btnUpdateAgent" name="btnUpdateAgent">
                                        	Update
                                        </button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->