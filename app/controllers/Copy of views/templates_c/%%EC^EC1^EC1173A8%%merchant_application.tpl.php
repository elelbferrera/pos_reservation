<?php /* Smarty version 2.6.26, created on 2016-10-21 01:24:38
         compiled from merchant_application.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'merchant_application.tpl', 63, false),array('modifier', 'count', 'merchant_application.tpl', 204, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/application.js">
</script>

	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Applications
                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/add">
                    	<!-- <button class="btn btn-primary" type="button">Add New Partner</button> -->
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <!-- <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li> -->
                        <li class="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/management">Merchant</li>
                        <li class="active">Applications</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Application List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="applicationlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="no-sort"></th>
                                                <th>Date</th>
                                                <th>Application ID.</th>
                                                <th>Product</th>
                                                <th>Agent Name</th>
                                                <th>Legal Name</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php $_from = $this->_tpl_vars['applications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        	
                                            <!-- <tr> -->
                                            	<!-- <td class="icon-td">
                                            		<a href="#" data-toggle="modal" data-target="#comment-modal"><i class="fa fa-comment"></i></a></td>
                                                <td>10/02/2012</td>
                                                <td>001</td>
                                                <td>Gift &amp; Rewards</td>
                                                <td>908 Hibachi Inc.</td>
                                                <td>Pending</td> -->
											<!-- </tr> -->
											<tr>
												<td class="icon-td">
													<a href="#" data-toggle="modal" data-target="#comment-modal<?php echo $this->_tpl_vars['item']['order_id']; ?>
"><i class="fa fa-comment"></i></a>
												</td>
												<td><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['create_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</td>
												<td><?php echo $this->_tpl_vars['item']['order_id']; ?>
</td>
												<td><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
												<td><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/agent/edit/<?php echo $this->_tpl_vars['item']['parent_id']; ?>
"><?php echo $this->_tpl_vars['item']['first_name']; ?>
 <?php echo $this->_tpl_vars['item']['last_name']; ?>
</a></td>
												<td><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['partner_id']; ?>
"><?php echo $this->_tpl_vars['item']['company_name']; ?>
</a></td>
												<td><?php echo $this->_tpl_vars['item']['status']; ?>

													<?php if ($this->_tpl_vars['is_admin']): ?>
													<a href="#" onclick="showModalStatus('<?php echo $this->_tpl_vars['item']['order_id']; ?>
','<?php echo $this->_tpl_vars['item']['status']; ?>
')">
														<i class="fa fa-edit"></i>
													</a>
													<!-- <input type="button" name="btnChange"  class="btn btn-primary" value="Change Status" id="btnChange"/> -->
													<?php endif; ?>
												</td>
											</tr>
											<?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    
      <!-- Main Footer -->
  <footer class="main-footer">
	<div class="container">
    	<div class="row">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                <!--Anything you want-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 <a href="./">GOETU</a>.</strong> All rights reserved.
        </div>
    </div>
</footer>
 
<?php $_from = $this->_tpl_vars['applications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?> 
<div class="modal fade custom-modal modal-comment" tabindex="-1" role="dialog" id="comment-modal<?php echo $this->_tpl_vars['item']['order_id']; ?>
">

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Comments - <?php echo $this->_tpl_vars['item']['name']; ?>
 | <?php echo $this->_tpl_vars['item']['company_name']; ?>
  </h4>
                </div>
                <div class="modal-body">
                	<form id="frmComment<?php echo $this->_tpl_vars['item']['order_id']; ?>
" name="frmComment<?php echo $this->_tpl_vars['item']['order_id']; ?>
" action="post" enctype="multipart/form-data">
                		<input type="hidden" name="txtProductId" id="txtProductId" value="<?php echo $this->_tpl_vars['item']['product_id']; ?>
"/>
                		<input type="hidden" name="txtOrderId" id="txtOrderId" value="<?php echo $this->_tpl_vars['item']['order_id']; ?>
"/>
                		<input type="hidden" name="txtPartnerId" id="txtPartnerId" value="<?php echo $this->_tpl_vars['item']['partner_id']; ?>
"/>
                		<input type="hidden" name="txtParentId" id="txtParentId" value="-1"/>
                		
	                	<div id="post-comment">
	                        <div class="form-group">
	                        	<input type="radio" class="inputradio" name="comment-type" id="comment-public<?php echo $this->_tpl_vars['item']['order_id']; ?>
" value="Public" checked> <label for="comment-public<?php echo $this->_tpl_vars['item']['order_id']; ?>
">Public Response</label>
								<input type="radio" class="inputradio" name="comment-type" id="comment-agent<?php echo $this->_tpl_vars['item']['order_id']; ?>
" value="Agent"> <label for="comment-agent<?php echo $this->_tpl_vars['item']['order_id']; ?>
">Agent Note</label>
	                            
	                            <?php if ($this->_tpl_vars['is_admin']): ?>
	                            <input type="radio" class="inputradio" name="comment-type" id="comment-internal<?php echo $this->_tpl_vars['item']['order_id']; ?>
" value="Internal"> <label for="comment-internal<?php echo $this->_tpl_vars['item']['order_id']; ?>
">Internal Note</label>
	                            <?php endif; ?>
	                            <div class="custom-fl-right comment-view">
	                            	<a href="#" class="cv-showall" onclick="showAllReplies()" title="Show All"><i class="fa fa-navicon"></i></a>
	                                <a href="#" class="cv-showless" onclick="hideAllReplies()" title="Show Less"><i class="fa fa-minus"></i></a>
	                            </div>
	                            <textarea name="txtComment" name="txtComment" class="form-control custom-textarea" placeholder="Type here..." rows="2"></textarea>
	                            <input type="file" name="file<?php echo $this->_tpl_vars['item']['order_id']; ?>
" id="file<?php echo $this->_tpl_vars['item']['order_id']; ?>
" class="inputfile" data-multiple-caption="files selected" multiple />
	                            <label for="file<?php echo $this->_tpl_vars['item']['order_id']; ?>
"><i class="fa fa-upload"></i> <span>Attach file</span></label>
	                            <!-- <div class="custom-fl-right">
	                                Update Status: 
	                                <select class="form-control">
	                                	<option>DO NOTHING</option>
	                                    <option>OPTION 1</option>
	                                    <option>OPTION 2</option>
	                                </select>
	                            </div> -->
	                        </div>
	                        <div class="form-group ta-right">
	                            <button type="submit" class="btn btn-primary">Post</button>
	                        </div>
	                    </div>
                   </form>
                   <?php $_from = $this->_tpl_vars['item']['comments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['comment']):
?> 
                    <div id="comment-list">
                    	
                    	<div class="comment" id="comment<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
">
                        	<div class="comment-block comment-main">
                            	<span class="comment-author"><?php echo $this->_tpl_vars['comment']['first_name']; ?>
 <?php echo $this->_tpl_vars['comment']['last_name']; ?>
</span> | 
                            	<span class="comment-date"><?php echo $this->_tpl_vars['comment']['create_date']; ?>
</span>
                                <div class="comment-desc">
                                	<?php echo $this->_tpl_vars['comment']['comment']; ?>

                                </div>
                            </div>
                            
                   			  
                   			<?php $_from = $this->_tpl_vars['comment']['sub_comments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sub']):
?> 			
                            <div class="comment-block comment-reply">
                            	<span class="comment-author"><?php echo $this->_tpl_vars['sub']['first_name']; ?>
 <?php echo $this->_tpl_vars['sub']['last_name']; ?>
</span> | 
                            	<span class="comment-date"><?php echo $this->_tpl_vars['sub']['create_date']; ?>
</span>
                                <div class="comment-desc">
                                	<?php echo $this->_tpl_vars['sub']['comment']; ?>

                                </div>
                            </div>
                            <?php endforeach; endif; unset($_from); ?>
                            
                            <!-- <div class="comment-block comment-reply">
                            	<span class="comment-author">Child John Doe</span> | <span class="comment-date">October 6, 2016 10:55 AM</span>
                                <div class="comment-desc">
                                	test 2
                                </div>
                            </div> -->

                        	
                        	
                            <form name="frmSubComment<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" id="frmSubComment<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
">
	                            <div class="comment-post-reply" id="divCommentPostReply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" name="divCommentPostReply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
">
	                                <div class="form-group">
	                                	<input type="hidden" name="txtProductId" id="txtProductId" value="<?php echo $this->_tpl_vars['item']['product_id']; ?>
"/>
				                		<input type="hidden" name="txtOrderId" id="txtOrderId" value="<?php echo $this->_tpl_vars['item']['order_id']; ?>
"/>
				                		<input type="hidden" name="txtPartnerId" id="txtPartnerId" value="<?php echo $this->_tpl_vars['item']['partner_id']; ?>
"/>
	                                	<input type="hidden" name="txtParentId" id="txtParentId" value="<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
"/>
 	                                    <textarea name="txtComment" id="txtComment" class="form-control custom-textarea" placeholder="Type here..." rows="2"></textarea>
	                                    <input type="file" name="file<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" id="file<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" class="inputfile" data-multiple-caption="files selected" multiple />
	                                    <label for="file<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
"><i class="fa fa-upload"></i> <span>Attach file</span></label>
	                                    <div class="custom-fl-right">
	                                        <button type="submit" class="btn btn-primary">Post</button>
	                                    </div>
	                                </div>
	                            </div>
                            </form>
                            <div class="comment-options">
                            	<a href="#" id="addreply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" name="addreply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" class="addreply" onClick="addReply('<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
')"><i class="fa fa-reply"></i> Add Reply</a>
                                <a href="#" id="cancelreply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" name="cancelreply<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" class="cancelreply" onClick="cancelReply('<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
')"><i class="fa fa-times"></i> Cancel Reply</a>
                                <?php if (count($this->_tpl_vars['comment']['sub_comments']) > 0): ?>
	                                <a href="#" class="showall" name="showall<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" id="showall<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" onclick="showAllSpecific('<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
')">
	                                	(<?php echo count($this->_tpl_vars['comment']['sub_comments']); ?>
)Show All
	                                </a>
                                <?php endif; ?>
                                <a href="#" class="showless" name="showless<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" id="showless<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
" onclick="hideAllSpecific('<?php echo $this->_tpl_vars['comment']['comment_id']; ?>
')">Show Less</a>
                            </div>
                            
                        </div> 
                    
                    </div>
                    <?php endforeach; endif; unset($_from); ?>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

	
</div><!-- /.modal -->

<?php endforeach; endif; unset($_from); ?>


<div class="modal fade custom-modal" tabindex="-1" role="dialog" id="update_status" name="update_status">
	<form name="frmChangeStatus" id="frmChangeStatus">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            	<input type="hidden" name="txtOrderIdStatus" id="txtOrderIdStatus">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Order Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                    	<label for="compname">Status:</label>
						<select class="form-control select2" style="width: 100%;" id="txtStatus" name="txtStatus">
			                  <?php $_from = $this->_tpl_vars['statuses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
			                  	<option value="<?php echo $this->_tpl_vars['item']['status']; ?>
"><?php echo $this->_tpl_vars['item']['status']; ?>
</option>
			                  <?php endforeach; endif; unset($_from); ?>
			            </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
     </form>
 </div><!-- /.modal -->