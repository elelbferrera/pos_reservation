<script type="text/javascript" src="{$webroot_resources}/js/specialist.js"></script>
                 <!-- .content -->
                   <section id="content">
                        <section class="vbox">
                            <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-6 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-users titleFA"></i> 
                                                <p class="headeerpage-title">Specialist</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-6 col-xs-9 ta-right">
                                        <div class="breadtitle-holder2">
                                            <!-- <div class="btn-group" style="margin-right:10px;">
                                                      <select class="form-control" name="repeatReservation">
                                                      <option selected="selected" value="">- sort by branch -</option>
                                                      <option>Bulacan Branch</option>
                                                      <option>Ortigas</option>
                                                      <option>Cubao</option>
                                                      <option>US</option>
                                                  </select>
                                            </div> -->
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary" onclick="add()">New Specialist</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                </div>
                            </header>
                            <section class="scrollable wrapper">
                                 <!-- <div id="example" class="overview"> -->
                                        
                                        <!-- <div class="viewport">
                                            <ul class="overview"> -->
                                            <ul id="example" class="overview">
                                            {foreach from=$specialist item=item}
                                            
                                                <li class="col-md-4 col-sm-4">
                                                    <div class="row">
                                                        <div class="specialist-header-list">Specialist ID #{$item.id}</div>
                                                        <div class="specialist-body-list">
                                                             <div class="row">
                                                                <div class="col-md-4">
                                                                      <img src="{$webroot_resources}/images/avatar_default.jpg" class="specialist-avatar" />
                                                                      <div class="clear"></div>
                                                                      <!--    <div class="ta-center" style="margin-top:10px;">
                                                                             <div class="price-services">14</div>
                                                                             <p>Total Service today</p>
                                                                         </div> -->
                                                                         <div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
                                                                         <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
                                                                         <div style="display: none" id="{$item.id}branch_id" name="{$item.id}branch_id">{$item.branch_id}</div>
                                                                         <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
                                                                         <div style="display: none" id="{$item.id}middle_name" name="{$item.id}middle_name">{$item.middle_name}</div>
                                                                         <div style="display: none" id="{$item.id}birth_date" name="{$item.id}birth_date">{$item.birth_date}</div>
                                                                         <div style="display: none" id="{$item.id}profile_picture" name="{$item.id}profile_picture">{$item.profile_picture}</div>
                                                                         <div style="display: none" id="{$item.id}gender" name="{$item.id}gender">{$item.gender}</div>
                                                                         <div style="display: none" id="{$item.id}city" name="{$item.id}city">{$item.city}</div>
                                                                         <div style="display: none" id="{$item.id}address" name="{$item.id}address">{$item.address}</div>
                                                                         <div style="display: none" id="{$item.id}state" name="{$item.id}state">{$item.state}</div>
                                                                         <div style="display: none" id="{$item.id}country" name="{$item.id}country">{$item.country}</div>
                                                                         <div style="display: none" id="{$item.id}schedule" name="{$item.id}schedule">{$item.schedule}</div>
                                                                         <div style="display: none" id="{$item.id}start_time" name="{$item.id}start_time">{$item.start_time}</div>
                                                                         <div style="display: none" id="{$item.id}end_time" name="{$item.id}end_time">{$item.end_time}</div>
                                                                         <div style="display: none" id="{$item.id}start_date" name="{$item.id}start_date">{$item.start_date}</div>
                                                                         <div style="display: none" id="{$item.id}end_date" name="{$item.id}end_date">{$item.end_date}</div>
                                                                </div>
                                                                <div class="col-md-8 ta-right">
                                                                    <div class="specialist-name">{$item.first_name} {$item.last_name}<br />
                                                                          <p>
                                                                          <!-- Position: <span> Body Treatment Expert</span><br /> -->
                                                                          Branch Assign:  <b>{$item.branch_name}</b><br /><br />
                                                                          <!-- Status: --></p>
                                                                          <!-- <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#newspecialist">View</button> -->
                                                                          <button type="button" class="btn btn-md btn-dark" onclick="edit('{$item.id}')">Edit</button>
                                                                         
                                                                         <!--  <div class="specialist-status-onservice col-sm-7">On Service</div> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="row">
                                                                <div class="specialist-status-onservice col-sm-5">Schedule Today</div>
                                                                <div class="specialist-status-viewall col-sm-4 col-sm-offset-3"><a href="specialist-full-schedule.php">View Schedule</a></div>
                                                                <table class="specialist-sched-table">
                                                                      <tr>
                                                                        <th>S#</th>
                                                                        <th>Service Type</th>
                                                                        <th>Time</th>
                                                                      </tr>

                                                                      <tr>
                                                                        <td>32</td>
                                                                        <td>Nail Spa</td>
                                                                        <td>4:13pm</td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>35</td>
                                                                        <td>Nail Spa</td>
                                                                        <td>5:13pm</td>
                                                                      </tr>
                                                                </table>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </li>
                                                {/foreach}
                                                
                                            </ul>
                                        <!-- </div> -->
                                        <!-- <div class="controls-specialist">
                                            <a class="buttons btn btn-sm btn-dark prev" href="#">&#60;</a>
                                            <a class="buttons btn btn-sm btn-dark next" href="#">&#62;</a>
                                        </div> -->
                                    <!-- </div> -->
                            </section>
                        </section>
                  </section>
                <!-- /.content -->
               
            <!-- </section>
        </section>
    </section> -->









<div id="dlgEditSpecialist" name="dlgEditSpecialist" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Specialist Information</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
      <div class="row">
             <form role="form" id="frmSpecialist" name="frmSpecialist">
             	<input type="hidden" id="txtSpecialistId" name="txtSpecialistId">
             	<input type="hidden" id="txtDays" name="txtDays">
             	<input type="hidden" id="txtStartTime" name="txtStartTime">
             	<input type="hidden" id="txtEndTime" name="txtEndTime">
                 <div class="col-md-3 col-xs-3">
                    <div class="ta-center" style="margin-top:20px;">
                          <img src="{$webroot_resources}/images/specialist-avatar-new.png" class="specialist-avatar" /><br />
                          <input type="file" name="file" id="file" onchange="ImagePreview();" class="inputfile"   /> 
                          <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Upload Photo</label>  
                          <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule"><i class="fa fa-calendar"></i> Days of Schedule</button>
                    </div>
                 </div>
                 <div class="col-md-9 col-xs-9">
                    <div class="row">
                       <div class="col-md-4 col-xs-4 col-xs-offset-8">
                             <div class="form-group">
                                <label>Branche</label>
                                    <select name="txtBranch" id="txtBranch" class="form-control m-b">
                                        {foreach from=$branches item=item}
                                        	<option value="{$item.id}">{$item.branch_name}</option>
                                        {/foreach}
                                    </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>First Name</label>
                                <input type="text" id="txtFirstName" name="txtFirstName" class="form-control" placeholder="First name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" id="txtMiddleName" name="txtMiddleName" class="form-control" placeholder="Middle name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" id="txtLastName" name="txtLastName" class="form-control" placeholder="Last name"> 
                              </div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Gender</label>
                                    <select name="txtGender" id="txtGender" class="form-control m-b">
                                        <option value="MALE">MALE</option>
                                        <option value="FEMALE">FEMALE</option>
                                    </select>
                              </div>
                        </div>
                        <!-- <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Status</label>
                                    <select name="branch" id="assignbranch" class="form-control m-b">
                                        <option>Single</option>
                                        <option>Married</option>
                                    </select>
                              </div>
                        </div> -->
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                  <label>Birth Date: <span class="req">*</span></label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right datepicker" id="txtBirthDate" name="txtBirthDate">
                                      <!-- <input type="text" class="form-control pull-right datepicker" id="txtDateBirth" name="txtDateBirth"> -->
                                  </div>
                              </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                          <div class="form-group">
                                <label>Address</label>
                                <textarea rows="2" cols="50" id="txtAddress" name="txtAddress"  class="form-control"></textarea>
                           </div>
                      </div>
                    </div>


                     <div class="row">
                           <div class="col-md-4 col-xs-4">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" id="txtCity" name="txtCity" class="form-control" placeholder="City">
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                  <div class="form-group">
                                    <label>Country</label>
                                        <select name="txtCountry" id="txtCountry" class="form-control m-b">
                                            {foreach from=$countries item=item}
                                        		<option value="{$item.name}">{$item.name}</option>
                                        	{/foreach}
                                        </select>
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                 <div class="form-group">
                                    <label>State</label>
                                        <input type="text" id="txtState" name="txtState" class="form-control" placeholder="City">
                                  </div>
                            </div>
                    </div>

                    <div class="row">
                          <div class="col-md-6 col-xs-6">
                                 <div class="form-group">
                                      <label>Start Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtStartDate" name="txtStartDate">
                                      </div>
                                 </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                  <div class="form-group">
                                      <label>End Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtEndDate" name="txtEndDate">
                                      </div>
                                  </div>
                            </div>
                    </div>

                 </div>
             </form>
	  </div>
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
                   <button class="btn btn-primary" id="btnSaveSpecialist" name="btnSaveSpecialist">Save Information</button>
                   <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>



  </div>
</div>

<!-- Modal -->
<div id="setschedule" class="modal fade" role="dialog">
  <div class="modal-dialog">



            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Schedule</h4>
                <p>Create Schedule for new Specialist</p>
            </div>




            <!-- Modal content-->
            <div class="modal-content">
                     <div class="row">
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Monday" class="flat-red"> Monday</label>
                          </div>
                      
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Tuesday"  class="flat-red"> Tuesday</label>
                          </div>
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Wednesday"  class="flat-red"> Wednesday</label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Thursday"  class="flat-red"> Thursday</label>
                          </div>
                      
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Friday" class="flat-red"> Friday</label>
                          </div>
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Saturday" class="flat-red"> Saturday</label> 
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Sunday" class="flat-red"> Sunday</label> 
                          </div>
                      </div>


                <div class="row" style="margin-top:20px;">
                      <div class="col-md-6 col-xs-6">
                         <div class="bootstrap-timepicker">
                            <div class="form-group">
                                  <label>Start Time:</label>
                                  <div class="input-group">
                                    <input type="text" id="txtSourceStartTime" name="txtSourceStartTime" class="form-control timepicker">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                  </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-xs-6">
                          <div class="bootstrap-timepicker">
                              <div class="form-group">
                                    <label>End Time:</label>
                                    <div class="input-group">
                                      <input type="text" id="txtSourceEndTime" name="txtSourceEndTime" class="form-control timepicker">
                                      <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    </div>
                              </div>
                          </div>
                      </div>
                </div>

          </div>


            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnSetSchedule" id="btnSetSchedule">Set Schedule</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</div>