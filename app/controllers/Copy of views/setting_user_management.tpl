<div id="content">
<h3>Users</h3>
<div class="innerLR">               
        <div class="widget">
        <!-- Widget heading -->
        <div class="widget-head">
            <a href="{$webroot}/setting/user_management">
                <h4 class="heading">User Lists</h4>
            </a>
            <a href="{$webroot}/setting/user_registration">
                <h4 class="floatright">+ Register User </h4>
            </a>
        </div>        
        <!-- // Widget heading END -->
        <div class="widget widget-tabs widget-tabs-double">
            <div class="widget-body">
                <div class="tab-content">    
                    <!-- Step 1 -->
                    <div class="tab-pane active" id="tabUserList">
                        <h3>User Lists</h3>
                        <table id="usersTable"  class="dynamicTable table table-striped table-bordered table-condensed">
                            <!-- Table heading -->
                            <thead>
                                <tr>
                                    <th width="15%">First Name</th>
                                    <th width="15%">Last Name</th>
                                    <th width="10%">E-mail Address</th>
                                    <th width="10%">Mobile Number</th>
                                    <th width="10%">Address</th>
                                    <th width="10%">Username</th>
                                    <th width="10%">User Type</th>
                                    <th width="10%">Status</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            <!-- // Table heading END -->    
                            <!-- Table body -->
                            <tbody>          
                                {foreach from=$users key=key item=item}
                                   
                                    <tr id="tr.{$key}">
                                        <td>{$item.fname}</td>
                                        <td>{$item.lname}</td>
                                        <td>{$item.email_address}</td>
                                        <td>{$item.mobile_number}</td>
                                        <td>{$item.address}</td>
                                        <td>{$item.username}</td>
                                        <td>{$item.user_type_desc}</td>
                                         {if $item.status==1}
                                         <td>Active</td>
                                         {else}
                                         <td>Inactive</td>
                                         {/if}
                                        <td>
                                        <input type="image" src="{$webroot}/images/edit.png" title="edit" class="editUserInformation"  onclick="showUserDialog('{$item.id}','{$item.lname}','{$item.fname}','{$item.email_address}','{$item.mobile_number}','{$item.address}','{$item.user_type}','{$item.user_type_desc}','{$item.status}', false);"/>
                                        <input type="image" src="{$webroot}/images/refresh.png" title="reset" onclick="ResetPassword('{$item.id}','{$item.email_address}','{$item.username}', false);"/>
                                        </td>
                                    </tr>
                                {/foreach}                   
                            </tbody>
                            <!-- // Table body END -->
                        </table>    
                    </div>      


     
            
                    <div class="tab-pane" id="userDialog" title="User Detail Information" style="display:none">
                    <div class="widget widget-tabs widget-tabs-double">
                        <!-- Widget heading -->
                        <div class="widget-head">
                          <ul>
                        <li class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Edit User</a></li>
                            </ul>
                        </div>
                        <!-- // Widget heading END -->
                            <div class="widget-body">
                                <div class="tab-content">      
                                    <form id="frmUserEditDetails" name="frmUserEditDetails">
                                         <fieldset style="margin:10px 0">              
                                         <legend>User Details</legend>
                                                <table cellpadding="5" style="float:left;width:500px;">
                                                    <tbody>
                                                      <tr>
                                                        <td class="discount-label">Last Name</td>
                                                        <td><strong>
                                                          <input id="last_name" name="last_name" size="32" type="text" maxlength="20">
                                                          *</strong></td>
                                                      </tr>
                                                      <tr>
                                                        <td class="discount-label">First Name</td>
                                                        <td><strong>
                                                          <input id="first_name" name="first_name" size="32" type="text" maxlength="20">
                                                        *</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="discount-label">Email Address</td>
                                                        <td><strong>
                                                          <input  id="email_address" name="email_address" size="32" type="text">
                                                        *</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="discount-label">Mobile Number</td>
                                                        <td><strong>
                                                          <input  id="mobile_number" name="mobile_number" size="32" type="text">
                                                        *</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="discount-label">Address</td>
                                                        <td><strong>
                                                          <input  id="address" name="address" size="32" type="text">
                                                        *</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="discount-label">User Type</td>
                                                        <td><strong>
                                                          {html_options id="user_type" name="user_type" options="$user_types" selected=3}
                                                        *</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="discount-label"></td>
                                                        <td><strong>
                                                          {html_options id="agent" name="agent" options="$agents" selected=3}
                                                        </strong></td>
                                                    </tr>
                                                     <tr><td>Status:</td><td>
                                                    <label>       
                                                        <select name="userStatus" id="userStatus">
                                                            {if $status==1}
                                                                <option value="1">Active</option>
                                                                <option value="0">Inactive</option>
                                                            {else}
                                                                <option value="0">Inactive</option>
                                                                <option value="1">Active</option>
                                                            {/if}
                                                        </select>    
                                                    
                                                    <tr>
                                                      <td class="discount-label">&nbsp;</td>
                                                      <td> 
                                                        <input id="btnSave" type="button" class="btn btn-success" value="Save" name="submit" align="center" onclick='updateUserInformation()' />
                                                        <input class="inputbox" id="optiontext" name="optiontext" size="400" type="hidden" />
                                                      </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <input type="hidden" id="user_id" name="user_id"/>
                                            </fieldset>
                                    </form>  
                                </div>     
                            </div>
                        </div>    
                    </div>    

                    <div class="tab-pane" id="dlgUserStatus" title="User Status" style="display:none">
                    <h4>Change status</h4>        
                    <form id="frmUserStatus" name="frmUserStatus">

                     <tr><td>Status:</td><td>
                                    <label>       
                                        <select name="transaction_status_approval" id="transaction_status_approval" class="tran_status_app">
                                            {if $status==1}
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            {else}
                                                <option value="0">Inactive</option>
                                                <option value="1">Active</option>
                                            {/if}
                                        </select>    
                                        <input id="btnUpdateUserStatus" value="Update" type="submit" class="btn btn-success" /> 


                    </form>  
                    </div> 

                </div>
            </div>
        </div>
    </div>
    <!-- // Widget END -->
    </div>  <!-- // Content END -->
</div>