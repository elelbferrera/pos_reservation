<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/deals.js"></script>

<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/deals.png" alt="" style="height: 30px;width: 30px;" />List of Deals</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             Deals</h4>
             
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterDeal(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New Deal </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>   
                	<th width="10%">Deal Code</th>    
                    <th width="10%">Start Date</th>
                    <th width="10%">End Date</th>
                    <th width="10%">Points Required</th>
                    <th width="10%">Create Date</th>
                    <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                
                {foreach from=$deals item=item}
                    <tr>
                    	<td>{$item.deal_code}</td> 
                        
                        <td>{$item.start_date}</td> 
                        <td>{$item.end_date}</td> 
                        <td>{$item.required_points|string_format:"%d"} pt(s)</td> 
                        <td>{$item.create_date}</td>
                        <td>
                            <a onclick="EditDeal('{$item.id}','{$item.merchant_id}','{$item.deal_code}','{$item.is_unlimited}'
                            ,'{$item.number_of_use}','{$item.start_date}','{$item.end_date}','{$item.description}','{$item.required_points}','{$item.required_amount}','{$item.allow_gift}','{$item.allow_other_payment}')">Edit</a>
                             <a href="#" onclick="DeleteDeal('{$item.id}')">Delete</a>
                        </td>
                    </tr>
                {/foreach}    
        
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgDeal" title="New Deal" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabDeal1" class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Deal Information</a></li>
           <!--     <li id="tabAgent2"><a href="#tab2" class="glyphicons adress_book" data-toggle="tab"><i></i>DBA Address</a></li>
                <li id="tabAgent3"><a href="#tab3" class="glyphicons home" data-toggle="tab"><i></i>Mailing Address</a></li>
               -->
              </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           
        <form id="frmRegisterDeal" name="frmRegisterDeal">
            <input type="hidden"  id = "txtMerchantId" name = "txtMerchantId"  value="-1"/>  
            <input type="hidden"  id = "txtDealId" name = "txtDealId"/>
            
            <div id ="divDealtep1"> 
                <table width="100%">
                <tbody>
                	<tr id="trDealCode" name="trDealCode">
                        <td class="tbl_label">Deal Code:</td>
                        <td>
                            <input type="text" readonly="readonly" name="txtDealCode" id="txtDealCode" size="29" maxlength="20" />
                         </td>
                    </tr>
                   <tr>
                        <td class="tbl_label">Is Unlimited:</td>
                        <td>
                            <label>
                        		<input type="checkbox" name="chkIsUnlimited" id="chkIsUnlimited" checked/>
                        		Create as Unlimited
                        	</label>
                    			     
                         </td>
                    </tr>
                   <tr id="trNumberOfUse" name="trNumberOfUse" style="display: none">
                        <td class="tbl_label">Limit Number of Use:</td>
                        <td>
                            <input type="text" name="txtNumberOfUse" id="txtNumberOfUse" onkeypress="return isNumberKey(event)" size="29" maxlength="60"  checked/>
                         </td>
                    </tr>  
                    <tr>
                        <td class="tbl_label">Start Date:</td>
                        <td>
                            <input type="text" name="txtStartDate" id="txtStartDate" size="29" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">End Date:</td>
                        <td>
                            <input type="text" name="txtEndDate" id="txtEndDate" size="29" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Description:</td>
                        <td>
                            <textarea name="txtDescription" id="txtDescription" size="40" maxlength="300"> </textarea> 
                         </td>
                    </tr>
                    
                    <tr>
                        <td class="tbl_label">Required Points:</td>
                        <td>
                            <input type="text" name="txtRequiredPoints" id="txtRequiredPoints" size="20" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Required Amount:</td>
                        <td>
                            <input type="text" name="txtRequiredAmount" id="txtRequiredAmount" size="20" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                    	                        	
                    	<td><label>
                        	<input type="checkbox" name="chkEnableRedeemByGift" id="chkEnableRedeemByGift" />
                        	Enable Redeem by Gift
                        	</label>
                        </td>
                    	<td>
                    		<label>
                    		<input type="checkbox" name="chkEnableRedeemByOther" id="chkEnableRedeemByOther" />
                        	Enable Redeem by Other Payment
                        	</label>
                        </td>
                    		
                    </tr>
                    
                </tbody>
            </table>
            </div>

            
            </div>
            

            <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnRegisterDeal" id="btnRegisterDeal" value="Register Deal" class="btn btn-info"/>
                <input type="button"  name="btnUpdateDeal" style="display:none" id="btnUpdateDeal" value="Update Deal" class="btn btn-info"/>
                <input type="button"  name="btnCancelDeal" id="btnCancelDeal" value="Cancel" class="btn btn-danger btn-small"/>
            </div>

          
        </form> 
        </div>   
        </div> 
        </div>
        </div>

        {$_messages_}

    </div>
</div>
</div>


        
