<script type="text/javascript" src="{$webroot_resources}/js/application.js">
</script>

	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Users
                    <!-- <a href="{$webroot}/partner/add"> -->
                    	<!-- <button class="btn btn-primary" type="button">Add New Partner</button> -->
                    <!-- </a> -->
					<button class="btn btn-primary" type="button" onclick="RegisterUser()">Add New User</button>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Users</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">User List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="applicationlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="10%">ID</th>
							                    <th width="10%">First Name</th>
							                    <th width="10%">Last Name</th>
							                    <th width="15%">User Type</th>
							                    <th width="10%">UserName</th>
							                    <th width="15%">Email</th>
							                    <!-- <th width="5%">Status</th>
							                    <th width="10%">Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	{foreach from=$users item=item}
								             <tr>    
								                <td>{$item.id}</td> 
								                <td>{$item.fname}</td> 
								                <td>{$item.lname}</td> 
								                <td>{$item.user_type_desc}</td>
								                <td>{$item.username}</td>
								                <td>{$item.email_address}</td>
								                <!-- <td>{$item.status}</td> -->
								                 
								            </tr>
								            {/foreach}  
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    
      <!-- Main Footer -->
  <footer class="main-footer">
	<div class="container">
    	<div class="row">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                <!--Anything you want-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 <a href="./">GOETU</a>.</strong> All rights reserved.
        </div>
    </div>
</footer>

<div class="modal fade custom-modal" tabindex="-1" role="dialog" id="dlgUser" name="dlgUser">
	<form name="frmNewUser" id="frmNewUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            	<input type="hidden" name="txtOrderIdStatus" id="txtOrderIdStatus">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                    	<label for="txtFirstName">First Name: *</label>
						<input type="text" class="form-control"  name="txtFirstName" id="txtFirstName" />
                    </div>
                    <div class="form-group">
                    	<label for="txtLastName">Last Name: *</label>
						<input type="text" class="form-control" name="txtLastName" id="txtLastName" />
                    </div>
                    <div class="form-group">
                    	<label for="txtEmail">Email:*</label>
						<input type="text" class="form-control" name="txtEmail" id="txtEmail" />
                    </div>
                    <!-- <div class="form-group">
                    	<label for="txtMobile">Mobile:</label>
						<input type="text" class="form-control" name="txtMobile" id="txtMobile" />
                    </div> -->
                    <div class="form-group">
                    	<label for="txtUsername">Username: *</label>
						<input type="text" class="form-control" name="txtUsername" id="txtUsername" />
                    </div>
                    <div class="form-group">
                    	<label for="txtPassword">Password: *</label>
						<input type="text" class="form-control" name="txtPassword" id="txtPassword" />
                    </div>
                    
                    <select style="display: none" class="form-control select2" style="width: 100%;" id="user_type" name="user_type">
			                 
			                  	<option value="1" selected="selected">Super Admin</option>
			                 
			        </select>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit"  id="btnSaveUser" name-"btnSaveUser" class="btn btn-success">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
     </form>
 </div><!-- /.modal -->
