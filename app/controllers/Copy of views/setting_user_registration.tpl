<div id="content">
<h3>Users</h3>
<div class="innerLR">               
        <div class="widget">
        <!-- Widget heading -->
        <div class="widget-head">
            <a href="{$webroot}/setting/user_management">
                <h4 class="floatright">< Go back to User Lists</h4>
            </a>
        </div>        
        <!-- // Widget heading END -->
        <div class="widget widget-tabs widget-tabs-double">
            <!-- Widget heading -->
            <!--<div class="widget-head">
                <ul>
                <li class="active"><a href="#tab1-4" class="glyphicons building" data-toggle="tab"><i></i> Employers List</a></li>
                    <li><a href="#tab2-4" class="glyphicons wallet" data-toggle="tab"><i></i>Registration</a></li>
                </ul>
            </div>-->
            <!-- // Widget heading END -->
            <div class="widget-body">
                <div class="tab-content">    
 <!-- Step 1 -->
                               
<div class="tab-pane active" id="tab2-4">
<h3>User Registation</h3>
<form id="frmRegisterUser" method="post">
     <fieldset style="margin:10px 0">
            <legend>User Details</legend>
        
            <table cellpadding="5" style="float:left;width:500px;">
                <tbody>
                  <tr>
                    <td class="discount-label">First Name</td>
                    <td><strong>
                      <input id="first_name" name="first_name" size="32" type="text" maxlength="20" autofocus/>
                    *</strong></td>
                  </tr>
                  <tr>
                    <td class="discount-label">Last Name</td>
                    <td><strong>
                      <input  id="last_name" name="last_name" size="32" type="text" maxlength="50" />
                    *</strong></td>
                </tr>
                <tr>
                    <td class="discount-label">Email Address</td>
                    <td><strong>
                      <input id="email_address" name="email_address" size="32" type="text"  maxlength="50" />
                      *</strong></td>
                </tr>
                <tr>
                    <td class="discount-label">
                        Confirm Email
                    </td>
                    <td><strong>
                      <input id="confirm_email" name="confirm_email" size="32" type="text"  maxlength="50" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" autocomplete=off />
                    *</strong></td>
                </tr>
                <tr>
                    <td class="discount-label">Mobile Number</td>
                    <td><strong>
                      <input id="mobile_number" name="mobile_number" size="32" type="text"  maxlength="50" />
                    *</strong></td>
                </tr>
                <tr>
                    <td class="discount-label">Address</td>
                    <td><strong>
                      <input  id="address" name="address" size="32" type="text"  maxlength="50"/>
                    *</strong></td>
                </tr>
                <tr>
                    <td class="discount-label">Username</td>
                    <td>
                        <strong>
                            <input id="username" name="username" size="32" type="text"  maxlength="20"//>
                        *</strong></td>
                </tr>
                <tr>
                  <td class="discount-label">User Type</td>
                  <td><strong>
                    {html_options id="user_type" name="user_type" options="$user_types" selected=6}
                  </strong></td>
                </tr>
                <tr>
                  <td style="display: none;"  name='agent_label' id='agent_label'>Agent</td>
                  <td><strong>
                    {html_options style="display: none;" id="agent" name="agent" options="$agents"}
                  </strong></td>
                </tr>
                <tr>
                  <td class="discount-label">&nbsp;</td>
                  <td>  
                    <input id="btnRegisterUser" value="Save" type="submit"  class="btn btn-success" />
                  </td>
                </tr>
                </tbody>
            </table>
        </fieldset> 
        </form>      
        </div>
            <!-- // Step 3 END -->
            <!-- Step 4 -->       
            <!-- // Step 4 END -->
            <!-- // Step 5  -->
            <!-- Wizard pagination controls -->
            <!--    <div class="pagination margin-bottom-none">
                    <ul>
                        <li class="primary previous first"><a href="javascript:;">First</a></li>
                        <li class="primary previous"><a href="javascript:;">Previous</a></li>
                        <li class="last primary"><a href="javascript:;">Last</a></li>
                          <li class="next primary"><a href="javascript:;">Next</a></li>
                        <li class="next finish primary" style="display:none;"><a href="javascript:;">Finish</a></li>
                    </ul>
                </div>-->
                <!-- // Wizard pagination controls END -->    
            </div>
        </div>
    </div>
    </div>
    <!-- // Widget END -->
</div>  <!-- // Content END -->
</div>