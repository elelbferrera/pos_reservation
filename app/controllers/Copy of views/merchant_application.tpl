<script type="text/javascript" src="{$webroot_resources}/js/application.js">
</script>

	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Applications
                    <a href="{$webroot}/partner/add">
                    	<!-- <button class="btn btn-primary" type="button">Add New Partner</button> -->
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <!-- <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li> -->
                        <li class="{$webroot}/merchant/management">Merchant</li>
                        <li class="active">Applications</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Application List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="applicationlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="no-sort"></th>
                                                <th>Date</th>
                                                <th>Application ID.</th>
                                                <th>Product</th>
                                                <th>Agent Name</th>
                                                <th>Legal Name</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	{foreach from=$applications item=item}
                                        	
                                            <!-- <tr> -->
                                            	<!-- <td class="icon-td">
                                            		<a href="#" data-toggle="modal" data-target="#comment-modal"><i class="fa fa-comment"></i></a></td>
                                                <td>10/02/2012</td>
                                                <td>001</td>
                                                <td>Gift &amp; Rewards</td>
                                                <td>908 Hibachi Inc.</td>
                                                <td>Pending</td> -->
											<!-- </tr> -->
											<tr>
												<td class="icon-td">
													<a href="#" data-toggle="modal" data-target="#comment-modal{$item.order_id}"><i class="fa fa-comment"></i></a>
												</td>
												<td>{$item.create_date|date_format:"%Y-%m-%d"}</td>
												<td>{$item.order_id}</td>
												<td>{$item.name}</td>
												<td><a href="{$webroot}/agent/edit/{$item.parent_id}">{$item.first_name} {$item.last_name}</a></td>
												<td><a href="{$webroot}/merchant/profile/{$item.partner_id}">{$item.company_name}</a></td>
												<td>{$item.status}
													{if $is_admin}
													<a href="#" onclick="showModalStatus('{$item.order_id}','{$item.status}')">
														<i class="fa fa-edit"></i>
													</a>
													<!-- <input type="button" name="btnChange"  class="btn btn-primary" value="Change Status" id="btnChange"/> -->
													{/if}
												</td>
											</tr>
											{/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    
      <!-- Main Footer -->
  <footer class="main-footer">
	<div class="container">
    	<div class="row">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                <!--Anything you want-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 <a href="./">GOETU</a>.</strong> All rights reserved.
        </div>
    </div>
</footer>
 
{foreach from=$applications item=item} 
<div class="modal fade custom-modal modal-comment" tabindex="-1" role="dialog" id="comment-modal{$item.order_id}">

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Comments - {$item.name} | {$item.company_name}  </h4>
                </div>
                <div class="modal-body">
                	<form id="frmComment{$item.order_id}" name="frmComment{$item.order_id}" action="post" enctype="multipart/form-data">
                		<input type="hidden" name="txtProductId" id="txtProductId" value="{$item.product_id}"/>
                		<input type="hidden" name="txtOrderId" id="txtOrderId" value="{$item.order_id}"/>
                		<input type="hidden" name="txtPartnerId" id="txtPartnerId" value="{$item.partner_id}"/>
                		<input type="hidden" name="txtParentId" id="txtParentId" value="-1"/>
                		
	                	<div id="post-comment">
	                        <div class="form-group">
	                        	<input type="radio" class="inputradio" name="comment-type" id="comment-public{$item.order_id}" value="Public" checked> <label for="comment-public{$item.order_id}">Public Response</label>
								<input type="radio" class="inputradio" name="comment-type" id="comment-agent{$item.order_id}" value="Agent"> <label for="comment-agent{$item.order_id}">Agent Note</label>
	                            
	                            {if $is_admin}
	                            <input type="radio" class="inputradio" name="comment-type" id="comment-internal{$item.order_id}" value="Internal"> <label for="comment-internal{$item.order_id}">Internal Note</label>
	                            {/if}
	                            <div class="custom-fl-right comment-view">
	                            	<a href="#" class="cv-showall" onclick="showAllReplies()" title="Show All"><i class="fa fa-navicon"></i></a>
	                                <a href="#" class="cv-showless" onclick="hideAllReplies()" title="Show Less"><i class="fa fa-minus"></i></a>
	                            </div>
	                            <textarea name="txtComment" name="txtComment" class="form-control custom-textarea" placeholder="Type here..." rows="2"></textarea>
	                            <input type="file" name="file{$item.order_id}" id="file{$item.order_id}" class="inputfile" data-multiple-caption="files selected" multiple />
	                            <label for="file{$item.order_id}"><i class="fa fa-upload"></i> <span>Attach file</span></label>
	                            <!-- <div class="custom-fl-right">
	                                Update Status: 
	                                <select class="form-control">
	                                	<option>DO NOTHING</option>
	                                    <option>OPTION 1</option>
	                                    <option>OPTION 2</option>
	                                </select>
	                            </div> -->
	                        </div>
	                        <div class="form-group ta-right">
	                            <button type="submit" class="btn btn-primary">Post</button>
	                        </div>
	                    </div>
                   </form>
                   {foreach from=$item.comments item=comment} 
                    <div id="comment-list">
                    	
                    	<div class="comment" id="comment{$comment.comment_id}">
                        	<div class="comment-block comment-main">
                            	<span class="comment-author">{$comment.first_name} {$comment.last_name}</span> | 
                            	<span class="comment-date">{$comment.create_date}</span>
                                <div class="comment-desc">
                                	{$comment.comment}
                                </div>
                            </div>
                            
                   			  
                   			{foreach from=$comment.sub_comments item=sub} 			
                            <div class="comment-block comment-reply">
                            	<span class="comment-author">{$sub.first_name} {$sub.last_name}</span> | 
                            	<span class="comment-date">{$sub.create_date}</span>
                                <div class="comment-desc">
                                	{$sub.comment}
                                </div>
                            </div>
                            {/foreach}
                            
                            <!-- <div class="comment-block comment-reply">
                            	<span class="comment-author">Child John Doe</span> | <span class="comment-date">October 6, 2016 10:55 AM</span>
                                <div class="comment-desc">
                                	test 2
                                </div>
                            </div> -->

                        	
                        	
                            <form name="frmSubComment{$comment.comment_id}" id="frmSubComment{$comment.comment_id}">
	                            <div class="comment-post-reply" id="divCommentPostReply{$comment.comment_id}" name="divCommentPostReply{$comment.comment_id}">
	                                <div class="form-group">
	                                	<input type="hidden" name="txtProductId" id="txtProductId" value="{$item.product_id}"/>
				                		<input type="hidden" name="txtOrderId" id="txtOrderId" value="{$item.order_id}"/>
				                		<input type="hidden" name="txtPartnerId" id="txtPartnerId" value="{$item.partner_id}"/>
	                                	<input type="hidden" name="txtParentId" id="txtParentId" value="{$comment.comment_id}"/>
 	                                    <textarea name="txtComment" id="txtComment" class="form-control custom-textarea" placeholder="Type here..." rows="2"></textarea>
	                                    <input type="file" name="file{$comment.comment_id}" id="file{$comment.comment_id}" class="inputfile" data-multiple-caption="files selected" multiple />
	                                    <label for="file{$comment.comment_id}"><i class="fa fa-upload"></i> <span>Attach file</span></label>
	                                    <div class="custom-fl-right">
	                                        <button type="submit" class="btn btn-primary">Post</button>
	                                    </div>
	                                </div>
	                            </div>
                            </form>
                            <div class="comment-options">
                            	<a href="#" id="addreply{$comment.comment_id}" name="addreply{$comment.comment_id}" class="addreply" onClick="addReply('{$comment.comment_id}')"><i class="fa fa-reply"></i> Add Reply</a>
                                <a href="#" id="cancelreply{$comment.comment_id}" name="cancelreply{$comment.comment_id}" class="cancelreply" onClick="cancelReply('{$comment.comment_id}')"><i class="fa fa-times"></i> Cancel Reply</a>
                                {if $comment.sub_comments|@count > 0}
	                                <a href="#" class="showall" name="showall{$comment.comment_id}" id="showall{$comment.comment_id}" onclick="showAllSpecific('{$comment.comment_id}')">
	                                	({$comment.sub_comments|@count})Show All
	                                </a>
                                {/if}
                                <a href="#" class="showless" name="showless{$comment.comment_id}" id="showless{$comment.comment_id}" onclick="hideAllSpecific('{$comment.comment_id}')">Show Less</a>
                            </div>
                            
                        </div> 
                    
                    </div>
                    {/foreach}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

	
</div><!-- /.modal -->

{/foreach}


<div class="modal fade custom-modal" tabindex="-1" role="dialog" id="update_status" name="update_status">
	<form name="frmChangeStatus" id="frmChangeStatus">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            	<input type="hidden" name="txtOrderIdStatus" id="txtOrderIdStatus">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Order Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                    	<label for="compname">Status:</label>
						<select class="form-control select2" style="width: 100%;" id="txtStatus" name="txtStatus">
			                  {foreach from=$statuses item=item}
			                  	<option value="{$item.status}">{$item.status}</option>
			                  {/foreach}
			            </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
     </form>
 </div><!-- /.modal -->
