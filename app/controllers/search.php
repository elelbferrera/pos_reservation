<?php
    class search extends template {
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
        }

    	public function customer() {
    		$actions = array("search_customer", "get_services_by_specialist", "update", "change_reservation_status", "send_sms", "show_suggestion"
            );
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            	$action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
                die();
            }

            $params = array(
                'session_id' => $_SESSION['sessionid'],
            );

            $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant', $params);
            $specialist = $response['respmsg'];
            $this->view->assign('specialist', $specialist);

            $response = lib::getWsResponse(API_URL, 'get_reservation_status_per_merchant', $params);
            $status = $response['respmsg'];
            $this->view->assign('status', $status);
    	}

       private function get_services_by_specialist()
      {
            $params = array(
                'session_id' => $_SESSION['sessionid'],
                'specialist_id' => $_POST['specialist'],
            );

            if ($params['specialist_id'] !== "") {
                $response = lib::getWsResponse(API_URL, 'get_products_per_merchant_per_specialist', $params);    
            } else {
                $response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);    
            }
            
            
            

            /*print_r($response);
            die();*/

              if (!(isset($response['respcode'], $response['respcode']))) {
                    $this->response['success'] = false;
                    $this->response['message'] = 'System error, unable to connect to database';
              } elseif (!($response['respcode'] == '0000')) {
                    $this->response['success'] = false;
                    $this->response['message'] = $response['respmsg'];
              } else {
                    $message ="No services define";
                    

                    $product_ids ="";
                    if(isset($_POST['product_ids']))
                    {
                        $product_ids = $_POST['product_ids'];
                    }

                    // <div class="row">
                    // {foreach from=$services item=item}
                    //   <div class="col-md-6 col-sm-6">
                    //             <label style="margin-right: 5px; font-size: 20px;">
                    //             <input type="checkbox" id="services[]" value="{$item.id}" name="services[]" height="50" class="flat-red"> <span style="margin-left:5px;">{$item.name}</span>  <span style="font-size:20px; color:#3b914d; font-weight:bold;">${$item.price|string_format:"%.2f"}</span></label> 
                              
                    //   </div>
                    // {/foreach}
                     if(count($response['respmsg']> 0))
                     {
                        $message ="";
                     }

                     $ctr =2;

                     $message .="<span class='step_descr section sidebar-title'>Select Services</span>";
                    foreach($response['respmsg'] as $r)
                    {
                        $id = $r['id'];
                        $name = $r['name'];
                        $price = "$".number_format($r['price'], 2);
                        // $price = "";
                        // if($ctr%2==0)
                        // {
                        //  $message .="<div class='row'>";
                        //  $is_for_close = true;
                        // }
                        //  $message .="
                        //  <div class='col-md-6 col-sm-6'>
                       //          <label style='margin-right: 5px; font-size: 15px;'>
                       //          <input type='checkbox' id='services[]' value='{$id}' name='services[]' height='50' class='flat-red'> <span style='margin-left:5px;'>{$name}</span>  <span style='font-size:20px; color:#3b914d; font-weight:bold;'>{$price}</span>
                       //          </label> 
                       //      </div>";


                        // if($ctr%2>0 )
                        // {
                        //  $message .="</div>";
                        // }

                        $check ="";
                        if($product_ids !=="")
                        {
                            if(strpos($product_ids, "{$id},")!== false)
                            {
                                $check ="checked";
                            }
                        }
                        // print_r($product_ids);
                        // die();

                        $message .="<div class='col-md-6 col-sm-6'>
                                <label style='margin-right: 5px; font-size: 14px;'><input type='checkbox' {$check} value='{$id}' id='services[]' name='services[]' class='flat-red'> {$name}</label> <span style='font-size:14px; color:#3b914d; font-weight:bold;'>{$price}</span>
                            </div>";
                        $ctr++;
                    }

                    // print_r($message);
                    // die();

                    // print_r($message);
                    // die();

                    


                    $resp = $response['respmsg'];
                    $this->response = array(
                        'success' => true,
                        'message' => $message,
                    );
              }
      }

    	private function search_customer() {
    		$params = array(
    			'session_id' 	=> $_SESSION['sessionid'],
				'search_value' 	=> $_POST['search_value']
			);

            $params['condition'] = "AND r.status = 'SCHEDULED' ORDER BY reservation_date DESC";

			$response = lib::getWsResponse(API_URL, 'search_customer_by_name_mobile', $params);

            $response_services = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
            $services = $response_services['respmsg'];
            
            if($response['respmsg'] > 0)
            {
                $customer = $response['respmsg'];

                $c_count = 0;
                
                $innerHTML="";
                foreach($customer as $c)
                {
                    $product_name = "";
                    foreach($c['details'] as $key => $details) {
                        if ($key !== 0) {
                            $product_name .= ", ";
                        }
                        $product_name .= $details['product_name'];
                    }

                    $innerHTML .='
                        <tr>
                    <td class="waitlist-table-td-th">'.$c['first_name'] . ' ' . $c['last_name'] .'</td>
                    <td class="waitlist-table-td-th">'.substr($c['mobile'],0,3) . '-' . substr($c['mobile'],3,3) . '-' . substr($c['mobile'],6,10).'</td>
                    <td class="waitlist-table-td-th">'.$c['reservation_date'].'</td>
                    <td class="waitlist-table-td-th">'.$c['reservation_time'].'</td>
                    <td class="waitlist-table-td-th">'.$product_name.'</td>
                    <td class="waitlist-table-td-th">'.$c['status'].'</td>
                    <td class="ping waitlist-table-td-th">
                        <button id="editBtn" class="btn btn-default" type="button" data-first="'.$c['first_name'].'" data-last="'.$c['last_name'].'" data-mobile="'.$c['mobile'].'" data-date="'.$c['reservation_date'].'" data-time="'.$c['reservation_time'].'" data-status="'.$c['status'].'" data-specialist="'.$c['specialist_first_name'].'" data-note="'.$c['notes'].'" data-sale-person-id="'.$c['sale_person_ids'].'" data-branch-id="'.$c['branch_id'].'" data-reservation-status-id="'.$c['reservation_status_id'].'" data-product-id="'.$c['product_ids'].'" data-product-name="'.$product_name.'" data-email-address="'.$c['email_address'].'" data-reservation-id="'.$c['id'].'" data-merchant-id="'.$c['merchant_id'].'">Edit</button>
                    </td>
                  </tr>';

                }
            } else{
                $customer = array();
            }

            //var_dump($innerHTML);die;
            return $this->response = array('customer' => $innerHTML, 'success' => true);
    	}

        private function update()
      {
        
        // session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
        $params = array(
            'session_id' => $_SESSION['sessionid'],
            'id' => $_POST['txtReservationId'],
            'reservation_status_id' => $_POST['txtStatus'],
            'reservation_date' => $_POST['txtDate'],
            'reservation_time' => $_POST['txtTime'],
            'branch_id' => $_POST['txtBranch'],
            'last_name' => $_POST['txtLastName'],
            'first_name' => $_POST['txtFirstName'],
            'notes' => $_POST['txtNotes'],
            'mobile' => $_POST['txtMobile'],
            'product_id' => $_POST['txtProducts'],
            'sale_person_id' => $_POST['txtSpecialist'],
         );

        $response = lib::getWsResponse(API_URL, 'update_reservation', $params);
                               
          if (!(isset($response['respcode'], $response['respcode']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
                
          } elseif (!($response['respcode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['respmsg'];
          } else {
                $this->response = array(
                    'success' => true,
                    'message' => $response['respmsg'],
                );
          }
      }

      private function change_reservation_status()
        {
            $params = array(
                'session_id' => $_SESSION['sessionid'],
                'reservation_id' => $_POST['reservation_id'],
                'status' => $_POST['status'],
                'update_date' => date('Y-m-d'),
            );

            // print_r($params);
            // die();
            
            $response = lib::getWsResponse(API_URL, 'change_reservation_status', $params);
                               
              if (!(isset($response['respcode'], $response['respcode']))) {
                    $this->response['success'] = false;
                    $this->response['message'] = 'System error, unable to connect to database';
              } elseif (!($response['respcode'] == '0000')) {
                    $this->response['success'] = false;
                    $this->response['message'] = $response['respmsg'];
              } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['respmsg'],
                    );
              }
        }

        private function send_sms() {
            // session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
            $params = array(
                'session_id' => $_SESSION['sessionid'],
                // 'status' => $_POST['txtStatus'],
                'last_name' => $_POST['txtSourceLastName'],
                'first_name' => $_POST['txtSourceFirstName'],
                'contact_number' => $_POST['txtSourceMobile'],
                'create_date' => date('Y-m-d'),
                'message' => $_POST['txtMessage'],
             );



            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'send_sms_to_customer', $params);
                                   
              if (!(isset($response['respcode'], $response['respcode']))) {
                    $this->response['success'] = false;
                    $this->response['message'] = 'System error, unable to connect to database';
              } elseif (!($response['respcode'] == '0000')) {
                    $this->response['success'] = false;
                    $this->response['message'] = $response['respmsg'];
              } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['respmsg'],
                    );
              }
        }

        //get reservation suggestions
        private function show_suggestion() {
            if($_POST['txtSpecialist'] !== "") {
                $date = explode("-", $_POST['txtDate']);
                $date = $date[2] . "-" . $date[0] . "-" . $date[1];
                
                $post_data = array(
                    'method'            => 'get_reservation_suggestions',
                    'merchant_id'       => $_POST['txtMerchantId'],
                    'product_ids'       => $_POST['txtProducts'],
                    'specialist_ids'    => $_POST['txtSpecialist'],
                    'reservation_date'  => $date/*$_POST['txtDate']*/,
                );

                $response = lib::getWsResponse(API_URL, 'get_reservation_suggestions', $post_data);

                


                    if (!(isset($response['respcode'], $response['respmsg']))) {
                        $this->response['success'] = false;
                        $this->response['message'] = 'System error, unable to connect to database';
                        return;
                    } elseif ($response['respcode'] == '0000') {
                        // $this->layout = "json";


                        $this->response['success'] = true;
                        $this->response['message'] = "Conflict";
                        // return;
                        

                        $suggestions = $response['respmsg'];
                        $s_count = 0;
                        
                        $innerHTML="";
                        foreach($suggestions as $s)
                        {
                            $s_count++;

                            $time = $s['time'];
                            $m_time = $s['m_time'];
                            $service = $s['service'];
                            $service_id = $s['service_id'];
                            $date = $s['date'];
                            $specialist_name = $s['first_name']." ". $s['last_name'];
                            $specialist_id = $s['specialist_id'];

                            $opacity ="";

                            $innerHTML .="
                                    <input type='hidden' name='txtData{$s_count}' id='txtData{$s_count}' value='{$time}|{$date}|{$specialist_id}|{$service_id}'>
                                    <li class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                  <div class='coupon_content suggestion-li'>
                                        <div class='row'>
                                            <div class='col-lg-6 col-sm-6 col-xs-12'>
                                                 <div class='suggestion-title'><strong>Time</strong>: {$time}</div>
                                                 <div class='suggestion-title'><strong>Specialist</strong>: {$specialist_name}</div>
                                                 <div class='suggestion-title'><strong>Services:</strong><br /> </div>
                                                 <p>{$service}</p>
                                            </div>
                                            <div class='col-lg-6 col-sm-6 col-xs-8'>
                                                 <button type='button' class='tabbtn btn btn-primary' id='btnReserveMySuggestion' name='btnReserveMySuggestion' style='width:100%' onClick='ReserveMe({$s_count})'><i class='fa fa-arrow-circle-right'></i> Reserve Now</button>
                                            </div>
                                        </div>
                                  </div>
                                </li>";
                        }

                        $this->response['suggestions'] = $innerHTML;
                        //print_r($response['suggestions']);die;
                        return;
                    }
            }
        }
    }
?>