<?php
class cash_advances extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function _afterAction() {
//        if (isset($_SESSION['sessionid']) && lib::$ws_flag == false) {
//            $response = lib::getWsResponse($this->meta['remote_api'], 'extend_session', array('P01' => $_SESSION['sessionid']));
//            if (!(isset($response['ResponseCode']) && $response['ResponseCode'] == '0000')) $this->check_session();
//        }
        parent::_afterAction();
    }
    
    public function generic($params = NULL, $current_menu = 'pending', $status = 0) {
        if ($params == NULL) {
            $params = array(0 => 50);
        }
        
        $param = array('username' => $_SESSION['username']);
        $this->backend->get_user_details($param);
        $user_info = $this->backend->get_response();
        
        $this->view->assign('usertype', $user_info['ResponseMessage']['user_type']);
        $this->view->assign('status', $status);
        
         
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        
        $this->view->assign('user_type_description', $usertype);   
        $yes_or_no = array(1 => ' Yes', 2 => ' No');
        $this->view->assign('yes_or_no', $yes_or_no);
        
        
        $limits_menu = array(
            50 => 50, 
            100 => 100, 
            500 => 500, 
        );
        $main_menu = array(
            'Pending' => 'pending', 
            //LC-09/11/2013
            'Validated' => 'validated', 
            'Approved' => 'approved', 
            'Declined' => 'declined',
             
        );
        if (isset($limits_menu[$params[0]])) {
            $limit = $limits_menu[$params[0]];
        } else {
            $limit = '';
        }
        $this->view->assign('main_menu', $main_menu);
        $this->view->assign('limits_menu', $limits_menu);
        $this->view->assign('current_menu', $current_menu);
        $this->view->assign('current_limit', $limits_menu[$params[0]]);
//        $view_params = array(
//            'P01' => $_SESSION['sessionid'],
//            'P02' => $status,
//            'P03' => $limit,
//        );
//        $response = lib::getWsResponse($this->meta['remote_api'], 'view_cash_advances', $view_params);
        $view_params = array(
            'status' => $status,
            'limit' => $limit
        );
        
        $result = $this->backend->view_loans($view_params);
        $response = $this->backend->get_response();
        
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']) && $response['ResponseCode'] == '0000' && is_array($response['ResponseMessage']))) {
            if (strtolower(substr($response['ResponseMessage'],0,15)) == 'session expired') {
                unset($_SESSION);
                session_destroy();
                return $this->redirect('log/in');
            }
            $this->view->assign('transactions', array());
        } else {
            $transactions = $response['ResponseMessage'];
            
            $sel_trans= array();
            foreach ($transactions as $record) {
                $paramsLoan= array("loan_id" => $record['id']);
//                $salary_documents =    array();
//                $out_salary_documents = array();
//                if ($this->backend->get_kyc_documents($paramsCust)) {
//                    $responseSal = $this->backend->get_salary_documents($paramsCust);
//                    foreach($salary_documents as $row)
//                    {
//                         
//                    }
//                }
                $comments ="";
                $loan_comments = array();
                $out_loan_comments = array();
                if ($this->backend->get_comments($paramsLoan)) 
                { 
                    $responseLoan = $this->backend->get_response();
                    $loan_comments = $responseLoan['ResponseMessage'];
                    
                    foreach ($loan_comments as $rowC) {
                        $out_loan_comments[] = array(
                            $rowC['comment_date'],
                            $rowC['comment'],
                            $rowC['comment_by'],
                        );
                    }
                    
                }
                
                $paramsCust= array("customer_id" => $record['customer_id']);
                
                $customer_documents = array(); 
                $out_customer_documents = array();
                if ($this->backend->get_kyc_documents($paramsCust)) {
                    $responseCust = $this->backend->get_response();
                    $customer_documents = $responseCust['ResponseMessage'];
                    
                    $kyc_capture_types = $this->backend->get_capture_types();
                    foreach ($customer_documents as $row) {
                    //PP 03/22/2014 added buttons and variables
                    
                  //  $varcustomerid='<input type="hidden" class="customerdetail" id="customerid_'.$row['id'].'" value="'.$row['customer_id'].'" name="customerid_'.$row['id'].'"  />';
                    //$buttonVerify = '<input type="button" class="btnVerifyDoc" value="Verify" id="'.$row['id'].'" />'; 
                    //$buttonRevision = '<input type="button" class="btnRevisionDoc"  value="Revision" id="'.$row['id'].'" />';    
                   //PP 04/07/2014 
                    $varcustomerid='';
                    $buttonVerify = '';
                    $buttonRevision = '';
                    
                    if($row['status']==1)
                    {
                        $buttonVerify='<label>Verified</label>';
                        $buttonRevision='';
                    }
                    if($row['status']==2)
                    {
                        $buttonVerify='<label>For Revision</label>';
                        $buttonRevision='';    
                    }
                    if($row['status']==0)
                    {
                        $buttonVerify='<label>Unprocess</label>';
                        $buttonRevision='';    
                    }
                    
                    $vardoc='<input type="hidden" class="docdetail" id="doctype" value="1" name="doctype"  />';  
       
                        $kycDoc =$row['kyc_document'];
                        $kycDoc = str_replace("'","",$kycDoc);
                        $out_customer_documents[] = array(
                            //$row['kyc_document'],
                            $kycDoc,
                            $row['document_id_no'],
                            $kyc_capture_types[$row['capture_type']],
                            //null,
                            //$this->kyc_view_path(1, $row['kyc_document'] . '-1', $row['path_filename1'], $row['customer_id']) . $this->kyc_view_path(2, $row['kyc_document'].'-2', $row['path_filename2'], $row['customer_id']),
                            $this->kyc_view_path(1, $kycDoc . '-1', $row['path_filename1'], $row['customer_id']) . $this->kyc_view_path(2, $kycDoc.'-2', $row['path_filename2'], $row['customer_id']),
                            //PP 03/22/2014 
                             $buttonVerify." ".$buttonRevision . " ".$varcustomerid . " ".$vardoc,
                        );
                        
                    }
                }
                
                  $permissions=$_SESSION['permissions'];
                  $can_save_comment ="NO";
                  if(array_key_exists("cash_advances/savecomment",$permissions))
                  {
                   $can_save_comment ="YES";
                  }
                  
                  $has_editing ="NO";
                  if(array_key_exists("cash_advances/releasefund",$permissions))
                  {
                   $has_editing ="YES";
                  }
                
                
                $trimmed = array(
                    'loan_no' => sprintf('%06d',$record['id']),
                    'cat_id'            => $record['id'],
                    'cat_requested'     => $record['created_by'],
                    'card_number'      => $record['linked_cardnumber'],
                    'date_processed'    => $record['approved_on'],
                    'merchant_name'     => $record['fname'].' '.$record['lname'],
                    'mobile_number'     => $record['mobile_number'],
                    'amount'            => $record['amount'],
                    'status'            => $record['status'],
                    'remarks'           => $record['comments'],
                    'date_requested' => $record['created_on'],
                    'validated_on' => $record['validated_on'],
                    'validated_by' => $record['validated_by'],
                    'approved_on' => $record['approved_on'],
                    'approved_by' => $record['approved_by'],
                    'customer_id' =>  $record['customer_id'],
                    'loan_disbursed' => $record['loan_disbursed'],
                    'disbursed_by' => $record['approved_by'],
                    'is_document_verified'=> $record['is_document_verified'],
                    'is_salary_verified'=> $record['is_salary_verified'],
                    'salary_verified_by'=> $record['salary_verified_by'],
                    'fund_release_on'=> date('m-d-Y',strtotime($record['fund_release_on'])),
                    //date('Y-m-d', strtotime($_POST['date_paid'])
                    'fund_release_by'=> $record['fund_release_by'],
                    
                    'customer_documents' => $out_customer_documents,
                    'loan_comments' => $out_loan_comments,
                    'fund_release_by' => $record['fund_release_by'],
                    'can_save_comment' => $can_save_comment, 
                    'has_action' => $can_save_comment, 
                    'has_editing' => $has_editing,
                );
                $trimmed['item_details'] = '(' . json_encode($trimmed) . ')';
                $sel_trans[] = $trimmed;
            }
            $this->view->assign('transactions', $sel_trans);
        }
    }
    
    protected function kyc_view_path($page, $doc_type, $path, $cust_id) {
        
        if ($path != '') {
            return '<input type="button" class="kyc_viewer" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>&nbsp;';
        } else {
            return '';
        }
    }
    
    protected function salary_view_path($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            return '<input type="button" class="kyc_viewer" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>&nbsp;';
        } else {
            return '';
        }
    }
    
    
    
    
    protected function view_document_image() {

        $this->layout = false;
        if (!(isset($_GET['token']) && $_GET['token'] != '')) {
            echo 'Invalid request';
        } elseif (!(isset($_GET['token2']) && $_GET['token2'] != '')) {
            echo 'Invalid request';
        } elseif (false === ($actual_file = $this->backend->get_actual_document_pathname(array('token' => $_GET['token'], 'token2' => $_GET['token2'])))) {
            echo 'Unable to retrieve document';
        } elseif (!file_exists($actual_file)) {
            echo $actual_file;
        } else {
            $imageinfo = @getimagesize($actual_file); //check image size
            if (!(in_array($imageinfo['mime'], $this->allowed_types))) {
                echo 'Invalid image';
            } else {
                $fp = fopen($actual_file, 'rb');
                header('Content-Type: '. $imageinfo['mime']);
                header('Content-Length: '. filesize($actual_file));
                fpassthru($fp);
                exit;
            }
        }
    }
    
    
    public function declined($params = NULL) {
        //$this->generic($params, 'declined', 2);
        $this->generic($params, 'declined', -1);
    }
    
    public function processed($params = NULL) {
        //$this->generic($params, 'processed', 1);
        $this->generic($params, 'approved', 2);
    }
    //LC-09/11/2013
    public function validated($params = NULL) {
        $this->generic($params, 'validated', 1);
    }
    
    
    public function savecomment() {
        if (!(isset($_POST['comment']))) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a comment.';
        } elseif(!($_POST['comment']!='')) 
        {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a comment.';
        }
        else{
            
            $this->layout = 'json';   
            $params = array(
                'loan_id' => $_POST['comment_loan_id'],
                'comments' => $_POST['comment'],
                'user' => $_SESSION['username']
            );
            $result = $this->backend->add_comment($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {

                 //PP 04/11/2014 Added for Logs
                        $datetime=$this->backend->get_server_date();
                        $this->backend->save_to_action_logs('Added comment loan#'.+ $params['loan_id'],'Loan Approval',$datetime,$_SESSION['username'],'');
                       
                
                $this->response = array(
                'success' => true,
                'message' => 'Comment has been saved.',
                );
            }
        }
    }
   
   
     //PP 03/18/2014 Added for Customer Gathered Documents
     protected function set_customer_document_status() {
            $params = array(
                'customer_document_id' => $_GET['id'],
                'verified_by' => $_SESSION['username'],
                'status' => $_GET['status'],
                'doctype' => $_GET['doctype'],
            );     
            
            $result = $this->backend->set_customer_document_validation_status($params);
           
            
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                 /* $this->response = array(
                'success' => true,
                'message' =>  $response['ResponseMessage'],
                );     */
                $_GET['id'] = $_GET['merchant_card_id'];
                $this->view_merchant('Document has been updated.');
                
              
               
               
            }
    }
      //PP 03/23/2014  
      public function customerdocuments() {
        $actions = array('set_customer_document_status','send_notification_customer');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
      }
    
    
    //PP 03/23/2014
    protected function send_notification_customer(){
        $params = array(
                'customer_id' => $_GET['id'],
                'verified_by' => $_SESSION['username'],
                'status' => $_GET['status'],
                'doctype' => $_GET['doctype'],
            );
              
                    
                    $contact = array();
                    $customerid=$params['customer_id'];//$this->backend->get_customer_id_by_doc($params['customer_document_id']);
                    
                    $contact = $this->backend->get_customer_contacts($customerid);
                    $email = $contact['email_address'];
                    $mobile = $contact['mobile_number'];  
                    $docs=  $this->backend->get_customerdocfor_revision($customerid);
                    $params = array('email_address' => $email,
                                    'mobile_number' => $mobile,
                                    'docs_revised' => $docs,
                                    'customer_id' => $customerid,
                                    'doctype' => $params['doctype'],);
                    $result = $this->backend->customer_doc_revision_email($params);
                    $response = $this->backend->get_response();
                    if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                    } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                    } else {
                    $this->response['message'] = $response['ResponseMessage']; 

                    }
    }
   
   
    public function releasefund(){
        if (!(isset($_GET['customer_id'],$_GET['cat_id']))) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a customer id and loan id';
        }
        else{                       
            $this->layout = 'json';
            $params= array(
                'user' => $_SESSION['username'],
                'customer_id' =>$_GET['customer_id'],
                'loan_id' =>$_GET['cat_id'],
            );
            
            $result = $this->backend->release_fund($params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']) && $response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
                );
            }
         }   
    }
    
   
   

    public function pending($params = NULL) {
        if (isset($_POST['process'])) {
            $this->layout = 'json';
            if (!(isset($_POST['cat_id'], $_POST['transaction_status']))) {
                $this->response['message'] = 'Please complete the form before proceding transaction.';
            } elseif (!is_numeric($_POST['transaction_status']) && $_POST['transaction_status'] > 0 && $_POST['transaction_status'] < 1) {
                $this->response['message'] = 'Please select either Processed or Declined or Validate option';
            } elseif (!is_numeric($_POST['cat_id']) && $_POST['cat_id'] > 0) {
                $this->response['message'] = 'Invalid transaction';
            } else {
//                $process_params = array(
//                    'P01' => $_SESSION['sessionid'],
//                    'P02' => $_POST['cat_id'],
//                    'P03' => $_POST['remarks'],
//                );
                //$process_method = $_POST['transaction_status'] == 1? 'approve_cash_advance': 'decline_cash_advance';
                //$response = lib::getWsResponse($this->meta['remote_api'], $process_method, $process_params);
                

                
                if($_POST['transaction_status']==1)
                {
                    $params= array(
                        'customer_id' =>  $_POST['customer_id'],
                        'loan_id' => $_POST['cat_id'],
                        'validated_by' => $_SESSION['username'],
                        'validated' => $_POST['transaction_status'],
                        'comments' => '',   
                        //'duedate' => $_POST['duedate'],
                        'is_document_verified' =>  $_POST['is_document_verified'],
                        'is_salary_verified' =>  $_POST['is_salary_verified'],
                        'salary_verified_by' => $_POST['is_salary_verified']==1 ? $_SESSION['username'] : null
                    );
                    $result = $this->backend->validate_loan($params);
                    $response = $this->backend->get_response();
                }
                
                if($_POST['transaction_status']==2)
                {
                    $params= array(
                        'customer_id' =>  $_POST['customer_id']);
                    
                    $result = $this->backend->checkIfHasCardNumber($params);
                    $response = $this->backend->get_response();
                    if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                        $this->response['message'] = 'System error, unable to connect to database';
                    } elseif (!($response['ResponseCode'] == '0000')) {
                        $this->response['message'] = $response['ResponseMessage'];
                    } else {
                        $params= array(
                            'customer_id' =>  $_POST['customer_id'],
                            'loan_id' => $_POST['cat_id'],
                            'approved_by' => $_SESSION['username'],
                            'approved' => $_POST['transaction_status'],
                            'is_document_verified' =>  $_POST['is_document_verified'],
                            'is_salary_verified' =>  $_POST['is_salary_verified'],  
                            'salary_verified_by' => $_POST['is_salary_verified']==1 ? $_SESSION['username'] : null,
                            'comments' => ''
                        );
                        $result = $this->backend->approve_loan($params);
                        $response = $this->backend->get_response();
                    }
                    
                    
                    

                }
                
                if($_POST['transaction_status']<0)
                {
                    if($_POST['current_status']==0)
                    {
                        $params= array(
                            'customer_id' =>  $_POST['customer_id'],
                            'loan_id' => $_POST['cat_id'],
                            'validated_by' => $_SESSION['username'],
                            'validated' => 0,
                            'comments' => '',
                            'is_document_verified' =>  $_POST['is_document_verified'],
                            'salary_verified_by' => $_POST['is_salary_verified']==1 ? $_SESSION['username'] : null,
                            'is_salary_verified' =>  $_POST['is_salary_verified'],
                        );
                        $result = $this->backend->validate_loan($params);
                        $response = $this->backend->get_response();
                    }
                    
                    if($_POST['current_status']==1)
                    {
                        $params= array(
                            'customer_id' =>  $_POST['customer_id']);
                        
                        $result = $this->backend->checkIfHasCardNumber($params);
                        $response = $this->backend->get_response();
                        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                            $this->response['message'] = 'System error, unable to connect to database';
                        } elseif (!($response['ResponseCode'] == '0000')) {
                            $this->response['message'] = $response['ResponseMessage'];
                        } else {
                            $params= array(
                                'customer_id' =>  $_POST['customer_id'],
                                'loan_id' => $_POST['cat_id'],
                                'approved_by' => $_SESSION['username'],
                                'approved' => 0,
                                'comments' => '',       
                                'is_document_verified' =>  $_POST['is_document_verified'],
                                'is_salary_verified' =>  $_POST['is_salary_verified'],
                                'salary_verified_by' => $_POST['is_salary_verified']==1 ? $_SESSION['username'] : null
                            );  
                            $result = $this->backend->approve_loan($params);
                            $response = $this->backend->get_response();
                        }
                    }
                }
                
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']) && $response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                }
            }
        } else {    
            $this->generic($params);
        }
    }
    
	
	public function view_merchant($message = null) {
          
        if (!(isset($_GET['id']))) {
            $this->response['message'] = 'Please select a valid merchant to view';
        } else {
//            $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => substr($_GET['id'],1),
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'view_merchant_details', $api_params);
            
            $search_params = array(
                'customer_id' => $_GET['id'],
            );
            $result = $this->backend->get_customer_info($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $profile = $response['ResponseMessage']['info'];
                $out_profile = array(
                    'card_id' => $profile['id'],
                    'merchant_fname' => $profile['fname'],
                    'merchant_mname' => $profile['mname'],
                    'merchant_lname' => $profile['lname'],
                    'merchant_lname' => $profile['lname'],
                    'national_insurance_no' => $profile['national_insurance_no'],
                    'merchant_mobilenumber' => $profile['mobile_number'],
                    'merchant_address' => $profile['street'] . ' ' . $profile['city'] . ' ' . $profile['country'],
                    'ca_limit' => number_format($profile['credit_limit'],2),
                    'prepaid_card' => $profile['linked_cardnumber'],
                    'card_balance' => number_format($profile['loan_balance'],2),
                    'due_date' => $profile['due_date']
                );
                $transactions = $response['ResponseMessage']['transactions'];
                $ca_history = $response['ResponseMessage']['loans'];
                $payment_history = $response['ResponseMessage']['payments'];
                $credit_limit_history = $response['ResponseMessage']['credit_limit'];
                $customer_documents = $response['ResponseMessage']['customer_documents'];
                $salary_documents = $response['ResponseMessage']['salary_documents'];

                $out_transactions = array();
                foreach ($transactions as $row) {
                    $out_transactions[] = array(
                        $row['transaction_date'],
                        $row['name'],
                        $row['is_debit'] == 1? number_format($row['amount'],2): '-',
                        $row['is_debit'] == 0? number_format($row['amount'],2): '-',
                        number_format($row['loan_balance'],2),
                    );
                }
                $out_ca_history = array();
                foreach ($ca_history as $row) {
                    $out_ca_history[] = array(
                        $row['created_on'],
                        number_format($row['amount'],2),
                    );
                }
                $out_payment_history = array();
                foreach ($payment_history as $row) {
                    $out_payment_history[] = array(
                        $row['date_paid'],
                        $row['date_paid'],
                        number_format($row['amount'],2),
                    );
                }
                
                $out_credit_limit_history = array();
                foreach ($credit_limit_history as $row) {
                    $out_credit_limit_history[] = array(
                        //$row['created_on'],
                        $row['date_created'],
                        number_format($row['credit_limit'],2),
                    );
                }
                 
                $kyc_capture_types = $this->backend->get_capture_types();
                $out_customer_documents = array();
                foreach ($customer_documents as $row) {
                    //PP 03/18/2014 Added button for verification and for revision
                        $buttonVerify = '<input type="button" class="btnVerify" value="Verify" id="'.$row['id'].'" />'; 
                        $buttonRevision = '<input type="button" class="btnRevision" value="Revision" id="'.$row['id'].'" />';    
                    if($row['status']==1)
                    {
                        $buttonVerify='<label>Verified</label>';
                        $buttonRevision='';
                    }
                    if($row['status']==2)
                    {
                        $buttonVerify='<label>For Revision</label>';
                        $buttonRevision='';    
                    }
                    
                    $varcustomerid='<input type="hidden" class="customerdetail" id="customerid_'.$row['id'].'" value="'.$row['customer_id'].'" name="customerid_'.$row['id'].'"  />';
                    $vardoc='<input type="hidden" class="docdetail" id="doctype" value="1" name="doctype"  />';
                    $out_customer_documents[] = array(
                        $row['kyc_document'],
                        $row['document_id_no'],  
                        $kyc_capture_types[$row['capture_type']],
//                        null,
                        $this->kyc_view_path(1, $row['kyc_document'] . '-1', $row['path_filename1'], $row['customer_id']) . $this->kyc_view_path(2, $row['kyc_document'].'-2', $row['path_filename2'], $row['customer_id']),
                        $buttonVerify." ".$buttonRevision . " ".$varcustomerid . " ".$vardoc,
                    );
                }
                
                $out_salary_documents = array();
                $ctr=1;
                foreach ($salary_documents as $row) {
                    //PP 03/24/2014 added buttons and variables
                    $buttonVerify = '<input type="button" class="btnVerifySalary" value="Verify" id="'.$row['id'].'" />'; 
                    $buttonRevision = '<input type="button" class="btnRevisionSalary" value="Revision" id="'.$row['id'].'" />';    
                    $varcustomerid='<input type="hidden" class="customerdetail" id="customerid" value="'.$row['customer_id'].'" name="customerid"  />';
                    $vardoc='<input type="hidden" class="docdetailsalary" id="doctype" value="2" name="doctype"  />';
                    
                    $out_salary_documents[] = array(
                        $this->salary_view_path($ctr, 'salary', $row['path_filename'], $row['customer_id']),
                        $row['date_uploaded'],$buttonVerify." ".$buttonRevision . " ".$varcustomerid . " ".$vardoc,
                    );
                    $ctr++;
                }
                
                $documents = $this->backend->get_document_types();
                $documents = array('' => '-SELECT-') + $documents;

                        
                $this->response = array(
                    'message'       => $message,
                    'success'       => true,
                    'profile'       => $out_profile,
                    'trans'         => $out_transactions,
                    'ca_history'    => $out_ca_history,
                    'payment_history' => $out_payment_history,
                    'credit_limit_history' => $out_credit_limit_history,
                    'customer_documents' => $out_customer_documents,
                    'salary_documents' => $out_salary_documents,
                    'documents' =>$documents,               
                );
                 /*print_r($response);
                 die();  */
            }
        }
    }
    
	
	
	
	
    public function update_loan_status()
    {
        if (!(isset($_POST['customer_id'],$_POST['loan_id']))) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a customer id and loan id';
        }
        else{                       
            $this->layout = 'json';
            $result = $this->backend->check_salary_validation($_POST['customer_id']);
            
            if($result)
            {
                $result = $this->backend->check_kyc_validation($_POST['customer_id']);
                if(count($result)>=5)
                {
                    $params= array(
                        'user' => $_SESSION['username'],
                        'loan_id' =>$_POST['loan_id'],
                        'status' => $_POST['transaction_status_approval'],
                        'customer_id' => $_POST['customer_id'],
                        'reason' => $_POST['remarks'],  
                    );            
                    $result = $this->backend->update_loan_status($params);
                    $response = $this->backend->get_response();
                    
                    if (!(isset($response['ResponseCode'], $response['ResponseMessage']) && $response['ResponseCode'] == '0000')) {
                        $this->response['message'] = $response['ResponseMessage'];
                    } else {
                        $this->response = array(
                            'success' => true,
                            'message' => $response['ResponseMessage'],
                        );
                           //PP 04/11/2014 Added for Logs    
                        $actionlog_msg='';
                        $action_status=$params['status'];
                        if($action_status==1)
                        {$actionlog_msg='Validated loan application';}
                        else if($action_status==2)
                        {$actionlog_msg='Approved loan application';}
                        else if($action_status<0)
                        {$actionlog_msg='Declined loan application';}
                        //PP 04/11/2014 Added for Logs
                        $datetime=$this->backend->get_server_date();
                        $this->backend->save_to_action_logs($actionlog_msg.' Customerid# '.$_POST['customer_id'],'Loan Approval',$datetime,$_SESSION['username'],'');
                         print_r(json_encode($this->response,true));
                         die(); 
                        
                    }        
                }else
                {
                   $this->response = array(
                    'success' => false,
                    'message' => " Loan status could not be change. Customer document has not been uploaded,\n \t\t\t\t\texpired or expiration not been assigned.",
                   );  
                }
                
                
                
            }else{
                $this->response = array(
                    'success' => false,
                    'message' => "Loan status not be change. Salary document has not been uploaded or not been verfied.",
                ); 
                
            } 
        }                                                         
    }   
    
}