<?php
class reports extends template {
    
    public function __construct($meta) {
        parent::__construct($meta);
    }
    
    public function usermanagement() {
        $this->showInfo('This page is currently unavailable.');
    }
    
    
    public function dlwl()
    {
//        $actions = array('search_daily_loan_log');
//        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
//              $action = $_GET['action'];
//            $this->layout = 'json';
//            $this->$action();
//        }       
 
        if (isset($_GET['startDate'],$_GET['endDate'])) 
        {
            $this->exportDailyLoan($_GET['startDate'],$_GET['endDate']);
        }
    }
    
    public function apl()
    {
//        $actions = array('search_application_processing');
//        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
//            $action = $_GET['action'];
//            $this->layout = 'json';
//            $this->$action();
//        }     
        if (isset($_GET['startDate'],$_GET['endDate'])) 
        {
            $this->exportApplicationLoan($_GET['startDate'],$_GET['endDate']);
        }
    }
    
    public function alr()
    { 
        if (isset($_GET['startDate'],$_GET['endDate'])) 
        {
            $this->exportActionLog($_GET['startDate'],$_GET['endDate']);
        }
    }
    
    
    public function exportApplicationLoan($startDate, $endDate)
    {
        $startDate= date("Y-m-d", strtotime($startDate));
        $endDate= date("Y-m-d", strtotime('+1 day',strtotime($endDate)));
        $search_params = array(
            'startdate' => $startDate,
            'enddate' => $endDate,
        );
        
        $result = $this->backend->search_application_processing($search_params);
        $response = $this->backend->get_response();
        
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {   
                    
                $rowPosition=1;
                $countRecord=0;
                $loanAmount=0;
                
                $arTotalMonths=0;
                $arTotalDays=0;
                $arTotalHours=0;
                $arCounter=0;
                
                $vaTotalMonths=0;
                $vaTotalDays=0;
                $vaTotalHours=0;
                $vaCounter=0;
                
                $vvTotalMonths=0;
                $vvTotalDays=0;
                $vvTotalHours=0;
                $vvCounter=0; 
                
                $cvTotalMonths=0;
                $cvTotalDays=0;
                $cvTotalHours=0;
                $cvCounter=0;
                
                require_once "PHPExcel.php"; 
                $excel = new PHPExcel();
                $excel->setActiveSheetIndex(0);
                $excel->getActiveSheet()->setTitle('Daily Loan');
                //set cell A1 content with some text
                $excel->getActiveSheet()->setCellValue('A1', 'Loan Number');
                $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                 
                $excel->getActiveSheet()->setCellValue('B1', 'Borrower First Name');
                $excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('C1', 'Borrower Last Name');
                $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);

                $excel->getActiveSheet()->setCellValue('D1', 'Loan Amount');
                $excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('E1', 'Verification Date Time');
                $excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);

                $excel->getActiveSheet()->setCellValue('F1', 'Verification Username');
                $excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('G1', 'Time between Initial application and Verification');
                $excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('H1', 'Validation Date Time');
                $excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);

                $excel->getActiveSheet()->setCellValue('I1', 'Validation Username');
                $excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('J1', 'Time between Verification and Validation');
                $excel->getActiveSheet()->getStyle('J1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('K1', 'Approval Date Time');
                $excel->getActiveSheet()->getStyle('K1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('L1', 'Approval Username');
                $excel->getActiveSheet()->getStyle('L1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('M1', 'Time between Validation and Approval');
                $excel->getActiveSheet()->getStyle('M1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('N1', 'Funds Released Date Time');
                $excel->getActiveSheet()->getStyle('N1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
                
                $excel->getActiveSheet()->setCellValue('O1', 'Funds Released Username');
                $excel->getActiveSheet()->getStyle('O1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);

                $excel->getActiveSheet()->setCellValue('P1', 'Time between Approval and Funds Released');
                $excel->getActiveSheet()->getStyle('P1')->getFont()->setSize(10);
                $excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);                
                
               foreach ($response['ResponseMessage'] as $key => $val) {    
                   
                   $rowPosition++;
//                    $search_result[] = array(
//                        sprintf('%06d',$val['loan_number']),  
//                        ,
//                        $val['validated_on'],
//                        $val['validated_by'],
//                       $val['approved_on'],
//                        $val['approved_by'],
//                    );
//                    $countRecord++;
//                    $loanAmount = $loanAmount+$val['amount'];
//                }
//                $this->response = array(
//                    'success'   => true,
//                    'data'      => $search_result,
//                    'totalcount' => $countRecord,
//                    'totalamount' => $loanAmount,
                    $excel->getActiveSheet()->setCellValue('A'.$rowPosition, sprintf('%06d',$val['loan_number']));
                    $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $val['firstname']);
                    $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('C'.$rowPosition,$val['lastname']);
                    $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('D'.$rowPosition, $val['amount']);
                    $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setSize(10);
                    
                    $excel->getActiveSheet()->setCellValue('E'.$rowPosition, $val['verified_on']);
                    $excel->getActiveSheet()->getStyle('E'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('F'.$rowPosition, $val['verified_by']);
                    $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getFont()->setSize(10);
                    
                    $dateDiffCreateAndVerified="";


                    if(($val['verified_on']!=null))
                    {
                        $datetime1 = new DateTime($val['created_on']);
                        $datetime2 = new DateTime($val['verified_on']);
                        
                        $interval = $datetime2->diff($datetime1);
                        $dateDiffCreateAndVerified = $interval->format('%m month, %d days ,%H hours');
                        $x = $interval->format('%m');
                        $y = $interval->format('%d');
                        $z = $interval->format('%H');
                        $cvTotalMonths= $cvTotalMonths + $x;
                        $cvTotalDays= $cvTotalDays + $y;
                        $cvTotalHours= $cvTotalHours + $z;
                        $cvCounter++;
                    }
                    
                    $excel->getActiveSheet()->setCellValue('G'.$rowPosition, $dateDiffCreateAndVerified);
                    $excel->getActiveSheet()->getStyle('G'.$rowPosition)->getFont()->setSize(10);
                    
                    $excel->getActiveSheet()->setCellValue('H'.$rowPosition, $val['validated_on']);
                    $excel->getActiveSheet()->getStyle('H'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('I'.$rowPosition, $val['validated_by']);
                    $excel->getActiveSheet()->getStyle('I'.$rowPosition)->getFont()->setSize(10);
                    
                    
                    $dateDiffVerifiedAndValidated="";
                    if(($val['verified_on']!=null && $val['validated_on']!=null))
                    {
                        $datetime1 = new DateTime($val['verified_on']);
                        $datetime2 = new DateTime($val['validated_on']);
                        
                        $interval = $datetime2->diff($datetime1);
                        $dateDiffVerifiedAndValidated = $interval->format('%m month, %d days ,%H hours');
                        $x = $interval->format('%m');
                        $y = $interval->format('%d');
                        $z = $interval->format('%H');
                        $vvTotalMonths= $vvTotalMonths + $x;
                        $vvTotalDays= $vvTotalDays + $y;
                        $vvTotalHours= $vvTotalHours + $z;
                        $vvCounter++; 
                    }
                    
                    $excel->getActiveSheet()->setCellValue('J'.$rowPosition, $dateDiffVerifiedAndValidated);
                    $excel->getActiveSheet()->getStyle('J'.$rowPosition)->getFont()->setSize(10);
                    
                    $excel->getActiveSheet()->setCellValue('K'.$rowPosition, $val['approved_on']);
                    $excel->getActiveSheet()->getStyle('K'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('L'.$rowPosition, $val['approved_by']);
                    $excel->getActiveSheet()->getStyle('L'.$rowPosition)->getFont()->setSize(10);
                    
                    

                    $dateDiffValidatedAndApproved="";
                    if(($val['approved_on']!=null && $val['validated_on']!=null))
                    {
                        $datetime1 = new DateTime($val['validated_on']);
                        $datetime2 = new DateTime($val['approved_on']);
                        
                        $interval = $datetime2->diff($datetime1);
                        $dateDiffValidatedAndApproved = $interval->format('%m month, %d days ,%H hours');
                        $x = $interval->format('%m');
                        $y = $interval->format('%d');
                        $z = $interval->format('%H');
                        $vaTotalMonths= $vaTotalMonths + $x;
                        $vaTotalDays= $vaTotalDays + $y;
                        $vaTotalHours= $vaTotalHours + $z;
                        $vaCounter++; 
                    }
                    
                    
                    $excel->getActiveSheet()->setCellValue('M'.$rowPosition, $dateDiffValidatedAndApproved);
                    $excel->getActiveSheet()->getStyle('M'.$rowPosition)->getFont()->setSize(10);
                    

                    $excel->getActiveSheet()->setCellValue('N'.$rowPosition, $val['fund_release_on']);
                    $excel->getActiveSheet()->getStyle('N'.$rowPosition)->getFont()->setSize(10);
                    $excel->getActiveSheet()->setCellValue('O'.$rowPosition, $val['fund_release_by']);
                    $excel->getActiveSheet()->getStyle('O'.$rowPosition)->getFont()->setSize(10);
                    
                    $dateDiffApprovedAndRelease="";
                    if(($val['fund_release_on']!=null && $val['approved_on']!=null))
                    {
                        $datetime1 = new DateTime($val['approved_on']);
                        $datetime2 = new DateTime($val['fund_release_on']);
                        
                        $interval = $datetime2->diff($datetime1);
                        $dateDiffApprovedAndRelease = $interval->format('%m month, %d days ,%H hours');
                        $x = $interval->format('%m');
                        $y = $interval->format('%d');
                        $z = $interval->format('%H');
                        $arTotalMonths= $arTotalMonths + $x;
                        $arTotalDays= $arTotalDays + $y;
                        $arTotalHours= $arTotalHours + $z;
                        $arCounter++;
                    }                   
                    $excel->getActiveSheet()->setCellValue('P'.$rowPosition, $dateDiffApprovedAndRelease);
                    $excel->getActiveSheet()->getStyle('P'.$rowPosition)->getFont()->setSize(10);
                    $countRecord++;
                    $loanAmount = $loanAmount+$val['amount'];

                }
                //JR-2014/03/31 For Summary (Count, Sum, Average)
                 //JR-2014/03/31 For Summary (Count, Sum, Average)
                 if ($countRecord>0){
                     $rowPosition+=4;
                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Count:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $countRecord);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                     
                     $rowPosition++;
                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Amount:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $loanAmount);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                     
                      $rowPosition++;
                     if ($cvCounter > 0){
                     $cvTotalMonths= $cvTotalMonths / $cvCounter;
                     $cvTotalDays= $cvTotalDays / $cvCounter;
                     $cvTotalHours= $cvTotalHours / $cvCounter;
                     $cvCombine = $cvTotalMonths . " months " . $cvTotalDays . " days " . $cvTotalHours . " hours";          }else{
                         $cvCombine = "0 months 0 days 0 hours";
                     }
                     
                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Time on Initial Application and Verification:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition,$cvCombine);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                     
                     $rowPosition++;
                     if ($vvCounter > 0){
                     $vvTotalMonths= $vvTotalMonths / $vvCounter;
                     $vvTotalDays= $vvTotalDays / $vvCounter;
                     $vvTotalHours= $vvTotalHours / $vvCounter;
                     $vvCombine = $vvTotalMonths . " months " . $vvTotalDays . " days " . $vvTotalHours . " hours";          }else{
                         $vvCombine = "0 months 0 days 0 hours";

                     }
                     
                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Time on Verification and Validation:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition,$vvCombine);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                     
                     $rowPosition++;
                     $vaTotalMonths= $vaTotalMonths / $vaCounter;
                     $vaTotalDays= $vaTotalDays / $vaCounter;
                     $vaTotalHours= $vaTotalHours / $vaCounter;
                     $vaCombine = $vaTotalMonths . " months " . $vaTotalDays . " days " . $vaTotalHours . " hours";
                     
                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Time on Validation and Approval:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition,$vaCombine);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                     
                     $rowPosition++;
                     $arTotalMonths= $arTotalMonths / $arCounter;
                     $arTotalDays= $arTotalDays / $arCounter;
                     $arTotalHours= $arTotalHours / $arCounter;
                     $arCombine = $arTotalMonths . " months " . $arTotalDays . " days " . $arTotalHours . " hours";

                     $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Time on Approval and Fund Release:');
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                     $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                     $excel->getActiveSheet()->setCellValue('B'.$rowPosition,$arCombine);
                     $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                 }
                $filename='Application Processing.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                             
                $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
            }
    }
    
    public function exportDailyLoan($startDate, $endDate)
    {
                 
        $startDate= date("Y-m-d", strtotime($startDate));
        
        $endDate= date("Y-m-d", strtotime('+1 day',strtotime($endDate)));
        
        $search_params = array(
            'startdate' => $startDate,
            'enddate' => $endDate,
        );
                                                                        
        $result = $this->backend->search_daily_loan_log($search_params);
        $response = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {   
            //$search_result = array();
            $rowPosition=1;
            $countRecord=0;
            $loanAmount=0;
            
            require_once "PHPExcel.php"; 
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('Daily Loan');
            //set cell A1 content with some text
            $excel->getActiveSheet()->setCellValue('A1', 'Loan Number');
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
             
            $excel->getActiveSheet()->setCellValue('B1', 'Borrower First Name');
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            
            $excel->getActiveSheet()->setCellValue('C1', 'Borrower Last Name');
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('D1', 'Borrower Employer');
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('E1', 'Loan Amount');
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            
            $excel->getActiveSheet()->setCellValue('F1', 'Loan Start Date');
            $excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('G1', 'Loan Termination Date');
            $excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            
            
            foreach ($response['ResponseMessage'] as $key => $val) {
                $rowPosition++;

                $excel->getActiveSheet()->setCellValue('A'.$rowPosition, "'".sprintf('%06d',$val['loan_number']));
                $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                
                 
                $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $val['firstname']);
                $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                
                
                $excel->getActiveSheet()->setCellValue('C'.$rowPosition, $val['lastname']);
                $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('D'.$rowPosition, $val['employer']);
                $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('E'.$rowPosition, $val['amount']);
                $excel->getActiveSheet()->getStyle('E'.$rowPosition)->getFont()->setSize(10);
                
                
                $excel->getActiveSheet()->setCellValue('F'.$rowPosition, $val['approved_on']);
                $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getFont()->setSize(10);
                
                $excel->getActiveSheet()->setCellValue('G'.$rowPosition, $val['TerminationDate']);
                $excel->getActiveSheet()->getStyle('G'.$rowPosition)->getFont()->setSize(10);

                
                $countRecord++;
                $loanAmount = $loanAmount+$val['amount'];
            }
             //JR-2014/03/31 For Summary (Count, Sum, Average)
              //JR-2014/03/31 For Summary (Count, Sum, Average)
             if ($countRecord>0){
                 $rowPosition+=4;
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Count:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $countRecord);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                 
                 $rowPosition++;
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Amount:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $loanAmount);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                 
                 $rowPosition++;
                 //$y = $x->format('%m month, %d days ,%H hours');
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Loan Amount:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $loanAmount / $countRecord);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
             }
             
        }
        
        
         
        $filename='Daily Loan.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        //        //merge cell A1 until D1
//        $excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
//        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    }
    
    
    protected function search_application_processing()
    {
        if(isset($_POST['startDate'], $_POST['endDate']))   
        {   
            $this->exportData();
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Generated Application Processing Log','Report',$datetime,$_SESSION['username'],'');

//             //$_POST = array_map('trim', $_POST);   
//            $startdate= date("Y-m-d", strtotime($_POST['startDate']));
//            $enddate= date("Y-m-d", strtotime('+1 day',strtotime($_POST['endDate'])));
//             
//            $search_params = array(
//                'startdate' => $startdate,
//                'enddate' => $enddate,
//            );
//             
//            $result = $this->backend->search_application_processing($search_params);
//            $response = $this->backend->get_response();
//            
//            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
//                $this->response['message'] = 'System error, unable to connect to database';
//            } elseif (!($response['ResponseCode'] == '0000')) {
//                $this->response['message'] = $response['ResponseMessage'];
//            } else {   
//                $search_result = array();
//                $countRecord=0;
//                $loanAmount=0;
//                foreach ($response['ResponseMessage'] as $key => $val) {
//                    $search_result[] = array(
//                        sprintf('%06d',$val['loan_number']),  
//                        substr($val['firstname'],0,20),
//                        substr($val['lastname'],0,20),
//                        $val['amount'],
//                        $val['validated_on'],
//                        $val['validated_by'],
//                        $val['approved_on'],
//                        $val['approved_by'],
//                    );
//                    $countRecord++;
//                    $loanAmount = $loanAmount+$val['amount'];
//                }
//                $this->response = array(
//                    'success'   => true,
//                    'data'      => $search_result,
//                    'totalcount' => $countRecord,
//                    'totalamount' => $loanAmount,
//                );
//            }
        }
        
    }
    
    
    protected function search_daily_loan_log()
    {
        if(isset($_POST['startDate'], $_POST['endDate']))   
        {   
            $this->exportData();
            //JR-2014/04/02 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Generated Daily Loan Log','Report',$datetime,$_SESSION['username'],'');
//            $_POST = array_map('trim', $_POST);   
//   
//            $startdate= date("Y-m-d", strtotime($_POST['startDate']));
//            
//            $enddate= date("Y-m-d", strtotime('+1 day',strtotime($_POST['endDate'])));
//            
//            $search_params = array(
//                'startdate' => $startdate,
//                'enddate' => $enddate,
//            );
//                                                  
//            $result = $this->backend->search_daily_loan_log($search_params);
//            $response = $this->backend->get_response();
//            
//            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
//                $this->response['message'] = 'System error, unable to connect to database';
//            } elseif (!($response['ResponseCode'] == '0000')) {
//                $this->response['message'] = $response['ResponseMessage'];
//            } else {   
//                $search_result = array();
//                $countRecord=0;
//                $loanAmount=0;
//                foreach ($response['ResponseMessage'] as $key => $val) {
//                    $search_result[] = array(
//                        sprintf('%06d',$val['loan_number']),  
//                        substr($val['firstname'],0,20),
//                        substr($val['lastname'],0,20),
//                        $val['employer'],
//                        $val['amount'],
//                        date("Y-m-d", strtotime($val['created_on'])),
//                        $val['nodata'],
//                    );
//                    $countRecord++;
//                    $loanAmount = $loanAmount+$val['amount'];
//                }
//                $this->response = array(
//                    'success'   => true,
//                    'data'      => $search_result,
//                    'totalcount' => $countRecord,
//                    'totalamount' => $loanAmount,
//                );
//            }
        }
    }
    
    public function changepassword() {
        if (isset($_POST['current_pw'])) {
            $this->layout = 'json';
            $_POST = array_map('trim', $_POST);
            if (!(isset($_POST['current_pw'], $_POST['new_pw1'], $_POST['new_pw2']))) {
                $this->response['message'] = 'All fields are required';
            } elseif (!($_POST['current_pw'] !='' && $_POST['new_pw1'] !='' && $_POST['new_pw2'] !='')) {
                $this->response['message'] = 'All fields are required';
            } elseif ($_POST['new_pw1'] != $_POST['new_pw2']) {
                $this->response['message'] = 'New password does not match';
            } elseif (strlen($_POST['new_pw1']) < 6) {
                $this->response['message'] = 'Password should be at least 6 characters long';
            } else {
                $params = array(
                    'username' => $_SESSION['username'],
                    'old_pw' => $_POST['current_pw'],
                    'new_pw' => $_POST['new_pw1'],
                );
                $result = $this->backend->change_password($params);
                $response = $this->backend->get_response();
                if (!$result) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                }
            }
        }
    }
    
    public function index() {
        $this->showInfo("Reports");
    }
    
    //JR-2014/04/16
    public function lpr()
    {
        $actions = array('search_lpr','print_lpr');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            if($action !='print_lpr')
            {
                $this->layout = 'json';
            }
            $this->$action();
        }
    }
    
    protected function search_lpr() {
        if (!isset($_POST['employer'], $_POST['fname'], $_POST['lname'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['employer']) !='' || trim($_POST['fname']) != '' || trim($_POST['lname'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {
        $search_params = array(
                'fname' => $_POST['fname'],
                'lname' => $_POST['lname'],
                'employer' => $_POST['employer'],
                'username' => $_SESSION['username'],
            );
            $result = $this->backend->search_lpr($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else { 
                $search_result = array();
                foreach ($response['ResponseMessage'] as $key => $val) {
                    //$id =$val['id'];
                    $webroot=WEBROOT."/reports/lpr?action=print_lpr&id=";
                     $button ="<a target='_blank' href='{$webroot}{$val['loan_number']}'>". '<input type="button" class="btn  btn-inverse" value="Generate Report" id="'.$val['loan_number'].'" /> </a>'."</a>";
                    $search_result[] = array(
                        $val['customer_name'],
                        $val['employer_name'],
                        $val['loan_number'],
                        $val['loan_amount'],
                        $button,
                    );
                }
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }
    
    public function print_lpr($message = null) {
        if (!(isset($_GET['id']))) {
            $this->response['message'] = 'Please select a valid salary deduction slip to print';
        } else {
            $search_params = array(
                'id' => $_GET['id'],
            );
            
        $result = $this->backend->search_lpr_by_id($search_params['id']);
        $response = $this->backend->get_response();

        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
                
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
                
            } else {   
                foreach ($response['ResponseMessage'] as $key => $val) {
                    
                    $customer_name = $val['customer_name'];
                    $today = date('m-d-y');
                    $street = $val['street'];
                    $city = $val['city'];
                    $mobile_number = $val['mobile_number'];
                    $employer_name = $val['employer_name'];
                    $loan_number = sprintf('%06d',$val['loan_number']);
                    $fund_release_on = date("F d Y", strtotime($val['fund_release_on']));//$val['fund_release_on'];
                    $loan_pay_off_date= date("F d Y", strtotime($val['loan_pay_off_date']));//$val['loan_pay_off_date'];
                    $total_payments_received = $val['total_payments_received'];
                    $per_diem_interest_amount = $val['per_diem_interest_amount'];
                    $loan_rebate = $val['loan_rebate'];
                    $loan_payoff_amount = $val['loan_payoff_amount'];
                    $loan_amount = $val['loan_amount'];
                    $payroll_cycle  = $val['payroll_cycle'];
                    $customers_pay_cycle_id  = $val['customers_pay_cycle_id'];
                    $loan_terms_id = $val['loan_terms_id'];
                    $repayment_amount = $val['repayment_amount'];
                }
                
                
        
        require_once('tcpdf/tcpdf.php');
        //require_once('tcpdf.php');
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING ="";
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        $pdf->SetAutoPageBreak(false, 0);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        //$pdf->setLanguageArray($l);
        //if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        //    require_once(dirname(__FILE__).'/lang/eng.php');
        //    $pdf->setLanguageArray($l);
        //}

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('Courier', 'B', 8);
        // add a page
        $pdf->AddPage();
        $image_path=WEBROOT . "/public/images/templates/lpr.png";
//print_r($image_path);
//die();       
        //JR-2014/04/05
       //$application_fee=$this->backend->get_application_fee($loan_amount,$loan_terms_id); 
        $loan_terms_count = $this->backend->get_loan_term_calendar_value($payroll_cycle,$loan_terms_id);
        
        $startDate= date("Y-m-d", strtotime($fund_release_on));
        $endDate= date("Y-m-d", strtotime($loan_pay_off_date));
        
        $date1 = new DateTime($startDate);
        $date2 = new DateTime($endDate);
        
        $loan_days_count = $date2->diff($date1)->format("%a");
        $remaining_days = $loan_days_count - $loan_terms_count;
        
        $per_diem_interest_amount = $this->backend->get_loan_per_diem_interest($payroll_cycle,$loan_terms_id,$loan_amount);
        
        $datetime=$this->backend->get_server_date();                
        $y = $pdf->GetY($image_path);
        $rby = $pdf->getImageRBY($image_path);
        $pdf->Image($image_path,10,20,200,$rby-$y);
        
        $pdf->Text(43,59.5,$customer_name);
        $pdf->Text(43,63,$street);
        $pdf->Text(43,71,$city);
        $pdf->Text(43,75,$mobile_number);
        $pdf->Text(43,79,$employer_name);
        $pdf->Text(43,83,$loan_number);
        
        //LOAN STATUS
        $pdf->Text(68,93,$fund_release_on);
        $pdf->Text(68,97,$loan_pay_off_date);
        $pdf->Text(68,101,'$'.$total_payments_received);
        $pdf->Text(68,105,'$'.$per_diem_interest_amount);
        $pdf->Text(68,109,$remaining_days);
        $pdf->Text(68,113,'$'. ($remaining_days*$per_diem_interest_amount));
        $pdf->Text(68,117,'$'.number_format($repayment_amount * $loan_terms_id - $total_payments_received,2));
        
        //FINACIAL INPUTS
        $pdf->Text(162,63,'$'.$loan_amount);
        $pdf->Text(162,67,'12.0%'); //should create a settings for this next time
        $pdf->Text(162,71,$payroll_cycle); 
        $pdf->Text(162,75,$loan_terms_id .' Month'); 
        
        //LOAN SUMMARY INPUTS
        $pdf->Text(180,86,'$'.$repayment_amount);
        $pdf->Text(180,90,'$'.number_format($repayment_amount * $loan_terms_id - $loan_amount,2));
        $pdf->Text(180,94,'$'.number_format($repayment_amount * $loan_terms_id,2));
        

        //$pdf->Text(33,47.5,$today);
        //$pdf->Text(53,72,$employer_name);
        
        $this->backend->save_to_action_logs('Generated Loan Payoff','Report',$datetime,$_SESSION['username'],'');
        
        $pdf->lastPage();
        $pdf->Output('lpr.pdf', 'I');
        $this->response = array(
                    'success'   => true,
                    'data'      => $response,
                );
            
            
            //        //merge cell A1 until D1
    //        $excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
    //        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
             
        }
        }
        
    }

    
	//JR-2014/03/24
    public function sds()
    {
        $actions = array('search_sds','print_sds');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            if($action !='print_sds')
            {
                $this->layout = 'json';
            }
            $this->$action();
        }
    }
    
    protected function search_sds() {
        if (!isset($_POST['employer'], $_POST['fname'], $_POST['lname'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['employer']) !='' || trim($_POST['fname']) != '' || trim($_POST['lname'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {
        $search_params = array(
                'fname' => $_POST['fname'],
                'lname' => $_POST['lname'],
                'employer' => $_POST['employer'],
                'username' => $_SESSION['username'],
            );
            $result = $this->backend->search_sds($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else { 
                $search_result = array();   
                $webroot=WEBROOT."/reports/sds?action=print_sds&id=";
                $value_sds = "&type=sds";
                $value_pn = "&type=pn";
                foreach ($response['ResponseMessage'] as $key => $val) {
                    //$id =$val['id'];

                    $button ="<a target='_blank' href='{$webroot}{$val['id']}{$value_sds}'>". '<input type="button" class="btn  btn-inverse" value="Salary Deduction Slip" id="'.$val['id'].'" /> </a>'."</a>";
                    $button_pn ="<a target='_blank' href='{$webroot}{$val['id']}{$value_pn}'>". '<input type="button" class="btn  btn-inverse" value="Promissory Note" id="'.$val['id'].'" /> </a>'."</a>";
                    $search_result[] = array(
                        $val['fname'],
                        $val['lname'],
                        $val['employer'],
                        $val['repayment_amount'],
                        $button,
						$button_pn
                    );
                }
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }


    //JR-2014/03/25
    public function print_sds($message = null) {
        if (!(isset($_GET['id']))) {
            $this->response['message'] = 'Please select a valid salary deduction slip to print';
        } else {
            $search_params = array(
                'id' => $_GET['id'],
				'type' => $_GET['type'],
            );
            
        $result = $this->backend->search_sds_by_id($search_params['id']);

        $response = $this->backend->get_response();

        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
                
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
                
            } else {   
                foreach ($response['ResponseMessage'] as $key => $val) {
                    
                    $name = $val['fname'] . " " . $val['lname'];
                    $today = date('m-d-y');
                    $employer = $val['employer'];
                    $amount = $val['repayment_amount'];
					$address = $val['address'];
					$loan_id = sprintf('%06d',$val['id']);
                    $loan_terms_id = $val['loan_terms_id'];
                    $loan_amount = $val['amount'];
                }
        
        $this->response = array(
                    'success'   => true,
                    'data'      => $response,
                );
            }
        
        require_once('tcpdf/tcpdf.php');
        //require_once('tcpdf.php');
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING ="";
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        $pdf->SetAutoPageBreak(false, 0);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        //$pdf->setLanguageArray($l);
        //if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        //    require_once(dirname(__FILE__).'/lang/eng.php');
        //    $pdf->setLanguageArray($l);
        //}

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('Courier', 'B', 12);
        // add a page
        $pdf->AddPage();
		if ($search_params['type']=='sds'){
            $image_path=WEBROOT . "/public/images/templates/sds1.png";
        }else{
            $image_path=WEBROOT . "/public/images/templates/pn1.png";
        }
//print_r($image_path);
//die();       
        //JR-2014/04/05
       $application_fee=$this->backend->get_application_fee($loan_amount,$loan_terms_id); 
       
        $datetime=$this->backend->get_server_date();                
        $y = $pdf->GetY($image_path);
        $rby = $pdf->getImageRBY($image_path);
        $pdf->Image($image_path,10,20,200,$rby-$y);
		if ($search_params['type']=='sds'){
            $pdf->Text(30,132,$name);
            $pdf->Text(33,47.5,$today);
            $pdf->Text(63,56.5,$employer);
            $pdf->Text(60,155,$employer);
            $pdf->Text(135,84,$amount);
            $this->backend->save_to_action_logs('Generated Salary Deduction Slip','Report',$datetime,$_SESSION['username'],'');
        }else{
            $pdf->Text(160,33,$loan_id);
            $pdf->Text(53,52,$name);
            $pdf->Text(53,62,$address);
            //$pdf->Text(33,47.5,$today);
            $pdf->Text(53,72,$employer);
            $pdf->Text(33,107,$name);
            $pdf->Text(86,120,$amount);
            $pdf->Text(98,126,$application_fee); //replace with activation fee
            $pdf->Text(46,195,$amount); 
            $this->backend->save_to_action_logs('Generated Promissory Note','Report',$datetime,$_SESSION['username'],'');
        }
        $pdf->lastPage();
        $pdf->Output('sds.pdf', 'I');
        
        }  
    }
    public function exportActionLog($startDate, $endDate)
    {
                 
        $startDate= date("Y-m-d", strtotime($startDate));
        
        $endDate= date("Y-m-d", strtotime('+1 day',strtotime($endDate)));
        
        $search_params = array(
            'startdate' => $startDate,
            'enddate' => $endDate,
        );
                                                                        
        $result = $this->backend->search_action_log($search_params);
        $response = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {   
            //$search_result = array();
            $rowPosition=1;
            $countRecord=0;
            
            require_once "PHPExcel.php"; 
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('Action Logs');
            //set cell A1 content with some text
            $excel->getActiveSheet()->setCellValue('A1', 'Action Log Number');
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
             
            $excel->getActiveSheet()->setCellValue('B1', 'Activity');
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            
            $excel->getActiveSheet()->setCellValue('C1', 'Module');
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('D1', 'Date');
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('E1', 'Done by');
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            
            $excel->getActiveSheet()->setCellValue('F1', 'Remark');
            $excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

            
            foreach ($response['ResponseMessage'] as $key => $val) {
                $rowPosition++;

                $excel->getActiveSheet()->setCellValue('A'.$rowPosition, "'".sprintf('%06d',$val['id']));
                $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                
                 
                $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $val['action_taken']);
                $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                
                
                $excel->getActiveSheet()->setCellValue('C'.$rowPosition, $val['action_module']);
                $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('D'.$rowPosition, $val['action_date']);
                $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('E'.$rowPosition, $val['action_by']);
                $excel->getActiveSheet()->getStyle('E'.$rowPosition)->getFont()->setSize(10);
                
                
                $excel->getActiveSheet()->setCellValue('F'.$rowPosition, $val['remark']);
                $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getFont()->setSize(10);
                
                
                $countRecord++;
            }
             //JR-2014/03/31 For Summary (Count, Sum, Average)
             
             $rowPosition+=4;
             $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Count:');
             $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
             $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
             $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $countRecord);
             $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
             
        }
        
        
         
        $filename='Action Logs.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        //        //merge cell A1 until D1
//        $excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
//        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    }   
}