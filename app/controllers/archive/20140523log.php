<?php
class log extends template {    
    public function in($var) {
        if (isset($_POST['submit'])) {
            if (isset($_POST['username'], $_POST['password'])) {
                

//        $params = array(
//        'customer_id' => 1,
//        'card_number' => '0291029012' 
//        );

//        $result = $this->backend->update_card_number($params);
//        $response = $this->backend->get_response();

//        print_r($response);

//        die();
                        
                $login_params = array(
                    'username' => $_POST['username'],
                    'password' => $_POST['password']
                );
            
                
                //LC-09/11/2013
                //$response = lib::getWsResponse($this->meta['remote_api'], 'login', $login_params);
                $result = $this->backend->login($login_params);
                $response = $this->backend->get_response();
               
                if (isset($response['ResponseMessage']['SessionID'], $response['ResponseCode']) && $response['ResponseCode'] == '0000') {
                    $_SESSION['sessionid'] = $response['ResponseMessage']['SessionID'];
                    $_SESSION['permissions'] = array_flip(explode(';', $response['ResponseMessage']['Permissions']));
                
                    $_SESSION['user_type'] = $response['ResponseMessage']['user_type'];
                    //print_r($_SESSION['user_type']);
                    
                    $_SESSION['username'] = $_POST['username'];
                    $_SESSION['last_access'] = time();
                    if (count($var) > 0 && is_array($var) && $redirect = base64_decode($var[0])) {
                        $path = $redirect;
                    } else {                        
                        $permissions=$_SESSION['permissions'];
                        //$this->meta['home_page']='customers/homepage';
                        $this->set_home_page();
                        $meta=$this->meta['home_page']; 
                        //print_r($meta);
                        //die();
                        if(array_key_exists($meta,$permissions))
                        {
                            $path = $this->meta['home_page'];
                        }else{
                            $path="";    
                        }
                        
                    }   
                    
                                        
                    
                    return $this->redirect($path);
                } else {
                    $this->showError('Invalid username or password.');
                }
            }
            $this->view->assign('_data_', $_POST);
        } elseif (isset($_SESSION['sessionid'], $_SESSION['username'])) {
            $time = time();
            if ($time - $_SESSION['last_access'] > $this->meta['max_session_minutes'] * 60) {
                unset($_SESSION);
                session_destroy();
                session_regenerate_id(true);
            } else {
                $path = $this->meta['home_page'];
                return $this->redirect($path);
                exit;
            }
        } else {
            if (isset($_SESSION['sessionid'])) {
                unset($_SESSION['sessionid']);
            }
        }
    }
    
    public function set_home_page() {
        $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        if ($usertype=='Admin'){
            
        } else if ($usertype=='Staff') {
            
        } else if ($usertype=='Agent') {
            
        } else if ($usertype=='Super Agent') {
            
        } else if ($usertype=='Customer') {
            $this->meta['home_page']='customers/homepage';
        } else if ($usertype=='Super User') {
            
        } else {
            // Do nothing
        }
    }
    

    public function out($var) {
        if (isset($_SESSION)) {
            if (isset($_SESSION['sessionid'])) {
                //lib::getWsResponse($this->meta['remote_api'], 'logout', array('P01' => $_SESSION['sessionid']));
                $params= array('session_id' => $_SESSION['sessionid']);
                $this->backend->logout($params);
            }
            unset($_SESSION);
            session_destroy();
            session_regenerate_id(true);
        }
        $this->redirect('log/in');
    }    
}