<?php
class settings extends template {
    
    public function __construct($meta) {
        parent::__construct($meta);
    }
    
    public function usermanagement() {
        $this->showInfo('This page is currently unavailable.');
    }
    
    public function changepassword() {
        if (isset($_POST['current_pw'])) {
            $response = array('success' => FALSE, 'message' => 'Unknown error');
            $user_check = $this->db->fetchAll("select * from ez_users where username=" . $this->db->db_escape($_SESSION['username']) . " and password=" . $this->db->db_escape(md5($_POST['current_pw'])));
            if (! (is_array($user_check) && count($user_check) == 1)) {
                $response['message'] = 'Invalid current password';
            } elseif ($_POST['new_pw1'] != $_POST['new_pw2']) {
                $response['message'] = 'New password does not match';
            } elseif (strlen($_POST['new_pw1']) < 6) {
                $response['message'] = 'Password should be at least 6 characters long';
            } else { 
                $update_data = array(
                    'password' => md5($_POST['new_pw1']),
                );
                if (!$this->db->update('ez_users', $update_data, 'username=' . $this->db->db_escape($_SESSION['username']))) {
                    $response['message'] = 'Unable to change the password';
                } else {
                    $response['success'] = TRUE;
                    $response['message'] = 'Successfully changed user password.';
                }
            }
            $this->layout = false;
            $output = json_encode($response);
            header('Content-type: application/json');
            echo $output;
        }
    }
    
    public function index() {
        $this->showInfo("Settings");
    }
}