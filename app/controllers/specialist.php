<?php
  class specialist extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }

      private function delete_specialist()
      {
		  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
      		// print_r($_POST);
      		// die();
			$params = array(
	        	'session_id' => $_SESSION['sessionid'],
	        	'specialist_id' => $_POST['specialist_id'],
	        	'update_date' => date('Y-m-d'),
	        );

	        //LC-09/11/2013
	        $response = lib::getWsResponse(API_URL, 'delete_my_specialist', $params);

				                   
		      if (!(isset($response['respcode'], $response['respcode']))) {
		            $this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		      } elseif (!($response['respcode'] == '0000')) {
		            $this->response['success'] = false;
		            $this->response['message'] = $response['respmsg'];
		      } else {
		            $this->response = array(
		                'success' => true,
		                'message' => $response['respmsg'],
		                'redirect_url' => WEBROOT ."/specialist/management",
		            );
		      }
      }
	  
	  public function management()
      {		
            $actions = array("update", "add", "delete_specialist");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant', $params);
			$specialist = $response['respmsg'];
			$this->view->assign('specialist', $specialist);
			
			// print_r($specialist);
			// die();

			$response = lib::getWsResponse(API_URL, 'get_business_hours_per_merchant', $params);
			$business_hours = $response['respmsg'];
			$this->view->assign('business_hours', json_encode($business_hours));
			
			$response = lib::getWsResponse(API_URL, 'get_branch_per_merchant', $params);
			$branches = $response['respmsg'];
			
			
			$this->view->assign('branches', $branches);
			
			$response = lib::getWsResponse(API_URL, 'get_countries', $params);
			$countries = $response['respmsg'];
			
			$this->view->assign('countries', $countries);
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
			
			$services = $response['respmsg'];
			
			$this->view->assign('services', $services);

			$response = lib::getWsResponse(API_URL, 'get_product_categories', $params);
		 	$categories = $response['respmsg'];
		 	$this->view->assign('categories', $categories);
			
      }
	  
	  private function add()
	  {
	  	
		$sTempFileName="";
		if($_FILES['file']['name']!=="")
        {
			if (! $_FILES['file']['error'] && $_FILES['file']['size'] < (1024*2) * 1024) {
	                if (is_uploaded_file($_FILES['file']['tmp_name'])) {
	
	                    $aSize = getimagesize($_FILES['file']['tmp_name']); // try to obtain image info
	                    if (!$aSize) {
	                        @unlink($sTempFileName);
	                        return;
	                    }
	    
	                    // check for image type
	                    switch($aSize[2]) {
	                        case IMAGETYPE_JPEG:
	                            $sExt = '.jpg';
	                            break;
	                        case IMAGETYPE_PNG:
	                            $sExt = '.png';
	                            break;
	                        default:
	                            @unlink($sTempFileName);
	                            return;
	                    }
	                    
	                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
	                    $sTempFileName = 'images/uploads/' .time().rand()."{$sExt}";
	
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['file']['tmp_name'], $sTempFileName);
	                    
	                    $sTempFileName = WEBROOT."/public/{$sTempFileName}";
	                  }
			}  
		}
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	// 'branch_id' => $_POST['txtBranch'],
			'branch_id' => "-1",
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'middle_name' => $_POST['txtMiddleName'],
        	'birth_date' => $_POST['txtBirthDate'],
        	'profile_picture' => $sTempFileName,
        	'gender' => $_POST['txtGender'],
        	'city' => $_POST['txtCity'],
        	'address' => $_POST['txtAddress'],
        	'state' => $_POST['txtState'],
        	// 'country' => $_POST['txtCountry'],
        	'country' => 'United States',
        	'start_date' => $_POST['txtStartDate'],
        	'end_date' => $_POST['txtEndDate'],
        	'commission_id' => -1,
        	'schedule' => $_POST['txtDays'],
        	'start_time' => $_POST['txtStartTime'],
        	'end_time' => $_POST['txtEndTime'],
        	'services' => $_POST['txtServices'],
        	'contact_number' => str_replace("-", "", $_POST['txtContactNumber']), 
        	'email_address' => $_POST['txtEmailAddress'],
         );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'register_salon_specialist', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	                'redirect_url' => WEBROOT ."/specialist/management",
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		$sTempFileName="";
		
		// print_r($_FILES);
		// die();
		if($_FILES['file']['name']!=="")
        {
			if (! $_FILES['file']['error'] && $_FILES['file']['size'] < (1024*2) * 1024) {
	                if (is_uploaded_file($_FILES['file']['tmp_name'])) {
	
	                    $aSize = getimagesize($_FILES['file']['tmp_name']); // try to obtain image info
	                    if (!$aSize) {
	                        @unlink($sTempFileName);
	                        return;
	                    }
	    
	                    // check for image type
	                    switch($aSize[2]) {
	                        case IMAGETYPE_JPEG:
	                            $sExt = '.jpg';
	                            break;
	                        case IMAGETYPE_PNG:
	                            $sExt = '.png';
	                            break;
	                        default:
	                            @unlink($sTempFileName);
	                            return;
	                    }
	                    
	                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
	                    $sTempFileName = 'images/uploads/' .time().rand()."{$sExt}";
	
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['file']['tmp_name'], $sTempFileName);
	                    
	                    $sTempFileName = WEBROOT."/public/{$sTempFileName}";
	                  }
			}     
		}


		// $post_data = array(
		// 	'method' => 'upload_merchant_image',
		// 	'session_id' => $_SESSION['sessionid'],
		// 	'image' =>   '@'   . $_FILES['file']['tmp_name']. ';filename=' . $_FILES['file']['name']
  //   	);

  //   	$filename = $_FILES['file']['name'];
		// $filedata = $_FILES['file']['tmp_name'];
		// $filesize = $_FILES['file']['size'];


		// $headers = array("Content-Type:multipart/form-data");

		// $post_data = array(
		// 	"image" => "@$filedata", "filename" => $filename,
		// 	'method' => 'upload_merchant_image',
		// 	'session_id' => $_SESSION['sessionid'],
		// 	);

		// $ch = curl_init();
		// $postUrl = "http://localhost/pos_reservation/pos_api.php";

		// curl_setopt($ch, CURLOPT_URL, $postUrl);
		
		// curl_setopt($ch, CURLOPT_HEADER, true);
		// curl_setopt($ch, CURLOPT_POST, true);

		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		
		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		
		// $response = curl_exec($ch);

		// print_r($response);
		// die();

		// print_r($_FILES);
		// die();
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtSpecialistId'],
        	// 'branch_id' => $_POST['txtBranch'],
        	'branch_id' => "-1",
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'middle_name' => $_POST['txtMiddleName'],
        	'birth_date' => $_POST['txtBirthDate'],
        	//'profile_picture' => $sTempFileName,
        	'gender' => $_POST['txtGender'],
        	'city' => $_POST['txtCity'],
        	'address' => $_POST['txtAddress'],
        	'state' => $_POST['txtState'],
        	//'country' => $_POST['txtCountry'],
        	'country' => 'United States',
        	'start_date' => $_POST['txtStartDate'],
        	'end_date' => $_POST['txtEndDate'],
        	'commission_id' => -1,
        	'schedule' => $_POST['txtDays'],
        	'start_time' => $_POST['txtStartTime'],
        	'end_time' => $_POST['txtEndTime'],
        	'services' => $_POST['txtServices'],
        	'contact_number' => str_replace("-", "", $_POST['txtContactNumber']),
        	'email_address' => $_POST['txtEmailAddress'],
			
         );

		if ($sTempFileName) {
			$params['profile_picture'] = $sTempFileName;
		}

        //LC-09/11/2013
        
        
        
        $response = lib::getWsResponse(API_URL, 'update_salon_specialist', $params);

		// print_r($params);
		// die();			                   
							   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	                'redirect_url' => WEBROOT ."/specialist/management",
	            );
	      }
	  }
	  
	  
      private function delete()
      {
            $id = $_GET['id'];
            
            $result = $this->backend->delete_agent($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
      }
  }  
?>
