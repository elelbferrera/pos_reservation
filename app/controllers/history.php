<?php
    class history extends template {
        protected $response;
		protected $merchant_id;
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
        }
		
		
		function hour_min($minutes){// Total
		   if($minutes <= 0) return '00 Hour(s) 00 Minute(s)';
			else    
		   return sprintf("%02d",floor($minutes / 60)).' Hour(s) '.sprintf("%02d",str_pad(($minutes % 60), 2, "0", STR_PAD_LEFT)). " Minute(s)";
		}
        
      	public function management($date)
        {
	
            $actions = array("add", "update", "delete", "search");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
			if(isset($date[0])){
				$date_filtered = $date[0];
			}else{
				$date_filtered = date("Y-m-d");
			}
			
			$this->view->assign("date_selected", $date_filtered);
			
			
			// $today = date("Y-m-d");
			//$prev_month = date("Y-m-d", strtotime("$today"));
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	//'date' => $prev_month,
            	'date' => $date_filtered,
            );

			$this->view->assign('date_filtered', $date_filtered);
            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservations_per_merchant', $params);
			$reservations = $response['respmsg'];
			$this->view->assign('reservations', $reservations);

			
			$r_list = array();
			
			$hidden_fields = "";
			foreach($reservations as $r)
			{
				// print_r($r);
				// die();
				
				$id = $r['id'];	
				$date = substr($r['reservation_date'], 0, 10);
				
				// print_r($date);
				// die();
				$date .= "T".$r['reservation_time'];
				
				
				$selectedTime = $r['reservation_time'];
				$duration = $r['duration'];
				$endTime = strtotime("+{$duration} minutes", strtotime($selectedTime));
				$endTime = date('h:i:s', $endTime);
				$endDate = substr($r['reservation_date'], 0, 10). "T{$endTime}";
				//echo date('h:i:s', $endTime);
				// print_r($endDate);
				// die();
				
				$customer = $r['first_name']." ".$r['last_name'];

				$r['customer_name'] = $customer;
				
				$services  = " ";
				foreach($r['details'] as $d)
				{
					$services .= " {$d['product_name']}, ";
				}
				$r['services_get'] = rtrim($services, ', ');

				$r_list[] = $r;
			}
			// print_r($r_list);
			// die();

			// print_r($r_list);
			// die();

			$this->view->assign('reservations', $r_list);
        }
        
	  private function search()
	  {
			$params = array(
				'session_id' => $_SESSION['sessionid'],
				'mobile' => str_replace("-", "", $_POST['txtMobileSearch']) 
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'search_customer_by_mobile', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'last_name' => $resp['last_name'],
			            'first_name' => $resp['first_name'],
			            'mobile' => $resp['mobile'],
			        );
			  }
	  }
	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'create_reservation_via_merchant', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtReservationId'],
        	'reservation_status_id' => $_POST['txtStatus'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );
		
		// print_r($params);
		// die();
        //LC-09/11/2013
        
        $response = lib::getWsResponse(API_URL, 'update_reservation', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	
		
        
        protected function delete()
        {
            $id = $_GET['id'];
            $result = $this->backend->delete_branch($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
        }
        
        
    }
?>
