<?php
  class category extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }
	  
	  public function management()
      {		
            $actions = array("update", "add", "delete_category");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

			 //get_product_categories
			$response = lib::getWsResponse(API_URL, 'get_product_categories', $params);
			$categories = $response['respmsg'];
			$this->view->assign('categories', $categories);
      }




      private function delete_category()
      {
		  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
      		// print_r($_POST);
      		// die();
			$params = array(
	        	'session_id' => $_SESSION['sessionid'],
	        	'category_id' => $_POST['category_id'],
	        	'update_date' => date('Y-m-d'),
	        );

	        //LC-09/11/2013
	        $response = lib::getWsResponse(API_URL, 'delete_my_category', $params);

	        // print_r($response);
	        // die();
				                   
		      if (!(isset($response['respcode'], $response['respcode']))) {
		            $this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		      } elseif (!($response['respcode'] == '0000')) {
		            $this->response['success'] = false;
		            $this->response['message'] = $response['respmsg'];
		      } else {
		            $this->response = array(
		                'success' => true,
		                'message' => $response['respmsg'],
		                'redirect_url' => WEBROOT ."/category/management",
		            );
		      }
      }

	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'name' => $_POST['txtName'],
        	'description' => $_POST['txtDescription'],
        );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'create_product_category', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	                'redirect_url' => WEBROOT ."/category/management",
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// print_r($_POST);
		// die();
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'name' => $_POST['txtName'],
        	'description' => $_POST['txtDescription'],
        	'id' => $_POST['txtCategoryId'],
        );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'update_product_category', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	                'redirect_url' => WEBROOT ."/category/management",
	            );
	      }
	  }
	  
	  
      private function delete()
      {
            $id = $_GET['id'];
            
            $result = $this->backend->delete_agent($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
      }
  }  
?>
