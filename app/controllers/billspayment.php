<?php
class billspayment extends template {
    protected $response;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'uploads/';
        $this->download_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'downloads/';
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
    public function customer() {
        $actions = array('search_customer','upload_payment','get_transaction_fee');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $billers = $this->backend->get_billers_pair();
        $this->view->assign('billers', $billers);
    }
    
    protected function get_transaction_fee(){
        $transaction_fee = $this->backend->get_system_setting('transaction_fee');
        //print ($transaction_fee);
        $this->response = array(
            'success' => true,
            'message' => $transaction_fee,
        );
        
    }
    
    protected function upload_payment(){
        $params = array(
            'account_no' => $_POST['bp_accountno_success'],
            'payment_amount' => $_POST['payment_amount_final'],
            'transaction_fee' => $_POST['transaction_fee_final'],
            'total_amount'  => $_POST['total_amount_final'],
            'username'  => $_SESSION['username'],
            'biller_code'  => $_POST['bp_biller_code'],
        );    
        $payment = $this->backend->add_payment($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
        );
    }
    
    protected function search_customer() {
     
        if (!isset($_POST['bp_account_no'], $_POST['bp_phone_number'], $_POST['bp_last_name'], $_POST['bp_first_name'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['bp_account_no']) !='' || trim($_POST['bp_phone_number']) != '' || trim($_POST['bp_first_name'])!= ''|| trim($_POST['bp_last'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'first_name' => $_POST['bp_first_name'],
                'last_name' => $_POST['bp_last_name'],
                'account_no' => $_POST['bp_account_no'],
                'phone_number' => $_POST['bp_phone_number'],
                'biller_code' => $_POST['biller'],
            );
            //print_r($search_params);
            //die();
            $result = $this->backend->search_customer_ex($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //JR-2014/05/23 Get loan count to check if user is still allowed to access the page
                $user_type_description = $this->backend->get_user_type_by_username_ex($_SESSION['username']);
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $button = '<input type="button" class="btn btn-file" value="Add Payment" id="'.$val['id'].'"  
                    onclick="addPaymentCustomerBP('."'".$val['account_no']."'".",'".$val['customer_name']."'".",'".$val['phone_number']."'"
                    .",'".$val['house_number']."'".",'".$val['street_name']."'".",'".$val['apartment_id']."'".",'".$val['zip_code']."'".",'".$_POST['biller']."'".')" />';          
                    $search_result[] = array(
                        $val['account_no'],
                        $val['customer_name'],
                        $val['phone_number'],
                        $val['house_number'],
                        $val['street_name'],
                        $val['apartment_id'],
                        $val['zip_code'],
                        $button,
                    );
                }
                 
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }
    
    public function upload() {
        $actions = array('upload_file','process_file');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    protected function process_file() {
        $_POST = $this->array_map_recursive('trim', $_POST);   
        $filename = $this->upload_path . $_POST['filename'];
        $handle = @fopen($filename, "r");
        $ctr =0;
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $customer_params = array(
                    'account_no'    => substr($buffer,0,18),
                    'customer_name' => substr($buffer,18,30),
                    'phone_number'  => substr($buffer,48,10),
                    'house_number'  => substr($buffer,58,5),
                    'street_name'   => substr($buffer,63,25),
                    'apartment_id'  => substr($buffer,88,5),
                    'zip_code'      => substr($buffer,93,9),
                    //'blank'         => substr($buffer,99,22),
                );
                $customers = $this->backend->add_customer($customer_params);
                $response = $this->backend->get_response();
                if ($response['ResponseCode']=="0000"){
                    $ctr = $ctr +1;
                }
            }
            if (!feof($handle)) {
                $this->response['message'] = 'Error: unexpected fgets() fail';
                $this->response['success'] = false;                
            }
            fclose($handle);
            $this->response['message'] = '';
            $this->response['success'] = true;
        }
       
        
        
    }
    
    protected function upload_file() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    
                    $this->response['message'] = 'That file type is not valid for this application process. Please use a text file.';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        $this->response['message'] = 'That file type is not valid for this application process. Please use a text file.';
                    } else {
                        $parts = explode('_', $key);
                        $destination = $this->upload_path . $upload['name'];
                        //echo $destination;
                        //die();
                        if (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    public function generate(){
        $actions = array('download_file');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }    
    }
    
    protected function download_file(){
    
        $creationdatetime=date('YmdHis');
        $realpath = $this->download_path;
        $url =$realpath . $creationdatetime . "_PaymentFile.txt";
        
        $customers = $this->backend->getPaymentsForExport();
        $response = $this->backend->get_response();
        $search_result = array();
        $ctr=1;
        $content="";                      
                  
        
        $spaces = str_pad("",64);
        $content="HBEC Payments{$creationdatetime}{$spaces}";
        $fp = fopen($url,"w");
        fwrite($fp,"{$content}\r\n");
        fclose($fp);
        
        $mangoNetworkId= str_pad(MANGO_NETWORK_ID,15);
        $mangoConfirmationNo= str_pad(MANGO_CONFIRMATION_NUMBER,16);
        $mangoSiteId= str_pad(MANGO_SITE_ID,10);
        //print_r(URL_API_MAIN);
        //die();
        $totalAmount=0;
        $strIDs="";
        foreach ($response['ResponseMessage'] as $key => $val) { 
            $date = new DateTime($val['transaction_date']);
            $transDate = $date->format('YmdHis');
            $sequenceno=str_pad($ctr,7,"0",STR_PAD_LEFT);
            $accountno= str_pad($val['account_no'],18,"0",STR_PAD_LEFT);
            $amount = str_pad($val['total_amount'],10,"0",STR_PAD_LEFT);
            
            $content="D{$mangoNetworkId}{$transDate}{$sequenceno}{$mangoConfirmationNo}{$accountno}{$amount}{$mangoSiteId}";
            $fp = fopen($url,"a+");
            fwrite($fp,"{$content}\r\n");
            fclose($fp);  
            $ctr++;
            
            $totalAmount = $totalAmount+$val['total_amount'];
            $strIDs.=$val['id'].",";
        }
        
        $ctr--;
        $totalRecord = str_pad($ctr,7,"0",STR_PAD_LEFT);
        $totalAmount = str_pad($totalAmount,12,"0",STR_PAD_LEFT);
        $spaces = str_pad("",71);
        
        $content="T{$totalRecord}{$totalAmount}{$spaces}";
        $fp = fopen($url,"a+");
        fwrite($fp,"{$content}\r\n");
        fclose($fp);
        
         if($strIDs!="")
        {
            $current = file_get_contents($url);
            header('Content-disposition: attachment; filename=payment.txt');
            header('Content-type: text/plain');
            echo $current;
            die();  
            
            /*$handle = fopen("file.txt", "w");
            fwrite($handle, $content);
            fclose($handle);

            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename('file.txt'));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize('file.txt'));
            readfile('file.txt');
            exit;
            */
            
            
        } else {
                $this->response['message'] = 'Error encountered while saving textfile.';
                $this->response['success'] = false;
        }
    }
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
}
