<?php
class viewimg extends template {
    protected $allowed_types = array('image/jpeg','image/gif','image/png','image/bmp');
    
    public function view_document_image() {
        $this->layout = false;
        if (!(isset($_GET['token']) && $_GET['token'] != '')) {
            echo 'Invalid request';
        } elseif (!(isset($_GET['token2']) && $_GET['token2'] != '')) {
            echo 'Invalid request';
        } elseif (false === ($actual_file = $this->backend->get_actual_document_pathname(array('token' => $_GET['token'], 'token2' => $_GET['token2'])))) {
            echo 'Unable to retrieve document';
        } elseif (!file_exists($actual_file)) {
            echo $actual_file;
        } else {
            $imageinfo = @getimagesize($actual_file); //check image size
            if (!(in_array($imageinfo['mime'], $this->allowed_types))) {
                echo 'Invalid image';
            } else {
                $fp = fopen($actual_file, 'rb');
                header('Content-Type: '. $imageinfo['mime']);
                header('Content-Length: '. filesize($actual_file));
                fpassthru($fp);
                exit;
            }
        }
    }
}