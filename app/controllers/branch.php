<?php
    class branch extends template {
        protected $response;
		protected $merchant_id;
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
        }
        
        public function management()
        {

            $actions = array("add", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_branch_per_merchant', $params);
			
			$branches = $response['respmsg'];
			$this->view->assign('branches', $branches);
		
			$response = lib::getWsResponse(API_URL, 'get_countries', $params);
			$countries = $response['respmsg'];
			
			$this->view->assign('countries', $countries);
			
			 
        }
        
		  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'branch_name' => $_POST['txtBranchName'],
        	'longitude' => $_POST['txtLongitude'],
        	'latitude' => $_POST['txtLatitude'],
        	'country' => $_POST['txtCountry'],
        	'state' => $_POST['txtState'],
        	'city' => $_POST['txtCity'],
        	'address' => $_POST['txtAddress'],
        	'zip' => $_POST['txtZip'],
        	'contact_number' => $_POST['txtContactNumber'],
        	'store_day' => $_POST['txtDays'],
        	'store_start_time' => $_POST['txtOpeningTime'],
        	'store_end_time' => $_POST['txtClosingTime']
         );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'register_merchant_branch', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtBranchId'],
        	'branch_name' => $_POST['txtBranchName'],
        	'longitude' => $_POST['txtLongitude'],
        	'latitude' => $_POST['txtLatitude'],
        	'country' => $_POST['txtCountry'],
        	'state' => $_POST['txtState'],
        	'city' => $_POST['txtCity'],
        	'address' => $_POST['txtAddress'],
        	'zip' => $_POST['txtZip'],
        	'contact_number' => $_POST['txtContactNumber'],
        	'store_day' => $_POST['txtDays'],
        	'store_start_time' => $_POST['txtOpeningTime'],
        	'store_end_time' => $_POST['txtClosingTime']
         );

        //LC-09/11/2013
        
        
        
        $response = lib::getWsResponse(API_URL, 'update_merchant_branch', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	
		
        
        protected function delete()
        {
            $id = $_GET['id'];
            $result = $this->backend->delete_branch($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
        }
        
        
    }
?>
