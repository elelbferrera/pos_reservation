<?php
  class merchant_profile extends template{
		protected $response;
		protected $merchant_id;
		
		public function __construct($meta) {
		    parent::__construct($meta);
		    $this->response = array('success' => FALSE, 'message' => 'Unknown error');
		    $this->check_session();
			$this->merchant_id = $_SESSION['user_info']['reference_id'];
		}
		
		public function update()
		{
			$params = $_POST;
	        $insert_data = array(
	            'id' => $params['txtMerchantId'],
	            'merchant_code' => $params['txtMerchantCode'],
	            'merchant_name' => $params['txtMerchantName'],
	            'business_type_id' => -1, //txtBusinessType
	            'address' => '',
	            'city' => isset($params['txtCity']) ? $params['txtCity'] : "",
	            'state' => isset($params['txtState']) ? $params['txtState'] : "",
	            'zip' => isset($params['txtZipCode']) ? $params['txtZipCode'] : "",
	            'country' => $params['txtCountry'],
	            'email' => $params['txtEmailAddress'],
	            'telephone' => $params['txtPhone'],
	            'mobile' => isset($params['txtMobile']) ? $params['txtMobile'] : "",
	            'fax' => isset($params['txtFax']) ? $params['txtFax'] : "",
	            'last_name' => $params['txtLastName'],
	            'first_name' => $params['txtFirstName'],
	        );
	            
	        $result = $this->backend->update_merchant($insert_data);
	        $response = $this->backend->get_response();
	                                                         
	        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['ResponseCode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['ResponseMessage'];
	        } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Merchant has been updated.",
	                );
	        }  
		}
		
		
		public function index()
		{
			$actions = array("upload_temp", "upload", "upload_crop","update");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }	
			
			$this->backend->get_my_profile("merchants", $this->merchant_id); //
            $info = $this->backend->get_response();
            $info = $info['ResponseMessage'];
			
			$countries = $this->backend->get_countries();
        	$this->view->assign('countries', $countries); 
			
			$this->view->assign('my_info', $info);
			
		}
		
		private function upload()
		{
			if(!file_exists("temp_images/"))
			{
				mkdir("temp_images/", 7775);
			}
			$sTempFileName ="";
			if ($_FILES) {

            	// if no errors and size less than 250kb
            	if (! $_FILES['image_file']['error'] && $_FILES['image_file']['size'] < 250 * 1024) {
                	if (is_uploaded_file($_FILES['image_file']['tmp_name'])) {

		                $aSize = getimagesize($_FILES['image_file']['tmp_name']); // try to obtain image info
		                if (!$aSize) {
		                    @unlink($sTempFileName);
		                    return;
		                }
		
		                // check for image type
		                switch($aSize[2]) {
		                    case IMAGETYPE_JPEG:
		                        $sExt = '.jpg';
		                        break;
		                    case IMAGETYPE_PNG:
		                        $sExt = '.png';
		                        break;
		                    default:
		                        @unlink($sTempFileName);
		                        return;
		                }
	                    
	                    $sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
	
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['image_file']['tmp_name'], $sTempFileName);

                    
	                }
            	}
        	}
			
		    $this->response = array(
                'data'=> array(),
                'success' => true,
                'message' => $sTempFileName,
            );
		
			// print_r($_FILES);
			// die();
		}
  
  		private function upload_crop()
		{
			$sTempFileName = "{$_POST['file_name']}";

			// change file permission to 644
            //@chmod($sTempFileName, 0644);
            //return file_exists($sTempFileName) ;
			//die();
            
	 		$iWidth = $iHeight = 200; // desired image result dimensions
	        $iJpgQuality = 90;
            if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                $aSize = getimagesize($sTempFileName); // try to obtain image info
                if (!$aSize) {
                    @unlink($sTempFileName);
                    return;
                }

                // check for image type
                switch($aSize[2]) {
                    case IMAGETYPE_JPEG:
                        $sExt = '.jpg';
                        // create a new image from file 
                        $vImg = @imagecreatefromjpeg($sTempFileName);
                        break;
                    case IMAGETYPE_PNG:
                        $sExt = '.png';

                        // create a new image from file 
                        $vImg = @imagecreatefrompng($sTempFileName);
                        break;
                    default:
                        @unlink($sTempFileName);
                        return;
                }

                // create a new true color image
                $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                // copy and resize part of an image with resampling
                imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                // define a result image filename
                $sResultFileName = "images/".md5(time().rand())."{$sExt}"; //$sTempFileName . $sExt;
               
              $params = array(
              	'merchant_id' => $this->merchant_id,
              	'merchant_logo' => $sResultFileName,
			  );
                
              $result = $this->backend->update_merchant_logo($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
		           // output image to file
		            
		            imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
		            @unlink($sTempFileName);
					$this->response = array(
		                'data'=> array(),
		                'success' => true,
		                'message' => $sResultFileName,
		            );
	          }
            }
			else{
				$this->response = array(
	                'data'=> array(),
	                'success' => false,
	                'message' => "Unable to crop image",
	            );			
			}

		}
  }
 ?>