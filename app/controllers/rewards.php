<?php
   class rewards extends template {
      	protected $response;
  		protected $merchant_id;
		
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			$this->merchant_id = $_SESSION['user_info']['reference_id'];
        }
		
		public function management()
		{
			$actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
            $this->backend->get_rewards($this->merchant_id); //
            $rewards = $this->backend->get_response();
            $rewards = $rewards['ResponseMessage'];
            
            $this->view->assign('rewards', $rewards);   
		}
		
		//merchant_id, reward_code, reward_name, start_date,
		//end_date, is_expire_on_day, number_of_days, is_expire_on_create_date, start_date_on_day
		//point, point_amount_conversion, amount, amount_point_conversion, goal_point, automatically_claim_reward
		private function register()
		{
			$params = array(
				'merchant_id' => $this->merchant_id,
				'reward_code' => $_POST['txtRewardCode'],
				'reward_name' => $_POST['txtRewardName'],
				//'start_date' => $_POST['txtStartDate'],
				'end_date' => $_POST['txtEndDate'],
				
				//'number_of_days' => $_POST['txtNoOfDays'],
				
				//'start_date_on_day' => $_POST['txtSpecifiedStartDate'],
				'amount' => $_POST['txtAmount1'],
				'amount_point_conversion' => $_POST['txtPoint1'],
				'point' => $_POST['txtPoint2'],
				'point_amount_conversion' => $_POST['txtAmount2'],
				'goal_point' => $_POST['txtGoalPoint'],
				//'automatically_claim_reward' =>  isset($_POST['chkRewardClaimAuto']) ? true : false,
				//'day_of_month' => $_POST['txtDayOfMonth'],
				//'is_expire_on_day' => isset($_POST['chkNoOfDay']) ? true : false,
				//'is_expire_on_create_date' => isset($_POST['chkSpecifyStartDate']) ? true : false,
				'description' => $_POST['txtDescription']
			);
			
			  $result = $this->backend->create_rewards($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "New Rewards has been registered.",
	                );
	          }
		}
		private function update()
		{
			 	$params = array(
			 		'id' => $_POST['txtRewardId'],
					'merchant_id' => $_POST['txtMerchantId'],
					'reward_code' => $_POST['txtRewardCode'],
					'reward_name' => $_POST['txtRewardName'],

					// 'number_of_days' => $_POST['txtNoOfDays'],
					
					// 'start_date_on_day' => $_POST['txtSpecifiedStartDate'] =="" ?  null : $_POST['txtSpecifiedStartDate'],
					'amount' => $_POST['txtAmount1'],
					'amount_point_conversion' => $_POST['txtPoint1'],
					'point' => $_POST['txtPoint2'],
					'point_amount_conversion' => $_POST['txtAmount2'],
					'goal_point' => $_POST['txtGoalPoint'],
					// 'automatically_claim_reward' =>  isset($_POST['chkRewardClaimAuto']) ? 1 : 0,
					// 'day_of_month' => $_POST['txtDayOfMonth'],
					// 'is_expire_on_day' => isset($_POST['chkNoOfDay']) ? 1 : 0,
					// 'is_expire_on_create_date' => isset($_POST['chkSpecifyStartDate']) ? 1 : 0,
					// 'start_date' => $_POST['txtStartDate'] =="" ? null : $_POST['txtStartDate'],
					'end_date' => $_POST['txtEndDate'] =="" ? null : $_POST['txtEndDate'],
					'description' => $_POST['txtDescription']
					
				);
							
			  $result = $this->backend->update_rewards($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Rewards has been updated.",
	                );
	          }
			  
		}
		private function delete()
		{
			$id = $_GET['id'];
            $result = $this->backend->delete_rewards($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
		}
   }
   
 ?>