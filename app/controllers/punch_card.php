<?php
    class punch_card extends template {
      	protected $response;
  		protected $merchant_id;
		
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
							
			$this->merchant_id = $_SESSION['user_info']['reference_id'];
        }
		
		public function management()
		{
			$actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
			
			
			
            $this->backend->get_list_punch_card($this->merchant_id); //
            $pcs = $this->backend->get_response();
            $pcs = $pcs['ResponseMessage'];
            
			// print_r(count($pcs));
			// die();
			
			// print_r($pcs);
			// die();
			$per_pcs = array();
			$details = "";

			foreach($pcs as $p)
			{
				$details = "";
				foreach($p['details'] as $d)
				{
					//$details[] = $d['detail_description'];
					//$details[] = $d['hole_no'];
					//$details[] = '<input type="button" value="Delete" onclick="deleteRow(this)"/>';
					$details[] = array($d['detail_description'],
					$d['hole_no'],
					'<input type="button" value="Delete" onclick="deleteRow(this)"/>'
					);
				}		

				
				 // print_r($details);
				 // die();
				// die();
				//$per_pcs['details'] = json_encode($details);
				$per_pcs[] = array(
					'punch_id' => $p['punch_id'],
					'merchant_id' => $p['merchant_id'],
					'merchant_id' => $p['merchant_id'],
					'punch_code' => $p['punch_code'],
					'description' => $p['description'],
					'number_of_punch' => $p['number_of_punch'],
					'cost' => $p['cost'],
					'pin' => $p['pin'],
					'start_date' => $p['start_date'],
					'end_date' => $p['end_date'],
					'create_date' => $p['create_date'],
					'status' => $p['status'],
					'details' => json_encode($details),
				);	

						
			}
			

						// print_r($per_pcs);
			// die();
            $this->view->assign('pcs', $per_pcs);   
		}
		
		private function register()
		{
			$this->response['success'] = false;
	        
			$punch_detail = substr($_POST['txtPunchCardDetail'], 0, strlen($_POST['txtPunchCardDetail']) - 1);
			


			$data = explode("|", $punch_detail);
			
			$details ="";
			foreach($data as $d)
			{
				$r = explode("~", $d);
				$details[] = $r;
			}
			//$this->response['message'] = serialize($details);
			//print_r($_POST['txtPunchCardDetail']);
			// die();
			
			$params = array(
				'merchant_id' => $this->merchant_id,
				'start_date' => $_POST['txtStartDate'],
				'end_date' => $_POST['txtEndDate'],
				'description' => $_POST['txtDescription'],
				'cost' => $_POST['txtCost'],
				'punch_card_details' => serialize($details),
				'number_of_holes' => $_POST['txtNumberOfPunch'],
				'pin' => $_POST['txtPin'],
			);
			
			  $result = $this->backend->create_punch_card($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "New Punch Card has been registered.",
	                );
	          }
		}
		private function update()
		{
			$punch_detail = substr($_POST['txtPunchCardDetail'], 0, strlen($_POST['txtPunchCardDetail']) - 1);
						
						
			if($_POST['txtPunchCardDetail'] !="")
			{
				$data = explode("|", $punch_detail);
				
				$details ="";
				foreach($data as $d)
				{
					$r = explode("~", $d);
					$details[] = $r;
				}
			}else{
				$details="";
			}
			//$this->response['message'] = serialize($details);
			//print_r($_POST['txtPunchCardDetail']);
			// die();
			
			$params = array(
				'punch_card_id' => $_POST['txtPunchCardId'],
				'merchant_id' => $this->merchant_id,
				'start_date' => $_POST['txtStartDate'],
				'end_date' => $_POST['txtEndDate'],
				'description' => $_POST['txtDescription'],
				'cost' => $_POST['txtCost'],
				'punch_card_details' => serialize($details),
				'number_of_holes' => $_POST['txtNumberOfPunch'],
				'pin' => $_POST['txtPin'],
			);
			
			  $result = $this->backend->update_punch_card($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Punch Card has been updated.",
	                );
	          }
		}
		
		private function delete()
		{
			$id = $_GET['id'];
            $result = $this->backend->delete_punch_card($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
		}
	}		
?>