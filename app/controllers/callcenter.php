<?php
class callcenter extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
    
    public function management() {
        
        $actions = array('register_callcanter','get_callcanter','edit_callcanter','get_callcenter_byid','delete_callcenter');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $call_center_list = $this->backend->get_callcenter();
        $response = $this->backend->get_response();
        $call_center_list = $response['ResponseMessage'];
        $out_call_center_list = array(); 
        foreach ($call_center_list as $row) {
            $out_call_center_list[] = array(
                'id' => sprintf('%06d',$row['id']),
                'center_code' => $row['center_code'],
                'name' =>  $row['name'],
                'ftp_site' =>$row['ftp_site'],
                'ftp_login' =>$row['ftp_login'],
                'ftp_password' =>$row['ftp_password'],
                'status' =>$row['status'],
                'button' =>'<input type="image" onclick="EditCallCenter('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
             
           $this->view->assign('call_center_list', $out_call_center_list);
    }
     
     
    protected function delete_callcenter()
    {
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_callcenter($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
           
            $call_center_list = $this->backend->get_callcenter();
            $response = $this->backend->get_response();
            $call_center_list = $response['ResponseMessage'];
            $out_call_center_list = array(); 
            foreach ($call_center_list as $row) {
            $out_call_center_list[] = array(
                sprintf('%06d',$row['id']),
                $row['center_code'],
                $row['name'],
                $row['ftp_site'],
                $row['ftp_login'],
                $row['ftp_password'],
                $row['status'],
                '<input type="image" onclick="EditCallCenter('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
            }

            $this->response = array(
                'data'=>$out_call_center_list,
                'success' => true,
                'message' => "Call Center has been deleted.",
            );
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Call Center ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'Call Centers',$datetime,$_SESSION['username'],'');
 
        }  
    }
     
    protected function register_callcanter()
    {
        $result = $this->backend->register_callcanter($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $call_center_list = $this->backend->get_callcenter();
        $response = $this->backend->get_response();
        $call_center_list = $response['ResponseMessage'];
        $out_call_center_list = array(); 
        foreach ($call_center_list as $row) {
            $out_call_center_list[] = array(
                sprintf('%06d',$row['id']),
                $row['center_code'],
                $row['name'],
                $row['ftp_site'],
                $row['ftp_login'],
                $row['ftp_password'],
                $row['status'],
                '<input type="image" onclick="EditCallCenter('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }

            $this->response = array(
                'data'=>$out_call_center_list,
                'success' => true,
                'message' => "New Call Canter has been added.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Registered new call center name ' .$_POST['txtCallCenterName'] ,'Call Centers',$datetime,$_SESSION['username'],'');
 
        }  
    }
    
    protected function edit_callcanter()
    {
       
        $result = $this->backend->edit_callcanter($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $call_center_list = $this->backend->get_callcenter();
        $response = $this->backend->get_response();
        $call_center_list = $response['ResponseMessage'];
        $out_call_center_list = array(); 
        foreach ($call_center_list as $row) {
            $out_call_center_list[] = array(
                sprintf('%06d',$row['id']),
                $row['center_code'],
                $row['name'],
                $row['ftp_site'],
                $row['ftp_login'],
                $row['ftp_password'],
                $row['status'],
                '<input type="image" onclick="EditCallCenter('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteCallCenter('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }

            $this->response = array(
                'data'=>$out_call_center_list,
                'success' => true,
                'message' => "Call Center has been updated.",
            ); 
            $id =$_POST['txtID'];
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Call Center ID# '. sprintf('%06d',$id) . ' was successfully updated' ,'Call Centers',$datetime,$_SESSION['username'],'');
 
        }  
    }
    
    protected function get_callcenter_byid()
    {
    $id= $_GET['id'];
        
        $params = array(
        'id' => $id,
        );
        $result = $this->backend->get_callcenter_byid($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
            foreach ($response['ResponseMessage'] as $key => $val) {
                $search_result[] = array(
                    'formated_id'=>sprintf('%06d',$val['id']),
                    'center_code' =>$val['center_code'],
                    'name' =>  $val['name'],
                    'ftp_site' =>$val['ftp_site'],
                    'ftp_login' =>$this->backend->hideit->decrypt(sha1(md5($this->backend->salt)), $val['ftp_login'], sha1(md5($this->backend->salt))),
                    'ftp_password' =>$this->backend->hideit->decrypt(sha1(md5($this->backend->salt)), $val['ftp_password'], sha1(md5($this->backend->salt))),
                    'status' =>$val['status'],
            );
            }
           
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
            );
        }          
   }

    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
