<?php
class contact extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
       public function management() {
        $actions = array('register_contact','get_contact','get_contact_byid','edit_contact','delete_contact');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $contact_list = $this->backend->get_contact();
        $response = $this->backend->get_response();
        $contact_list = $response['ResponseMessage'];
        $out_contact_list = array(); 
        foreach ($contact_list as $row) {
            $out_contact_list[] = array(
                'id'=> sprintf('%06d',$row['id']),
                'first_name'=>$row['first_name'],
                'last_name'=>$row['last_name'],
                'city'=> $row['city'],
                'state'=>$row['state'],
                'phone'=>$row['phone'],
                'status'=>$row['status'],
                'button'=> '<input type="image" onclick="EditContact('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditContact('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteContact('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
        $this->view->assign('contact_list', $out_contact_list);
    }
     
    
    protected function delete_contact()
    {
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_contact($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

            $contact_list = $this->backend->get_contact();
            $response = $this->backend->get_response();
            $contact_list = $response['ResponseMessage'];
            $out_contact_list = array(); 
            foreach ($contact_list as $row) {
                $out_contact_list[] = array(
                    sprintf('%06d',$row['id']),
                    $row['first_name'],
                    $row['last_name'],
                    $row['city'],
                    $row['state'],
                    $row['phone'],
                    $row['status'],
                    '<input type="image" onclick="EditContact('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                    <input type="image" onclick="EditContact('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                    <input type="image" onclick="DeleteContact('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
                );
            }
            $this->response = array(
                'data'=>$out_contact_list,
                'success' => true,
                'message' => "Contact has been deleted.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Contact ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'Contacts',$datetime,$_SESSION['username'],'');
 
        }  
    }
    
     
    
    protected function edit_contact()
    {
        $result = $this->backend->edit_contact($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

            $contact_list = $this->backend->get_contact();
            $response = $this->backend->get_response();
            $contact_list = $response['ResponseMessage'];
            $out_contact_list = array(); 
            foreach ($contact_list as $row) {
                $out_contact_list[] = array(
                    sprintf('%06d',$row['id']),
                    $row['first_name'],
                    $row['last_name'],
                    $row['city'],
                    $row['state'],
                    $row['phone'],
                    $row['status'],
                    '<input type="image" onclick="EditContact('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                    <input type="image" onclick="EditContact('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                    <input type="image" onclick="DeleteContact('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
                );
            }

            $this->response = array(
                'data'=>$out_contact_list,
                'success' => true,
                'message' => "Contact has been updated.",
            );
            $id =$_POST['txtID'];
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Contact ID# '. sprintf('%06d',$id) . ' was successfully updated.' ,'Contacts',$datetime,$_SESSION['username'],'');
  
        }  
    }
    
    
    
    protected function get_contact_byid()
    {
    $id= $_GET['id'];
               
        $params = array(
        'id' => $id,
        );
        $result = $this->backend->get_contact_byid($params);
        $response = $this->backend->get_response();

        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
           
            foreach ($response['ResponseMessage'] as $key => $val) {
                $search_result[] = array(
                        'formated_id'=>sprintf('%06d',$val['id']),
                        'first_name'=>$val['first_name'], 
                        'last_name' =>$val['last_name'],
                        'city'=>$val['city'],
                        'state'=>$val['state'],
                        'zip'=>$val['zip'],  
                        'phone'=>$val['phone'],
                        'ext'=>$val['ext'],
                        'fax'=>$val['fax'],
                        'email'=>$val['email'],
                        'login_id'=>$val['login_id'],
                        'password' =>$this->backend->hideit->decrypt(sha1(md5($this->backend->salt)), $val['password'], sha1(md5($this->backend->salt))),
                        'notes'=>$val['notes'],
            );
            }
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
            );
        }          
   }
    
     protected function register_contact()
    {
        $result = $this->backend->register_contact($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

            
        $contact_list = $this->backend->get_contact();
        $response = $this->backend->get_response();
        $contact_list = $response['ResponseMessage'];
        $out_contact_list = array(); 
        foreach ($contact_list as $row) {
            $out_contact_list[] = array(
                 sprintf('%06d',$row['id']),
                $row['first_name'],
                $row['last_name'],
                $row['city'],
                $row['state'],
                $row['phone'],
                $row['status'],
                '<input type="image" onclick="EditContact('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditContact('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteContact('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
           
            $this->response = array(
                'data'=>$out_contact_list,
                'success' => true,
                'message' => "New Contact has been added.",
            );
             $datetime=$this->backend->get_server_date();
             $this->backend->save_to_action_logs('Registered new contact ' .$_POST['txtFirstName'] . ' '.$_POST['txtLastName'] ,'Contacts',$datetime,$_SESSION['username'],'');
  
        }  
    }
    

    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
