<?php
class customers extends template {
    protected $response;
    protected $kyc_upload_path;
    protected $salary_upload_path;
    protected $salary_upload_path_final;
    protected $salt;
    //protected $allowed_extensions = array('jpeg','jpg','gif','png','bmp');
    //protected $allowed_types = array('image/jpeg','image/gif','image/png','image/bmp');
    protected $allowed_extensions = array('jpeg','jpg','gif','png','bmp','pdf');
    protected $allowed_types = array('image/jpeg','image/gif','image/png','image/bmp','application/pdf');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->kyc_upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'kyc/temp/';
        $this->salary_upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'salary/temp/';
        $this->salary_upload_path_final = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'salary/unverified/';
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function get_document_label()
    {          
        $id =  $_POST['id'];
        $result = $this->backend->get_document_label($id);
        $this->response = array(
        'success' => true,
        'message' =>$result);
    }
    
    
    public function update_loan_status()
    {
        if (!(isset($_POST['customer_id'],$_POST['loan_id']))) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a customer id and loan id';
        }
        else{                       
            $this->layout = 'json';
            //$result = $this->backend->check_salary_validation($_POST['customer_id']);
            
            /*if($result)
            {*/
                $result = $this->backend->check_kyc_validation($_POST['customer_id']);
                if(count($result)>=5)
                {
                    $params= array(
                        'user' => $_SESSION['username'],
                        'loan_id' =>$_POST['loan_id'],
                        'status' => $_POST['transaction_status_approval'],
                        'customer_id' => $_POST['customer_id'],
                        'reason' => $_POST['remarks'],  
                    );             
                    $result = $this->backend->update_loan_status($params);
                    $response = $this->backend->get_response();
                    
                    if (!(isset($response['ResponseCode'], $response['ResponseMessage']) && $response['ResponseCode'] == '0000')) {
                        $this->response['message'] = $response['ResponseMessage'];
                    } else {
                        $this->response = array(
                            'success' => true,
                            'message' => $response['ResponseMessage'],
                        );
                           //PP 04/11/2014 Added for Logs    
                        $actionlog_msg='';
                        $action_status=$params['status'];
                        if($action_status==1)
                        {$actionlog_msg='Validated loan application';}
                        else if($action_status==2)
                        {$actionlog_msg='Approved loan application';}
                        else if($action_status<0)
                        {$actionlog_msg='Declined loan application';}
                        //PP 04/11/2014 Added for Logs
                        $datetime=$this->backend->get_server_date();
                        $this->backend->save_to_action_logs($actionlog_msg.' Customerid# '.$_POST['customer_id'],'Loan Approval',$datetime,$_SESSION['username'],'');
                         print_r(json_encode($this->response,true));
                         die(); 
                        
                    }        
                }else
                {
                   $this->response = array(
                    'success' => false,
                    'message' => " Loan status could not be change. Customer document has not been uploaded,\n \t\t\t\t\texpired or expiration not been assigned.",
                   );  
                }
                
                
                
          /*  }else{
                $this->response = array(
                    'success' => false,
                    'message' => "Loan status could not be change. Salary document has not been uploaded or not been verfied.",
                ); 
                
            } */
        }                                                         
    }   
    
    
    
    //JR-2014/03/24
   /* public function get_sub_agents_by_agentId()
    {
        $id = $_POST['id'];
        $result = $this->backend->get_sub_agents_by_agentId($id);
        $this->view->assign('sub_agents',$result);
        $this->response = array(
        'success' => true,
        'message' =>$result);
    }
    */
    //JR-2014/03/24
    protected function get_sub_agents_by_agentId(){
         $id = $_GET['id'];
         $sub_agent_id=$_GET['sub_agent_id'];
         $sub_agent = $this->backend->get_sub_agents_by_agentId($id,'',$sub_agent_id);
         
         if (count($sub_agent) > 0){
             $option = "";
             foreach($sub_agent as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
                
             }
             //print_r($option);
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             $this->response = array(
                'success'   => false,
                'data'      => "Error"
            );
         }
         
    }
    
    public function registration() {
        //JR-2014/03/24  
        $actions = array('get_sub_agents_by_agentId');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $countries = $this->backend->get_countries();
        $this->view->assign('countries', $countries);
        $employers = $this->backend->get_employers();
        $this->view->assign('employers', $employers);
        $documents = $this->backend->get_document_types();
        $documents = array('' => '-SELECT-') + $documents;
        $this->view->assign('documents', $documents);
        $titles = array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Miss' => 'Miss', 'Dr' => 'Dr');
        $this->view->assign('titles', $titles);
        $genders = array('Male' => ' Male', 'Female' => ' Female');
        $this->view->assign('genders', $genders);
        $languages = array('English' => 'English');
        $this->view->assign('languages', $languages);
        $methods_of_contact = array('sms' => ' SMS', 'email' => ' E-Mail');
        $this->view->assign('methods_of_contact', $methods_of_contact);
        
        
        include("mdetect/mdetect.php");
        $uagent_obj = new uagent_info();
        if (($uagent_obj->DetectIos() == $uagent_obj->true) ||($uagent_obj->DetectAndroid() == $uagent_obj->true) || ($uagent_obj->DetectWindowsPhone() == $uagent_obj->true) || ($uagent_obj->DetectBlackBerry() == $uagent_obj->true) || ($uagent_obj->DetectSymbianOS() == $uagent_obj->true)|| ($uagent_obj->DetectPalmOS() == $uagent_obj->true)|| ($uagent_obj->DetectTierOtherPhones() == $uagent_obj->true)) {          $capture_method = array(1 => ' Camera', 2 => ' Scanner');
           $this->view->assign('method_of_c', 'sms');
         //print_r('mobile');
         //die();
        }else{
            $capture_method = array(1 => ' Digital Document', 2 => ' Scanner');
            $this->view->assign('method_of_c', 'email');
         //print_r('pc');
         //die();   
        }
        
        //$capture_method = array(1 => ' Camera', 2 => ' Scanner', 3 => ' Other');
        $this->view->assign('capture_method', $capture_method);
        $yes_or_no = array(1 => ' Yes', 2 => ' No');
        $this->view->assign('yes_or_no', $yes_or_no);
        
        //$loan_terms = $this->backend->get_loan_terms();
        //$this->view->assign('loan_terms', $loan_terms);
        $loan_terms = $this->backend->get_loan_terms_by_id(1,'Pair');
        $this->view->assign('loan_terms', $loan_terms);
        
        $loan_request_amounts = $this->backend->get_loan_request_amounts();
        $loan_request_amounts = array('' => '-SELECT-') + $loan_request_amounts;
        $this->view->assign('loan_request_amounts', $loan_request_amounts);
        
        $param = array('username' => $_SESSION['username']);
        $this->backend->get_user_details($param);
        $user_info = $this->backend->get_response();
        
        $this->view->assign('usertype', $user_info['ResponseMessage']['user_type']);
        
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        $this->view->assign('user_type_description', $usertype);   
        
        
        //JR-2014/03/20 load customer payment cycle in drop down
        $pay_cycle = $this->backend->get_pay_cycle();
        $this->view->assign('pay_cycle', $pay_cycle);
        //JR-2014/03/24 getId if agent
        if(in_array($user_info['ResponseMessage']['user_type'], array(3,4)))
        {
            $tmp_agent_id = $this->backend->get_agent_id_in_user($_SESSION['username']);
            $agent_id = $this->backend->get_agent_by_type($tmp_agent_id, 'Agent');
        }else{
          $agent_id=-1;  
        }
        //JR-2014/03/21 load agents
        $agents = $this->backend->get_agents($agent_id);
        $this->view->assign('agents', $agents);
        
        //JR-2014/03/24 sub_agent
        $sub_agents= $this->backend->get_sub_agents_by_agentId($agent_id,'pair',0);
        $this->view->assign('sub_agents', -1);
        
        
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            //JR-2014/03/19 change nsi to tin
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['cpno'], $_POST['street'],$_POST['tin']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
                //$this->response['message'] = 'test';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['cpno']) != '' && 
            trim($_POST['street']) != '' && trim($_POST['tin']) != '')) {
            //trim($_POST['street']) != '' && trim($_POST['nsi']) != '' && trim($_POST['card_no'])!='' )) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';                
            } elseif (!(is_numeric($_POST['cpno']) && strlen($_POST['cpno']) > 5)) {
                $this->response['message'] = 'The mobile number should be numeric.';
            }
//             elseif (isset($_POST['credit_limit']) && trim($_POST['credit_limit']) != '' && !(is_numeric($_POST['credit_limit']) && $_POST['credit_limit']) > 0) {
//                $this->response['message'] = 'The loan amount limit should be numeric.';
//            } 
            elseif(!(isset($_POST['methods'])))
            {
                $this->response['message'] = 'Select one preferred of contact.';
            }elseif(($_POST['email']=='' && in_array('email', $_POST['methods'])))
            {
                //$this->response['message'] = 'Email preferred contact cannot be checked. Must enter email address first.';
                $this->response['message'] = 'If you would like for us to communicate with you via email, please hit the Back button and enter your email address';
            }elseif(($_POST['title']=='Mr' && $_POST['gender'] =="Female"))
            {
                $this->response['message'] = 'Title Mr cannot be associated with Female gender.';
            }
            elseif((($_POST['title']=='Ms' || $_POST['title']=='Miss' || $_POST['title']=='Mrs')   && ($_POST['gender'] =="Male") ))
            {
                $this->response['message'] = 'Title Ms/Miss/Mrs cannot be associated with Male gender.';
            }
            else {
                 
                           //echo (isset($_POST['methods']) && in_array('email', $_POST['methods']))? 1: 0;
                           //echo (isset($_POST['methods']) && in_array('sms', $_POST['methods']))? 1: 0;
                            //die();         
                       //$user_Id = $this->backend->get_user_id($_POST['user_name']);
                        
                        //$this->response['message'] = $agent_Id;
                        
                         if (!isset($_POST['isIndividual']))
                         { $isIndividual= false;}
                         else
                         { $isIndividual= $_POST['isIndividual'];}
                        
                       
                          if($isIndividual)
                            {
                                $agent_id=0;
                                $individual = 1;
                            }
                            else
                            {
                                $agent_id= $_POST['agent'];
                                $individual = 0;
                            }
                        
                        $reg_params = array(
                            'fname' => $_POST['first_name'],
                            'lname' => $_POST['last_name'],
                            'mname' => $_POST['middle_name'],
                            'title' => $_POST['title'],
                            'gender' => $_POST['gender'],
                            'linked_cardnumber' => $_POST['card_no'],
                            'mobile_number' =>  $_POST['cpno'],//$_POST['cpno_prefix'].$_POST['cpno'],
                            'email_address' => $_POST['email'],
                            'language' => $_POST['language'],
                            'dob' => $_POST['dob'],
                            'street' => $_POST['street'],
                            'city' => $_POST['city'],
                            'country' => $_POST['country'],
                            'pobox' => $_POST['pobox'],
                            'telephone1' => $_POST['tel2'], //$_POST['tel2_prefix'].$_POST['tel2'],
                            'telephone2' => $_POST['tel3'],//$_POST['tel3_prefix'].$_POST['tel3'],
                            'national_insurance_no' => $_POST['tin'],
                            'employer_id' => $_POST['employer'],
                            'contact_via_email' => (isset($_POST['methods']) && in_array('email', $_POST['methods']))? 1: 0,
                            'contact_via_sms' => (isset($_POST['methods']) && in_array('sms', $_POST['methods']))? 1: 0,
                            'created_by' => $_SESSION['username'],
                            'credit_limit' => $_POST['credit_limit'],
                            
                            //LC-12/12/2013
                            'employer_name' => $_POST['employer_name'],
        //                    'first_cidoc' => $_POST['first_cidoc'],
        //                    'second_cidoc' => $_POST['second_cidoc'],
        //                    'first_cidno' => $_POST['first_cidno'],
        //                    'second_cidno' => $_POST['second_cidno']
                            
                            //JR-2014/03/18 Added new field agentId and userId
                            //'agent_id' => $agent_Id ,                  
                            //,'users_id' => $user_Id
                            //JR-2014/03/20 Added ZIP
                            'zip' => $_POST['zip'],
                            //JR-2014/03/20 Added Address Line 2
                            'address_line_2' => $_POST['address_sec'],
                            //JR-2014/03/24
                           // 'agent_id' => $_POST['agent'],
                            'agent_id' => $agent_id,
                            'sub_agent_id' => $_POST['sub_agent'],
                            
                            //JR-2014/04/22
                            'pin' => $_POST['cus_pin'],
                            //PP 05/09/2014
                            'is_individual'=>$individual,
                         );
                         
//                         echo $reg_params['contact_via_email'];
//                         echo $reg_params['contact_via_sms'];
//                         die();
                        //LC-09/11/2013
                        //$response = lib::getWsResponse($this->meta['remote_api'], 'merchant_registration', $reg_params); 
                        $result = $this->backend->register_customer($reg_params);
                        $response = $this->backend->get_response();
                        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                            $this->response['message'] = 'System error, unable to connect to database';
                        } elseif (!($response['ResponseCode'] == '0000')) {
                            $this->response['message'] = $response['ResponseMessage'];
                        } else {
                            
                            
                            $datetime=$this->backend->get_server_date();
                            $this->backend->save_to_action_logs('Added new customer '.$reg_params['fname'].' '.$reg_params['lname'],'Customer Registration',$datetime,$_SESSION['username'],'');
                            
                            //JR-2014/04/13 
                            if($_POST['employer_name']!=''){
                                $this->backend->send_notification_employer($reg_params);
                                $this->backend->save_to_action_logs('Requesting registration of employer '.$reg_params['employer_name'],'Customer Registration',$datetime,$_SESSION['username'],'');
                            }
                            
                            //JR-2014/03/18 Insert to User then get Id to add in Customer
                            $users_type_id = $this->backend->get_user_types_ByDesc('Customer');
                            //$this->response['message'] = $users_type_id;
                            $customers_id = $this->backend->get_customers_id_by_mobile($_POST['cpno'] );
                            //$this->response['message'] = $customers_id;
                            
                            $reg_params_user = array(
                                'fname' => $_POST['first_name'],
                                'lname' => $_POST['last_name'],
                                'email_address' => $_POST['email'],
                                'mobile_number' =>  $_POST['cpno'],
                                'address' => $_POST['street'].' '. $_POST['city'].' '.$_POST['country'],
								'username' => $_POST['cpno'],//$_POST['user_name'],
                                'user_type' => $users_type_id,
                                'created_by' => $_SESSION['username'],
                                'customers_id' => $customers_id,
                                'agent_id' => -1,
                                'isEmail' => $reg_params['contact_via_email'],
                                'isSMS' =>  $reg_params['contact_via_sms'],
                                
                             );
                             
                            $result = $this->backend->register_user($reg_params_user);
                            
                        $suffix = md5($response['ResponseMessage']['customer_id'] . $this->salt);
                        $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'] . '. You are almost done finalizing your loan request! To complete the process we will ask for you to upload your personal documents.',
                        'cust_id' => $response['ResponseMessage']['customer_id'],
                        'suffix' => $suffix,
                        );
                        }  
                
            }
        }
    }
    
    public function homepage() {
       $actions = array('load_agents','load_employers','update_customer_home');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $titles = array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Miss' => 'Miss', 'Dr' => 'Dr');
        $this->view->assign('titles', $titles);
        $genders = array('Male' => ' Male', 'Female' => ' Female');
        $this->view->assign('genders', $genders);  
        //$customer_id = $this->backend->get_customers_id_by_mobile($_SESSION['username']);
        $customer_id = $this->backend->get_field_value_from_table_by_field_name('users','customers_id','username',$_SESSION['username']);
        //print_r($_SESSION['username']);
        //die();
        $this->backend->get_customers_by_id($customer_id);
        $response = $this->backend->get_response();        
        $customers = $response['ResponseMessage'];
        foreach ($customers as $row) 
            $agent_name = $this->backend->get_agent_name_by_id($row['agent_id']);
            $sub_agent_name = $this->backend->get_sub_agent_name_by_id($row['sub_agent_id']);
            $employer =  $this->backend->get_employer_name_by_id($row['employer_id']);
        {$trimmed = array(
                'id' => $row['id'],
                'fname'            => $row['fname'],
                'lname'     => $row['lname'],
                'mname'      => $row['mname'],
                'mobile_number'    => $row['mobile_number'],
                'email_address'     => $row['email_address'],
                'dob'     => $row['dob'],
                'city'            => $row['city'],
                'street'            => $row['street'],
                'telephone1' => $row['telephone1'],
                'telephone2' => $row['telephone2'],
                'national_insurance_no' => $row['national_insurance_no'],
                'employer_id' => $row['employer_id'],
                'employer_name' => $row['employer_name'],
                'agent_id' =>  $row['agent_id'],
                'pin' => $row['pin'],             
                'address_line_2' => $row['address_line_2'],
                'sub_agent_id' => $row['sub_agent_id'],   
                'agent_name' => $agent_name,
                'sub_agent_name' => $sub_agent_name,
                'employer' => $employer,
                'is_individual' => $row['is_individual']       
            );
            $trimmed['customer_details'] = '(' . json_encode($trimmed) . ')';
            $sel_trans[] = $trimmed;
        }
        $this->view->assign('customers_page', $sel_trans);
    }
    
    public function management() {
        //PP 03/18/2014 Added new actions
     
        
        
        
        $actions = array('view_document_image','inital_loan_request', 'make_payment', 'search_merchant','view_merchant','update_ca_limit','apply_loan', 'upload_kyc', 'submit_upload','save_due_date', 'update_card_number', 'get_repayment_amount','upload_salary','get_repayment_amount_cm','upload_salary_manage','submit_upload_salary','get_document_label','get_sub_agents_by_agentId','set_customer_document_status','send_notification_customer','save_contact_info','save_document_dates','load_agents','load_employers','update_customer','get_loan_terms','submit_upload_edit','submit_upload_edit_reupload','get_edit_document','upload_kyc_edit','reload_documents');
        
        
        
        
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        //include("mdetect/mdetect.php");
        $uagent_obj = new uagent_info();
         if (($uagent_obj->DetectIos() == $uagent_obj->true) ||($uagent_obj->DetectAndroid() == $uagent_obj->true) || ($uagent_obj->DetectWindowsPhone() == $uagent_obj->true) || ($uagent_obj->DetectBlackBerry() == $uagent_obj->true) || ($uagent_obj->DetectSymbianOS() == $uagent_obj->true)|| ($uagent_obj->DetectPalmOS() == $uagent_obj->true)|| ($uagent_obj->DetectTierOtherPhones() == $uagent_obj->true)) {          $capture_method = array(1 => ' Camera', 2 => ' Scanner');
         //print_r('mobile');
         //die();
        }else{
            $capture_method = array(1 => ' Digital Document', 2 => ' Scanner');
         //print_r('pc');
         //die();   
        }
        //$capture_method = array(1 => ' Camera', 2 => ' Scanner', 3 => ' Other');
        $this->view->assign('capture_method', $capture_method);
        
        $this->view->assign('upload_docu_no_1','uploadsu_1_1');
        $this->view->assign('upload_docu_no_2','uploadsu_1_2');
        $this->view->assign('upload_docu_no_3','uploadsu_1_3');
        
        $titles = array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Miss' => 'Miss', 'Dr' => 'Dr');
        $this->view->assign('titles', $titles);
        $genders = array('Male' => ' Male', 'Female' => ' Female');
        $this->view->assign('genders', $genders);
        
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        
        $this->view->assign('user_type_description', $usertype);
        
        $customer_id = $this->backend->get_field_value_from_table_by_field_name('users','customers_id','username',$_SESSION['username']);
        $loan_parameter_cm = array(
            'customer_id' => $customer_id,
            'status' => 1,
        );
        $loan_count_cm = $this->backend->get_customer_loan($loan_parameter_cm);
        $this->view->assign('existing_loan_count', $loan_count_cm);
        
        echo ' loan count = ' .  $loan_count_cm;
        die();
        
        $yes_or_no = array(1 => ' Yes', 2 => ' No');
        $this->view->assign('yes_or_no', $yes_or_no);
        
        //$loan_terms = $this->backend->get_loan_terms();
//        $this->view->assign('loan_terms', $loan_terms);
        $loan_terms = $this->backend->get_loan_terms_by_id(1,'Pair'); //default value temporarily for loading
        $this->view->assign('loan_terms', $loan_terms);
        
        $loan_request_amounts = $this->backend->get_loan_request_amounts();
        $loan_request_amounts = array('' => '-SELECT-') + $loan_request_amounts;
        $this->view->assign('loan_request_amounts', $loan_request_amounts);
        
        //JR-2014/03/19 load customer payment cycle in drop down
        $pay_cycle = $this->backend->get_pay_cycle();
        $this->view->assign('pay_cycle', $pay_cycle);
        
        //default: display search form
        
        $documents = $this->backend->get_doctype_for_editing();
        $documents = array('' => '-SELECT-') + $documents;
        $this->view->assign('doc_types', $documents);
        
      
        
        
        
        
    }
    
    //PP 03/24/2014  not yet done
    protected function save_contact_info() {
        if (!($_POST['contact_person']!='')) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a Contact Person';
        } elseif(!($_POST['title']!=''))  
        {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a Title.';
        }
        elseif(!($_POST['tel']!=''))  
        {
            $this->layout = 'json';
            $this->response['message'] = 'Enter a Telephone #.';
        }
        else{
            
/*            $this->layout = 'json';   
            $params = array(
                'loan_id' => $_POST['loan_id'],
                'comments' => $_POST['comment'],
                'user' => $_SESSION['username']
            );
            $result = $this->backend->add_comment($params);
            $response = $this->backend->get_response();        */
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response = array(
                'success' => true,
                'message' => 'Comment has been saved.',
                );
            }
        }
    }
	
	 //PP 03/15/2014
    protected function save_document_dates() {
        if (!($_POST['issuedate']!='')) {
            $this->layout = 'json';
            $this->response['message'] = 'Enter issue date.';
        } elseif(!($_POST['expdate']!=''))  
        {
            $this->layout = 'json';
            $this->response['message'] = 'Enter expiration date.';
        }
       
        else{
            
            $this->layout = 'json'; 
            //echo $_POST['issuedate'] . '----' . $_POST['expdate'];
            $params = array(
                'doc_id' => $_POST['doc_id'],
                'customer_id' => $_POST['customer_id'],
                'issuedate' => $_POST['issuedate'],
                'expdate' => $_POST['expdate'],
                
            );
            
            $result = $this->backend->add_docdates($params);
            $response = $this->backend->get_response();        
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
/*                $this->response = array(
                'success' => true,
                'message' => 'Dates has been saved.',
                );      */
                
              //  $_GET['id'] = $_GET['merchant_card_id'];
                
                $this->view_merchant('Document has been updated.');
            }
        }
    }
    
	
	
    //PP 03/18/2014 Added for Customer Gathered Documents
     protected function set_customer_document_status() {
            $params = array(
                'customer_document_id' => $_GET['id'],
                'verified_by' => $_SESSION['username'],
                'status' => $_GET['status'],
                'doctype' => $_GET['doctype'],
                'customer_id' =>$_GET['merchant_card_id'],
            );   
            $result = $this->backend->set_customer_document_validation_status($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
//               $this->response = array(
//                'success' => true,
//                'message' =>  $response['ResponseMessage'],
//                );
                //JR-2014/04/11
                if ($params['status']==1){
                    $doc_status=' was successfully verified.';    
                }else{
                    $doc_status=' was set to FOR REVISION.';    
                }
                
                if ($params['doctype']=='kyc'){
                    $doc_type='Customer document';    
                }else{
                    $doc_type='Salary document';    
                }
                
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs($doc_type.' Id#'.$params['customer_document_id']. $doc_status,'Customer Management',$datetime,$_SESSION['username'],'');
                $_GET['id'] = $_GET['merchant_card_id'];
                
                $this->view_merchant('Document has been updated.');
               
            }
    }
    //PP 03/20/2014
    protected function send_notification_customer(){
        $params = array(
                'customer_id' => $_GET['id'],
                'verified_by' => $_SESSION['username'],
                'status' => $_GET['status'],
                'doctype' => $_GET['doctype'],   
            );
              
                    
                    $contact = array();
                    $customerid=$params['customer_id'];//$this->backend->get_customer_id_by_doc($params['customer_document_id']);
                    
                    $contact = $this->backend->get_customer_contacts($customerid);
                    $email = $contact['email_address'];
                    $mobile = $contact['mobile_number'];
                    if($params['doctype']==1)
                    {$docs=  $this->backend->get_customerdocfor_revision($customerid);}
                    else  
                    {$docs=$this->backend->check_if_salary_needtorevise($customerid);}
                    $params = array('email_address' => $email,
                                    'mobile_number' => $mobile,
                                    'docs_revised' => $docs,
                                    'customer_id' => $customerid,
                                    'doctype' => $_GET['doctype'],);
                    $result = $this->backend->customer_doc_revision_email($params);
                    $response = $this->backend->get_response();
                    if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                    } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                    } else {
                    //JR-2014/04/11
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Salary document email notification was sent to '.$params['email_address'],'Customer Management',$datetime,$_SESSION['username'],'');
                    $this->response = array(
                                    'success' => true,
                                    'message' =>  $response['ResponseMessage'],
                                    );

                    }
    }
    
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    protected function get_repayment_amount(){
        $_POST = $this->array_map_recursive('trim', $_POST);
        
         if (!(isset($_POST['loan_request_amount'],$_POST['term_request']) && is_numeric($_POST['loan_request_amount']) && is_numeric($_POST['term_request']) && $_POST['loan_request_amount'] > 0 && $_POST['term_request'] > 0)) {
            $this->response['message'] = $_POST;    
            $this->response['success'] = false;
         }else{
             $amount = $this->backend->get_repayment_amount($_POST['term_request'],$_POST['loan_request_amount']);
             $this->response['message'] = $amount;
             $this->response['success'] = true;
         }
    }
    
    protected function get_repayment_amount_cm(){
        $_POST = $this->array_map_recursive('trim', $_POST);
        
         if (!(isset($_POST['loan_request_amount_cm'],$_POST['term_request_cm']) && is_numeric($_POST['loan_request_amount_cm']) && is_numeric($_POST['term_request_cm']) && $_POST['loan_request_amount_cm'] > 0 && $_POST['term_request_cm'] > 0)) {
            $this->response['message'] = $_POST;    
            $this->response['success'] = false;
         }else{
             $amount = $this->backend->get_repayment_amount($_POST['term_request_cm'],$_POST['loan_request_amount_cm']);
             $this->response['message'] = $amount;
             $this->response['success'] = true;
         }
    }
    
    
    
    protected function inital_loan_request() {
        $_POST = $this->array_map_recursive('trim', $_POST);
        if (!(isset($_POST['cust_id']) && is_numeric($_POST['cust_id']) && $_POST['cust_id'] > 0) ) {
            $this->response['message'] = 'Invalid customer ID';
        } elseif (!(isset($_POST['loan_request']) && is_numeric($_POST['loan_request']) && $_POST['loan_request'] > 0) ) {
            $this->response['message'] = 'Invalid loan request';
        } elseif (!(isset($_POST['salary_deduction']) && is_numeric($_POST['salary_deduction']) && $_POST['salary_deduction'] > 0) ) {
            $this->response['message'] = 'Please specify if salary deduction or not';
        } else {
            $params = array(
                'customer_id' => $_POST['cust_id'],
                'amount' => $_POST['loan_request'],
                'created_by' => $_SESSION['username'],
                'salary_deduction' => $_POST['salary_deduction'],
                'loan_request_amounts_id' => $_POST['loan_request_amount'],
                'loan_terms_id' => $_POST['term_request'],
                'repayment_amount' => $_POST['repayment_amount'],
                
                'customer_ref'      => $_POST['customer_ref_salary'], 
                'capture_type'     => $_POST['capture_method3'],
                'uploaded_by'       => $_SESSION['username'],
                //JR-2014/03/20 Added is_visa_applicant and pay cycle
                'is_visa_applicant'       => $_POST['visa_applicant2'],
                'customers_pay_cycle_id' => $_POST['pay_cycle'],  
            );
            
        $result = $this->backend->process_salary_upload($params);
        $response = $this->backend->get_response();
            
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $result = $this->backend->new_loan($params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $datetime=$this->backend->get_server_date();
                $loan_id = $response['ResponseMessage']['loan_id'];
                $this->backend->save_to_action_logs('Created new initial loan request with loan_id # ' . $loan_id,'Initial Loan Request',$datetime,$_SESSION['username'],'');
                //JR-2014/04/02 logs
                
                $this->backend->save_to_action_logs('Created new initial loan request with amount of '. $_POST['loan_request'].' for customer id#'.$_POST['cust_id'],'Initial Loan Request',$datetime,$_SESSION['username'],'');
                $this->response['message'] = 'Successfully submitted new loan request';
                $this->response['success'] = true;
                
                //JR-2014/04/09
                if ($params['salary_deduction'] == 1){
                    $today=$this->backend->get_server_date();
                    $this->print_and_email_sds($loan_id);                    
                }
                
				
            }        
        }    
        
        }
    }
    
    
    protected function submit_upload_salary() {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $customer_ref = md5($_POST['merchant_card_id'] . $this->salt);
        $params = array(
               'customer_ref'      => $customer_ref, 
               'capture_type'     => $_POST['capture_method3'],
               'uploaded_by'       => $_SESSION['username'],
               'customer_id'       => $_POST['merchant_card_id'],
        );
        $result = $this->backend->process_salary_upload_manage($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            //JR-2014/04/11
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Uploaded salary for customerId#'. $_POST['merchant_card_id'],'Customer Management',$datetime,$_SESSION['username'],'');
            $_GET['id'] = $_POST['merchant_card_id'];
            $this->view_merchant('Successfully processed salary documents');
            //$this->response['message'] = 'Successfully processed salary documents';
            //$this->response['success'] = true;
        }
    }
    
    protected function submit_upload() {
        $_POST = $this->array_map_recursive('trim', $_POST);   
    
        $noDocs = $_GET['docno'];  
        $params = array(
            'customer_ref'      => $_POST['customer_ref'],
            'customer_id'       => $_POST['cust_id'],
            'doc_type1'         => $_POST['doc_type'][0],
            'capture_type1'     => $_POST['capture_method1'],
            'document_id_no1'   => $_POST['customer_id'][0],
//            'doc_type2'         => $_POST['doc_type'][1],
//            'capture_type2'     => $_POST['capture_method2'],
//            'document_id_no2'   => $_POST['customer_id'][1],
            'uploaded_by'       => $_SESSION['username'],
            'doc_no' => $noDocs,
            'is_edit' => -1,
        );
        $result = $this->backend->process_kyc_upload($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
			//JR-2014/04/02
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Uploaded document #'.$_POST['doc_type'][0]. ' for customerId #'.$_POST['cust_id'],'Customer Document Upload',$datetime,$_SESSION['username'],'');
            
            //JR-2014/04/04 reload document_types
            $id=$_POST['cust_id'];
            $dt=$this->backend->get_document_types_by_customers_id($id);
             if (count($dt) > 0){
                 $option = "<option value=''>-SELECT-</option>";
                 foreach($dt as $row){
                    $option .= '<option value="' . $row['id'] .  '">' . $row['name']  . '</option> ';
                    
                 }
                 //print_r($option);
                $this->response = array(
                    'success'   => true,
                    'data'      => $option,
                    'message'   => 'Successfully processed uploaded documents'
                );
             }
             else{
                 
                 $this->response = array(
                    'success'   => true,
                    'data'      => "<option value=''>-SELECT-</option>",
                    'message'   => 'Successfully processed uploaded documents'
                );
             }
            //$this->response['message'] = 'Successfully processed uploaded documents';
            //$this->response['success'] = true;
        }
    }
    
    //JR-2014/05/22 Reload Documents
    protected function reload_documents() {
        $_POST = $this->array_map_recursive('trim', $_POST);   
        $id = $_POST['merchant_card_id'];
        
        //die();
        //JR-2014/04/04 reload document_types
        $dt=$this->backend->get_document_types_by_customers_id($id);
         if (count($dt) > 0){
             $option = "<option value=''>-SELECT-</option>";
             foreach($dt as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['name']  . '</option> ';
                
             }
             //print_r($option);
            $this->response = array(
                'success'   => true,
                'data'      => $option,
                'message'   => 'Successfully processed uploaded documents'
            );
         }
         else{
             
             $this->response = array(
                'success'   => true,
                'data'      => "<option value=''>-SELECT-</option>",
                'message'   => 'Successfully processed uploaded documents'
            );
         }
    }
    
   
    //PP 05/10/2014 For reupload and edting of docs
      protected function submit_upload_edit() {
        $is_edit= $_GET['isedit'];
          
        $_POST = $this->array_map_recursive('trim', $_POST);   
        $customer_ref = md5($_POST['merchant_card_id'] . $this->salt);
        $doc_no = $this->backend->get_kyc_doc_counter($_POST['merchant_card_id']);
       // $noDocs = $_GET['docno'];                          
        $params = array(
            'customer_ref'      =>$customer_ref,
            'customer_id'       => $_POST['merchant_card_id'],//$_POST['merchant_card_id'],
            'doc_type1'         => $_POST['doc_type'][0],
            'capture_type1'     => $_POST['capture_method'],
            'document_id_no1'   => $_POST['add_docid'],//$_POST['customerid'][0],
//            'doc_type2'         => $_POST['doc_type'][1],
//            'capture_type2'     => $_POST['capture_method2'],
//            'document_id_no2'   => $_POST['customer_id'][1],
            'uploaded_by'       => $_SESSION['username'],
            'doc_no' => $doc_no//$noDocs,
        );
        
      
        $result = $this->backend->process_kyc_upload($params,1);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            //JR-2014/04/02
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Uploaded document #'.$_POST['doc_type'][0]. ' for customerId #'.$_POST['merchant_card_id'],'Customer Document Upload',$datetime,$_SESSION['username'],'');
            
            //$this->response['message'] = 'Successfully processed uploaded documents';
            //$this->response['success'] = true;
            
             $_GET['id'] = $_POST['merchant_card_id'];
            $this->view_merchant('Successfully uploaded documents');
        }
    }
      
      protected function submit_upload_edit_reupload() {
        $is_edit= $_GET['isedit_e'];
        $_POST = $this->array_map_recursive('trim', $_POST);   
        $customer_ref = md5($_POST['merchant_card_id'] . $this->salt);
       // $noDocs = $_GET['docno']; 
       
       $document_id = $_POST['edit_docid'];
       if ($document_id == "0"){ $document_id = "";}
        
       if ($is_edit==0){
        $params = array(
            'customer_ref'      =>$customer_ref,
            'customer_id'       => $_POST['merchant_card_id'],//$_POST['merchant_card_id'],
            'doc_type1'         => $_POST['doc_type'][0],
            'capture_type1'     => $_POST['capture_method'],
            'document_id_no1'   => $document_id,//$_POST['customerid'][0],
//            'doc_type2'         => $_POST['doc_type'][1],
//            'capture_type2'     => $_POST['capture_method2'],
//            'document_id_no2'   => $_POST['customer_id'][1],
            'uploaded_by'       => $_SESSION['username'],
            'doc_no' => 1, //$noDocs,
            'is_edit' =>$is_edit,
            'kyc_id'  =>$_POST['kyc_id'],
        );
       } else {
           $params = array(
            'customer_ref'      => $customer_ref,
            'customer_id'       => $_POST['merchant_card_id'],//$_POST['merchant_card_id'],
            'doc_type1'         => $_POST['edit_doc_type_id'],
            'capture_type1'     => $_POST['capture_method'],
            'document_id_no1'   => $document_id,//$_POST['customerid'][0],
            'uploaded_by'       => $_SESSION['username'],
            'doc_no' => 1, //$noDocs,
            'is_edit' => $is_edit,
            'kyc_id'  =>$_POST['kyc_id'],
            );
       }
        
      
        $result = $this->backend->process_kyc_upload_edit($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            //JR-2014/04/02
            //$datetime=$this->backend->get_server_date();
            //$this->backend->save_to_action_logs('Uploaded document #'.$_POST['doc_type'][0]. ' for customerId #'.$_POST['merchant_card_id'],'Customer Document Upload',$datetime,$_SESSION['username'],'');
            
            //$this->response['message'] = 'Successfully processed uploaded documents';
            //$this->response['success'] = true;
            
            //JR-2014/04/04 reload document_types
             $_GET['id'] = $_POST['merchant_card_id'];
            $this->view_merchant('Successfully processed documents');
        }
    }
     
     
     
    //upload_salary_manage
    //md5($response['ResponseMessage']['customer_id'] . $this->salt) = $key
    protected function upload_salary_manage() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted:'.$extension;
                        $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file '.$extension;
                    } else {
                        $parts = explode('_', $key);                               
                        $pathPrefix = md5($parts[3].$this->salt)."_".$parts[2];
                        
                        $destination = $this->salary_upload_path . $pathPrefix. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        
                        $destinationFinal = $this->salary_upload_path_final . $pathPrefix. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->salary_upload_path . $pathPrefix)) @mkdir ($this->salary_upload_path. $pathPrefix);
                        if (!is_dir($this->salary_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } 
                        elseif(file_exists($destinationFinal))
                        {
                            $this->response['message'] = 'Salary has been uploaded and cannot be overwritten.';
                        } 
                        elseif (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }


    
    protected function upload_salary() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                    } else {
                        $parts = explode('_', $key);       
                        
                        $destination = $this->salary_upload_path . $parts[3]."_".$parts[2]. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->salary_upload_path . $parts[3]."_".$parts[2])) @mkdir ($this->salary_upload_path. $parts[3]."_".$parts[2]);
                        
                        if (!is_dir($this->salary_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        }    
                        elseif (!move_uploaded_file($upload['tmp_name'], $destination)) 
                        {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
        
    
    protected function upload_kyc() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                    } else {
                        $parts = explode('_', $key);
                        $destination = $this->kyc_upload_path . $parts[3]. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->kyc_upload_path . $parts[3])) @mkdir ($this->kyc_upload_path. $parts[3]);
                        if (!is_dir($this->kyc_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } elseif (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    protected function upload_kyc_edit() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                    } else {
                        //echo $key;
                        $parts = explode('_', $key);
                        //echo $parts[2];
                        $pathPrefix = md5($parts[3].$this->salt);
                        $destination = $this->kyc_upload_path .$pathPrefix. DIRECTORY_SEPARATOR . $parts[1]  . '_' .$parts[2] .'.' . $extension;

                        if (!is_dir($this->kyc_upload_path .$pathPrefix)) @mkdir ($this->kyc_upload_path. $pathPrefix);
                        if (!is_dir($this->kyc_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } elseif (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    protected function save_due_date() {
        if (!(isset($_POST['duedate']) && is_numeric($_POST['duedate']) && $_POST['duedate'] >= 0)) {
            $this->response['message'] = 'Invalid due date.';
        } else {
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'due_date' => $_POST['duedate'],
            );
            $result = $this->backend->save_due_date($params);
            $response = $this->backend->get_response();
                                                                 
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully set due date.');
                //JR-2014/04/11
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Due date was set to '.$params['due_date'].' for customerId#'.$params['customer_id'],'Customer Management',$datetime,$_SESSION['username'],'');
            }
        }
    }
    
    protected function update_ca_limit() {
        if (!(isset($_POST['new_ca_limit']) && is_numeric($_POST['new_ca_limit']) && $_POST['new_ca_limit'] >= 0)) {
            $this->response['message'] = 'Invalid loan amount limit.';
        } else {            
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'credit_limit' => $_POST['new_ca_limit'],
                'created_by' => $_SESSION['username']
            );
            $result = $this->backend->set_credit_limit($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully updated loan amount limit.');
                //JR-2014/04/11
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Updated CA limit to '.$params['credit_limit'].' for customerId#'.$params['customer_id'],'Customer Management',$datetime,$_SESSION['username'],'');
            }
        }
    }
    
    protected function update_card_number() {
        if (!(isset($_POST['adjust_card_no']) && is_numeric($_POST['adjust_card_no']) && $_POST['adjust_card_no'] >= 0)) {
            $this->response['message'] = 'Invalid Card Number.';
        } else{
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'card_number' => $_POST['adjust_card_no'] 
            );
            
            $result = $this->backend->update_card_number($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully updated card number.');
                //JR-2014/04/11
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Card # was set to '.$params['card_number'].' for customerId#'.$params['customer_id'],'Customer Management',$datetime,$_SESSION['username'],'');
            }
        }
    }
    
    
    protected function apply_loan() {
        if (!(isset($_POST['loan_amount']) && is_numeric($_POST['loan_amount']) && $_POST['loan_amount'] >= 0)) {
            $this->response['message'] = 'Invalid loan amount.';
        }
        else if ($this->backend->has_existing_loan($_POST['merchant_card_id'])) {
            $this->response['message'] = 'Customer has existing loan.';
        } else {
//           $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['merchant_card_id'],
//                'P03' => $_POST['new_ca_limit'],
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'adjust_cash_advance_limit', $api_params);
            
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'amount' => $_POST['loan_amount'],
                'created_by' => $_SESSION['username'],
                'salary_deduction' => $_POST['salary_deduction'],
                
                'loan_request_amounts_id' => $_POST['loan_request_amount_cm'],
                'loan_terms_id' => $_POST['term_request_cm'],
                'repayment_amount' => $_POST['repayment_amount'],
                //JR-2014/03/19 Added is_visa_applicant
                'is_visa_applicant'       => $_POST['visa_applicant'],
                //JR-2014/03/19 Added customer payment cycle
                'customers_pay_cycle_id' =>  $_POST['pay_cycle_cm'],
                
            );
            $result = $this->backend->new_loan($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //$_GET['id'] = 'x' . $_POST['merchant_card_id'];
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Applied new loan for customer id#'.$params['customer_id']. ' with the amount of '.$params['amount'],'Customer Management',$datetime,$_SESSION['username'],'');
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Your loan application has been saved.');
            }
        }
    }
    
    protected function make_payment() {
        if (!(isset($_POST['payment_amount']) && is_numeric($_POST['payment_amount']) && $_POST['payment_amount'] >= 0)) {
            $this->response['message'] = 'Invalid payment amount.';
        } elseif (!(isset($_POST['date_paid']) && count(explode('/', $_POST['date_paid'])) == 3)) {
            $this->response['message'] = 'Invalid payment date.';
        } elseif (!(isset($_POST['payment_ref']) && trim($_POST['payment_ref']) != '')) {
            $this->response['message'] = 'Invalid payment reference.';
        } else {
            $date_paid = date('Y-m-d', strtotime($_POST['date_paid']));
//           $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['merchant_card_id'],
//                'P03' => $_POST['payment_amount'],
//                'P04' => $_POST['payment_ref'],
//                'P05' => $_POST['payment_method'],
//                'P06' => $date_paid,
//            );
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'make_payment', $api_params);
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'amount' => $_POST['payment_amount'],
                'created_by' => $_SESSION['username'],
                'method' =>  $_POST['payment_method'],
                'payment_reference' => $_POST['payment_ref']
            );
            $result = $this->backend->pay_loan($params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //$_GET['id'] = 'x' . $_POST['merchant_card_id'];
                //JR-2014/04/11
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Applied new payment for customer id#'.$params['customer_id']. ' using '.$params['method'].' payment method amounting to '.$params['amount'],'Customer Management',$datetime,$_SESSION['username'],'');
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully processed payment request to merchant.');
            }
        }
    }
    
    protected function search_merchant() {
        if (!isset($_POST['mobile_number'], $_POST['first_name'], $_POST['last_name'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['mobile_number']) !='' || trim($_POST['first_name']) != '' || trim($_POST['last_name'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {
//            $search_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['first_name'],
//                'P03' => $_POST['last_name'],
//                'P04' => $_POST['mobile_number'],
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'search_merchants', $search_params);
            $search_params = array(
                'fname' => $_POST['first_name'],
                'lname' => $_POST['last_name'],
                'mobile_number' => $_POST['mobile_number'],
                'username' => $_SESSION['username'],
            );
            $result = $this->backend->search_customer($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //JR-2014/05/23 Get loan count to check if user is still allowed to access the page
                $user_type_description = $this->backend->get_user_type_by_username_ex($_SESSION['username']);
                //$user_type_description = $row[0]['description'];   
                //echo $user_type_description;
                //die();
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $loan_parameter = array(
                        'customer_id' => $val['id'],
                        'status' => 1,
                    );
                    $loan_count = $this->backend->get_customer_loan($loan_parameter);
                    $agent_name = $this->backend->get_agent_name_by_id($val['agent_id']);
                    $sub_agent_name = $this->backend->get_sub_agent_name_by_id($val['sub_agent_id']);
                    $employer =  $this->backend->get_employer_name_by_id($val['employer_id']);
                    $button_edit = '<input type="image" src="'. WEBROOT. '/images/edit.png" class="btnEditMerchant" value="Edit" id="'.$val['id'].'" onClick="showCustomerDialog(' ."'". $val['lname']."'" . ","."'". $val['id']."'".",'". $val['title']."'".",'". $val['gender']."'".",'". $val['fname']."'".",'". $val['mname']."'".",'". $val['mobile_number']."'".",'". $val['email_address']."'".",'". $val['dob']."'".",'". $val['street']."'".",'". $val['city']."'".",'". $val['telephone1']."'".",'". $val['telephone2']."'".",'". $val['national_insurance_no']."'".",'". $val['employer_id']."'".",'". $val['employer_name']."'".",'". $val['agent_id']."'".",'". $val['address_line_2']."'".",'". $val['sub_agent_id']."'".",'". $val['pin']."'" .",'". $agent_name."'".",'". $sub_agent_name."'".",'". $employer."'".",'". $val['is_individual']."'".')" />';
                    $button = '<input type="image" src="' . WEBROOT .'/images/view.png" class="btnViewMerchant" value="View" id="'.$val['id'].'" />';    
                    if((strpos($user_type_description,"Admin") !== TRUE || strpos($user_type_description,"Super User") !== TRUE) && ($loan_count > 0))  { 
                        $button_edit="";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                    }
                    if (strpos($user_type_description,"Staff") !== FALSE) {
                        $button_edit = '<input type="image" src="'. WEBROOT. '/images/edit.png" class="btnEditMerchant" value="Edit" id="'.$val['id'].'" onClick="showCustomerDialog(' ."'". $val['lname']."'" . ","."'". $val['id']."'".",'". $val['title']."'".",'". $val['gender']."'".",'". $val['fname']."'".",'". $val['mname']."'".",'". $val['mobile_number']."'".",'". $val['email_address']."'".",'". $val['dob']."'".",'". $val['street']."'".",'". $val['city']."'".",'". $val['telephone1']."'".",'". $val['telephone2']."'".",'". $val['national_insurance_no']."'".",'". $val['employer_id']."'".",'". $val['employer_name']."'".",'". $val['agent_id']."'".",'". $val['address_line_2']."'".",'". $val['sub_agent_id']."'".",'". $val['pin']."'" .",'". $agent_name."'".",'". $sub_agent_name."'".",'". $employer."'".",'". $val['is_individual']."'".')" />';    
                    }   
                    if(strpos($user_type_description,"Staff") !== FALSE){
                        $button = '';    
                    }
                    if(strpos($user_type_description,"Admin") !== FALSE || strpos($user_type_description,"Super User") !== FALSE){
                        $button_edit = '<input type="image" src="'. WEBROOT. '/images/edit.png" class="btnEditMerchant" value="Edit" id="'.$val['id'].'" onClick="showCustomerDialog(' ."'". $val['lname']."'" . ","."'". $val['id']."'".",'". $val['title']."'".",'". $val['gender']."'".",'". $val['fname']."'".",'". $val['mname']."'".",'". $val['mobile_number']."'".",'". $val['email_address']."'".",'". $val['dob']."'".",'". $val['street']."'".",'". $val['city']."'".",'". $val['telephone1']."'".",'". $val['telephone2']."'".",'". $val['national_insurance_no']."'".",'". $val['employer_id']."'".",'". $val['employer_name']."'".",'". $val['agent_id']."'".",'". $val['address_line_2']."'".",'". $val['sub_agent_id']."'".",'". $val['pin']."'" .",'". $agent_name."'".",'". $sub_agent_name."'".",'". $employer."'".",'". $val['is_individual']."'".')" />';    
                    }   
                    $search_result[] = array(
                        $val['fname'],
                        $val['lname'],
                        $val['mobile_number'],
                        str_repeat('xxxx ', 3) . substr($val['linked_cardnumber'], -4),
                        $button . " " . $button_edit,
                    );
                }
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }
    
    
    public function view_merchant($message = null) {
        if (!(isset($_GET['id']))) {
            $this->response['message'] = 'Please select a valid merchant to view';
        } else {
//            $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => substr($_GET['id'],1),
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'view_merchant_details', $api_params);
            
            
            
            $search_params = array(
                'customer_id' => $_GET['id'],
            );
            
            
            //JR-2014/05/23 Get loan count to check if user is still allowed to access the page
            $loan_parameter = array(
                'customer_id' => $_GET['id'],
                'status' => 1,
            );
            $loan_count = $this->backend->get_customer_loan($loan_parameter);
            $user_type = $this->backend->get_user_type_by_username($_SESSION['username']);
            $response = $this->backend->get_response();
            $row=$response['ResponseMessage'];
            $user_type_description = $row[0]['description'];
            //echo $row[0]['description'];
            //die();
            //if(strpos($row[0]['description'],"Agent") !== FALSE)
                
            
            
            
            //PP 04/10/2014
            $isallverify = $this->backend->is_doc_verified($search_params['customer_id'],1);
            $isallverifysalary = $this->backend->is_doc_verified($search_params['customer_id'],2); 
            //print_r($isallverifysalary."lems");
            //die();
            $result = $this->backend->get_customer_info($search_params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $profile = $response['ResponseMessage']['info'];
                $out_profile = array(
                    'card_id' => $profile['id'],
                    'merchant_fname' => $profile['fname'],
                    'merchant_mname' => $profile['mname'],
                    'merchant_lname' => $profile['lname'],
                    'merchant_lname' => $profile['lname'],
                    'national_insurance_no' => $profile['national_insurance_no'],
                    'merchant_mobilenumber' => $profile['mobile_number'],
                    'merchant_address' => $profile['street'] . ' ' . $profile['city'] . ' ' . $profile['country'],
                    'ca_limit' => number_format($profile['credit_limit'],2),
                    'prepaid_card' => $profile['linked_cardnumber'],
                    'card_balance' => number_format($profile['loan_balance'],2),
                    'due_date' => $profile['due_date']
                );
                $transactions = $response['ResponseMessage']['transactions'];
                $ca_history = $response['ResponseMessage']['loans'];
                $payment_history = $response['ResponseMessage']['payments'];
                $credit_limit_history = $response['ResponseMessage']['credit_limit'];
                $customer_documents = $response['ResponseMessage']['customer_documents'];
                $salary_documents = $response['ResponseMessage']['salary_documents'];
                
                                
            
                $out_transactions = array();
                foreach ($transactions as $row) {
                    $out_transactions[] = array(
                        $row['transaction_date'],
                        $row['name'],
                        $row['is_debit'] == 1? number_format($row['amount'],2): '-',
                        $row['is_debit'] == 0? number_format($row['amount'],2): '-',
                        number_format($row['loan_balance'],2),
                    );
                }
                $out_ca_history = array();
                foreach ($ca_history as $row) {
                    $out_ca_history[] = array(
                        $row['created_on'],
                        number_format($row['amount'],2),
                    );
                }
                $out_payment_history = array();
                foreach ($payment_history as $row) {
                    $out_payment_history[] = array(
                        $row['date_paid'],
                        $row['date_paid'],
                        number_format($row['amount'],2),
                    );
                }
                
                $out_credit_limit_history = array();
                foreach ($credit_limit_history as $row) {
                    $out_credit_limit_history[] = array(
                        //$row['created_on'],
                        $row['date_created'],
                        number_format($row['credit_limit'],2),
                    );
                }
                
                /*
                //JR-2014/05/23 Get loan count to check if user is still allowed to access the page
                $loan_parameter = array(
                    'customer_id' => $_GET['id'],
                    'status' => 1,
                );
                $loan_count = $this->backend->get_customer_loan($loan_parameter);
                $agent = $this->get_user_type_by_username($_SESSION['username']);
                $response = $this->get_response();
                $row=$response['ResponseMessage'][''];
                //if(strpos($row[0]['description'],"Agent") !== FALSE)
                */
                
                $kyc_capture_types = $this->backend->get_capture_types();
                $out_customer_documents = array();
                foreach ($customer_documents as $row) {
                        if ($row['document_id_no']==""){
                            $document_id_no = "0";
                        } else {
                            $document_id_no = $row['document_id_no'];
                        }
                        //$document_id_no = str_replace(" ","~",);
                        //echo $document_id_no;
                        //die();
                    //PP 03/18/2014 Added button for verification and for revision
                    
                     //PP  04/16/2014 Added for expirationdate and issuedate
                   
                        $buttonVerify="";
                        $buttonCofig="";
                        $buttonRevision="";
                        $buttonEdit="";
                        $varcustomerid1="";
                        $buttonEditConfig="";
                        $docStatus="";
                       	if((strpos($user_type_description,"Admin") !== FALSE || strpos($user_type_description,"Super User") !== FALSE))  { 
                            $buttonCofig = '<input class="btn btn-success" type="button" value="Add" class="btnAddDates" onclick="showAddIssueExpirydate('.$row['id'].','.$row['customer_id'].')"  id="'.$row['id'].'"  />'; 
                        }
                        
                        if(!$row['expiration_date']==0)
                        {
                        $date1 = new DateTime($row['issue_date']);
                        $date2 = new DateTime($row['expiration_date']);
                         if($date2->format('Y-m-d')>date('Y-m-d'))     
                         { 
							if(strpos($user_type_description,"Admin") !== FALSE || strpos($user_type_description,"Super User") !== FALSE  ) { $buttonVerify = '<input  type="button" class="btnVerify" value="Verify" id="'.$row['id'].'" />'; }
                         else
                          {
							$docStatus='<label>Docs Expired</label>';}
                          }
                         }
                    
                        //$buttonRevision = '<input type="button" class="btnRevision" value="Revision" id="'.$row['id'].'" />';    
						if(strpos($user_type_description,"Admin") !== FALSE || strpos($user_type_description,"Super User") !== FALSE  ) { 
                            $buttonRevision = '<input type="button" class="btnRevision" value="Revision" id="'.$row['id'].'" />';    
						}
                        $buttonEdit = '<input type="button"   value="Edit" onclick="showEditDocs('.$row['id'].','.$document_id_no.','.$row['document_type_id'].')" id="'.$row['id'].'" />';   
						if(((strpos($user_type_description,"Admin") !== TRUE || strpos($user_type_description,"Super User") !== TRUE) && ($loan_count > 0)) || strpos($user_type_description,"Staff") !== FALSE )  { 
                            $buttonEdit = "";   
                        }
                        $varcustomerid1='<input type="hidden" class="customerdetail" id="customerid" value="'.$row['customer_id'].'" name="customerid"  />'; 
                         if($row['status']==1)
                    	{
                        $buttonVerify='<label>Verified</label>';
                        $docStatus='<label>Docs Done</label>';
                        $buttonRevision='';
                        $buttonEdit='';   
                    }
                    if($row['status']==2)
                    {
                        $buttonVerify='<label>For Revision</label>';
                        $docStatus='';   
                        $buttonRevision='';
                        $buttonEdit = '<input type="button"  class="btn btn-inverse" value="Edit" onclick="showEditDocs('.$row['id'].','.$document_id_no.','.$row['document_type_id'].')" id="'.$row['id'].'" />';   
                    }
                    
                    if($row['status']==0)
                    {
                           $docStatus='<label>Docs Started</label>';
                    }
                    
                       
                        
                      
                        
                   
                    
                     //PP  04/16/2014 Added for expirationdate and issuedate
                    if(!$row['expiration_date']==0)
                    {
                        $date1 = new DateTime($row['issue_date']);
                        $date2 = new DateTime($row['expiration_date']);
 
                        
                        $buttonCofig='Issue: '.$date1->format('Y-m-d').''; 
                        $buttonCofig.='<br>Expiry: '.$date2->format('Y-m-d');
                        if(strpos($user_type_description,"Admin") !== FALSE || strpos($user_type_description,"Super User") !== FALSE  ) {
                            $buttonEditConfig = '<br><input class="btn btn-success" type="button" value="Edit" class="btnAddDates" onclick="showAddIssueExpirydate('.$row['id'].','.$row['customer_id'].')"  id="'.$row['id'].'"  />'; 
                        }
                        //$buttonCofig='<label>ExpirationDate: '.$row['expiration_date'].'</label>';
                        //$buttonCofig.='<br><label>IssueDate: '.$row['issue_date'].'</label>'; 
                    }
                    
                    $varcustomerid='<input type="hidden" class="customerdetail" id="customerid_'.$row['id'].'" value="'.$row['customer_id'].'" name="customerid_'.$row['id'].'"  />';
                    $vardoc='<input type="hidden" class="docdetail" id="doctype" value="1" name="doctype"  />';
                    $out_customer_documents[] = array(
                        $row['kyc_document'],
                        $row['document_id_no'],  
                        $kyc_capture_types[$row['capture_type']],
//                        null,
                        $this->kyc_view_path(1, $row['kyc_document'] . '-1', $row['path_filename1'], $row['customer_id']) . $this->kyc_view_path(2, $row['kyc_document'].'-2', $row['path_filename2'], $row['customer_id']) . $this->kyc_view_path(3, $row['kyc_document'].'-3', $row['path_filename3'], $row['customer_id']),$buttonCofig . $buttonEditConfig,$docStatus,
                        $buttonVerify." ".$buttonRevision ." ".$buttonEdit .  " ".$varcustomerid . " ".$vardoc. " " . $varcustomerid1,
                    );
                }
                $out_salary_documents = array();
                $ctr=1;
               foreach ($salary_documents as $row) {
                    //PP 03/24/2014 added buttons and variables
                  
                    $buttonVerify = '<input  type="button" class="btnVerifySalary" value="Verify" id="'.$row['id'].'" />'; 
                    $buttonRevision = '<input type="button" class="btnRevisionSalary" value="Revision" id="'.$row['id'].'" />';
                    //JR-2014/05/06
                    //$buttonEdit = '<input type="button" class="btnRevisionSalary" value="Edit" id="'.$row['id'].'" />';   
                    //$varcustomerid1='<input type="hidden" class="customerdetail" id="customerid" value="'.$row['customer_id'].'" name="customerid"  />';
                    $vardoc='<input type="hidden" class="docdetailsalary" id="doctype" value="2" name="doctype"  />';
                     if($row['status']==1)
                    {
                        $buttonVerify='<label>Verified</label>';
                        $buttonRevision='';   
                    }
                    if($row['status']==2)
                    {
                        $buttonVerify='<label>For Revision</label>';
                        $buttonRevision='';    
                        
                    }
                    
                    
                    
                    
                   $varcustomerid='<input type="hidden" class="customerdetail" id="customerid_'.$row['id'].'" value="'.$row['customer_id'].'" name="customerid_'.$row['id'].'"  />';
                   
                    $out_salary_documents[] = array(
                        $this->salary_view_path($ctr, 'salary', $row['path_filename'], $row['customer_id']),
                        $row['date_uploaded'],$buttonVerify." ".$buttonRevision . " ".$varcustomerid . " ".$vardoc,
                    );
                    $ctr++;
                }
                $documents = $this->backend->get_document_types();
                $documents = array('' => '-SELECT-') + $documents;
                $is_sdf_sent= $this->backend->get_loan_sdf_id($search_params);
                  
                     
                $this->response = array(
                    'message'       => $message,
                    'success'       => true,
                    'profile'       => $out_profile,
                    
                    'trans'         => $out_transactions,
                    'ca_history'    => $out_ca_history,
                    'payment_history' => $out_payment_history,
                    'credit_limit_history' => $out_credit_limit_history,
                    'customer_documents' => $out_customer_documents,
                    'salary_documents' => $out_salary_documents,
                    'documents' =>$documents,     
                    'isallverify' => $isallverify,  //PP 04/07/2014   for notification 
                    'isallverifysalary' => $isallverifysalary,
                    'suffix'=>md5($search_params['customer_id'] . $this->salt),
                    'is_sdf_sent' =>$is_sdf_sent,
                    //'doc_no_count' => $this->backend->get_kyc_doc_counter($search_params['customer_id']),
                );                     
            }
        }
    }
 
    protected function kyc_view_path($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            
            return '<input class="view_doc_u" type="button" title="'.$doc_type.'" value="'.$page.'" href="'.WEBROOT.'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'"/>&nbsp;</a>';
            
          /*  return '<input id="view_doc_u" type="button" onclick="load_Image_kyc();" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>&nbsp;';*/
          /*  return '<a target="_blank" href="'.WEBROOT.'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'">'.'<input class="btn btn-file" type="button" title="'.$doc_type.'" value="'.$page.'"/>&nbsp;</a>';
            */
            
            
            
            //return '<a href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>';
        } else {
            return '';
        }
    }
    
    protected function kyc_view_doc_newtab($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            
            return 'href="'.WEBROOT.'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'"/>&nbsp;</a>';
            
          /*  return '<input id="view_doc_u" type="button" onclick="load_Image_kyc();" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>&nbsp;';*/
          /*  return '<a target="_blank" href="'.WEBROOT.'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'">'.'<input class="btn btn-file" type="button" title="'.$doc_type.'" value="'.$page.'"/>&nbsp;</a>';
            */
            
            
            
            //return '<a href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>';
        } else {
            return '';
        }
    }
    
    
    protected function salary_view_path($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            return '<input  type="button" class="salary_viewer" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="Salary'.$page.'"/>&nbsp;';
        } else {
            return '';
        }
    }
    
    //JR-2014/04/09
    protected function print_and_email_sds($loan_id) {
        $result = $this->backend->search_sds_by_id($loan_id);
        $response_sds = $this->backend->get_response();
        //echo $response_sds['ResponseCode'];
        if (!(isset($response_sds['ResponseCode'], $response_sds['ResponseMessage']))) {
                //$this->response_sds['message'] = 'System error, unable to connect to database';
                //echo 'test1';
            } elseif (!($response_sds['ResponseCode'] == '0000')) {
                //$this->response['message'] = $response['ResponseMessage'];
                //echo 'test2';
            } else {   
                foreach ($response_sds['ResponseMessage'] as $key => $val) {
                    
                    $name = $val['fname'] . " " . $val['lname'];
                    $today = date('m-d-y');
                    $employer = $val['employer'];
                    $amount = $val['repayment_amount'];
                    $address = $val['address'];
                    $loan_id = $val['id'];
                    $loan_terms_id = $val['loan_terms_id'];
                    $loan_amount = $val['amount'];
                    $email_address = $val['email_address'];
                }
        
            }
        
        require_once('tcpdf/tcpdf.php');
        //header('Content-type: application/pdf');
        //require_once('tcpdf.php');
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING ="";
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        $pdf->SetAutoPageBreak(false, 0);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
       
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('Courier', 'B', 12);
        // add a page
        $pdf->AddPage();
        $image_path=WEBROOT . "/public/images/templates/sds1.png";
        //$destination_path =WEBROOT .  "/public/images/pdf/";
        $destination_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'sds/';
        //print_r($image_path);
        
        
        //die();       
        //JR-2014/04/05
       $application_fee=$this->backend->get_application_fee($loan_amount,$loan_terms_id); 
       
        $datetime=$this->backend->get_server_date();                
        $y = $pdf->GetY($image_path);
        $rby = $pdf->getImageRBY($image_path);
        $pdf->Image($image_path,10,20,200,$rby-$y);
        $pdf->Text(30,132,$name);
        $pdf->Text(33,47.5,$today);
        $pdf->Text(63,56.5,$employer);
        $pdf->Text(60,155,$employer);
        $pdf->Text(135,84,$amount);
        //$this->backend->save_to_action_logs('Generated Salary Deduction Slip','Report',$datetime,$_SESSION['username'],'');

        $pdf->lastPage();
        //$filename = $pdf->Output('sds.pdf', 'E');
        $pdf_file = $today . '_' . $name . '.pdf';
        $full_path = $destination_path . $pdf_file;
        
        $filename = $pdf->Output($full_path, 'F');
        //exit();
        //echo 'test';
        //die();
        $datetime=$this->backend->get_server_date();
        $this->backend->sds_notification($email_address, $destination_path,$pdf_file,$name);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $params = array(
                'loan_id' => $loan_id,
            );
            $this->backend->set_sdf_sent($params);
            
            $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message'],
            );
            $this->backend->save_to_action_logs('Salary deduction slip was sent to '.$email_address,'Customer Registration',$datetime,$_SESSION['username'],'');
        ob_clean(); //turns off output buffer
        //$attachment = chunk_split(base64_encode($pdfdoc));
     }
        
    }   
    
    protected function load_agents(){
         $id = $_GET['id'];
         //echo $id;
         //die();
         $agents=$this->backend->get_agents_for_customers($id,'');
         if (count($agents) > 0){
             $option = "";
             foreach($agents as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
                
             }
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             $this->response = array(
                'success'   => false,
                'data'      => "Error"
            );
         }
         
    }
    
    protected function get_loan_terms(){
         $id = $_GET['id'];
         //echo $id;
         //die();
         $terms=$this->backend->get_loan_terms_by_id($id,'All');
         if (count($terms) > 0){
             $option = "";
             foreach($terms as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['term']  . '</option> ';
                
             }
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             $this->response = array(
                'success'   => false,
                'data'      => "Error"
            );
         }
         
    }
    
    protected function load_employers(){
         $id = $_GET['id'];
         $employer=$this->backend->get_employers_for_customers($id);
         //echo count($employers);
         //die();
         if (count($employer) > 0){
             $option = "";
             foreach($employer as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['name']  . '</option> ';
                
             }
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             $this->response = array(
                'success'   => false,
                'data'      => "Error"
            );
         }
         
    }
    
    public function update_customer()
    {
       
       $_POST = $this->array_map_recursive('trim', $_POST);
       $reg_params = array(
            'id' => $_POST['edit_customer_id'],
            'fname' => $_POST['edit_fname'],
            'lname' => $_POST['edit_lname'],
            'mname' => $_POST['edit_mname'],
            'title' => $_POST['edit_title'],
            'gender' => $_POST['edit_gender'],
            'mobile_number' =>  $_POST['edit_cpno'],
            'email_address' => $_POST['edit_email'],
            'dob' => $_POST['edit_dob'],
            'street' => $_POST['edit_street'],
            'city' => $_POST['edit_city'],
            'telephone1' => $_POST['edit_telephone2'],
            'telephone2' => $_POST['edit_telephone3'],
            'national_insurance_no' => $_POST['edit_tin'],
            'employer_id' => $_POST['edit_employer'],
            'employer_name' => $_POST['edit_employer_name'],
            'address_line_2' => $_POST['edit_address_line_2'],
            'agent_id' => $_POST['edit_agent'],
            'sub_agent_id' => $_POST['edit_sub_agent'],
            'pin' => $_POST['edit_cus_pin'],
             );
            $result = $this->backend->update_customer($reg_params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response['message'] = $response['ResponseMessage'];
                $this->response['success'] = true;             
            }
   }
   
   public function update_customer_home()
    {
       
       $_POST = $this->array_map_recursive('trim', $_POST);
       $reg_params = array(
            'id' => $_POST['edit_home_customer_id'],
            'fname' => $_POST['edit_home_fname'],
            'lname' => $_POST['edit_home_lname'],
            'mname' => $_POST['edit_home_mname'],
            'title' => $_POST['edit_home_title'],
            'gender' => $_POST['edit_home_gender'],
            'mobile_number' =>  $_POST['edit_home_cpno'],
            'email_address' => $_POST['edit_home_email'],
            'dob' => $_POST['edit_home_dob'],
            'street' => $_POST['edit_home_street'],
            'city' => $_POST['edit_home_city'],
            'telephone1' => $_POST['edit_home_telephone2'],
            'telephone2' => $_POST['edit_home_telephone3'],
            'national_insurance_no' => $_POST['edit_home_tin'],
            'employer_id' => $_POST['edit_home_employer'],
            'employer_name' => $_POST['edit_home_employer_name'],
            'address_line_2' => $_POST['edit_home_address_line_2'],
            'agent_id' => $_POST['edit_home_agent'],
            'sub_agent_id' => $_POST['edit_home_sub_agent'],
            'pin' => $_POST['edit_home_cus_pin'],
             );
            $result = $this->backend->update_customer($reg_params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response['message'] = $response['ResponseMessage'];
                $this->response['success'] = true;             
            }
   }
    
            
    
    protected function view_document_image() {

        $this->layout = false;
        if (!(isset($_GET['token']) && $_GET['token'] != '')) {
            echo 'Invalid request';
        } elseif (!(isset($_GET['token2']) && $_GET['token2'] != '')) {
            echo 'Invalid request';
        } elseif (false === ($actual_file = $this->backend->get_actual_document_pathname(array('token' => $_GET['token'], 'token2' => $_GET['token2'])))) {
            echo 'Unable to retrieve document';
        } elseif (!file_exists($actual_file)) {
            echo $actual_file;
        } else {
            $imageinfo = @getimagesize($actual_file); //check image size
            
            //print_r($imageinfo);
//            die();
//            if (!(in_array($imageinfo['mime'], $this->allowed_types))) {
//                echo 'Invalid image';
//            } else {
                $fp = fopen($actual_file, 'rb');
                header('Content-Type: '. $imageinfo['mime']);
                header('Content-Length: '. filesize($actual_file));
                fpassthru($fp);
                exit;
//            }
        }
    }
}
