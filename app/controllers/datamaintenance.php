<?php
class datamaintenance extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }

    public function management() {
        $actions = array('register_contact','get_contact','get_contact_byid','edit_contact','delete_contact');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
