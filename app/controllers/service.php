<?php
  class service extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }

     private function delete_service()
      {
            // session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
            // print_r($_POST);
            // die();
            $params = array(
                'session_id' => $_SESSION['sessionid'],
                'product_id' => $_POST['product_id'],
                'update_date' => date('Y-m-d'),
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'delete_my_product', $params);

                                   
              if (!(isset($response['respcode'], $response['respcode']))) {
                    $this->response['success'] = false;
                    $this->response['message'] = 'System error, unable to connect to database';
              } elseif (!($response['respcode'] == '0000')) {
                    $this->response['success'] = false;
                    $this->response['message'] = $response['respmsg'];
              } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['respmsg'],
                        'redirect_url' => WEBROOT ."/service/management",
                    );
              }
      }
	  
	  public function management()
      {		
            $actions = array("update", "add", "delete_service");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
			
			$params = array(
                'session_id' => $_SESSION['sessionid'],
                'condition'  => " ORDER BY category_id"
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_products_per_merchant_order', $params);
			$services = $response['respmsg'];
			$this->view->assign('services', $services);
			
			
			 //get_product_categories
			$response = lib::getWsResponse(API_URL, 'get_product_categories', $params);
			$categories = $response['respmsg'];
			$this->view->assign('categories', $categories);
			
			
			// print_r($services);
			// die();
// 			
      }
	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'name' => $_POST['txtName'],
        	'description' => $_POST['txtDescription'],
        	// 'category_id' => !isset($_POST['txtCategory']) ? -1: $_POST['txtCategory'],
        	'category_id' => $_POST['txtCategory'],
        	'price' => $_POST['txtPrice'],
        	'cost' => $_POST['txtCost'],
        	'quantity' => $_POST['txtQuantity'],
        	'minutes_of_product' => $_POST['txtMinutesOfService'],
        	// 'product_type' => $_POST['txtProductType']
        	'product_type' => 'SERVICE',
        );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'register_product', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
                    'redirect_url' => WEBROOT ."/service/management",
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// print_r($_POST);
		// die();
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtProductId'],
        	'name' => $_POST['txtName'],
        	'description' => $_POST['txtDescription'],
        	// 'category_id' => !isset($_POST['txtCategory']) ? -1: $_POST['txtCategory'],
        	'price' => $_POST['txtPrice'],
        	'cost' => $_POST['txtCost'],
        	'quantity' => $_POST['txtQuantity'],
        	'minutes_of_product' => $_POST['txtMinutesOfService'],
        	// 'product_type' => $_POST['txtProductType']
        	'product_type' => 'SERVICE',
        	'category_id' => $_POST['txtCategory'],
        	//txtMinutesOfService
        	
        );

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'update_product', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
                    'redirect_url' => WEBROOT ."/service/management",
	            );
	      }
	  }
	  
	  
      private function delete()
      {
            $id = $_GET['id'];
            
            $result = $this->backend->delete_agent($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
      }
  }  
?>
