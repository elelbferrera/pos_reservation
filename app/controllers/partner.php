<?php
    class partner extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }
      
	  public function contact($get)
	  {
	  		$id = $get[0];
		
	  		$actions = array("update_partner");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }

		    $this->backend->get_partner_info($id);
	        $response = $this->backend->get_response();

			$this->view->assign('partner_info', $response['ResponseMessage']);
			$this->view->assign("partner_id", $id);
			
			$result = $this->backend->get_partner_types();
			$partner_type = $this->backend->get_response();
			$partner_type = $partner_type['ResponseMessage'];
			
			$this->view->assign('partner_type', $partner_type);
			
			$result = $this->backend->api_get_countries();
			$country = $this->backend->get_response();
			$country = $country['ResponseMessage'];
			
			$this->view->assign('country', $country);
	  }
	  
	  
      public function management()
      {		
            //print_r("hello"); 
            $result = $this->backend->get_partners();
            $response = $this->backend->get_response();
			
			$partners = $response['ResponseMessage'];
			
			$this->view->assign("partners", $partners);
			
			// die(); 
      }
	  
	  private function create_partner(){
	  		$params = array(
				'create_date' => '',
				'create_by' => $_SESSION['username'],
				'status' => 'A',
				'partner_type_id' => $_POST['txtPartnerTypeId'],
				'parent_id' => -1,
				'dba' => $_POST['txtDBA'],
				'company_name' => $_POST['txtCompanyName'],
				'address1' => $_POST['txtAddress1'],
				'address2' => $_POST['txtAddress2'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country_id' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'phone1' => $_POST['txtPhone1'],
				'phone2' => $_POST['txtPhone2'],
				'logo' => '',
			);
			
		  $result = $this->backend->create_partner($params);
	      $response = $this->backend->get_response();
	                         			                                 
	      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['ResponseCode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['ResponseMessage'];
	      } else {
	            $this->response = array(
	                'id'=> $response['ResponseMessage']['id'],
	                'success' => true,
	                'message' => $response['ResponseMessage']['message'],
	                'redirect' => WEBROOT."/partner/edit/".$response['ResponseMessage']['id'],
	            );
	      }
	  }

 	  private function update_partner(){
	  		$params = array(
	  			'partner_id' => $_POST['txtPartnerID'],
				'create_date' => '',
				'create_by' => $_SESSION['username'],
				'status' => 'A',
				'partner_type_id' => $_POST['txtPartnerTypeId'],
				'parent_id' => -1,
				'dba' => $_POST['txtDBA'],
				'company_name' => $_POST['txtCompanyName'],
				'address1' => $_POST['txtAddress1'],
				'address2' => $_POST['txtAddress2'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country_id' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'phone1' => $_POST['txtPhone1'],
				'phone2' => $_POST['txtPhone2'],
				'logo' => '',
			);
			
		  $result = $this->backend->update_partner($params);
	      $response = $this->backend->get_response();
	                         			                        
	      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['ResponseCode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['ResponseMessage'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['ResponseMessage'],
	                'redirect' => WEBROOT."/partner/edit/".$_POST['txtPartnerID'],
	            );
	      }
	  }
	  
	  
	 
	  
	  
	  public function edit($get)
	  {
	  		$id = $get[0];
		
	  		$actions = array("update_partner");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }

		    $this->backend->get_partner_info($id);
	        $response = $this->backend->get_response();

			$this->view->assign('partner_info', $response['ResponseMessage']);
			$this->view->assign("partner_id", $id);
			
			$result = $this->backend->get_partner_types();
			$partner_type = $this->backend->get_response();
			$partner_type = $partner_type['ResponseMessage'];
			
			$this->view->assign('partner_type', $partner_type);
			
			$result = $this->backend->api_get_countries();
			$country = $this->backend->get_response();
			$country = $country['ResponseMessage'];
			
			$this->view->assign('country', $country);
			
			
			
			

			// print_r($response['ResponseMessage']);
			// die();
		
	  }
	  
	  
	  
	  
	  public function add()
	  {
	  		$actions = array("create_partner");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
			
			$result = $this->backend->get_partner_types();
			$partner_type = $this->backend->get_response();
			$partner_type = $partner_type['ResponseMessage'];
			
			$this->view->assign('partner_type', $partner_type);
			
			$result = $this->backend->api_get_countries();
			$country = $this->backend->get_response();
			$country = $country['ResponseMessage'];
			
			
			$this->view->assign('country', $country);
			
			// print_r($partner_type);
			
			// die();
	  }
	  
	  
	  private function delete()
	  {
	  	
	  }
 }