<?php
  class sign_application extends template{
  	
	
	public function index()
	{
		// print_r($_GET);
		$actions = array("submit_sign");
        
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
				
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			// die();
        }
		
		
		$product_sign = $this->backend->get_product_order_info($_GET['order_id']);
		$data = $product_sign['signature'];
		
		
		
		
		$this->view->assign('signature', $data);
		$this->view->assign('order_id', $_GET['order_id']);
		$this->view->assign('partner_id', $_GET['partner_id']);
		// die();
	}
	
	private function submit_sign()
	{
		// print_r($_POST);
		// die();
		//order_id, status, update_by, signature
		$insert_data= array(
			'order_id' => $_POST['txtOrderId'],
			'partner_id' => $_POST['txtPartnerId'],
			'signature' => $_POST['txtImage'],
			'create_by' => "MERCHANT",
			'status' => 'Application Signed',
		);
		
		
		
		
		
		$result = $this->backend->update_signature($insert_data);
        $response = $this->backend->get_response();

		
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			$this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['success'] = false;
            $this->response['message'] = $response['ResponseMessage'];
			
        } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Signature has been submitted.",
                );
        }
		
	}
	
	public function generate_pdf_doc()
	{
		
		$id = $_GET['partner_id'];
		
		$order_id = $_GET['order_id'];
		
		$product = $this->backend->get_product_order_info($order_id);
		
	
		
		//$order_id = str_replace("order_id=", "", $info[1]);
		
		require_once('tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING ="";
        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        // set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks        
        $pdf->SetAutoPageBreak(false, 0);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf->SetFont('Courier', 'B', 12);
        $cnt = 1;
        $page_cnt = 1;
        $row_limit = 200;
        $isAddPage = false;
        // add a page
        $pdf->AddPage();
		
		if($product['product_id'] ==1)
		{  
			$image_path = 'images/template.png';
	        $destination_path = 'images/pdf_applications/';
	        
	        //$datetime=$this->backend->get_server_date();                
	        $y = $pdf->GetY($image_path);
	        $rby = $pdf->getImageRBY($image_path);
			
			
	        $pdf->Image($image_path,7,8,200,$rby-$y);
			
	        // set information font
	        $pdf->SetFont('times', '', 12);    
			
			
			// https://localhost/registration/merchant/generate_pdf_doc/32
			
			$result = $this->backend->get_partner_info($id);
			
			
			$merchant_info = $this->backend->get_response();
			
			$merchant_info = $merchant_info['ResponseMessage'];
			
			
	
			
			$pdf->Text(52.8,38.5,$merchant_info['company_name']); //company name
			$pdf->Text(136,38.5,$merchant_info['merchant_id']); //MID
			
			
			$pdf->Text(30,45.8,$merchant_info['dba']); 
			$pdf->Text(148,45.8,$merchant_info['merchant_processor']); //MID
			
			$pdf->Text(40,52.8,$merchant_info['address1']);
			
			$pdf->Text(100,52.8,$merchant_info['city']);
			
			$pdf->Text(138,52.8,$merchant_info['state']);
			
			$pdf->Text(168,52.8,$merchant_info['zip']);
			
			$pdf->Text(50,60.5,$merchant_info['email']);
			$pdf->Text(151,60.5,$merchant_info['phone1']);
			
			
			$pdf->Text(46,67.5,$merchant_info['fax']);
			$pdf->Text(151,67.5,$merchant_info['phone2']); 
			
			
			$pdf->Text(42,88.8,$merchant_info['first_name']);
			$pdf->Text(140,88.8,$merchant_info['mobile_number']);
			
			
			$pdf->Text(42,95.8,$merchant_info['last_name']);
			$pdf->Text(144,95.8,$merchant_info['office_number']);
			
			$pdf->Text(38,102,$merchant_info['position']);
			
			$pdf->Text(135,102,$merchant_info['contact_fax']);
			
			$pdf->Text(46.5,109,$merchant_info['contact_email']);
		
			//get parent_partner
			$parent_id = $merchant_info['parent_id'];
			$result = $this->backend->get_partner_info($parent_id);
			$agent = $this->backend->get_response();
			$agent = $agent['ResponseMessage'];
			
			
			$pdf->Text(46.5,228.5,$agent['first_name']." ".$agent['last_name']);
			$pdf->Text(127.5,228.5,$agent['merchant_id']);
			$pdf->Text(36.5,235.5,$agent['contact_email']);
			
			$pdf->Text(133,235.5,$agent['mobile_number']);
			
			
			$merchant_fees = $this->backend->get_product_fee($order_id);
			
			$total_fee = 0;
			foreach($merchant_fees as $fee)
			{
				$total_fee = $total_fee + $fee['product_information'];
				if($fee['product_name']== 'MONTHLY FEE')
				{
					$pdf->Text(80,127.5,$fee['product_information']);		
				}
				else{
					$pdf->Text(80,133.5,$fee['product_information']);	
				}
			}
			
			$pdf->Text(80,154,number_format($total_fee,2));	
			
			$pdf->Text(143,127.5,$merchant_info['bank_name']);
			$pdf->Text(143,134.5,$merchant_info['account_name']);
			$pdf->Text(143,141.5,$merchant_info['account_number']);
			$pdf->Text(143,147.5,$merchant_info['routing_number']);
			
			
			$product_sign = $this->backend->get_product_order_info($order_id);
			$data = $product_sign['signature'];
			$destination_path = 'images/pdf_applications/';
			if($data !="")
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
		
				file_put_contents($destination_path.'/'.$order_id.'.png', $data);
				
				$pdf->Image($destination_path.'/'.$order_id.'.png',40,202,60,10);
			}
			$pdf_file =  $id.'.pdf';
	        $full_path = $destination_path . $pdf_file;   
			$pdf->Output($full_path, 'F');
		}
		// $pdf->Image($image_path."hello.jpg");
		//$pdf->Ou
		
		
		// $im = new Imagick();
		// $im->setResolution(300, 300);     //set the resolution of the resulting jpg
		// $im->readImage($full_path);    //[0] for the first page
		// $im->setImageFormat('jpg');
		// header('Content-Type: image/jpeg');
		// echo $im;
		// die();
		if($product['product_id'] ==2) //for online order
		{
			$image_path = 'images/GO2POS.png';
	        $destination_path = 'images/pdf_applications/';
	        
	        //$datetime=$this->backend->get_server_date();                
	        $y = $pdf->GetY($image_path);
	        $rby = $pdf->getImageRBY($image_path);
			
			
	        $pdf->Image($image_path,7,8,200,$rby-$y);
			
	        // set information font
	        $pdf->SetFont('times', '', 9.5);    
			
			
			// https://localhost/registration/merchant/generate_pdf_doc/32
			
			$result = $this->backend->get_partner_info($id);
			
			
			$merchant_info = $this->backend->get_response();
			
			$merchant_info = $merchant_info['ResponseMessage'];
			
			
	
			
			$pdf->Text(45.8,33.0,$merchant_info['company_name']); //company name
			$pdf->Text(100,33.0,$merchant_info['dba']);
			$pdf->Text(160,33.0,$merchant_info['merchant_id']); //MID
			
			$pdf->Text(35,37.8,$merchant_info['address1']);
			$pdf->Text(101,37.8,$merchant_info['city']);
			$pdf->Text(140,37.8,$merchant_info['state']);
			$pdf->Text(174,37.8,$merchant_info['zip']);
			 
			//$pdf->Text(148,45.8,$merchant_info['merchant_processor']); //MID
			$pdf->Text(40,42.5,$merchant_info['phone1']);
			$pdf->Text(88,42.5,$merchant_info['fax']);
			
			$pdf->Text(136,42.5,$merchant_info['email']);
		
			$pdf->Text(40,54.5,$merchant_info['first_name']);
			$pdf->Text(96,54.5,$merchant_info['last_name']);
			$pdf->Text(154,54.5,$merchant_info['position']);
			
			
			
			$pdf->Text(46,59.8,$merchant_info['office_number']);
			$pdf->Text(118.5,59.8,$merchant_info['contact_email']);
			
			
			
			
			
		
			//get parent_partner
			$parent_id = $merchant_info['parent_id'];
			$result = $this->backend->get_partner_info($parent_id);
			$agent = $this->backend->get_response();
			$agent = $agent['ResponseMessage'];
			
			
			// $pdf->Text(46.5,228.5,$agent['first_name']." ".$agent['last_name']);
			// $pdf->Text(127.5,228.5,$agent['merchant_id']);
			// $pdf->Text(36.5,235.5,$agent['contact_email']);
// 			
			// $pdf->Text(133,235.5,$agent['mobile_number']);
			
			
			$merchant_fees = $this->backend->get_product_fee($order_id);
			
			
			
			$other_info = array();
			
			$other_fee= 0;
			$total_fee = 0;
			foreach($merchant_fees as $fee)
			{
				//$total_fee = $total_fee + $fee['product_information'];
				$prod_info = $fee['product_information'];
				
				if($fee['product_name']== 'ONLINE ORDERING')
				{
					$other_info = unserialize($prod_info);
					// print_r($prod_info);
					// die();
					//$pdf->Text(80,127.5,$fee['product_information']);
							
				}
				else{ //other
					$other_fee =$fee['product_information'];
					//$pdf->Text(80,133.5,$fee['product_information']);	
				}
			}
			$total_fee = $other_fee;

			
			$_path = 'images/check_pdf.png';
			if($other_info['order_type'] =="WEBSITE")
			{
				$pdf->Image($_path,21.5,75.8,3,3);
			}else{
				$pdf->Image($_path,56,75.8,3,3);
			}
			if(isset($other_info['domain_name']))
			{
				$pdf->Text(56,78,$other_info['domain_name']);		
			}else{
				$pdf->Text(56,78,"DOMAIN NAME");
			}
			
			$pdf->Text(56,82,$other_info['template_no']);
			if(isset($other_info['domain_name_plugin']))
			{
				$pdf->Text(56,93.8,$other_info['domain_name_plugin']);		
			}else{
				$pdf->Text(56,93.8,"DOMAIN NAME PLUGIN");
			}
			
			// print_r($other_info);
			// die();
			
			if($other_info['is_existing_domain'] == "YES")
			{
				$pdf->Image($_path,79,98.5,3,3);
			}else{
				$pdf->Image($_path,94.5,98.5,3,3);
			}
			
			
			if(isset($other_info['receive_order_by'][0]))
			{
				if($other_info['receive_order_by'][0] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][0] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][0] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			if(isset($other_info['receive_order_by'][1]))
			{
				if($other_info['receive_order_by'][1] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][1] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][1] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			if(isset($other_info['receive_order_by'][2]))
			{
				if($other_info['receive_order_by'][2] =="PHONE")
				{
					//for PHONE
					$pdf->Image($_path,49,112,3,3);
					
					$pdf->Text(62,110.5,$merchant_info['office_number']);
				}
				if($other_info['receive_order_by'][2] =="FAX")
				{
					//for FAX
					$pdf->Image($_path,94.5,112,3,3);
					$pdf->Text(103.5,110.5,$merchant_info['contact_fax']);
				}
				if($other_info['receive_order_by'][2] =="EMAIL")
				{
					//for EMAIL
					$pdf->Image($_path,136,112,3,3);
					$pdf->Text(147.5,110.5,$merchant_info['contact_email']);
				}	
			}
			
			if(isset($other_info['accept_by'][0]))
			{
				if($other_info['accept_by'][0] =="Delivery")
				{
					//for PHONE
					$pdf->Image($_path,45,119,3,3);	
					
					$pdf->Text(86,118,$other_info['delivery_estimate_time']);
				}
				if($other_info['accept_by'][0] =="Pickup")
				{
					//for PHONE
					$pdf->Image($_path,70,119,3,3);
					
					$pdf->Text(62,110.5,$other_info['pickup_estimate_time']);
				}
			}
			if(isset($other_info['accept_by'][1]))
			{
				if($other_info['accept_by'][1] =="Delivery")
				{
					//for PHONE
					$pdf->Image($_path,45,119,3,3);	
					
					$pdf->Text(86,118,$other_info['delivery_estimate_time']);
				}
				if($other_info['accept_by'][1] =="Pickup")
				{
					//for PHONE
					$pdf->Image($_path,121.5,119,3,3);
					
					$pdf->Text(165,118,$other_info['pickup_estimate_time']);
				}
			}
			
			
			 
			$pdf->Text(40,124.5,$other_info['delivery_fee']);
			$pdf->Text(90,124.5,$other_info['minimum_delivery']);
			$pdf->Text(160,124.5,$other_info['radius']);
			
			if(isset($other_info['payment_method'][0]))
			{
				if($other_info['payment_method'][0] =="CASH")
				{
					//for PHONE
					$pdf->Image($_path,65.5,129.5,3,3);
					
				}
				if($other_info['payment_method'][0] =="CREDIT CARD")
				{
					$pdf->Image($_path,103.5,129.5,3,3);
				}
			}
			if(isset($other_info['payment_method'][1]))
			{
				if($other_info['payment_method'][1] =="CASH")
				{
					//for PHONE
					$pdf->Image($_path,65.5,129.5,3,3);
					
				}
				if($other_info['payment_method'][1] =="CREDIT CARD")
				{
					$pdf->Image($_path,103.5,129.5,3,3);
				}
			}
			
			
			$pdf->Text(165,128.5,$other_info['sales_tax']);
			$pdf->Text(45,132.5,$other_info['payment_gateway']);
			
			$pdf->Text(45,153,$other_info['coupon_code']);
			$pdf->Text(87,153,$other_info['title']);
			
			if(isset($other_info['discount_type'][0]))
			{
				if($other_info['discount_type'][0] =="Fixed Amount")
				{
					//for PHONE
					$pdf->Image($_path,68.2,157.2,3,3);
					$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
					
					
				}
				if($other_info['discount_type'][0] =="Percentage")
				{
					$pdf->Image($_path,127,157.2,3,3);
					$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			if(isset($other_info['discount_type'][1]))
			{
				if($other_info['discount_type'][1] =="Fixed Amount")
				{
					//for PHONE
					$pdf->Image($_path,68.2,157.2,3,3);
					$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
					
					
				}
				if($other_info['discount_type'][1] =="Percentage")
				{
					$pdf->Image($_path,127,157.2,3,3);
					$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			
			if(isset($other_info['how_many_times'][0]))
			{
				if($other_info['how_many_times'][0] =="One Time")
				{
					//for PHONE
					$pdf->Image($_path,109,161,3,3);
					//$pdf->Text(95,156.5,$other_info['fixed_amount']);
					
				}
				if($other_info['how_many_times'][0] =="Multiple")
				{
					$pdf->Image($_path,127,157.5,3,3);
					//$pdf->Text(152,156.5,$other_info['percentage']);
					
				}
			}
			if(isset($other_info['how_many_times'][1]))
			{
				if($other_info['how_many_times'][1] =="One Time")
				{
					//for PHONE
					$pdf->Image($_path,109,161,3,3);
					
					
				}
				if($other_info['how_many_times'][1] =="Multiple")
				{
					$pdf->Image($_path,137,161,3,3);
					$pdf->Text(159,160,$other_info['multiple_count']);
					
				}
			}
			
			$pdf->Text(76.5,171.2,$other_info['fee_design']);
			$pdf->Text(76.5,175.2,$other_info['fee_hosting']);
			$pdf->Text(76.5,179.2,$other_info['fee_setup']);
			$pdf->Text(76.5,182.2,$other_info['fee_service']);
			$pdf->Text(76.5,186.2,$other_fee);
			
			$total_fee = $total_fee + $other_info['fee_design'];
			$total_fee = $total_fee + $other_info['fee_hosting'];
			$total_fee = $total_fee + $other_info['fee_setup'];
			$total_fee = $total_fee + $other_info['fee_service'];
			
			
			// Array ( [order_type] => WEBSITE [template_no] => Template No. 
			// [plugin] => ONLINE ORDERING [STAND ALONE] [is_existing_domain] => YES 
			// [receive_order_by] => Array ( [0] => PHONE ) 
			// [accept_by] => Array ( [0] => Delivery [1] => Pickup ) 
			// [delivery_estimate_time] => 1 [pickup_estimate_time] => 1 
			// [delivery_fee] => 2 [minimum_delivery] => 200 [radius] => 2 
			// [payment_method] => Array ( [0] => CASH ) [sales_tax] => 788 
			// [payment_gateway] => GGE4 [coupon_code] => COUPCODE 
			// [title] => TITLECODE 
			// [discount_type] => Array ( [0] => Fixed Amount [1] => Percentage ) 
			// [fixed_amount] => 2 
			// [percentage] => 5 
			//[how_many_times] => Array ( [0] => One Time [1] => Multiple ) 
			// [multiple_count] => 10 
			// [fee_design] => 10 [fee_hosting] => 10 
			// [fee_setup] => 10 [fee_service] => 10 )
			
			
			
			// $pdf->Text(151,67.5,$merchant_info['phone2']);
			// $pdf->Text(140,88.8,$merchant_info['mobile_number']);
			// $pdf->Text(135,102,$merchant_info['contact_fax']);
			
			//http://localhost/registration/sign_application/generate_pdf_doc?partner_id=43&order_id=23
			
			$pdf->Text(76.5,193.5,number_format($total_fee,2));	
			$pdf->Text(105.5,178,$merchant_info['account_number']);
			$pdf->Text(105.5,185,$merchant_info['routing_number']);
			
			//$pdf->Text(143,127.5,$merchant_info['bank_name']);
			//$pdf->Text(143,134.5,$merchant_info['account_name']);
			//$pdf->Text(143,141.5,$merchant_info['account_number']);
			//$pdf->Text(143,147.5,$merchant_info['routing_number']);
			$pdf->Text(46.5,219.5,$merchant_info['first_name']." ".$merchant_info['last_name']);
			
			$pdf->Text(43.5,243,$agent['first_name']." ".$agent['last_name']);
			$pdf->Text(127.5,243,$agent['merchant_id']);

			$pdf->Text(44,246.3,$agent['contact_email']);

			$pdf->Text(128.5,246.5,$agent['mobile_number']);	
			
			
			$product_sign = $this->backend->get_product_order_info($order_id);
			$data = $product_sign['signature'];
			
			if($data !="")
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
		
				file_put_contents($destination_path.'/'.$order_id.'.png', $data);
				
				$pdf->Image($destination_path.'/'.$order_id.'.png',40,202,60,10);
			}
			$pdf_file =  $id.'.pdf';
	        $full_path = $destination_path . $pdf_file;   
			$pdf->Output($full_path, 'F');
		}
			
		
		header('Location: '.WEBROOT."/public/".$full_path);

	}
  }
  
?>
