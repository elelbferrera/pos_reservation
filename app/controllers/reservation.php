<?php
    class reservation extends template {
        protected $response;
		protected $merchant_id;
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
        }

       	private function send_sms()
	  	{
		  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
			$params = array(
	        	'session_id' => $_SESSION['sessionid'],
	        	// 'status' => $_POST['txtStatus'],
	        	'last_name' => $_POST['txtSourceLastName'],
	        	'first_name' => $_POST['txtSourceFirstName'],
	        	'contact_number' => $_POST['txtSourceMobile'],
	        	'create_date' => date('Y-m-d'),
	        	'message' => $_POST['txtMessage'],
	         );



	        //LC-09/11/2013
	        $response = lib::getWsResponse(API_URL, 'send_sms_to_customer', $params);
				                   
		      if (!(isset($response['respcode'], $response['respcode']))) {
		            $this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		      } elseif (!($response['respcode'] == '0000')) {
		            $this->response['success'] = false;
		            $this->response['message'] = $response['respmsg'];
		      } else {
		            $this->response = array(
		                'success' => true,
		                'message' => $response['respmsg'],
		            );
		      }
	  	}
		
		
		function hour_min($minutes){// Total
		   if($minutes <= 0) return '00 Hour(s) 00 Minute(s)';
			else    
		   return sprintf("%02d",floor($minutes / 60)).' Hour(s) '.sprintf("%02d",str_pad(($minutes % 60), 2, "0", STR_PAD_LEFT)). " Minute(s)";
		}

		private function get_refresh_events()
		{
			if(isset($_GET['date'])){
				$date_filtered = $_GET['date'];
			}else{
				$date_filtered = date("Y-m-d");
			}
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	//'date' => $prev_month,
            	'date' => $date_filtered,
            	'iscancelled' => true
            );

            // print_r($params);
            // die();
            

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservations_per_merchant', $params);
			$reservations = $response['respmsg'];
			//$this->view->assign('reservations', $reservations);
			// print_R($reservations);
			// die();



			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant', $params);
			$specialist = $response['respmsg'];

			$response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
		 	$services = $response['respmsg'];
			
			// $this->view->assign('services', $services);
			
			
			// print_r($reservations);
			// die();
			
			// $results = "resources: [";
			// foreach($specialist as $sp)
			// {
			// 	$id = $sp['id'];
			// 	//$name = $sp['first_name'] . " " .$sp['last_name'];
			// 	$name = $sp['first_name'];
			// 	$results.="{ id: '{$id}', title: '{$name}' },";
			// }
			// $results.="{ id: '-1', title: 'N/A' },";
			// $results .="],";
			
			// $results ="resources: [{ id: 'a', title: 'Room A' },
        	// { id: 'b', title: 'Room B' },
        	// { id: 'c', title: 'Room C'},
        	// { id: 'd', title: 'Room D'}";
			
			
			
			// $events = "events: [";
			// $events = "[";
			
// 			
		 	$response_merchant = lib::getWsResponse(API_URL, 'get_merchant_profile', $params);
		 	$merchant_info = $response_merchant['respmsg'];
			

		 	$strday = strtotime($date_filtered);
			$day = date("l", $strday);
			
			//$results = "[";
			$specialist_array = [];
			$specialist_id_array = [];
			foreach($specialist as $key2 => $sp)
			{
				$results = '';
				$id = $sp['id'];
				$name = $sp['first_name'];
				/*$results.="{ id: '{$id}', title: '{$name}', ";*/
				$specialist_array[$key2]['id'] = $id;
				$title = '';

				foreach ($sp['new_schedule'] as $key => $value) {
					if ($value['day'] == strtoupper($day) && $value['is_off'] == 1) {
						$title = $name . ' (DAY OFF)';
					} 
					$start_time = $value['start_time'];
					$end_time 	= $value['end_time'];
					$is_off 	= $value['is_off'];
					//$results.="{$value['day']}: '{$start_time},{$end_time},{$is_off}', ";
					$specialist_array[$key2][$value['day']] = $start_time.','.$end_time.','.$is_off;
				}

				if ($title == '') {
					//$results .= "title: '{$name}', ";
					$specialist_array[$key2]['title'] = $name;
				} else {
					//$results .= "title: '{$title}', ";
					$specialist_array[$key2]['title'] = $title;
				}
				$results .= "}";
				
				$specialist_id_array[] = $id;
			}

			$specialist_id_array[] = '-1';
			$specialist_array[] = array('id' => '-1', 'title' => $merchant_info['business_name']);
			$results.="{ id: '-1', title: '".$merchant_info['business_name']."' }";

			$hidden_fields = "";

			$events = array();
			// print_r($reservations);
			// die();
			foreach($reservations as $r)
			{
				// print_r($r);
				// die();
				
				$id = $r['id'];	
				$orig_date = substr($r['reservation_date'], 0, 10);
				$date = substr($r['reservation_date'], 0, 10);
				$status = $r['status'];
				
				// print_r($date);
				// die();
				$date .= "T".$r['reservation_time'];
				
				
				$selectedTime = $r['reservation_time'];
				$duration = $r['duration'];
				// if($duration >= 60)
				// {
					$endTime = strtotime("+{$duration} minutes", strtotime($selectedTime));
				// }else{
				// 	$endTime = strtotime("+60 minutes", strtotime($selectedTime));
				// }



				$endTime = date('H:i:s', $endTime);

				// print_r($endTime);
				// die();

				$endDate = substr($r['reservation_date'], 0, 10). "T{$endTime}";
				//echo date('h:i:s', $endTime);
				// print_r($endDate);
				// die();
				
				// $title = $r['first_name']." ".$r['last_name']. "-". $r['mobile'];
				$title = $r['first_name']." ".$r['last_name']."\n";
				// $title .= " Duration: ".$this->hour_min($duration);
				$specialist = $r['sale_person_ids'];

				$specialist_id = $r['sale_person_ids'];

				$color = $r['color'];
				if($color == "")
				{
					$color ="#00CC66";
					// print_R('lems');
					// die();
				}

				$services ="";
				$service_id ="";
				// $title .= " Service(s): ";
				foreach($r['details'] as $d)
				{
					$title .= " {$d['product_name']}, ";
					$services .= " {$d['product_name']}, ";
					$service_id .= " {$d['product_id']}, ";
				}
				
				$title = substr($title, 0, strlen($title)-2);
				$services = substr($services, 0, strlen($services)-2);
				$service_id = substr($service_id, 0, strlen($service_id)-2);

				// print_r($title);
				// die();

				// print_r($r['sales_person'][0]);
				// die();
				$last_name = $r['last_name'];
				$first_name = $r['first_name'];
				$mobile = $r['mobile'];
				$sale_person_ids = $r['sale_person_ids'];
				$product_ids = $r['product_ids'];
				$branch_id = $r['branch_id'];
				$reservation_status_id = $r['reservation_status_id'];
				$notes = $r['notes'];
				$reservation_date = $r['reservation_date'];
				$reservation_time = $r['reservation_time'];

				
				
				if($specialist !="")
				{
					$specialist_name = $r['sales_person'][0]['first_name']; 
					$specialist = explode(",", $specialist);
					foreach($specialist as $s)
					{
						// $events.="{ id: '{$id}', resourceId: '{$s[0]}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' },";
						if($status == "CANCELLED" || $status=="DONE")
						{


							// $events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}', specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}' ,overlap: true, editable: false, date:'{$orig_date}', time:'{$selectedTime}' },";

							$event = array(
								'id' => $id,
								'resourceId' => $s,
								'start' => $date,
								'end' => $endDate,
								'title' => $title,
								'backgroundColor' => $color,
								'specialist' => $specialist_name,
								'status' => $status,
								'services' => $services,
								'sale_person_ids' => $specialist_id,
								'product_ids' => $service_id,
								'overlap' => true,
								'editable' => false,
								'date' => $orig_date,
								'time' => $selectedTime

								,'last_name' => $last_name
								,'first_name' => $first_name
								,'mobile' => $mobile
								,'sale_person_ids' => $sale_person_ids
								,'product_ids' => $product_ids
								,'reservation_status_id' => $reservation_status_id
								,'notes' => $notes
								,'reservation_date' => $reservation_date
								,'reservation_time' => $reservation_time
								,'reservation_end_time' => $endTime
							);

						}else{

							// $events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}', specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}',overlap: true, date:'{$orig_date}', time:'{$selectedTime}' },";
							$event = array(
								'id' => $id,
								'resourceId' => $s,
								'start' => $date,
								'end' => $endDate,
								'title' => $title,
								'backgroundColor' => $color,
								'specialist' => $specialist_name,
								'status' => $status,
								'services' => $services,
								'sale_person_ids' => $specialist_id,
								'product_ids' => $service_id,
								'overlap' => true,
								// 'editable' => false,
								'date' => $orig_date,
								'time' => $selectedTime

								,'last_name' => $last_name
								,'first_name' => $first_name
								,'mobile' => $mobile
								,'sale_person_ids' => $sale_person_ids
								,'product_ids' => $product_ids
								,'reservation_status_id' => $reservation_status_id
								,'notes' => $notes
								,'reservation_date' => $reservation_date
								,'reservation_time' => $reservation_time
								,'reservation_end_time' => $endTime
							);

						}
						//$events.="{ id: '{$id}', resourceId: '{$s[0]}', start: '{$date}',title: '{$title}',backgroundColor: '{$color}' },";
						// $events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}',title: '{$title}',backgroundColor: '{$color}' },";
						goto nextRec;
					}
					nextRec:
				}else{
					if($status == "CANCELLED" || $status=="DONE")
					{
						// $events.="{ id: '{$id}', resourceId: '-1', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' ,specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}', overlap: true, editable: false, date:'{$orig_date}', time:'{$selectedTime}'  },";
						$event = array(
								'id' => $id,
								'resourceId' => -1,
								'start' => $date,
								'end' => $endDate,
								'title' => $title,
								'backgroundColor' => $color,
								'specialist' => "NA",
								'status' => $status,
								'services' => $services,
								'sale_person_ids' => $specialist_id,
								'product_ids' => $service_id,
								'overlap' => true,
								'editable' => false,
								'date' => $orig_date,
								'time' => $selectedTime
								,'last_name' => $last_name
								,'first_name' => $first_name
								,'mobile' => $mobile
								,'sale_person_ids' => $sale_person_ids
								,'product_ids' => $product_ids
								,'reservation_status_id' => $reservation_status_id
								,'notes' => $notes
								,'reservation_date' => $reservation_date
								,'reservation_time' => $reservation_time
								,'reservation_end_time' => $endTime
							);
					}else{
						// $events.="{ id: '{$id}', resourceId: '-1', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' ,specialist: '{$specialist_name}', status:'{$status}',, services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}', overlap: false, date:'{$orig_date}', time:'{$selectedTime}'  },";
						$event = array(
								'id' => $id,
								'resourceId' => -1,
								'start' => $date,
								'end' => $endDate,
								'title' => $title,
								'backgroundColor' => $color,
								'specialist' => "NA",
								'status' => $status,
								'services' => $services,
								'sale_person_ids' =>"",
								'product_ids' => $service_id,
								// 'overlap' => true,
								'editable' => true,
								'date' => $orig_date,
								'time' => $selectedTime
								,'last_name' => $last_name
								,'first_name' => $first_name
								,'mobile' => $mobile
								,'sale_person_ids' => $sale_person_ids
								,'product_ids' => $product_ids
								,'reservation_status_id' => $reservation_status_id
								,'notes' => $notes
								,'reservation_date' => $reservation_date
								,'reservation_time' => $reservation_time
								,'reservation_end_time' => $endTime
							);
					}
				}

				$events[] =$event;
			}
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	//'date' => $prev_month,
            	'day' => date('l', strtotime($date_filtered))
            );
			
			// $events .="]";

			$response = lib::getWsResponse(API_URL, 'get_time_per_day', $params);
			$time = $response['respmsg'];

			// return $events;
			$this->response = array(
	            'success' => true,
	            'message' => $events,
	            'resources_info' => $specialist_array,
	            'specialist_id' => $specialist_id_array,
	            'time' 	  => array('start_time'=>$time['start_time'], 'end_time'=>$time['end_time'])
	        );
		}
		
		private function change_reservation_status()
		{
			$params = array(
				'session_id' => $_SESSION['sessionid'],
				'reservation_id' => $_POST['reservation_id'],
				'status' => $_POST['status'],
				'update_date' => date('Y-m-d'),
			);

			// print_r($params);
			// die();
			
			$response = lib::getWsResponse(API_URL, 'change_reservation_status', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			        $this->response = array(
			            'success' => true,
			            'message' => $response['respmsg'],
			        );
			  }
		}


        
      	public function management($date)
        {
	
            $actions = array("add", "update", "delete", "search", 
            	"get_salon_specialist", "get_total_waiting"
            	, "get_services_by_specialist", "change_reservation_status",
            	"get_refresh_events",
            	"send_sms", "show_suggestion", "get_products_per_merchant", "get_schedule_per_specialist_per_day"
            );
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
                die();
            }
            
			if(isset($date[0])){
				$date_filtered = $date[0];
			}else{
				$date_filtered = date("Y-m-d");
			}
			
			$this->view->assign("date_selected", $date_filtered);

			
			$today = date("Y-m-d");

            $params = array(
                'session_id' => $_SESSION['sessionid'],
                'create_date' => $today,
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_waiting_list_per_merchant', $params);
            if($response['respmsg'] > 0)
            {
                $waiting_lists = $response['respmsg'];
            }else{
                $waiting_lists = array();
            }

            // print_r(count($waiting_lists));
            // die();
            $this->view->assign('waiting_count', count($waiting_lists));

			//$prev_month = date("Y-m-d", strtotime("$today"));
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	//'date' => $prev_month,
            	'date' => $date_filtered,
            	'iscancelled' => true
            );

            // print_r($params);
            // die();

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservations_per_merchant', $params);
			$reservations = $response['respmsg'];
			$this->view->assign('reservations', $reservations);

			// print_r($reservations);
			// die();



			// print_r($reservations);
			// die();
			
			// $response = lib::getWsResponse(API_URL, 'get_branch_per_merchant', $params);
			// $branches = $response['respmsg'];
			
			$branches = array();
			
			$this->view->assign('branches', $branches);
			// print_r($branches);
			// die();
			
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	// 'date' => date('Y-m-d'),
            	// 'date' => $date_filtered
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant', $params);
			$specialist = $response['respmsg'];

			// print_r($specialist);
			// die();
			$this->view->assign('specialist', $specialist);



            //LC-09/11/2013
   //          $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant_per_day', $params);
		 // 	$specialist = $response['respmsg'];
			// $this->view->assign('specialist', $specialist);

			// print_r($specialist);
			// die();
	
			$response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
		 	$services = $response['respmsg'];
			
			$this->view->assign('services', $services);

			$response_merchant = lib::getWsResponse(API_URL, 'get_merchant_profile', $params);
		 	$merchant_info = $response_merchant['respmsg'];

		 	$response_business_hours = lib::getWsResponse(API_URL, 'get_business_hours_per_merchant', $params);
		 	$business_hours = $response_business_hours['respmsg'];

		 	
		  	$businessHours = "businessHours: [";

		  	foreach ($business_hours as $key => $value) {
		  		switch ($value['day']) {
			 		case ('MONDAY'): {
			 			$dow = 1;
			 			break;
			 		} 
			 		case ('TUESDAY'): {
			 			$dow = 2;
			 			break;
			 		}
			 		case ('WEDNESDAY'): {
			 			$dow = 3;
			 			break;
			 		}
			 		case ('THURSDAY'): {
			 			$dow = 4;
			 			break;
			 		}
			 		case ('FRIDAY'): {
			 			$dow = 5;
			 			break;
			 		}
			 		case ('SATURDAY'): {
			 			$dow = 6;
			 			break;
			 		}
			 		case ('SUNDAY'): {
			 			$dow = 0;
			 			break;
			 		}
			 		default: break;
			 	}
			 	$start_time = $value['start_time'];
			 	$end_time 	= $value['end_time'];
			 	if ($value['is_close'] == 0) {
				 	$businessHours .= "{dow: [{$dow}], start: '{$start_time}', end: '{$end_time}' },";
				 }
		  		/*$businessHours .= "{"*/
		  	}

		  	$businessHours .= "],";
			
			
			// print_r($reservations);
			// die();
			
			/*$results = "resources: [";
			foreach($specialist as $sp)
			{
				$id = $sp['id'];
				//$name = $sp['first_name'] . " " .$sp['last_name'];
				$name = $sp['first_name'];
				$results.="{ id: '{$id}', title: '{$name}' },";
			}
			$results.="{ id: '-1', title: '".$merchant_info['business_name']."' },";
			$results .="],";*/

			$strday = strtotime($today);
			$day = date("l", $strday);

			$results = "resources: [";
			foreach($specialist as $sp)
			{
				$id = $sp['id'];
				//$name = $sp['first_name'] . " " .$sp['last_name'];
				$name = $sp['first_name'];
				/*$results.="{ id: '{$id}', title: '{$name}', ";*/
				$results.="{ id: '{$id}', ";
				$title = '';

				foreach ($sp['new_schedule'] as $key => $value) {
					if ($value['day'] == strtoupper($day) && $value['is_off'] == 1) {
						$title = $name . ' (DAY OFF)';
					} 
					$start_time = $value['start_time'];
					$end_time 	= $value['end_time'];
					$is_off 	= $value['is_off'];
					$results.="{$value['day']}: '{$start_time},{$end_time},{$is_off}', ";
				}

				if ($title == '') {
					$results .= "title: '{$name}', ";
				} else {
					$results .= "title: '{$title}', ";
				}

				$results .= "},";
			}
			$results.="{ id: '-1', title: '".addslashes($merchant_info['business_name'])."' },";
			$results .="],";
			
			// $results ="resources: [{ id: 'a', title: 'Room A' },
        	// { id: 'b', title: 'Room B' },
        	// { id: 'c', title: 'Room C'},
        	// { id: 'd', title: 'Room D'}";
			
			
			
			$events = "events: [";
			
			
// 			
			$hidden_fields = "";

			// print_r($reservations);
			// die();
			foreach($reservations as $r)
			{
				// print_r($r);
				// die();
				
				$id = $r['id'];	
				$orig_date = substr($r['reservation_date'], 0, 10);
				$date = substr($r['reservation_date'], 0, 10);
				$status = $r['status'];
				
				// print_r($date);
				// die();
				$date .= "T".$r['reservation_time'];
				
				
				$selectedTime = $r['reservation_time'];
				$duration = $r['duration'];
				// if($duration >= 60)
				// {
					$endTime = strtotime("+{$duration} minutes", strtotime($selectedTime));
				// }else{
				// 	$endTime = strtotime("+60 minutes", strtotime($selectedTime));
				// }



				$endTime = date('H:i:s', $endTime);

				// print_r($endTime);
				// die();

				$endDate = substr($r['reservation_date'], 0, 10). "T{$endTime}";
				//echo date('h:i:s', $endTime);
				// print_r($endDate);
				// die();
				
				// $title = $r['first_name']." ".$r['last_name']. "-". $r['mobile'];
				$title = $r['first_name']." ".$r['last_name'] .' \n';
				// $title .= " Duration: ".$this->hour_min($duration);
				
				//$specialist = $r['sale_person_ids'];
				$specialist =  $r['sale_person_ids']  == "" ? -1 : $r['sale_person_ids'];

				$specialist_id = $r['sale_person_ids'];

				$color = $r['color'];
				if($color == "")
				{
					$color ="#00CC66";
					// print_R('lems');
					// die();
				}

				$services ="";
				$service_id ="";
				// $title .= " Service(s): ";
				foreach($r['details'] as $d)
				{
					$title .= " {$d['product_name']}, ";
					$services .= " {$d['product_name']}, ";
					$service_id .= " {$d['product_id']}, ";
				}
				
				$title = substr($title, 0, strlen($title)-2);
				$services = substr($services, 0, strlen($services)-2);
				$service_id = substr($service_id, 0, strlen($service_id)-2);

				// print_r($title);
				// die();

				// print_r($r['sales_person'][0]);
				// die();
				// <div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
				// <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
				// <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
				// <div style="display: none" id="{$item.id}mobile" name="{$item.id}mobile">{$item.mobile}</div>
				// <div style="display: none" id="{$item.id}reservation_date" name="{$item.id}reservation_date">{$item.reservation_date}</div>
				// <div style="display: none" id="{$item.id}reservation_time" name="{$item.id}reservation_time">{$item.reservation_time}</div>

				// <div style="display: none" id="{$item.id}sale_person_ids" name="{$item.id}sale_person_ids">{$item.sale_person_ids}</div>  
				// <div style="display: none" id="{$item.id}product_ids" name="{$item.id}product_ids">{$item.product_ids}</div> 
				// <div style="display: none" id="{$item.id}branch_id" name="{$item.id}branch_id">{$item.branch_id}</div>
				// <div style="display: none" id="{$item.id}reservation_status_id" name="{$item.id}reservation_status_id">{$item.reservation_status_id}</div>
				// <div style="display: none" id="{$item.id}notes" name="{$item.id}notes">{$item.notes}</div> 	
				$last_name = $r['last_name'];
				$first_name = $r['first_name'];
				$mobile = $r['mobile'];
				$sale_person_ids = $r['sale_person_ids'];
				$product_ids = $r['product_ids'];
				$branch_id = $r['branch_id'];
				$reservation_status_id = $r['reservation_status_id'];
				$notes = addslashes($r['notes']);
				$reservation_date = $r['reservation_date'];
				$reservation_time = $r['reservation_time'];


				if(isset($r['sales_person'][0]))
				{
					$specialist_name = $r['sales_person'][0]['first_name']; 	
				}else{
					$specialist_name ="NA";
				}

				// print_r($specialist_name);
				// die();
				
				
				// print_r($specialist_name);
				// die();
				if($specialist !="")
				{

					$specialist = explode(",", $specialist);
					foreach($specialist as $s)
					{
						// $events.="{ id: '{$id}', resourceId: '{$s[0]}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' },";
						if($status == "CANCELLED" || $status=="DONE")
						{
							$events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}', specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}' ,overlap: true, editable: false, date:'{$orig_date}', time:'{$selectedTime}' 
								,last_name: '{$last_name}'
								,first_name: '{$first_name}'
								,mobile: '{$mobile}'
								,sale_person_ids: '{$sale_person_ids}'
								,product_ids: '{$product_ids}'
								,reservation_status_id: '{$reservation_status_id}'
								,notes: '{$notes}'
								,reservation_date: '{$reservation_date}'
								,reservation_time: '{$reservation_time}'
								,reservation_end_time : '{$endTime}'
								},";

						}else{

							$events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}', specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}',overlap: true, date:'{$orig_date}', time:'{$selectedTime}' 
								,last_name: '{$last_name}'
								,first_name: '{$first_name}'
								,mobile: '{$mobile}'
								,sale_person_ids: '{$sale_person_ids}'
								,product_ids: '{$product_ids}'
								,reservation_status_id: '{$reservation_status_id}'
								,notes: '{$notes}'
								,reservation_date: '{$reservation_date}'
								,reservation_time: '{$reservation_time}'
								,reservation_end_time : '{$endTime}'
								},";
						}
						//$events.="{ id: '{$id}', resourceId: '{$s[0]}', start: '{$date}',title: '{$title}',backgroundColor: '{$color}' },";
						// $events.="{ id: '{$id}', resourceId: '{$s}', start: '{$date}',title: '{$title}',backgroundColor: '{$color}' },";
						goto nextRec;
					}
					nextRec:
				}else{
					if($status == "CANCELLED" || $status=="DONE")
					{
						$events.="{ id: '{$id}', resourceId: '-1', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' ,specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}', overlap: true, editable: false, date:'{$orig_date}', time:'{$selectedTime}'  
							,last_name: '{$last_name}'
							,first_name: '{$first_name}'
							,mobile: '{$mobile}'
							,sale_person_ids: '{$sale_person_ids}'
							,product_ids: '{$product_ids}'
							,reservation_status_id: '{$reservation_status_id}'
							,notes: '{$notes}'
							,reservation_date: '{$reservation_date}'
							,reservation_time: '{$reservation_time}'
							,reservation_end_time : '{$endTime}'
							},";
					}else{
						$events.="{ id: '{$id}', resourceId: '-1', start: '{$date}', end: '{$endDate}' ,title: '{$title}',backgroundColor: '{$color}' ,specialist: '{$specialist_name}', status:'{$status}', services:'{$services}', sale_person_ids:'{$specialist_id}', product_ids: '{$service_id}', overlap: true, date:'{$orig_date}', time:'{$selectedTime}' 
							,last_name: '{$last_name}'
							,first_name: '{$first_name}'
							,mobile: '{$mobile}'
							,sale_person_ids: '{$sale_person_ids}'
							,product_ids: '{$product_ids}'
							,reservation_status_id: '{$reservation_status_id}'
							,notes: '{$notes}'
							,reservation_date: '{$reservation_date}'
							,reservation_time: '{$reservation_time}'
							,reservation_end_time : '{$endTime}'
							},";
					}
				}
			}
			
			
			$events .="],";;
			
			// print_r($events);
			// die();
			  // events: [
		        // // { id: '1', resourceId: 'a', start: '2016-12-06', end: '2016-12-08', title: 'event 1',backgroundColor: "#0073b7",borderColor: "#0073b7"  },
		        // { id: '2', resourceId: '4', start: '2016-12-19T22:00', title: 'event 2' },
		        // { id: '3', resourceId: '5', start: '2016-12-07T12:00:00', end: '2016-12-08T06:00:00', title: 'event 3' },
		        // { id: '4', resourceId: '6', start: '2016-12-07T07:30:00', end: '2016-12-07T09:30:00', title: 'event 4' },
		        // { id: '5', resourceId: '5', start: '2016-12-07T10:00:00', end: '2016-12-07T15:00:00', title: 'event 5' }
		      // ],
			 
			// print_r($reservations);
			// die(); 
			
			$this->view->assign('events', $events);
			$this->view->assign('resources_info', $results);
			$this->view->assign('businessHours', $businessHours);

			// print_r($results);
			// die();
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservation_status_per_merchant', $params);
		 	$status = $response['respmsg'];
			$this->view->assign('status', $status);


			$this->view->assign('business_start_time', $_SESSION['business_start_time']);
			$this->view->assign('business_end_time', $_SESSION['business_end_time']);

			
        }

      private function get_total_waiting()
      {

      		$today = date("Y-m-d");

            $params = array(
                'session_id' => $_SESSION['sessionid'],
                'create_date' => $today,
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_waiting_list_per_merchant', $params);
            if($response['respmsg'] > 0)
            {
                $waiting_lists = $response['respmsg'];
            }else{
                $waiting_lists = array();
            }

            // $this->view->assign('waiting_count', count($waiting_lists));
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        //$this->response['message'] = 'System error, unable to connect to database';
			        $this->response['count'] =0;
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['count'] =0;
			  } else {
			  		$message ="";
			  		//$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'count' => count($waiting_lists),
			        );
			  }
      }

      private function get_schedule_per_specialist_per_day() {
      	$params = array(
      		'session_id' => $_SESSION['sessionid'],
			'sales_person_id' => $_POST['sales_person_id'],
			'date' => $_POST['date']
  		);

  		$response = lib::getWsResponse(API_URL, 'get_schedule_per_specialist_per_day', $params);
	 	
	 	if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$message ="";
			  		
			  		$resp = $response['respmsg'];
			        return $this->response = array(
			            'success' => true,
			            'message' => $resp
			        );
			  }

      }

      private function get_services_by_specialist()
      {
      		$params = array(
				'session_id' => $_SESSION['sessionid'],
				'specialist_id' => $_POST['specialist'],
			);

			if ($params['specialist_id'] == -1 || $params['specialist_id'] == '') { 
				$response = lib::getWsResponse(API_URL, 'get_product_categories', $params);
				if (!(isset($response['respcode'], $response['respcode']))) {
		        	$this->response['success'] = false;
		        	$this->response['message'] = 'System error, unable to connect to database';
			  	} elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
				} else {
					$message ="No categories define";

				  	if(count($response['respmsg']> 0)) {
					 	$message ="";
				 	}

				 	$categories_msg = '<ul class="nav nav-tabs" role="tablist">';
				 	
				 	$response_products = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);

				 	if (!(isset($response_products['respcode'], $response_products['respcode']))) {
			        	$this->response_products['success'] = false;
			        	$this->response_products['message'] = 'System error, unable to connect to database';
				  	} elseif (!($response_products['respcode'] == '0000')) {
				        $this->response_products['success'] = false;
				        $this->response_products['message'] = $response_products['respmsg'];
					} else {
						$products_msg = '<div class="tab-content">';
					}

					$product_ids ="";
			  		if(isset($_POST['product_ids']))
			  		{
			  			$product_ids = $_POST['product_ids'];
			  		}

			  		foreach ($response['respmsg'] as $key => $category) {
				 		$href = 'cat' . $category['id'];
				 		if ($key == 0) {
				 			$categories_msg .= '<li role="presentation" class="active"><a href="#'.$href.'" aria-controls="'.$href.'" role="tab" data-toggle="tab"><strong>'.($category['name'] == "" ? 'N/A' : $category['name']).'</strong></a></li>';
				 			$products_msg .= '<div role="tabpanel" class="tab-pane active" id="'.$href.'">';
				 		} else {
				 			$categories_msg .= '<li role="presentation"><a href="#'.$href.'" aria-controls="'.$href.'" role="tab" data-toggle="tab"><strong>'.($category['name'] == "" ? 'N/A' : $category['name']).'</strong></a></li>';
				 			$products_msg .= '<div role="tabpanel" class="tab-pane" id="'.$href.'">';
				 		}

				 		$ctr =2;
				 		foreach($response_products['respmsg'] as $key => $product) {
				 			if ($product['category_id'] == $category['id']) {
				 				$id = $product['id'];
					  			$name = $product['name'];
					  			$price = "$".number_format($product['price'], 2);

					  			$check ="";
				  				if($product_ids !=="")
				  				{
				  					if(strpos($product_ids, "{$id},")!== false)
				  					{
				  						$check ="checked";
				  					}
				  				}
				  				// print_r($product_ids);
				  				// die();

				  				$products_msg .="<div class='col-md-6 col-sm-6'>
		                                <label style='margin-right: 5px; font-size: 14px;'><input type='checkbox' {$check} value='{$id}' id='services[]' name='services[]' class='flat-red'> {$name}</label> <span style='font-size:14px; color:#3b914d; font-weight:bold;'>{$price}</span>
		                            </div>";
				  				$ctr++;
				 			}
				 		}

				 		$products_msg .= '</div>';
				 	}
				 	$categories_msg .= '</ul>';
				}

				$message = $categories_msg . $products_msg;
				
			} else {


				//LC-09/11/2013
				$response = lib::getWsResponse(API_URL, 'get_products_per_merchant_per_specialist', $params);
				/*echo "<pre>";
				var_dump($response);die;*/
				$message = "";

				  if (!(isset($response['respcode'], $response['respcode']))) {
				        $this->response['success'] = false;
				        $this->response['message'] = 'System error, unable to connect to database';
				  } elseif (!($response['respcode'] == '0000')) {
				        $this->response['success'] = false;
				        $this->response['message'] = $response['respmsg'];
				  } else {
				  	$message ="No services define";

				  	if(count($response['respmsg']> 0))
						 {
						 	$message ="";
						 }
				  		$responseN = lib::getWsResponse(API_URL, 'get_product_category_per_merchant_per_specialist', $params);

				  		  if (!(isset($responseN['respcode'], $responseN['respcode']))) {
						        $this->responseN['success'] = false;
						        $this->responseN['message'] = 'System error, unable to connect to database';
						  } elseif (!($responseN['respcode'] == '0000')) {
						        $this->responseN['success'] = false;
						        $this->responseN['message'] = $responseN['respmsg'];
						  } else {
						  	$message ='<ul class="nav nav-tabs" role="tablist">';
	    
						  	foreach ($responseN['respmsg'] as $key => $value) {
						  		$href = 'cat' . $value['category_id'];
						  		if ($key == 0) {
						  			$message .= '<li role="presentation" class="active"><a href="#'.$href.'" aria-controls="'.$href.'" role="tab" data-toggle="tab"><strong>'.($value['category_name'] == "" ? 'N/A' : $value['category_name']).'</strong></a></li>';
						  		} else {
						  			$message .= '<li role="presentation"><a href="#'.$href.'" aria-controls="'.$href.'" role="tab" data-toggle="tab"><strong>'.($value['category_name'] == "" ? 'N/A' : $value['category_name']).'</strong></a></li>';
						  		}
						  	}

						  	$message .='</ul><div class="tab-content">';

						  	$product_ids ="";
					  		if(isset($_POST['product_ids']))
					  		{
					  			$product_ids = $_POST['product_ids'];
					  		}

					  		

						  	foreach ($responseN['respmsg'] as $key => $value) {
						  		$href = 'cat' . $value['category_id'];
						  		if ($key == 0) {
						  			$message .= '<div role="tabpanel" class="tab-pane active" id="'.$href.'">';
						  		} else {
						  			$message .= '<div role="tabpanel" class="tab-pane" id="'.$href.'">';
						  		}

						  		$ctr =2;
						  		foreach($response['respmsg'] as $r)
						  		{
						  			if ($r['category_id'] == $value['category_id']) {
						  			$id = $r['id'];
						  			$name = $r['name'];
						  			$price = "$".number_format($r['price'], 2);

						  			$check ="";
					  				if($product_ids !=="")
					  				{
					  					if(strpos($product_ids, "{$id},")!== false)
					  					{
					  						$check ="checked";
					  					}
					  				}
					  				// print_r($product_ids);
					  				// die();

					  				$message .="<div class='col-md-6 col-sm-6'>
			                                <label style='margin-right: 5px; font-size: 14px;'><input type='checkbox' {$check} value='{$id}' id='services[]' name='services[]' class='flat-red'> {$name}</label> <span style='font-size:14px; color:#3b914d; font-weight:bold;'>{$price}</span>
			                            </div>";
					  				$ctr++;
					  				}
						  		}

						  		$message .= "</div>";

						  	}

						  	$message .= "</div>";



						  }

						  //var_dump($message);die;

				  		
						/*$product_ids ="";
				  		if(isset($_POST['product_ids']))
				  		{
				  			$product_ids = $_POST['product_ids'];
				  		}*/

						// <div class="row">
						// {foreach from=$services item=item}
						//   <div class="col-md-6 col-sm-6">
						//             <label style="margin-right: 5px; font-size: 20px;">
						//             <input type="checkbox" id="services[]" value="{$item.id}" name="services[]" height="50" class="flat-red"> <span style="margin-left:5px;">{$item.name}</span>  <span style="font-size:20px; color:#3b914d; font-weight:bold;">${$item.price|string_format:"%.2f"}</span></label> 
						          
						//   </div>
						// {/foreach}
						 

						/* $ctr =2;

						 $message .="<span class='step_descr section sidebar-title'>Select Services</span>";
				  		foreach($response['respmsg'] as $r)
				  		{
				  			$id = $r['id'];
				  			$name = $r['name'];
				  			$price = "$".number_format($r['price'], 2);*/
				  			// $price = "";
			  				// if($ctr%2==0)
			  				// {
			  				// 	$message .="<div class='row'>";
			  				// 	$is_for_close = true;
			  				// }
			  				// 	$message .="
			  				// 	<div class='col-md-6 col-sm-6'>
						   //          <label style='margin-right: 5px; font-size: 15px;'>
						   //          <input type='checkbox' id='services[]' value='{$id}' name='services[]' height='50' class='flat-red'> <span style='margin-left:5px;'>{$name}</span>  <span style='font-size:20px; color:#3b914d; font-weight:bold;'>{$price}</span>
						   //          </label> 
						   //      </div>";


			  				// if($ctr%2>0 )
			  				// {
			  				// 	$message .="</div>";
			  				// }

			  				/*$check ="";
			  				if($product_ids !=="")
			  				{
			  					if(strpos($product_ids, "{$id},")!== false)
			  					{
			  						$check ="checked";
			  					}
			  				}
			  				// print_r($product_ids);
			  				// die();

			  				$message .="<div class='col-md-6 col-sm-6'>
	                                <label style='margin-right: 5px; font-size: 14px;'><input type='checkbox' {$check} value='{$id}' id='services[]' name='services[]' class='flat-red'> {$name}</label> <span style='font-size:14px; color:#3b914d; font-weight:bold;'>{$price}</span>
	                            </div>";
			  				$ctr++;
				  		}*/

				  		// print_r($message);
				  		// die();

				  		// print_r($message);
				  		// die();

				  		
				  		//var_dump($message);die;

				  		
				  }
			}

			$resp = $response['respmsg'];
	        $this->response = array(
	            'success' => true,
	            'message' => $message,
	        );
      }


      
      	// <div class="row">
		// 			{foreach from=$services item=item}
		// 			  <div class="col-md-6 col-sm-6">
		// 			            <label style="margin-right: 5px; font-size: 20px;">
		// 			            <input type="checkbox" id="services[]" value="{$item.id}" name="services[]" height="50" class="flat-red"> <span style="margin-left:5px;">{$item.name}</span>  <span style="font-size:20px; color:#3b914d; font-weight:bold;">${$item.price|string_format:"%.2f"}</span></label> 
					          
		// 			  </div>
		// 			{/foreach}
					  
		//           </div>

      private function get_salon_specialist()
      {
      		$params = array(
				'session_id' => $_SESSION['sessionid'],
				'services' => $_POST['services'],
				'date' => $_POST['date'],
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant_per_day', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$message ="";
			  		foreach($response['respmsg'] as $r)
			  		{
			  			$id =$r['id'];	
			  			$first_name =$r['first_name'];
			  			$last_name =$r['last_name'];
						$message .= "<div class='col-md-6 col-sm-6'>
	                        <label style='margin-right: 5px; font-size: 14px;'>
	                        	<input type='checkbox' value='{$id}' id='specialist[]' name='specialist[]' class='flat-red'> 
	                        	{$first_name} {$last_name}
	                        </label> 
	                  	</div>";

			  		}
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'message' => $message,
			        );
			  }
      }
        
	  private function search()
	  {
			$params = array(
				'session_id' => $_SESSION['sessionid'],
				'mobile' => str_replace("-", "", $_POST['txtMobileSearch']) 
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'search_customer_by_mobile', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'last_name' => $resp['last_name'],
			            'first_name' => $resp['first_name'],
			            'mobile' => $resp['mobile'],
			            'email_address' => $resp['email_address']
			        );
			  }
	  }
	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
        	'email_address' => $_POST['txtEmail'],
         );
		// print_r($params);
		// die();

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'create_reservation_via_merchant', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtReservationId'],
        	'reservation_status_id' => $_POST['txtStatus'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );
		
		// print_r($params);
		// die();
        //LC-09/11/2013

        $params2 = array(
        	'session_id' => $_SESSION['sessionid'],
        	'date' => $_POST['txtDate'],
        	'sales_person_id' => $_POST['txtSpecialist'],
        );

        $response = lib::getWsResponse(API_URL, 'get_schedule_per_specialist_per_day', $params2);

        //var_dump($response);die;
        	                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	      		$sched = $response['respmsg'];

	      		if ($sched[0]['is_off'] == 1) {
	      			return $this->response = array(
		                'success' => 'na',
		                'message' => $response['respmsg']
		            );
	      		}
	      }
        
        $response = lib::getWsResponse(API_URL, 'update_reservation', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	
		
        
        protected function delete()
        {
            $id = $_GET['id'];
            $result = $this->backend->delete_branch($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
        }

        private function show_suggestion() {
            /*if($_POST['txtSpecialist'] !== "") {*/
           	// added for checking if this will work on updating reservation
            $date = explode("-", $_POST['txtDate']);
        	$date = $date[2] . "-" . $date[0] . "-" . $date[1];
        	// --- end

                $post_data = array(
                    'method'            => 'get_reservation_suggestions',
                    'merchant_id'       => $_SESSION['merchant_id'],
                    'product_ids'       => $_POST['txtProducts'],
                    'specialist_ids'    => $_POST['txtSpecialist'],
                    'reservation_date'  => $date/*$_POST['txtDate']*/,
                );


                $response = lib::getWsResponse(API_URL, 'get_reservation_suggestions', $post_data);
                //var_dump($response);die;
                
                    if (!(isset($response['respcode'], $response['respmsg']))) {
                        $this->response['success'] = false;
                        $this->response['message'] = 'System error, unable to connect to database';
                        return;
                    } elseif ($response['respcode'] == '0000') {
                        // $this->layout = "json";


                        $this->response['success'] = true;
                        $this->response['message'] = "Conflict";
                        // return;
                        

                        $suggestions = $response['respmsg'];
                        $s_count = 0;
                        
                        $innerHTML="";
                        foreach($suggestions as $s)
                        {
                            $s_count++;

                            $time = $s['time'];
                            $m_time = $s['m_time'];
                            $service = $s['service'];
                            $service_id = $s['service_id'];
                            $date = $s['date'];
                            $specialist_name = $s['first_name']." ". $s['last_name'];
                            $specialist_id = $s['specialist_id'];

                            $opacity ="";

                            $innerHTML .="
                                    <input type='hidden' name='txtData{$s_count}' id='txtData{$s_count}' value='{$time}|{$date}|{$specialist_id}|{$service_id}'>
                                    <li class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                  <div class='coupon_content suggestion-li'>
                                        <div class='row'>
                                            <div class='col-lg-6 col-sm-6 col-xs-12'>
                                                 <div class='suggestion-title'><strong>Time</strong>: {$time}</div>
                                                 <div class='suggestion-title'><strong>Specialist</strong>: {$specialist_name}</div>
                                                 <div class='suggestion-title'><strong>Services:</strong><br /> </div>
                                                 <p>{$service}</p>
                                            </div>
                                            <div class='col-lg-6 col-sm-6 col-xs-8'>
                                                 <button type='button' class='tabbtn btn btn-primary' id='btnReserveMySuggestion' name='btnReserveMySuggestion' style='width:100%' onClick='ReserveMe({$s_count})'><i class='fa fa-arrow-circle-right'></i> Reserve Now</button>
                                            </div>
                                        </div>
                                  </div>
                                </li>";
                        }

                        $this->response['suggestions'] = $innerHTML;
                        //print_r($response['suggestions']);die;
                        return;
                    }
            /*}*/
        }
        
        
    }
?>
