<?php
class schedule extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
   
        
       public function sched_list(){
       
//        $actions = array('get_center');
//        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
//            $action = $_GET['action'];
//            $this->layout = 'json';
//            $this->$action();
//        }
           
         
        $schedule_list = $this->backend->get_po_schedule_all();
        $response = $this->backend->get_response();
        $schedule_list = $response['ResponseMessage'];
           
        
        $out_schedule_list = array(); 
        foreach ($schedule_list as $row) {
           // print_r($row);die();
            $dates=$this->get_check_my_date($row);
            if($dates['istorun']){
                
                //get_number of final_dispo per po
                
              $out_schedule_list[] = array(
                'id'=> $row['po_no'] ,     //sprintf('%06d',$row['po_no']), 3/18
                'state'=> $row['state'],
                'project_name'=> $row['name'],
                'goal'=>$row['goal_value'],
                'goal_type'=> $row['goal_type'],
                'end_date'=> $row['end_date'],
                'center'=>$row['center'],
                'cid'=>$row['caller_id'],
                'notes'=>$row['goal_note'],
                );
//                print_r($out_schedule_list[0]);
//                die();
              
           }    
        }
            
          $this->view->assign('schedule_list', $out_schedule_list);
//           print_r($out_schedule_list[0]);
//           die();
//               
       
      //  $dates=$this->get_check_my_date($out_schedule_list['end_date'],$out_schedule_list['goal_days_of_week']);
           
                
     
        
    }
    
 protected function get_check_my_date($params)
    {
               
//              print_r($params);
//              die();
              
               $istorun=0;
                
                $dates   = array();
                $current =  strtotime("now");
                $my_today = $current;
                $end_date = strtotime($params['end_date']);
               
                //parse the day of the week
             
                $days_separated=array(explode('-',$params['goal_days_of_week'])); 
             
//                print_r($days_separated);
//                die();
                foreach($days_separated as $key=>$val){

                     
                     if(!empty($val[0])){
//                         print_r($params);die();
                         $sun=$val[0];
                     }else{$sun='';}
                    if(!empty($val[1])){
                         $mon=$val[1];
                     }else{$mon="";}
                    
                     if(!empty($val[2])){
                         $tue=$val[2];
                     } else{$tue="";} 
                     if(!empty($val[3])){
                         $wed=$val[3];
                     }else{$wed="";}
                    if(!empty($val[4])){
                         $thu=$val[4];
                     }else{$thu="";}
                    if(!empty($val[5])){
                         $fri=$val[5];
                     }else{$fri="";}
                       if(!empty($val[6])){
                         $sat=$val[6];
                     }else{$sat="";}

                         
                     
//                    
                     
                    
                    
                }
                 $end_date = strtotime('+1 day', $end_date);
               

                //loop the dates between the date range9o
                while( $current <= $end_date) {
                    
                    $curr_day_week=date('w', $current);
//                    print_r($curr_day_week.",");
                   
                    
                //    echo date('Y-m-d' , $current);
                  if($curr_day_week==0 and $sun<>'') {
                        
                        $dates[] = date('Y-m-d' , $current);
                       if($my_today==$current){
                            $istorun = 1;
                        }   
                        
                    }
                    if($curr_day_week==1 and $mon<>null){
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                           
                        }
                    }
                    if($curr_day_week==2 and $tue<>null){
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                            
                        }
                    }
                    if($curr_day_week==3 and $wed<>null){
                      
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                                                   }
                    }
                    if($curr_day_week==4 and $thu<>null){
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                        }
                            
                    }
                    if($curr_day_week==5 and $fri<>null){
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                        }
                    }
                    if($curr_day_week==6 and $sat<>null){
                        $dates[] = date('Y-m-d' , $current);
                        if($my_today==$current){
                            $istorun = 1;
                        }
                    }
                    
                    
                    //last date is not being processed
                    
                    $current = strtotime('+1 day', $current);
//                     echo 'Current' . date('Y-m-d',$current);
//                     echo 'End' .    date('Y-m-d',$end_date);
                 
                
                }
      
//                   print_r($dates);
//                   die();
//                 
     
                    $arr_cnt=count($dates);           
               
       return array('date_count' => $arr_cnt,
                    'istorun' => $istorun);
        
        
    }
     
    protected function get_center()
    {
    $user= $_GET['user'];
               
        $params = array(
        'user' => $user,
        );
        $result = $this->backend->get_goal_by_user($params);
        $response = $this->backend->get_response();
       
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
           
            foreach ($response['ResponseMessage'] as $key => $val) {
             
                $search_result[] = array(
                        'formated_id'                 =>sprintf('%06d',$val['id']),
                        'name'                        =>$val['name'], 
                        'client_code'                 =>$val['client_code'],
                        'last_po_no'                  =>$val['last_po_no'], 
                        'street'                      =>$val['street'], 
                        'city'                        =>$val['city'], 
                        'state'                       =>$val['state'], 
                        'zip'                         =>$val['zip'], 
                        'phone'                       =>$val['phone'], 
                        'fax'                         =>$val['fax'], 
                        'notes'                       =>$val['notes'], 
                        'default_report_status'       =>$val['default_report_status'], 
                        'databack_status'             =>$val['databack_status'], 
                        'basket_notification_status'  =>$val['basket_notification_status'], 
                        'databack_fields'             =>unserialize($val['databack_fields']),
                        'allow_co_fields'             =>unserialize($val['allow_co_fields']),
                        'co_notification_status'      =>$val['co_notification_status'],
                        'schedule_notification_status'=>$val['schedule_notification_status'], 
                        'schedule_access'             =>$val['schedule_access'], 
                        'show_discount_code'          =>$val['show_discount_code'],

            );
            }
          
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
            );
        }          
   }
    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
