<?php
class pomaintenance extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('csv','tsv','zip');
    protected $allowed_types = array('text/csv','text/x-csv','text/comma-separated-values','application/vnd.ms-excel','application/zip', 'application/x-zip-compressed', 
'multipart/x-zip', 'application/x-compressed');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'uploads/';
        $this->download_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'downloads/';
        $this->incoming_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'incoming/';  
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    } 
     protected function hide_show_completed()
     {
        $status=$_GET['status']; 
        $po = $this->backend->get_po($status);
        $response = $this->backend->get_response();
        $po = $response['ResponseMessage'];
        $out_po = array(); 
         //User type for access rights    
      /*  $user_type = $this->backend->get_usertype($_SESSION['username']);*/
       $user_type   = $_SESSION['user_type_desc'];
        foreach ($po as $row) {
            
             if(in_array($user_type,array('Centers','Clients','Data','Admin','System Admin','Managers'))){
                $view_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="View PO" />';
            }else{
                $view_button="";
            }
            if(in_array($user_type,array('Clients','Data','Admin','System Admin','Managers'))){
                $edit_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="Edit PO" />';
            }else{
                $edit_button="";
            }
            
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="DeletePO('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
            
             if($this->backend->check_if_has_script($row['id']) && in_array($user_type,array('Data','Managers','Clients','Centers','Admin','System Admin'))){
                $view_script=' <a href='.WEBROOT.'/pomaintenance/view_script/'. $row['id'].'>
                <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/script.png" alt="View Script" /></a>';   
            }else{
                $view_script='';   
            }  
            if($row['status']!='ACTIVE' && in_array($user_type,array('Data','Managers','Clients','Admin','System Admin'))){
              $edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'>  <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/edit_script.png" alt="Edit Script" /></a>';
                                                                                                                
            }else{
                $edit_script  ='';
            }
            
               //Report
                $report_module = "";
               if( $row['allow_report']==1 && $this->backend->check_data_if_result_exist($row['id'])>0 || $this->backend->check_data_if_manual_data_exist($row['id'])>0  && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))){
                    $report_module=' <a href="'.WEBROOT.'/pomaintenance/report?po_id='. $row['id'].'">'."<img src='".WEBROOT."/public/images/generate.png' alt='View Script' style='height: 25px;width: 25px;' />".'</a>';
                }else{
                    $report_module="";
                }
            //
            
            
            $out_po[] = array(
                $row['client_code'].sprintf('%06d',$row['id']),
                $row['client_name'],
                '<u><a  href='.WEBROOT.'/pomaintenance/project_glance/'. $row['id'].'>'.$row['po_name'].'</a></u>',
                $row['status'],
                $row['template_name'],
               $row['start_date'],
               $row['end_date'],
               $row['universe'],
               $row['complete'],
                $edit_button
               .$view_button
               .$delete_button 
               .$view_script
               .$edit_script
               .$report_module
                
            );
        }

            $this->response = array(
                'data'=>$out_po,
                'success' => true,
                'message' => "",
            ); 
         
     }
     public function co_response($get="") {
        $actions = array('add_co_response');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
        $status = array(
            'N'=>'NEW',
            'A'=>'ASSIGNED',
            'P'=>'PROCESSING',
            'H'=>'HOLD',
            'C'=>'COMPLETED',
        );
        $change_order_type=array(
        '1'=> 'Date / Time Change',
        '2'=> 'Script Change',
        '3'=> 'Project Hold',
        '4'=> 'Data Change',
        '5'=> 'Complaint Investigation',
        '6'=> 'Monitor Request',
        '7'=> 'Goals Change',
        '8'=> 'Other',);
                                  
      if($get[0]>0){ 
          $params=array(
            'po_id'=>$get[0],
            'co_id'=>$get[1],
          );
     
        
        $center_list=$this->backend->get_center_for_co($get[0]);
        $center_id = 0;
         foreach ($center_list as $key => $c) {
            $center_id = $key;
         }
        $co_response_list=$this->backend->get_co_response($params);
        $co_info=$this->backend->get_for_coby_id($params);
        $this->view->assign('co_response_list', $co_response_list);
        $user_list=$this->backend->get_users_per_center($center_id);   
        $this->view->assign('change_order_type', $change_order_type);
        $this->view->assign('center_list', $center_list);
        $this->view->assign('user_list', $user_list);
        $this->view->assign('status', $status);
        $this->view->assign('po_id', $get[0]);
        $this->view->assign('co_id', $get[1]);
        
        $this->view->assign('co_formated_id','CO'.sprintf('%06d',$co_info[0]['id']) );
        $this->view->assign('co_status', $co_info[0]['status']);
        $this->view->assign('creator', $co_info[0]['creator']);
        $this->view->assign('change_type', $co_info[0]['change_type']);
        $this->view->assign('description', $co_info[0]['description']);
        $formated_po_id=$this->backend->get_po_number($get[0]);
        $this->view->assign('formated_po_id', $formated_po_id);     
        }
          //User type for access rights    
            /*$user_type = $this->backend->get_usertype($_SESSION['username']);
            $this->view->assign('user_type', $user_type); */
            $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
        //$params = array('id' => $get[0],); 
        
    
    }
    public function add_co_response()
    {
       
      $result = $this->backend->add_co_response($_POST);
      $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
          $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => "Response has been submitted.",
            );   
        }  
    }
    
    
    public function create_change_order()
    {

      $result = $this->backend->create_change_order($_POST);
      $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
               $params=array(
            'po_id'=>$_POST['txtID'],
          );
            $co_list = $this->backend->get_change_order_list($params);
            $response = $this->backend->get_response();
            $co_list = $response['ResponseMessage'];
            $out_co_list = array(); 
            
            foreach ($co_list as $row) {
                
                $out_co_list[] = array(
                     '<u><a  href='.WEBROOT.'/pomaintenance/co_response/'.$_POST['txtID'].'/'.$row['id'].'>'.'CO'.sprintf('%06d',$row['id']).'</a></u>',
                    $row['create_date'],
                    $row['change_type'],
                    $row['description'],
                    $row['assigned'],
                    $row['status'],
                );
            }
            
            
            
          $this->response = array(
                'data'=>$out_co_list,
                'success' => true,
                'message' => "New change order has beeen submitted.",
            );   
            
            
        }  
        
        
    }
      public function change_order($get="") {
        $actions = array('create_change_order');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
        $status = array(
            'N'=>'NEW',
            'A'=>'ASSIGNED',
            'P'=>'PROCESSING',
            'H'=>'HOLD',
            'C'=>'COMPLETED',
        );
        
        $change_order_type=array(
        '1'=> 'Date / Time Change',
        '2'=> 'Script Change',
        '3'=> 'Project Hold',
        '4'=> 'Data Change',
        '5'=> 'Complaint Investigation',
        '6'=> 'Monitor Request',
        '7'=> 'Goals Change',
        '8'=> 'Other',);
                                    
      if($get[0]>0){ 
          $params=array(
            'po_id'=>$get[0],
          );
        $co_list = $this->backend->get_change_order_list($params);
        
        $response = $this->backend->get_response();
        $co_list = $response['ResponseMessage'];
        $out_co_list = array(); 
        
        foreach ($co_list as $row) {
            
            $out_co_list[] = array(
                'id' => '<u><a  href='.WEBROOT.'/pomaintenance/co_response/'.$get[0].'/'. $row['id'].'>'.'CO'.sprintf('%06d',$row['id']).'</a></u>',
                'create_date' =>  $row['create_date'],
                'change_type' =>  $row['change_type'],
                'description' =>$row['description'],
                'assigned' =>$row['assigned'],
                'status' =>$row['status'],
            );
        }
       
        $this->view->assign('out_co_list',$out_co_list);
        
        $center_list=$this->backend->get_center_for_co($get[0]);
        $center_id = 0;
         foreach ($center_list as $key => $c) {
            $center_id = $key;
         }
        $user_list=$this->backend->get_users_per_center($center_id);   
        $this->view->assign('change_order_type', $change_order_type);
        $this->view->assign('center_list', $center_list);
        $this->view->assign('center_id', $center_id);
        $this->view->assign('user_list', $user_list);
        $this->view->assign('status', $status);
        $this->view->assign('po_id', $get[0]);
        
            $formated_po_id=$this->backend->get_po_number($get[0]);
            $this->view->assign('formated_po_id', $formated_po_id);     
        }
          //User type for access rights    
            /*$user_type = $this->backend->get_usertype($_SESSION['username']);
            $this->view->assign('user_type', $user_type); */
            $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
            
        //$params = array('id' => $get[0],); 
        
    
    }
  
    
    
   protected function download_reassign()
   {
//        $param=array(
//            'po_id' => $_GET['po_id'],
//            'batch_no' => $_GET['batch_no'],
//            'center_id' => $_GET['center_id'],
//            'process_option_id' => $_GET['process_option_id'],
//            'from' => $_GET['from'],
//            'to' => $_GET['to'],
//        );   
$calling_list = $this->backend->reassign_specific_center($_GET['center_id'],$_GET['po_id'],$_GET['batch_no'],$_GET['from'],$_GET['to']);
//header("Refresh: 0; url=".WEBROOT."/pomaintenance/downloadcall_list/{$_GET['po_id']}");
        
//        print_r($calling_list);
        die();
    } 
    public function reportdisplay()
    {
        
        $po_id = $_GET['po_id'];
        $this->view->assign('po_id', $po_id);
        
        $status_codes = $this->backend->get_status_codes($po_id);
        
        $dates = $this->backend->get_result_dates($po_id);
        
        $center = $this->backend->get_result_centers($po_id);
         
        $po = $this->backend->get_universe_start_date($po_id);
          
        $questions = $this->backend->get_script_question_response($po_id);
       
        $status_codes_not_complete = $this->backend->get_status_codes($po_id, "yes");
         
        $call_date ="default";
        if(isset($_GET['date']))
        {
            $call_date = $_GET['date'];
        }
        
        $center_id ="";
        if(isset($_GET['center_id']))
        {
            $center_id = $_GET['center_id'];
        }       
        
        $status_code ="";
        if(isset($_GET['status_code']))
        {
            $status_code = $_GET['status_code'];
        }    
        
        $this->view->assign('status_code', $status_code);
        $this->view->assign('center_id', $center_id);
        
        
        //$question_daily_result = $this->backend->get_question_results($po_id, $status_codes, $questions, $call_date,$center_id, $status_code);
        
        //$question_cumulative_result = $this->backend->get_question_results($po_id, $status_codes, $questions, "",$center_id, $status_code);
        
		if(count($questions)>0)
		{
			$question_cumulative_result = $this->backend->get_question_results($po_id, $status_codes, $questions, "",$center_id, $status_code);
			$question_daily_result = $this->backend->get_question_results($po_id, $status_codes, $questions, $call_date,$center_id, $status_code);
        }else{
			$question_daily_result = array();
			$question_cumulative_result = array();
		}
		
        
        $this->view->assign('q_d_result', $question_daily_result);

        
        $this->view->assign('q_c_result', $question_cumulative_result);
        
        $this->view->assign('status_codes_not_complete', $status_codes_not_complete);
        $this->view->assign('questions', $questions);
        $this->view->assign('universe', $po['universe']);
        $this->view->assign('start_date', $po['start_date']);
        $this->view->assign('dates', $dates);
        $this->view->assign('centers', $center);
        $this->view->assign('status_codes', $status_codes);
        
        //get first date for default loading of value
        foreach($dates as $d)
        {
            $minimum_date = $d['call_date'];
            break;
        }
        
        if(isset($_GET['date']))
        {
            $minimum_date = $_GET['date'];
        }
        
        
        
        $this->view->assign('minimum_date', $minimum_date);
        //$cumulative = $this->backend->get_summary_report_data($po_id, "", "", "");
        $cumulative = $this->backend->get_summary_report_data($po_id, "", $status_codes, $center_id, $status_code);
        $this->view->assign('cumulative', $cumulative);
        

        
        //$day_summary = $this->backend->get_summary_report_data($po_id, $call_date, "", "");
        $day_summary = $this->backend->get_summary_report_data($po_id, $call_date, $status_codes, $center_id, $status_code);
        $this->view->assign('day_summary', $day_summary);
        
        

        if(isset($_GET['is_export']))
        {
            $rowPosition=1;
            $countRecord=0;
                
            require_once "PHPExcel.php"; 
                //Header
            $excel = new PHPExcel(); 
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('PO Report');
            
            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                ->setValueExplicit("Cumulative", PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $excel->getActiveSheet()->mergeCells("A{$rowPosition}:B{$rowPosition}");
            $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                ->setValueExplicit("", PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setBold(true);                    
            $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            
            $excel->getActiveSheet()->getStyle("C{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                ->setValueExplicit($minimum_date, PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setBold(true);                    
            
            $excel->getActiveSheet()->mergeCells("D{$rowPosition}:E{$rowPosition}");
            
            $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            
            //cumulative and daily total complete
            $rowPosition++;
            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                ->setValueExplicit($cumulative['complete'], PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $percentage = ($cumulative['complete']/$po['universe'])*100;
            $percentage = number_format($percentage, 2);
            $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("B{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
                        
            $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                ->setValueExplicit('Total Complete/Contacts', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("C{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                ->setValueExplicit($day_summary['complete'], PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $percentage = ($day_summary['complete']/$po['universe'])*100;
            $percentage = number_format($percentage, 2);
            $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("E{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //end cumulative and daily total complete
            
            
            //cumulative and daily total list penetration
            $rowPosition++;
            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                ->setValueExplicit($cumulative['penetration'], PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $percentage = ($cumulative['penetration']/$po['universe'])*100;
            $percentage = number_format($percentage, 2);
            $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("B{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
                        
            $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                ->setValueExplicit('Total List Penetration', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("C{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                ->setValueExplicit($day_summary['penetration'], PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $percentage = ($day_summary['penetration']/$po['universe'])*100;
            $percentage = number_format($percentage, 2);
            $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle("E{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //end cumulative and daily total complete
            
            foreach ($status_codes_not_complete as  $c)
            {
                $status_code = $c['status_code'];
                $rowPosition++;
                $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit($cumulative[$status_code], PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $percentage = ($cumulative[$status_code]/$po['universe'])*100;
                $percentage = number_format($percentage, 2);
                $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                    ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("B{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $status_code_text = $status_code;
                if(strtoupper($status_code)=="IR")
                {
                    $status_code_text="Initial Refusal";
                }

                if(strtoupper($status_code)=="WN")
                {
                    $status_code_text="Wrong Number";
                }
                        
                            
                $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                    ->setValueExplicit($status_code_text, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("C{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                    ->setValueExplicit($day_summary[$status_code], PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $percentage = ($day_summary[$status_code]/$po['universe'])*100;
                $percentage = number_format($percentage, 2);
                $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                    ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("E{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
            }
            
            
            foreach($questions as $q)
            {
               $rowPosition++;
               $question_con = trim($q['question']);
               $question_desc = "Q{$q['question_no']}: {$question_con}";
                $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit($question_desc, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
                $excel->getActiveSheet()->mergeCells("A{$rowPosition}:E{$rowPosition}");
                $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $q_no = $q['question_no'];
                foreach($q['responses'] as $r)
                {
                    $r_no = $r['row_no'];
                    $key = "Q{$q_no}{$r_no}";
                    $rowPosition++;
                    
                    $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                        ->setValueExplicit($question_cumulative_result[$key], PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                     if($question_cumulative_result["QTotal{$q_no}"]>1){
                        $percentage = ($question_cumulative_result[$key]/$question_cumulative_result["QTotal{$q_no}"])*100;
                     }else{
                        $percentage = 0;
                     }
                    $percentage = number_format($percentage, 2);
                    $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                        ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle("B{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    
                                
                    $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                        ->setValueExplicit($r['response'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle("C{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    
                    $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                        ->setValueExplicit($question_daily_result[$key], PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    
                    if($question_daily_result["QTotal{$q_no}"]>1){
                        $percentage = ($question_daily_result[$key]/$question_daily_result["QTotal{$q_no}"])*100;
                    }else{
                        $percentage=0;
                    }
                    
                    
                    $percentage = number_format($percentage, 2);
                    $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                        ->setValueExplicit($percentage.'%', PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle("E{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            
                    
                }
                
                $rowPosition++;
                
                $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                ->setValueExplicit($question_cumulative_result["QTotal{$q_no}"], PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("A{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                ->setValueExplicit($question_daily_result["QTotal{$q_no}"], PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle("D{$rowPosition}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                
                
            }
            
            $po_info = $this->backend->get_po_code_name($po_id);    
            $date=date('Y-m-d H:i:s');
            $filename=$po_info['po_no']."_{$date}.xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
                             
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            if(isset($_GET['is_email'])){
                 $this->layout = 'json';
                 $email = $_GET['emails'];   
                 $file = 'POReport_'.time().'.xls';    
                 $full_file_path = $this->upload_path.DIRECTORY_SEPARATOR.'POReport_'.time().'.xls';
                 $objWriter->save($full_file_path);
                 
                   $body="<br><br>Greetings!<br><br>
                                Please see attached file for the report. <br> <br>Thanks,<br> PBS Admin";
                 
                 if($this->backend->send_mail($body,$email,$this->upload_path.DIRECTORY_SEPARATOR,$file,'PBS Notification')){
                        $this->response = array(
                            'data'=>'',
                            'success' => true,
                            'message' => "Email has been sent.",
                        ); 
                 }else{
                    $this->response = array(
                        'data'=>'',
                        'success' => false,
                        'message' => "Unable to send email.",
                    );
                 }
       
                
            }else{
                $objWriter->save('php://output');    
                die();  
            }
              
            
                       
        }
    }
    
    public function report()
    {
	
        $po_id = $_GET['po_id'];
        $this->view->assign('po_id', $po_id);
		
        $po_info = $this->backend->get_po_code_name($po_id);
		
        $this->view->assign('po_no', $po_info['po_no']);
        $this->view->assign('po_name', $po_info['name']);
        
        
        $status_codes = $this->backend->get_status_codes($po_id);


		
     
        $dates = $this->backend->get_result_dates($po_id);

		
        $center = $this->backend->get_result_centers($po_id);
         	
        $po = $this->backend->get_universe_start_date($po_id);
		
          
        $questions = $this->backend->get_script_question_response($po_id);
			
       
        $status_codes_not_complete = $this->backend->get_status_codes($po_id, "yes");

        $call_date ="default";
		
		//get first date for default loading of value
        foreach($dates as $d)
        {
            $minimum_date = $d['call_date'];
            break;
        }
		
		
        if(isset($_GET['date']))
        {
            $call_date = $_GET['date'];
        }else{
			$call_date = $minimum_date;
			$date = new DateTime($call_date);
            $call_date = $date->format('Y-m-d');
		}
		
        $center_id ="";
        if(isset($_GET['center_id']))
        {
            $center_id = $_GET['center_id'];
        }       
        
        $status_code ="";
        if(isset($_GET['status_code']))
        {
            $status_code = $_GET['status_code'];
        }    
        
        $this->view->assign('status_code', $status_code);
        $this->view->assign('center_id', $center_id);
      
		if(count($questions)>0)
		{
			$question_cumulative_result = $this->backend->get_question_results($po_id, $status_codes, $questions, "",$center_id, $status_code);
			$question_daily_result = $this->backend->get_question_results($po_id, $status_codes, $questions, $call_date,$center_id, $status_code);
        }else{
			$question_daily_result = array();
			$question_cumulative_result = array();
		}

	   
         
        $this->view->assign('q_d_result', $question_daily_result);

        
        $this->view->assign('q_c_result', $question_cumulative_result);
        
        $this->view->assign('status_codes_not_complete', $status_codes_not_complete);
        $this->view->assign('questions', $questions);
        $this->view->assign('universe', $po['universe']);
        $this->view->assign('start_date', $po['start_date']);
        $this->view->assign('dates', $dates);
        $this->view->assign('centers', $center);
        $this->view->assign('status_codes', $status_codes);
        

        
        if(isset($_GET['date']))
        {
            $minimum_date = $_GET['date'];
        }
        
         
        
        $this->view->assign('minimum_date', $minimum_date);
        //$cumulative = $this->backend->get_summary_report_data($po_id, "", "", "");
        $cumulative = $this->backend->get_summary_report_data($po_id, "", $status_codes, $center_id, $status_code);
        $this->view->assign('cumulative', $cumulative);
        
      //  $day_summary = $this->backend->get_summary_report_data($po_id, $call_date, "", "");
        $day_summary = $this->backend->get_summary_report_data($po_id, $call_date, $status_codes, $center_id, $status_code);
        $this->view->assign('day_summary', $day_summary);
          
        
        
//        print_r($this->backend->get_summary_report_data($po_id, "", "", "" ));
//        die();
//        print_r($po_id);
//        die();
    }
    
    
    public function get_answer_call_date()
    {
        $po_id = $_GET['po_id'];
        $center_id=$_GET['center_id'];
        $center_list=$this->backend->get_answer_call_date($po_id,$center_id);
        $this->response = array(
                'data'=>$center_list,
                'success' => true,
            ); 
        
    } 
    protected function reapply_specific_center()
    {
        $center_list =explode( '|', $_GET['txtCenterList'],-1 ) ;
    
        $center_calling_rec = array();
        $cnt=1;
        foreach($center_list as $row)
        {
           $data =explode( ',', $row ) ;
           $center_calling_rec[]= array(
                'list_cnt'         =>  $cnt,
                'center_id'        =>  $data[0], 
                'record_cnt_from'  =>  $data[1], 
                'record_cnt_to'    =>  $data[1],
                'po_id'            =>  $_GET['txtPoId'],
                'batch_no'         =>  $_GET['txtBatchNo'],
           );
           $cnt++;
        }
        

        $result = $this->backend->reassign_apply_data_center_specific($center_calling_rec, $_GET['txtBatchNo'], $_GET['txtPoId'], $_GET['txtIdCallingList']);
        $response = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => "Records had been reassigned.",
                'redirect'=> '../downloadcall_list/'. $_GET['txtPoId'],
            ); 
        }  
    } 
    
    
    
    protected function reapply_center()
    {
        $center_list =explode( '|', $_GET['txtCenterList'],-1 ) ;
    
        $center_calling_rec = array();
        $cnt=1;
        foreach($center_list as $row)
        {
           $data =explode( ',', $row ) ;
           $center_calling_rec[]= array(
                'list_cnt'         =>  $cnt,
                'center_id'        =>  $data[0], 
                'record_cnt_from'  =>  $data[1], 
                'record_cnt_to'    =>  $data[1],
                'po_id'            =>  $_GET['txtPoId'],
                'batch_no'         =>  $_GET['txtBatchNo'],
           );
           $cnt++;
        }
        
        $result = $this->backend->reassign_apply_data_center($center_calling_rec, $_GET['txtBatchNo'], $_GET['txtPoId']);
        $response = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => "Records had been reassigned.",
                'redirect'=> '../downloadcall_list/'. $_GET['txtPoId'],
            ); 
        }  
    } 
    
    public function apply_center()
    {
        
        $center_list =explode( '|', $_GET['txtCenterList'],-1 ) ;
    
        $center_calling_rec = array();
        $cnt=1;
        foreach($center_list as $row)
        {
           $data =explode( ',', $row ) ;
           $center_calling_rec[]= array(
                'list_cnt'         =>  $cnt,
                'center_id'        =>  $data[0], 
                'record_cnt_from'  =>  $data[1], 
                'record_cnt_to'    =>  $data[2],
                'po_id'            =>  $_GET['txtID'],
                'batch_no'         =>  $_GET['batch_no'],
           );
           $cnt++;
        }
          
        //$result = $this->backend->apply_data_center($center_calling_rec);
        $result = $this->backend->apply_data_center_2($center_calling_rec, $_GET['batch_no'], $_GET['txtID']);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
             $this->response['message'] = 'System error, unable to connect to database';
             $this->phpAlertWithRedirect("Unable to apply center","../pomaintenance/upload_data/{$_GET['txtID']}");
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->phpAlertWithRedirect("Unable to apply center","../pomaintenance/upload_data/{$_GET['txtID']}");
        } else {
            $this->phpAlertWithRedirect("Center had been assigned","../pomaintenance/downloadcall_list/{$_GET['txtID']}");
        }  
        
    }
    
    
    
    protected function drop_result()
    {
        $id= $_POST['id'];
        if(isset($_POST['center_name'])){
            $params=array(
                'id'        =>$_POST['id'],
                'center_id' =>$_POST['center_name'],
                'call_date'=>$_POST['txtDate'],
            );
        }else{
             $params=array(
                'id'        =>$_POST['id'],
                'center_id' =>0,
                'call_date'=>0,
            );
        }
        $result = $this->backend->drop_result($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => $response['ResponseMessage'],
                'redirect'=> '../datamanagement/'. $_POST['id'],
            ); 
        }
       
          
    } 
   public function file_type()
   {
       ini_set ('max_execution_time', 10000); 
        $max_upload_size = 52428800; //50MB
        //$max_upload_size = 26214400; //25MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!(in_array($upload['type'], array('text/plain')))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'ERROR';
                }
             
                if ($this->response['message'] != '') { 
                   print_r($this->response['message']);
                } 
                  
                die();
            }
        }
       
   } 
    public function import_manual_file()
    {
        ini_set ('max_execution_time', 10000); 
        $max_upload_size = 52428800; //50MB
        //$max_upload_size = 26214400; //25MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'FILESIZE';
                } elseif (!(in_array($upload['type'], array('text/plain')))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'ERROR';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, array('txt')))) {
                        $this->response['message'] = 'Invalid file type';
                    } else {
                        $file_name = time().".{$extension}";
                        $file = $this->upload_path. DIRECTORY_SEPARATOR . $file_name;
                        if (!is_dir($this->upload_path)) @mkdir ($this->upload_path);
                        if (!is_dir($this->upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } 
                        elseif (!move_uploaded_file($upload['tmp_name'], $file)) {
                            $this->response['message'] = 'Error in uploading document';}  
                        else {
                                  $this->response['message'] = $file_name;
                           
                        }
                    }
                }
             
                if ($this->response['message'] != '') { 
                   print_r($this->response['message']);
                } 
                  
                die();
            }
        }
    }
    
    
    public function process_manual_file()
    {
        $file=$_GET['file'];
        $po_id=$_GET['po_id'];
      
        $file = $this->upload_path. DIRECTORY_SEPARATOR .$file; 
        
        $result = $this->backend->process_manual_file($file, $po_id);
         
         $this->response = array(
                'record'=> isset($result['no_of_records'])?$result['no_of_records']:0,
                'message' => $result['message'],
                'success' => $result['success'],
         );
    }
    
    public function manual_load($get="") {
        $actions = array('import_manual_file', 'process_manual_file');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
        
        $params = array(
                    'id' =>$get[0],
        );
        $checking = array();
        $isTrue = 0;
        if($get[0]>0){
            $checking=$this->backend->check_data_if_exist($params);
            if(count($checking)>0){
               $isTrue = 1; 
            } else{
                $this->phpAlertWithRedirect("Please upload first the raw data.","../datamanagement/{$get[0]}");
            }
            $po_status=$this->backend->check_po_status($params);
             if($po_status['status']=='HD'){    //IF the  status is Hold for Data.
               $this->phpAlertWithRedirect("You're not allow to upload result.","../datamanagement/{$get[0]}"); 
            }  
            
        }else{
             $isTrue = 1;
        }
         if($get[0]>0){
            $formated_po_id= $get[0];
//            $process_list_option = $this->backend->get_process_list_option();
//            $response = $this->backend->get_response();
//            $process_list_option = $response['ResponseMessage'];
//            $this->view->assign('process_list_option', $process_list_option);
            $this->view->assign('po_id', $get[0]);
             $formated_po_id=$this->backend->get_po_number($get[0]);
             $this->view->assign('formated_po_id', $formated_po_id); 
//            $center_list = $this->backend->get_center_list();
//            $response = $this->backend->get_response();
//            $center_list = $response['ResponseMessage'];
//            $this->view->assign('center_list', $center_list);
        }
        /*else{
            $this->phpAlertWithRedirect("Please upload first the raw data.","../datamanagement/{$get[0]}");
        }    */
        
       
    
    }
   protected function get_po_listby_id()
  {      
       $client_id=$_GET['id'];
        $result = $this->backend->get_po_listby_id($client_id);
        $response = $this->backend->get_response();
        $out_po_temp = array();                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
            foreach ($response['ResponseMessage'] as $row) {
            $out_po_temp[] = array(
                'id'=>$row['id'],
                'name'=>$row['name'],
            );
        }
            $this->response = array(
                'data'=>$out_po_temp,
                'success' => true,
            ); 
        }
       
      
  }  
  protected function get_po_listby_all_id($client_id)
  {
      
        $result = $this->backend->get_po_listby_all_id($client_id);
        $response = $this->backend->get_response();
        $out_po_temp = array();                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
           
            foreach ($response['ResponseMessage'] as $row) {
            $out_po_temp[] = array(
                'id'=>$row['id'],
                'name'=>$row['name'],
            );
        }
     
            
            
            
           return $out_po_temp;
        }
       
      
  }
  protected function get_po_template_listby_all_id($client_id)
  {
        $result = $this->backend->get_po_template_listby_all_id($client_id);
        $response = $this->backend->get_response();
        $out_po_temp = array();                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
             
            foreach ($response['ResponseMessage'] as $row) {
            $out_po_temp[] = array(
                'id'=>$row['id'],
                'name'=>$row['name'],
            );
        }
          
           return $out_po_temp;
        }
       
      
  }
  protected function get_po_template_listby_id()
  {
        $client_id=$_GET['id'];
        $result = $this->backend->get_po_template_listby_id($client_id);
        $response = $this->backend->get_response();
        $out_po_temp = array();                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
             
            foreach ($response['ResponseMessage'] as $row) {
            $out_po_temp[] = array(
                'id'=>$row['id'],
                'name'=>$row['name'],
            );
        }
           $this->response = array(
                'data'=>$out_po_temp,
                'success' => true,
            ); 
           
        }
       
      
  }
    
    
  public function drop_data($get=""){
        $actions = array('drop_client_data', 'drop_client_data_per_batch');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
            return;
        }
       
        $params = array(
            'id' =>$get[0],
        );
       
        $this->view->assign('po_id', $get[0]);
        $formated_po_id=$this->backend->get_po_number($get[0]);
        
        $batch_list=$this->backend->get_upload_batch_list($get[0]);
		$this->view->assign('batch_list', $batch_list); 
        $this->view->assign('formated_po_id', $formated_po_id); 
     }
    protected function drop_client_data()
    {
        $id= $_POST['txtID'];
        $result = $this->backend->drop_client_data($id);
        
        $response = $this->backend->get_response();                                              
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => $response['ResponseMessage'],
                'redirect'=> '../datamanagement/'. $_POST['txtID'],
            ); 
        }  
    } 
    
    protected function drop_client_data_per_batch()
    {
        $po_id= $_GET['po_id'];
        $batch_no= $_GET['batch_no'];
        $result = $this->backend->drop_client_data($po_id, $batch_no);
        
        $response = $this->backend->get_response();                                              
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => $response['ResponseMessage'],
                'redirect'=> '../datamanagement/'. $po_id,
            ); 
        }  
    } 
    protected function drop_manual_load()
    {
        $id= $_POST['id'];
        $result = $this->backend->drop_manual_load($id);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => $response['ResponseMessage'],
                'redirect'=> '../datamanagement/'. $_POST['id'],
            ); 
        }
       
          
    } 
    
    
    protected function apply_data_center()
    {
        
        $center_list =explode( '|', $_POST['txtCenterList'],-1 ) ;
    
        $center_calling_rec = array();
        $cnt=1;
        foreach($center_list as $row)
        {
           $data =explode( ',', $row ) ;
           $center_calling_rec[]= array(
                'list_cnt'         =>  $cnt,
                'center_id'        =>  $data[0], 
                'record_cnt_from'  =>  $data[1], 
                'record_cnt_to'    =>  $data[2],
                'po_id'            =>  $_POST['txtID'],
                'batch_no'         =>  $_POST['batch_no'],
           );
           $cnt++;
        }
          
        $result = $this->backend->apply_data_center($center_calling_rec);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'data'=>'',
                'success' => true,
                'message' => "Call Canter has been assigned.",
                'redirect'=> '../downloadcall_list/'. $_POST['txtID'],
            ); 
           
        }  
        
    }
     protected function addcenter_tolist(){
       $field_info= array(
            $_POST['center_id'],
            $_POST['center_name_selectedText'],
            //$_POST['txtRowCount'],
            $_POST['txtRowCount_2'],
            ''
       );
      
       
        $this->response = array(
                    'data'=>$field_info,
                    'success' => true,
                ); 
     }
      protected function reload_param_2()
   {
      
         $data = serialize(explode( '|', $_GET['fields'],-1 ) );
            $search_result = array();
            $datafields=unserialize($data);
            $fields = array();
            $rowIndex= 0;
           if($datafields!=""){
                foreach($datafields  as $key => $val) {
                                         $fields[]=array_merge(
                                            explode( ',',$val),
                                             array('4'=> '',)
                                            /*array('4'=> '<input type="image" id="deleteRowDT" name ="deleteRowDT" onclick="delete_response('.$rowIndex.'); return false;" src="'.WEBROOT.'/public/images/deleted.png" title="edit" />',)*/
                                        );
                  $rowIndex++; 
                }
           }
            
           
            $this->response = array(
                'success'   => true,
                'message'      => $fields,
            );
                  
   }
       public function downloadcall_list($get=""){
        $actions = array('import_file','read_file','process_data','process_finished_data', 'reapply_center','reapply_specific_center','download_reassign');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
            return;
        }
       
        if(!is_numeric($get[0])){
            $this->phpAlertWithRedirect("Invalid parameter.",WEBROOT.'/pomaintenance/project_list');
        }
        
        $params = array('id' => $get[0],);
        $calling_list = $this->backend->get_calling_list($params);
    
        $response = $this->backend->get_response();
        
        $calling_list = $response['ResponseMessage'];
        
        
        $out_calling_list = array(); 
        foreach ($calling_list as $row) {
           
            $out_calling_list[] = array(
                'id' => $row['list_cnt'],
                'batch_no' =>$row['batch_no'],
                'po_id' => sprintf('%06d',$row['po_id']),
                'poid' => $row['po_id'],
                'center_name' =>$row['center_name'],
                'center_code' =>$row['center_code'], 
                //'record_cnt' =>  $row['record_cnt_from'] ." - ". $row['record_cnt_to'], //$row['record_cnt'],
                //'record_cnt' =>   $row['record_cnt_to'], //$row['record_cnt'],
                
                //'record_cnt' =>  $row['record_cnt_to'] - $row['record_cnt_from'] + 1     , //$row['record_cnt'],
                'record_cnt' =>  $row['assign_record_cnt'],
                //'record_cnt' =>  $rec_count,
                'record_cnt_from' =>$row['record_cnt_from'],
                'record_cnt_to' =>$row['record_cnt_to'],
                'offset' => $row['record_cnt_from'],
                'limit'=>($row['record_cnt_to'] -($row['record_cnt_from']-1)),
                'Date_uploaded' => $row['create_date'], //$row['Date_uploaded'],
                'button' =>'',
                'center_id' => $row['center_id'],
                'process_option_id' => $row['process_option_id'],
                'householded_count' => $row['householded_count'],
                'badphone_count' => $row['badphone_count'],
                'total_count' => $row['total_count'],
                'final_count' => $row['final_count'],
                'dedup_count' => $row['dedup_count'],
                'calling_list_id' => $row['id'],
                'is_reassign'     =>$row['is_reassign'],
            );
        }
        
        $center_list = $this->backend->get_center_list();
        $response = $this->backend->get_response();
        $center_list = $response['ResponseMessage'];
       $this->view->assign('center_list', $center_list);
           $formated_po_id=$this->backend->get_po_number($get[0]);
            $this->view->assign('formated_po_id', $formated_po_id); 
           $this->view->assign('out_calling_list', $out_calling_list);
           $this->view->assign('po_id', $get[0]);
        
        
     }
  
  
     public function datamanagement($get="") {
        $actions = array('import_file','read_file','process_data','process_finished_data','drop_manual_load','drop_result','get_answer_call_date');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
       
       
        $this->view->assign('po_id', $get[0]);
        if($get[0]>0){
            $is_had_result = $this->backend->check_data_if_result_exist($get[0]);
            $this->view->assign('is_had_result', $is_had_result);
            $center_list=$this->backend->get_center_for_upload_result($get[0]);
            $this->view->assign('center_list', array('<----SELECT---->')+$center_list);
            $this->view->assign('call_date', array('<----SELECT---->'));
            $formated_po_id=$this->backend->get_po_number($get[0]);
            $this->view->assign('formated_po_id', $formated_po_id);     
        }
          //User type for access rights    
            /*$user_type = $this->backend->get_usertype($_SESSION['username']);
            $this->view->assign('user_type', $user_type); */
            
            $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
        //$params = array('id' => $get[0],); 
        
    
    }
     
  
   protected function get_po_transaction_data()
    {
            
         /*$message = '';                         
            echo '<script type="text/javascript">   document.getElementById("ajax_loader").dialog("close");' . $message . '</script>'; 
            exit;   */
//            $param=array(
//                'id' => $_GET['id'],
//                'limit' => $_GET['limit'],
//                'offset' => $_GET['offset'],
//                'batch_no' => $_GET['batch_no'],
//            );   

        $param=array(
            'id'   => $_GET['id'],
            'po_id' => $_GET['po_id'],
            'batch_no' => $_GET['batch_no'],
            'center_id' => $_GET['center_id'],
            'process_option_id' => $_GET['process_option_id'],
            'center_code' => $_GET['center_code'],
        );   
                                 
        //$result = $this->backend->export_calling_list($param);
        $result = $this->backend->export_calling_list_2($param);
        $response = $this->backend->get_response();
        
                
 /*       $url = WEBROOT."/".$response['ResponseMessage'];
        
        
        
        header('Location: '.$url);       */
        
        //
//        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
//            $this->response['message'] = 'System error, unable to connect to database';
//        } elseif (!($response['ResponseCode'] == '0000')) {
//            $this->response['message'] = $response['ResponseMessage'];
//        } else {
//            
//        }
    } 
  
  
  
  protected function get_po_transaction_data_back()
    {
            
         /*$message = '';                         
            echo '<script type="text/javascript">   document.getElementById("ajax_loader").dialog("close");' . $message . '</script>'; 
            exit;   */
             
            $param=array(
                'id' => $_GET['id'],
                'limit' => $_GET['limit'],
                'offset' => $_GET['offset'],
                'batch_no' => $_GET['batch_no'],
            );   
                                                      
        $result = $this->backend->get_po_transaction_data($param);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            
            $rowPosition=1;
            require_once "PHPExcel.php"; 
            $excel = new PHPExcel(); 
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('Calling_List_'.$_GET['center_code']."_".date('Y_m_d'));
            
            
            $result_databack = $this->backend->get_customer_databack_fields($param);
            $response_databack = $this->backend->get_response();
            $result_databack = $response_databack['ResponseMessage'];
            $arr_databack = array();
            $arr_databack=unserialize($result_databack );
            
            $num_of_q = $this->backend->get_po_number_of_question($param);  
            $response_q = $this->backend->get_response();
            $num_of_q = $response_q['ResponseMessage'];
            $is_col=true;
            $colPosition_crid='A';
            $colPosition_phone='B';
            $fname_field_position = 'C';
            $lname_field_position = 'D';
            $crID_col_position=1;
            $phone_position_cnt=1;
            $fname_field_position_cnt = 1;
            $lname_field_position_cnt = 1;
            $misc_fields_postiob_cnt = 1;
            foreach ($response['ResponseMessage'] as $key => $val) {
                //Data Row
                 $row_data = array();
                 $row_data =explode("|",$val['row_data']);
                 $colPosition='A'; 
                 $phone_field=$val['phone_field'];
                 $fname_field =$val['fname_field'];
                 $lname_field=$val['lname_field'];
                 if($is_col)
                 {
                        $is_col=false;
                        $misc_fields_db = array();
                        $misc_fields =  array();
                        $temp="";
                        $misc="";
                        $questions = "";
                        $remarks="";
                        //Get the Misc Fields
                        $misc_fields_db =unserialize($val['misc_fields']);
                        $remarks_db =unserialize($val['remarks']);
                        for ($x = 0; $x <count($misc_fields_db); $x++) {
                            $temp .='misc'.($x+1)."|";
                            $misc .=$misc_fields_db[$x]."|";
                        }
                        for ($x = 0; $x <count($remarks_db); $x++) {
                            $remarks .='remarks'.($x+1)."|";
                           // $misc .=$misc_fields_db[$x]."|";
                        }
                        for($x=0;$x<$num_of_q;$x++)  {
                            $questions .='Q'.($x+1)."|";    
                        }
                        //Get the Phone value column position
                         $cnt=1;
                         $misc =explode("|",$misc);
                                                 
                         foreach ($row_data as $row){
                            if(trim($val['phone_no'])==trim($row)){
                                $phone_position_cnt = $cnt;       
                            }
                            if(trim($fname_field)==trim($row)){
                               $fname_field_position_cnt=$cnt;     
                            }
                             if(trim($lname_field)==trim($row)){
                                $lname_field_position_cnt=$cnt;     
                            }
                            
                            $cnt++;
                         }                           
                        //Bulid the column header
                        $temp = substr($temp,0,-1);
                        $temp =explode("|",$temp);
                        $questions = explode("|",$questions);
                        $remarks = explode("|",$remarks);
                        $phone_no_col=array_merge($row_data,$temp);
                        $merged_array_final= array_merge(array('crID',$phone_field,$fname_field,$lname_field),$temp, $arr_databack,$questions);
                      /*  for ($x = 1; $x <count($phone_no_col); $x++) {
                            $colPosition_phone++;
                        } */
                     //$colPosition_phone++;
                        foreach ($merged_array_final as $row) {
                              $excel->getActiveSheet()->getCell($colPosition.$rowPosition)
                                    ->setValueExplicit($row, PHPExcel_Cell_DataType::TYPE_STRING);
                              $excel->getActiveSheet();
                              $colPosition++;
                        } 
                        
                 }else{ 
                     
                         $cnt=1;
                         foreach ($row_data as $row){
                             //Write the data rows
                             /* $excel->getActiveSheet()->getCell($colPosition.$rowPosition)
                                    ->setValueExplicit($row, PHPExcel_Cell_DataType::TYPE_STRING);
                              $excel->getActiveSheet();  */
                              //Write the phone_clean field
                              
                               $excel->getActiveSheet()->getCell('A'.$rowPosition)
                                    ->setValueExplicit($val['id'], PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet();
                               if($cnt==$phone_position_cnt){ 
                                    $excel->getActiveSheet()->getCell($colPosition_phone.$rowPosition)
                                    ->setValueExplicit( $row, PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet();  
                               }  
                                 if($cnt==$fname_field_position_cnt){ 
                                    $excel->getActiveSheet()->getCell($fname_field_position.$rowPosition)
                                    ->setValueExplicit( $row, PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet();  
                               }  
                                if($cnt==$lname_field_position_cnt){ 
                                    $excel->getActiveSheet()->getCell($lname_field_position.$rowPosition)
                                    ->setValueExplicit( $row, PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet();  
                               }             
                               
                               
                            $colPosition++;
                            $cnt++; 
                        }
                         
                 }
                 
                $rowPosition++;
            }
           
            $date=date('Y-m-d');
            $filename="Calling_List_".$_GET['center_code']."_{$date}_".time().".xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
          //$objWriter->save('nameoffile.xls');
       //   die();
        
    
        }  
          
    } 
   
     protected function get_databack()
    {
        

//        $param = array(
//            'po_id'=> $_GET['id'],
//          );                                            
        //$result = $this->backend->get_databack_2($param);
        $result = $this->backend->get_databack_2($_GET);
        $response = $this->backend->get_response();
        $url = WEBROOT."/".$response['ResponseMessage'];
        header('Location: '.$url);  
        
    } 
 
  

     protected function get_databack_backup()
    {
               
          $param = array(
            'id'=> $_GET['id'],
          );                                            
        $result = $this->backend->get_databack($param);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            
            $rowPosition=1;
            require_once "PHPExcel.php"; 
            $excel = new PHPExcel(); 
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle("Calling_List_".date('Y_m_d'));
            
            $result_databack = $this->backend->get_customer_databack_fields($param);
            $response_databack = $this->backend->get_response();
            $result_databack = $response_databack['ResponseMessage'];
            $arr_databack = array();
            $arr_databack=unserialize($result_databack );
          
            $is_col=true;
            $colPosition_phone='A';
            $crID_col_position='0';
            $phone_position_cnt=1;
             
            foreach ($response['ResponseMessage'] as $key => $val) {
               
                //Data Row
                 $row_data = array();
                 $row_data =explode("~",$val['row_data']);
                 $colPosition='A';
                 if($is_col)
                 {
                        $is_col=false;
                        $misc_fields_db = array();
                        $misc_fields =  array();
                        $temp="";
                        //Get the Misc Fields
                        /*$misc_fields_db =unserialize($val['misc_fields']);
                        for ($x = 0; $x <count($misc_fields_db); $x++) {
                            $temp .='misc'.($x+1)."|";
                        }*/
                        //Get the Phone value column position
                         $cnt=1; 
                         foreach ($row_data as $row) {
                            if(trim($val['phone_no'])==trim($row)){
                                $phone_position_cnt = $cnt;       
                            }
                            $cnt++;
                         }
                        //Bulid the column header
                        $temp = substr($temp,0,-1);
                        $temp =explode("|",$temp);
                        $phone_no_col=array_merge($row_data,  array('centerID','master_rec'),$temp,array('phone_clean'));
                        //$merged_array_final= array_merge($row_data,  array('centerID','master_rec'),$temp,array('phone_clean'),$arr_databack,array('status_code','downloaded','crID'));
                        $merged_array_final= array_merge($row_data,  array('centerID','master_rec'),$temp,array('phone_clean'),array('downloaded','crID'));
                        for ($x = 1; $x <count($phone_no_col); $x++) {
                            $colPosition_phone++;
                        } 
                     
                        foreach ($merged_array_final as $row) {
                              $excel->getActiveSheet()->getCell($colPosition.$rowPosition)
                                    ->setValueExplicit($row, PHPExcel_Cell_DataType::TYPE_STRING);
                              $excel->getActiveSheet();
                              $colPosition++;
                              if($crID_col_position=='0'){
                                  $crID_col_position='A';
                              }else{
                                 $crID_col_position++; 
                              }
                              
                        } 
                        
                 }else{ 
                     
                         $cnt=1;
                         foreach ($row_data as $row) {
                             //Write the data rows
                              $excel->getActiveSheet()->getCell($colPosition.$rowPosition)
                                    ->setValueExplicit($row, PHPExcel_Cell_DataType::TYPE_STRING);
                              $excel->getActiveSheet();
                              //Write the phone_clean field
                               if($cnt==$phone_position_cnt){ 
                                     $replace = array(
                                        '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                                        '&quot;' => '',
                                        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                                        '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                                        'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', 'Ð' => 'D',
                                        'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                                        'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                                        'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', 'Í' => 'I',
                                        'Î' => 'I', 'Ï' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                                        'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                                        'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                                        'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                                        'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                                        'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                                        'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                                        '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                                        '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                                        'W' => 'W', 'Ý' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                                        'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                                        'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                                        'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                                        'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                                        'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                                        'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                                        'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                                        'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                                        'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                                        '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                                        '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                                        '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                                        'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                                        'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                                        'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                                        'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                                        '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                                        '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                                        '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                                        '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                                        '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                                        '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                                        '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                                        '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                                        '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                                        '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                                        '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                                        '?' => 'yu', '?' => 'ya'
                                        );
                                    $excel->getActiveSheet()->getCell($colPosition_phone.$rowPosition)
                                    ->setValueExplicit(str_replace(array_keys($replace), $replace, $row), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet();  
                               }           
                                
                            $colPosition++;
                            $cnt++; 
                        }
                        $excel->getActiveSheet()->getCell($crID_col_position.$rowPosition)
                                    ->setValueExplicit($val['id'], PHPExcel_Cell_DataType::TYPE_STRING);
                                    $excel->getActiveSheet(); 
                        
                         
                 }
                
                $rowPosition++;
            }
        
            $date=date('Y-m-d');
            $filename="DataBack_{$date}_".time().".xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
                         
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
          die();
            
        }  
          
    } 
  
     
  
  
  
      public function download_data($get="") {
        $actions = array('get_databack','get_po_transaction_data','export_calling_list','download_reassign');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
          
        $params=array(
            'id'=>  $get[0],
        );
      
      
       $checking = array();
        $isTrue = 0;
        if($get[0]>0){
              $formated_po_id=$this->backend->get_po_number($get[0]);
              $this->view->assign('formated_po_id', $formated_po_id); 
            $checking=$this->backend->check_data_if_exist($params);
            if(count($checking)>0){
               $isTrue = 1; 
            }
        }else{
             $isTrue = 1;
        }
        
        if($isTrue==1){
      
            /*$result = $this->backend->get_column_data($params);
            $response = $this->backend->get_response();
            $result = $response['ResponseMessage'];
            
            $result_databack = $this->backend->get_customer_databack_fields($params);
            $response = $this->backend->get_response();
            $result_databack = $response['ResponseMessage'];
            
            $result_misc = $this->backend->get_po_transaction_data_col($params);
            $response_misc = $this->backend->get_response();
            $result_misc = $response_misc['ResponseMessage'];
            
            $arr_databack = array();
            $arr_databack=unserialize($result_databack );
         
            $arr_data_col = array();
            $arr_data_col =  explode( '|', $result );
            $column_data = array();
                 for ($c=0; $c <count($arr_data_col); $c++) {                   
                    $column_data= array_merge_recursive($column_data,array($arr_data_col[$c]=> $arr_data_col[$c],));
                 }   
                    $misc_fields_db = array();
                    $misc_fields =  array();
                    $temp="";    
              foreach ($response_misc['ResponseMessage'] as $key => $val) {
                     $misc_fields_db =unserialize($val['misc_fields']);
                            for ($x = 0; $x <count($misc_fields_db); $x++) {
                                $temp .='misc'.($x+1)."|";
                            }
                            $temp = substr($temp,0,-1);
                            $temp =explode("|",$temp);                        
                            $merged_array_final= array_merge($column_data,  array('centerID','master_rec'),$temp,array('phone_clean'),$arr_databack,array('status_code','downloaded','crID'));
              }*/
           
         //   $this->view->assign('column_data', $merged_array_final   );  
            $this->view->assign('po_id', $get[0]);
            $coded_types_ids = array('Complete', 'Callable','Contact', 'Terminal','Sale', 'Bad Number','Leave Message(Complete)'); 
            $coded_types = array('Complete' => ' Complete', 
                                 'Callable' => ' Callable',
                                 'Contact' => ' Contact', 
                                 'Terminal' => ' Terminal',
                                 'Sale' => ' Sale', 
                                 'Bad Number' => ' Bad Number',
                                 'Leave Message(Complete)',
                                 );
            $this->view->assign('coded_types', $coded_types);
            $this->view->assign('coded_types_ids', $coded_types_ids);
        }else{
              $this->phpAlertWithRedirect("Please upload first the raw data.","../datamanagement/{$get[0]}");
        }
        
      
     }
  
   public function process_finished_data()
   {
       $file = $this->upload_path. DIRECTORY_SEPARATOR . $_POST['filename'];
        $params=array(
                      'file'=>$file,
                      'id'=>$_POST['id'],
                      'batch_no' =>-1, 
                      'center_id' =>-1, 
                      'record_no' =>0,
                   );
             
        $result = $this->backend->upload_finished_call_list($params);
        $response = $this->backend->get_response();
       if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            if(isset($response['ResponseMessage']['message'])){
                $this->response['message'] = $response['ResponseMessage']['message'];        
            }else{
                $this->response['message'] = $response['ResponseMessage'];        
            }
        } else {
            
            $upload_param=array(
                    'po_id'=> $params['id'],
                    'type' => 'householded_records',
          );  
        
          
          $this->response = array(
                'success'   => true,
                'message'      => 'CSV file has been uploaded.',  
            );
            
        }
 
   }
  
  
   public function process_data()
   {
       ini_set('max_execution_time', 0);
      /* $ext = pathinfo($_POST['filename'], PATHINFO_EXTENSION);
           if($ext == "zip"){
              $file = str_replace(".zip",".csv",$_POST['filename']);
           }else{
               $file=$_POST['filename'];
           }*/
       $file=$_POST['filename'];
       $file = $this->upload_path. DIRECTORY_SEPARATOR . $file;
        /*$center_list =explode( '|', $_POST['txtCenterList'],-1 ) ;
     
        $center_calling_rec = array();
        foreach($center_list as $row)
        {
           $data =explode( ',', $row ) ;
           $center_calling_rec[]= array(
                'center_id'        =>  $data[0], 
           );
           
        }     */
        
        
        $params=array(
                      'file'=>$file,
                      'id'=>$_POST['id'],
                      //  'center_details'=>$center_list,
                      'process_option_id'=> $_POST['process_option_id'],
                      'phone_field'=>$_POST['phone_number'],
                      'batch_no'=>$_POST['batch_no'],
                      'fname_field'=>$_POST['first_name'],
                      'lname_field'=>$_POST['last_name'],      
                      'misc_fields'=>serialize(isset($_POST['misc_fields'])? $_POST['misc_fields']: ""), 
                      'remarks'=> serialize(isset($_POST['remarks'])? $_POST['remarks']: ""), 
                     // 'center_info' =>$center_calling_rec,
                     'fnames_col' =>isset($_POST['txtFnames_col'])? $_POST['txtFnames_col']: "" //PP 02/08/2017
                   );
                   
         //PP 02/08/2017
           if($params['fnames_col']=="" && $params['process_option_id']==4)
           {
                   $this->response = array(
                    'data'      =>"",
                    'success'   => false,  
                    'is_drop' => true, 
                    'message'      => "Unable to process. Please check the column header format for first name.",  
                ); 
                return;
           } 
                        
                   
       // $res_call_list = $this->backend->upload_call_list($params);
        $res_call_list = $this->backend->upload_call_list_2_temp($params);
        
        $res = $res_call_list['err_num'];      
        $data_format = $res_call_list['data'];    
        if($res=="0000")
        {         
              
             if($params['process_option_id']==1){ //Non-Household List AS-IS
                   $upload_param=array(
                        'batch_no'=>$params['batch_no'],
                        'po_id'=> $params['id'],
                        'type' => 'AS-IS',
                    );             
             }elseif($params['process_option_id']==2){ //Non-Household List DE-DUP
                      $upload_param=array(
                            'batch_no'=>$params['batch_no'],
                            'po_id'=> $params['id'],
                            'type' => 'NON-H',
                        ); 
             }elseif($params['process_option_id']==3){ //Load then Household list
                      $upload_param=array(
                            'batch_no'=>$params['batch_no'],
                            'po_id'=> $params['id'],
                            'type' => 'LD-H',
                        ); 
             }elseif($params['process_option_id']==4){  //Already-Householded List Load
                     $upload_param=array(
                            'batch_no'=>$params['batch_no'],
                            'po_id'=> $params['id'],
                             'type' => 'AL-H',
                        ); 
             }
          $householded_records= $this->backend->get_upload_summary($upload_param);
         
          $upload_param=array(
                    'batch_no'=>$params['batch_no'],
                    'po_id'=> $params['id'],
                    'type' => 'bad_phone',
          ); 
          $bad_phone = $this->backend->get_upload_summary($upload_param);
          //if($params['process_option_id']==2){
          $no_of_dedup = -1;
          if($params['process_option_id']==2 || $params['process_option_id']==3 || $params['process_option_id']==4){
            //$bad_phone = ($bad_phone+$this->backend->get_total_dedup($upload_param));
            $no_of_dedup = $this->backend->get_total_dedup($upload_param);
          }
         
          //$householded_records = str_replace(',','',$_POST['rec_count']);
          
           if($householded_records==0){
               $householded_records = str_replace(',','',$_POST['rec_count']);
           }
           $total_rec_inFile = $_POST['txtRecInFile'];
           //process_option_id, po_id, batch_no, householded_count,
           //badphone_count,total_count,final_count,dedup_count
           
           $no_of_records = $this->backend->get_total_count($params['batch_no'], $params['id']);
           
           $dedup_from_prev = 0;
           if($params['process_option_id']==2 || $params['process_option_id']==3 || $params['process_option_id']==4){
                $dedup_from_prev = $this->backend->get_dedup_from_previous_file($params['batch_no'], $params['id']);
            }
            
            
            // print_r($dedup_from_prev);
            // die();
            

           
           $insert_summary = array(
               'process_option_id' => $params['process_option_id'], 
               'po_id' => $params['id'],
               'batch_no'=>$params['batch_no'],
               'householded_count' => $householded_records,
               'badphone_count' => $bad_phone,
               //'final_count' => $no_of_records-$bad_phone,
               
               // 'final_count' => $dedup_from_prev+$no_of_records-($bad_phone),
               // 'total_count' => $dedup_from_prev+$no_of_records,
               'final_count' => $no_of_records-($bad_phone),
               'total_count' => $no_of_records,
               
               //'dedup_count' =>  $no_of_dedup ==-1 ? 0 :  $no_of_dedup,
               'dedup_count' =>  $no_of_dedup ==-1 ? $dedup_from_prev :  $no_of_dedup + $dedup_from_prev,
           );
          
           //LC-09/26/2016
           if($insert_summary['final_count'] <=0)
           {
                $this->backend->drop_client_data($params['id'], $params['batch_no']);
                $this->response = array(
                    'data'      =>"",
                    'success'   => false,  
                    'is_drop' => true, 
                    'message'      => "Please check your raw data. It will result to an invalid final count.",  
                ); 
                return;
           }
          
           $this->backend->insert_summary($insert_summary);
           
           
           
           
           $params_calling_list[] = array(
                'list_cnt' => '1',
                'po_id' => $params['id'],
                'batch_no' => $params['batch_no'],
                'center_id' => -1,
                //'record_cnt_to' => $dedup_from_prev+$no_of_records-($bad_phone+$no_of_dedup),
                // 'record_cnt_to' => $no_of_records-($bad_phone+$no_of_dedup),
                'record_cnt_to' => $no_of_records-($bad_phone+$no_of_dedup+$dedup_from_prev),
           );

            // print
            // print_r($params_calling_list);
            // die();
            
            //PP 02/03/2017
           if($params_calling_list[0]['record_cnt_to'] <0)
           {
                   $this->backend->drop_client_data($params['id'], $params['batch_no']);
                   $this->response = array(
                    'data'      =>"",
                    'success'   => false,  
                    'is_drop' => true, 
                    'message'      => "Please check your raw data. It will result to an invalid final count.",  
                ); 
                return;
           } 
            
           $this->backend->apply_data_center_2($params_calling_list, $params['batch_no'], $params['id']);
           
           
           
           $this->response = array(
                //'no_of_dedup' => $no_of_dedup,
                'no_of_dedup' => number_format($no_of_dedup + $dedup_from_prev),
                'householded_records'=>  number_format($householded_records),
                'bad_phone'=> number_format($bad_phone),
                //'final_project_universe'=> number_format($total_rec_inFile-$bad_phone),
                //'final_project_universe'=> number_format($total_rec_inFile-($bad_phone+$dedup_from_prev)),
                'final_project_universe'=> number_format($dedup_from_prev+$no_of_records-($bad_phone+$no_of_dedup)),
                'success'   => true,
                'message'      => 'CSV file has been uploaded.',  
            );
        }else{
             if($res=="0001"){
               $this->response = array(
                    'data'     =>$data_format,
                    'success'   => false,
                    'message'      => 'Invalid format. Please check your upload file.',  
                );
             }else{
                $this->response = array(
                    'data'      =>"",
                    'success'   => false,
                    //'message'      => 'Unable to upload datarow.',  
                    'message'      => $res_call_list['message'],  
                ); 
             }
        
         
        }     
        
        
        
       
   }
   public function import_file()
    {          
                      
       ini_set ('max_execution_time', 10000); 
        $max_upload_size = 60000000; //60M
        //$max_upload_size = 52428800; //50M
        //$max_upload_size = 26214400; //25MB
        //$max_upload_size = 42428800;
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            foreach ($_FILES as $key => $upload) {

                  
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                }elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'ERROR';
                } else {
                        $file_parts = explode('.', $upload['name']);
                        $extension = strtolower(end($file_parts));
                          if (strtolower($extension)=='zip') {
                             $max_upload_size = 20000000; //20MB     
                           }
                      
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted:'.$extension;
                        $this->response['message'] = 'ERROR';
                    }elseif ((!($upload['size'] <= $max_upload_size)) && strtolower($extension)=='zip') {
                        $this->response['message'] = 'FILESIZE_ZIP';
                    }elseif (!($upload['size'] <= $max_upload_size)) {
                        $this->response['message'] = 'FILESIZE';
                    }else {
                       
                        //$file = $this->upload_path. DIRECTORY_SEPARATOR . $upload['name'];
                        //$file_name = $upload['name'];
                        $file_name = time().".{$extension}";
                        $file = $this->upload_path. DIRECTORY_SEPARATOR . $file_name;
                        if (!is_dir($this->upload_path)) @mkdir ($this->upload_path);
                        if (!is_dir($this->upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } 
                        elseif (!move_uploaded_file($upload['tmp_name'], $file)) {
                            $this->response['message'] = 'Error in uploading document';}  
                        else {
                                  $this->response['message'] = $file_name;
                           
                        }
                    }
                }
             
                if ($this->response['message'] != '') { 
                   print_r($this->response['message']);
                } 
                  
                die();
            }
        }
        
    }
 /*   public function import_file()
    {          
            
       ini_set ('max_execution_time', 10000); 
        $max_upload_size = 60000000; //60M
        //$max_upload_size = 52428800; //50M
        //$max_upload_size = 26214400; //25MB
        //$max_upload_size = 42428800;
        //var_dump($max_upload_size);
         
        if (isset($_FILES) && is_array($_FILES)) {
               
            foreach ($_FILES as $key => $upload) {
              
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'FILESIZE';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'ERROR';
                } else {
                    
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted:'.$extension;
                        $this->response['message'] = 'ERROR';
                    } else {
                       
                        //$file = $this->upload_path. DIRECTORY_SEPARATOR . $upload['name'];
                         $file_name = time().".{$extension}";
                        $file = $this->upload_path. DIRECTORY_SEPARATOR . $file_name;
                        if (!is_dir($this->upload_path)) @mkdir ($this->upload_path);
                        if (!is_dir($this->upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } 
                        elseif (!move_uploaded_file($upload['tmp_name'], $file)) {
                            $this->response['message'] = 'Error in uploading document';}  
                        else {
                                  $this->response['message'] = $file_name;
                           
                        }
                    }
                }
             
                if ($this->response['message'] != '') { 
                   print_r($this->response['message']);
                } 
                  
                die();
            }
        }
        
    }
              */
    public function read_file()
    {
           $file=$_GET['file'];              
           $file_zip = $this->upload_path. DIRECTORY_SEPARATOR .$file; 
           $file = $this->upload_path. DIRECTORY_SEPARATOR .$file; 
           $ext = pathinfo($file, PATHINFO_EXTENSION);
           if($ext == "zip")
           {
               $zip = new ZipArchive;
               if ($zip->open($file_zip) === TRUE) {
                   if($zip->numFiles > 1)
                   {
                        $this->response = array(
                            'data'=> array(),
                            'record_count'=> 0,
                            'column'=> '',
                            'message' => "Unable to process zip file. Plase check your zip file content.",
                            'success' => false,
                            'options' => array(),
                            'has_existing' => false,
                        );  
                        return $this->response;
                   }     
                   
                   
                   $file = $this->upload_path. DIRECTORY_SEPARATOR .$zip->getNameIndex(0);    
                   $extension = pathinfo($file, PATHINFO_EXTENSION);
                   if (!(in_array($extension, $this->allowed_extensions))) {
                            
                        $this->response = array(
                            'data'=> array(),
                            'record_count'=> 0,
                            'column'=> '',
                            'message' => "Invalid file",
                            'success' => false,
                            'options' => array(),
                            'has_existing' => false,
                        );  
                        return $this->response;
                   }          
                   $zip->extractTo($this->upload_path. DIRECTORY_SEPARATOR);
                   $filename =   $zip->getNameIndex(0);
                   $zip->close();
                   unlink($file_zip);
                } else {
                    $this->response = array(
                        'data'=> array(),
                        'record_count'=> 0,
                        'column'=>$col,
                        'message' => "Unable to process zip file.",
                        'success' => false,
                        'options' => array(),
                        'has_existing' => false,
                    );
                    return $this->response;
                }
           }
           
           
           
            $row=1;
            $str="";
            $row_count=0;
            $col=array();
            $data_row1 = array();
            $data_row2 = array();
            //PP 02/08/2017
            $fnames_col = "";     
            $fnames_col_cnt = 1;
            $col_postion = 1;
            $is_col_header = true;
            
            if (($handle = fopen($file, "r")) !== FALSE) {
            $fp = file($file);
            while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                $num = count($data);
                
                //PP 02/08/2017
                if($is_col_header){
                   $is_col_header = false; 
                    foreach ($data as $key => $val) {
                        if($val=='fname'.$fnames_col_cnt){
                           $fnames_col = $fnames_col .$val."|"."col".$col_postion.";"; 
                          $fnames_col_cnt++;  
                         }
                        $col_postion++;
                    } 
                }
                
               if($row<=3) {
                for ($c=0; $c < $num; $c++) {   
                        if($row==1){
                          $col[]= $data[$c]; 
                        }elseif($row==2){
                            $data_row1[]=$data[$c];
                        }elseif($row==3){
                             $data_row2[]=$data[$c];
                            
                        }   
                }
               }else{
                    goto end;
               }
                $row++;
            }
            end:
           $row_count = $row-1;    
           fclose($handle); 
           
        } 
        //PP 02/07/2017 
        $initial_data = array();
        $temp_from_file = array();  
        $diff_value = array();
        $diff_value_final = array();
        $temp_from_original=array();
     
         for ($c=0; $c <$num ; $c++) {  
           $temp_from_file = array_merge($temp_from_file,array($col[$c]));
           $initial_data[]= array(
                    $c+1,
                    $col[$c],
                    $data_row1[$c],
                    $data_row2[$c]           
           );
           
       }
       
       //PP 02/02/2017
       $original_col = array();
       if($_GET['batch_no']>1){
           $params = array(                                   
                'po_id'=>$_GET['po_id'],
                'batch_no'=>1, 
           );
           $rs = $this->backend->get_column_data_raw($params);
       
              foreach ($rs as $key => $val) {
                     //PP 02/07/2017
                     $temp_from_original =  array_merge($temp_from_original,array($val));
                     $original_col[] = array($val);
              }
               $diff_value = array_diff($temp_from_file,$temp_from_original);
       }
        //PP 02/07/2017     
         foreach ($diff_value as $key => $val) {
               $diff_value_final[] = array($val);
          }
       
          
       $hasExising = false;
       $options = array();
       if(isset($_GET['po_id'])){
           
       
               $po_id =$_GET['po_id'];
               $batch_no =$_GET['batch_no'];
               $p= $this->backend->get_existing_process_option_id_remarks($batch_no-1, $po_id);
           
            
               if(isset($p['lname_field']))
               {
                          $remarks = unserialize($p['remarks']);
               $misc_fields = unserialize($p['misc_fields']);
                 $hasExising = true;   
                 $options = array(
                    'lname_field' => $p['lname_field'],
                    'fname_field' => $p['fname_field'],
                    'phone_field' => $p['phone_field'],
                    'misc_fields' => $misc_fields,
                    'remarks' => $remarks,
                    'process_option_id'=> $p['process_option_id'],
                 );
               }

        }
        
        
//        if($ext != "zip")
//        {
//            $file=$_GET['file'];   
//        }

         $this->response = array(
                    'data'=>$initial_data,
                    'original_col' =>$original_col, //PP 02/02/2017
                    'diff_value'  =>$diff_value_final, //PP 02/07/2017
                    'record_count'=>count($fp)-1,
                    'column'=>$col,
                    'message' => $_GET['file']." file has been uploaded.",
                    'success' => true,
                    'options' => $options,
                    'has_existing' => $hasExising,
                    'filename' => pathinfo($file, PATHINFO_BASENAME),
          );
       
          
           
              
    }
    public function upload_result($get="") {
        $actions = array('import_file','read_file','process_data','process_finished_data');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
        
        $params = array(
                    'id' =>$get[0],
        );
        $checking = array();
        $isTrue = 0;
        if($get[0]>0){
            $formated_po_id=$this->backend->get_po_number($get[0]);
            $this->view->assign('formated_po_id', $formated_po_id); 
            $checking=$this->backend->check_data_if_exist($params);
            if(count($checking)>0){
               $isTrue = 1; 
            }
            $po_status=$this->backend->check_po_status($params);
             if($po_status['status']=='HD'){    //IF the  status is Hold for Data.
               $this->phpAlertWithRedirect("You're not allow to upload result.","../datamanagement/{$get[0]}"); 
            }
            
        }else{
             $isTrue = 1;
        }
        
        
        if($isTrue==1){
            $formated_po_id= $get[0];
            $process_list_option = $this->backend->get_process_list_option();
            $response = $this->backend->get_response();
            $process_list_option = $response['ResponseMessage'];
            $this->view->assign('process_list_option', $process_list_option);
            $this->view->assign('po_id', $get[0]);
            $center_list = $this->backend->get_center_list();
            $response = $this->backend->get_response();
            $center_list = $response['ResponseMessage'];
            $this->view->assign('center_list', $center_list);
        }else{
            $this->phpAlertWithRedirect("Please upload first the raw data.","../datamanagement/{$get[0]}");
        }
        
       
    
    }
    protected function update_finale_universe()
    {
      
        $params = array(
          'po_id'=>$_GET['id'],
          'universe'=> $_GET['universe'],
        );    
        
        
        $result = $this->backend->update_final_universe($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                        'success' => true,
                        'message' => "PO final universe has been updated.",
                    ); 
        }
    }
    public function upload_data($get="") {
        $actions = array('import_file','read_file','process_data','process_finished_data','addcenter_tolist','apply_data_center','reload_param_center','update_finale_universe');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
            return;
        }  
       
       
        $formated_po_id= $get[0];
        $process_list_option = $this->backend->get_process_list_option();
        
 
        
        $response = $this->backend->get_response();
        $process_list_option = $response['ResponseMessage'];
        $this->view->assign('process_list_option', $process_list_option);
        $this->view->assign('po_id', $get[0]);
        $batch_no=$this->backend->get_uploadbatchno($formated_po_id);
        
        $has_old_upload = $this->backend->has_existing_batch_without_applied_center($batch_no-1, $formated_po_id);
        
        if($batch_no-1==0)
        {
            $has_old_upload =0;
        }
    
//        if($has_old_upload==1)
//        {     
//        }else{
            $old_data = $this->backend->get_existing_process_option_id_remarks($batch_no-1, $formated_po_id);
            if(isset($old_data['process_option_id']))
            {
                 if($old_data['process_option_id']==1){ //Non-Household List AS-IS
                       $upload_param=array(
                            'batch_no'=>$batch_no-1,
                            'po_id'=> $formated_po_id,
                            'type' => 'AS-IS',
                        );             
                 }
                 elseif($old_data['process_option_id']==2){ //Non-Household List DE-DUP
                          $upload_param=array(
                                'batch_no'=>$batch_no-1,
                                'po_id'=> $formated_po_id,
                                'type' => 'NON-H',
                            ); 
                 }
                 elseif($old_data['process_option_id']==3){ //Load then Household list
                          $upload_param=array(
                                'batch_no'=>$batch_no-1,
                                'po_id'=> $formated_po_id,
                                'type' => 'LD-H',
                            ); 
                 }
                 elseif($old_data['process_option_id']==4){  //Already-Householded List Load
                         $upload_param=array(
                                'batch_no'=>$batch_no-1,
                                'po_id'=> $formated_po_id,
                                'type' => 'AL-H',
                            ); 
                 }
                 if($has_old_upload==0)
                 {
                    $upload_param['batch_no']= $batch_no; 
                 }
                 
                 $summary = $this->backend->get_uploaded_summary_count($upload_param['batch_no'], $formated_po_id);

//                      $householded_records= $this->backend->get_upload_summary($upload_param);
//                      $upload_param=array(
//                                'batch_no'=>$batch_no-1,
//                                'po_id'=> $formated_po_id,
//                                'type' => 'bad_phone',
//                      ); 
//                      if($has_old_upload==0)
//                      {
//                        $upload_param['batch_no']= $batch_no; 
//                      }
//                      $bad_phone = $this->backend->get_upload_summary($upload_param);
//                      $no_of_dedup = 0;
//                      if($old_data['process_option_id']==2 || $old_data['process_option_id']==3 || $old_data['process_option_id']==4){
//                        $no_of_dedup = $this->backend->get_total_dedup($upload_param);
//                      }
//                       $no_of_records = $this->backend->get_total_count($batch_no-1, $formated_po_id);
//                       if($householded_records==0){
//                           $householded_records = $this->backend->get_total_count($batch_no-1, $formated_po_id);
//                       }
                if(isset($summary['id']))
                {
                   $no_of_records =$summary['total_count']; 
                   $no_of_dedup= $summary['dedup_count'];
                   $householded_records = $summary['householded_count'];
                   $bad_phone = $summary['badphone_count']; 
                }else{
                   $no_of_records =0; 
                   $no_of_dedup= 0;
                   $householded_records =0;
                   $bad_phone =0;
                }
                $this->view->assign('no_of_records', $no_of_records); 
                $this->view->assign('no_of_dedup', $no_of_dedup);     
                $this->view->assign('householded_records', $householded_records);  
                $this->view->assign('bad_phone', $bad_phone); 
                $this->view->assign('final_project_universe', $no_of_records-($bad_phone+$no_of_dedup)); 
                
                
                
                $this->view->assign('has_existing', $has_old_upload); 
                $this->view->assign('has_existing_option', 1);
//                print_r($no_of_records-($bad_phone+$no_of_dedup));
//                die();
                
                if($has_old_upload==0)
                {
                    $this->view->assign('batch_no', $batch_no);   
                }else{
                     $this->view->assign('batch_no', $batch_no-1);
                }
                   
            }
            else{
                $this->view->assign('has_existing', 0); 
                $this->view->assign('batch_no', $batch_no); 
                $this->view->assign('no_of_records', number_format(0)); 
                $this->view->assign('no_of_dedup', number_format(0));       
                $this->view->assign('householded_records', number_format(0));  
                $this->view->assign('bad_phone', number_format(0)); 
                $this->view->assign('has_existing', $has_old_upload);
                $this->view->assign('final_project_universe', number_format(0)); 
            }
             
            
//            $this->view->assign('has_existing', 0); 
//            $this->view->assign('batch_no', $batch_no); 
//            $this->view->assign('no_of_records', number_format(0)); 
//            $this->view->assign('no_of_dedup', number_format(0));       
//            $this->view->assign('householded_records', number_format(0));  
//            $this->view->assign('bad_phone', number_format(0)); 
//            $this->view->assign('final_project_universe', number_format(0)); 
//        }
        
        
        $formated_po_id=$this->backend->get_po_number($get[0]);
        $this->view->assign('formated_po_id', $formated_po_id); 
      //  $this->view->assign('po_name',$this->backend->get_poname($get[0]));
        $center_list = $this->backend->get_center_list();
        $response = $this->backend->get_response();
        $center_list = $response['ResponseMessage'];
        $this->view->assign('center_list', $center_list);
        $params = array('id' => $get[0],); 
        /*$result = $this->backend->get_poid_by_encrypted_id($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
                $formated_po_id=sprintf('%06d',$response['ResponseMessage']);
        } 
        $this->view->assign('formated_po_id', $formated_po_id);     */
    
    }
    
     public function po_script_response() {
        $actions = array('add_response_tolist','reload_param','add_po_script','download_script','reload_delete_response');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
           
    }
    
     public function view_script($get="") {
        $actions = array('');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
        
         $param=array(
                'id' => $get[0],
            ); 
            
           
            $num_of_q = $this->backend->get_po_number_of_question($param);  
            $response = $this->backend->get_response();
            $num_of_q = $response['ResponseMessage'];
            $this->view->assign('num_of_q', $num_of_q);
            $this->view->assign('po_id', $param['id']);
            
            //Opening
            $sql="select question from po_script_question where sequence=0 and po_id =". $param['id'];
            $result = $this->db->fetchRow($sql);
            $this->view->assign('opening', $result['question']);   
            //Closing  
            $close_num = $num_of_q + 1;
            $sql="select question from po_script_question where sequence= {$close_num} and po_id =". $param['id'];
            $result = $this->db->fetchRow($sql);
            $this->view->assign('closing', $result['question']);

            //Questions
            $sql="select id,sequence, question from po_script_question where sequence>0 and sequence<{$close_num} and po_id =". $param['id'];
            $result = $this->db->fetchAll($sql);
            
             $questions_response = array();
             foreach ($result as $row) {
                 $sql="select r.row_no,r.response,  r.go_to from po_script_question q"; 
                 $sql .=" inner join po_script_q_response r on(q.id = r.po_script_question_id) where po_script_question_id=".$row['id'];
                 $sql .=" group by q.sequence,r.row_no";
                
                 $res = $this->db->fetchAll($sql);
                 
                    $questions_response[] = array(
                        'sequence' =>$row['sequence'],
                        'question' =>$row['question'],
                        'response' => $res,
                    );
        }
            
            
            $this->view->assign('questions_response', $questions_response);
            
        
     }
    
     public function po_script($get="") {
        $actions = array('get_number_of_questions');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];              
            $this->layout = 'json';
            $this->$action();
        }
     
            $param=array(
                'id' => $get[0],
            ); 
            
           
            $num_of_q = $this->backend->get_po_number_of_question($param);   
            $response = $this->backend->get_response();
            $num_of_q = $response['ResponseMessage'];
            $this->view->assign('num_of_q', $num_of_q);
            $this->view->assign('po_id', $param['id']);
            
            
            //Opening
            $sql="select question from po_script_question where sequence=0 and po_id =". $param['id'];
            $result = $this->db->fetchRow($sql);
            $this->view->assign('opening', $result['question']);  
           
            //Closing  
            $close_num = $num_of_q + 1;
            $sql="select question from po_script_question where sequence= {$close_num} and po_id =". $param['id'];
            $result = $this->db->fetchRow($sql);
            $this->view->assign('closing', $result['question']);

            //Questions
            $sql="select id,sequence, question,rate from po_script_question where sequence>0 and sequence<{$close_num} and po_id =". $param['id'];
            $result = $this->db->fetchAll($sql);
            
           
             $questions_response = array();
             foreach ($result as $row) {
//                 $sql="select r.row_no,r.response,  r.go_to from po_script_question q"; 
                 $sql="select r.row_no,r.response,  r.go_to, r.rate from po_script_question q"; 
                 $sql .=" inner join po_script_q_response r on(q.id = r.po_script_question_id) where po_script_question_id=".$row['id'];
                 $sql .=" group by q.sequence,r.row_no";
                 
                 $res = $this->db->fetchAll($sql);
                   $res_2 = array();
                   $rowIndex = 0;
                   

                   foreach ($res as $row_2) {
                       
                       if($row_2['rate'] !="")
                       {
                        $rate = $row_2['rate'];
                       }else{
                          $rate =0.00; 
                       }
                        $res_2[]=array(
                            'row_no'=>$row_2['row_no'],
                            'response'=>$row_2['response'],
                            'go_to' => $row_2['go_to'],
                            'rate' => $rate, //rate for response
                            'button'=>'<a href="javascript:void(0);">
                            <image align="right" onclick="show_edit_response('.($rowIndex).'); return false;" src="'.WEBROOT.'/public/images/edit.png" title="cancel" /></a>
                            <a href="javascript:void(0);">
                            <image align="right" onclick="delete_response('.$rowIndex.');" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>',
                        );
                        $rowIndex++;
                   }
                  
                    $questions_response[] = array(
                        'sequence' =>$row['sequence'],
                        'question' =>$row['question'],
                        'rate'     =>$row['rate'],
                        'response' => $res_2,
                    );
                    
                    
        }
            

           
            
            $this->view->assign('questions_response', $questions_response);
            $this->view->assign('questions_response_count', count($questions_response));
            $this->view->assign('question', $questions_response);
         
    }
    
    protected function download_script()
    {
        
            $rowPosition=1;
            $countRecord=0;
            
            require_once "PHPExcel.php"; 
            //Header
            $excel = new PHPExcel(); 
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('PO Script');
        
        $param=array(
                'id' => $_GET['id'],
            ); 
            
           
            $num_of_q = $this->backend->get_po_number_of_question($param);  
            $response = $this->backend->get_response();
            $num_of_q = $response['ResponseMessage'];
            $po_no = $this->backend->get_po_number($param['id']);
            
            //Opening
            $sql="select question from po_script_question where sequence=0 and po_id =". $param['id'];
           $result_opening = $this->db->fetchRow($sql);
           $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit("Opening", PHPExcel_Cell_DataType::TYPE_STRING);
           $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
           $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
           $excel->getActiveSheet();
           $excel->getActiveSheet()->getColumnDimension('B')->setWidth(70);
           $excel->getActiveSheet()->getCell('B'.($rowPosition+1))
                    ->setValueExplicit($result_opening['question'], PHPExcel_Cell_DataType::TYPE_STRING);
            $excel->getActiveSheet()->getStyle('B'.($rowPosition+1))->getAlignment()->setWrapText(true);
            $excel->getActiveSheet();
            
            
              
            //Closing  
            $close_num = $num_of_q + 1;
            $sql="select question from po_script_question where sequence= {$close_num} and po_id =". $param['id'];
            $result_closing = $this->db->fetchRow($sql);
            

            //Questions
            $sql="select id,sequence, question from po_script_question where sequence>0 and sequence<{$close_num} and po_id =". $param['id'];
            $result_q = $this->db->fetchAll($sql);
            
             $questions_response = array();
             foreach ($result_q as $row) {
                 $sql="select r.row_no,r.response,  r.go_to from po_script_question q"; 
                 $sql .=" inner join po_script_q_response r on(q.id = r.po_script_question_id) where po_script_question_id=".$row['id'];
                 $sql .=" group by q.sequence,r.row_no";
                 $res = $this->db->fetchAll($sql);
                 
                    $questions_response[] = array(
                        'sequence' =>$row['sequence'],
                        'question' =>$row['question'],
                        'response' => $res,
                    );
            }
        

            
            foreach ($questions_response as $key => $val) {
       
                $rowPosition++;
                $excel->getActiveSheet()->getCell('A'.($rowPosition+1)) 
                    ->setValueExplicit("Question ".$val['sequence'], PHPExcel_Cell_DataType::TYPE_STRING);
               $excel->getActiveSheet()->getStyle('A'.($rowPosition+1))->getFont()->setBold(true);
                       $rowPosition=$rowPosition+2;
                $excel->getActiveSheet()->getCell('B'.$rowPosition)
                        ->setValueExplicit($val['question'], PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getAlignment()->setWrapText(true);        
                $excel->getActiveSheet();
                    foreach ($val['response'] as $key => $val_res) {
                                     $rowPosition++;
                            $excel->getActiveSheet()->getCell('B'.($rowPosition+1)) 
                                ->setValueExplicit("Response ".$val_res['row_no'] ." : ".$val_res['response'], PHPExcel_Cell_DataType::TYPE_STRING);
                                $excel->getActiveSheet()->getStyle('B'.($rowPosition+1))->getAlignment()->setIndent(1);
                                $rowPosition=$rowPosition+1;
                               if($close_num==$val_res['go_to']){
                                     $excel->getActiveSheet()->getCell('B'.($rowPosition+1)) 
                                     ->setValueExplicit("Goto-> Closing" , PHPExcel_Cell_DataType::TYPE_STRING);
                               }else{
                                    $excel->getActiveSheet()->getCell('B'.($rowPosition+1)) 
                                        ->setValueExplicit("Goto-> ".$val_res['go_to'] , PHPExcel_Cell_DataType::TYPE_STRING);
                                   
                               }
                                     $excel->getActiveSheet()->getStyle('B'.($rowPosition+1))->getAlignment()->setIndent(2);
                                   $rowPosition=$rowPosition+1;
                                
                            $excel->getActiveSheet();
                    }
                
                
                
            }         
              $rowPosition++;
            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit("Closing", PHPExcel_Cell_DataType::TYPE_STRING);
           $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);     
           $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
           $excel->getActiveSheet();
           $excel->getActiveSheet()->getColumnDimension('B')->setWidth(70);
           $excel->getActiveSheet()->getCell('B'.($rowPosition+1))
                    ->setValueExplicit($result_closing['question'], PHPExcel_Cell_DataType::TYPE_STRING);
           $excel->getActiveSheet()->getStyle('B'.($rowPosition+1))->getAlignment()->setWrapText(true);
           $excel->getActiveSheet();
              
                

               
            $date=date('Y-m-d H:i:s');
            $filename="{$po_no}_POSCRIPT_{$date}.xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
                         
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');  
            
           
            

          
    } 

    
    protected function reload_param_center()
   {
       
    $data = serialize(explode( '|', $_GET['fields'],-1 ) );
    $search_result = array();
    $datafields=unserialize($data);
    $fields = array();
    $rowIndex= 0;
   if($datafields!=""){
        foreach($datafields  as $key => $val) {
            $data_ex = explode( ',',$val);
         $fields[]=array_merge(
            $data_ex,
             array('3'=> '<a href="javascript:void(0)";><input type="image" align="right" onclick="delete_centertolist('.$rowIndex.','.$data_ex[2].'); return false;" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>',)
        );
         $rowIndex++;  
        }
   }
    
   
    $this->response = array(
        'success'   => true,
        'message'      => $fields,
    );
          
   }
    
   protected function reload_param_center_temp()
    {
       
        $params = $_POST;     
        
        $row = explode( '|', $_GET['fields']);
        
        foreach($row as $res_row) 
        { $temp[] = explode(",",$res_row); }
        foreach($temp as $val) 
        {          
                
            $fields[] = array(
                $val[0],
                $val[1],
                $val[2],
                '<a href="javascript:void(0)"; onclick="return false;">
                <input type="image" align="right" onclick="delete_centertolist('.$val[0].')" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>',);
        }  
       
        $this->response = array(
            'success'   => true,
            'data'      => $fields,
        );
                  
   }
    
    protected function reload_delete_response()
   {
      
         $data = serialize(explode( '|', $_GET['fields'],-1 ) );
            $search_result = array();
            $datafields=unserialize($data);
            $fields = array();
            $rowIndex= 0;
          
           if($datafields!=""){
                foreach($datafields  as $key => $val) {
                     $fields[]=array_merge(
                         array('0'=> $rowIndex+1),
                         explode( ',',$val),
                         array('4'=> '<a href="javascript:void(0)";><input type="image" align="right" onclick="show_edit_response('.($rowIndex).'); return false;" src="'.WEBROOT.'/public/images/edit.png" title="cancel" /></a>
                         <a href="javascript:void(0)"; onclick="return false;"><input type="image" align="right" onclick="delete_response('.$rowIndex.');" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>',)
                     );
                  $rowIndex++; 
                }
           }
            
           
            $this->response = array(
                'success'   => true,
                'message'      => $fields,
            );
                  
   }
    protected function reload_param()
   {
      
         $data = serialize(explode( '|', $_GET['fields'],-1 ) );
            $search_result = array();
            $datafields=unserialize($data);
            $fields = array();
            $rowIndex= 0;
          
           if($datafields!=""){
                foreach($datafields  as $key => $val) {
                     $fields[]=array_merge(
                         explode( ',',$val),
                         array('4'=> '<a href="javascript:void(0)";><input type="image" align="right" onclick="show_edit_response('.($rowIndex).'); return false;" src="'.WEBROOT.'/public/images/edit.png" title="cancel" /></a>
                         <a href="javascript:void(0)"; onclick="return false;"><input type="image" align="right" onclick="delete_response('.$rowIndex.');" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" title="cancel" /></a>',)
                     );
                  $rowIndex++; 
                }
           }
            
           
            $this->response = array(
                'success'   => true,
                'message'      => $fields,
            );
                  
   }
    protected function add_response_tolist()
   {    
   
        $goto =$_POST['goto']; 
          if ( $goto==($_POST['NumOfQuestion']+1)){
            $goto='Closing';   
          }  
       $field_info= array(
            $_POST['row_index'],
            $_POST['txtResponse'],
            $goto,
            isset($_POST['txtRate'])?$_POST['txtRate']:"0.00",
            ''
           /* '<input type="image" onclick="" src="'.WEBROOT.'/public/images/edit.png" title="edit" />',*/
              /*'<input type="image" onclick="" src="'.WEBROOT.'/public/images/deleted.png" title="edit" />*/
       );
      
       
        $this->response = array(
                    'data'=>$field_info,
                    'success' => true,
                ); 
   }
    
     protected function add_po_script(){
        
                
       $result = $this->backend->add_po_script($_POST);
       $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $po = $this->backend->get_po();
        $response = $this->backend->get_response();
        $po = $response['ResponseMessage'];
        $out_po = array(); 
        foreach ($po as $row) {
            $out_po[] = array(
                sprintf('%06d',$row['id']),
                $row['client_name'],
                $row['po_name'],
                $row['status'],
                $row['template_name'],
                '',
                '',
               '<input type="image" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeletePO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />
                <input type="image" style="height: 30px;width: 30px"  onclick="ScriptPO('.$row['id'].')" src="'.WEBROOT.'/public/images/script.png" title="script" />',
            );
        }
     

            $this->response = array(
                'data'=>$out_po,
                'success' => true,
                'message' => "PO script has beed added.",
            ); 
           
            
        }  
    }
    
    
    
    
    public function projecttemplate() {
        $actions = array('get_project_template','add_projecttemplate','get_project_template_byid','delete_projecttemplate','edit_projecttemplate');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        
         //User type for access rights  
            //$user_type = $this->backend->get_usertype($_SESSION['username']);
            //$this->view->assign('user_type', $user_type); 
            
            $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
        
        $client_list = $this->backend->get_client_list($_SESSION["client_id"]);
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $this->view->assign('client_list', $client_list);
        
          
        $report_type_list = $this->backend->get_report_type_list();
        $response = $this->backend->get_response();
        $report_type_list = $response['ResponseMessage'];
        $this->view->assign('report_type_list', $report_type_list);
        
          
        $projecttemplate_list = $this->backend->get_project_template();
        $response = $this->backend->get_response();
        $projecttemplate_list = $response['ResponseMessage'];
        $out_projecttemplate_list = array(); 
        foreach ($projecttemplate_list as $row) {
           //View  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $view_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />';
            }else{
                $view_button="";
            }
            //Edit  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $edit_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',false)" src="'.WEBROOT.'/public/images/edit.png" title="edit" />';
            }else{
                $edit_button="";
            }
           //Delete 
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" onclick="DeleteProjectTemplate('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
            
            
            
            $out_projecttemplate_list[] = array(
                'id' => sprintf('%06d',$row['id']),
                'template_name' =>  $row['template_name'],
                'client_name' =>$row['client_name'],
                'report_type' =>$row['report_type'],
                'status' =>$row['status'],
                'is_auto_call' =>$row['is_auto_call'],
                'button' =>$view_button . $edit_button . $delete_button,
            );
        }
                
           $this->view->assign('projecttemplate_list', $out_projecttemplate_list);
    }
    
    protected function get_number_of_questions()
    {
       $id= $_GET['id'];
               
        $params = array(
        'id' => $id,
        );
        $result = $this->backend->get_po_number_of_question($params);
        $response = $this->backend->get_response();
       
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
           
            foreach ($response['ResponseMessage'] as $key => $val) {
                $search_result[] = array(
                        'number_of_question' =>$val['number_of_question'],
            );
            }
         
           
        } 
    } 
    
     protected function get_client_databack_fields()
    {
         $id= $_GET['po_id'];
               
        $params = array(
        'id' => $id,
        ); 
        $result = $this->backend->get_client_databack_fields($params);
        $response = $this->backend->get_response();
       
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            
                $search_result = array(
                         unserialize($response['ResponseMessage']), //customer_databack_fields
            );
                $search_result_out = array();
                foreach (unserialize($response['ResponseMessage']) as $row) {
                    $search_result_out[] = array(
                        $row,
                        $row,
                    );
                }
            
         
            $this->response = array(
                'success'   => true,
                'data'      => $search_result_out,
            );
        }          
   }
    
    
    
    public function project_list() {
        
      
        $actions = array('project_glance','add_project','delete_po','get_po_byid','edit_project','get_client_databack_fields','get_po_template_listby_id','get_po_listby_id','get_po_listby_all_id','get_po_template_listby_all_id','file_type','hide_show_completed');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
       
        
        
            //User type for access rights
            
            //$user_type = $this->backend->get_usertype($_SESSION['username']);
            $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
            $this->view->assign('user_type_id', $_SESSION['user_type']);
           $options = array(); $min30=array('00','30');
           foreach (range(0,23) as $fullhour) {
           $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
           foreach($min30 as $int){
            if($fullhour > 11){
                $options[$parthour.":".$int." PM"]=$parthour.":".$int." PM";
            }else{
                if($fullhour == 0){$parthour='12';}
                $options[$parthour.":".$int." AM"]=$parthour.":".$int." AM" ;
                }
              }
            }
        $this->view->assign('time_list', $options);
        
        $select = array('<----SELECT---->');
        $client_list = $this->backend->get_client_list($_SESSION['client_id']);
        
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $client_list= $select+$client_list;    
        
        $this->view->assign('clients', $client_list);
        
        
        
       /* $contact_list = $this->backend->get_contact_list();
        $response = $this->backend->get_response();
        $contact_list = $response['ResponseMessage'];
        $contact_list= $select + $contact_list;
        $this->view->assign('contact', $contact_list);*/
           
             
        $state_list = $this->backend->get_states_list();
        $response = $this->backend->get_response();
        $state_list = $response['ResponseMessage'];
        $state_list= $select + $state_list;
        $this->view->assign('state', $state_list);
      
        $po_list = $this->backend->get_po_listby_id(-1);
        $response = $this->backend->get_response();
        $po_list = $response['ResponseMessage'];
        $po_list= $select+$po_list;
        $this->view->assign('po_list', $po_list);
        
    
            
        $po_template_list = $this->backend->get_po_template_listby_id(-1);
        $response = $this->backend->get_response();
        $po_template_list = $response['ResponseMessage'];
        $po_template_list= $select+$po_template_list;
        $this->view->assign('po_template_list', $po_template_list);  

          
        
        $goal_type_list = $this->backend->get_goal_type();
        $response = $this->backend->get_response();
        $goal_type_list = $response['ResponseMessage'];
        $goal_type_list= $select+$goal_type_list;
        $this->view->assign('goal_type_list', $goal_type_list);  
        
       
        $po_status_list = $this->backend->get_project_status();
        $response = $this->backend->get_response();

        $po_status_list = $response['ResponseMessage'];
        $po_status_list= $po_status_list;
                
        $this->view->assign('po_status', $po_status_list); 
       
        $po = $this->backend->get_po();

        $response = $this->backend->get_response();
        $po = $response['ResponseMessage'];
        $out_po = array(); 
         
        foreach ($po as $row) {
            
             if(in_array($user_type,array('Centers','Clients','Data','Admin','System Admin','Managers'))){
                $view_button='<input type="image"  style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="View PO" />';
            }else{
                $view_button="";
            }
            if(in_array($user_type,array('Clients','Data','Admin','System Admin','Managers'))){
                $edit_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="Edit PO" />';
            }else{
                $edit_button="";
            }
            
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="DeletePO('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="Change Status" />';
            }else{
                $delete_button="";
            }
            
            if($this->backend->check_if_has_script($row['id']) && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))){
                $view_script=' <a href="'.WEBROOT.'/pomaintenance/view_script/'. $row['id'].'">'."<img src='".WEBROOT."/public/images/script.png' alt='View Script' style='height: 25px;width: 25px;' />".'</a>';
				
				
				//."<input type='image' style='height: 30px;width: 30px' src='".WEBROOT."/public/images/script.png' title='View Script' />"  ;   

            }else{
                $view_script='';   
            }  
            
            //Report
                $report_module = "";
				// if($row['id'] == 25)
				// {
					// print_r($row['allow_report']);
					// die();
				// }

         
                if(  $row['allow_report']==1 && ($this->backend->check_data_if_result_exist($row['id'])>0 || $this->backend->check_data_if_manual_data_exist($row['id'])>0)  && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))
				){
                    $report_module=' <a href="'.WEBROOT.'/pomaintenance/report?po_id='. $row['id'].'">'."<img src='".WEBROOT."/public/images/generate.png' alt='View Script' style='height: 25px;width: 25px;' />".'</a>';
                }else{
                    $report_module="";
                }
            //
            
            if($row['status']!='ACTIVE'  && in_array($user_type,array('Data','Managers','Clients','Admin','System Admin'))){
              //$edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'> <input type="image" style="height: 30px;width: 30px" src="'.WEBROOT.'/public/images/edit_script.png" title="Edit Script" /></a>';
			  $edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'> <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/edit_script.png" alt="Edit Script" /></a>';
            }else{
                $edit_script  ='';
            }
            $out_po[] = array(
                'id' => $row['po_no'],
                'client_name' =>  $row['client_name'],
                'po_name' =>'<u><a  href='.WEBROOT.'/pomaintenance/project_glance/'. $row['id'].'>'.$row['po_name'].'</a></u>',
                'status'=> $row['status'],
                'start_date' => $row['start_date'],
                'end_date' => $row['end_date'],
                'template_name'=> $row['template_name'],
                'universe' => $row['universe'],
                'complete' => $row['complete'],
                'button' =>
                 $view_button
                 .$edit_button
                 .$delete_button
                 .$view_script
                 .$edit_script
                 .$report_module
       
            );
        }
                
           $this->view->assign('out_po', $out_po);
        
        
    }
    protected function add_project(){
       
        
      $result = $this->backend->add_project($_POST);
      $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
        $po = $this->backend->get_po();
        $response = $this->backend->get_response();
        $po = $response['ResponseMessage'];
        $out_po = array(); 
         //User type for access rights    
      /*  $user_type = $this->backend->get_usertype($_SESSION['username']);*/
       $user_type   = $_SESSION['user_type_desc'];
        foreach ($po as $row) {
            
             if(in_array($user_type,array('Centers','Clients','Data','Admin','System Admin','Managers'))){
                $view_button='<input type="image" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="View PO" />';
            }else{
                $view_button="";
            }
            if(in_array($user_type,array('Clients','Data','Admin','System Admin','Managers'))){
                $edit_button='<input type="image" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="Edit PO" />';
            }else{
                $edit_button="";
            }
            
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" onclick="DeletePO('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
            
             if($this->backend->check_if_has_script($row['id']) && in_array($user_type,array('Data','Managers','Clients','Centers','Admin','System Admin'))){
                $view_script=' <a href='.WEBROOT.'/pomaintenance/view_script/'. $row['id'].'>
                <img style="height: 30px;width: 30px" src="'.WEBROOT.'/public/images/script.png" alt="View Script" /></a>';   
            }else{
                $view_script='';   
            }  
            if($row['status']!='ACTIVE' && in_array($user_type,array('Data','Managers','Clients','Admin','System Admin'))){
              $edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'>  <img style="height: 30px;width: 30px" src="'.WEBROOT.'/public/images/edit_script.png" alt="Edit Script" /></a>';
                                                                                                                
            }else{
                $edit_script  ='';
            }
            
               //Report
                $report_module = "";
               if( $row['allow_report']==1 && $this->backend->check_data_if_result_exist($row['id'])>0 || $this->backend->check_data_if_manual_data_exist($row['id'])>0  && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))){
                    $report_module=' <a href="'.WEBROOT.'/pomaintenance/report?po_id='. $row['id'].'">'."<img src='".WEBROOT."/public/images/generate.png' alt='View Script' style='height: 30px;width: 30px;' />".'</a>';
                }else{
                    $report_module="";
                }
            //
            
            
            $out_po[] = array(
                $row['client_code'].sprintf('%06d',$row['id']),
                $row['client_name'],
                '<u><a  href='.WEBROOT.'/pomaintenance/project_glance/'. $row['id'].'>'.$row['po_name'].'</a></u>',
                $row['status'],
                $row['template_name'],
               $row['start_date'],
               $row['end_date'],
               $row['universe'],
               $row['complete'],
                $edit_button
               .$view_button
               .$delete_button 
               .$view_script
               .$edit_script
               .$report_module
                
            );
        }

            $this->response = array(
                'data'=>$out_po,
                'success' => true,
                'message' => "New PO has been added.",
            ); 
            
        }  
    }   
    protected function get_po_byid()
    {
    $id= $_GET['id'];
               
        $params = array(
        'id' => $id,
        ); 
        $result = $this->backend->get_po_byid($params);
        $response = $this->backend->get_response();
         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
             $po_list_by_client=$this->get_po_listby_all_id($response['ResponseMessage'][0]['client_id']);
             $po_temp_by_client= $this->get_po_template_listby_all_id($response['ResponseMessage'][0]['client_id']);
            $search_result = array();
            foreach ($response['ResponseMessage'] as $key => $val) {  
                $search_result[] = array(
                        'formated_id'                 =>sprintf('%06d',$val['id']),
                        'po_name'                     =>$val['po_name'],
                        'client_id'                     =>$val['client_id'],
                        'project_id_old'=>$val['project_id_old'],
                        'project_template_id' =>$val['project_template_id'],
                        'contact_id'=>$val['contact_id'],  
                        'start_date'=>date('Y-m-d',strtotime($val['start_date'])), 
                        'start_time'=>$val['start_time'], 
                        'end_date'=> date('Y-m-d',strtotime($val['end_date'])), 
                        'end_time'=>$val['end_time'], 
                        'state'=>$val['state'], 
                        'universe'=>$val['universe'],  
                        'total_contact'=>$val['total_contact'],  
                        'caller_id'=>$val['caller_id'],  
                        'goal_value'=>$val['goal_value'],
                        'goal_days_of_week'=>explode('-',$val['goal_days_of_week']),
                        'goal_type'=>$val['goal_type'],  
                        'goal_note'=>$val['goal_note'], 
                        'number_of_question'=>$val['number_of_question'], 
                        'campaign_note'=>$val['campaign_note'], 
                        'center_note'=>$val['center_note'], 
                        'manager_note'=>$val['manager_note'], 
                        'email_list'=>unserialize($val['email_list']), 
                        'allow_download'=>$val['allow_download'], 
                        'allow_report'=>$val['allow_report'], 
                        'allow_grab_report'=>$val['allow_grab_report'], 
                        'allow_blank_question_zero'=>$val['allow_blank_question_zero'], 
                        'allow_disable_logic'=>$val['allow_disable_logic'], 
                        'custom_status'=>$val['custom_status'], 
                        'custom_databack'=>$val['custom_databack'], 
                        'customer_response_map'=>$val['customer_response_map'], 
                        'customer_databack_fields'=> unserialize($val['customer_databack_fields']),
                        //'misc_field_databack'=>$val['misc_field_databack'],
                        'status'=>$val['po_status'], 
                        'report_template'=>$val['report_template'], 
                        'manager_id'=>$val['manager_id'],
                        'po_list_by_client'=>$po_list_by_client,
                        'po_temp_by_client'=>$po_temp_by_client,
            );
            
            }
               
     
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
            );
        }          
   }
    protected function edit_project(){
       
      
      $result = $this->backend->edit_project($_POST);
      $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
        $po = $this->backend->get_po();
        $response = $this->backend->get_response();
        $po = $response['ResponseMessage'];
        $out_po = array(); 
         //User type for access rights    
        // $user_type = $this->backend->get_usertype($_SESSION['username']);
         $user_type   = $_SESSION['user_type_desc'];
        foreach ($po as $row) {
            
             if(in_array($user_type,array('Centers','Clients','Data','Admin','System Admin','Managers'))){
                $view_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="View PO" />';
            }else{
                $view_button="";
            }
            if(in_array($user_type,array('Clients','Data','Admin','System Admin','Managers'))){
                $edit_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="Edit PO" />';
            }else{
                $edit_button="";
            }
            
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="DeletePO('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
             if($this->backend->check_if_has_script($row['id']) && in_array($user_type,array('Data','Centers','Managers','Clients','Admin','System Admin'))){
                $view_script=' <a href='.WEBROOT.'/pomaintenance/view_script/'. $row['id'].'>
                 <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/script.png" alt="View Script" /></a>';   
            }else{
                $view_script='';   
            }  
            if($row['status']!='ACTIVE' && in_array($user_type,array('Data','Managers','Clients','Admin','System Admin'))){
              $edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'>  <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/edit_script.png" alt="Edit Script" /></a>';  
            }else{
                $edit_script  ='';
            } 
            
             //Report
                $report_module = "";
               if( $row['allow_report']==1 && $this->backend->check_data_if_result_exist($row['id'])>0 || $this->backend->check_data_if_manual_data_exist($row['id'])>0  && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))){
                    $report_module=' <a href="'.WEBROOT.'/pomaintenance/report?po_id='. $row['id'].'">'."<img src='".WEBROOT."/public/images/generate.png' alt='View Script' style='height: 25px;width: 25px;' />".'</a>';
                }else{
                    $report_module="";
                }
            //
            
            $out_po[] = array(
                //$row['client_code'].sprintf('%06d',$row['id']),
                $row['po_no'],
                $row['client_name'],
                '<u><a  href='.WEBROOT.'/pomaintenance/project_glance/'. $row['id'].'>'.$row['po_name'].'</a></u>',
                $row['status'],
                $row['template_name'],
                $row['start_date'],
                $row['end_date'],
                $row['universe'],
                $row['complete'],
                $view_button
               .$edit_button
               .$delete_button 
               .$view_script
               .$edit_script
               .$report_module
                
            );
        }

            $this->response = array(
                'data'=>$out_po,
                'success' => true,
                'message' => "PO has been updated.",
            ); 
            
            
        }  
    }    
    protected function delete_po(){
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_po($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
           
                $po = $this->backend->get_po();
                $response = $this->backend->get_response();
                $po = $response['ResponseMessage'];
                $out_po = array(); 
                //User type for access rights    
                /*$user_type = $this->backend->get_usertype($_SESSION['username']);*/ 
                 $user_type   = $_SESSION['user_type_desc'];
           foreach ($po as $row) {
               
                if(in_array($user_type,array('Centers','Clients','Data','Admin','System Admin','Managers'))){
                $view_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="EditPO('.$row['id'].',true,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/view.png" title="View PO" />';
            }else{
                $view_button="";
            }
            if(in_array($user_type,array('Clients','Data','Admin','System Admin','Managers'))){
                $edit_button='<input type="image" onclick="EditPO('.$row['id'].',false,'.$_SESSION['user_type'].')" src="'.WEBROOT.'/public/images/edit.png" title="Edit PO" />';
            }else{
                $edit_button="";
            }
               
               if(in_array($user_type,array('Data','Admin','System Admin'))){
                    $delete_button='<input type="image" style="padding-right:1px;height: 25px;width: 25px" onclick="DeletePO('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
                }else{
                    $delete_button="";
                }
               
             if($this->backend->check_if_has_script($row['id']) && in_array($user_type,array('Data','Managers','Clients','Centers','Admin','System Admin'))){
                $view_script=' <a href='.WEBROOT.'/pomaintenance/view_script/'. $row['id'].'>
                 <img style="height: 25px;width: 25px" src="'.WEBROOT.'/public/images/script.png" alt="View Script" /></a>';  ;   
            }else{
                $view_script='';   
            }  
            if($row['status']!='ACTIVE' && in_array($user_type,array('Data','Managers','Clients','Admin','System Admin'))){
              $edit_script  ='<a href='.WEBROOT.'/pomaintenance/po_script/'. $row['id'].'>  <img style="height: 30px;width: 30px" src="'.WEBROOT.'/public/images/edit_script.png" alt="Edit Script" /></a>';  
            }else{
                $edit_script  ='';
            }
            
            //Report
                $report_module = "";
                if( $row['allow_report']==1 && $this->backend->check_data_if_result_exist($row['id'])>0 || $this->backend->check_data_if_manual_data_exist($row['id'])>0  && in_array($user_type,array('Data','Managers','Centers','Clients','Admin','System Admin'))){
                    $report_module=' <a href="'.WEBROOT.'/pomaintenance/report?po_id='. $row['id'].'">'."<img src='".WEBROOT."/public/images/generate.png' alt='View Script' style='height: 25px;width: 25px;' />".'</a>';
                }else{
                    $report_module="";
                }
            //
            
            
            $out_po[] = array(
                $row['po_no'],
                $row['client_name'],
                '<u><a  href='.WEBROOT.'/pomaintenance/project_glance/'. $row['id'].'>'.$row['po_name'].'</a></u>',
                $row['status'],
                $row['template_name'],
                $row['start_date'],
                 $row['end_date'],
                 $row['universe'],
                 $row['complete'],
                 $view_button
                .$edit_button
               .$delete_button 
               .$view_script
               .$edit_script
               .$report_module
               
            );
        }
                    $this->response = array(
                        'data'=>$out_po,
                        'success' => true,
                        'message' => "PO has been deleted.",
                    );
            
        }  
    }
    public function project_glance($get="") {
        $actions = array('project_glance');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
       
         $param=array(
                'id' => $get[0],
            ); 
          
            $sql="select p.name as po_name,c.name as client_name,pp.number_of_question,p.create_by,pt.name as template_name,";
            $sql .=" DATE_FORMAT(pp.start_date,'%Y-%m-%d')as start_date,DATE_FORMAT(pp.end_date,'%Y-%m-%d') as end_date,ps.name as po_status from po p";
            $sql .=" left join po_parameter pp on(p.id = pp.po_id)";
            $sql .=" left join project_status ps on(p.status = ps.code)";
            $sql .=" left join project_template pt on(pt.id=p.project_template_id)";
            $sql .=" left join client c on(c.id = p.client_id)";
            $sql .=" where p.id =". $param['id'];
            $result = $this->db->fetchRow($sql);
            
            //logs
              $projecttemplate_list = $this->backend->get_project_template();
              $log_list=$this->backend->get_action_logs("PO",$param['id']);
              $response = $this->backend->get_response();
              $log_list = $response['ResponseMessage'];
               $params=array(
                'po_id' => $get[0],
            );
             $co_list = $this->backend->get_change_order_list($params);
        
            $response = $this->backend->get_response();
            $co_list = $response['ResponseMessage'];
            $out_co_list = array(); 
            
            foreach ($co_list as $row) {
                
            $out_co_list[] = array(
                'id' => '<u><a  href='.WEBROOT.'/pomaintenance/co_response/'.$get[0].'/'. $row['id'].'>'.'CO'.sprintf('%06d',$row['id']).'</a></u>',
                'create_date' =>  $row['create_date'],
                'change_type' =>  $row['change_type'],
                'description' =>$row['description'],
                'assigned' =>$row['assigned'],
                'status' =>$row['status'],
            );
            }
       
        $this->view->assign('out_co_list',$out_co_list);
            
            //User type for access rights  
            /*$user_type = $this->backend->get_usertype($_SESSION['username']);
            $this->view->assign('user_type', $user_type); */
             $user_type   = $_SESSION['user_type_desc'];
            $this->view->assign('user_type', $user_type);
            $this->view->assign('number_of_question', $result['number_of_question']); 
            $this->view->assign('po_name', $result['po_name']); 
            $this->view->assign('client_name', $result['client_name']); 
            $this->view->assign('template_name', $result['template_name']); 
            $this->view->assign('start_date', $result['start_date']); 
            $this->view->assign('end_date', $result['end_date']); 
            $this->view->assign('po_status', $result['po_status']); 
            $this->view->assign('manager', $_SESSION['username']); 
            $this->view->assign('log_list',$log_list);
            $formated_po_id=$this->backend->get_po_number($get[0]);
            $this->view->assign('formated_po_id', $formated_po_id); 
            
         if($this->backend->check_if_has_po_transaction_data($param))  {
             $this->view->assign('has_po',TRUE );         
         }else{
             $this->view->assign('has_po',FALSE );
         }
         $this->view->assign('po_id',$param['id'] );    
    }
    protected function edit_projecttemplate(){
        $result = $this->backend->edit_projecttemplate($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $projecttemplate_list = $this->backend->get_project_template();
        $response = $this->backend->get_response();
        $projecttemplate_list = $response['ResponseMessage'];
        $out_projecttemplate_list = array(); 
         //User type for access rights  
         /*$user_type = $this->backend->get_usertype($_SESSION['username']);*/
          $user_type   = $_SESSION['user_type_desc'];
        foreach ($projecttemplate_list as $row) {
            
             //View  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $view_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />';
            }else{
                $view_button="";
            }
            //Edit  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $edit_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',false)" src="'.WEBROOT.'/public/images/edit.png" title="edit" />';
            }else{
                $edit_button="";
            }
           //Delete 
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" onclick="DeleteProjectTemplate('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
            
            $out_projecttemplate_list[] = array(
                sprintf('%06d',$row['id']),
                $row['template_name'],
                $row['client_name'],
                $row['report_type'],
                $row['status'],
                $row['is_auto_call'],
                $view_button . $edit_button . $delete_button,
            );
        }

            $this->response = array(
                'data'=>$out_projecttemplate_list,
                'success' => true,
                'message' => "Project Template has been updated.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Registered new project template name ' .$_POST['txtTemplateName'] ,'Project Template',$datetime,$_SESSION['username'],'');
            
        }  
    }
    protected function delete_projecttemplate(){
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_projecttemplate($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {
           
            $projecttemplate_list = $this->backend->get_project_template();
            $response = $this->backend->get_response();
            $projecttemplate_list = $response['ResponseMessage'];
            $out_projecttemplate_list = array(); 
             //User type for access rights  
            /*$user_type = $this->backend->get_usertype($_SESSION['username']);*/
             $user_type   = $_SESSION['user_type_desc'];
            foreach ($projecttemplate_list as $row) {
                
                  //View  
            if(in_array($user_type,array('Data','Clients','Admin','System Admin'))){
                $view_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />';
            }else{
                $view_button="";
            }
            //Edit  
            if(in_array($user_type,array('Data','Clients','Admin','System Admin'))){
                $edit_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',false)" src="'.WEBROOT.'/public/images/edit.png" title="edit" />';
            }else{
                $edit_button="";
            }
           //Delete 
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" onclick="DeleteProjectTemplate('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
                
                $out_projecttemplate_list[] = array(
                    sprintf('%06d',$row['id']),
                    $row['template_name'],
                    $row['client_name'],
                    $row['report_type'],
                    $row['status'],
                    $row['is_auto_call'],
                    $view_button . $edit_button . $delete_button,
                );
            }

            $this->response = array(
                'data'=>$out_projecttemplate_list,
                'success' => true,
                'message' => "Project Template has been deleted.",
            );
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Project Template ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'Project Template',$datetime,$_SESSION['username'],'');
 
        }  
    }
    protected function get_project_template_byid(){
    $id= $_GET['id'];
        $params = array(
        'id' => $id,
        );
        $result = $this->backend->get_project_template_byid($params);
        $response = $this->backend->get_response();
        $result = $this->backend->get_project_template_code_byid($params);
        $template_code = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
            
            foreach ($response['ResponseMessage'] as $key => $val) {
                
                $search_result[] = array(
                    'formated_id'=>sprintf('%06d',$val['id']),
                    'name' =>  $val['name'],
                    'client_id' =>  $val['client_id'],
                    'report_type_id' =>  $val['report_type_id'],
                    
            );
            
             
            }
             $code_temp = array();
             $cnt = 1;
             $row = 0;
             $disabled = "";
             
             if($_GET['isView']==1){
                 $disabled = "disabled=disabled"; 
             }else{
                 $disabled="";
             }
                foreach ($template_code['ResponseMessage'] as $key => $val) { 
                    $selected1='';
                    $selected2='';
                    $selected3='';
                    $selected4='';
                    $selected5='';
                    $selected6='';
                    if($val['type']==1){
                       $selected1='selected="selected"';  
                    }else if($val['type']==2){
                         $selected2='selected="selected"';  
                    }else if($val['type']==3){
                        $selected3='selected="selected"';   
                    }else if($val['type']==4){
                        $selected4='selected="selected"';   
                    }else if($val['type']==5){
                        $selected5='selected="selected"';   
                    }else if($val['type']==6){
                        $selected6='selected="selected"';   
                    }
                   
                    $code_temp[] = array(
                       // $cnt ,    
                        '<input '.$disabled.' type="text" style="text-transform:uppercase" name="txtStatus[]" value="'.$val['status_code'].'" id="txtStatus[]" size="4" maxlength="20" />',
                        '<select '.$disabled.' name="entity_type[]"  id="entity_type[]">
                        <option '.$selected1.' value="1"><---Select Type---></option>
                        <option '.$selected2.' value="2">Complete</option>
                        <option '.$selected3.' value="3">Bad Number</option>
                        <option '.$selected4.' value="4">Callable</option>      
                        <option '.$selected5.' value="5">Terminal</option>
                        <option '.$selected6.' value="6">Leave Message(Complete)</option></select>',
                        '<input '.$disabled.' type="text" name="txtPayRate[]" value="'.$val['pay_rate'].'" id="txtPayRate[]" size="6" maxlength="20" />', 
                        '<a  href="javascript:void(0)" onclick="delete_code('.$row.',false);return false;">
                        <input '.$disabled.' type="image" onclick="" src="'.WEBROOT.'/public/images/deleted.png" title="delete" /> </a>'
                );
                $cnt++;
                $row++;
            } 
            
            
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
                'template_code'         => $code_temp,
            );
        }          
   }
    protected function add_projecttemplate(){
      $result = $this->backend->add_projecttemplate($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $projecttemplate_list = $this->backend->get_project_template();
        $response = $this->backend->get_response();
        $projecttemplate_list = $response['ResponseMessage'];
        $out_projecttemplate_list = array(); 
         //User type for access rights  
         /*$user_type = $this->backend->get_usertype($_SESSION['username']);*/
          $user_type   = $_SESSION['user_type_desc'];
        foreach ($projecttemplate_list as $row) {
            
            //View  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $view_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />';
            }else{
                $view_button="";
            }
            //Edit  
            if(in_array($user_type,array('Clients','Data','Admin','System Admin'))){
                $edit_button='<input type="image" onclick="EditProjectTemplate('.$row['id'].',false)" src="'.WEBROOT.'/public/images/edit.png" title="edit" />';
            }else{
                $edit_button="";
            }
           //Delete 
            if(in_array($user_type,array('Data','Admin','System Admin'))){
                $delete_button='<input type="image" onclick="DeleteProjectTemplate('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />';
            }else{
                $delete_button="";
            }
            
            $out_projecttemplate_list[] = array(
                sprintf('%06d',$row['id']),
                $row['template_name'],
                $row['client_name'],
                $row['report_type'],
                $row['status'],
                $row['is_auto_call'],
                $view_button . $edit_button . $delete_button,
            );
        }

            $this->response = array(
                'data'=>$out_projecttemplate_list,
                'success' => true,
                'message' => "New Project Template has been added.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Registered new project template name ' .$_POST['txtTemplateName'] ,'Project Template',$datetime,$_SESSION['username'],'');
            
        }  
    }
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    function phpRedirect($url) {
        echo 'window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
