<?php
class billing extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function billing_report()
    {
        $billing = array();
        $summary_amount= 0;
       
        if(isset($_GET['start_date']))
        {
          $start_date = $_GET['start_date'];
          $end_date = $_GET['end_date'];
          $client_id = $_GET['client_id'];
          
          
          //get_question_results_amount
          //For Bill Amount Per Question
          if(isset($_GET['is_check']))
          {
              
              $questions = array();
              $results_amount = array();
              
              $po_no = $_GET['po_number'];
              $po_id = $this->backend->get_poid($po_no);
               
              if($po_id > 0)
              {
                   $status_codes = $this->backend->get_status_codes($po_id);
                   $questions = $this->backend->get_script_question_response($po_id);
                   
                   $is_gotv = 1;
                   if(count($questions) > 0)
                   {
                       $is_gotv = 0;
                   }
                   
                   $results_amount = $this->backend->get_question_results_amount($po_id,$status_codes,$questions,$start_date, $end_date, "");
                              
              }
              

              
              $this->view->assign('is_gotv',$is_gotv); //1 for true 0 for false
              $this->view->assign('status_codes', $status_codes);
              

              
              $this->view->assign('questions', $questions);
              $this->view->assign('questions_amount', $results_amount);
              $this->view->assign('is_detail', 1);
              $this->view->assign('po_no', $po_no);
              
              if(isset($_GET['is_export']))
              {
                    $rowPosition=1;
                    require_once "PHPExcel.php"; 
                        //Header
                    $excel = new PHPExcel(); 
                    $excel->setActiveSheetIndex(0);
                    $excel->getActiveSheet()->setTitle('BillingReportPO#'.$po_no);
                    
                    $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit("PO# {$po_no}", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    
                    $rowPosition++;
                    
                    $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit("From# {$start_date} to {$end_date}", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    
                    $rowPosition++;
                    
//                    $excel->getActiveSheet()->getCell('A'.$rowPosition)->setValueExplicit("Questions", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getCell('A'.$rowPosition)->setValueExplicit("Description", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(80);
                    
                    
                    
                    
                    
                   // $excel->getActiveSheet()->getCell("B".$rowPosition) 
//                    ->setValueExplicit("Amount", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getCell("B".$rowPosition) 
                    ->setValueExplicit("Quantity", PHPExcel_Cell_DataType::TYPE_STRING);

                    $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);  
                    
                    $excel->getActiveSheet()->getCell("C".$rowPosition) 
                    ->setValueExplicit("Rate", PHPExcel_Cell_DataType::TYPE_STRING);

                    $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);  
                    
                    $excel->getActiveSheet()->getCell("D".$rowPosition) 
                    ->setValueExplicit("Amount", PHPExcel_Cell_DataType::TYPE_STRING);

                    $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);  
                    
                    
                    $total_amount =0;
                    
                    if($is_gotv == 0)
                    {
                        foreach($questions  as $q)
                        {
                            
                            $rowPosition++;
                            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                            ->setValueExplicit(rtrim(ltrim($q['question'])), PHPExcel_Cell_DataType::TYPE_STRING);             
                            $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true); 
                            
                            $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                            ->setValueExplicit($results_amount['Q'.$q['question_no']], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                            $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0');
                            $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setBold(true); 
                            
                            $total_amount = $total_amount + $results_amount['Q'.$q['question_no']];
                            
                            foreach($q['responses']  as $r)
                            {
                                $rowPosition ++;
                                $r_no = $r['row_no'];
                                
                                $rate = $r['rate'];
                                $result = "Q{$q['question_no']}$r_no";
                                $qtyresult = "QQty{$q['question_no']}$r_no";
                                
                                $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                                ->setValueExplicit(rtrim(ltrim($r['response'])), PHPExcel_Cell_DataType::TYPE_STRING);   
                                
                                $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                                ->setValueExplicit($results_amount[$qtyresult], PHPExcel_Cell_DataType::TYPE_STRING); 
                                $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0');                              
                                                            
                                $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                                ->setValueExplicit($rate, PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                                $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');
                                $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                                ->setValueExplicit($results_amount[$result], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                                $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');
                                
                                
    //                            $excel->getActiveSheet()->getCell('B'.$rowPosition) 
    //                            ->setValueExplicit($results_amount[$result], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
    //                            $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');
                            }
                            
                        }
                    }else{
                        foreach($status_codes  as $q)
                        {
                            $status = $q['status_code'];
                            $status_type = $q['status_type'];
                            $rate = $q['pay_rate'];
                            $rowPosition++;
                            $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                            ->setValueExplicit($status." - ".$status_type, PHPExcel_Cell_DataType::TYPE_STRING);             
                            $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true); 
                            
                            $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                            ->setValueExplicit($results_amount['QQty'.$status], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                            $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0');
                            
                            $total_amount = $total_amount + $results_amount['Q'.$status];
                                                            
                            $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                            ->setValueExplicit($rate, PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                            $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');
                            $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                            ->setValueExplicit($results_amount['Q'.$status], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                            $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');                           
                        }
                        
                    }
                    $rowPosition++;
                    $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                    //->setValueExplicit($results_amount['TotalAmount'], PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                    ->setValueExplicit($total_amount, PHPExcel_Cell_DataType::TYPE_NUMERIC);   
                    $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');
                    $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setBold(true); 
                    
                    $date=date('Y-m-d H:i:s');
                    $filename="BillingDetail{$po_no}.xls"; //save our workbook as this file name
                    header('Content-Type: application/vnd.ms-excel'); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header('Cache-Control: max-age=0'); //no cache
                                     
                    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                    //if you want to save it as .XLSX Excel 2007 format
                    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
                    //force user to download the Excel file without writing it to server's HD
                    $objWriter->save('php://output');  
                    die();
                    
                    
              }
              
              
          }
          else{
              
                
              $billing = $this->backend->get_billing_report_summary($start_date, $end_date, $client_id);
              
           
              $summary_amount = $billing['summary_amount'];
             
               if(isset($billing['records']))
               {
                    $billing = $billing['records'];   
               }
              
              
              
                if(isset($_GET['is_export']))
                {
                    $rowPosition=1;
                    require_once "PHPExcel.php"; 
                        //Header
                    $excel = new PHPExcel(); 
                    $excel->setActiveSheetIndex(0);
                    $excel->getActiveSheet()->setTitle('Billing Report');
                    
          
                    $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                    ->setValueExplicit("PO#", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);

                    $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                    ->setValueExplicit("PO Name", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    
                    $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                    ->setValueExplicit("Client Name", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

                    $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                    ->setValueExplicit("PO Start Date", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

                    $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                    ->setValueExplicit("PO End Date", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('E'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

                    $excel->getActiveSheet()->getCell('F'.$rowPosition) 
                    ->setValueExplicit("Total Amount", PHPExcel_Cell_DataType::TYPE_STRING);
                    $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getFont()->setBold(true);                    
                    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);            
                    
                    
                    
                    
                    foreach($billing as $b)
                    {
                        $rowPosition++;
                        $excel->getActiveSheet()->getCell('A'.$rowPosition) 
                        ->setValueExplicit($b['po_no'], PHPExcel_Cell_DataType::TYPE_STRING);             
                        
                        
                        $excel->getActiveSheet()->getCell('B'.$rowPosition) 
                        ->setValueExplicit($b['name'], PHPExcel_Cell_DataType::TYPE_STRING);             
                        
                        $excel->getActiveSheet()->getCell('C'.$rowPosition) 
                        ->setValueExplicit($b['client_name'], PHPExcel_Cell_DataType::TYPE_STRING);             

                        $excel->getActiveSheet()->getCell('D'.$rowPosition) 
                        ->setValueExplicit($b['po_start_date'], PHPExcel_Cell_DataType::TYPE_STRING);             
                        
                        $excel->getActiveSheet()->getCell('E'.$rowPosition) 
                        ->setValueExplicit($b['po_end_date'], PHPExcel_Cell_DataType::TYPE_STRING);             

                        $excel->getActiveSheet()->getCell('F'.$rowPosition) 
                        ->setValueExplicit($b['total_amount'], PHPExcel_Cell_DataType::TYPE_NUMERIC);    
                        $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getNumberFormat()->setFormatCode('#,##0.00');         
                    }
                    
                    
                    $date=date('Y-m-d H:i:s');
                    $filename="BillingReport_{$date}.xls"; //save our workbook as this file name
                    header('Content-Type: application/vnd.ms-excel'); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header('Cache-Control: max-age=0'); //no cache
                                     
                    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                    //if you want to save it as .XLSX Excel 2007 format
                    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
                    //force user to download the Excel file without writing it to server's HD
                    $objWriter->save('php://output');  
                    die();
                    
                }
              
          }
          
/*          $status_codes = $this->backend->get_status_codes(9);
          $questions = $this->backend->get_script_question_response(9);
          
          print_r($this->backend->get_question_results_amount(9,$status_codes,$questions,$start_date, $end_date, ""));
          die();
*/          
          //End for Bill Amount Per Question
          
          
          
          
          
          

          

        }else{
          $start_date = "";
          $end_date = "";
          $client_id = "";
        }
        

        
        
        $this->view->assign('summary_amount', $summary_amount);
        
        $this->view->assign('billing', $billing);
        $this->view->assign('start_date', $start_date);
        $this->view->assign('end_date', $end_date);
        $this->view->assign('client_id', $client_id);
        
        $this->backend->get_client();
        $client_list = $this->backend->get_response();
        
        $this->view->assign('client_list', $client_list['ResponseMessage']);
        

//        $rs = $this->backend->get_billing_report_summary('2016-01-01','2016-02-29', 9);
//        print_r($rs);
//        die();
    }
    
    
    
     protected function get_item_for_billing()
     {
            $params = array(
                'center_id'=> $_POST['txtCenter_id'],
                'start_date'=>$_POST['startDateBilling'],
                'end_date'=>$_POST['endDateBilling'],
            ); 
            $result = $this->backend->get_item_for_billing($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            } else {
                   
            } 
     }
     protected function paid_billing()
    {
           $id= $_GET['id'];
           $params = array(
                'id' => $id,
            );
            $result = $this->backend->paid_billing($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $billing_list = $this->backend->get_billing_list();
                    $response = $this->backend->get_response();
                    $billing_list = $response['ResponseMessage'];
                    $out_billing_list = array(); 
                    foreach ($billing_list as $row) {
                        $print='';
                        $mark_as_paid='';
                       
                            $print=' <a target="_blank" href="'.WEBROOT.'/billing/transactions?action=print_billing&id='.sha1(md5($this->backend->salt.$row['id'])).'">
                                    <input type="image" style="margin-buttom:10px;padding-right:1px;height: 30px;width: 30px"  src="'.WEBROOT.'/images/printer.png" title="Generate Items" />
                                    </a>';
                      
                        if($row['status']=='SAVED'){
                             $mark_as_paid='<input type="image" src="'.WEBROOT.'/public/images/payment.png" onclick="PaidBilling('.$row['id'].');" style="height: 30px;width: 30px" title="Mark as Paid" />';
                        }
                        $button= '<a href="transactions/view/'.sha1(md5($this->backend->salt.$row['id'])).'">
                                 <input type="image" src="'.WEBROOT.'/public/images/view.png" title="view" /> </a>
                                 <a href="transactions/edit/'.sha1(md5($this->backend->salt.$row['id'])).'">
                                 <input type="image" src="'.WEBROOT.'/public/images/edit.png" title="edit" /> </a>
                                '.$mark_as_paid.$print;
                        
                        
                        $out_billing_list[] = array(
                            $row['invoice_no'],
                            $row['client_name'],
                            $row['billing_date'],
                            $row['due_date'],
                            $row['create_by'],
                            '$'. number_format($row['amount_due'],2),
                            $row['status'],                         
                           $button
                        );
                    }
                      
                $this->response = array(
                    'data'=>$out_billing_list,
                    'success' => true,
                    'message' => "Billing has been paid.",
                ); 
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Billing ID# '.$params['id']. ' has been paid.' ,'Billing',$datetime,$_SESSION['username'],'');
 
                    
            }
        
        
    }
    
    protected function print_billing()
    {
        
        $param=array('id' => $_GET['id'],);                         
        $result = $this->backend->get_billing_details($param);        
        $response = $this->backend->get_response();     
                   
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) { 
            $this->response['message'] = 'System error, unable to connect to database'; 
        }elseif (!($response['ResponseCode'] == '0000')){ 
            $this->response['message'] = $response['ResponseMessage']; 
        }else{ 
            $invoice_no="";
            $due_date="";
            $billing_date="";
            $client_name = "";
            $amount_due = "";
            $details = array();
              foreach ($response['ResponseMessage'] as $key => $val) {
                       $invoice_no =$val['invoice_no'];
                       $due_date = $val['due_date'];
                       $billing_date = $val['billing_date'];
                       $client_name = $val['client_name'];
                       $amount_due = number_format($val['total_amount'],2);    
                       $details[] = array(
                                    'line_number'=>$val['line_number'],
                                    'description'=>$val['description'],
                                    'quantity'=>$val['quantity'],
                                    'rate'=>$val['rate'],
                                    'amount'=>number_format($val['amount'],2),
                       );
              } 
            
                 
            
                require_once('tcpdf/tcpdf.php');
                // create new PDF document
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('');
                $pdf->SetTitle($invoice_no);
                $pdf->SetSubject('');
                $pdf->SetKeywords('');
                $PDF_HEADER_TITLE = "";
                $PDF_HEADER_STRING ="";
                $pdf->SetAutoPageBreak(false, 0);
                //set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                //set auto page breaks
                //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
                //$pdf->SetAutoPageBreak(true, 0);
                // ---------------------------------------------------------
                // set font
              //  $pdf->SetFont('Courier', 'B', 9);
              
                $pdf->SetFont('times', 'B', 9);  
                // add a page
                $pdf->AddPage();
                $image_path=WEBROOT . "/public/images/templates/billing.png";
                

                $datetime=$this->backend->get_server_date();                
                $y = $pdf->GetY($image_path);
                $rby = $pdf->getImageRBY($image_path);
                $pdf->Image($image_path,1,20,200,$rby-$y);
                //Details
                 $pdf->Text(148,41,$billing_date);  //Billing Date
                 $pdf->Text(172,41,$invoice_no);  //Invoice
                 $pdf->Text(172,52,$due_date);  //Due Date
                 $pdf->Text(25,72,$client_name);  //Client Name
                 $pdf->Text(148,102,'$ '.$amount_due);  //Amount Due
                 $row = 120;
                 foreach ($details as $key => $details) {
                      $pdf->Text(10,$row,'* '.$details['description']);
                      $pdf->Text(130,$row,$details['quantity']);
                      $pdf->Text(150,$row,$details['rate']);
                      $pdf->Text(170, $row,$details['amount']);
                      $row = $row+4;   
                 }
                $pdf->Text(170, 222,'$ '.$amount_due);  
                $pdf->lastPage();
                $pdf->Output('sample.pdf', 'I');
                die();
            
        }
    }
    protected function save_billing()
    {
       
            $result = $this->backend->save_billing($_POST);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            } else {
                      $this->response = array(
                        'id'  =>   $response['doc_id'],
                        'invoice' => $response['invoice'],
                        'success' => true,               
                        'message' =>  $response['ResponseMessage'],
                    );
                      
            }
        
        
    }
    
    protected function reload_param()
    {
       
        $params = $_POST;     
        
        $row = explode(";",$params['txtRowValues']);
        
        foreach($row as $res_row) 
        { $temp[] = explode("|",$res_row); }
            
        foreach($temp as $val) 
        {            
            $fields[] = array(
                $val[0] + 1,
                $val[1],
                $val[2],
                $val[3],
                $val[4],
                '<a href="javascript:void(0)"; onclick="return false;">
                <input type="image" align="right" onclick="delete_item('.$val[0].')" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>',);
        }  
       
        $this->response = array(
            'success'   => true,
            'data'      => $fields,
        );
                  
   }
    
    public function management() {
        $actions = array('paid_billing');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
           
        $billing_list = $this->backend->get_billing_list();
        $response = $this->backend->get_response();
        $billing_list = $response['ResponseMessage'];
        $out_billing_list = array(); 
        foreach ($billing_list as $row) {
            $print='';
            $mark_as_paid='';
           
           $print=' <a target="_blank" href="'.WEBROOT.'/billing/transactions?action=print_billing&id='.$row['id'].'">
                    <img  style="margin-buttom:10px;padding-right:1px;height: 30px;width: 30px" src="'.WEBROOT.'/images/printer.png" alt="Generate Items" />  
                    </a>';
              if($row['status']=='SAVED'){
                    $mark_as_paid='<input type="image" src="'.WEBROOT.'/public/images/payment.png" onclick="PaidBilling('.$row['id'].');" style="height: 30px;width: 30px" title="Mark as Paid" />';
              }
            $button= '<a href="transactions/view/'.$row['id'].'">
                    <img src="'.WEBROOT.'/public/images/view.png" alt="view" /></a>
                     <a href="transactions/edit/'.$row['id'].'">
                     <img src="'.WEBROOT.'/public/images/edit.png" alt="edit" /> </a>'
                     .$mark_as_paid.$print;
            
            $out_billing_list[] = array(
                'invoice_no'=> $row['invoice_no'],
                'client_name'=>$row['client_name'],
                'billing_date'=>$row['billing_date'],
                'due_date'=>$row['due_date'],
                'create_by'=> $row['create_by'],
                'amount_due'=>'$ '. number_format($row['amount_due'],2),
                'status'=>$row['status'],
                'button'=>$button,
            );
        }
        $this->view->assign('billing_list', $out_billing_list);
 
    }
    
    public function transactions($get="") {
        $actions = array('reload_param','save_billing','print_billing','get_item_for_billing');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        if (isset($get[0])){            
            $this->view->assign('isView', $get[0]);        
        }
        $out_detail_list = array();
         if (isset($get[1])){  //If Edit or View
            
            $param=array('id' => $get[1],);                        
            $result = $this->backend->get_billing_details($param);        
            $response = $this->backend->get_response();     
                 
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) { 
                $this->response['message'] = 'System error, unable to connect to database'; 
            }elseif (!($response['ResponseCode'] == '0000')){ 
                $this->response['message'] = $response['ResponseMessage']; 
            }else{ 

                foreach ($response['ResponseMessage'] as $key => $val) {
                    $this->view->assign('invoice_no', $val['invoice_no']);
                    $this->view->assign('billing_date', $val['billing_date']);
                    $this->view->assign('due_date', $val['due_date']);
                    $this->view->assign('client_id', $val['client_id']);
                    $this->view->assign('status', $val['status']);
                    $this->view->assign('remarks', $val['remarks']);
                    $this->view->assign('total_amount', number_format($val['total_amount'],2));
                    if($get[0]=='edit'){
                        $button = '<a href="javascript:void(0)"; onclick="return false;">
                                   <input type="image" align="right" onclick="delete_item('.$val['line_number'].')" src="'.WEBROOT.'/public/images/deleted.png" title="cancel" /></a>';
                    }else{
                        $button='';
                    }
                     $out_detail_list[] = array(                             
                                'line_number'          =>$val['line_number'],
                                'description'          =>$val['description'],
                                'quantity'             =>$val['quantity'],
                                'rate'                 =>number_format($val['rate'],2),
                                'amount'               =>number_format($val['amount'],2),
                                'button'               =>  $button,);
                    
                }
                  $this->view->assign('detail_list', $out_detail_list); 
                  $this->view->assign('id', sha1(md5($this->backend->salt.$param['id']))); 
                  $this->view->assign('id_encrypted',  $param['id']); 
            }
         }else{   //If New
             $this->view->assign('status', 'NEW');
             $this->view->assign('detail_list', $out_detail_list);
             $this->view->assign('total_amount', '0.00'); 
             $this->view->assign('id',"");
         }
        $client_list = $this->backend->get_client_list();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $this->view->assign('client_list', $client_list);
       
        
        
      
        
    }
    
    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
