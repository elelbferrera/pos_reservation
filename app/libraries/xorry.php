<?php
class xorry {
    public static function encode($string, $key) {
        $rand = '';
        while (strlen($rand) < 32) $rand .= mt_rand(0, mt_getrandmax());
        $rand = sha1($rand);
        $enc = '';
        for ($i = 0; $i < strlen($string); $i++) $enc .= substr($rand, ($i % strlen($rand)), 1).(substr($rand, ($i % strlen($rand)), 1) ^ substr($string, $i, 1));
        return base64_encode(self::_xor_merge($enc, $key));
    }

    public static function decode($string, $key) {
        $string = self::_xor_merge(base64_decode($string), $key);
        $dec = '';
        for ($i = 0; $i < strlen($string); $i++) $dec .= (substr($string, $i++, 1) ^ substr($string, $i, 1));
        return $dec;
    }

    private static function _xor_merge($string, $key) {
        $hash = sha1($key);
        $str = '';
        for ($i = 0; $i < strlen($string); $i++) $str .= substr($string, $i, 1) ^ substr($hash, ($i % strlen($hash)), 1);
        return $str;
    }
}