<?php
abstract class template{
    public $meta;
    public $view;
    public $layout = true;
    protected $db;
    protected $response = array();

    public function __construct($meta) {
        $this->response = array(
            'message'   => 'Unknown error',
            'success'   => false,
        );
        if (is_array($meta)) {
            $this->meta = $meta;
        }
        
        //LC-09/11/2013
        //require_once 'backend.php';
        
    
        //load and initialize libraries
        require_once('smarty/Smarty.class.php');
        $smarty = new Smarty();
        $smarty->template_dir = DOCROOT . '/views';
        $smarty->compile_dir  = DOCROOT . '/views/templates_c/';
        $smarty->cache_dir    = DOCROOT . '/views/cache/';
        $this->view = $smarty;
        $this->view->assign('webroot', WEBROOT);
        $this->view->assign('webroot_resources', WEBROOT . '/public');
        $this->view->assign('meta', $meta);
		
		
        require_once('library.php');
		if (isset($meta['db_credentials']) && is_array($meta['db_credentials']) && count($meta['db_credentials']) > 0) {
			require_once('db.php');
			$this->db = db::getInstance($meta['db_credentials']);
		}
        
		
        require_once('backend_'.PROGRAM.'.php');
        $this->backend = new backend($this->db);

		if(isset($_SESSION['sessionid']))
		{
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $analytics = lib::getWsResponse(API_URL, 'get_total_analytics', $params);
			
			$analytics = $analytics['respmsg'];
			// print_r($analytics);
			// die();

            $param_time = array(
                'merchant_id' => $_SESSION['merchant_id'],
            );
            /*$timezone = lib::getWsResponse(API_URL, 'get_time_zone', $param_time);
            
            date_default_timezone_set($timezone['respmsg']['timezone']);*/

            if ($_SESSION['user_type'] != 1){
                $timezone = lib::getWsResponse(API_URL, 'get_time_zone', $param_time);
                date_default_timezone_set($timezone['respmsg']['timezone']);
            }

            $this->view->assign('current_time', date("h:i a"));
            $this->view->assign('current_date', date("Y-m-d"));
            // print_r(date("h:i:s a"));
            // die();
			
			$this->view->assign('analytics', $analytics);

            $response = lib::getWsResponse(API_URL, 'get_merchant_profile', $params);
            $response = $response['respmsg'];
            $profile = $response;
            $this->view->assign('profile', $profile);

		}

                
        // lib::setTimeZone($this->meta['timezone']);
        $this->_preFilter();
        $this->_postFilter();

        if (isset($_SESSION['user_type'])){
            $this->view->assign("userType", $_SESSION['user_type']);
        }
    }
    
    public function check_session() {
    	
        if (!isset($_SESSION['sessionid'])) {
            unset($_SESSION);
            session_destroy();
            $path = '';
            if (isset($this->meta['class'])) $path = $this->meta['class']; 
            if (isset($this->meta['action'])) $path .= '/'. $this->meta['action']; 
            
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $this->response = array(
                    'message' => 'Session expired',
                    'success' => false,
                    'redirect'=> 'log/in/' . base64_encode($path),
                );
                header('Content-type: application/json');
                echo json_encode($this->response);
                exit;
            } else {
            	
                return $this->redirect('log/in/' . base64_encode($path), 'Session expired');
            }
        }else{
        	/*$param_time = array(
				'merchant_id' => $_SESSION['merchant_id'],
			);
			$timezone = lib::getWsResponse(API_URL, 'get_time_zone', $param_time);
			
			date_default_timezone_set($timezone['respmsg']['timezone']);*/

            if ($_SESSION['user_type'] != 1){
                $param_time = array(
                    'merchant_id' => $_SESSION['merchant_id'],
                );
                $timezone = lib::getWsResponse(API_URL, 'get_time_zone', $param_time);
                date_default_timezone_set($timezone['respmsg']['timezone']);
            }
        }
    }
     
    public function access_denied() {
    }
    
    public function notfound() {
    }
    
    public function _beforeAction() {

    }

    public function _afterAction() {
    	
// 		
        if ($this->layout === true) {
        	
			
            if (isset($_SESSION['timez']) && is_array($_SESSION['timez'])) {
                foreach ($_SESSION['timez'] as $t) {
                    $timez[] = '<a style="text-decoration:none;color:lightgray;" href="#" title="' . $t . '">.</a>';
                }
                $this->view->assign('timez', implode('', $timez));
                unset($_SESSION['timez']);
            }
			
			
			

            $this->displayMessages($this->view);

            if (file_exists(DOCROOT . '/views/' . $this->meta['class'] . '_' . $this->meta['action'] .'.tpl')) {
                $action_tpl = $this->meta['class'] . '_' . $this->meta['action'] .'.tpl';
				
				
                $this->view->assign('action_tpl', $action_tpl);
                if (isset($this->meta['menu_no_parent_template']) && in_array($this->meta['class'] . '/' . $this->meta['action'], $this->meta['menu_no_parent_template'])) {
                    $this->view->display($action_tpl);
                } elseif (file_exists(DOCROOT . '/views/' . $this->meta['class'] .'.tpl')) {
                    $this->view->display($this->meta['class'] . '.tpl');
                } elseif (file_exists(DOCROOT . '/views/default.tpl')) {
                    //$this->view->display('default.tpl');
                    
                  
                    if($action_tpl =="sign_application_index.tpl" || $action_tpl=="calculator_compute.tpl")
                    {
                        $this->view->display($action_tpl);
                    }else
                    {
                       $this->view->display('default.tpl'); 
                    }
                    
                    
                } else {
                	
                    $this->view->display($action_tpl);
					
                }
            } elseif (file_exists(DOCROOT . '/views/default.tpl')) {
                $this->view->display('default.tpl');
            }
        } elseif ($this->layout == 'json') {
            header('Content-type: application/json');
            echo json_encode($this->response);
            exit;
        }
    }

    public function _preFilter() {

    }

    public function _postFilter() {

    }

    public function index() {
        $this->showError('Empty page');
    }

    public function redirect($path, $alert_message = null) {
        $message = '';
        if ($alert_message != null) {
            $message = 'alert("' . $alert_message . '");';
        }
        $path = trim($path, '/');
        $message .= 'window.location.href="' . WEBROOT .'/'. $path .'";';
        echo '<script>' . $message . '</script>';
        exit;
    }

    public function showInfo($message, $redirect = NULL) {
        $_SESSION['_messages_']['info'][] = $message;
        if ($redirect !== NULL) {
            $this->redirect($redirect);
        }
        return FALSE;
    }

    public function showError($message, $redirect = NULL) {
        $_SESSION['_messages_']['error'][] = $message;
        if ($redirect !== NULL) {
            $this->redirect($redirect);
        }
        return FALSE;
    }

    public function displayMessages(&$view) {
        //priority is the content of info array index
        $buffer = NULL;
        $message = NULL;
        if (isset($_SESSION['_messages_']['info'])) {
            foreach ($_SESSION['_messages_']['info'] as $msg) {
                if (trim($msg) != '') {
                    $buffer .= '<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>' . $msg . '</p>';
                }
            }
            if ($buffer != NULL) {
                $message = '<div style="padding: 0.4em 0.8em;margin: .7em 1em;" class="ui-state-highlight ui-corner-all">' . $buffer . '</div>';
            }
            unset($_SESSION['_messages_']);
        } elseif (isset($_SESSION['_messages_']['error'])) {
            foreach ($_SESSION['_messages_']['error'] as $msg) {
                if (trim($msg) != '') {
                    $buffer .= '<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.3em;"></span>' . $msg . '</p>';
                }
            }
            if ($buffer != NULL) {
                $message = '<div style="padding: 0.4em 0.8em;margin: .7em 1em;" class="ui-state-error ui-corner-all">' . $buffer . '</div>';
            }
            unset($_SESSION['_messages_']);
        }
        $view->assign('_messages_', $message);
    }
}