<?php
class utilities extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'uploads/';
        $this->download_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'downloads/';
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
    
    public function customer_list() {
        $actions = array('search_customer','upload_payment');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $result = $this->backend->get_all_customers();
        $response = $this->backend->get_response();
        $customers = $response['ResponseMessage'];
        $this->view->assign('customers', $customers);
    }
    
    public function payment_list(){
        $actions = array('download_payment_file');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $ftp_server = "ftp.billspayexpress.com";
        $ftp_user_name = "becpayment";
        $ftp_user_pass = "M1O1GQvDI3Ez0Ub"; 
        
             // set up basic connection
        $conn_id = ftp_connect($ftp_server);
        //print_r($conn_id);
        //die();

        // login with username and password
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        // get contents of the current directory
        //$contents = ftp_nlist($conn_id, ".");
        ftp_pasv($conn_id,true);

        // Print a directory listing
        $contents = ftp_nlist($conn_id, ".");
        $i=0;
        foreach ($contents as $key => $val) {
            // get the last modified time
            $check = $this->backend->checkPaymentDownloadLogs("filename='". $contents[$i] ."'");
            if ($check){
                $status = 'Downloaded by BEC';  
            } else {
                $status = 'Available for Download';
            }
            $lastchanged = ftp_mdtm($conn_id, $contents[$i]);
            $button_df = '<button type="submit" class="btn btn-primary" id="dfile" name="dfile" value="'.$contents[$i].'">Download File</button>';  
            $button_ef = '<button type="submit" class="btn btn-danger" id="efile" name="efile" value="'.$contents[$i].'">Email File</button>';  
            $payment[] = array(
                'datetime'  => date("F d Y H:i:s.",$lastchanged),
                'filename'  => $contents[$i],
                'status'  =>  $status,
                'actions'  =>  $button_df . $button_ef,
            );
            $i+=1;
        } 
        $this->view->assign('payments', $payment);    
    }
    
    protected function download_payment_file(){
        if (isset($_POST['efile'])) 
        { 
           //echo $_POST['efile']; 
           //die();
            $realpath = $this->download_path;
            $url =$realpath . $_POST['efile'];
            $params = array(
                'username'  => $_SESSION['username'],
                'return_value'  =>  'email_address',
            );
            $email = $this->backend->get_user_info($params);
            $params = array(
                'email_address' =>  $email,
                'name'          =>  $_SESSION['username'],
                'pathfile'      =>  $url,
                'filename'      =>  $_POST['efile']
                
            );
            $this->backend->payment_email_notification($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->phpAlertWithRedirect('System error, unable to connect to database',WEBROOT.'/utilities/payment_list');
            } else {
                $params = array(
                'filename'      =>  $_POST['efile'],
                'downloaded_by' =>  $_SESSION['username'],
                );
                $this->backend->addPaymentDownloadLogs($params);
                
                $update_params = array(
                    'filename'      =>  $_POST['efile'],
                    'status'        =>  'PROCESSING',
                );
                $this->backend->update_payment_status_by_filename($update_params);
                $this->phpAlertWithRedirect('Email sent!',WEBROOT.'/utilities/payment_list');
            }
            die();
               
        }  
        
        if (isset($_POST['dfile'])) 
        { 
           //echo "button 2 has been pressed"; 
           //die();
            
            $params = array(
                'filename'      =>  $_POST['dfile'],
                'downloaded_by' =>  $_SESSION['username'],
            );
            $this->backend->addPaymentDownloadLogs($params);
            
            $update_params = array(
                'filename'      =>  $_POST['dfile'],
                'status'        =>  'PROCESSING',
            );
            $this->backend->update_payment_status_by_filename($update_params);

            
            
            $initial_filename= $_POST['dfile'];
            $realpath = $this->download_path;
            $url =$realpath . $initial_filename;
        
            $current = file_get_contents($url);
            header('Content-disposition: attachment; filename='. $initial_filename);
            header('Content-type: text/plain');
            echo $current;
            die();  
            
        }  
    }
    
    protected function upload_payment(){
        $params = array(
            'account_no' => $_POST['accountno'],
            'total_amount' => $_POST['amount'],
        );    
        $payment = $this->backend->add_payment($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
    protected function search_customer() {
     
        if (!isset($_POST['account_no'], $_POST['phone_number'], $_POST['customer_name'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['account_no']) !='' || trim($_POST['phone_number']) != '' || trim($_POST['customer_name'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'customer_name' => $_POST['customer_name'],
                'account_no' => $_POST['account_no'],
                'phone_number' => $_POST['phone_number'],
            );
           
            $result = $this->backend->search_customer($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //JR-2014/05/23 Get loan count to check if user is still allowed to access the page
                $user_type_description = $this->backend->get_user_type_by_username_ex($_SESSION['username']);
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $button = '<input type="button" class="btn btn-file" value="Add Payment" id="'.$val['id'].'"  onclick="addPaymentCustomer('."'".$val['account_no']."'".')" />';          
                    $search_result[] = array(
                        $val['account_no'],
                        $val['customer_name'],
                        $val['phone_number'],
                        $val['house_number'],
                        $val['street_name'],
                        $val['apartment_id'],
                        $val['zip_code'],
                        //$button,
                    );
                }
                 
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }
    
    public function upload() {
        $actions = array('upload_file','process_file');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    protected function process_file() {
        $_POST = $this->array_map_recursive('trim', $_POST);   
        $filename = $this->upload_path . $_POST['filename'];
        $handle = @fopen($filename, "r");
        $ctr =0;
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $customer_params = array(
                    'account_no'    => substr($buffer,0,18),
                    'customer_name' => substr($buffer,18,30),
                    'phone_number'  => substr($buffer,48,10),
                    'house_number'  => substr($buffer,58,5),
                    'street_name'   => substr($buffer,63,25),
                    'apartment_id'  => substr($buffer,88,5),
                    'zip_code'      => substr($buffer,93,9),
                    //'blank'         => substr($buffer,99,22),
                );
                $customers = $this->backend->add_customer($customer_params);
                $response = $this->backend->get_response();
                if ($response['ResponseCode']=="0000"){
                    $ctr = $ctr +1;
                }
            }
            if (!feof($handle)) {
                $this->response['message'] = 'Error: unexpected fgets() fail';
                $this->response['success'] = false;                
            }
            fclose($handle);
            $this->response['message'] = '';
            $this->response['success'] = true;
        }
       
        
        
    }
    
    protected function upload_file() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    
                    $this->response['message'] = 'That file type is not valid for this application process. Please use a text file.';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        $this->response['message'] = 'That file type is not valid for this application process. Please use a text file.';
                    } else {
                        $parts = explode('_', $key);
                        $destination = $this->upload_path . $upload['name'];
                        //echo $destination;
                        //die();
                        if (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                            $this->response['success'] = false;
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    public function generate(){
        $actions = array('download_file','test_sftp');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }    
    }
    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    
    protected function test_sftp()
    {
        //print('test');
        //die();
        include('Net/SFTP.php');

        $sftp = new Net_SFTP('23.253.195.156');
        if (!$sftp->login('jeff.ramos', 'zb30MIvNeK')) {
            exit('Login Failed');
        }

        print_r($sftp->nlist()); // == $sftp->nlist('.')
        print_r($sftp->rawlist()); // == $sftp->rawlist('.')
        die();
    }
    
    protected function download_file(){
  
        if (empty($_POST["startDate"])) {
            $this->phpAlertWithRedirect('Date should not be empty.',WEBROOT.'/utilities/generate');
            die();
        }
        
        /*if (empty($_POST["endDate"])) {
            $this->phpAlertWithRedirect('End Date should not be empty.',WEBROOT.'/utilities/generate');
            die();
        }
        
        if ($_POST["startDate"] > $_POST["endDate"]) {
            $this->phpAlertWithRedirect('Start Date should not be greater than End Date.',WEBROOT.'/utilities/generate');
            die();
        }
        */
      
        $params = array (
            'startDate'   => $_POST['startDate'],
            //'endDate'     => $_POST['endDate'],
        );
        
        
        
        //$this->alertMessage('test');
        $initial_filename="BEC_OMNI_";
        $creationdatetime=date('Y-m-d',strtotime($_POST['startDate']));
        $realpath = $this->download_path;
        //$url =$realpath . $creationdatetime . "_PaymentFile.txt";
        $url =$realpath . $initial_filename . $creationdatetime . ".txt";
    
        $customers = $this->backend->getPaymentsForExport($params);
        $response = $this->backend->get_response();
        $search_result = array();
        $ctr=1;
        $content="";                      
                  
        
        $spaces = str_pad("",64);
        $content="HBEC Payments{$creationdatetime}{$spaces}";
        $fp = fopen($url,"w");
        fwrite($fp,"{$content}\r\n");
        fclose($fp);
        
        $mangoNetworkId= str_pad(MANGO_NETWORK_ID,15);
        $mangoConfirmationNo= str_pad(MANGO_CONFIRMATION_NUMBER,16);
        $mangoSiteId= str_pad(MANGO_SITE_ID,10);
        //print_r(URL_API_MAIN);
        //die();
        $totalAmount=0;
        $strIDs="";
        foreach ($response['ResponseMessage'] as $key => $val) { 
            $date = new DateTime($val['transaction_date']);
            $transDate = $date->format('YmdHis');
            $sequenceno=str_pad($ctr,7,"0",STR_PAD_LEFT);
            $accountno= str_pad($val['account_no'],18,"0",STR_PAD_LEFT);
            $amount = str_pad($val['total_amount'],10,"0",STR_PAD_LEFT);
            
            $content="D{$mangoNetworkId}{$transDate}{$sequenceno}{$mangoConfirmationNo}{$accountno}{$amount}{$mangoSiteId}";
            $fp = fopen($url,"a+");
            fwrite($fp,"{$content}\r\n");
            fclose($fp);  
            $ctr++;
            
            $totalAmount = $totalAmount+$val['total_amount'];
            $strIDs.=$val['id'].",";
        }
        
        $ctr--;
        $totalRecord = str_pad($ctr,7,"0",STR_PAD_LEFT);
        $totalAmount = str_pad($totalAmount,12,"0",STR_PAD_LEFT);
        $spaces = str_pad("",71);
        
        $content="T{$totalRecord}{$totalAmount}{$spaces}";
        $fp = fopen($url,"a+");
        fwrite($fp,"{$content}\r\n");
        fclose($fp);
        
         if($strIDs!="")
        {
            $current = file_get_contents($url);
            header('Content-disposition: attachment; filename=payment.txt');
            header('Content-type: text/plain');
            echo $current;
            die();  
            
            /*$handle = fopen("file.txt", "w");
            fwrite($handle, $content);
            fclose($handle);

            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename('file.txt'));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize('file.txt'));
            readfile('file.txt');
            exit;
            */
            
            
        } else {
                $this->response['message'] = 'Error encountered while saving textfile.';
                $this->response['success'] = false;
        }
    }
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}
