<?php
class backend {
    protected $response;
    protected $db;
    public $salt;
 
    public function __construct(&$db) {
        $this->salt = PROGRAM;  
        $this->salt2 = '2016'.PROGRAM.'2016';
        $this->db = $db;
        require_once 'xorry.php';
        require_once 'class.phpmailer.php';
        require_once dirname(__DIR__) . '/libraries/HideIt.php';
        $this->hideit = new HideIt('aes-256-cbc');
		error_reporting(E_ALL);
    }

    //merchant_id, reservation_date, reservation_time, product_id, specialist_id
    public function check_for_conflict($params)
    {
        $reservation_time = $params['reservation_time'];
        $n_day = date('l', strtotime($params['reservation_date']));
        $day = strtoupper($n_day);

        $cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']} AND day='{$day}'";
        $b_hours = $this->db->fetchRow($cmd);

        if($b_hours['is_close'])
        {
            return $this->success("Sorry, we are closed at this date and time.");
        }


        $opening_time = $b_hours['start_time'];
        $closing_time = $b_hours['end_time'];


        $product_id = $params['product_id'];
        $cmd ="SELECT minutes_of_product, name, id FROM product WHERE id IN({$product_id})";
            
        $prod = $this->db->fetchRow($cmd);
        $p_name = $prod['name'];
        $p_id = $prod['id'];

        $minutes = $prod['minutes_of_product'];
        $minutes = round($minutes);

        $start_time = strtotime($opening_time);
        $end_time = strtotime($closing_time); 

        

        $cmd ="SELECT s.*, sch.start_time AS new_start_time, sch.end_time AS new_end_time FROM sale_person s LEFT JOIN sale_person_schedule AS sch ON s.id = sch.sales_person_id WHERE s.status='A' AND sch.day='{$n_day}' AND is_off = 0 AND s.id IN(SELECT sale_person_id FROM product_sale_person WHERE product_id IN({$product_id}) AND sale_person_id IN({$params['specialist_id']}))";


        $sp = $this->db->fetchRow($cmd);

        if(!(isset($sp['id'])))
        {
            return $this->failed("Specialist is unavailable.");
        }

        $s_id = $sp['id'];

        $s_start_time =strtotime($sp['new_start_time']);
        $s_end_time =strtotime($sp['new_end_time']);

        $new_time = $params['reservation_time'];

        if(!((strtotime($new_time) >= $s_start_time) && $s_end_time >= strtotime($new_time)))
        {
            return $this->failed("Specialist is unavailable on your selected time.");
        }

        // print_r("hello");
        // die();


        $cmd ="SELECT h.id FROM reservation_header h
        INNER JOIN reservation_sale_person s ON h.id=s.reservation_header_id
        WHERE s.sale_person_id={$s_id}
        AND h.reservation_date='{$params['reservation_date']}'";


        // $cmd .= "AND ('{$nnew_time}' >= reservation_time AND reservation_time<='{$nnew_time}') LIMIT 1";
        $next_time = strtotime("+{$minutes} minutes", strtotime($new_time));
        $next_time = date('H:i', $next_time);

        // $cmd .= "AND (reservation_time >='{$new_time}' AND reservation_time<='{$next_time}') LIMIT 1";
        $cmd .= "AND (ADDTIME(reservation_time,SEC_TO_TIME({$minutes}*60)) >='{$new_time}' AND reservation_time<='{$next_time}') LIMIT 1";


        // print_r($cmd);
        // die();
        $reserve = $this->db->fetchRow($cmd);
        if(count($reserve) > 0)
        {
            return $this->success("Conflict");
        }else{
            return $this->success("NoConflict");
        }
    }

    public function check_for_conflict_two($params) {
        $day = date('l', strtotime($params['reservation_date']));
        
        $time =$params['reservation_time'];
        $u_day = strtoupper($day);

        $cmd ="SELECT id, start_time, end_time FROM merchant_business_hours mb WHERE merchant_id='{$params['merchant_id']}' AND TIME_TO_SEC('{$time}') >= TIME_TO_SEC(start_time) AND
        TIME_TO_SEC(end_time) >= TIME_TO_SEC('{$time}')
        AND is_close=0 AND day='{$u_day}'";

        $b_hours = $this->db->fetchRow($cmd);

        if(count($b_hours) == 0)
        {
            return $this->success("Closedate");
        }

        $opening_time = $b_hours['start_time'];
        $closing_time = $b_hours['end_time'];

        $product_id = $params['product_id'];
        $cmd ="SELECT minutes_of_product, name, id FROM product WHERE id IN({$product_id})";
            
        $prod = $this->db->fetchRow($cmd);
        $p_name = $prod['name'];
        $p_id = $prod['id'];

        $minutes = $prod['minutes_of_product'];
        $minutes = round($minutes);

        $start_time = strtotime($opening_time);
        $end_time = strtotime($closing_time); 

        $cmd ="SELECT s.*, sch.start_time AS new_start_time, sch.end_time AS new_end_time FROM sale_person s LEFT JOIN sale_person_schedule AS sch ON s.id = sch.sales_person_id WHERE s.status='A' AND sch.day='{$u_day}' AND is_off = 0 AND s.id IN(SELECT sale_person_id FROM product_sale_person WHERE product_id IN({$product_id}) AND sale_person_id IN({$params['specialist_id']}))";

        $sp = $this->db->fetchRow($cmd);

        if(!(isset($sp['id'])))
        {
            return $this->success("SpecialistUnavailable"); //"Specialist is unavailable."
        }

        $s_id = $sp['id'];

        $s_start_time =strtotime($sp['new_start_time']);
        $s_end_time =strtotime($sp['new_end_time']);

        $new_time = $params['reservation_time'];

        if(!((strtotime($new_time) >= $s_start_time) && $s_end_time >= strtotime($new_time)))
        {
            return $this->success("SpecialistUnavailableTime"); //"Specialist is unavailable on your selected time."
        }

        // print_r("hello");
        // die();


        $cmd ="SELECT h.id FROM reservation_header h
        INNER JOIN reservation_sale_person s ON h.id=s.reservation_header_id
        WHERE s.sale_person_id={$s_id}
        AND h.reservation_date='{$params['reservation_date']}'";


        // $cmd .= "AND ('{$nnew_time}' >= reservation_time AND reservation_time<='{$nnew_time}') LIMIT 1";
        $next_time = strtotime("+{$minutes} minutes", strtotime($new_time));
        $next_time = date('H:i', $next_time);

        // $cmd .= "AND (reservation_time >='{$new_time}' AND reservation_time<='{$next_time}') LIMIT 1";
        $cmd .= "AND (ADDTIME(reservation_time,SEC_TO_TIME({$minutes}*60)) >='{$new_time}' AND reservation_time<='{$next_time}') LIMIT 1";


        // print_r($cmd);
        // die();
        $reserve = $this->db->fetchRow($cmd);
        if(count($reserve) > 0)
        {
            return $this->success("ReservationConflict");
        }else{
            return $this->success("NoConflict");
        }
    }

    public function get_specialist_suggestions($params) {
        $products = explode(',', $params['product_ids']);

        $param_time = array(
            'merchant_id' => $params['merchant_id'],
        );

        $this->get_time_zone($param_time);
        $timezone = $this->response['ResponseMessage']['timezone'];
        date_default_timezone_set($timezone);

        $n_day = date('l', strtotime($params['reservation_date']));
        $day = strtoupper($n_day);

        $cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']} AND day='{$day}'";
        $b_hours = $this->db->fetchRow($cmd);

        if($b_hours['is_close'])
        {
            return $this->success("Sorry, we are closed at this date and time.");
        }


        $opening_time = $b_hours['start_time'];
        $closing_time = $b_hours['end_time'];

        $query_specialist = "SELECT s.*, sch.start_time AS new_start_time, 
                            sch.end_time AS new_end_time 
                            FROM sale_person s 
                            LEFT JOIN sale_person_schedule AS sch 
                            ON s.id = sch.sales_person_id 
                            WHERE s.status='A' 
                            AND sch.day='{$day}' 
                            AND is_off = 0 
                            AND '{$params['reservation_time']}' BETWEEN sch.start_time AND sch.end_time
                            AND s.merchant_id = {$params['merchant_id']}";

        $avlble_specialist_day = $this->db->fetchAll($query_specialist);

        $specialist_has_all_service = array();
        $available_specialist = array();

        $reservation_start_time = $params['reservation_time']; 
        
        foreach ($avlble_specialist_day as $specialist) {

            $has_all_service = true;
            $total_mins_service = 0;

            foreach ($products as $p) {
                $query_service_spec = "SELECT * FROM product p
                                        LEFT JOIN product_sale_person ps
                                        ON p.id = ps.product_id
                                        WHERE p.id = {$p}
                                        AND ps.sale_person_id = {$specialist['id']}";

                $product_specialist = $this->db->fetchAll($query_service_spec);
                //var_dump($product_specialist);die;

                if (count($product_specialist) <= 0) {
                    $has_all_service = false;
                } else {
                    $total_mins_service += intval($product_specialist[0]['minutes_of_product']);
                }
            }

            if ($has_all_service) {
                $specialist_has_all_service[] = $specialist['id'];
                $time = strtotime($reservation_start_time);
                $end_time = date("H:i", strtotime('+'.$total_mins_service.' minutes', $time));
                
                $query_reservation = "SELECT * FROM reservation_header rh
                                    LEFT JOIN reservation_detail rd
                                    ON rh.id = rd.reservation_header_id
                                    WHERE reservation_date = '{$params['reservation_date']}'
                                    AND sale_person_ids = {$specialist['id']}
                                    AND (ADDTIME(rh.reservation_time,SEC_TO_TIME({$total_mins_service}*60))  >='{$params['reservation_time']}' 
                                    AND rh.reservation_time <='{$end_time}') LIMIT 1";

                $result_reservation = $this->db->fetchRow($query_reservation);

                if (count($result_reservation) <= 0) {
                    $available_specialist[] = $specialist;
                }
            }
        }


        return $this->success($available_specialist);
    }

    public function check_if_store_open($params) {
        $day = date('l', strtotime($params['reservation_date']));
        
        $time =$params['reservation_time'];
        $u_day = strtoupper($day);

        $cmd ="SELECT id, start_time, end_time FROM merchant_business_hours mb WHERE merchant_id='{$params['merchant_id']}' AND TIME_TO_SEC('{$time}') >= TIME_TO_SEC(start_time) AND
        TIME_TO_SEC(end_time) >= TIME_TO_SEC('{$time}')
        AND is_close=0 AND day='{$u_day}'";

        $result = $this->db->fetchAll($cmd);
        if(count($result) == 0)
        {
            return $this->success("Closedate");
        } else {
            return $this->success("NoConflict");
        }
    }


    //merchant_id, product_ids, optional specialist_ids, reservation_date
    public function get_reservation_suggestions($params)
    {
    	//get duration of product
    	//per product
    	

    	$products = explode(',', $params['product_ids']);

        $param_time = array(
            'merchant_id' => $params['merchant_id'],
        );
        $this->get_time_zone($param_time);
        $timezone = $this->response['ResponseMessage']['timezone'];
        date_default_timezone_set($timezone);

    	$n_day = date('l', strtotime($params['reservation_date']));
    	$day = strtoupper($n_day);

        //return $this->success($day);

    	$cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']} AND day='{$day}'";
    	$b_hours = $this->db->fetchRow($cmd);

    	if($b_hours['is_close'])
    	{
    		return $this->success("Sorry, we are closed at this date and time.");
    	}


    	$opening_time = $b_hours['start_time'];
    	$closing_time = $b_hours['end_time'];

    	// $opening_time = strtotime("+30 minutes", strtotime($opening_time));
    	// $opening_time = date('h:i A', $opening_time);
    	// print_r($opening_time);
    	// die();

    	$time_list= array();

    	foreach($products as $p)
    	{
    		$cmd ="SELECT minutes_of_product, name, id FROM product WHERE id={$p}";
    		
    		$prod = $this->db->fetchRow($cmd);
    		$p_name = $prod['name'];
    		$p_id = $prod['id'];

    		$minutes = $prod['minutes_of_product'];
    		$minutes = round($minutes);

    		$start_time = strtotime($opening_time);
    		$end_time = strtotime($closing_time); 

    		$cmd ="SELECT s.*, sch.start_time AS new_start_time, sch.end_time AS new_end_time FROM sale_person s LEFT JOIN sale_person_schedule AS sch ON s.id = sch.sales_person_id WHERE s.status='A' AND sch.day='{$day}' AND is_off = 0 AND s.id IN(SELECT sale_person_id FROM product_sale_person WHERE product_id={$p}";

    		if(isset($params['specialist_ids'])  && $params['specialist_ids'] !== "")
    		{
    			$cmd .=" AND sale_person_id IN({$params['specialist_ids']})"; 
    		}
    		$cmd .=")";

    		// print_r($cmd);
    		// die();
    		$specialists = $this->db->fetchAll($cmd);

    		// print_r($specialists);
    		// die();

    		$new_time = date('h:i A', $start_time);
    		$time = array();
    		foreach($specialists as $s)
			{
				$s_start_time =strtotime($s['new_start_time']);
                $s_end_time =strtotime($s['new_end_time']);

				
				$first_name = $s['first_name'];
				$last_name = $s['last_name'];
				$s_id = $s['id'];


				if((strtotime($new_time) >= $s_start_time) && $s_end_time >= strtotime($new_time))
				{

					//checkif_specilist has sked on the said date and time
					$cmd ="SELECT h.id FROM reservation_header h
						INNER JOIN reservation_sale_person s ON h.id=s.reservation_header_id
						WHERE s.sale_person_id={$s_id}
						AND h.reservation_date='{$params['reservation_date']}'";

						$nnew_time = date('H:i', strtotime($new_time));
                        $m_time = date('H:i', strtotime($new_time));

						// $cmd .= "AND ('{$nnew_time}' >= reservation_time AND reservation_time<='{$nnew_time}') LIMIT 1";
						$next_time = strtotime("+{$minutes} minutes", strtotime($new_time));
                        $next_time = date('H:i', $next_time);

                        // $cmd .= "AND (reservation_time >='{$nnew_time}' AND reservation_time<='{$next_time}') LIMIT 1";
                        $cmd .= "AND (ADDTIME(reservation_time,SEC_TO_TIME({$minutes}*60))  >='{$nnew_time}' AND reservation_time <='{$next_time}') LIMIT 1";

						// print_r($cmd);
						// die();
                        $nnew_time = date('h:i A', strtotime($nnew_time));
						$reserve = $this->db->fetchRow($cmd);
						if(count($reserve) == 0)
						{
							$time = array(
				    			'time' => $nnew_time,
                                'm_time' => $m_time,
				    			'service' => $p_name,
				    			'service_id' => $p_id,
				    			'date' => $params['reservation_date'],
				    			'first_name' => $first_name,
				    			'last_name' => $last_name,
				    			'specialist_id' => $s_id,
				    		);
				    		$time_list[] = $time;
				    	}
				}
			}


    		// $time_list[] = $time;


    		while($start_time < $end_time)
    		{
    			$start_time = strtotime("+{$minutes} minutes", $start_time);
                $m_time = date('H:i', $start_time);
    			$new_time = date('h:i A', $start_time);
    			foreach($specialists as $s)
    			{
    				$s_start_time =strtotime($s['new_start_time']);
                    $s_end_time =strtotime($s['new_end_time']);
    				$first_name = $s['first_name'];
					$last_name = $s['last_name'];
    				$s_id = $s['id'];


    				if((strtotime($new_time) >= $s_start_time) && $s_end_time >= strtotime($new_time))
    				{

    					$cmd ="SELECT h.id FROM reservation_header h
						INNER JOIN reservation_sale_person s ON h.id=s.reservation_header_id
						WHERE s.sale_person_id={$s_id}
						AND h.reservation_date='{$params['reservation_date']}'";

						$nnew_time = date('H:i', strtotime($new_time));

    					$next_time = strtotime("+{$minutes} minutes", strtotime($new_time));

                        $next_time = date('H:i', $next_time);

						// $cmd .= "AND (reservation_time >='{$nnew_time}' AND reservation_time<='{$next_time}') LIMIT 1";
                        $cmd .= "AND (ADDTIME(reservation_time,SEC_TO_TIME({$minutes}*60))  >='{$nnew_time}' AND reservation_time <='{$next_time}') LIMIT 1";
                        
						$reserve = $this->db->fetchRow($cmd);
						if(count($reserve) == 0)
						{
				    		$time = array(
				    			'time' => $new_time,
                                'm_time' => $m_time,
				    			'service' => $p_name,
				    			'service_id' => $p_id,
				    			'date' => $params['reservation_date'],
				    			'first_name' => $first_name,
			    				'last_name' => $last_name,
				    			'specialist_id' => $s_id,
				    		);
			    			$time_list[] = $time;
			    		}
		    		}
	    		}
    		}
    	}

    	return $this->success($time_list);
    }


    //merchant_id, reservation_id, status, update_date, update_by
    public function change_reservation_status($params)
    {

    	$status = strtoupper($params['status']);
    	$cmd = "SELECT id FROM status_default WHERE name='{$status}'";
    	$res = $this->db->fetchRow($cmd);
    	if(count($res)==0)
    	{
    		return $this->failed("Invalid reservation status.");
    	}

    	$id = $params['reservation_id'];
    	$update_data = array(
    		'update_date' => $params['update_date'],
    		'update_by' => $params['update_by'],
    		'status' => $params['status'],
    		'reservation_status_id' => $res['id'],
    	);

        if(!$this->db->update('reservation_header', $update_data, 'id='.$id))
        {
        	return $this->failed("Unable to delete service.");
        }

        if( ($params['status'] != 'SCHEDULED') ){
            $update_data2 = array(
                'status' => 'D'
            );

            if(!$this->db->update('reservation_detail', $update_data2, 'reservation_header_id='.$id))
            {
                return $this->failed("Unable to change reservation status.");
            }
        }

        return $this->success("Reservation status has been updated");
    }	


	//merchant_id, category_id, specialist_id
	public function get_products_per_merchant_per_specialist($params)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd = "SELECT p.*, c.name as category_name, c.description as category_description FROM product p LEFT JOIN  product_category c ON p.category_id=c.id WHERE p.status='A' AND p.merchant_id={$params['merchant_id']}";
		
		if($params['category_id']>0)
		{
			$cmd .=" AND category_id={$params['category_id']}";
		}

		$cmd .= " AND p.id IN(SELECT product_id FROM product_sale_person WHERE sale_person_id = {$params['specialist_id']} ) ";
		
		// print_r($cmd);
		// die();

		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}

    public function get_product_category_per_merchant_per_specialist($params) {
        $cmd ="SELECT psp.product_id, p.name, p.category_id, pc.name AS category_name FROM product_sale_person psp
                LEFT JOIN product p
                ON psp.product_id = p.id
                LEFT JOIN product_category pc
                ON p.category_id = pc.id
                WHERE sale_person_id = {$params['specialist_id']}
                GROUP BY p.category_id";

        $result = $this->db->fetchAll($cmd);
        return $this->success($result);       
    }



	//merchant_id, product_id, update_date, update_by
    public function delete_my_product($params)
    {
    	//check if category is tag a parent of product
    	// $cmd = "SELECT  p.id FROM  product_sale_person sp LEFT JOIN 
    	// product p ON sp.product_id=p.id WHERE sp.product_id={$params['product_id']} AND sp.merchant_id={$params['merchant_id']} LIMIT 1";

    	// $rs = $this->db->fetchRow($cmd);

    	// if(count($rs) > 0)
    	// {
    	// 	return $this->failed("Unable to delete service. Service has been associated with a specialist.");
    	// }

    	$cmd = "SELECT  p.id FROM  reservation_detail rd LEFT JOIN 
    	product p ON rd.product_id=p.id WHERE rd.product_id={$params['product_id']} AND p.merchant_id={$params['merchant_id']} AND rd.status='A' LIMIT 1";

    	$rs = $this->db->fetchRow($cmd);

    	if(count($rs) > 0)
    	{
    		return $this->failed("Unable to delete service. Service has been associated with a reservation.");
    	}

        $params = array_map('trim', $params);
        $id = $params['product_id'];
       	$values = array(
       		'update_date' => $params['update_date'],
       		'update_by' => $params['update_by'],
       		'status'=> 'D',
       	);

        if(!$this->db->update('product', $values, 'id='.$id))
        {
        	return $this->failed("Unable to delete service.");
        }
        return $this->success("Service has been deleted.");
    }



	//merchant_id, specialist_id, update_date, update_by
    public function delete_my_specialist($params)
    {
    	//check if category is tag a parent of product
    	$cmd = "SELECT  rh.id FROM  reservation_sale_person rp LEFT JOIN 
    	reservation_header rh ON rp.reservation_header_id=rh.id WHERE rp.sale_person_id={$params['specialist_id']} AND rh.merchant_id={$params['merchant_id']} LIMIT 1";

    	// $rs = $this->db->fetchRow($cmd);

    	// if(count($rs) > 0)
    	// {
    	// 	return $this->failed("Unable to delete specialist. Specialist has already been used.");
    	// }

        $params = array_map('trim', $params);
        $id = $params['specialist_id'];
       	$values = array(
       		'update_date' => $params['update_date'],
       		'update_by' => $params['update_by'],
       		'status'=> 'D',
       	);

        if(!$this->db->update('sale_person', $values, 'id='.$id))
        {
        	return $this->failed("Unable to delete specialist.");
        }
        return $this->success("Specialist has been deleted.");
    }


    //merchant_id, category_id, update_date, update_by
    public function delete_my_category($params)
    {
    	//check if category is tag a parent of product
    	$cmd = "SELECT  id FROM  product WHERE category_id={$params['category_id']} AND merchant_id={$params['merchant_id']} AND status='A' LIMIT 1";

    	$rs = $this->db->fetchRow($cmd);

    	if(count($rs) > 0)
    	{
    		return $this->failed("Unable to delete category. Category has already been linked to a product.");
    	}

        $params = array_map('trim', $params);
        $id = $params['category_id'];
       	$values = array(
       		'update_date' => $params['update_date'],
       		'update_by' => $params['update_by'],
       		'status'=> 'D',
       	);

        if(!$this->db->update('product_category', $values, 'id='.$id))
        {
        	return $this->failed("Unable to delete category.");
        }
        return $this->success("Category has been deleted.");
    }



    public function get_merchant_info($merchant_id)
    {
    	$cmd = "SELECT * FROM merchant WHERE id={$merchant_id}";
    	$rs = $this->db->fetchRow($cmd);

    	return $rs;
    }	



    //merchant_id
    public function get_merchant_customers($params)
    {

    }

    // //merchant_id, start_date, end_date
    // public function get_reservation_history($params)
    // {

    // }


	
	//merchant_id, message ,last_name, first_name, contact_number, create_date, create_by
	public function send_sms_to_customer($params)
	{
		// if(IS_PRODUCTION)
		// {
		// 	$sms_response = $this->send_sms("{message}", $params['contact_number']);
		// }else{
		// 	$sms_response = array(
		// 		'success' => true,
		// 		'message' => 'ok'
		// 	); 
		// }
		// return $this->failed("Unable to send SMS.");
		// die();

		$merchant = $this->get_merchant_info($params['merchant_id']);

		$business_name = $merchant['business_name'];

		$params['message'] = "FROM: {$business_name} - ". $params['message'];

		if(IS_PRODUCTION)
		{
			$sms_response = $this->send_sms($params['message'], PHONEPREFIX.$params['contact_number']);
		}else{
			$sms_response['success'] = true;
		}

		if($sms_response['success'])
		{
			$insert_data = array(
				'contact_number' => $params['contact_number'],
				'last_name' => $params['last_name'],
				'first_name' => $params['first_name'],
				'create_date' => $params['create_date'],
				'create_by' => $params['create_by'],
				'merchant_id' => $params['merchant_id'],
				'message' => $params['message'],
			);
			
			if(!$this->db->insert("merchant_sms", $insert_data))
			{
				return $this->failed("Error sending message.");
			}
			return $this->success("Message has been sent.");
		}else{
			return $this->failed("Unable to send SMS.");
		}
	}
	

	private function send_sms($message, $mobile_number)
	{
		$params = array(
            'user'      => 'GO3INFOTECH',
            'password'  => 'TA0828g3i',
            'sender'    => 'Go3Solutions',
            'SMSText'   => $message,
            'GSM'       => $mobile_number,
        );
        $send_url = 'https://api2.infobip.com/api/v3/sendsms/plain?' . http_build_query($params);

        $send_response = file_get_contents($send_url);
        if (!($send_response != '')) {
            return  array(
                'success' => FALSE,
                'message' => 'No Response',
            );
        } else {
            if (strstr($send_response, '<status>0</status>') === false) {
                return array(
                    'success' => FALSE,
                    'message' => 'Failed:' . $send_response,
                );
            } else {
                return array(
                    'success' => true,
                    'message' => 'Success:' . $send_response,
                );
            }
        }
	}

	// } elseif (strtotime($chk['last_seen']) + (3600) < time()) {
	//contact_number, verfication_code, new_password
	public function reset_password($params)
	{
		$cmd="SELECT id FROM customer WHERE status='A' AND mobile_number='{$params['contact_number']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Contact number cannot be found.");
		}
		
		$id =  $res['id'];
		
		$cmd =" SELECT create_date FROM customer_verification  WHERE customer_id={$id} AND status='A' AND passcode='{$params['verification_code']}'";
		
		$chk= $this->db->fetchRow($cmd);
		
		if (strtotime($chk['create_date']) + (3600) < time()) {
			return $this->failed("Your verification code already expires.");
		}
		
		$new_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_password'], sha1(md5($this->salt)));  
		
		$update_user = array(
			'password' => $new_password
		);
		
		
		if(!$this->db->update('users', $update_user, 'reference_id='. $id. " AND status='A' AND user_type_id=3"))
		{
            // $this->db->rollback_transaction();	
            $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error resetting password '  ,'ERROR',$datetime,"API",'');
            return $this->failed("Unable to reset password.");
		}
		return $this->success("Your password has been reset.");
	}
		

	//contact_number
	public function forgot_password($params)
	{
		$cmd="SELECT id FROM customer WHERE status='A' AND mobile_number='{$params['contact_number']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Contact number cannot be found.");
		}else{
			$id = $res['id'];
			$passcode = $this->generate_passcode();
			//add verification code to table
			$insert_data = array(
				'customer_id' => $id,
				'passcode' => $passcode,
				'status' => 'A',
				'create_date' => date('Y-m-d H:i:s'),
			);
			
			// var_dump(IS_PRODUCTION);
			// die();
			
			if(IS_PRODUCTION)
			{
				$insert_data['is_sms'] = 1;
			}
			
			if(!$this->db->insert("customer_verification", $insert_data))
			{
				return $this->failed("Unable to generate verification code.");
			}
			
			//send verification code to sms
			if(IS_PRODUCTION)
			{
				$this->send_sms("Your verification code is {$passcode}", $params['contact_number']);
				return $this->success("Forgot password verification code has been sent on your mobile.");
			}else{
				return $this->success("Forgot password verification code is {$passcode}.");
			}
			
		} 	
	}

    public function forgot_password_email($params) {
        $cmd="SELECT u.id, u.first_name, u.email_address, m.business_name FROM users u 
        LEFT JOIN merchant m
        ON u.reference_id = m.id
        WHERE u.status='A' 
        AND email_address='{$params['email_address']}'";

        $res = $this->db->fetchRow($cmd);

        if(count($res) == 0)
        {
            return $this->failed("Email Address cannot be found.");
        } else {
            $code = md5($res['id']);
            $body = '<html>
    <body style="max-width: 100%; min-width: 40%; margin-left: 25px;margin-right: 25px;">
        <div style="margin: 0 auto; max-width: 600px; border-style: solid; border-width: 15px 1px 1px; border-color: #65bd77 #dddddd #dddddd;">
            <div style="padding: 30px; font: 16px arial, sans-serif; color: #797c80;">
                <p>Hello '. $res['first_name'] . ',</p>  
                <p>We received a request to reset your password for your Go3 Reservation account.</p>
                <p>Simply click on the button to set a new password:</p>
                <a href="http://'.$_SERVER['SERVER_NAME'].'/v2/log/forgotpassword?code='.$code.'" style="background-color: #65bd77; color: white;
    padding: 7px;
    text-decoration: none;
    border-radius: 2px;">Set a New Password</a>
    <p>Cheers,</p>
                <p>'.$res['business_name'].'</p>
            </div>
        </div>
    </body>
</html>';
            
            $this->send_mail_notification($body, $res['email_address'], "Reservation", "", $res['business_name']);

            return $this->success("Success");
        }
    }

    public function change_password_email($params) {
        $new_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_password'], sha1(md5($this->salt)));
        
        $cmd="SELECT id from users WHERE md5(id)='{$params['code']}'";

        $rs = $this->db->fetchRow($cmd);

        if(count($rs)==0)
        {
            return $this->failed("Unable to update password. Invalid code.");
        }

        $update_data = array(
            'update_date' => $params['update_date'],
            'password' => $new_password,
        );
        
        if (!($this->db->update('users', $update_data, 'id=' . $rs['id']))) {
            return $this->failed("Unable to update password.");    
        } else {
            return $this->success("Your account password has been updated.");
        }
    }

    public function check_md5_password($params) {
        $cmd="SELECT id from users WHERE md5(id)='{$params['code']}'";

        $res = $this->db->fetchRow($cmd);

        if (count($res) == 0) {
            return $this->failed("Invalid code");
        } else {
            return $this->success("Success");
        }

    }
	
	protected function generate_passcode() {
        do {
            $passcode = mt_rand(9999, 99999999);
            $check = $this->db->fetchRow('select id from customer_verification where passcode="' . $passcode .'"');
        } while (isset($check['id']));
        return $passcode;
    }
	

	//customer_id, merchant_id
	public function check_if_favorite($params)
	{
		$cmd =" SELECT id FROM customer_favorite WHERE customer_id={$params['customer_id']} AND merchant_id={$params['merchant_id']} AND status='A'";
		
		$res = $this->db->fetchRow($cmd);
		
		if(count($res) == 0)
		{
			return $this->success(array("favorite_id" => $res['id']));
		}else
		{
			return $this->success(array("favorite_id" => $res['id']));
		}
	}	

	//customer_id
	public function list_my_favorite($params)
	{
		$cmd =" SELECT cf.id as favorite_id, m.* FROM customer_favorite cf INNER JOIN merchant m ON cf.merchant_id=m.id WHERE cf.customer_id={$params['customer_id']} AND cf.status='A'";
		
		$results = $this->db->fetchAll($cmd);
		return $this->success($results);
	}
	
	//id, customer_id, update_by, update_date
	public function remove_as_favorite($params)
	{
		$cmd =" SELECT id FROM  customer_favorite WHERE id={$params['favorite_id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)==0)
		{
			return $this->failed("Unable to remove the selected merchant favorite.");
		}
		$update_data = array(
			'status' => 'D',
			'update_by' => $params['update_by'],
			'update_date' => $params['update_date'],	
		);
		
		if(!$this->db->update('customer_favorite', $update_data, 'id='. $params['favorite_id']. " AND customer_id={$params['customer_id']}"))
		{
			$datetime = date('Y-m-d H:i:s');
            $this->db->rollback_transaction();	
			$this->save_to_action_logs('Error removing from favorites '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to remove customer favorite.");
		}
		
		return $this->success("Favorite has been removed.");
	}
	
	//customer_id, merchant_id, create_date, create_by
	public function save_as_favorite($params)
	{
		$cmd =" SELECT id FROM merchant WHERE id={$params['merchant_id']}";
		$res = $this->db->fetchRow($cmd);
		if(count($res)==0)
		{
			return $this->failed("Unable to find merchant.");
		}
		
		$cmd =" SELECT id FROM  customer_favorite WHERE customer_id={$params['customer_id']} AND merchant_id={$params['merchant_id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)>0)
		{
			return $this->failed("Merchant has already been added on your favorites.");
		}
		
		$insert_data = array(
			'customer_id' => $params['customer_id'],
			'merchant_id' => $params['merchant_id'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => 'A'
		);
		
		$this->db->begin_transaction();
		
		if(!$this->db->insert('customer_favorite', $insert_data))
		{
			return $this->failed("Unable to save as favorite.");
		}
		
		$this->db->commit_transaction();
		return $this->success("Merchant has been added on your list of favorite.");
	}
	
	
	//customer_id email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender, update_date, update_by
	public function update_my_customer_information($params)
	{
		
		$cmd="SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' AND status='A' AND id<> {$params['customer_id']}";
		
		// print_r($cmd);
		// die();
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Mobile number has already been registered.");
		}
		
		$this->db->begin_transaction();
		
		
		$update_data = array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'middle_name' => isset($params['middle_name']) ? $params['middle_name'] : '',
			'email' => isset($params['email']) ? $params['email'] : '',
			'mobile_number' => $params['contact_number'],
			'city' => isset($params['city']) ? $params['city'] : '',
			'state' => isset($params['state']) ? $params['state'] : '',
			'zip' => isset($params['zip']) ? $params['zip'] : '',
			'country' => isset($params['country']) ? $params['country'] : '',
			'birth_date' => isset($params['birth_date']) ? $params['birth_date'] : '',
			'gender' => isset($params['gender']) ? $params['gender'] : '',
			'address' => isset($params['address']) ? $params['address'] : '',
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
            'photo' => $params['photo']
			
		);
		
		if(!$this->db->update('customer', $update_data, 'id='. $params['customer_id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating customer information '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update cusotmer information.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Customer information has been updated.");
	}

    public function update_my_customer_information_no_session($params)
    {
    
        $cmd="SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}'";
        
        // print_r($cmd);
        // die();
        
        $res = $this->db->fetchRow($cmd);        
        $this->db->begin_transaction();
        
        
        $update_data = array(
            'last_name' => $params['last_name'],
            'first_name' => $params['first_name'],
            'middle_name' => isset($params['middle_name']) ? $params['middle_name'] : '',
            'email' => isset($params['email']) ? $params['email'] : '',
            'mobile_number' => $params['contact_number'],
            'city' => isset($params['city']) ? $params['city'] : '',
            'state' => isset($params['state']) ? $params['state'] : '',
            'zip' => isset($params['zip']) ? $params['zip'] : '',
            'country' => isset($params['country']) ? $params['country'] : '',
            'birth_date' => isset($params['birth_date']) ? $params['birth_date'] : '',
            'gender' => isset($params['gender']) ? $params['gender'] : '',
            'address' => isset($params['address']) ? $params['address'] : '',
            'update_date' => $params['update_date'],
            'update_by' => $params['update_by'],
            
        );
        
        if(!$this->db->update('customer', $update_data, 'mobile_number='. $params['contact_number']))
        {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating customer information '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update cusotmer information.");
        }  
        
        $this->db->commit_transaction();
        return $this->success("Customer information has been updated.");
    }
	
	
	
	//customer_id
	public function get_customer_check_ins($params)
	{
		$cmd =" SELECT mobile_number FROM customer WHERE status='A' AND id={$params['customer_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(isset($res['mobile_number']))
		{
			$mobile_number = $res['mobile_number'];
		}
		
		$cmd = "SELECT m.business_name,m.logo,m.description, m.id as merchant_id,w.create_date, TIME_FORMAT(check_in_time, '%h:%i%p') as check_in_time, w.status, wd.product_id, p.name as product FROM waiting_list w 
		LEFT JOIN merchant m ON w.merchant_id=m.id
        LEFT JOIN waiting_list_detail wd ON w.id=wd.waiting_list_header_id
        LEFT JOIN product p ON wd.product_id=p.id
		WHERE contact_number='{$mobile_number}' AND customer_id=-1

		UNION ALL SELECT  m.business_name,m.logo,m.description, m.id as merchant_id,w.create_date, TIME_FORMAT(check_in_time, '%h:%i%p') as check_in_time, w.status, wd.product_id, p.name as product FROM waiting_list w 
		LEFT JOIN merchant m ON w.merchant_id=m.id
        LEFT JOIN waiting_list_detail wd ON w.id=wd.waiting_list_header_id
        LEFT JOIN product p ON wd.product_id=p.id
		WHERE customer_id={$params['customer_id']}

        ORDER BY create_date DESC
		";

		
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	
	//customer_id
	public function get_my_customer_reservation($params)
	{
		
		$cmd =" SELECT mobile_number FROM customer WHERE status='A' AND id={$params['customer_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(isset($res['mobile_number']))
		{
			$mobile_number = $res['mobile_number'];
		}
		
		
		
		
		$cmd =" SELECT rh.*,TIME_FORMAT(rh.reservation_time, '%h:%i%p') asis_time,m.business_name, m.description, m.id as merchant_id, m.logo, p.name as product, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN merchant m ON rh.merchant_id=m.id
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
        LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id=m.id 
        LEFT JOIN reservation_detail rd ON rh.id=rd.reservation_header_id
		LEFT JOIN product p ON rd.product_id=p.id
		WHERE rh.customer_id={$params['customer_id']} 
		
		UNION ALL
		
		SELECT rh.*,TIME_FORMAT(rh.reservation_time, '%h:%i%p') asis_time,m.business_name, m.description, m.id as merchant_id, m.logo, p.name as product, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN merchant m ON rh.merchant_id=m.id
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id=m.id
        LEFT JOIN reservation_detail rd ON rh.id=rd.reservation_header_id
        LEFT JOIN product p ON rd.product_id=p.id 
		WHERE  rh.mobile='{$mobile_number}' AND rh.customer_id<0
		
		ORDER BY create_date DESC";
		
		// print_r($cmd);
		// die();
		
		$records = $this->db->fetchAll($cmd);
		
		return $this->success($records);	
	}
	
	//customer_id, merchant_id, comment, create_date, create_by
	public function create_my_review($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		
		$insert_data = array(
			'customer_id' => $params['customer_id'],
			'merchant_id' => $params['merchant_id'],
			'comment' => $params['comment'],
			'status' => 'A',
			'create_by' => $params['create_by'],
			'create_date' => $params['create_date'],
		);
		
		if(!$this->db->insert('merchant_review', $insert_data)) {
            return $this->failed("Unable to add merchant review.");
        }
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant Review has been created.");
	}
	
	
	//id, status 'A' for Active 'D' for deleted, comment
	public function update_my_review($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		$update_data = array(
			'status' => $params['status'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'comment' => $params['comment'],	
		);
		
		if(!$this->db->update('merchant_review', $update_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating merchant review '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to updating merchant review.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Merchant review has been updated.");
	}
	
	
	//merchant_id
	public function get_time_zone($params)
	{
		$cmd =" SELECT timezone FROM merchant WHERE id={$params['merchant_id']} ";
		$timezone = $this->db->fetchRow($cmd);
		
		return $this->success($timezone);
	}

	
	//merchant_id
	public function get_all_merchant_details($params)
	{
		$cmd ="SELECT * FROM merchant WHERE id={$params['merchant_id']}";
		$merch = $this->db->fetchRow($cmd);
		
		if(count($merch)==0)
		{
			return $this->failed("Unable to find merchant.");
		}
		
		//get services
		$cmd ="SELECT * FROM product_category WHERE status='A' AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['service_category'] = $res;
		
		$cmd ="SELECT * FROM product WHERE status='A' AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['services'] = $res;
		
		$cmd = "SELECT sp.id, sp.last_name, sp.first_name, GROUP_CONCAT(CASE WHEN p.name IS NULL THEN 'NA' ELSE p.name END) as services FROM sale_person sp 
LEFT JOIN product_sale_person psp ON sp.id=psp.sale_person_id
LEFT JOIN product p ON psp.product_id=p.id
 WHERE sp.status='A' AND sp.merchant_id={$params['merchant_id']}
 GROUP BY sp.id, sp.last_name, sp.first_name";
		$res = $this->db->fetchAll($cmd);
		$merch['specialist'] = $res;
		
		
		//get business hours
		$cmd ="SELECT TIME_FORMAT(start_time, '%h:%i%p') as start_time, TIME_FORMAT(end_time, '%h:%i%p') as end_time,
		day, is_close, merchant_id, id 
		FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['business_hours'] = $res;
		
		$cmd =" SELECT comment, r.create_date, c.first_name, c.last_name FROM merchant_review r INNER JOIN customer c ON r.customer_id=c.id  WHERE r.status='A' AND r.merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['reviews'] = $res;
		
		
		//get gallery NA
		//get reviews NA
		//get promo NA
		return $this->success($merch);
	}
	
	//last_name, first_name, contact_number, merchant_id, check_in_time, create_date, create_by
	//produc_id
	public function check_in_for_waiting_list($params)
	{
		$params = array_map('trim', $params);
		
		$cmd =" SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' ";
		
		$rs = $this->db->fetchRow($cmd);
		if(isset($rs['id']))
		{
			$params['customer_id'] = $rs['id'];
		}
		
		$day = date('l', strtotime($params['create_date']));
		
		
		$this->db->begin_transaction();
		
		$insert_data = array(
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'contact_number' => $params['contact_number'],
			'status' => 'WAITING', //SEATED for 
			'check_in_time' => $params['check_in_time'],
			'merchant_id' => $params['merchant_id'],
			'email_address' => $params['email_address'],
		);
		
		if(!$this->db->insert('waiting_list', $insert_data)) {
            return $this->failed("Unable to add waiting list.");
        }

        $waiting_list_id = $this->db->lastInsertId();

        if($params['product_id'] !=="")
        {
			$products = explode(",",$params['product_id']);
	        foreach ($products as $p)
			{
				$prod = $this->get_product_info($p);
				
				$price = $prod['price'];
				$minutes = $prod['minutes_of_product'];
				
				$insert_data = array(
					'waiting_list_header_id' => $waiting_list_id,
					'product_id' => $p,
					'price' => $price,
					'create_date' => $params['create_date'],
					'create_by' => $params['create_by'],
					'minutes_of_product' => $minutes,
					'status' => 'A',
				);
				
				if(!$this->db->insert('waiting_list_detail', $insert_data))
				{
					$this->db->rollback_transaction();
					return $this->failed("Unable to create waiting list.");
				}
			}
		}

		$this->db->commit_transaction();	

        if ($params['email_address']!=="") {
                $query_business = "SELECT business_name FROM merchant WHERE id='{$params['merchant_id']}'";
            
            $business = $this->db->fetchRow($query_business);
            $first_name_email = $params['first_name'];
            $last_name_email = $params['last_name'];
            $reservation_date = date("l, j M Y", strtotime($params['create_date']));
            $reservation_time = date("g:i a", strtotime($params['check_in_time']));
            $reservation_email = $params['email_address']; /*added*/
            $reservation_mobile = $params['contact_number'];

            
            $body = '<html>
                <body style="max-width: 100%; min-width: 40%; margin-left: 25px;margin-right: 25px;">
                    <div style="margin: 0 auto; max-width: 600px; border-style: solid; border-width: 15px 1px 1px; border-color: #65bd77 #dddddd #dddddd;">
                        <div style="padding: 30px; font: 16px arial, sans-serif; color: #797c80;">
                            <p>Hello '. $first_name_email . ',</p>  
                            <p>You are now added on our waiting list.</p>
                            <p>Business Name:'.$business['business_name'].
                            '</p><a href="http://'.$_SERVER['SERVER_NAME'].'/v2/mp/waitinglist/status?id='.$params['merchant_id'].'&mobile='.$reservation_mobile.'">Click here to check your current waiting status</a>
                        </div>
                    </div>
                </body>
            </html>';
        
            $this->send_mail_notification($body, $reservation_email, "Reservation");
        }    

		// return $this->success("Record has been added to the waiting list.");
        return $this->success("You had been added to the waiting list.");
        
	}
	
	//merchant_id, create_date, is_display_all_status
	public function get_waiting_list_per_merchant($params)
	{
		$param_time = array(
			'merchant_id' => $params['merchant_id'],
		);
		$this->get_time_zone($param_time);
		$timezone = $this->response['ResponseMessage']['timezone'];
		date_default_timezone_set($timezone);

		// $current_time = date("G:i");
	
		
		
		// $cmd ="SELECT w.*,TIME_FORMAT(check_in_time, '%h:%i%p') as normal_time FROM waiting_list w WHERE  w.create_date='{$params['create_date']}' AND w.merchant_id={$params['merchant_id']}";

		$cmd ="SELECT w.*,TIME_FORMAT(check_in_time, '%h:%i%p') as normal_time ,GROUP_CONCAT(p.name) as services, GROUP_CONCAT(p.id) as product_ids
		FROM waiting_list w 
		LEFT JOIN waiting_list_detail wd ON w.id=wd.waiting_list_header_id
		LEFT JOIN product p ON wd.product_id=p.id 
		WHERE  w.create_date='{$params['create_date']}' AND w.merchant_id={$params['merchant_id']}";
		
		if(!$params['is_display_all_status']) //if false
		{
			$cmd .= " AND w.status='WAITING'";
		}
	
		$cmd .=" GROUP BY w.id";
		// print_r($cmd);
		// die();

		$results = $this->db->fetchAll($cmd);

		
		
		$results_array ="";
		
		foreach($results as $r)
		{
			$current_time = new DateTime('NOW');
			$time_in = new DateTime($r['check_in_time']);
			$since_start = $current_time->diff($time_in);
			$r['waiting_time'] = $since_start->format('%h hour(s) %i minute(s)');
			$results_array[] = $r;
		}
		
		return $this->success($results_array);	
	}
	
	//id, merchant_id ,status = SEATED or CANCELLED, update_date, update_by, product_id, specialist
	public function update_waiting_status($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		$update_data = array(
			'status' => $params['status'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'update_time' => $params['update_time'],
		);
		
		if(!$this->db->update('waiting_list', $update_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating waiting list '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update waiting list.");
        }  

        $this->db->query("DELETE FROM waiting_list_detail WHERE waiting_list_header_id={$params['id']}");
		
		//$reservation_id = $this->db->lastInsertId();
		$reservation_id = $params['id'];
		$products = explode(",",$params['product_id']);

		$total_minutes = 0;

		//$this->db->query("DELETE FROM waiting_list_sale_person WHERE waiting_list_header_id={$params['id']}");

		$sale_persons = trim($params['sale_person_id']);

		if($sale_persons != "")
		{
			$sale_persons = explode(",",$sale_persons);
			foreach ($sale_persons as $s)
			{
				$sale_id = $s;
				
				$insert_data = array(
					'waiting_list_header_id' => $reservation_id,
					'sale_person_id' => $sale_id,
					'create_date' => $params['update_date'],
					'create_by' => $params['update_by'],
				);
				
				if(!$this->db->insert('waiting_list_sale_person', $insert_data))
				{
					$this->db->rollback_transaction();
					return $this->failed("Unable to update waiting list.");
				}
				
			}
		}

		
		foreach ($products as $p)
		{
			$prod = $this->get_product_info($p);
			
			$price = $prod['price'];
			$minutes = $prod['minutes_of_product'];
			

			$insert_data = array(
				'waiting_list_header_id' => $reservation_id,
				'product_id' => $p,
				'price' => $price,
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'minutes_of_product' => $minutes,
				'status' => 'A',
			);
			
			if(!$this->db->insert('waiting_list_detail', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation.");
			}
		}
		
		$this->db->commit_transaction();
		return $this->success("Waiting list has been updated.");
	}
	
	
	//params: username, password
    public function login_customer($params) {
        $params = array_map('trim', $params);

        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
			 // print_r("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
			 // die();
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
        	
			// print_r($user_check);
			// die();
			
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				// $profile = '';
				// if($user_check['is_iso'])
				// {
					// $profile = 'iso';
				// }elseif($user_check['is_merchant']){
					// $profile = 'merchants';
				// }elseif ($user_check['is_agent']){
					// $profile = 'agents';
				// }else{
					// $profile = 'customers';
				// }				
				
				$id = $user_check['reference_id'];
				// print_r($id);
				// die();
				// $user_info = $this->get_profile($profile, $id);
				$user_info = array();
				
				//$user_info['profile'] = $profile;
				
				$user_info['user_id'] = $user_check['id'];
				$user_info['partner_id'] = $id;
				$user_info['is_agent'] = $user_check['is_agent'];
				$user_info['user_type'] = $user_check['user_type_id'];
                $user_info['user_type_desc'] = $user_check['user_type_desc'];
				$user_info['username'] = $params['username'];
				// $user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
				$customer_info = $this->db->fetchRow("SELECT * FROM customer WHERE id={$id} AND status='A'");
				
				if(count($customer_info) ==0)
				{
					return $this->failed("Unable to find customer");
				}
				
				
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                    'customer_information' => $customer_info,
                );
                
                return $this->success($response);
            }
        }
    }
	
	//username, password, email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender
	public function register_customer($params)
	{
		$this->db->begin_transaction();
		
		$cmd="SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' AND status='A'";
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Mobile number has already been registered.");
		}
		
		
		$insert_data = array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'middle_name' => isset($params['middle_name']) ? $params['middle_name'] : '',
			'email' => isset($params['email']) ? $params['email'] : '',
			'mobile_number' => $params['contact_number'],
			'city' => isset($params['city']) ? $params['city'] : '',
			'state' => isset($params['state']) ? $params['state'] : '',
			'zip' => isset($params['zip']) ? $params['zip'] : '',
			'country' => isset($params['country']) ? $params['country'] : '',
			'birth_date' => isset($params['birth_date']) ? $params['birth_date'] : '',
			'gender' => isset($params['gender']) ? $params['gender'] : '',
			'address' => isset($params['address']) ? $params['address'] : '',
		);
		
		if(!$this->db->insert('customer', $insert_data)) {
            return $this->failed("Unable to add customer.");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
			
            if($params['username'] != '')
            {
    			//user_type_id =3 for customers
    			$cmd ="SELECT id FROM users WHERE username='{$params['username']}' AND status='A' LIMIT 1";
    			$results = $this->db->fetchRow($cmd);
    			if(count($results)>0)
    			{
    				$this->db->rollback_transaction();
    				return $this->failed("Username has already been use");
    			}
            }
			
			$params['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));  
        
			$insert = array(
				'username' => $params['username'],
				'password' => $params['password'],
				'last_name' => $params['last_name'],
				'first_name' => $params['first_name'],
				'email_address' => isset($params['email']) ? $params['email'] : '',
				'user_type_id' => 3,
				'reference_id' => $id,
				'status' => 'A',
			);
			
			
	        if (!$this->db->insert('users', $insert)) {
	            $this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
	            $this->save_to_action_logs('Error creating new customer'  ,'ERROR',$datetime,$_SESSION['username'],'');
	            return $this->failed("Unable to create new user.");
	            die();
	        }  
			
		}
		
		$this->db->commit_transaction();
		return $this->success("Customer has been registered.");
	}
	
	//merchant_id, service_id, date, time
	public function get_specialist_for_reservation($params)
	{
        //added AND status NOT IN ('CANCELLED', 'DONE') -- Alshey
		// $merchant_id = $params['merchant_id'];
		$cmd ="SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status FROM sale_person sp 
		LEFT JOIN 
		(SELECT DISTINCT s.* FROM 
		reservation_header h
		INNER JOIN reservation_sale_person s ON h.id=reservation_header_id
		WHERE h.reservation_status_id<>5
        AND status NOT IN ('CANCELLED', 'DONE') 
		AND reservation_time>='{$params['reservation_time']}'
		AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) )
		AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d
		ON sp.id=d.id ";
		
		$cmd .=" LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
		
		//add 1 hour as a buffer

		$day = date('l', strtotime($params['reservation_date']));
		$cmd .=" WHERE sp.schedule LIKE '%{$day}%'";
		$cmd .=" AND sp.merchant_id={$params['merchant_id']}";
		
		if($params['service_id'] > 0)
		{
			$cmd .=" AND ps.product_id={$params['service_id']}";
		}
		
		// print_r($cmd);
		// die();
// 		
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	public function get_product_categories($params)
	{
		$cmd ="SELECT * FROM product_category WHERE status='A' AND merchant_id ={$params['merchant_id']}";
		
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	//merchant_id, name, description, create_date, create_by
	public function create_product_category($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'name' => $params['name'],
			'description' => $params['description'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
		);
		
		if (!$this->db->insert('product_category', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new product category '  ,'ERROR',$datetime,$params['create_by'],'');
            return $this->failed("Unable to create product category.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Product category has been created.");
	}
	
	
	//id, merchant_id, name, description, update_date, update_by
	public function update_product_category($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'name' => $params['name'],
			'description' => $params['description'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
		);
		
		if(!$this->db->update('product_category', $insert_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error update product category '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update product category.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Product category has been updated.");
	}
	
	//id, merchant_id
	public function delete_product_category($params)
	{
		$this->db->begin_transaction();
		$cmd ="SELECT id FROM product WHERE category_id={$params['id']} AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Unable to delete category. Category has already been used.");
		}
		
		$cmd ="SELECT id FROM product_category WHERE id={$params['id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Unable to delete category. Id cannot be found.");
		}
		
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'status' => "D",
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
		);
		
		if(!$this->db->update('product_category', $insert_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error deleting product category '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to delete product category.");
        }  
		
		
		$this->db->commit_transaction();
		return $this->success("Product category has been deleted.");
		
	}
	
	
	public function get_merchant_profile($params)
	{
		$cmd ="SELECT m.*,mc.*  FROM  merchant m 
		LEFT JOIN merchant_contact mc on m.id=mc.merchant_id 
		LEFT JOIN merchant_mailing_address mm on m.id=mm.merchant_id  WHERE m.id={$params['merchant_id']}";
		
		
		// $cmd ="SELECT * FROM merchant WHERE id={$params['merchant_id']}";
		$merch = $this->db->fetchRow($cmd);
		
		if(count($merch)==0)
		{
			return $this->failed("Unable to find merchant.");
		}
		
		//get services
		// $cmd ="SELECT * FROM product_category WHERE status='A' AND merchant_id={$params['merchant_id']}";
		// $res = $this->db->fetchAll($cmd);
		// $merch['service_category'] = $res;
// 		
		// $cmd ="SELECT * FROM product WHERE status='A' AND merchant_id={$params['merchant_id']}";
		// $res = $this->db->fetchAll($cmd);
		// $merch['services'] = $res;
		
		// $cmd = "SELECT sp.id, sp.last_name, sp.first_name, GROUP_CONCAT(CASE WHEN p.name IS NULL THEN 'NA' ELSE p.name END) as services FROM sale_person sp 
// LEFT JOIN product_sale_person psp ON sp.id=psp.sale_person_id
// LEFT JOIN product p ON psp.product_id=p.id
 // WHERE sp.status='A' AND sp.merchant_id=52
 // GROUP BY sp.id, sp.last_name, sp.first_name";
		// $res = $this->db->fetchAll($cmd);
		// $merch['specialist'] = $res;
		
		
		//get business hours
		$cmd ="SELECT TIME_FORMAT(start_time, '%h:%i %p') as start_time, TIME_FORMAT(end_time, '%h:%i %p') as end_time,
		day, is_close, merchant_id, id 
		FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['business_hours'] = $res;
		
		// $cmd =" SELECT comment, r.create_date, c.first_name, c.last_name FROM merchant_review r INNER JOIN customer c ON r.customer_id=c.id  WHERE r.status='A' AND r.merchant_id={$params['merchant_id']}";
		// $res = $this->db->fetchAll($cmd);
		// $merch['reviews'] = $res;
		
		$results =$this->db->fetchRow($cmd);
		return $this->success($merch);
	}
	
	//merchant
	//id
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status
	//logo, description
	
	//merchant_contact
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//merchant_mailing_address
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	
	public function update_merchant_profile($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'business_name' => $params['business_name'],
			'dba' => $params['dba'],
			'processor' => $params['processor'],
			'mid' => $params['mid'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'fax' => $params['fax'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			
			'description' => $params['description'],
			'longitude' => $params['longitude'],
			'latitude' => $params['latitude'],
            'caption' => $params['caption']
		);
		
		if($params['logo'] !="")
		{
			$insert_data['logo'] = $params['logo'];
		}
		
		$merchant_id = $params['id'];
		if(!$this->db->update('merchant', $insert_data, 'id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant. 1");
		}
		
		$insert_data = array(
			'first_name' => $params['first_name'], 
			'last_name' => $params['last_name'],
			'position' => $params['position'],
			'mobile_number' => $params['mobile_number'],
			'business_phone1' => $params['business_phone1'],
			'business_phone2' => $params['business_phone2'],
			'extension' => $params['extension'],
			'fax' => $params['contact_fax'],
			'email' => $params['contact_email'],
			'website' => $params['website'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			// 'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->update('merchant_contact', $insert_data, 'merchant_id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant. 2");
		}
		
		
		// mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
		$insert_data = array(
			'country' => $params['mailing_country'],
			'address' => $params['mailing_address'],
			'city' => $params['mailing_city'],
			'state' => $params['mailing_state'],
			'zip' => $params['mailing_zip'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			// 'merchant_id' => $merchant_id,
		);
		
		// if(!$this->db->insert('merchant_mailing_address', $insert_data))
		if(!$this->db->update('merchant_mailing_address', $insert_data, 'merchant_id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant. 3");
		}
		
		
		$cmd ="SELECT id FROM merchant_business_hours WHERE merchant_id={$merchant_id} AND day='MONDAY'";
		$res = $this->db->fetchRow($cmd);
		
		if(count($res)>0)
		{
			//update
			// $update_query = "UPDATE merchant_business_hours"
			$update_data = array(
				'is_close' => $params['monday_is_close'],
				'start_time' => $params['monday_start_time'],
				'end_time' => $params['monday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'id='. $res['id']))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 4");
			}
			
			$update_data = array(
				'is_close' => $params['tuesday_is_close'],
				'start_time' => $params['tuesday_start_time'],
				'end_time' => $params['tuesday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='TUESDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 5");
			}
			
			$update_data = array(
				'is_close' => $params['wednesday_is_close'],
				'start_time' => $params['wednesday_start_time'],
				'end_time' => $params['wednesday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='WEDNESDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 6");
			}
			
			$update_data = array(
				'is_close' => $params['thursday_is_close'],
				'start_time' => $params['thursday_start_time'],
				'end_time' => $params['thursday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='THURSDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 7");
			}
			
			$update_data = array(
				'is_close' => $params['friday_is_close'],
				'start_time' => $params['friday_start_time'],
				'end_time' => $params['friday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='FRIDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 8");
			}
			
			$update_data = array(
				'is_close' => $params['saturday_is_close'],
				'start_time' => $params['saturday_start_time'],
				'end_time' => $params['saturday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='SATURDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 9");
			}
			$update_data = array(
				'is_close' => $params['sunday_is_close'],
				'start_time' => $params['sunday_start_time'],
				'end_time' => $params['sunday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='SUNDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant. 10	");
			}
		}
		else{
			//insert
			$insert_data = array(
				'day' => 'MONDAY',
				'is_close' => $params['monday_is_close'],
				'start_time' => $params['monday_start_time'],
				'end_time' => $params['monday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}
			
			$insert_data = array(
				'day' => 'TUESDAY',
				'is_close' => $params['tuesday_is_close'],
				'start_time' => $params['tuesday_start_time'],
				'end_time' => $params['tuesday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}    
			$insert_data = array(
				'day' => 'WEDNESDAY',
				'is_close' => $params['wednesday_is_close'],
				'start_time' => $params['wednesday_start_time'],
				'end_time' => $params['wednesday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}  
			$insert_data = array(
				'day' => 'THURSDAY',
				'is_close' => $params['thursday_is_close'],
				'start_time' => $params['thursday_start_time'],
				'end_time' => $params['thursday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}  
			$insert_data = array(
				'day' => 'FRIDAY',
				'is_close' => $params['friday_is_close'],
				'start_time' => $params['friday_start_time'],
				'end_time' => $params['friday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}
			$insert_data = array(
				'day' => 'SATURDAY',
				'is_close' => $params['saturday_is_close'],
				'start_time' => $params['saturday_start_time'],
				'end_time' => $params['saturday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	} 
			$insert_data = array(
				'day' => 'SUNDAY',
				'is_close' => $params['sunday_is_close'],
				'start_time' => $params['sunday_start_time'],
				'end_time' => $params['sunday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	} 
		}
		
		
		
		
		// $user_name = $this->generate_username();
		// $password = rand(100000, 999999);
// 		
		// //username , password, last_name, first_name, email_address, user_type_id, reference_id
		// $user_insert = array(
			// 'last_name' => $params['last_name'],
			// 'first_name' => $params['first_name'],
			// 'email_address' => $params['email'],
			// 'update_date' => $params['update_date'],
			// 'update_by' => $params['update_by'],
		// );
// 		
// 		
		// // if(!$this->db->insert('merchant_mailing_address', $insert_data))
		// if(!$this->db->update('users', $user_insert, 'reference_id='. $merchant_id. " AND email_address='{$params['email']}'"))
		// {
			// $this->db->rollback_transaction();
			// return $this->failed("Unable to update merchant. 11");
		// }
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant has been updated.");
	}
	
	
	//merchant_id
	public function get_total_analytics($params)
	{
		$cmd=" SELECT CASE WHEN rs.name  IS NULL THEN 'New' ELSE rs.name END as name, count(rh.id) AS total FROM reservation_header rh 
LEFT JOIN reservation_status rs on rh.reservation_status_id=rs.id
WHERE rh.merchant_id={$params['merchant_id']}
GROUP BY rs.name
		";	
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);	
	}
	
	//merchant_id, mobile
	public function search_customer_by_mobile($params)
	{
		$cmd ="SELECT last_name, first_name, mobile, email_address FROM reservation_header WHERE merchant_id={$params['merchant_id']}";
		$cmd .=" AND mobile =". $this->db->db_escape($params['mobile']) ." LIMIT 1";
		
		$result = $this->db->fetchRow($cmd);	
		
		if(isset($result['last_name']))
		{
			return $this->success($result);
		}else{
			return $this->failed("Unable to find mobile.");
		}	
	}
	
	//merchant_id, date, branch_id, 
	//merchant_id, date, services
	public function get_salon_specialist_per_merchant_per_day($params)
	{
		// $merchant_id = $params['merchant_id'];
		
		// $cmd = "SELECT sp.*, mb.branch_name FROM sale_person sp LEFT JOIN 
		// merchant_branch mb ON sp.branch_id=mb.id WHERE sp.status='A' AND sp.merchant_id={$params['merchant_id']}";

		$cmd = "SELECT DISTINCT sp.* FROM sale_person sp ";

		$cmd .=" LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";

        $cmd .="LEFT JOIN sale_person_schedule sch ON sp.id = sch.sales_person_id "; /*added*/

		$cmd .= "WHERE sp.status='A' AND sp.merchant_id={$params['merchant_id']} ";
		
		// if($params['branch_id'] !== -1)
		// {
		// 	$cmd .=" AND mb.id={$params['branch_id']}";	
		// }

		if($params['services'] !="")
		{
			$cmd .= " AND ps.product_id IN({$params['services']})";
		}

		
		$day = date('l', strtotime($params['date']));
		/*$cmd .=" AND sp.schedule LIKE '%{$day}%'";*/
        $cmd .=" AND sch.day LIKE '%{$day}%' AND sch.is_off = 0";

		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}

    public function get_schedule_per_specialist_per_day($params) {
        $day = date('l', strtotime($params['date']));

        $cmd = "SELECT * FROM sale_person_schedule WHERE sales_person_id = {$params['sales_person_id']}";
        $cmd .=" AND day LIKE '%{$day}%'";
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
	
	public function get_countries()
	{
		$cmd = "SELECT id,name, iso_code_2, iso_code_3 FROM country";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//merchant_id
	public function get_merchant_users($params)
	{
		$cmd ="SELECT * FROM users WHERE user_type_id=8 AND status='A' AND reference_id={$params['merchant_id']}";
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	//user_id, last_name, first_name, email_address, update_date, update_by
	public function update_user_information($params)
	{
		$cmd = " SELECT id FROM users WHERE id<>{$params['user_id']} AND email_address='{$params['email_address']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)>0)
		{
			return $this->failed("Email has already been use.");
		}
		
		$update_data =array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email_address'],
            'position' => $params['position'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
 		);
		
		if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
            return $this->failed("Unable to update user information.");    
        } else {
            return $this->success("Account information has been updated");
        }
	}
	
	//user_id, password, new_password, update_date, update_by
	public function update_my_password($params)
	{
		//check if current_password, via user_id
		if($params['password']== $params['new_password'])
		{
			return $this->failed("Current password and new password cannot be the same.");
		}
		
		$current_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));
		$new_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_password'], sha1(md5($this->salt)));
		
		$cmd ="SELECT id FROM users WHERE id={$params['user_id']}";
		$cmd .= " AND password=".$this->db->db_escape($current_password);
		
		$rs = $this->db->fetchRow($cmd);
		if(count($rs)==0)
		{
			return $this->failed("Unable to update password. Invalid old password.");
		}
		
		$update_data = array(
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'password' => $new_password,
		);
		
		if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
            return $this->failed("Unable to update password.");    
        } else {
            return $this->success("Your account password has been updated.");
        }
	}
	
	//merchant_id, username, password, last_name, first_name, email_address, create_by, create_date, status
	public function merchant_create_user($params)
	{
		
		$cmd ="SELECT id FROM  users WHERE username=".$this->db->db_escape($params['username']) ." OR email_address=".$this->db->db_escape($params['email_address']);
		$cmd .=" LIMIT 1";
		
		
		$res = $this->db->fetchRow($cmd);
		
		if(count($res))
		{
			return $this->failed("Username/email address has already been used.");
		}
		
		//user_type_id = 8;
		$params['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        	
		$insert = array(
			'username' => $params['username'],
			'password' => $params['password'],
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email_address'],
            'position' => $params['position'],
			'user_type_id' => 8,
			'is_merchant_user' => 1,
			'reference_id' => $params['merchant_id'],
			'is_agent' => 0,
			'status' => "A",
		);
		
        if (!$this->db->insert('users', $insert)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
        }    
		
		return $this->success("New merchant user has been created."); 
	}
	
	//id from reservation list,reservation_status_id = just put -1 if the information will just be the one that needs to be update
	////merchant_id, reservation_date, reservation_time, create_date, create_by, status
	//branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited
	public function update_reservation($params)
    {
        $this->db->begin_transaction(); 
        $insert_data = array(
            'merchant_id' => $params['merchant_id'],
            'customer_id' => isset($params['customer_id']) ? $params['customer_id'] : -1,
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'mobile' => $params['mobile'],
            'notes' => $params['notes'],
            'reservation_date' => $params['reservation_date'],
            'reservation_time' => $params['reservation_time'],
            'update_date' => $params['update_date'],
            'update_by' => $params['update_by'],
            //'status' => "NEW",
            'reservation_status_id' => $params['reservation_status_id'],
            'sale_person_ids' => $params['sale_person_id'],
            'branch_id' => $params['branch_id'],

            //LC-03-08-2017
            'email_address' => $params['email_address'],
        );
        

        //check if the selected services is being catered by the selected specialist
        $products = explode(",",$params['product_id']);
        $sale_persons = trim($params['sale_person_id']);

        foreach($products as $p)
        {
            $sale_persons = trim($params['sale_person_id']);
            $product_id =$p;
            if($sale_persons != "")
            {

                $sale_persons = explode(",", $sale_persons);
                foreach ($sale_persons as $s)
                {
                    $sp_id = $s;
                    $cmd ="SELECT id FROM product_sale_person WHERE product_id={$product_id} AND sale_person_id = {$sp_id} LIMIT 1";
                    $rsp = $this->db->fetchRow($cmd);
                    if(count($rsp) == 0)
                    {
                        //return $this->failed($cmd);
                        return $this->failed("Selected specialist is not capable of the service.");
                    }
                }
            }
        }

        $day = date('l', strtotime($params['reservation_date']));

        //added
        //$cmd .= "AND status NOT IN ('CANCELLED', 'DONE')";

                //check if there is available specialist on the said service
        $cmd ="SELECT * FROM (SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status ";  
        $cmd .= "FROM sale_person sp LEFT JOIN "; 
        $cmd .= "sale_person_schedule sch ON sp.id = sch.sales_person_id LEFT JOIN ";
        $cmd .= "(SELECT DISTINCT s.* FROM reservation_header h INNER JOIN reservation_sale_person s ON h.id=reservation_header_id "; 
       /* $cmd .= "WHERE h.reservation_status_id<>5 ";
        $cmd .= "AND status NOT IN ('CANCELLED', 'DONE') ";
        $cmd .= "AND reservation_time>='{$params['reservation_time']}' 
        AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) ";*/
        $cmd .= "WHERE h.reservation_status_id<>5 AND reservation_time>='{$params['reservation_time']}' 
        AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) "; 
        
        $cmd .= "AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d ON sp.id=d.sale_person_id  ";
        $cmd .= "LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
        $cmd .= "WHERE sch.day LIKE '%{$day}%' AND sp.merchant_id={$params['merchant_id']} AND ps.product_id IN({$params['product_id']})) as dt 
        WHERE current_status='AVAILABLE'";
        

        $sale_persons = trim($params['sale_person_id']);
        // if($sale_persons != "")
        // {
        //  $cmd .=" AND dt.id  IN({$sale_persons})";
        // }


        $rs = $this->db->fetchAll($cmd);
        if(count($rs)==0)
        {
            return $this->failed("Unable to update reservation. Specialist is already booked.");
        }


        // if($params['reservation_status_id'] > 0)
        // {
            // $insert_data['reservation_status_id'] = $params['reservation_status_id'];
        // }
        
        if(!$this->db->update('reservation_header', $insert_data, "id=". $params['id']))
        {
            $this->db->rollback_transaction();
            return $this->failed("Unable to update reservation.");
        }
        
        $this->db->query("DELETE FROM reservation_detail WHERE reservation_header_id={$params['id']}");
        
        //$reservation_id = $this->db->lastInsertId();
        $reservation_id = $params['id'];
        $products = explode(",",$params['product_id']);

        $total_minutes = 0;

        $this->db->query("DELETE FROM reservation_sale_person WHERE reservation_header_id={$params['id']}");

        $sale_persons = trim($params['sale_person_id']);

        if($sale_persons != "")
        {
            $sale_persons = explode(",",$sale_persons);
            foreach ($sale_persons as $s)
            {
                $sale_id = $s;
                
                $insert_data = array(
                    'reservation_header_id' => $reservation_id,
                    'sale_person_id' => $sale_id,
                    'create_date' => $params['update_date'],
                    'create_by' => $params['update_by'],
                );
                
                if(!$this->db->insert('reservation_sale_person', $insert_data))
                {
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to update reservation.");
                }
                
            }
        }

        
        foreach ($products as $p)
        {
            $prod = $this->get_product_info($p);
            
            $price = $prod['price'];
            $minutes = $prod['minutes_of_product'];
            
            $total_minutes += $minutes;

            $insert_data = array(
                'reservation_header_id' => $reservation_id,
                'product_id' => $p,
                'price' => $price,
                'create_date' => $params['update_date'],
                'create_by' => $params['update_by'],
                'sales_person_id' => -1,
                'minutes_of_product' => $minutes,
                'status' => 'A',
            );
            
            if(!$this->db->insert('reservation_detail', $insert_data))
            {
                $this->db->rollback_transaction();
                return $this->failed("Unable to update reservation.");
            }
        }

        if($total_minutes > 0)
        {
            if(!$this->db->update('reservation_header', $data = array('duration'=> $total_minutes, 'update_date' => date('Y-m-d h:i:s')), 'id='.$reservation_id))
            {
                $this->db->rollback_transaction();
                return $this->failed("Unable to update reservation. 1");
            }
        }

        $this->db->commit_transaction();
        
        return $this->success("Reservation has been updated.");
    }

    public function update_reservation2($params)
    {
        $this->db->begin_transaction(); 
        $insert_data = array(
            'merchant_id' => $params['merchant_id'],
            'customer_id' => isset($params['customer_id']) ? $params['customer_id'] : -1,
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'mobile' => $params['mobile'],
            'notes' => $params['notes'],
            'reservation_date' => $params['reservation_date'],
            'reservation_time' => $params['reservation_time'],
            'update_date' => $params['update_date'],
            'update_by' => $params['update_by'],
            //'status' => "NEW",
            'reservation_status_id' => $params['reservation_status_id'],
            'sale_person_ids' => $params['sale_person_id'],
            'branch_id' => $params['branch_id'],
            'email_address' => $params['email_address']
        );

        //check if the selected services is being catered by the selected specialist
        $products = explode(",",$params['product_id']);
        $sale_persons = trim($params['sale_person_id']);

        foreach($products as $p)
        {
            $sale_persons = trim($params['sale_person_id']);
            $product_id =$p;
            if($sale_persons != "")
            {

                $sale_persons = explode(",", $sale_persons);
                foreach ($sale_persons as $s)
                {
                    $sp_id = $s;
                    $cmd ="SELECT id FROM product_sale_person WHERE product_id={$product_id} AND sale_person_id = {$sp_id} LIMIT 1";
                    $rsp = $this->db->fetchRow($cmd);
                    if(count($rsp) == 0)
                    {

                        return $this->failed("Selected specialist is not capable of the service.");
                    }
                }
            }
        }

        $day = date('l', strtotime($params['reservation_date']));

        //added
        //$cmd .= "AND status NOT IN ('CANCELLED', 'DONE')";

        //check if there is available specialist on the said service
        $cmd ="SELECT * FROM (SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status ";  
        $cmd .= "FROM sale_person sp LEFT JOIN "; 
        $cmd .= "sale_person_schedule sch ON sp.id = sch.sales_person_id LEFT JOIN ";
        $cmd .= "(SELECT DISTINCT s.* FROM reservation_header h INNER JOIN reservation_sale_person s ON h.id=reservation_header_id "; 
        /*$cmd .= "WHERE h.reservation_status_id<>5 ";
        $cmd .= "AND status NOT IN ('CANCELLED', 'DONE') ";
        $cmd .= "AND reservation_time>='{$params['reservation_time']}' AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) ";*/
        $cmd .= "WHERE h.reservation_status_id<>5 AND reservation_time>='{$params['reservation_time']}' AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) "; 
        
        $cmd .= "AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d ON sp.id=d.sale_person_id  ";
        $cmd .= "LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
        $cmd .= "WHERE sch.day LIKE '%{$day}%' AND sp.merchant_id={$params['merchant_id']} AND ps.product_id IN({$params['product_id']})) as dt WHERE current_status='AVAILABLE'";

        return $this->failed($cmd);



        $sale_persons = trim($params['sale_person_id']);
        /*if($sale_persons != "")
        {
            $cmd .=" AND dt.id  IN({$sale_persons})";
        }*/

       /* return $this->failed($cmd);*/


        $rs = $this->db->fetchAll($cmd);
        
        /*return $this->failed(count($rs));die;*/
        if(count($rs)==0)
        {
            return $this->failed("Unable to update reservation. Specialist is already booked.");
        }


        // if($params['reservation_status_id'] > 0)
        // {
            // $insert_data['reservation_status_id'] = $params['reservation_status_id'];
        // }
        
        if(!$this->db->update('reservation_header', $insert_data, "id=". $params['id']))
        {
            $this->db->rollback_transaction();
            return $this->failed("Unable to update reservation.");
        }
        
        $this->db->query("DELETE FROM reservation_detail WHERE reservation_header_id={$params['id']}");
        
        //$reservation_id = $this->db->lastInsertId();
        $reservation_id = $params['id'];
        $products = explode(",",$params['product_id']);

        $total_minutes = 0;

        $this->db->query("DELETE FROM reservation_sale_person WHERE reservation_header_id={$params['id']}");

        $sale_persons = trim($params['sale_person_id']);

        if($sale_persons != "")
        {
            $sale_persons = explode(",",$sale_persons);
            foreach ($sale_persons as $s)
            {
                $sale_id = $s;
                
                $insert_data = array(
                    'reservation_header_id' => $reservation_id,
                    'sale_person_id' => $sale_id,
                    'create_date' => $params['update_date'],
                    'create_by' => $params['update_by'],
                );
                
                if(!$this->db->insert('reservation_sale_person', $insert_data))
                {
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to update reservation.");
                }
                
            }
        }

        
        foreach ($products as $p)
        {
            $prod = $this->get_product_info($p);
            
            $price = $prod['price'];
            $minutes = $prod['minutes_of_product'];
            
            $total_minutes += $minutes;

            $insert_data = array(
                'reservation_header_id' => $reservation_id,
                'product_id' => $p,
                'price' => $price,
                'create_date' => $params['update_date'],
                'create_by' => $params['update_by'],
                'sales_person_id' => -1,
                'minutes_of_product' => $minutes,
                'status' => 'A',
            );
            
            if(!$this->db->insert('reservation_detail', $insert_data))
            {
                $this->db->rollback_transaction();
                return $this->failed("Unable to update reservation.");
            }
        }

        if($total_minutes > 0)
        {
            if(!$this->db->update('reservation_header', $data = array('duration'=> $total_minutes, 'update_date' => date('Y-m-d h:i:s')), 'id='.$reservation_id))
            {
                $this->db->rollback_transaction();
                return $this->failed("Unable to update reservation. 1");
            }
        }

        $this->db->commit_transaction();
        
        return $this->success("Reservation has been updated.");
    }
	
	//merchant_id, date
	public function get_reservations_per_merchant($params)
	{

		// $cmd =" SELECT rh.*, rs.color FROM reservation_header rh
		// LEFT JOIN reservation_status rs ON rh.reservation_status_id=rs.id 
		// WHERE rh.create_date>='{$params['date']}' AND rh.merchant_id={$params['merchant_id']} ORDER BY create_date DESC";
		
		// $cmd =" SELECT TIME_FORMAT(rh.reservation_time, '%h:%i%p') as normal_time, rh.*, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		// LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		// LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id={$params['merchant_id']} 
		// WHERE rh.reservation_date>='{$params['date']}' AND rh.merchant_id={$params['merchant_id']} ORDER BY create_date DESC";
		
		$cmd =" SELECT TIME_FORMAT(rh.reservation_time, '%h:%i%p') as normal_time, rh.*, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id={$params['merchant_id']} 
		WHERE rh.reservation_date='{$params['date']}' AND rh.merchant_id={$params['merchant_id']}";
		
		// WHERE rh.create_date>='{$params['date']}' AND rh.merchant_id={$params['merchant_id']} ORDER BY create_date DESC
		// print_r($cmd);
		// die();

        if ($params['iscancelled'] == true) {
            $cmd .= " AND rh.status != 'CANCELLED'";
        } 

        $cmd .= " ORDER BY create_date DESC";

		$results = $this->db->fetchAll($cmd);
		
		$reservations = array();
		foreach ($results as $r)
		{
			$cmd ="SELECT rd.*, p.id as product_id,p.name as product_name FROM reservation_detail rd LEFT JOIN product p ON rd.product_id=p.id WHERE p.status='A' ";
			$cmd .=" AND rd.reservation_header_id={$r['id']}";
			$details = $this->db->fetchAll($cmd);
			
			$product_id = "";
			foreach($details as $d)
			{
				$product_id .= $d['product_id'] .",";
			}
			
			
			
			$r['product_ids'] = $product_id;
			$r['details'] = $details;
			
			$cmd =" SELECT sp.* FROM reservation_sale_person rsp LEFT JOIN sale_person sp ON rsp.sale_person_id =sp.id";
			$cmd .=" WHERE rsp.reservation_header_id={$r['id']}";

			// print_r($cmd);
			// die();
			$sales_person = $this->db->fetchAll($cmd);
			
			$r['sales_person'] = $sales_person;
			
			$reservations[] = $r;
		}
		return $this->success($reservations);
	}

    public function get_time_per_day($params) {

        $cmd = "SELECT start_time, end_time, is_close FROM merchant_business_hours 
                WHERE day ='{$params['day']}'
                AND merchant_id={$params['merchant_id']}";

        $results = $this->db->fetchRow($cmd);
        return $this->success($results);

    }

    public function get_business_hours_per_merchant($params) {

        $cmd ="SELECT day, start_time, end_time, is_close FROM merchant_business_hours
               WHERE merchant_id={$params['merchant_id']}";

        $results = $this->db->fetchAll($cmd);
        return $this->success($results);
    }
	
	
	//merchant_id
	public function get_reservation_status_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		//$cmd = "SELECT * FROM reservation_status WHERE status='A' AND merchant_id={$merchant_id}";
		
		$cmd ="SELECT s.id, CASE  WHEN rs.color IS NULL THEN s.color ELSE rs.color END as color, s.name, s.description
		FROM status_default s LEFT JOIN reservation_status rs ON s.id=rs.status_default_id AND rs.merchant_id={$merchant_id}";	
		
		
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	//merchant_id, name, description, color, create_date, create_by, status
	public function create_reservation_status($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('reservation_status', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create reservation status.");
		}
		
		$this->db->commit_transaction();
		return $this->success("Reservation status has been created.");
	}
	
	//merchant_id, name, description, color, create_date, create_by, status
	public function update_reservation_status($params)
	{
		$this->db->begin_transaction();
		$cmd ="SELECT id FROM reservation_status WHERE status_default_id={$params['id']} AND merchant_id={$params['merchant_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) >0)
		{
			$id = $params['id'];
			unset($params['id']);
			//if(!$this->db->update('reservation_status', $params, 'id='. $params['id']))
			if(!$this->db->update('reservation_status', $params, 'status_default_id='. $id. " AND merchant_id={$params['merchant_id']}"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation status.");
			}
		}else{
			$insert_data = array(
				'color' => $params['color'],
				'name' => $params['name'],
				'description' => $params['description'],
				'status_default_id' => $params['id'],
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'merchant_id' => $params['merchant_id']
			);
			if(!$this->db->insert('reservation_status', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation status.");
			}
		}
		$this->db->commit_transaction();
		return $this->success("Reservation status has been updated.");
	}
	
	
	//id, merchant_id, branch_id, last_name, first_name, middle_name, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time, services, contact_number, email_address
	public function update_salon_specialist($params)
	{
		$this->db->begin_transaction();
		$services = $params['services'];
		unset($params['services']);

        $schedule = $params['schedule'];
        unset($params['schedule']);
        $start_time = $params['start_time'];
        unset($params['start_time']);
        $end_time = $params['end_time'];
        unset($params['end_time']);

		if(!$this->db->update('sale_person', $params, 'id='.$params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update salon specialist.");
		}
		
		$sale_person_id = $params['id'];

        $days = array('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');

        $selected_days = explode(",", $schedule);
        
        $start_time = explode(",", $start_time);
        $end_time = explode(",", $end_time);

        foreach ($days as $key => $value) {
            $new_key = array_search($value, $selected_days);
            
            $schedule = array(
                'update_date'     => $params['update_date'],
                'update_by'       => $params['update_by']
            );

            if ($new_key !== false) {
                $schedule['is_off'] = 0;
                $schedule['start_time'] = $start_time[$new_key];
                $schedule['end_time'] = $end_time[$new_key];
            } else {
                $schedule['is_off'] = 1;
            }

            $check_query = "SELECT * FROM sale_person_schedule WHERE sales_person_id={$params['id']} AND day='{$value}'";
            $check_exist = $this->db->fetchRow($check_query);

            if (count($check_exist) > 0) {
                if(!$this->db->update('sale_person_schedule', $schedule, 'sales_person_id='.$params['id'].' AND day="'.$value.'"'))
                {
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to update salon specialist.");
                }
            } else {
                $schedule['day'] = $value;
                $schedule['create_date'] = $params['update_date'];
                $schedule['create_by'] = $params['update_by'];
                $schedule['sales_person_id'] = $params['id'];

                unset($schedule['update_date']);
                unset($schedule['update_by']);
                   
                if(!$this->db->insert('sale_person_schedule', $schedule))
                {
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to update salon specialist.");
                }
            }
        }

		$products = explode(",",$services);
		
		
		$this->db->query("DELETE FROM product_sale_person WHERE sale_person_id={$sale_person_id}");
		
		foreach($products as $p)
		{
			
			$prod = $this->get_product_info($p);
			
			if(count($prod) == 0)
			{
				return $this->failed("Unable to find product.");
			}
			
			
			$product = array(
				'product_id' => $prod['id'],
				'sale_person_id' => $sale_person_id,
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'merchant_id' => $params['merchant_id'],
			);
			if(!$this->db->insert('product_sale_person', $product))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create salon specialist.");
			}
			
		}
		
		
		
		$this->db->commit_transaction();
		return $this->success("Salon Specialist has been updated.");
	}
	
	
	
	//merchant_id, id, longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time,contact_number,
	//store_day, update_date, update_by
	public function update_merchant_branch($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->update('merchant_branch', $params, 'id='.$params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant branch.");
		}
		$this->db->commit_transaction();
		return $this->success("Merchant Branch has been updated.");
		
	}
	
	//id, merchant_id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//is_inventory, update_date, update_by, status
	public function update_product($params)
	{
		$update_data =  $params;
		$this->db->begin_transaction();
		if(!$this->db->update('product', $params, 'id='. $params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update product.");
		}
		$this->db->commit_transaction();
		return $this->success("Product has been updated.");
	}
	
	
	
	//merchant_id
	public function get_branch_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd = "SELECT * FROM merchant_branch WHERE status='A' AND merchant_id={$merchant_id}";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	//merchant_id, category_id
	public function get_products_per_merchant($params)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd = "SELECT p.*, c.name as category_name, c.description as category_description FROM product p LEFT JOIN  product_category c ON p.category_id=c.id WHERE p.status='A' AND p.merchant_id={$params['merchant_id']}";
		
		if($params['category_id']>0)
		{
			$cmd .=" AND category_id={$params['category_id']}";
		}
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}

    //with order
    public function get_products_per_merchant_order($params)
    {
        // $merchant_id = $params['merchant_id'];
        $cmd = "SELECT p.*, c.name as category_name, c.description as category_description FROM product p LEFT JOIN  product_category c ON p.category_id=c.id WHERE p.status='A' AND p.merchant_id={$params['merchant_id']}";
        
        if($params['category_id']>0)
        {
            $cmd .=" AND category_id={$params['category_id']}";
        }

        if ($params['condition'] !== "") {
            $cmd .= $params['condition'];
        }
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }

    public function search_customer_by_name_mobile($params)
    {
        $cmd ="SELECT r.first_name, r.last_name, r.mobile, r.reservation_date, r.reservation_time, r.status, r.notes, r.id, r.sale_person_ids, r.branch_id, r.reservation_status_id, r.email_address, r.merchant_id, s.first_name AS specialist_first_name
            FROM reservation_header r
            LEFT JOIN sale_person s
            ON s.id = r.sale_person_ids
            WHERE r.merchant_id={$params['merchant_id']} 
            AND (CONCAT(r.first_name, ' ', r.last_name) 
            LIKE '%{$params['search_value']}%' OR r.mobile LIKE '%{$params['search_value']}%')";

            if ($params['condition'] !== "") {
            $cmd .= $params['condition'];
        }

        $results = $this->db->fetchAll($cmd);
        
        $reservations = array();
        foreach ($results as $r)
        {
            $cmd ="SELECT rd.*, p.id as product_id,p.name as product_name FROM reservation_detail rd LEFT JOIN product p ON rd.product_id=p.id WHERE p.status='A' ";
            $cmd .=" AND rd.reservation_header_id={$r['id']}";
            $details = $this->db->fetchAll($cmd);
            
            $product_id = "";
            foreach($details as $d)
            {
                $product_id .= $d['product_id'] .",";
            }
            
            
            
            $r['product_ids'] = $product_id;
            $r['details'] = $details;
            
            $cmd =" SELECT sp.* FROM reservation_sale_person rsp LEFT JOIN sale_person sp ON rsp.sale_person_id =sp.id";
            $cmd .=" WHERE rsp.reservation_header_id={$r['id']}";

            // print_r($cmd);
            // die();
            $sales_person = $this->db->fetchAll($cmd);
            
            $r['sales_person'] = $sales_person;
            
            $reservations[] = $r;
        }
        return $this->success($reservations);

        /*$result = $this->db->fetchAll($cmd);

        return $this->success($result);*/
    }

    public function search_customer_waiting_reservation($params) {
        $cmd = "SELECT DISTINCT r.last_name, r.first_name, r.email_address FROM reservation_header r
                WHERE merchant_id = {$params['merchant_id']}
                AND r.mobile = '{$params['term']}'
                UNION
                SELECT DISTINCT w.last_name, w.first_name, w.email_address FROM waiting_list w
                WHERE merchant_id = {$params['merchant_id']}
                AND w.contact_number = '{$params['term']}'
                ";

        $results = $this->db->fetchAll($cmd);
        return $this->success($results);
    }
	
	
	//merchant_id
	public function get_salon_specialist_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		//$cmd = "SELECT sp.*, mb.branch_name FROM sale_person sp LEFT JOIN merchant_branch mb ON sp.branch_id=mb.id WHERE sp.status='A' AND sp.merchant_id={$merchant_id}";
		
		$cmd = "SELECT sp.* FROM sale_person sp WHERE sp.status='A' AND sp.merchant_id={$merchant_id}";
		
		$results = $this->db->fetchAll($cmd);
		
		$specialist = array();
		foreach ($results as $r)
		{
			$cmd ="SELECT p.* FROM product p LEFT JOIN product_sale_person  ps ON p.id=ps.product_id";
			$cmd .=" WHERE sale_person_id={$r['id']}";
			$details = $this->db->fetchAll($cmd);

            $cmd2 ="SELECT day, start_time, end_time, is_off FROM sale_person_schedule";
            $cmd2 .=" WHERE sales_person_id={$r['id']}";
            $details2 = $this->db->fetchAll($cmd2);
            
            $r['products'] = $details;
            $r['new_schedule'] = $details2;
            $r['encoded_sched'] = json_encode($details2);			
			$specialist[] = $r;
		}
		
		return $this->success($specialist);	
	}
	
	
	
	//merchant_id
	public function list_reservations_per_merchant()
	{
		$cmd =" SELECT * FROM reservation_heaer h LEFT JOIN reservation_detail d ON h.id=d.reservation_header_id";
		$cmd .= " WHERE h.status=''";
		
		return $this->db->fetchAll($cmd);
	}
	
	
	
	//merchant_id, reservation_date, reservation_time, create_date, create_by, status
	//branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited
	public function create_reservation_via_merchant($params)
	{
		//search for customer for later
		
		//if not exist add to customer
		$cmd =" SELECT id FROM customer WHERE mobile_number='{$params['mobile']}' ";
		
		$rs = $this->db->fetchRow($cmd);
		if(isset($rs['id']))
		{
			$params['customer_id'] = $rs['id'];
		}

        /*added timezone*/
        $this->get_time_zone($params);
        $timezone = $this->response['ResponseMessage']['timezone'];
        date_default_timezone_set($timezone);
		
		$day = date('l', strtotime($params['reservation_date']));
		
		$time =$params['reservation_time'];
		$u_day = strtoupper($day);

		$cmd ="SELECT id, start_time, end_time FROM merchant_business_hours mb WHERE merchant_id='{$params['merchant_id']}' AND TIME_TO_SEC('{$time}') >= TIME_TO_SEC(start_time) AND
		TIME_TO_SEC(end_time) >= TIME_TO_SEC('{$time}')
		AND is_close=0 AND day='{$u_day}'";

		/*$result = $this->db->fetchAll($cmd);*/
        $result = $this->db->fetchRow($cmd);

        $business_end_time = $result['end_time'];

		if(count($result) == 0)
		{
			return $this->failed("Store is close on your selected date/time.");
		}

		//check if the selected services is being catered by the selected specialist
		$products = explode(",",$params['product_id']);
		$sale_persons = trim($params['sale_person_id']);


		$sale_persons = explode(",",$sale_persons);

		// print_r($sale_persons);
		// die();
        $service_total_minutes = 0;
		foreach($products as $p)
		{
			$product_id =$p;

            $service_query  = "SELECT minutes_of_product FROM product WHERE id = {$product_id}";
            $service_info   = $this->db->fetchRow($service_query);
            $service_total_minutes += $service_info['minutes_of_product'];

			if(trim($params['sale_person_id']) != "")
			{
				foreach ($sale_persons as $s)
				{

					$sp_id = $s;
					$cmd ="SELECT id FROM product_sale_person WHERE product_id={$product_id} AND sale_person_id = {$sp_id} LIMIT 1";

					$rsp = $this->db->fetchRow($cmd);
					if(count($rsp) == 0)
					{
						return $this->failed("Selected specialist is not capable of the services you selected.");
					}
				}
			}
		}

        $str_reservation_time = strtotime($params['reservation_time']);

        $end_reservation_time = date("H:i", strtotime('+'.intval($service_total_minutes).' minutes', $str_reservation_time));

        if (new DateTime($end_reservation_time) > new DateTime($business_end_time)) {
            return $this->failed("Store is closed based on total time of the service/s.");
        }
		// print_r($products);
		// die();

        // added AND status NOT IN ('CANCELLED', 'DONE') -- Alshey

		//check if there is available specialist on the said service
        $cmd ="SELECT * FROM (SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status ";  
        $cmd .= "FROM sale_person sp LEFT JOIN "; 
        $cmd .= "sale_person_schedule sch ON sp.id = sch.sales_person_id  AND sch.is_off=0 LEFT JOIN
";
        $cmd .= "(SELECT DISTINCT s.* FROM reservation_header h INNER JOIN reservation_sale_person s ON h.id=reservation_header_id "; 
        $cmd .= "WHERE h.reservation_status_id<>5 ";
        $cmd .= "AND status NOT IN ('CANCELLED', 'DONE') ";
        $cmd .= "AND reservation_time>='{$params['reservation_time']}' 
        AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) ";
        /*$cmd .= "WHERE h.reservation_status_id<>5 AND reservation_time>='{$params['reservation_time']}' 
        AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) "; */
        
        $cmd .= "AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d ON sp.id=d.sale_person_id  ";
        $cmd .= "LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
        $cmd .= "WHERE sch.day LIKE '%{$day}%' AND sp.merchant_id={$params['merchant_id']} AND ps.product_id IN({$params['product_id']})) as dt 
        WHERE current_status='AVAILABLE'";

        

        $sale_persons = trim($params['sale_person_id']);
        if($sale_persons != "")
        {
            $cmd .=" AND dt.id  IN({$sale_persons})";
        }

		$rs = $this->db->fetchAll($cmd);
		if(count($rs)==0)
		{
            if (!isset($params['is_market'])) {
                return $this->failed("Unable to reserve. No more available technicians.");
            }
		}
		
		//check if select specialist is available
		$sale_persons = trim($params['sale_person_id']);
		// if($sale_persons != "")
		// {
		// 	$cmd .= " AND id IN({$sale_persons}) ";
		// 	$rs = $this->db->fetchAll($cmd);
		// 	if(count($rs)==0)
		// 	{
		// 		return $this->failed("Unable to reserve. You choice of technician is not available.");
		// 	}		
		// }
		
		$this->db->begin_transaction();	
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'customer_id' => isset($params['customer_id']) ? $params['customer_id'] : -1,
			'first_name' => $params['first_name'],
			'last_name' => $params['last_name'],
			'mobile' => $params['mobile'],
			'notes' => $params['notes'],
			'reservation_date' => $params['reservation_date'],
			'reservation_time' => $params['reservation_time'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			// 'status' => "NEW",
			'status' => "SCHEDULED",
			'sale_person_ids' => $params['sale_person_id'],
			'branch_id' => $params['branch_id'],
			// 'reservation_status_id' => -1
			'reservation_status_id' => 1,

            //LC-03-08-2017
            'email_address' => $params['email_address'],
		);
		
		
		if(!$this->db->insert('reservation_header', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create reservation.");
		}
		
		$reservation_id = $this->db->lastInsertId();
		
		
		if($sale_persons != "")
		{
			$sale_persons = explode(",",$sale_persons);
			foreach ($sale_persons as $s)
			{
				$sale_id = $s;
				
				$insert_data = array(
					'reservation_header_id' => $reservation_id,
					'sale_person_id' => $sale_id,
					'create_date' => $params['create_date'],
					'create_by' => $params['create_by'],
				);
				
				if(!$this->db->insert('reservation_sale_person', $insert_data))
				{
					$this->db->rollback_transaction();
					return $this->failed("Unable to create reservation.");
				}
				
			}
		}
		
		
		$products = explode(",",$params['product_id']);
		
		$total_minutes = 0;

        $product_name_list = '';
		
		
		foreach ($products as $p)
		{
			$prod = $this->get_product_info($p);

            /*$product_name_list .= $prod['name'] . ', ';*/
            $product_name_list .= $prod['name'] . ' ('. $prod['category_name'] .'), ';
			
			$price = $prod['price'];
			$minutes = $prod['minutes_of_product'];
			$total_minutes += $minutes;
			
			$insert_data = array(
				'reservation_header_id' => $reservation_id,
				'product_id' => $p,
				'price' => $price,
				'create_date' => $params['create_date'],
				'create_by' => $params['create_by'],
				'sales_person_id' => -1,
				'minutes_of_product' => $minutes,
				'status' => 'A',
			);
			
			if(!$this->db->insert('reservation_detail', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create reservation.");
			}
		}
		
		if($total_minutes > 0)
		{
			if(!$this->db->update('reservation_header', $data = array('duration'=> $total_minutes), 'id='.$reservation_id))
			{
				return $this->failed("Unable to create reservation.");
			}
		}
		
		$this->db->commit_transaction();

        if ($params['email_address']!="") {
            $query_business = "SELECT business_name, address1, city, state, zip, phone1, logo FROM merchant WHERE id='{$params['merchant_id']}'";
            $query_tech = "SELECT last_name, first_name FROM sale_person WHERE id='{$params['sale_person_id']}'";
            
            $business = $this->db->fetchRow($query_business);
            $first_name_email = $params['first_name'];

            $first_name_email = $params['first_name'];
            $last_name_email = $params['last_name'];
            $reservation_date = date("l, j M Y", strtotime($params['reservation_date']));
            $reservation_time = date("g:i a", strtotime($params['reservation_time']));
            $reservation_email = $params['email_address']; /*added*/

            $tech = $this->db->fetchRow($query_tech);
            
            $body = '<html><body style="max-width: 100%; min-width: 40%; margin-left: 25px;margin-right: 25px;"><div style="margin: 0 auto; max-width: 600px; border-style: solid; border-width: 1px; border-color: #dddddd;"><div style="padding: 30px; font: 16px arial, sans-serif; color: #797c80;"><div style="float: left; width: 100%;"><div style="width: 70%; margin: 0 auto;"><div style="float: left; margin-right: 20px; "><img src="'.$business['logo'].'" height="70" width="70"></div><div style=""><h3 style="margin-top: 0; margin-bottom: 2px; text-align: center;">'.$business['business_name'].'</h3><p style="margin-top: 0; text-align: center; font-size: 12px;">' . $business['address1'].'<br>'.$business['city']. ' ' . $business['state'] . ' ' . $business['zip'] . '<br>'. $business['phone1'].'</p></div></div></div><div style="float: left; width: 100%;"><p style="text-align: center;">'. $first_name_email . ', see you soon!</p></div><hr><div style="float: left; width: 100%;"><p style="text-align: center;">Your visit on '.$reservation_date.' <br>'.$reservation_time.' <br>'.rtrim($product_name_list, ', ').' - '.$tech['first_name']. ' ' .$tech['last_name'].'</p></div><hr><div style="float: left; width: 100%; margin-bottom: 8px;"><p><strong>Reminder</strong>: '.$business['business_name'].' requires 24 hours advance notice when cancelling online. If you have any questions, please contact '.$business['business_name'].' at '.$business['phone1'].'</p></div><hr></div></div></body></html>';
            
            $this->send_mail_notification($body, $reservation_email, "Reservation", "", $business['business_name']);
            
        }

		
		return $this->success("Reservation has been created.");
	}

    private function get_product_info($product_id)
	{
		/*$cmd ="SELECT * FROM product WHERE id={$product_id} AND status='A'";*/
        $cmd = "SELECT p.*, pc.name AS category_name FROM product p
                LEFT JOIN product_category pc
                ON p.category_id = pc.id
                WHERE p.id={$product_id} AND p.status='A'";
		$result = $this->db->fetchRow($cmd);
		
		return $result;
		
	}
	
	
	//merchant_id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//is_inventory, create_date, create_by, status
	public function register_product($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('product', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create product.");
		}
		$this->db->commit_transaction();
		return $this->success("Product has been created.");
		
	}
	
	//merchant_id, branch_id, last_name, first_name, middle_name, create_date, create_by, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time, services, contact_number, email_address
	public function register_salon_specialist($params)
	{
		$this->db->begin_transaction();
		$services = $params['services'];
		unset($params['services']);

        $schedule = $params['schedule'];
        unset($params['schedule']);
        $start_time = $params['start_time'];
        unset($params['start_time']);
        $end_time = $params['end_time'];
        unset($params['end_time']);

		if(!$this->db->insert('sale_person', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create salon specialist.");
		}
		
		$sale_person_id = $this->db->lastInsertId();

        $days = array('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');

        $selected_days = explode(",", $schedule);
        
        $start_time = explode(",", $start_time);
        $end_time = explode(",", $end_time);

        foreach ($days as $key => $value) {
            $schedule = array(
                'sales_person_id' => $sale_person_id,
                'day'             => strtoupper($value),
                'create_date'     => $params['create_date'],
                'create_by'       => $params['create_by']
            );

            $new_key = array_search($value, $selected_days);

            if ($new_key !== false) {
                $schedule['is_off'] = 0;
                $schedule['start_time'] = $start_time[$new_key];
                $schedule['end_time'] = $end_time[$new_key];
            } else {
                $schedule['is_off'] = 1;
            }

            if(!$this->db->insert('sale_person_schedule', $schedule))
            {
                $this->db->rollback_transaction();
                return $this->failed("Unable to create salon specialist.");
            }
        }
        
		$products = explode(",",$services);
		foreach($products as $p)
		{
			
			$prod = $this->get_product_info($p);
			
			if(count($prod) == 0)
			{
				return $this->failed("Unable to find product.");
			}
			
			$product = array(
				'product_id' => $prod['id'],
				'sale_person_id' => $sale_person_id,
				'create_date' => $params['create_date'],
				'create_by' => $params['create_by'],
				'merchant_id' => $params['merchant_id'],
			);
			if(!$this->db->insert('product_sale_person', $product))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create salon specialist.");
			}
		}
		
		$this->db->commit_transaction();
		return $this->success("Salon Specialist has been created.");
	}
	
	
	
	

	
	
	
	//merchant_id,longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time,contact_number,
	//store_day, create_date, create_by
	public function register_merchant_branch($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('merchant_branch', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create branch.");
		}
		$this->db->commit_transaction();
		return $this->success("Branch has been created.");
		
	}
	
	
	//search_value can be city/state/zip/business_name
	public function get_merchant($params)
	{
		
		
		if($params['search_value'] !="")
		{
			// $cmd =" SELECT * FROM merchant WHERE status='A' ";
			// $cmd .=" AND (city LIKE '%{$params['search_value']}%' OR state LIKE '%{$params['search_value']}%'
			//  OR zip LIKE '%{$params['search_value']}%'
			//  OR business_name LIKE '%{$params['search_value']}%')
			// ";
            $cmd =' SELECT * FROM merchant WHERE status="A" ';
            $cmd .=' AND (city LIKE "%'.$params['search_value'].'%" OR state LIKE "%'.$params['search_value'].'%"
             OR zip LIKE "%'.$params['search_value'].'%"
             OR business_name LIKE "%'.$params['search_value'].'%")
            ';
            
			$merchants = $this->db->fetchAll($cmd);	
		}else{
			$merchants = array();
		}
		
		//get if is close for the said day
		//get if close base on the time
		
		//get waiting time
		
		$list_merchants = array();
		
		
		foreach ($merchants as $m)
		{
			$merchant_id = $m['id'];
			$params = array(
				'merchant_id' => $merchant_id,
			);
			$this->get_time_zone($params);
			$timezone = $this->response['ResponseMessage']['timezone'];
			date_default_timezone_set($timezone);
			
			
			$current_date = date("Y-m-d");
			
			$day = date('l', strtotime(date("Y-m-d")));
			$current_time = date("G:i");
			
			$current_time = strtotime($current_time);
			
			$day = strtoupper($day);
			
			$cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$merchant_id} AND day='{$day}'";
			
			$rs = $this->db->fetchRow($cmd);
			
			
			if($rs['is_close'])
			{
				$m['is_close'] = $rs['is_close']; 
			}else{
				$start_time = strtotime($rs['start_time']);
				$end_time = strtotime($rs['end_time']);	
				
				if( ($end_time>$current_time)  AND ($start_time<$current_time))
				{
					$m['is_close'] = 0;
				}else{
					//close
					$m['is_close'] = 1;
				}
			}
			
			$m['closing_time'] = date("h:i a", strtotime($rs['end_time']));
			$m['opening_time'] = date("h:i a", strtotime($rs['start_time']));
			$m['day'] = $day;
			
			
			
			
			$cmd =" SELECT  SUM(CASE WHEN check_in_time IS NULL THEN 0 ELSE check_in_time END)  as waiting_time  
			FROM waiting_list WHERE create_date='{$current_date}' AND merchant_id={$merchant_id}";
			
			$rs = $this->db->fetchRow($cmd);

			$hours = floor($rs['waiting_time'] / 60);
			$minutes = $rs['waiting_time'] % 60;

			$m['waiting_time'] = "{$hours} hour(s) and {$minutes} minute(s)";
			
			$list_merchants[] = $m;
			
		}
		
		// $merchant_list = array();
		// foreach($merchants as $m)
		// {
			// $merchant_id = $m['id'];
			// //$m['branches'] = $this->db->fetchAll("SELECT * FROM merchant_branch WHERE merchant_id={$merchant_id} AND status='A'");
			// // $m['contacts'] = $this->db->fetchAll("SELECT * FROM merchant_contact WHERE merchant_id={$merchant_id} AND status='A'");
// 			
			// //sale_person
			// // $m['salon_specialists'] = $this->db->fetchAll("SELECT * FROM sale_person WHERE merchant_id={$merchant_id} AND status='A'");
			// // $m['products'] = $this->db->fetchAll("SELECT * FROM product WHERE merchant_id={$merchant_id} AND status='A'");
// 			
			// $merchant_list[] = $m;
		// }
		
		// return $this->success($merchant_list);
		// return $this->success($merchants);
		return $this->success($list_merchants);
	}
	
	//merchant
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status
	
	//merchant_contact
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//merchant_mailing_address
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	
	public function register_merchant($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'business_name' => $params['business_name'],
			'dba' => $params['dba'],
			'processor' => $params['processor'],
			'mid' => $params['mid'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country' => $params['country'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'fax' => $params['fax'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
		);
		
		
		if(!$this->db->insert('merchant', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		$merchant_id = $this->db->lastInsertId();
		
		
		$insert_data = array(
			'first_name' => $params['first_name'], 
			'last_name' => $params['last_name'],
			'position' => $params['position'],
			'mobile_number' => $params['mobile_number'],
			'business_phone1' => $params['business_phone1'],
			'business_phone2' => $params['business_phone2'],
			'extension' => $params['extension'],
			'fax' => $params['contact_fax'],
			'email' => $params['contact_email'],
			'website' => $params['website'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
			'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->insert('merchant_contact', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		
		// mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
		$insert_data = array(
			'country' => $params['mailing_country'],
			'address' => $params['mailing_address'],
			'city' => $params['mailing_city'],
			'state' => $params['mailing_state'],
			'zip' => $params['mailing_zip'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
			'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->insert('merchant_mailing_address', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		
		
		$user_name = $this->generate_username();
		$password = rand(100000, 999999);
		
		//username , password, last_name, first_name, email_address, user_type_id, reference_id
		$user_insert = array(
			'username' => $user_name,
			'password' => $password,
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email'],
			'user_type_id' =>  7, //merchant admin 8, 7 merchant super admin
			'reference_id' => $merchant_id,
		);
		
		$this->create_user($user_insert, true);
		if($this->response['ResponseCode'] !="0000")
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant");
		}
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant has been created.");
	}
	
	public function generate_username()
	{
		do {
            $user_name = time();
            $check = $this->db->fetchRow('select id from users where username="' . $user_name .'"');
        } while (isset($check['id']));
        return $user_name;
	}

    public function test_appointment($params){
       
        $this->db->begin_transaction();
        
        $insert_data = array(
            'merchant_id' => $params['merchant_id'],
            'is_notification_on' => '1',
            'subject' => 'hello',
            'html_template' => 'hi',
            'frequency_value' => '1',
            'frequency_option' => 'hours',
            'create_by' => 'test',
            'create_date' => date('Y-m-d h:i:s')
        );

        if(!$this->db->insert('merchant_appointment_reminder', $insert_data)) {
            return $this->failed("Unable to set appointment reminder.");
        }

        $this->db->commit_transaction();
    
        return $this->success("Appointment reminder has been created.");

    }

	

	public function send_mail_notification($body,$email_address,$subject='', $attachment ="", $from_name = "")
    {
    	// mail("lemmuelcabuhat@gmail.com", "Hello", "TESTING");
		// die();		
			
    	//date_default_timezone_set('America/Toronto');
    	
    	
    	//$file = $path.$filename;
// $content = file_get_contents( $attachment);
// $content = chunk_split(base64_encode($content));
// $uid = md5(uniqid(time()));
// $name = basename($attachment);
// 
// // header
// $header = "From: <".EMAIL_USERNAME.">\r\n";
// $header .= "Reply-To: ".EMAIL_USERNAME."\r\n";
// $header .= "MIME-Version: 1.0\r\n";
// $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
// 
// // message & attachment
// $nmessage = "--".$uid."\r\n";
// $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
// $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
// $nmessage .= $body."\r\n\r\n";
// $nmessage .= "--".$uid."\r\n"; 
		// if($attachment !="")
	   // {	 
			// // $mail->addAttachment($attachment);
// 				   
			// $nmessage .= "Content-Type: application/octet-stream; name=\"".$attachment."\"\r\n";
			// $nmessage .= "Content-Transfer-Encoding: base64\r\n";
// 			       
			// $nmessage .= "Content-Disposition: attachment; filename=\"".$attachment."\"\r\n\r\n";
	   // }
// $nmessage .= $content."\r\n\r\n";
// $nmessage .= "--".$uid."--";
// 
// 
// 
// if (mail($email_address, $subject, $nmessage, $header)) {
    	// print_r("true...");
// die();
    // die();	
    // return true; // Or do something here
// } else {
	// print_r("false...");
	// die();
  // return false;
// }
    	
        
        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
        // $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        // if(!IS_PRODUCTION)
		// {
		 	$mail->SMTPAuth   = true;                  // enable SMTP authentication
        	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        // }
        
        // print_r(HOST.PORT.EMAIL_USERNAME.EMAIL_PASSWORD);
        // die();
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');       
        $mail->From = 'admin@go3reservation.com';
        $mail->FromName = ($from_name == "") ? 'Go3 Admin' : $from_name;
       if($attachment !="")
	   {	 
	   		$mail->addAttachment($attachment);
	   }
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             //$mail->addAddress($ad);
             $mail->AddAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
         
        if(!$mail->Send()) {
        	// print_r($mail->ErrorInfo());
        	// die();
            return false;//
        } else {
        	// die();
            return true;// $this->success("Message sent!");
        } 
    }
	

	
	//username , password, last_name, first_name, email_address, user_type_id, reference_id
    public function create_user($insert_data, $is_send_access = false)
    {
        //$insert_data = array_map('trim', $insert_data);
        //customer 3
        $password =$insert_data['password'];
        if(in_array($insert_data['user_type_id'], unserialize(CUSTOMER_USER_TYPE_ID)))
        {
            $insert_data['is_customer'] =1;
        }
        //agent 6
        if(in_array($insert_data['user_type_id'], unserialize(AGENT_USER_TYPE_ID)))
        {
            $insert_data['is_agent'] =1;
        }
        //merchant admin 2, 8 merchant, 7 branch merchant admin
        if(in_array($insert_data['user_type_id'], unserialize(MERCHANT_USER_TYPE_ID)))
        {
            $insert_data['is_merchant'] =1;
        }
        //iso_user_type 4, 5
        if(in_array($insert_data['user_type_id'], unserialize(ISO_USER_TYPE_ID)))
        {
            $insert_data['is_iso'] = 1;
        }   
        //super admin 1, 9 admin
        if(in_array($insert_data['user_type_id'], unserialize(ADMIN_USER_TYPE_ID)))
        {
            $insert_data['is_admin'] = 1;
        }
        
        $insert_data['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$insert_data['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        
		
		$insert = array(
			'username' => $insert_data['username'],
			'password' => $insert_data['password'],
			'last_name' => $insert_data['last_name'],
			'first_name' => $insert_data['first_name'],
			'email_address' => $insert_data['email_address'],
			'user_type_id' => $insert_data['user_type_id'],
			'reference_id' => $insert_data['reference_id'],
			'is_agent' => isset($insert_data['is_agent']) ? 1 : 0,
			'status' => $insert_data['status'],
		);
		
		
        if (!$this->db->insert('users', $insert)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
            die();
        }        
		
		
		if($is_send_access)
		{
			$first_name = $insert['first_name'];
			$last_name = $insert['last_name'];
			$username = $insert['username'];
			
			$body = "Hi {$first_name} {$last_name}, <br><br>";
            $body .= " Your merchant has been created. <br>";
            $body .=" Your username: {$username} <br>";
            $body .=" Your password: {$password}";
			
			// if(IS_PRODUCTION)
			// {
				$this->send_mail_notification($body, $insert_data['email_address'], "Merchant Account Creation(Reservation)");
			// }
		}
		
		return $this->success("User has been created.");
		
		
		                                                                
    }


    //params: action_taken, action_module, action_date, action_by, remark
    public function save_to_action_logs($action_taken, $action_module, $action_date, $action_by, $remark) {
        if (!($action_taken != '' && $action_module != '' && $action_date != '' && $action_by != '')) {
            return $this->failed('Invalid parameters');
        } else {
            if($action_module =="ERROR")
            {
                $action_taken .= " ". $this->db->get_error_string();
            }
            
            $insert_data = array(
                'action_taken'  => $action_taken,
                'action_module'            => $action_module,
                'action_date'              => $action_date,
                'action_by'         => $action_by,
                'remark'      => $remark,            
            );
            if (!$this->db->insert('action_logs', $insert_data)) {
                return $this->failed('Unable to save action_logs');
            } else {
                return $this->success('Successfully saved new action_logs');

            }
        }
    }
    
    
    // ACL PROFILE
    public function delete_module($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array('deleted'=>1);
        $this->db->update('resources', $values, 'id='.$id);
        $response = array(
            'message' => "Successfully deleted module",
        );
        return $this->success($response);
    }   
    
    
    public function add_module($params)
    {
         $params = array_map('trim', $params);
         //print_r($params);
         //die();
         
         $insert_data = array(
              'resource' => $params['resource'],
              'description' => $params['description'],
              'create_date' => date('Y-m-d H:i:s'),
              
         );           
         
         
         if (!$this->db->insert('resources', $insert_data)) 
         {
            return $this->failed("Error creating module.");
         }else {
            $response = array(
                'message' => "Successfully created module.",
            );
            return $this->success($response);
         }   
    }

    
    public function get_all_profile($where = null)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A'";

        if(!is_null($where))
            $query .= ' AND '.$where;

        $profile_list = $this->db->fetchAll($query);

        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }
    
    public function get_all_profile_selected($user_type_id)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A' AND create_by =". $user_type_id;
        $profile_list = $this->db->fetchAll($query);
        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }    
    
    public function get_all_profile_access($id)
    {
        $query = 'SELECT resource_id FROM user_templates WHERE user_type_id =' . $id;
        $access_list = $this->db->fetchAll($query);

        if(!$access_list)
            return array();
        else
            return $access_list;
    }
    
    public function get_all_modules_list($where = null)
    {
        $query = 'SELECT * FROM resources where deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $query .= ' ORDER BY description';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    public function get_all_modules_list_selected($user_type_id)
    {
        //$query = 'SELECT * FROM resources where deleted = 0';
        $query = 'SELECT r.* FROM resources r inner join user_templates ut on ut.resource_id = r.id';
        $query .= ' WHERE deleted = 0 and user_type_id = ' . $this->db->db_escape($user_type_id);         
        $query .= ' ORDER BY resource';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    protected function is_profile_existing($profile_name)
    {
        $query = 'select * from user_types where description ='.$this->db->db_escape($profile_name);
        $check_profile = $this->db->fetchAll($query);
        if (count($check_profile)>0) {
           return true;
        } else {
           return false;
        }
           
    }

    public function add_profile($params)
    {
        if(count($params['module_access']) == 0 || !isset($params['module_access']))
            return $this->failed('Select a module');
            
        //print_r($params['module_access']);
        //die();

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }
        
        if($this->is_profile_existing($params['description'])){
            return $this->failed('Profile already exist!');
        }

        $data = array(
            'description'   => $params['description'],
            'create_by'    => $params['created_by']
        );

       if(!$this->db->insert('user_types', $data)) {
            return $this->failed("Unable to add profile");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
            //print_r($id);
            // delete all access from user_templates
            $this->db->delete('user_templates', 'user_type_id=' . $id);
            // insert new access rights
            foreach($params['module_access'] as $data) {
                $insert_data = array(
                    'resource_id'   => $data,
                    'user_type_id'  => $id,
                );
                $this->db->insert('user_templates', $insert_data);
            }
            return $this->success('Profile Saved');
        }
    }

    public function edit_profile($params)
    {
        if(!isset($params['module_access']) || count($params['module_access']) == 0)
            return $this->failed('You cant add a profile with no module');

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }

        if(!is_numeric($params['id']))
            return FALSE;
        
        $id = $params['id'];
        // delete all access from user_templates
        $this->db->delete('user_templates', 'user_type_id=' . $id);
        // insert new access rights
        foreach($params['module_access'] as $data) {
            $insert_data = array(
                'resource_id'   => $data,
                'user_type_id'  => $id,
            );
            $this->db->insert('user_templates', $insert_data);
        }
        return $this->success('Profile updated');
    }

    public function delete_profile($profile_id = null)
    {
        if(is_null($profile_id) || !is_numeric($profile_id))
            return $this->failed('Invalid ID');

        if($this->db->update('user_types', array('status' => 'D'), 'id='.$this->db->db_escape($profile_id))){
            $response = array(
                'message' => "Successfully deleted module",
            );
            return $this->success($response);
        }
        else{
            $response = array(
                'message' => "Unable to delete profile",
            );
            return $this->failed($response);
        }
    }
    
    public function get_all_resources()
    {
        $query = 'SELECT * FROM resources where deleted = 0';
        $query .= ' ORDER BY id';
        //print_r($query);
        //die();
        $module_list = $this->db->fetchAll($query);
        return $this->success($module_list);
    }
    
    public function update_module($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
           'resource'      => $params['value'], 
           'description'    => $params['description'],
        );
        if (!($this->db->update('resources', $update_data, 'id=' . $params['id']))) {
            $response = array(
                'success' => false,
                'message' => 'Unable to update module.',
            );
            return $this->failed($response);    
        } else {
            $response = array(
                'success' => true,
                'message' => 'Successfully updated module',
            );
            return $this->success($response);
        }
    }
    
    // End of New code for go3 rewards
    
    public function replace_schar($val){
        
          $replace = array(
                    '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                    '&quot;' => '',
                    'À' => 'A', '??' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                    '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                    'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', '??' => 'D',
                    '??' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                    'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                    'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', '??' => 'I',
                    'Î' => 'I', '??' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                    'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                    'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                    'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                    'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                    'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                    'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                    '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                    '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                    'W' => 'W', '??' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                    'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                    'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                    'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                    'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                    'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                    'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                    'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                    'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                    'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                    '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                    '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                    '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                    'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                    'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                    'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                    'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                    '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                    '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                    '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                    '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                    '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                    '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                    '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                    '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                    '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                    '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                    '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                    '?' => 'yu', '?' => 'ya', "'"=>"`"
                    );
                    
                    $final = str_replace(array_keys($replace), $replace, $val);
        return $final;
        
    }
        
    //=============LOOKUP FUNCTIONS=====================    
    
    public function validate_session($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']) && strlen($params['session_id']) == 32)) {
            return $this->failed('Invalid or expired session id');
        } else {
            $chk = $this->db->fetchRow('select * from sessions where session_id='. $this->db->db_escape($params['session_id']));
            if (!(isset($chk['session_id']))) {
                return $this->failed('Invalid or expired session id');
            } elseif (isset($chk['logout_date']) && $chk['logout_date'] != '') {
                return $this->failed('Invalid or expired session id');
            //} elseif (strtotime($chk['last_seen']) + (3600) < time()) {
            // } elseif (strtotime($chk['last_seen']) + (600) < time()) {
            // //} elseif (strtotime($chk['last_seen']) + (30) < time()) {
                // return $this->failed('Invalid or expired session id');
            } 
            else {
                $values = array("last_seen" => date('Y-m-d H:i:s'));
                $this->db->update('sessions', $values, 'session_id=' . $this->db->db_escape($params['session_id']));
                //return $this->success('session id valid');
				$user_info = unserialize($chk['session_data']);
				return $this->success($user_info);
            }
        }
    }
 
    public function get_user_by_id($params) 
    {
        $query = "select u.user_center, u.is_center, CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type WHERE u.id = ".$params['id'];

        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    
    public function get_users_detailed_selected($user_type_id) {
        $query = 'select u.status, u.id,fname,lname,username,u.email_address,mobile_number,user_type_id,ut.description ' ;                $query .= 'user_type_desc from users u ';
        $query .= 'inner join user_types ut on ut.id=u.user_type_id ';
        $query .= 'where ut.created_by=' . $user_type_id;
/*        print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_list()
    {
        $query="SELECT id,username,password,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'I' THEN 'INACTIVE' ELSE 'PENDING' END as status";
        $query.=",date_created,created_by,fname";
        $query.=",lname,email_address,mobile_number,address,user_type,customers_id,agent_id FROM users";
        $users= $this->db->fetchAll($query);
        $this->success($users); 
        $response = $this->get_response(); 
        return    $response;
    }
	
	public function get_user_type_by_username($username){
        $sql = "SELECT ut.* FROm users u LEFT JOIN user_types ut ON u.user_type_id=ut.id WHERE u.username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchAll($sql);
        return $this->success($result);  
    }
    
    public function generate_password() {
        return substr(md5(uniqid(time(), true)), 5,8);
    }

    protected function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } else {
                    if (file_exists($dst . '/' . $file)) @unlink($dst . '/' . $file);
                    copy($src . '/' . $file, $dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }
                                   
    //params: username, password
    public function login($params) {
        $params = array_map('trim', $params);

        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
			 // print_r("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
			 // die();
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
        
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				if($user_check['user_type_desc'] =="CUSTOMER")
				{
					return $this->failed("Invalid access rights. Customer is not allowed.");
				}
				
				$profile = '';
				if($user_check['is_iso'])
				{
					$profile = 'iso';
				}elseif($user_check['is_merchant']){
					$profile = 'merchants';
				}elseif ($user_check['is_agent']){
					$profile = 'agents';
				}else{
					$profile = 'customers';
				}				
				
				$id = $user_check['reference_id'];
				// print_r($id);
				// die();
				// $user_info = $this->get_profile($profile, $id);
				$user_info = array();
				
				//$user_info['profile'] = $profile;
				


				$user_info['user_id'] = $user_check['id'];
				$user_info['partner_id'] = $id;

				$cmd =" SELECT * FROM merchant_business_hours  WHERE merchant_id={$id}";
				$b_results = $this->db->fetchAll($cmd);


				$user_info['is_agent'] = $user_check['is_agent'];
				$user_info['user_type'] = $user_check['user_type_id'];
                $user_info['user_type_desc'] = $user_check['user_type_desc'];
				$user_info['username'] = $params['username'];
				// $user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                    'business_hours' => $b_results,
                );
                
                return $this->success($response);
            }
        }
    }
    
    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = $this->db->fetchRow('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
	
    protected function create_session($user_id, $session_data = "") {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
            //'session_data' => $session_data,
        );
		if(is_array($session_data))
		{
			$insert_data['session_data'] = serialize($session_data);
		}else
		{
			$insert_data['session_data'] =  $session_data;
		}
		
        $this->db->insert('sessions', $insert_data);
        return $session_id;        
    }

    //user_id
    public function get_user_permissions($user_id, $user_type_id) {        
        $permissions = $this->db->fetchPairs('select p.id,r.resource from permissions p inner join resources r on p.resource_id=r.id where p.user_id=' . $this->db->db_escape($user_id));

        
        $permissions2 = $this->db->fetchPairs('select ut.id,r.resource from user_templates ut inner join resources r on ut.resource_id=r.id where ut.user_type_id=' . $this->db->db_escape($user_type_id));
        $permissions = array_flip(array_merge(array_flip($permissions), array_flip($permissions2)));
        

        if (is_array($permissions) && count($permissions) > 0) {
            return $permissions;
        }
    }

    //params: session_id
    public function logout($params) {//note: session maintenance is not yet implemented in backend.php, client must implement its own
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']))) {
            return $this->failed('Invalid session ID');
        } else {
            $check = $this->db->fetchRow('select * from sessions where logout_date is null and session_id=' . $this->db->db_escape($params['session_id']));
            if (!(isset($check['id']))) {
                return $this->failed('Invalid session ID');
            } else {
                $update_data = array(
                    'logout_date' => date('Y-m-d H:i:s'),
                );
                if (!$this->db->update('sessions', $update_data, 'id='. $check['id'])) {
                    return $this->failed('Unable to logout');
                } else {
                    return $this->success('Successfully logged out user.');
                }
            }
        }
    }

    
    protected function failed($message) {
        $this->response = array(
            'ResponseCode' => '0001',
            'ResponseMessage' => $message,
        );
        return false;
    }
    
    protected function success($message, $record_count=-1) {
        $this->response = array(
            'ResponseCode' => '0000',
            'ResponseMessage' => $message,
        );
        return true;
    }
    
    public function get_response() {
        return $this->response;
    }

     // ACL
    public function get_module_name($module_list = array(), $include_action = false)
    {
        if(count($module_list) > 0) {
            $return_arr = array();
            $query = "SELECT * FROM acl_modules WHERE id IN(".implode(',', $module_list).") AND main = 1";
            $module_list = $this->db->fetchAll($query);

            foreach($module_list as $module) {
                $return_arr[] = $module['module_class'];
                $return_arr[] = $module['main_module_index'];
                if($include_action)
                    $return_arr[] = $module['module_action'];
            }

            $result = array_unique($return_arr);
            return $result;
        } else {
            return FALSE;
        }
    }

    public function remove_module_buttons($class)
    {
        $module_btn_return = array();

        $all_module_btn = $this->db->fetchAll('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND with_button = 1');
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];
        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_access = $this->db->fetchRow($query);
        $module_access = explode(',', $module_access['module_access']);

        if($all_module_btn && $module_access) {
            foreach($all_module_btn as $btn) {
                if(in_array($btn['id'], $module_access)) {
                    $module_btn_return[$btn['module_action']] = '';
                } else {
                    $module_btn_return[$btn['module_action']] = 'display: none;';
                }
            }
        }

        return $module_btn_return;
    }

    public function get_all_modules()
    {
        $return_arr = array();
        $query = "SELECT * FROM acl_modules"; // check if it will be a 1d array if i just get the id instead of all
        $all_modules = $this->db->fetchAll($query);

        foreach($all_modules as $module)
            $return_arr[] = $module['id'];

        return $return_arr;
    }

    public function get_user_modules()
    {
        $user_info = $this->get_user_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['user_type'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            $module_list = explode(',', $module_list['module_access']);
            $all_modules = $this->get_all_modules();

            $unset_modules = array_diff($all_modules, $module_list);

            if(count($unset_modules) > 0)
                return $unset_modules;
            else
                return FALSE;
        }
    }

    public function get_user_access_list()
    {
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            return explode(',', $module_list['module_access']);
        } else {
            return array();
        }
    }

    public function get_module_id($class, $action)
    {
        $module_info = $this->db->fetchRow('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND module_action = '.$this->db->db_escape($action));

        if(!$module_info)
            return FALSE;

        $module_id = $module_info['id'];
        $user_list = $this->get_user_access_list();

        if(in_array($module_id, $user_list)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

     public function get_pan_key() {                                                     
        //$query = $this->db->select('key')->from('keys')->order_by('timestamp', 'desc')->get();
        $query = $this->db->fetchRow("SELECT `key` FROM `keys` ORDER by `timestamp` desc");
        if (!$query) {
            //$key = $query->result_array();
            $dek_enc = $query['key'];
            $iv = sha1('aes-256-cbc');
            //$kek = file_get_contents('http://192.168.2.16/web/i.php?token=cfbe176207b80774e8911c10893f5a0f');
            $kek="cfbe176207b80774e8911c10893f5a0f";
            $dek_plain = $this->hideit->decrypt($kek, $dek_enc, $iv);
            return $dek_plain;
        } else {
            return '';
        }
    }  

    public function get_contact_number($params)
    {

        $query = "SELECT * FROM customer WHERE mobile_number={$params['contact_number']}";  
        $result = $this->db->fetchRow($query);
        $count = count($result); 
 
        if($count == 0){
            return $this->success("false");
        }else{
            return $this->success("true");
        }
    }

    public function get_all_active_merchant() {
        $merchants = $this->db->fetchAll('SELECT * FROM merchant WHERE status="A"');

        foreach ($merchants as $m)
        {
            $merchant_id = $m['id'];
            $params = array(
                'merchant_id' => $merchant_id,
            );
            $this->get_time_zone($params);
            $timezone = $this->response['ResponseMessage']['timezone'];
            date_default_timezone_set($timezone);
            
            
            $current_date = date("Y-m-d");
            
            $day = date('l', strtotime(date("Y-m-d")));
            $current_time = date("G:i");
            
            $current_time = strtotime($current_time);
            
            $day = strtoupper($day);
            
            $cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$merchant_id} AND day='{$day}'";
            
            $rs = $this->db->fetchRow($cmd);
            
            
            if($rs['is_close'])
            {
                $m['is_close'] = $rs['is_close']; 
            }else{
                $start_time = strtotime($rs['start_time']);
                $end_time = strtotime($rs['end_time']); 
                
                if( ($end_time>$current_time)  AND ($start_time<$current_time))
                {
                    $m['is_close'] = 0;
                }else{
                    //close
                    $m['is_close'] = 1;
                }
            }
            
            $m['closing_time'] = date("h:i a", strtotime($rs['end_time']));
            $m['opening_time'] = date("h:i a", strtotime($rs['start_time']));
            $m['day'] = $day;
            
            
            
            
            $cmd =" SELECT  SUM(CASE WHEN check_in_time IS NULL THEN 0 ELSE check_in_time END)  as waiting_time  
            FROM waiting_list WHERE create_date='{$current_date}' AND merchant_id={$merchant_id}";
            
            $rs = $this->db->fetchRow($cmd);

            $hours = floor($rs['waiting_time'] / 60);
            $minutes = $rs['waiting_time'] % 60;

            $m['waiting_time'] = "{$hours} hour(s) and {$minutes} minute(s)";
            
            $list_merchants[] = $m;
            
        }
        return $this->success($list_merchants);
    }

    //ADMIN
    public function getAllMerchants() {
        $result = $this->db->fetchAll('SELECT * FROM merchant');
        return $this->success($result);
    }
    
    public function getAllTimezone() {
        $result = $this->db->fetchAll('SELECT * FROM timezone');
        return $this->success($result);
    }
    
    public function getCountryState($params) {
        $result = $this->db->fetchAll("SELECT * FROM state WHERE country_id = {$params['country_id']}");
        return $this->success($result);
    }
    
    public function getMerchantInfoByID($params) {
        $result = $this->db->fetchAll("SELECT * FROM merchant WHERE id = {$params['id']}");
        return $this->success($result);
    }
    
    public function registerAdminMerchant($params) {
        $insert_data = array(
            'business_name' => $params['business_name'],
            'dba' => $params['dba'],
            'processor' => $params['processor'],
            'mid' => $params['mid'],
            'description' => $params['description'],
            'timezone' => $params['timezone'],
            'address1' => $params['address1'],
            'address2' => $params['address2'],
            'city' => $params['city'],
            'state' => $params['state'],
            'zip' => $params['zip'],
            'country' => $params['country'],
            'email' => $params['email'],
            'phone1' => $params['phone1'],
            'phone2' => $params['phone2'],
            'fax' => $params['fax'],
            'create_date' => $params['create_date'],
            'create_by' => $params['create_by'],
            'status' => $params['status'],
            'is_sms_on' => $params['sms_notif']
        );
        
        if(!$this->db->insert('merchant', $insert_data)){
            $this->db->rollback_transaction();
            return $this->failed("Unable to create merchant.");
        }else{
            
            $merchant_id = $this->db->lastInsertId();
        
            $insert_data2 = array(
                'first_name' => $params['first_name'], 
                'last_name' => $params['last_name'],
                'position' => $params['position'],
                'mobile_number' => $params['mobile_number'],
                'business_phone1' => $params['business_phone1'],
                'business_phone2' => $params['business_phone2'],
                'extension' => $params['extension'],
                'fax' => $params['contact_fax'],
                'email' => $params['contact_email'],
                'website' => $params['website'],
                'create_date' => $params['create_date'],
                'create_by' => $params['create_by'],
                'status' => $params['status'],
                'merchant_id' => $merchant_id
            );

            if(!$this->db->insert('merchant_contact', $insert_data2)){
                
                $this->db->rollback_transaction();
                return $this->failed("Unable to create merchant.");
                
            }else{
                
                $insert_data3 = array(
                    'country' => $params['mailing_country'],
                    'address' => $params['mailing_address'],
                    'city' => $params['mailing_city'],
                    'state' => $params['mailing_state'],
                    'zip' => $params['mailing_zip'],
                    'create_date' => $params['create_date'],
                    'create_by' => $params['create_by'],
                    'status' => $params['status'],
                    'merchant_id' => $merchant_id
                );
                
                if(!$this->db->insert('merchant_mailing_address', $insert_data3)){
                    
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to create merchant.");
                    
                }else{
                    
                    $user_insert = array(
                        'username' => $params['username'],
                        'password' => $params['password'],
                        'last_name' => $params['last_name'],
                        'first_name' => $params['first_name'],
                        'email_address' => $params['usremail'],
                        'user_type_id' =>  7,
                        'reference_id' => $merchant_id,
                        'created_by' => $params['create_by']
                    );
                    
                    $this->create_user($user_insert, true);
                    
                    if($this->response['ResponseCode'] !="0000") {
                        
                        $this->db->rollback_transaction();
                        return $this->failed("Unable to create merchant.");
                        
                    }else{
                        $_myresponse = array(
                            'success' => true,
                            'message' => 'New merchant Added',
                            'redirect_url' => 'merchant'
                            // 'redirect_url' => WEBROOT.'/admin/newmerchant'
                        );
                        return $this->success($_myresponse);
                    }
                }
                
            }
            
        }
        
    }
    
    public function editAdminMerchant($params) {
        $merchant_id = $params['merchant_id'];

        $update_data = array(
            'business_name' => $params['business_name'],
            'description' => $params['description'],
            'timezone' => $params['timezone'],
            'address1' => $params['address1'],
            'address2' => $params['address2'],
            'city' => $params['city'],
            'state' => $params['state'],
            'zip' => $params['zip'],
            'country' => $params['country'],
            'email' => $params['email'],
            'phone1' => $params['phone1'],
            'phone2' => $params['phone2'],
            'fax' => $params['fax'],
            'status' => $params['status'],
            'update_date' => $params['update_date'],
            'update_by' => $params['update_by'],
            'is_sms_on' => $params['sms_notif']
        );
        
        
        if(!$this->db->update('merchant', $update_data, 'id='.$merchant_id)){
                
            $this->db->rollback_transaction();
            return $this->failed("Unable to create merchant.");

        }else{
            
            $update_data2 = array(
                'business_phone1' => $params['business_phone1'],
                'business_phone2' => $params['business_phone2'],
                'fax' => $params['contact_fax'],
                'email' => $params['contact_email'],
                'update_date' => $params['update_date'],
                'update_by' => $params['update_by']
            );
            
            if(!$this->db->update('merchant_contact', $update_data2, 'merchant_id='.$merchant_id)){
                
                $this->db->rollback_transaction();
                return $this->failed("Unable to create merchant.");

            }else{
                
                $update_data3 = array(
                    'country' => $params['mailing_country'],
                    'address' => $params['mailing_address'],
                    'city' => $params['mailing_city'],
                    'state' => $params['mailing_state'],
                    'zip' => $params['mailing_zip'],
                    'update_date' => $params['update_date'],
                    'update_by' => $params['update_by']
                );
                
                if(!$this->db->update('merchant_mailing_address', $update_data3, 'merchant_id='.$merchant_id)){
                    
                    $this->db->rollback_transaction();
                    return $this->failed("Unable to create merchant.");
                    
                }else{
                    
                    $_myresponse = array(
                        'success' => true,
                        'message' => 'Merchant Information Updated',
                        'redirect_url' => 'editmerchant?merchantid='.$merchant_id
                        // 'redirect_url' => WEBROOT.'/admin/editmerchant?merchantid='.$merchant_id
                    );
                    return $this->success($_myresponse);
                    
                }
                
            }
            
        }
        
    }

    //merchant_id, create_date, create_by
    public function set_appointment_reminder($params)
    {
        
        $this->db->begin_transaction();
        
        $insert_data = array(
            'merchant_id' => $params['merchant_id'],
            'is_notification_on' => $params['is_notification_on'],
            'subject' => $params['subject'],
            'html_template' => $params['html_template'],
            'frequency_value' => $params['frequency_value'],
            'frequency_option' => $params['frequency_option'],
            'create_by' => $params['create_by'],
            'create_date' => $params['create_date'],
        );

        $update_data = array(
            'is_notification_on' => $params['is_notification_on'],
            'subject' => $params['subject'],
            'html_template' => $params['html_template'],
            'frequency_value' => $params['frequency_value'],
            'frequency_option' => $params['frequency_option'],
            'update_by' => $params['create_by'],
            'update_date' => $params['create_date'],
        );
        
        $merchant_id = $params['merchant_id'];

        $query = "SELECT merchant_id FROM merchant_appointment_reminder WHERE merchant_id='{$merchant_id}' ";
        $result = $this->db->fetchRow($query);

        if($result['merchant_id']){
            if(!$this->db->update('merchant_appointment_reminder', $update_data, 'merchant_id='.$merchant_id))
            {
                return $this->failed("Unable to update appointment reminder.");
            }

            $this->db->commit_transaction();
        
            return $this->success("Appointment reminder has been updated.");
        }else{

            if(!$this->db->insert('merchant_appointment_reminder', $insert_data)) {
                return $this->failed("Unable to set appointment reminder.");
            }

            $this->db->commit_transaction();
        
            return $this->success("Appointment reminder has been created.");
        }

        
    }

    public function send_appointment_reminder() {
        $cmd = "SELECT * FROM merchant_appointment_reminder ar
                LEFT JOIN merchant m
                ON ar.merchant_id = m.id
                WHERE is_notification_on = 1";

        $merchant = $this->db->fetchAll($cmd);

        foreach ($merchant as $m) {
            // minute
            $date = new DateTime("now", new DateTimeZone($m['timezone']));
            $date = $date->format('Y-m-d H:i:00');

            $cmd = "SELECT *, r.id as reservation_id FROM merchant_appointment_reminder ar
                    LEFT JOIN reservation_header r
                    ON ar.merchant_id = r.merchant_id
                    WHERE ar.merchant_id = {$m['id']}
                    AND ar.frequency_option = 'minutes'
                    AND r.is_sent_mail = 0
                    AND r.status = 'SCHEDULED'
                    AND r.email_address != ''
                    AND '{$date}' = DATE_SUB(STR_TO_DATE(CONCAT(r.reservation_date, ' ', TIME_FORMAT(r.reservation_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s'), INTERVAL ar.frequency_value MINUTE)";

            $reservation = $this->db->fetchAll($cmd); 
            
            if (count($reservation) > 0) {
                foreach ($reservation as $r) {
                    $query_product = "SELECT p.name FROM reservation_detail rd
                                        LEFT JOIN product p
                                        ON rd.product_id = p.id
                                        WHERE rd.reservation_header_id = {$r['id']}";

                    $product_list = $this->db->fetchAll($query_product);  
                    $product_name = "";

                    foreach ($product_list as $p) {
                        $product_name .= $p['name'] . ", ";
                    }

                    $product_name = substr($product_name, 0, -2);

                    $body = $r['html_template'];

                    $list_var = array("[first_name]", "[appointment_date]", "[company_name]", "[company_phone]", "[company_email]", "[company_address]", "[company_city]", "[company_state]", "[company_postal_code]", "[services_list]", "[appointment_time]");

                    foreach ($list_var as $key => $value) {
                        if (strpos($body, $value) !== false) {
                            switch ($value) {
                                case '[first_name]':
                                    $body = str_replace($value, $r['first_name'], $body);
                                    break;
                                case '[appointment_date]':
                                    $date = date("M d, Y", strtotime($r['reservation_date']));
                                    $body = str_replace($value, $date, $body);
                                    break;
                                case '[company_name]':
                                    $body = str_replace($value, $m['business_name'], $body);
                                    break;
                                case '[company_email]':
                                    $body = str_replace($value, $m['email'], $body);
                                    break;
                                case '[company_phone]':
                                    $body = str_replace($value, $m['phone1'], $body);
                                    break;
                                case '[company_address]':
                                    $body = str_replace($value, $m['address1'], $body);
                                    break;
                                case '[company_city]':
                                    $body = str_replace($value, $m['city'], $body);
                                    break;
                                case '[company_state]':
                                    $body = str_replace($value, $m['state'], $body);
                                    break;
                                case '[company_postal_code]':
                                    $body = str_replace($value, $m['zip'], $body);
                                    break;
                                case '[services_list]':
                                    $body = str_replace($value, $product_name, $body);
                                    break;
                                case '[appointment_time]':
                                    $time = date("h:i A", strtotime($r['reservation_time']));
                                    $body = str_replace($value, $time, $body);
                                    break;    

                                
                                default:
                                    # code...
                                    break;
                            }
                        }
                    }

                    if ($this->send_mail_notification($body, $r['email_address'], "Reservation")) {
                        $update_data = array('is_sent_mail' => 1);

                        if (!$this->db->update('reservation_header', $update_data, 'id='.$r['reservation_id'])) {
                            return $this->failed("Unable to update reservation.");
                        }
                    }
                }
            }

            // hour

            $date = new DateTime("now", new DateTimeZone($m['timezone']));
            $date = $date->format('Y-m-d H:i:00');

            $cmd = "SELECT *, r.id as reservation_id FROM merchant_appointment_reminder ar
                    LEFT JOIN reservation_header r
                    ON ar.merchant_id = r.merchant_id
                    WHERE ar.merchant_id = {$m['id']}
                    AND ar.frequency_option = 'hours'
                    AND r.is_sent_mail = 0
                    AND r.status = 'SCHEDULED'
                    AND r.email_address != ''
                    AND '{$date}' = DATE_SUB(STR_TO_DATE(CONCAT(r.reservation_date, ' ', TIME_FORMAT(r.reservation_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s'), INTERVAL ar.frequency_value HOUR)";

            $reservation = $this->db->fetchAll($cmd); 
            
            if (count($reservation) > 0) {
                foreach ($reservation as $r) {
                    $query_product = "SELECT p.name FROM reservation_detail rd
                                        LEFT JOIN product p
                                        ON rd.product_id = p.id
                                        WHERE rd.reservation_header_id = {$r['id']}";

                    $product_list = $this->db->fetchAll($query_product);  
                    $product_name = "";

                    foreach ($product_list as $p) {
                        $product_name .= $p['name'] . ", ";
                    }

                    $product_name = substr($product_name, 0, -2);

                    $body = $r['html_template'];

                    $list_var = array("[first_name]", "[appointment_date]", "[company_name]", "[company_phone]", "[company_email]", "[company_address]", "[company_city]", "[company_state]", "[company_postal_code]", "[services_list]", "[appointment_time]");

                    foreach ($list_var as $key => $value) {
                        if (strpos($body, $value) !== false) {
                            switch ($value) {
                                case '[first_name]':
                                    $body = str_replace($value, $r['first_name'], $body);
                                    break;
                                case '[appointment_date]':
                                    $date = date("M d, Y", strtotime($r['reservation_date']));
                                    $body = str_replace($value, $date, $body);
                                    break;
                                case '[company_name]':
                                    $body = str_replace($value, $m['business_name'], $body);
                                    break;
                                case '[company_email]':
                                    $body = str_replace($value, $m['email'], $body);
                                    break;
                                case '[company_phone]':
                                    $body = str_replace($value, $m['phone1'], $body);
                                    break;
                                case '[company_address]':
                                    $body = str_replace($value, $m['address1'], $body);
                                    break;
                                case '[company_city]':
                                    $body = str_replace($value, $m['city'], $body);
                                    break;
                                case '[company_state]':
                                    $body = str_replace($value, $m['state'], $body);
                                    break;
                                case '[company_postal_code]':
                                    $body = str_replace($value, $m['zip'], $body);
                                    break;
                                case '[services_list]':
                                    $body = str_replace($value, $product_name, $body);
                                    break;
                                case '[appointment_time]':
                                    $time = date("h:i A", strtotime($r['reservation_time']));
                                    $body = str_replace($value, $time, $body);
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            }
                        }
                    }

                    if ($this->send_mail_notification($body, $r['email_address'], "Reservation")) {
                        $update_data = array('is_sent_mail' => 1);

                        if (!$this->db->update('reservation_header', $update_data, 'id='.$r['reservation_id'])) {
                            return $this->failed("Unable to update reservation.");
                        }
                    }
                }
            }


            // day

            $date = new DateTime("now", new DateTimeZone($m['timezone']));
            $date = $date->format('Y-m-d');

            $cmd = "SELECT *, r.id as reservation_id FROM merchant_appointment_reminder ar
                    LEFT JOIN reservation_header r
                    ON ar.merchant_id = r.merchant_id
                    WHERE ar.merchant_id = {$m['id']}
                    AND ar.frequency_option = 'days'
                    AND r.is_sent_mail = 0
                    AND r.status = 'SCHEDULED'
                    AND r.email_address != ''
                    AND DATE('{$date}') = DATE_SUB(r.reservation_date, INTERVAL ar.frequency_value DAY)";

            //email should not be null

            $reservation = $this->db->fetchAll($cmd);   
            if (count($reservation) > 0) {
                //sending of email here, check body if with html
                foreach ($reservation as $r) {
                    $query_product = "SELECT p.name FROM reservation_detail rd
                                        LEFT JOIN product p
                                        ON rd.product_id = p.id
                                        WHERE rd.reservation_header_id = {$r['id']}";

                    $product_list = $this->db->fetchAll($query_product);  
                    $product_name = "";

                    foreach ($product_list as $p) {
                        $product_name .= $p['name'] . ", ";
                    }

                    $product_name = substr($product_name, 0, -2);

                    $body = $r['html_template'];

                    $list_var = array("[first_name]", "[appointment_date]", "[company_name]", "[company_phone]", "[company_email]", "[company_address]", "[company_city]", "[company_state]", "[company_postal_code]", "[services_list]", "[appointment_time]");

                    foreach ($list_var as $key => $value) {
                        if (strpos($body, $value) !== false) {
                            switch ($value) {
                                case '[first_name]':
                                    $body = str_replace($value, $r['first_name'], $body);
                                    break;
                                case '[appointment_date]':
                                    $date = date("M d, Y", strtotime($r['reservation_date']));
                                    $body = str_replace($value, $date, $body);
                                    break;
                                case '[company_name]':
                                    $body = str_replace($value, $m['business_name'], $body);
                                    break;
                                case '[company_email]':
                                    $body = str_replace($value, $m['email'], $body);
                                    break;
                                case '[company_phone]':
                                    $body = str_replace($value, $m['phone1'], $body);
                                    break;
                                case '[company_address]':
                                    $body = str_replace($value, $m['address1'], $body);
                                    break;
                                case '[company_city]':
                                    $body = str_replace($value, $m['city'], $body);
                                    break;
                                case '[company_state]':
                                    $body = str_replace($value, $m['state'], $body);
                                    break;
                                case '[company_postal_code]':
                                    $body = str_replace($value, $m['zip'], $body);
                                    break;
                                case '[services_list]':
                                    $body = str_replace($value, $product_name, $body);
                                    break;
                                case '[appointment_time]':
                                    $time = date("h:i A", strtotime($r['reservation_time']));
                                    $body = str_replace($value, $time, $body);
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            }
                        }
                    }

                    if ($this->send_mail_notification($body, $r['email_address'], "Reservation")) {
                        $update_data = array('is_sent_mail' => 1);

                        if (!$this->db->update('reservation_header', $update_data, 'id='.$r['reservation_id'])) {
                            return $this->failed("Unable to update reservation.");
                        }
                    }
                }
            }
            
        }
        
    }
    
    public function get_appointment_reminder($params)
    {
        $cmd ="SELECT * FROM  merchant_appointment_reminder m 
        WHERE m.merchant_id={$params['merchant_id']}";
        
        
        // $cmd ="SELECT * FROM merchant WHERE id={$params['merchant_id']}";
        $reminder = $this->db->fetchRow($cmd);
        
        if(count($reminder)==0)
        {
            return $this->failed("Unable to find merchant.");
        }

        return $this->success($reminder);
    }

    public function get_merchant_business_hours($params){

        $cmd =" SELECT * FROM merchant_business_hours  WHERE merchant_id={$params['merchant_id']}";
        $b_results = $this->db->fetchAll($cmd);

        $response = array(
            'business_hours' => $b_results,
        );
        
        return $this->success($response);

    }

    
}
