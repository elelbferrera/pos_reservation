<?php
class HideIt {
    private $method;
    private $salt;

    public function __construct($method = 'aes-128-cbc', $salt = '') {
        $this->method = $method;
        $this->salt = $salt;
    }

    function encrypt($pass, $plain, $iv) {
        $encrypted = openssl_encrypt($plain, $this->method, $this->salt.$pass.$this->salt, false, substr(sha1($iv), 3, 16));
        return $encrypted;
    }

    function decrypt($pass, $encrypted, $iv) {
        $decrypted = openssl_decrypt($encrypted, $this->method, $this->salt.$pass.$this->salt, false, substr(sha1($iv), 3, 16));
        return $decrypted;
    }
}