<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/customer.js"></script>

<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/agent.png" alt="Click to hide" style="height: 30px;width: 30px;" />List of Customers</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             Customer</h4>
             
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterCustomer(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New Customer </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>       
                    <th width="10%">Customer Code</th>
                    <th width="10%">Last Name</th>
                    <th width="10%">First Name</th>
                    <th width="10%">Mobile</th>
                    <th width="10%">Email</th>
                    <th width="10%">State</th>
                    <th width="10%">City</th>
                    <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                
                {foreach from=$customers item=item}
                    <tr>
                        <td>{$item.customer_code}</td> 
                        <td>{$item.last_name}</td> 
                        <td>{$item.first_name}</td> 
                        <td>{$item.mobile}</span></td> 
                        <td>{$item.email}</td>
                        <td>{$item.state}</td>
                        <td>{$item.city}</td>
                        <td>
                            <a onclick="EditCustomer('{$item.id}','{$item.merchant_id}','{$item.customer_code}', 
                            '{$item.last_name}', '{$item.first_name}', '{$item.middle_name}', '{$item.mobile}', 
                            '{$item.points_earned}', '{$item.total_amount}', '{$item.telephone}', '{$item.address}', '{$item.state}','{$item.city}',
                            '{$item.zip}', '{$item.country}', '{$item.email}', '{$item.udf1}', '{$item.udf2}', 
                            '{$item.udf3}')">Edit</a>
                            <a href="#" onclick="DeleteCustomer('{$item.id}')">Delete</a>
                        </td>
                    </tr>
                {/foreach}    
        
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgCustomer" title="New Customer" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabCustomer1" class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Customer Information</a></li>
           <!--     <li id="tabAgent2"><a href="#tab2" class="glyphicons adress_book" data-toggle="tab"><i></i>DBA Address</a></li>
                <li id="tabAgent3"><a href="#tab3" class="glyphicons home" data-toggle="tab"><i></i>Mailing Address</a></li>
               -->
              </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           
        <form id="frmRegisterCustomer" name="frmRegisterCustomer">
            <input type="hidden"  id = "txtMerchantId" name = "txtMerchantId"  value="-1"/>  
            <input type="hidden"  id = "txtCustomerId" name = "txtCustomerId"/>
            
            <div id ="divAgentStep1"> 
                <table width="100%">
                <tbody>
                   <tr>
                        <td class="tbl_label">Last Name:</td>
                        <td>
                            <input type="text" name="txtLastName" id="txtLastName" size="29" maxlength="60" />
                         </td>
                    </tr>
                   
                   <tr>
                        <td class="tbl_label">First Name:</td>
                        <td>
                            <input type="text" name="txtFirstName" id="txtFirstName" size="29" maxlength="60" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Middle Name:</td>
                        <td>
                            <input type="text" name="txtMiddleName" id="txtMiddleName" size="29" maxlength="60" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Mobile:</td>
                        <td>
                            <input type="text" name="txtMobile" id="txtMobile" size="29" maxlength="60" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Telephone:</td>
                        <td>
                            <input type="text" name="txtTelephone" id="txtTelephone" size="29" maxlength="20" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Address:</td>
                        <td>
                            <input type="text" name="txtAddress" id="txtAddress" size="29" maxlength="80" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">State:</td>
                        <td>
                            <input type="text" name="txtState" id="txtState" size="29" maxlength="80" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">City:</td>
                        <td>
                            <input type="text" name="txtCity" id="txtCity" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Zip:</td>
                        <td>
                            <input type="text" name="txtZip" id="txtZip" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Country:</td>
                        <td>
                            <input type="text" name="txtCountry" id="txtCountry" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Email Address:</td>
                        <td>
                            <input type="text" name="txtEmail" id="txtEmail" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Account Password:</td>
                        <td>
                            <input type="password" name="txtPassword" id="txtPassword" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF1:</td>
                        <td>
                            <input type="text" name="txtUDF1" id="txtUDF1" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF2:</td>
                        <td>
                            <input type="text" name="txtUDF2" id="txtUDF2" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF3:</td>
                        <td>
                            <input type="text" name="txtUDF3" id="txtUDF3" size="29" maxlength="80" />
                         </td>
                    </tr>
                </tbody>
            </table>
            </div>

            
            </div>
            

            <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnRegisterCustomer" id="btnRegisterCustomer" value="Register Customer" class="btn btn-info"/>
                <input type="button"  name="btnUpdateCustomer" style="display:none" id="btnUpdateCustomer" value="Update Customer" class="btn btn-info"/>
                
                <input type="button"  name="btnCancelNewCustomer" id="btnCancelNewCustomer" value="Cancel" class="btn btn-danger btn-small"/>
            </div>

          
        </form> 
        </div>   
        </div> 
        </div>
        </div>
        

        
        
        
        
        {$_messages_}
    
    
    </div>
</div>
</div>


        
