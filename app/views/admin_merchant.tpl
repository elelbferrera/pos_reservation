<script type="text/javascript" src="{$webroot_resources}/js/admin.js"></script>	

<section class="vbox">
<section>
	<section class="hbox stretch">
		

  	<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
		<section class="vbox">

			 <header class="header bg-primary lter text-center clearfix">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                    <div class="btn-group hidden-nav-xs">
                         <a href="{$webroot}/admin/newmerchant"><button id="findApptBtn" type="button" class="btn btn-primary" style="width: 90%;">Add New</button></a>
                    </div>
                </div>
            </header>


			<section>
			<!-- 	<div class="newmerchant-btn">
					<a href="{$webroot}/admin/newmerchant" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a>
				</div> -->
				
				<div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color: #262626; padding: 55px;">
					<div class="sidebar-waitingtitle ta-center" style="color: #ffffff;font-size: 25px;line-height: 25px;">Total Merchants Registered</div>
					<div class="sidebar-numbers ta-center" style="color: #ffffff;font-size: 60px;">
					{$data|@count gt 0}
					</div>
				</div>

			</section>
		</section>
	</aside>
		
 <!-- .content -->
	<section id="content">  	
		<section class="vbox">
			<div class="merchantList-wrap">
				<table id="merchant-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th hidden>ID</th>
							<th>Business Name</th>
							<th>Date Registered</th>
							<th class="no-sort">Marketplace Status</th>
							<th class="no-sort"></th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$data item=merchant}
						<tr>
							<td hidden>{$merchant.id}</td>
							<td>{$merchant.business_name}</td>
							<td>{$merchant.create_date}</td>
							<td>{if $merchant.status eq 'A'}<span class="merchant-status-enabled">Enabled</span>{else}<span class="merchant-status-disabled">Disabled</span>{/if}</td>
							<td class="ta-right"><a href="{$webroot}/admin/editmerchant?merchantid={$merchant.id}" class="merchant-edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> EDIT</a></td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
		</section>
	</section>
  


 	</section>
  </section>
</section>


{literal}
<script>
	$(document).ready(function() {
		$('#merchant-list').DataTable({
			"searching": false,
			"iDisplayLength": 5,
			"bLengthChange": false,
			"aoColumnDefs": [ {
				'bSortable': false,
				'aTargets': [ "no-sort" ] 
			} ]
		});
	});
</script>
{/literal}