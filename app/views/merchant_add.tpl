<script type="text/javascript" src="{$webroot_resources}/js/merchant.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div class="container">
		<div class="row">
			<!-- Content Header (Page header / Title / Breadcrumbs) -->
			<section class="content-header">
				<h1> Merchant <small>Profile</small></h1>
				<ol class="breadcrumb">
					<li>
						<a href="{$webroot}/merchant/management"><i class="fa fa-dashboard"></i> Merchants</a>
					</li>
					<!-- <li class="active">Merchant</li> -->
					<li class="active">
						New Merchant
					</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- Your Page Content Here -->
					<!-- <div class="col-sm-3">
					<div class="sidelink">
					<h3 class="sidelink-title">Merchant Tabs</h3>
					<ul class="sidelink-nav">
					<li><a href="summary.php">Summary</a></li>
					<li><a href="profile.php">Profile</a></li>
					<li><a href="#">Contacts</a></li>
					<li><a href="#">Payment Info</a></li>
					<li><a href="#">Products</a></li>
					<li><a href="#">Invoices</a></li>
					<li><a href="#">Attachments</a></li>
					<li><a href="#">Notes</a></li>
					<li><a href="#">Communication</a></li>
					<li><a href="#">Log</a></li>
					</ul>
					</div>
					</div> -->
					<div class="col-sm-12">
						<form role="form" method="post" name="frmRegisterMerchant" id="frmRegisterMerchant" enctype="multipart/form-data">
							<div class="box" id="merchant-info-wrap" name="merchant-info-wrap">
								<div class="box-header with-border" >
									<!-- <h3 class="box-title">Company Profile</h3> -->
								</div>
								<!-- /.box-header -->
								<!-- form start -->

								<div class="box-body">
									<div class="row">

									</div>

									<div class ="row">
										<div class="col-md-6">

										</div>
										<div class="col-md-6">

										</div>
									</div>

									<div class="row">
										<div class="col-md-6">

											<div class="box-header with-border">
												<h3 class="box-title">Company Profile</h3>
											</div>
											<div class="form-group">
												<label for="address1">Legal Business Name (as it appears on your income tax return):</label>
												<input type="text" class="form-control" id="txtCompanyName" name="txtCompanyName" placeholder="Enter Legal Business Name">
											</div>

											<div class="form-group">
												<label for="address2">DBA:</label>
												<input type="text" class="form-control" id="txtDBA" name="txtDBA" placeholder="Enter DBA">
											</div>

											<div class="box-header with-border">
												<h3 class="box-title">Business Address</h3>
											</div>

											<div class="form-group">
												<label for="address1">Address 1:</label>
												<input type="text" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
											</div>

											<div class="form-group">
												<label for="address2">Address 2:</label>
												<input type="text" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
											</div>

											<div class="form-group">
												<label for="city">City:</label>
												<input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
											</div>

											<div class="form-group">
												<label for="state">State:</label>

												<select class="form-control select2" style="width: 100%;" id="txtState" name="txtState">
													{foreach from=$states item=item}
													<option value="{$item.code}">{$item.code}</option>
													{/foreach}
												</select>

												<!-- <input type="text" class="form-control" id="txtState" name="txtState" placeholder="Enter state"> -->
											</div>

											<div class="form-group">
												<label for="zip">Zip:</label>
												<input type="text" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
											</div>

											<div class="form-group">
												<label for="country">Country:</label>
												<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
													{foreach from=$country item=item}
													<option value="{$item.name}">{$item.name}</option>
													{/foreach}
												</select>
											</div>

											<div class="form-group">
												<label for="email">Email:</label>
												<input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
											</div>

											<div class="form-group">
												<label for="phone1">Phone 1:</label>
												<input type="text" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
											</div>

											<div class="form-group">
												<label for="phone2">Phone 2:</label>
												<input type="text" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
											</div>

											<div class="form-group">
												<label for="phone2">Fax:</label>
												<input type="text" class="form-control" id="txtFax" name="txtFax" placeholder="Enter Fax">
											</div>
											<div class="box-header with-border">
												<h3 class="box-title">Mailing Address</h3>
											</div>
											<div class="form-group">
												<label>
													<input type="checkbox" id="chkSameAsBusiness" name="chkSameAsBusiness" placeholder="Enter Fax">
													Same as Business Address </label>
											</div>

											<div class="form-group">
												<label for="country">Country:</label>
												<select class="form-control select2" style="width: 100%;" id="txtCountryBusiness" name="txtCountryBusiness">
													{foreach from=$country item=item}
													<option value="{$item.name}">{$item.name}</option>
													{/foreach}
												</select>
											</div>
											<div class="form-group">
												<label for="phone2">Address:</label>
												<input type="text" class="form-control" id="txtAddressBusiness" name="txtAddressBusiness" placeholder="Enter Business Address">
											</div>

											<div class="form-group">
												<label for="phone2">City:</label>
												<input type="text" class="form-control" id="txtCityBusiness" name="txtCityBusiness" placeholder="Enter City">
											</div>

											<div class="form-group">
												<label for="phone2">State:</label>
												<select class="form-control select2" style="width: 100%;" id="txtStateBusiness" name="txtStateBusiness">
													{foreach from=$states item=item}
													<option value="{$item.code}">{$item.code}</option>
													{/foreach}
												</select>
												<!-- <input type="text" class="form-control" id="txtStateBusiness" name="txtStateBusiness" placeholder="Enter State"> -->
											</div>

											<div class="form-group">
												<label for="phone2">Zip:</label>
												<input type="text" class="form-control" id="txtZipBusiness" name="txtZipBusiness" placeholder="Enter Zip">
											</div>

										</div>

										<div class="col-md-6">

											<div class="box-header with-border">
												<h3 class="box-title">Contact Information</h3>
											</div>

											<div class="form-group">
												<label for="phone2">First Name:</label>
												<input type="text" class="form-control" id="txtFirstName" name="txtFirstName" placeholder="Enter First Name">
											</div>

											<div class="form-group">
												<label for="phone2">Last Name:</label>
												<input type="text" class="form-control" id="txtLastName" name="txtLastName" placeholder="Enter Last Name">
											</div>

											<div class="form-group">
												<label for="phone2">Position:</label>
												<input type="text" class="form-control" id="txtPosition" name="txtPosition" placeholder="Enter Position">
											</div>

											<div class="form-group">
												<label for="phone2">Mobile Number:</label>
												<input type="text" class="form-control" id="txtMobileNumber" name="txtMobileNumber" placeholder="Enter Your Mobile Number">
											</div>

											<div class="form-group">
												<label for="phone2">Business Phone 1:</label>
												<input type="text" class="form-control" id="txtOfficeNumber" name="txtOfficeNumber" placeholder="Enter Business Phone 1">
											</div>

											<div class="form-group">
												<label for="phone2">Business Phone 2:</label>
												<input type="text" class="form-control" id="txtOfficeNumber2" name="txtOfficeNumber2" placeholder="Enter Business Phone 2">
											</div>

											<div class="form-group">
												<label for="phone2">Extension:</label>
												<input type="text" class="form-control" id="txtExtension" name="txtExtension" placeholder="Enter Extension">
											</div>

											<div class="form-group">
												<label for="phone2">Fax:</label>
												<input type="text" class="form-control" id="txtContactFax" name="txtContactFax" placeholder="Enter Contact Fax">
											</div>

											<div class="form-group">
												<label for="phone2">Email Address:</label>
												<input type="text" class="form-control" id="txtContactEmail" name="txtContactEmail" placeholder="Enter Contact Email Address">
											</div>

											<div class="form-group">
												<label for="phone2">Website:</label>
												<input type="text" class="form-control" id="txtWebsite" name="txtWebsite" placeholder="Enter Website">
											</div>

											<br>
											<br>
											<div class="form-group">
												<label for="compname">Processor:</label>
												<input type="text" class="form-control" id="txtProcessor" name="txtProcessor" placeholder="Enter processor">
											</div>

											<div class="form-group">
												<label for="dba">Merchant ID:</label>
												<input type="text" class="form-control" id="txtMerchantID" name="txtMerchantID" placeholder="Enter Merchant ID">
											</div>

											<div class="box-header with-border">
												<h3 class="box-title">Payment Information</h3>
											</div>

											<div class="form-group">
												<label for="phone2">Bank Name:</label>
												<input type="text" class="form-control" id="txtBankName" name="txtBankName" placeholder="Enter Bank Name">
											</div>
											<div class="form-group">
												<label for="phone2">Account Name:</label>
												<input type="text" class="form-control" id="txtAccountName" name="txtAccountName" placeholder="Enter Account Name">
											</div>
											<div class="form-group">
												<label for="phone2">Account Number:</label>
												<input type="text" class="form-control" id="txtAccountNumber" name="txtAccountNumber" placeholder="Enter Account Number">
											</div>
											<div class="form-group">
												<label for="phone2">Routing Number:</label>
												<input type="text" class="form-control" id="txtRoutingNumber" name="txtRoutingNumber" placeholder="Enter Routing Number">
											</div>
											
											

										</div>
									</div>

									<div class="row">
										<div class="col-md-6">

										</div>
										<div class="col-md-6">

										</div>
									</div>
									<div class="row">
										<div class="col-md-6">

										</div>
										<div class="col-md-6">

										</div>
									</div>
									<div class="row">
										<div class="col-md-6">

										</div>
										<div class="col-md-6">

										</div>
									</div>
									<div class="row">
										<div class="col-md-6">

										</div>

										<div class="col-md-6">

										</div>

										<div class="col-md-6">
											<div class="form-group" style="display: none">
												<label>Partner Type:</label>
												<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
													<!-- <option selected="selected">Alabama</option>
													<option>Alaska</option>
													<option>California</option>
													<option>Delaware</option>
													<option>Tennessee</option>
													<option>Texas</option>
													<option>Washington</option> -->
													{foreach from=$partner_type item=item}
													<option value="{$item.id}"{if $item.name eq 'MERCHANT'} selected="selected" {/if}>{$item.name}</option>
													{/foreach}
												</select>

											</div>
										</div>

									</div>

									<div class="row"></div>

									<div class="row">
										<div class="col-md-12">

											<div class="form-group">
												<label>Void Check:</label>
												<input type="file" id="fileUploadVoidCheck" name="fileUploadVoidCheck">
												<!-- <p class="help-block">
												<button class="btn btn-sm btn-success" id="add-doc"><i class="fa fa-plus-circle"></i> Add</button>
												</p> -->
											</div>
											<div class="form-group">
												<label>Logo:</label>
												<input type="file" id="fileUploadLogo" name="fileUploadLogo">
											</div>
										</div>

									</div>

								</div>
								<!-- /.box-body -->

								<div class="box-footer ta-center">
									<button type="submit" class="btn btn-primary" id="btnOrderProduct" name="btnOrderProduct">
										Order Product
									</button>
								</div>

								<!-- /.box-body -->
							</div>
							<!-- /.box -->

							<div class="box-hidden" id="product-info-wrap">

								<div class="box">
									<div class="box-header with-border">
										<h3 class="box-title">Products</h3>
										<div class="box-tools">
											<a href="#" id="backtoProfile"><i class="fa fa-arrow-circle-left"></i> Back to Profile</a>
										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Product:</label>
													<select class="form-control" name="txtProduct" id="txtProduct" onchange="HideShowFields()">
														<!-- <option value="" disabled hidden="true" selected>- Select Product -</option> -->

														{foreach from=$products item=item}
														<option value="{$item.id}">{$item.name}</option>
														{/foreach}
														<!-- <option value="1">Gift &amp; Rewards</option> -->
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="box" id="divWebsite" name="divWebsite" style="display: none">

									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">Order Type</h3>
										</div>

										<div class="box-body">

											<div class="col-md-12" >
												<label>
													<input type="radio" name="txtOrderType" id="txtOrderType"  value="WEBSITE" checked="checked">
													&nbsp; &nbsp;WEBSITE </label>
												&nbsp; &nbsp;
												<label>
													<input type="radio" name="txtOrderType" id="txtOrderType" value="WEBSITE AND ONLINE ORDERING">
													&nbsp;WEBSITE AND ONLINE ORDERING </label>
											</div>
											<div class="col-md-6" >
												<div class="form-group">
													<label>Domain Name:</label>
													<input type="text" class="form-control" name="txtDomainName" id="txtDomainName">
												</div>
											</div>

											<div class="col-md-6" >
												<div class="form-group">
													<label>Template No.:</label>
													<input type="text" class="form-control" name="txtTemplateNo" id="txtTemplateNo">
												</div>
											</div>

											<div class="col-md-12" >
												<label>
													<input type="radio" name="txtPlugin" id="txtPlugin"  value="ONLINE ORDERING [PLUGIN]" checked="checked">
													&nbsp; &nbsp;ONLINE ORDERING [PLUGIN] </label>
												&nbsp; &nbsp;
												<label>
													<input type="radio" name="txtPlugin" id="txtPlugin" value="ONLINE ORDERING [STAND ALONE]">
													&nbsp;ONLINE ORDERING [STAND ALONE] </label>
											</div>

											<div class="col-md-6" >
												<div class="form-group">
													<label>Domain Name:</label>
													<input type="text" class="form-control" name="txtDomainNamePlugin" id="txtDomainNamePlugin">
												</div>
											</div>

											<div class="col-md-12" >
												<div class="form-group">
													<label>Is this an existing domain:</label>
													<label>
														<input type="radio" name="txtExistingDomain" id="txtExistingDomain" checked="checked"  value="YES">
														&nbsp; &nbsp;YES </label>
													&nbsp; &nbsp;
													<label>
														<input type="radio" name="txtExistingDomain" id="txtExistingDomain" value="NO">
														&nbsp;NO </label>
												</div>
											</div
										</div>

									</div>

								</div>
							</div>

							<div class="row" id="divWebsite2" name="divWebsite2" style="display: none">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">For Online Order Only</h3>
										</div>

										<div class="box-body">
											<div class="col-md-12" >
												<div class="form-group">
													<label>Receive orders by:</label>
													<label>
														<input type="checkbox" name="txtReceiveOrderBy[]" id="txtReceiveOrderBy[]" checked="checked"  value="PHONE">
														&nbsp; &nbsp;PHONE </label>
													&nbsp; &nbsp;
													<label>
														<input type="checkbox" name="txtReceiveOrderBy[]" id="txtReceiveOrderBy[]" value="FAX">
														&nbsp;FAX </label>
													&nbsp; &nbsp;
													<label>
														<input type="checkbox" name="txtReceiveOrderBy[]" id="txtReceiveOrderBy[]" value="EMAIL">
														&nbsp;EMAIL </label>
												</div>
											</div>

											<div class="col-md-12" >
												<div class="form-group">
													<label>Accept: </label>
													<label>
														<div class="form-group">
															<input type="checkbox" name="txtAcceptBy[]" id="txtAcceptBy[]" value="Delivery">
															&nbsp; &nbsp;Delivery
															<br>
															<label>Estimated Time:</label>
															<input type="text" class="form-control" name="txtDeliveryTime" id="txtDeliveryTime">
														</div> </label>

													&nbsp; &nbsp;
													<label>
														<div class="form-group">
															<input type="checkbox" name="txtAcceptBy[]" id="txtAcceptBy[]" value="Pickup">
															&nbsp;Pickup
															<br>
															<label>Estimated Time:</label>
															<input type="text" class="form-control" name="txtPickUpEstimatedTime" id="txtPickUpEstimatedTime">
														</div> </label>
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label>Delivery Fee:</label>
													<input type="text" class="form-control" name="txtDeliveryFee" id="txtDeliveryFee" onkeypress="return isNumberKey(event)">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Delivery Minimum:</label>
													<input type="text" class="form-control" name="txtDeliveryMinimum" id="txtDeliveryMinimum" onkeypress="return isNumberKey(event)">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Delivery Radius [Miles]:</label>
													<input type="text" class="form-control" name="txtDeliveryRadius" id="txtDeliveryRadius" onkeypress="return isNumberKey(event)">
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label>Payment Method:</label>
													<label>
														<input type="checkbox" name="txtPaymentMethod[]" id="txtPaymentMethod[]" checked="checked"  value="CASH">
														&nbsp; &nbsp;CASH </label>
													&nbsp; &nbsp;
													<label>
														<input type="checkbox" name="txtPaymentMethod[]" id="txtPaymentMethod[]" value="CREDIT CARD">
														&nbsp;CREDIT CARD </label>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label>SALES TAX:</label>
													<input type="text" class="form-control" name="txtSalesTax" id="txtSalesTax" onkeypress="return isNumberKey(event)">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Payment Gateway:</label>
													<input type="text" class="form-control" name="txtPaymentGateway" id="txtPaymentGateway">
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>

							<div class="row" id="divWebsite3" name="divWebsite3" style="display: none">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">ONLINE ORDERING COUPON CODE</h3>
										</div>
										<div class="box-body">
											<div class="col-md-6" >
												<div class="form-group">
													<label>Coupon Code:</label>
													<input type="text" class="form-control" name="txtCouponCode" id="txtCouponCode">
												</div>
											</div>
											<div class="col-md-6" >
												<div class="form-group">
													<label>Title:</label>
													<input type="text" class="form-control" name="txtCouponTitle" id="txtCouponTitle">
												</div>
											</div>

											<div class="col-md-12" >
												<div class="form-group">
													<label>Discount Type: </label>
													<label>
														<!-- <div class="form-group"> -->
															<input type="checkbox" name="txtDiscountType[]" id="txtDiscountType[]" value="Fixed Amount">
															&nbsp; &nbsp;Fixed Amount
															<input type="text" class="form-control" name="txtFixedAmount" id="txtFixedAmount">
														<!-- </div>  -->
													</label>
													<label>
														<!-- <div class="form-group"> -->
															<input type="checkbox" name="txtDiscountType[]" id="txtDiscountType[]" value="Percentage">
															&nbsp; &nbsp;Percentage
															<input type="text" class="form-control" name="txtPercentage" id="txtPercentage">
														<!-- </div>  -->
													</label>
												</div>
											</div>
											
											<div class="col-md-12" >
												<div class="form-group">
													<label>How many times can a customer use the coupon code? </label>
													<label>
														<!-- <div class="form-group"> -->
															<input type="checkbox" name="txtHowManyTimes[]" id="txtHowManyTimes[]" value="One Time">
															&nbsp;One Time
														<!-- </div>  -->
													</label>
													<label>
														<!-- <div class="form-group"> -->
															<input type="checkbox" name="txtHowManyTimes[]" id="txtHowManyTimes[]" value="Multiple">
															&nbsp;Multiple
															<input type="text" class="form-control" name="txtMultiple" id="txtMultiple">
														<!-- </div>  -->
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">Fees</h3>
										</div>

										<div class="box-body">
											<div class="col-md-6" id="divWebsite4" name="divWebsite4" style="display: none">
												<div class="form-group">
													<label>Website Design</label>
													<input type="text" class="form-control" name="txtWebsiteDesign" id="txtWebsiteDesign" onkeypress="return isNumberKey(event)">
												</div>
												<div class="form-group">
													<label>Yearly Hosting</label>
													<input type="text" class="form-control" name="txtYearlyHosting" id="txtYearlyHosting" onkeypress="return isNumberKey(event)">
												</div>
												<div class="form-group">
													<label>Online Ordering Set up</label>
													<input type="text" class="form-control" name="txtSetup" id="txtSetup" onkeypress="return isNumberKey(event)">
												</div>
												<div class="form-group">
													<label>Online Ordering Monthly Service</label>
													<input type="text" class="form-control" name="txtMonthlyService" id="txtMonthlyService" onkeypress="return isNumberKey(event)">
												</div>
											</div>
											
											
											
											
											<div class="col-md-6" id="divRewardFee" name="divRewardFee">
												<div class="form-group">
													<label>Monthly Fee:</label>
													<input type="text" class="form-control" name="txtMonthlyFee" id="txtMonthlyFee" onkeypress="return isNumberKey(event)">
												</div>
											</div>

											<div class="col-md-6" >
												<div class="form-group">
													<label>Other Fee:</label>
													<input type="text" class="form-control" name="txtOtherFeeName" id="txtOtherFeeName" placeholder="Other Fee Name">
													<br>
													<input type="text" class="form-control" name="txtOtherFee" id="txtOtherFee" placeholder="Amount" onkeypress="return isNumberKey(event)">
												</div>
											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="row" id="divRewards" name="divRewards">

								<div class="col-md-4">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">Punchcard</h3>
										</div>
										<div class="box-body">
											<div class="form-group">
												<label>Number of Holes:</label>
												<input type="number" class="form-control" name="txtNoOfHole" id="txtNoOfHole">
											</div>
											<div class="form-group">
												<label>Start Date:</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right datepicker" name="txtStartDatePC" id="txtStartDatePC">
												</div>
											</div>
											<div class="form-group">
												<label>End Date:</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right datepicker" name="txtEndDatePC" id="txtEndDatePC">
												</div>
											</div>
											<div class="form-group">
												<label>Description:</label>
												<textarea class="form-control custom-textarea" rows="3" id="txtDescriptionPC" name="txtDescriptionPC"></textarea>
											</div>
											<div class="form-group">
												<label>Freebie Description:</label>
												<textarea class="form-control custom-textarea" rows="3" id="txtFreebieDescription" name="txtFreebieDescription"></textarea>
											</div>
											<div class="form-group">
												<label>Hole Position:</label>
												<input type="text" class="form-control" name="txtFreebieHolePosition" id="txtFreebieHolePosition">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">Promotions</h3>
										</div>
										<div class="box-body">
											<div class="form-group">
												<label>Is Unlimited:</label>
												<div class="custom-checkboxgrp">
													<label>
														<input type="checkbox" name="chkIsUnlimited" id="chkIsUnlimited" class="minimal">
														Create as Unlimited </label>
												</div>
											</div>
											<div class="form-group">
												<label>Start Date:</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right datepicker" name="txtStartDatePromo" id="txtStartDatePromo">
												</div>
											</div>
											<div class="form-group">
												<label>End Date:</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right datepicker" name="txtEndDatePromo" id="txtEndDatePromo">
												</div>
											</div>
											<div class="form-group">
												<label>Description:</label>
												<textarea class="form-control custom-textarea" rows="4" name="txtDescriptionPromo" id="txtDescriptionPromo"></textarea>
											</div>
											<div class="form-group">
												<label>Required Points:</label>
												<input type="number" class="form-control" name="txtRequiredPoints" id="txtRequiredPoints">
											</div>
											<div class="form-group">
												<label>Required Amount:</label>
												<input type="text" class="form-control" name="txtRequiredAmount" id="txtRequiredAmount">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="box">
										<div class="box-header with-border">
											<h3 class="box-title">Loyalty</h3>
										</div>
										<div class="box-body">
											<div class="form-group">
												<label>Loyalty Name:</label>
												<input type="text" class="form-control" name="txtLoyaltyName" id="txtLoyaltyName">
											</div>
											<div class="form-group">
												<label>Description:</label>
												<textarea class="form-control custom-textarea" rows="4" id="txtLoyaltyDescription" name="txtLoyaltyDescription"></textarea>
											</div>
											<div class="form-group">
												<label>Points Reset After:</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right datepicker" name="txtDateReset" id="txtDateReset">
												</div>
											</div>
											<h4>Conversions</h4>
											<div class="form-group">
												<label>Points Conversion:</label>
												<div class="custom-convert-wrap">
													$
													<input type="text" class="form-control custom-convert-input" name="txtDollarAmount" id="txtDollarAmount">
													=
													<input type="text" class="form-control custom-convert-input" name="txtPointConversion" id="txtPointConversion">
													Points
												</div>
											</div>
											<div class="form-group">
												<label>Amount Conversion:</label>
												<div class="custom-convert-wrap">
													Points
													<input type="text" class="form-control custom-convert-input" name="txtPoints" id="txtPoints">
													=
													<input type="text" class="form-control custom-convert-input" name="txtAmountConversion" id="txtAmountConversion">
													$
												</div>
											</div>
											<div class="form-group">
												<label>Goal Points:</label>
												<input type="text" class="form-control" name="txtGoalPoints" id="txtGoalPoints">
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="ta-center">
								<button type="submit" class="btn btn-primary" id="btnSubmit" name="btnSubmit">
									Submit
								</button>
							</div>
					</div>

					</form>
				</div>
		</div>
		</section>
		<!-- /.content -->
	</div>
</div>
</div>
<!-- /.content-wrapper -->