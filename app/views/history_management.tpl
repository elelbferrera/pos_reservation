<script type="text/javascript" src="{$webroot_resources}/js/history.js"></script>

<input type="hidden" name="txtURL" id="txtURL" value="{$webroot}/history/management">
<section id="content">
                        <section class="vbox" id="paginate-table">  


                        <section class="scrollable">


                          <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-user titleFA"></i> 
                                                <p class="headeerpage-title">Reservation List</p>
                                            </div>
                                        </div>
                                     </div>
                                      <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2">
                                            <div class="btn-group">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right datepicker" id="txtSourceDate" name="txtSourceDate" value="{$date_filtered}">
                                                </div>
                                            </div>

                                          

                                            <!-- <div class="btn-group">
                                                <div class="input-group">
                                                   <input type="text" class="form-control" placeholder="Search">
                                               </div>
                                            </div>
                                             <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary btn-icon" title="New project"><i class="fa fa-search"></i></button>
                                             </div> -->
                                        </div>
                                     </div>
                                     <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search customer name" />
                                        </div>
                                     </div>
                                </div>
                            </header>





                              <!-- table -->
                              <table id="tbUser" class="waitlist-table">
                                 <thead>
                                    <tr class="header bg-gray b-b b-light">
                                      <th class="waitlist-table-td-th" style="color: #FFF">Reservation Date</th>
                                      <th class="waitlist-table-td-th" style="color: #FFF">Time</th>
                                      <th class="waitlist-table-td-th" style="color: #FFF">Customer Name</th>
                                      <th class="waitlist-table-td-th" style="color: #FFF">Contact Number</th>
                                      <th class="waitlist-table-td-th" style="color: #FFF">Services</th>
                                      <th class="waitlist-table-td-th" style="color: #FFF">Status</th>
                                      <!-- <th class="waitlist-table-td-th" style="text-align: center;color: #FFF;">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody class="list">
									 {foreach from=$reservations item=item}
									                  <tr>
                                      <td class="waitlist-table-td-th">{$item.reservation_date}</td>
                                      <td class="waitlist-table-td-th">{$item.normal_time}</td>
                                      <td class="waitlist-table-td-th customer-name">{$item.customer_name}</td>
                                      <td class="waitlist-table-td-th">{$item.mobile}</td>
                                      <td class="waitlist-table-td-th">{$item.services_get}</td>
                                      <td class="waitlist-table-td-th">{$item.status}</td>
                                      
                                    </tr>
									{/foreach}
                               </tbody>     
                              </table>
                              <!-- /. table -->
                                <div class="clearfix"></div>
                                
                                    <div class="col-lg-12 pull-right">
                                       <ul class="pagination"></ul>
                                    </div>
                               


                              </section>



                              
                       </section>
                     </section>

                <!-- /.content -->
               
            </section>
        </section>
    </section>






<!-- Modal -->
<div id="question" class="modal fade" role="dialog">
  <div class="modal-dialog3">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ta-center">
                      <p>Are you sure want to delete?</p>
                      <button type="button" class="btn btn-md btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#updateservices">Yes</button>
                      <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        
  </div>
</div>



<div id="dlgEditUser" name="dlgEditUser" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit User</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
                   <form role="form" name="frmUser" id="frmUser">
                   			<input type="hidden" name="txtUserId" id="txtUserId" />
                                               <div class="row">
                                                 <div class="col-md-6 col-xs-6">
                                                      <div class="form-group" id="divUsername" name="divUsername">
                                                            <label>Username</label>
                                                            <input type="text" class="form-control" placeholder="Username" id="txtUsername" name="txtUsername"> 
                                                       </div>
                                                       <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" id="txtFirstName" name="txtFirstName"> 
                                                       </div>
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" id="txtLastName" name="txtLastName"> 
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6 col-xs-6">
                                                      <div class="form-group" id="divPassword" name="divPassword">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control" name="txtPassword" id="txtPassword"> 
                                                       </div>
                                                       <div class="form-group" id="divRepeatPassword" name="divRepeatPassword">
                                                            <label>Repeat Password</label>
                                                            <input type="password" class="form-control" name="txtRepeatPassword" id="txtRepeatPassword"> 
                                                       </div>
                                                       <div class="form-group">
                                                            <label>Email Address</label>
                                                            <input type="text" class="form-control" id="txtEmailAddress" name="txtEmailAddress"> 
                                                       </div>
                                                  </div>
                                               </div>
                                              
                                              <!-- <div class="row">
                                                 <div class="col-md-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>User Level</label>
                                                                <select name="txtUserType" id="txtUserType" class="form-control m-b">
                                                                    <option>Admin</option>
                                                                    <option>Staff</option>
                                                                </select>
                                                        </div>
                                                  </div>
                                               </div> -->

                                              
				</form>                      
    
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
           			<button type="button" name="btnSaveUser" id="btnSaveUser" class="btn btn-primary" data-dismiss="modal">Save</button>
                   	<button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>



  </div>
</div>