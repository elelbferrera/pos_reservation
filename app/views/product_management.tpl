	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Products
                    <a href="{$webroot}/product/add">
                    	<button class="btn btn-primary" type="button">Add New Product</button>
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Product</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Product List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Description</th>
                                                <th>Create Date</th>
                                                <th>Buy Rate</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	{foreach from=$products item=item}
	                                            <tr>
	                                            	<td><a href="{$webroot}/product/edit/{$item.id}">{$item.name}</a></td>
	                                                <td>{$item.description}</td>
	                                                <td>{$item.create_date}</td>
	                                                <td>{$item.buy_rate}</td>
	                                            </tr>
                                            {/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->