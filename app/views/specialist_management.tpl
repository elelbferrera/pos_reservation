<script type="text/javascript" src="{$webroot_resources}/js/specialist.js"></script>
<style type="text/css">
  {literal}
    .datepicker{
      z-index: 10000 !important;
    }
  {/literal}
</style>
<input type="hidden" value="{$webroot}" name="txtURL" id="txtURL">
                 <!-- .content -->
                   <section id="content">
                        <section class="vbox" id="paginateDiv">
                            <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-users titleFA"></i> 
                                                <p class="headeerpage-title">Specialist</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2">
                                            <!-- <div class="btn-group" style="margin-right:10px;">
                                                      <select class="form-control" name="repeatReservation">
                                                      <option selected="selected" value="">- sort by branch -</option>
                                                      <option>Bulacan Branch</option>
                                                      <option>Ortigas</option>
                                                      <option>Cubao</option>
                                                      <option>US</option>
                                                  </select>
                                            </div> -->
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary" onclick="add()">New Specialist</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                     <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search specialist name" />
                                        </div>
                                     </div>
                                </div>
                            </header>
                            <section class="scrollable wrapper">
                                 <!-- <div id="example" class="overview"> -->
                                        
                                        <!-- <div class="viewport">
                                            <ul class="overview"> -->
                                            <ul id="example" class="overview list paginated-list">
                                            {foreach from=$specialist item=item}
                                            
                                                <li class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <div class="specialist-header-list">Specialist ID #{$item.id}</div>
                                                        <div class="specialist-body-list">
                                                            <div class="row">
                                                                <div class="col-md-4  col-sm-4 col-xs-4">
                                                                	  {if $item.profile_picture eq ''}
                                                                	  	<img src="{$webroot_resources}/images/avatar_default.jpg" class="specialist-avatar" />
                                                                	  {else}
                                                                	  	<img src="{$item.profile_picture}" class="specialist-avatar" />
                                                                	  {/if}	
                                                                      <div class="clear"></div>
                                                                      <!--    <div class="ta-center" style="margin-top:10px;">
                                                                             <div class="price-services">14</div>
                                                                             <p>Total Service today</p>
                                                                         </div> -->
                                                                         <div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
                                                                         <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
                                                                         <div style="display: none" id="{$item.id}branch_id" name="{$item.id}branch_id">{$item.branch_id}</div>
                                                                         <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
                                                                         <div style="display: none" id="{$item.id}middle_name" name="{$item.id}middle_name">{$item.middle_name}</div>
                                                                         <div style="display: none" id="{$item.id}birth_date" name="{$item.id}birth_date">{$item.birth_date}</div>
                                                                         {if $item.profile_picture eq ''}
                                                                         	<div style="display: none" id="{$item.id}profile_picture" name="{$item.id}profile_picture">{$webroot_resources}/images/avatar_default.jpg</div>
                                                                         {else}
                                                                         	<div style="display: none" id="{$item.id}profile_picture" name="{$item.id}profile_picture">{$item.profile_picture}</div>
                                                                         {/if}
                                                                         
                                                                         <div style="display: none" id="{$item.id}gender" name="{$item.id}gender">{$item.gender}</div>
                                                                         <div style="display: none" id="{$item.id}city" name="{$item.id}city">{$item.city}</div>
                                                                         <div style="display: none" id="{$item.id}address" name="{$item.id}address">{$item.address}</div>
                                                                         <div style="display: none" id="{$item.id}state" name="{$item.id}state">{$item.state}</div>
                                                                         <div style="display: none" id="{$item.id}country" name="{$item.id}country">{$item.country}</div>
                                                                         <div style="display: none" id="{$item.id}schedule" name="{$item.id}schedule">{$item.schedule}</div>
                                                                         <div style="display: none" id="{$item.id}start_time" name="{$item.id}start_time">{$item.start_time}</div>
                                                                         <div style="display: none" id="{$item.id}end_time" name="{$item.id}end_time">{$item.end_time}</div>
                                                                         <div style="display: none" id="{$item.id}start_date" name="{$item.id}start_date">{$item.start_date}</div>
                                                                         <div style="display: none" id="{$item.id}end_date" name="{$item.id}end_date">{$item.end_date}</div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-8 ta-right">
                                                                    <div class="specialist-name">{$item.first_name} {$item.last_name}<br />
                                                                          
                                                                          {assign var="productids" value=""}
                                                                          <p>
                                                                          Services: 
                                                                          <!-- <span>  -->
                                                                          {foreach from=$item.products item=product}
                                                                          		
                                                                          		{assign var=productids value=$productids|cat:$product.id}
                                                                          		{assign var=productids value=$productids|cat:','}
                                                                          		
                                                                          		{$product.name},
                                                                          {/foreach}	
                                                                             </p> 
                                                                          
                                                                          <div style="display: none" id="{$item.id}services" name="{$item.id}services">{$productids}</div>
                                                                          <div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
                                                                          <div style="display: none" id="{$item.id}email_address" name="{$item.id}email_address">{$item.email_address}</div>

                                                                          {assign var="new_day" value=""}
                                                                          {assign var="new_start_time" value=""}
                                                                          {assign var="new_end_time" value=""}
                                                                          {assign var="new_is_off" value=""}

                                                                          {foreach from=$item.new_schedule item=sched}
                                                                             {assign var=new_day value=$new_day|cat:$sched.day}
                                                                             {assign var=new_day value=$new_day|cat:','}

                                                                             {assign var=new_start_time value=$new_start_time|cat:$sched.start_time}
                                                                             {assign var=new_start_time value=$new_start_time|cat:','}
                                                                             
                                                                             {assign var=new_end_time value=$new_end_time|cat:$sched.end_time}
                                                                             {assign var=new_end_time value=$new_end_time|cat:','}

                                                                             {assign var=new_is_off value=$new_is_off|cat:$sched.is_off}
                                                                             {assign var=new_is_off value=$new_is_off|cat:','}
                                                                          {/foreach}  

                                                                          <div style="display: none" id="{$item.id}new_day" name="{$item.id}new_day">{$new_day}</div>
                                                                          <div style="display: none" id="{$item.id}new_start_time" name="{$item.id}new_start_time">{$new_start_time}</div>
                                                                          <div style="display: none" id="{$item.id}new_end_time" name="{$item.id}new_end_time">{$new_end_time}</div>
                                                                          <div style="display: none" id="{$item.id}new_is_off" name="{$item.id}new_is_off">{$new_is_off}</div>
                                                                            
                                                                          <!-- </span> -->
                                                                          <br />

                                                                      
                                                                          
                                                                          <!-- <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#newspecialist">View</button> -->
                                                                          <button type="button" class="btn btn-md btn-dark" onclick="edit('{$item.id}')">Edit</button>
                                                                          <button type="button" class="btn btn-sm btn-md btn-delete" onClick="delete_specialist('{$item.id}')">Delete</button>
                                                                         
                                                                         <!--  <div class="specialist-status-onservice col-sm-7">On Service</div> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- <div class="row">
                                                            	<div class="col-md-12 col-sm-12 col-xs-12">
                                                            		<div class="specialist-name">
                                                            			<p>Schedule: <span>
                                                                  {$item.schedule|replace:',':', '}
                                                                   </span><br /></p>
                                                            			<p>Schedule Time: <span> {$item.start_time} - {$item.end_time}</span><br /></p>
                                                            		</div>
                                                            	</div>		
                                                            	
                                                            </div> -->
                                                           
                                                        </div>
                                                    </div>
                                                </li>
                                                {/foreach}
                                            </ul>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-lg-12 pull-right">
                                                    <ul class="pagination"></ul>
                                                </div>
                                            </div>
                            </section>
                        </section>
                  </section>
                <!-- /.content -->
               
         </section>
        </section>
    </section>









<div id="dlgEditSpecialist" name="dlgEditSpecialist" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Specialist Information</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
      <div class="alert alert-danger error-prompt" role="alert" hidden>
          <div class="error-holder">
           
          </div>
      </div>
                    
      <div class="row">
             <form role="form" id="frmSpecialist" name="frmSpecialist" enctype="multipart/form-data">
             	<input type="hidden" id="txtSpecialistId" name="txtSpecialistId">
             	<input type="hidden" id="txtDays" name="txtDays">
             	<input type="hidden" id="txtServices" name="txtServices">
             	<input type="hidden" id="txtStartTime" name="txtStartTime">
             	<input type="hidden" id="txtEndTime" name="txtEndTime">
                 <div class="col-md-3 col-xs-3">
                    <div class="ta-center" style="margin-top:20px;">
                          <img id="imgProfile" name="imgProfile" src="{$webroot_resources}/images/specialist-avatar-new.png" class="specialist-avatar specialist-avatar-add" /><br />
                          <input type="file" name="file" id="file" class="inputfile"   /> 
                          <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Upload Photo</label>  
                          <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule"><i class="fa fa-calendar"></i> Days of Schedule</button>
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setservices"><i class="fa fa"></i> Services</button>
                    </div>
                 </div>
                 <div class="col-md-9 col-xs-9">
                    <div class="row" style="display:none;">
                       <div class="col-md-4 col-xs-4 col-xs-offset-8">
                             <div class="form-group">
                                <label>Branch</label>
                                    <select name="txtBranch" id="txtBranch" class="form-control m-b">
                                        {foreach from=$branches item=item}
                                        	<option value="{$item.id}">{$item.branch_name}</option>
                                        {/foreach}
                                    </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>First Name <span class="req">*</span></label>
                                <input type="text" id="txtFirstName" name="txtFirstName" class="form-control" placeholder="First name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" id="txtMiddleName" name="txtMiddleName" class="form-control" placeholder="Middle name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Last Name <span class="req">*</span></label>
                                <input type="text" id="txtLastName" name="txtLastName" class="form-control" placeholder="Last name"> 
                              </div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Gender</label>
                                    <select name="txtGender" id="txtGender" class="form-control m-b">
                                        <option value="MALE">MALE</option>
                                        <option value="FEMALE">FEMALE</option>
                                    </select>
                              </div>
                        </div>
                        <!-- <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Status</label>
                                    <select name="branch" id="assignbranch" class="form-control m-b">
                                        <option>Single</option>
                                        <option>Married</option>
                                    </select>
                              </div>
                        </div> -->
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                  <label>Birth Date: <span class="req">*</span></label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right datepicker" id="txtBirthDate" name="txtBirthDate">
                                      <!-- <input type="text" class="form-control pull-right datepicker" id="txtDateBirth" name="txtDateBirth"> -->
                                  </div>
                              </div>
                        </div>
                        
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                  <label>Contact Number: <span class="req">*</span></label>
                                  <div class="form-group">
                                      
                                      <input type="text" id="txtContactNumber" name="txtContactNumber"  class="form-control" placeholder="Contact Number">
                                      <!-- <input type="text" class="form-control pull-right datepicker" id="txtDateBirth" name="txtDateBirth"> -->
                                  </div>
                              </div>
                        </div>
                        
                    </div>

                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                          <div class="form-group">
                                <label>Address</label>
                                <textarea rows="2" cols="50" id="txtAddress" name="txtAddress"  class="form-control"></textarea>
                           </div>
                      </div>
                    </div>


                     <div class="row">
                           <div class="col-md-4 col-xs-4">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" id="txtCity" name="txtCity" class="form-control" placeholder="City">
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4" style="display: none;">
                                  <div class="form-group">
                                    <label>Country</label>
                                        <select name="txtCountry" id="txtCountry" class="form-control m-b">
                                            {foreach from=$countries item=item}
                                        		<option value="{$item.name}" {if $item.name eq 'United States'} selected="selected"   {/if} >{$item.name}</option>
                                        	{/foreach}
                                        </select>
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                 <div class="form-group">
                                    <label>State</label>
                                        <input type="text" id="txtState" name="txtState" class="form-control" placeholder="State">
                                  </div>
                            </div>
                            
                            <div class="col-md-4 col-xs-4">
                                 <div class="form-group">
                                      <label>Start Date <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtStartDate" name="txtStartDate">
                                      </div>
                                 </div>
                            </div>
                    </div>
                    
                    <div class="row">
                       <div class="col-md-12 col-xs-12">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="text" id="txtEmailAddress" name="txtEmailAddress" class="form-control" placeholder="Email Address">
                              </div>
                        </div>
                    </div>

                    <div class="row">
                          <!-- <div class="col-md-6 col-xs-6">
                                 <div class="form-group">
                                      <label>Start Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtStartDate" name="txtStartDate">
                                      </div>
                                 </div>
                            </div> -->
                            <div class="col-md-6 col-xs-6" style="display: none;">
                                  <div class="form-group">
                                      <label>End Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtEndDate" name="txtEndDate">
                                      </div>
                                  </div>
                            </div>
                    </div>

                 </div>
             
	  </div>
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
                   <button type="submit" class="btn btn-primary" id="btnSaveSpecialist" name="btnSaveSpecialist">Save Information</button>
                   <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>

	</form>

  </div>
</div>


<div id="setservices" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Service</h4>
                
        </div>
        
        <div class="modal-content">
             <div class="row">
             	<div class="col-lg-12">
              <ul class="nav nav-tabs" role="tablist">
                {foreach from=$categories key=key item=item}
                  {if $key==0}
                  <li role="presentation" class="active"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {else}
                  <li role="presentation"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {/if}
                {/foreach}
              </ul>
              <div class="tab-content">
             {foreach from=$categories key=key item=item2}
              {if $key==0}
              <div role="tabpanel" class="tab-pane active" id="cat{$key}">
              {else}
              <div role="tabpanel" class="tab-pane" id="cat{$key}">
              {/if}

              {foreach from=$services item=item}
                {if $item.category_name == $item2.name}
                <div class="col-md-4 col-sm-4 col-xs-4">
                          <label style="margin-right: 5px; font-size: 14px;">
                            <input type="checkbox" name="services[]" id="services[]" value="{$item.id}" class="flat-red"> {$item.name}</label>
                    </div>

                {/if}
              {/foreach}
              </div>
             {/foreach}
             </div>
             </div>
             </div>
		</div>
		
		<!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnCheckAllService" id="btnCheckAllService">Check All Services</button>
                <button class="btn btn-primary" name="btnUncheckAllService" id="btnUncheckAllService">Uncheck All Services</button>
                <button class="btn btn-primary" name="btnSetService" id="btnSetService">Set Service</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
		
	</div>
</div>

<!-- Modal -->
<div id="setschedule" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <input type="hidden" id="business_hours" name="business_hours" value='{$business_hours}'>

            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Schedule</h4>
                <p>Create Schedule for new Specialist</p>
            </div>




            <!-- Modal content-->
            <div class="modal-content">
                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                      <div class="col-md-6 col-sm-6">
                             <div class="col-md-12 col-sm-12">
                                        <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Monday" class="flat-red"> Monday</label>
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                       <label>End Time:</label>
                                      <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                     </div>
                              </div>
                      </div>  
                      <div class="col-md-6 col-sm-6">
                          <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Tuesday"  class="flat-red"> Tuesday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                              <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                        <div class="input-group">
                                          <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                          <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                               <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                        <div class="input-group">
                                          <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                          <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                      </div>



                  </div>

                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                      <div class="col-md-6 col-sm-6">
                         <div class="col-md-12 col-sm-12">
                                <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Wednesday"  class="flat-red"> Wednesday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                               <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                          <div class="col-md-12 col-sm-12">
                                <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Thursday"  class="flat-red"> Thursday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          
                                          <div class="input-group">
                                            <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                               </div>
                          </div>
                      </div>        
                  </div>




                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                          <div class="col-md-6 col-sm-6">
                             <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Friday" class="flat-red"> Friday</label>
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                      <div class="form-group">
                                            
                                            <div class="input-group">
                                              <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                              <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                            </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                   <label>End Time:</label>
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>  
                          </div>
                          <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-12 col-sm-12">
                                      <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Saturday" class="flat-red"> Saturday</label>
                                </div>
                                  <div class="col-md-6 col-sm-6">   
                                     <label>Start Time:</label>
                                      <div class="bootstrap-timepicker">
                                          <div class="form-group">
                                                <div class="input-group">
                                                  <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6">  
                                       <label>End Time:</label>
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                  <div class="input-group">
                                                    <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                  </div>
                                            </div>
                                        </div>
                                  </div> 
                             </div>        
                         
                   </div>


                    <div class="row" style="margin-top: 10px;">
                          <div class="col-md-6 col-sm-6">
                                <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Sunday" class="flat-red"> Sunday</label> 
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                   <label>End Time:</label>
                                   <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-6">
                          </div>
                     </div>







                  

               

                <!-- <div class="row" style="margin-top:20px;">
                      <div class="col-md-6 col-xs-6">
                         <div class="bootstrap-timepicker">
                            <div class="form-group">
                                  <label>Start Time:</label>
                                  <div class="input-group">
                                    <input type="text" id="txtSourceStartTime" name="txtSourceStartTime" class="form-control timepicker">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                  </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-xs-6">
                          <div class="bootstrap-timepicker">
                              <div class="form-group">
                                    <label>End Time:</label>
                                    <div class="input-group">
                                      <input type="text" id="txtSourceEndTime" name="txtSourceEndTime" class="form-control timepicker">
                                      <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    </div>
                              </div>
                          </div>
                      </div>
                </div> -->

          </div>


            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnSetSchedule" id="btnSetSchedule">Set Schedule</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</div>