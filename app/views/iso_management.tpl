<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>

<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/iso.png" alt="Click to hide" style="height: 30px;width: 30px;" />List of ISO</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             ISO</h4>
             
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterISO(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New ISO </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>       
                    <th width="10%">ISO Code</th>
                    <th width="15%">Company Name</th>
                    <th width="10%">DBA Name</th>
                    <th width="10%">City</th>
                    <th width="10%">State</th>
                    <th width="10%">Zip</th>
                    <th width="10%">Country</th>
                     <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                
                {foreach from=$isos item=item}
                 <tr>    
                    <td>{$item.iso_code}</td> 
                    <td>{$item.company_name}</td> 
                    <td>{$item.dba_name}</td> 
                    <td>{$item.city}</span></td> 
                    <td>{$item.state}</td>
                    <td>{$item.zip}</td>
                    <td>{$item.country}</td>
                    <td>
                    <!--id, iso_code, company_name, dba_name, address1, address2, city, state, zip, country, contact_no1, contact_no2, internal_id,
account_number, routing_number, udf1, udf2, udf3, parent_iso_id, first_name, last_name, email, admin_contact_no1, admin_contact_no2,
admin_udf1, admin_udf2, admin_udf3, username, password-->
                    
                        <a href="#" onclick="EditISO('{$item.id}', '{$item.iso_code}', '{$item.company_name}', '{$item.dba_name}', '{$item.address1}', '{$item.address2}', '{$item.city}', '{$item.state}', '{$item.zip}', '{$item.country}', '{$item.contact_number_1}', '{$item.contact_number_2}', '{$item.internal_id}',
'{$item.account_number}', '{$item.routing_number}', '{$item.udf1}', '{$item.udf2}', '{$item.udf3}', '{$item.parent_iso_id}', '{$item.first_name}', '{$item.last_name}', '{$item.email}', '{$item.admin_contact_number1}', '{$item.admin_contact_number2}','{$item.admin_udf1}', '{$item.admin_udf2}', '{$item.admin_udf3}', '{$item.username}', '{$item.password}')">Edit</a>
                        <a href="#" onclick="DeleteISO('{$item.id}')">Delete</a>
                        
                        
                    </td>      
                </tr>
                {/foreach}    
        
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgISO" title="New ISO" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabISO1" class="active"><a href="#tab1" class="glyphicons notes" data-toggle="tab"><i></i>ISO Information</a></li>
                <li id="tabISO2"><a href="#tab2" class="glyphicons adress_book" data-toggle="tab"><i></i>ISO Contact Info</a></li>
                <li id="tabISO3"><a href="#tab3" class="glyphicons old_man" data-toggle="tab"><i></i>ISO Admin</a></li>
              </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           
        <form id="frmRegisterISO" name="frmRegisterISO">
            <input type="hidden"  id = "txtISOId" name = "txtISOId"/>  
            <input type="hidden"  id = "txtParentISOId" name = "txtParentISOId"/>
            
            <div id ="divISOStep1"> 
                <table width="100%">
                <tbody>
                   <tr>
                        <td class="tbl_label">ISO Code:</td>
                        <td>
                            <input type="text" name="txtISOCode" id="txtISOCode" size="29" maxlength="20" />
                         </td>
                    </tr>
                   
                   <tr>
                        <td class="tbl_label">Company Name:</td>
                        <td>
                            <input type="text" name="txtCompanyName" id="txtCompanyName" size="29" maxlength="60" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">DBA Name:</td>
                        <td>
                            <input type="text" name="txtDBAName" id="txtDBAName" size="29" maxlength="60" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Internal ID:</td>
                        <td>
                            <input type="text" name="txtInternalID" id="txtInternalID" size="29" maxlength="20" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Account Number:</td>
                        <td>
                            <input type="text" name="txtAccountNumber" id="txtAccountNumber" size="29" maxlength="20" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Routing Number:</td>
                        <td>
                            <input type="text" name="txtRoutingNumber" id="txtRoutingNumber" size="29" maxlength="20" />
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">UDF 1:</td>
                        <td>
                            <input type="text" name="txtUDF1" id="txtUDF1" size="29" maxlength="100" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF 2:</td>
                        <td>
                            <input type="text" name="txtUDF2" id="txtUDF2" size="29" maxlength="100" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF 3:</td>
                        <td>
                            <input type="text" name="txtUDF3" id="txtUDF3" size="29" maxlength="100" />
                         </td>
                    </tr>
                    
                </tbody>
            </table>
            </div>
            <div id ="divISOStep2" style="display:none"> 
                <table width="100%">
                <tbody>
                    <tr>
                        <td class="tbl_label">Address:</td>
                        <td>
                            <textarea  name="txtAddress1" id="txtAddress1"  maxlength="100" rows="1" ></textarea>
                         </td>
                         
                   </tr> 

                   <tr>
                        <td class="tbl_label">Address 2:</td>
                        <td>
                            <textarea  name="txtAddress2" id="txtAddress2"  maxlength="100" rows="1"></textarea>
                         </td>
                         
                   </tr> 
                   <tr>
                        <td class="tbl_label">City:</td>
                        <td>
                            <input type="text" name="txtCity" id="txtCity" size="29" maxlength="80" />
                         </td>      
                   </tr> 
                   <tr>
                        <td class="tbl_label">State:</td>
                        <td>
                            <input type="text" name="txtState" id="txtState" size="29" maxlength="80" />
                         </td>
                    </tr> 
                    <tr>
                        <td class="tbl_label">Zip:</td>
                        <td>
                            <input type="text" name="txtZip" id="txtZip" size="29" maxlength="80"/>
                         </td>
                    </tr> 
                    
                    <tr>
                        <td class="tbl_label">Country:</td>
                        <td>
                            {html_options id="txtCountry" name="txtCountry" options="$countries"}
                         </td>
                    </tr> 
                    <tr>
                        <td class="tbl_label">Contact Number 1:</td>
                        <td>
                            <input type="text" name="txtContactNumber1" id="txtContactNumber1" size="20"/>
                         </td>
                    </tr> 
                    <tr>
                        <td class="tbl_label">Contact Number 2:</td>
                        <td>
                            <input type="text" name="txtContactNumber2" id="txtContactNumber2" size="20"/>
                         </td>
                    </tr> 
                   </tbody>
                   </table>
            </div>
                               
            <div id ="divISOStep3" style="display:none"> 
                <table width="100%">
                <tbody>
                    <tr>
                        <td class="tbl_label">First Name:</td>
                        <td>
                            <input type="text" name="txtFirstName" id="txtFirstName" size="29" maxlength="80" />
                         </td>
                    </tr>
                                 
                   <tr>
                        <td class="tbl_label">Last Name:</td>
                        <td>
                            <input type="text" name="txtLastName" id="txtLastName" size="29" maxlength="80" />
                         </td>
                         
                   </tr> 

                   <tr>
                        <td class="tbl_label">Email Address:</td>
                        <td>
                            <input type="text" name="txtEmail" id="txtEmail" size="29" maxlength="80" />
                         </td>
                         
                   </tr> 
                   <tr>
                        <td class="tbl_label">Contact Number 1:</td>
                        <td>
                            <input type="text" name="txtContactNumber1Admin" id="txtContactNumber1Admin" size="29" maxlength="20" />
                         </td>      
                   </tr> 
                   <tr>
                        <td class="tbl_label">Contact Number 2:</td>
                        <td>
                            <input type="text" name="txtContactNumber2Admin" id="txtContactNumber2Admin" size="29" maxlength="20" />
                         </td>
                    </tr> 
                    <tr>
                        <td class="tbl_label">UDF 1:</td>
                        <td>
                            <input type="text" name="txtUDFAdmin1" id="txtUDFAdmin1" size="29" maxlength="100" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF 2:</td>
                        <td>
                            <input type="text" name="txtUDFAdmin2" id="txtUDFAdmin2" size="29" maxlength="100" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">UDF 3:</td>
                        <td>
                            <input type="text" name="txtUDFAdmin3" id="txtUDFAdmin3" size="29" maxlength="100" />
                         </td>
                    </tr>
                    <tr id="trUsername" name="trUsername">
                        <td class="tbl_label">Username:</td>
                        <td>
                            <input type="text" name="txtUsername" id="txtUsername" size="29" maxlength="80" />
                         </td>
                    </tr>
                    <tr id="trPassword" name="trPassword">
                        <td class="tbl_label">Password:</td>
                        <td>
                            <input type="password" name="txtPassword" id="txtPassword" size="29" maxlength="80" />
                         </td>
                    </tr>     
                </tbody>
            </table>
            
            </div>
            
            
            </div>
            

            <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnRegisterISO" id="btnRegisterISO" value="Register ISO" class="btn btn-info"/>
                <input type="button"  name="btnUpdateISO" style="display:none" id="btnUpdateISO" value="Update ISO" class="btn btn-info"/>
                
                <input type="button"  name="btnCancelNewISO" id="btnCancelNewISO" value="Cancel" class="btn btn-danger btn-small"/>
            </div>

          
        </form> 
        </div>   
        </div> 
        </div>
        </div>
        

        
        
        
        
        {$_messages_}
    
    
    </div>
</div>
</div>


        
