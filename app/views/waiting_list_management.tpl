<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/waitlist.js"></script>



<input type="hidden" name="txtSourceDate" id="txtSourceDate" value="{$current_date}">




<!-- Main content -->
    <section class="content">

<section class="vbox" id="paginate-table">  


                        <section class="scrollable">


                          <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-book titleFA"></i> 
                                                <p class="headeerpage-title">Waiting List</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2">
                                            <div class="btn-group" onclick="add_waiting()">
                                                   <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                                      <div class="btn-group hidden-nav-xs">
                                                           <button  class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#newservices">Add Waiting Customer</button>
                                                    </div>
                                            </div>
                                            <!-- <div class="btn-group">
                                                <div class="input-group">
                                                   <input type="text" class="form-control" placeholder="Search">
                                               </div>
                                            </div>
                                             <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary btn-icon" title="New project"><i class="fa fa-search"></i></button>
                                             </div> -->
                                        </div>
                                     </div>
                                     <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search customer name" />
                                        </div>
                                     </div>
                                </div>
                            </header>



      <!-- <div class="row"> -->
        <!-- <div class="col-xs-12"> -->
          <!-- <div class="box"> -->

            <!-- /.box-header -->
            <!-- <div class="box-body"> -->

            
              <table id="example2" class="waitlist-table">
                <thead>
                <tr>
                  <th style="color: #FFF;" class="waitlist-table-td-th">Customer Name</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Contact No</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Check In Time</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Waiting Time</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Email Address</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Services</th>
                  <!-- <th class="waitlist-table-td-th" style="color: #FFF;">Status</th> -->
                  <!-- <th class="waitlist-table-td-th" style="color: #FFF;">Send Message</th> -->
                  <th class="waitlist-table-td-th" style="color: #FFF;">Actions</th>
                </tr>
                </thead>
                <tbody class="list">
               {foreach from=$waiting_lists item=item}
            <div style="display: none" id="{$item.id}wait_id" name="{$item.id}wait_id">{$item.id}</div>
            <div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
            <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
            <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
            <div style="display: none" id="{$item.id}status" name="{$item.id}status">{$item.status}</div>
            <div style="display: none" id="{$item.id}email_address" name="{$item.id}email_address">{$item.email_address}</div>
            <div style="display: none" id="{$item.id}services" name="{$item.id}services">{$item.services}</div>
            <div style="display: none" id="{$item.id}product_ids" name="{$item.id}product_ids">{$item.product_ids}</div>
                      <tr>
                        <td class="waitlist-table-td-th customer-name">{$item.first_name} {$item.last_name}</td>
                        <td class="waitlist-table-td-th">{$item.contact_number}</td>
                        <td class="waitlist-table-td-th">{$item.normal_time}</td>
                        
                        {if $item.status neq 'WAITING'}
                          <td class="waitlist-table-td-th">00:00</td>
                        {else}
                          <td class="waitlist-table-td-th">{$item.waiting_time}</td>
                        {/if}

                        <td class="waitlist-table-td-th">{$item.email_address}</td>
                        <td class="waitlist-table-td-th">{$item.services}</td>
                        <!-- <td class="waitlist-table-td-th">{$item.status}</td> -->
                        

                        {if $item.status neq 'WAITING'}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon-hover.png'/>
                            <!-- </td> -->
                            <!-- <td class="ping waitlist-table-td-th"> -->
                              <img src='{$webroot_resources}/images/done-icon-hover.png'/>
                            </td>
                              
                        {else}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon.png' onmouseover="this.src='{$webroot_resources}/images/message-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/message-icon.png';" onclick="show_send_sms({$item.id})" />
                            <!-- </td> -->
                            <!-- <td class="ping waitlist-table-td-th"> -->
                              <img src='{$webroot_resources}/images/done-icon.png' onmouseover="this.src='{$webroot_resources}/images/done-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/done-icon.png';" onclick="edit_waiting('{$item.id}')"/>
                            </td>   
                        {/if}

                        
                      </tr>
                   {/foreach}
               
                </tbody>
               
              </table>
               <div class="clearfix"></div>
                                
                                    <div class="col-lg-12 pull-right">
                                       <ul class="pagination"></ul>
                                    </div>
            <!-- </div> -->
              
            <!-- /.box-body -->
          <!-- </div> -->
          <!-- /.box -->
<!--           <header class="header bg-gray b-b b-light" style="background: transparent;">
                  <div class="row">
                       <div class="col-md-2 col-xs-5 ta-right col-lg-offset-10">
                          <div class="breadtitle-holder2 m-b-none" onclick="add_waiting()">
                               <div class="btn-group">
                                   <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                      <div class="btn-group hidden-nav-xs">
                                          <button  class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#newservices">Add Waiting Customer</button>
                                    </div>
                               </div>
                          </div>
                       </div>
                  </div>
               </header>   -->
 <!-- </div> -->
  <!-- </div> -->
  </section>
  </section>
  </section>























 <!-- .content -->
    <!--   <section id="content">
        <section class="vbox">
        <section class="scrollable"> -->
              <!-- table -->
<!--               <table id="tbUser" class="merchant-table">
                 
                    <tr class="header bg-gray b-b b-light"> -->
                      <!-- <th class="waitlist-table-td-th">Service #</th> -->
                     <!--  <th style="width: 20%;" class="waitlist-table-td-th">Customer Name</th>
                      <th class="waitlist-table-td-th">Contact No</th>
                      <th class="waitlist-table-td-th">Check In Time</th>
                      <th class="waitlist-table-td-th">Waiting Time</th>
                      <th class="waitlist-table-td-th">Status</th>
                      <th class="waitlist-table-td-th">Send Message</th>
                      <th class="waitlist-table-td-th">Change Status</th>
                    </tr> -->
				
				<!-- 	{foreach from=$waiting_lists item=item}
						<div style="display: none" id="{$item.id}wait_id" name="{$item.id}wait_id">{$item.id}</div>
						<div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
						<div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
						<div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
						<div style="display: none" id="{$item.id}status" name="{$item.id}status">{$item.status}</div>
	                    <tr>
	                      <td class="waitlist-table-td-th">{$item.first_name} {$item.last_name}</td>
	                      <td class="waitlist-table-td-th">{$item.contact_number}</td>
	                      <td class="waitlist-table-td-th">{$item.normal_time}</td>
	                      
                        {if $item.status neq 'WAITING'}
                          <td class="waitlist-table-td-th">00:00</td>
                        {else}
                          <td class="waitlist-table-td-th">{$item.waiting_time}</td>
                        {/if}
	                      <td class="waitlist-table-td-th">{$item.status}</td>
	                      

                        {if $item.status neq 'WAITING'}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon-hover.png'/>
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon-hover.png'/>
                            </td>
                              
                        {else}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon.png' onmouseover="this.src='{$webroot_resources}/images/message-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/message-icon.png';" onclick="show_send_sms({$item.id})" />
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon.png' onmouseover="this.src='{$webroot_resources}/images/done-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/done-icon.png';" onclick="edit_waiting('{$item.id}')"/>
                            </td>   
                        {/if}

	                      
	                    </tr>
	                 {/foreach}
              </table>
              <header class="header bg-gray b-b b-light" style="background: transparent;">
                  <div class="row">
                       <div class="col-md-2 col-xs-5 ta-right col-lg-offset-10">
                          <div class="breadtitle-holder2 m-b-none" onclick="add_waiting()">
                               <div class="btn-group">
                                   <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                      <div class="btn-group hidden-nav-xs">
                                          <button  class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#newservices">Add Waiting Customer</button>
                                    </div>
                               </div>
                          </div>
                       </div>
                  </div>
               </header> -->
              <!-- /. table -->
            <!--   </section>


       </section>
       
	      
     </section> -->
     </section>
  </section>
</section>
  </section>
</section>
<!-- /.content -->

<div id="dlgEditCheckIn" name="dlgEditCheckIn" class="modal fade" role="dialog">
<form id="frmCheckInStatus" name="frmCheckInStatus">	
	<input type="hidden" name="txtCheckInId" id="txtCheckInId">
  
  <input type="hidden" name="txtSpecialist" id="txtSpecialist">
  <input type="hidden" name="txtProducts" id="txtProducts">

  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Check In</h4>
                <p>Enter required information</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
              <div class="alert alert-danger error-prompt" role="alert" hidden>
                  <div class="error-holder">
                  </div>
              </div>
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label><strong>Contact Number</strong> *</label>
                             <input type="text" name="txtContactNumber" data-mask="999-999-9999" data-mask-reverse="true" id="txtContactNumber" class="form-control" placeholder="Contact Number" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label><strong>First Name</strong> *</label>
                             <input type="text" name="txtFirstName" id="txtFirstName" class="form-control" placeholder="First Name" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label><strong>Last Name</strong> *</label>
                            <input type="text" name="txtLastName" id="txtLastName" class="form-control" placeholder="Last Name" readonly="readonly"> 
                        </div>
                        <div class="form-group">
                            <label><strong>Email Address</strong></label>
                             <input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form-control" placeholder="Email Address" readonly="readonly">
                        </div>

                        <div class="form-group" id="divStatus" name="divStatus">
                            <label><strong>Status</strong></label>
              							<select id="txtStatus" name="txtStatus" class="form-control m-b">
              							  	<option value="WAITING">WAITING</option>
              							  	<option value="CANCELLED">CANCELLED</option>
              							  	<option value="SEATED">SEATED</option>
              							</select>
                       </div> 
                     </div>
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" style="display: none;" class="btn btn-primary" id="btnFinalizeFirst" onclick="finalize()" name="btnFinalizeFirst">Save</button>
                <button type="button" class="btn btn-primary" id="btnSelectService" onclick="gotoServices()" name="btnSelectService">Select Services</button>
                <button type="button" style="display: none;" class="btn btn-primary" id="btnUpdateFirst" onclick="updateBasedFromStatus()" name="btnUpdateFirst">Save</button>
                <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='serviceslist.php'">Save Service</button> -->
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</form>
</div>

<!-- Modal -->
<div id="dlgEditCheckInService" name="dlgEditCheckInService" class="modal fade" role="dialog" >
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Services</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->

      </div>

      <!-- Modal content--> 
      <div class="modal-content">
              <div class="alert alert-danger error-prompt" role="alert" hidden>
                <div class="error-holder">
                </div>
            </div>         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <!-- <span class="step_descr section">Select Services</span> -->
                      <!-- <p>Search Service Name: <input type="text" ng-model="services"></p> -->
                  </div>
              </div>
              
            
            <!-- <div class="row"> -->
            <div class="row">
<div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
                {foreach from=$categories key=key item=item}
                  {if $key==0}
                  <li role="presentation" class="active"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {else}
                  <li role="presentation"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {/if}
                {/foreach}
              </ul>
              <div class="tab-content">
             {foreach from=$categories key=key item=item2}
              {if $key==0}
              <div role="tabpanel" class="tab-pane active" id="cat{$key}">
              {else}
              <div role="tabpanel" class="tab-pane" id="cat{$key}">
              {/if}

              {foreach from=$services item=item}
                {if $item.category_name == $item2.name}
                <div class="col-md-4 col-sm-6">
                 <label style="margin-right: 5px; font-size: 14px;">
                    <input type="checkbox" id="services[]" value="{$item.id}" name="services[]" height="50" class="flat-red"> <span style="margin-left:5px;">{$item.name}</span>  <span style="font-size:14px; color:#3b914d; font-weight:bold;">${$item.price|string_format:"%.2f"}</span></label> 
                    </div>

                {/if}
              {/foreach}
              </div>
             {/foreach}
             </div>
          <!-- 
            </div> -->
            
              </div>
              </div>

      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#makereservation3" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <button type="button" style="display: none;" class="btn btn-primary" id="btnUpdateCheckIn" onclick="finalize()" name="btnUpdateCheckIn">Save</button>
            <button type="button" name="btnSpecialist" id="btnSpecialist" style="display: none;" class="btn btn-primary" onclick="gotoSpecialist()">Next</button>
            <button type="button" class="btn btn-dark" onclick="gotoInitial()">Previous</button>
            <!-- <a data-toggle="modal" href="#makereservation1" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
      </div>

      
  </div>
</div>

<!-- Modal -->
<div  id="dlgEditCheckInSpecialist" name="dlgEditCheckInSpecialist" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Specialist</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Specialist</span>
                  </div>
              </div>
              <div class="row" id="divSpecialist" name="divSpecialist">
                {foreach from=$specialist item=item}
                  <div class="col-md-6 col-sm-6">
                        <label style="margin-right: 5px; font-size: 14px;">
                          <input type="checkbox" value="{$item.id}" id="specialist[]" name="specialist[]" class="flat-red"> 
                          {$item.first_name} {$item.last_name}
                        </label> 
                  </div>
               {/foreach}
              
                  
              </div> 
      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Finish</a>
            <a data-toggle="modal" href="#makereservation2" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
            <!-- <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button> -->
            
            <button type="button" class="btn btn-primary" id="btnFinalizie" name="btnFinalizie" onclick="finalize()">Update</button>
            <button type="button" class="btn btn-dark"  onclick="goBackToServices()">Previous</button>
      </div>
  </div>
</div>




<!-- Modal -->
<div id="dlgMessageCustomer" name="dlgMessageCustomer" class="modal fade" role="dialog">
 <form id="frmSendMessage" name="frmSendMessage">
 	<input type="hidden" name="txtMessageLastName" id="txtMessageLastName">
 	<input type="hidden" name="txtMessageFirstName" id="txtMessageFirstName">
 	<input type="hidden" name="txtMessageContactNumber" id="txtMessageContactNumber">
 	
 	
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Alert SMS</h4>
                <p id="pSendTo" name="pSendTo">Send Message to customer right now</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Message</label>
                             <textarea rows="2" cols="50" id="txtMessage" name="txtMessage"  class="form-control" style="width:100%;">In a few minutes your reservation to 'Beauty Spa' available, please confirm if you still want to avail our services.</textarea>
                        </div>
                     </div>
                </div> 
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Send it now</a> -->
                <button  class="btn btn-md btn-primary" name="btnSendSMS" id="btnSendSMS">Send SMS</button> 
                <button class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
  </form>
</div>

