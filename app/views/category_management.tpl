<script type="text/javascript" src="{$webroot_resources}/js/category.js"></script>
 <section id="content">
    <section class="vbox" id="paginateDiv">
       <header class="header bg-darkblue b-b b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">List of Categories</p>
                                            </div>
                                        </div>
                                     </div>
                                        <!--<div class="col-md-1 col-xs-3 ta-right">
                                      <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div>
                                    </div> -->
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New Product and Service" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add_category()">Add New Category</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                     <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search category" />
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable wrapper">
        	<ul id="example" class="list paginated-list">
	
	            	{foreach from=$categories item=item}
                        
	                     <li class="col-lg-4 col-md-6 col-xs-12">
	                        <div class="services-holder">
	                            <div class="row services-holder-body">
	                                <div class="col-md-8 col-xs-4">
	                                	
	                                    <div class="title-services category-name" id="{$item.id}name" name="{$item.id}name">{$item.name}</div>
	                                    <div class="description-services"  id="{$item.id}description" name="{$item.id}description">
	                                    		{$item.description}
	                                   	</div>
	                                </div>
	                                <!-- <div class="col-md-4 col-xs-8 ta-right">
	                                    <div class="price-services" id="{$item.id}price">${$item.price|string_format:"%.2f"}</div>
	                                </div> -->
	                            </div>
	                            <div class="ta-right">
	                                <!-- <button type="button" class="btn btn-sm btn-primary" onclick="view_service('{$item.id}')">View</button> -->
	                                <button type="button" class="btn btn-sm btn-dark" onClick="edit_category('{$item.id}')">Edit</button>
                                    <button type="button" class="btn btn-sm btn-md btn-delete" onClick="delete_category('{$item.id}')">Delete</button>
	                            </div>
	                        </div>
	                    </li>

	                {/foreach}

            </ul>
            <div class="clear"></div>
            <div class="row">
                <div class="col-lg-12 pull-right">
                    <ul class="pagination"></ul>
                </div>
            </div>
            <div class="clear"></div>
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>

         </section>
        </section>
    </section>



<div id="dlgEditCategory" name="dlgEditCategory" class="modal fade" role="dialog">
<form id="frmEditCategory" name="frmEditCategory">	
	<input type="hidden" name="txtCategoryId" id="txtCategoryId">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Category</h4>
                <p>Enter required information</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                         <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Category Name *</label>
                                <input type="text" name="txtName" id="txtName" class="form-control" placeholder="Name"> 
                            </div>
                            <div class="form-group">
                                <label>Description *</label>
                                 <textarea rows="5" cols="50" id="txtDescription" name="txtDescription"  class="form-control" style="width:100%;"></textarea>
                            </div>
                         </div>
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" id="btnUpdateCategory" name="btnUpdateCategory">Save Category</button>
                <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='serviceslist.php'">Save Service</button> -->
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</form>
</div>



