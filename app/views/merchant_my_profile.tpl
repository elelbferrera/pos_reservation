<script type="text/javascript" src="{$webroot_resources}/js/my_profile.js"></script>
 <!-- .content -->
                      <section id="content">
                        <section class="vbox">

                           <header class="header bg-gray b-b b-light">
                                  <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <div class="breadtitle-holder">
                                              <div class="breadtitle">
                                                  <i class="fa fa-user titleFA"></i> 
                                                  <p class="headeerpage-title">Merchant Profile</p>
                                              </div>
                                          </div>
                                       </div>
                                       <div class="col-md-6 col-xs-6 ta-right">
                                          <div class="breadtitle-holder2">
                                              <div class="btn-group">
                                                     <!-- <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-home"></i></button> -->
                                                      <div class="btn-group hidden-nav-xs">
                                                            <!-- <button type="button" class="btn btn-sm btn-primary" onClick="location.href='register-branch.php'">Register New Branch</button> -->
                                                      </div>
                                               </div>
                                             <!--   <div class="btn-group">
                                                  <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                      <div class="btn-group hidden-nav-xs">
                                                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                    </div>
                                               </div> -->
                                          </div>
                                       </div>
                                  </div>
                          </header>


                           
                            <section class="scrollable wrapper">
                                <div class="row">
                                     <form role="form" id="frmMerchantProfile" name="frmMerchantProfile" enctype="multipart/form-data">
                                         <div class="col-lg-2 col-md-3 col-xs-3">
                                                   <label>Merchant Logo:</label><br />
                                                  	{if $profile.logo eq ''}
                                                  		<img src="{$webroot}/images/store-avatar.png" class="profile-avatar" /><br />
                                                  	{else}
                                                  		<img src="{$profile.logo}" class="profile-avatar" /><br />
                                                  	{/if}
                                                  
                                                  <input type="file" name="file" id="file" class="inputfile" value="$profile.logo"  /> 
                                                  <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary">
                                                  	<i class="fa fa-upload"></i> Change Photo</label>  
                                                  <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                                         
                                         </div>
                                         <div class="col-lg-10 col-md-9 col-xs-9">
                                            <section class="panel panel-default2">
                                                <header class="panel-heading bg-light">
                                                    <ul class="nav nav-tabs pull-right">
                                                        <li class="active">
                                                        	<a href="#information" data-toggle="tab"><i class="fa fa-file text-default"></i> Basic Information</a></li>
                                                        <li>
                                                        	<a href="#businesshours" data-toggle="tab"><i class="fa fa-home text-default"></i> Business Hours</a>
                                                        </li>
                                                        <!-- <li> -->
                                                        	<!-- <a href="#branch" data-toggle="tab"><i class="fa fa-home text-default"></i> Branch</a> -->
                                                        <!-- </li> -->
                                                    </ul> 
                                                    <span class="hidden-sm" style="font-size:18px; font-weight:bold;">Merchant Information</span> </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="information">
                                                                  <div class="row">
                                                                      <div class="col-md-12 col-xs-12">
                                                                          <div class="form-group">
                                                                                <label style="font-weight: bold;">Merchant Name <span class="req">*</span></label>
                                                                                <input type="text" class="form-control" name="txtMerchantName" id="txtMerchantName" placeholder="Merchant name" value="{$profile.business_name}">
                                                                                <input type="hidden" class="form-control" name="txtDBA" id="txtDBA" placeholder="Merchant name" value="{$profile.dba}">
                                                                                <input type="hidden" class="form-control" name="txtProcessor" id="txtProcessor" placeholder="Merchant name" value="{$profile.processor}">
                                                                                <input type="hidden" class="form-control" name="txtMID" id="txtMID" placeholder="Merchant name" value="{$profile.mid}"> 
                                                                           </div>
                                                                      </div>
																	
                                                                  </div>

                                                                  <div class="row">
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <div class="form-group">
                                                                              <label style="font-weight: bold;">About <span class="req">*</span></label>
                                                                              <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control">{$profile.description|strip}</textarea>
                                                                         </div>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <div class="form-group">
                                                                              <label style="font-weight: bold;">Address <span class="req">*</span></label>
                                                                              <textarea rows="1" cols="50" id="txtAddress" name="txtAddress" class="form-control">{$profile.address1|strip}</textarea>
                                                                         </div>
                                                                    </div>
                                                                  </div>
																  <div class="row">
																  	<!-- 'address1' => 'Lot 25 Blk 11 La Aldea Subd., Tabang',
																	'city' => 'Bulacan',
																	'state' => 'Guiguinto',
																	'zip' => '3015',
																	'country' => 'Philippines',
																	'email' => 'lemmuelcabuhat@gmail.com',
																	'phone1' => '639178580549',
																	'first_name' => 'Lemmuel Alvin',
																	'last_name' => 'Cabuhat',
																	'contact_email' => 'lemmuelcabuhat@gmail.com' -->
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label style="font-weight: bold;">City: <span class="req">*</span></label>
                                                                                <input type="text" id="txtCity" name="txtCity" value="{$profile.city}" class="form-control" placeholder="City"> 
                                                                           </div>
                                                                      </div>
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label style="font-weight: bold;">State: <span class="req">*</span></label>
                                                                                <input type="text" id="txtState" name="txtState" value="{$profile.state}" class="form-control" placeholder="State"> 
                                                                           </div>
                                                                      </div>
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label style="font-weight: bold;">Zip: <span class="req">*</span></label>
                                                                                <input type="text" id="txtZip" name="txtZip" class="form-control" placeholder="Zip" value="{$profile.zip}"> 
                                                                           </div>
                                                                      </div>
																  	  <div class="col-md-3 col-xs-3">
	                                                                        <div class="form-group">
	                                                                          <label style="font-weight: bold;">Country <span class="req">*</span></label>
	                                                                              <select name="txtCountry" id="txtCountry" class="form-control m-b">
											                                            {foreach from=$countries item=item}
											                                        		<option value="{$item.name}" {if $profile.country eq $item.name} selected="selected" {/if}>{$item.name}</option>
											                                        	{/foreach}
											                                        </select>
	                                                                        </div>
                                                                      </div>
																  	
																  </div>

                                                                  <div class="row">
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Owner First Name <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="First name" name="txtFirstName" id="txtFirstName" value="{$profile.first_name}"> 
                                                                            </div>
                                                                      </div>
                                                                      <!-- <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner Middle Name</label>
                                                                              <input type="text" class="form-control" placeholder="Middle name" id="txtMiddleName" name="txtMiddleName" value="{$profile.middle_name}"> 
                                                                            </div>
                                                                      </div> -->
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Owner Last Name <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="Last name" id="txtLastName" name="txtLastName"  value="{$profile.last_name}"> 
                                                                            </div>
                                                                      </div>
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Contact No. <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="Contact No." name="txtContactNo" id="txtContactNo"  value="{$profile.phone1}"> 
                                                                            </div>
                                                                      </div>
                                                                  </div>
                                                                  <div class="row">
                                                                      
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Email Address <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="Email Address" id="txtEmail" name="txtEmail"  value="{$profile.email}"> 
                                                                            </div>
                                                                      </div>
                                                                      <!-- <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner Email</label>
                                                                              <input type="text" class="form-control" placeholder="Owners Email" id="txtContactEmail" name="txtContactEmail"> 
                                                                            </div>
                                                                      </div> -->
                                                                      
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Longitude <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="Longitude" id="txtLongitude" name="txtLongitude"  value="{$profile.longitude}"> 
                                                                            </div>
                                                                      </div>
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label style="font-weight: bold;">Latitude <span class="req">*</span></label>
                                                                              <input type="text" class="form-control" placeholder="Latitude" id="txtLatitude" name="txtLatitude"  value="{$profile.latitude}"> 
                                                                            </div>
                                                                      </div>
                                                                      
                                                                  </div>
                                                                   <div class="row">
                                                                    <div class="col-md-12 col-xs-12">
                                                                    <div class="form-group">
                                                                      <label>Caption</label>
                                                                      <textarea class="form-control" placeholder="Caption" id="txtCaption" name="txtCaption">{$profile.caption}</textarea>
                                                                    </div>
                                                                    </div>
                                                                  </div>

														</div>
                                                        <!-- end -->
                                                        <div class="tab-pane" id="businesshours">
                                                                    <div class="col-md-6 col-xs-6">
                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Monday <label> <input type="checkbox" id="chkMonday" name="chkMonday" {if $monday_is_close } checked="checked" {/if} /> Salon Is Close</label> </div>
                                                                                        <div class="sched-body">

                                                                                              <div class="row" id="divMonday" name="divMonday" {if $monday_is_close} style="display: none;" {/if}>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" id="txtMondayOpeningTime" name="txtMondayOpeningTime" value="{$monday_start_time}" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text"  id="txtMondayClosingTime" name="txtMondayClosingTime"  value="{$monday_end_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>    
                                                                                              </div>
                                                                                         </div>
                                                                                    </div>

                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Tuesday <label> <input type="checkbox" id="chkTuesday" name="chkTuesday"  {if $tuesday_is_close } checked="checked" {/if} /> Salon Is Close</label> </div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row" id="divTuesday" name="divTuesday" {if $tuesday_is_close} style="display: none;" {/if}>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtTuesdayOpeningTime" id="txtTuesdayOpeningTime"  value="{$tuesday_start_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtTuesdayClosingTime" id="txtTuesdayClosingTime"  value="{$tuesday_end_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>

                                                                                     <div class="sched-holder">
                                                                                        <div class="sched-header">Wednesday <label> <input type="checkbox" id="chkWednesday" name="chkWednesday"  {if $wednesday_is_close } checked="checked" {/if}  /> Salon Is Close</label></div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row" name="divWednesday" id="divWednesday"  {if $wednesday_is_close} style="display: none;" {/if}>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtWednesdayOpeningTime" id="txtWednesdayOpeningTime"  value="{$wednesday_start_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtWednesdayClosingTime" id="txtWednesdayClosingTime"  value="{$wednesday_end_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>

                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Thursday <label> <input type="checkbox" id="chkThursday" name="chkThursday"  {if $thursday_is_close } checked="checked" {/if}  /> Salon Is Close</label></div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row" id="divThursday" name="divThursday" {if $thursday_is_close} style="display: none;" {/if}>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtThursdayOpeningTime"  id="txtThursdayOpeningTime"  value="{$thursday_start_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" name="txtThursdayClosingTime" name="txtThursdayClosingTime"  value="{$thursday_end_time}"  class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>



                                                                                </div>
                                                                                <div class="col-md-6 col-xs-6">
                                                                                  			<div class="sched-holder">
			                                                                                      <div class="sched-header">Friday <label> <input type="checkbox" id="chkFriday" name="chkFriday"  {if $friday_is_close } checked="checked" {/if} /> Salon Is Close</label></div>
			                                                                                      <div class="sched-body">
			                                                                                            <div class="row" id="divFriday" name="divFriday"  {if $friday_is_close} style="display: none;" {/if}>
			                                                                                                <div class="col-md-6 col-xs-6">
			                                                                                                    <div class="bootstrap-timepicker">
			                                                                                                        <div class="form-group">
			                                                                                                              <label>Start Time:</label>
			                                                                                                              <div class="input-group">
			                                                                                                                <input type="text" name="txtFridayOpeningTime" id="txtFridayOpeningTime"  value="{$friday_start_time}"  class="form-control timepicker">
			                                                                                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
			                                                                                                              </div>
			                                                                                                        </div>
			                                                                                                    </div>
			                                                                                                </div>
			                                                                                                <div class="col-md-6 col-xs-6">
			                                                                                                   <div class="bootstrap-timepicker">
			                                                                                                        <div class="form-group">
			                                                                                                              <label>End Time:</label>
			                                                                                                              <div class="input-group">
			                                                                                                                <input type="text" name="txtFridayClosingTime" id="txtFridayClosingTime" value="{$friday_end_time}"  class="form-control timepicker">
			                                                                                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
			                                                                                                              </div>
			                                                                                                        </div>
			                                                                                                    </div>
			                                                                                                </div>
			                                                                                            </div>
			                                                                                       </div>
			                                                                                   </div>

                                                                                               <div class="sched-holder">
                                                                                                  <div class="sched-header">Saturday <label> <input type="checkbox" id="chkSaturday" name="chkSaturday"  {if $saturday_is_close } checked="checked" {/if}  /> Salon Is Close</label></div>
                                                                                                  <div class="sched-body">
                                                                                                        <div class="row" id="divSaturday" name="divSaturday" {if $saturday_is_close} style="display: none;" {/if}>
                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                <div class="bootstrap-timepicker">
                                                                                                                    <div class="form-group">
                                                                                                                          <label>Start Time:</label>
                                                                                                                          <div class="input-group">
                                                                                                                            <input type="text" name="txtSaturdayOpeningTime" id="txtSaturdayOpeningTime"  value="{$saturday_start_time}"  class="form-control timepicker">
                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                          </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                               <div class="bootstrap-timepicker">
                                                                                                                    <div class="form-group">
                                                                                                                          <label>End Time:</label>
                                                                                                                          <div class="input-group">
                                                                                                                            <input type="text" id="txtSaturdayClosingTime" name="txtSaturdayClosingTime"  value="{$saturday_end_time}"  class="form-control timepicker">
                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                          </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                   </div>
                                                                                               </div>

                                                                                           		<div class="sched-holder">
	                                                                                                  <div class="sched-header">Sunday <label> <input type="checkbox" id="chkSunday" name="chkSunday"  {if $sunday_is_close } checked="checked" {/if}  /> Salon Is Close</label></div>
	                                                                                                  <div class="sched-body">
	                                                                                                        <div class="row" id="divSunday" name="divSunday"  {if $sunday_is_close} style="display: none;" {/if}>
	                                                                                                            <div class="col-md-6 col-xs-6">
	                                                                                                                <div class="bootstrap-timepicker">
	                                                                                                                    <div class="form-group">
	                                                                                                                          <label>Start Time:</label>
	                                                                                                                          <div class="input-group">
	                                                                                                                            <input type="text" name="txtSundayOpeningTime"  id="txtSundayopeningTime"  value="{$sunday_start_time}"   class="form-control timepicker">
	                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                                                                                                                          </div>
	                                                                                                                    </div>
	                                                                                                                </div>
	                                                                                                            </div>
	                                                                                                            <div class="col-md-6 col-xs-6">
	                                                                                                               <div class="bootstrap-timepicker">
	                                                                                                                    <div class="form-group">
	                                                                                                                          <label>End Time:</label>
	                                                                                                                          <div class="input-group">
	                                                                                                                            <input type="text" name="txtSundayClosingTime" id="txtSundayClosingTime"  value="{$sunday_end_time}"  class="form-control timepicker">
	                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                                                                                                                          </div>
	                                                                                                                    </div>
	                                                                                                                </div>
	                                                                                                            </div>
	                                                                                                        </div>
	                                                                                                   </div>
	                                                                                               </div>
                                                                                             </div>
                                                        </div>
                                                        <!-- end -->
                                                        <div class="tab-pane" id="branch">
                                                              <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                           <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>


                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                               <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                      <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                             <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                           <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                              <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>

                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                               <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                           <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                              <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>

                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>



                                             
                                         <div class="row">
                                              <div class="col-md-12 col-xs-12">
                                                      <div class="ta-right">
                                                          <button type="submit" class="btn btn-primary">Save Information</button>
                                                          <!-- <button type="button" class="btn btn-md btn-dark" onClick="location.href='profile.php'">Cancel</button> -->
                                                     </div>
                                              </div>
                                        </div>
                                           


                                         </div>






<!--                                          <div class="clear"></div> -->

                                     </form>
                                 </div>
                            </section>
                       </section>
                     </section>