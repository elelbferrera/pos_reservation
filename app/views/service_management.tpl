<script type="text/javascript" src="{$webroot_resources}/js/service.js"></script>
 <section id="content">
    <section class="vbox" id="paginateDiv">
       <header class="header bg-darkblue b-b b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">Services Offer</p>
                                            </div>
                                        </div>
                                     </div>
                                     <!--<div class="col-md-1 col-xs-3 ta-right">
                                         <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div> 
                                    </div>-->
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New Product and Service" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add_service()">Add New Service</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                      <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search product or category" />
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable wrapper">
        	<ul id="example"  class="list paginated-list">
	
	            	{foreach from=$services item=item}
	                     <li class="col-lg-4 col-md-6 col-xs-12">
	                        <div class="services-holder">
	                            <div class="row">
	                                <div class="col-md-8 col-xs-4">
	                                	<div style="display: none" id="{$item.id}cost" name="{$item.id}cost">{$item.cost|string_format:"%.2f"}</div>
	                                	<div style="display: none" id="{$item.id}product_type" name="{$item.id}product_type">{$item.product_type}</div>
	                                	<div style="display: none" id="{$item.id}minutes" name="{$item.id}minutes">{$item.minutes_of_product|string_format:"%.0f"}</div>
	                                	
	                                    <div class="title-services product-name" id="{$item.id}product_name" name="{$item.id}product_name">{$item.name}</div>
                                        <div class="title-services category-name" id="{$item.id}category_name" name="{$item.id}category_name" style="font-weight: 400; font-size: 16px;">{$item.category_name}</div>
	                                    <div style="display: none" id="{$item.id}quantity" name="{$item.id}quantity">{$item.quantity}</div>
	                                    <div style="display: none" id="{$item.id}category_id" name="{$item.id}category_id">{$item.category_id}</div>
	                                    <div style="display: none" id="{$item.id}category_name" name="{$item.id}category_name">{$item.category_name}</div>
	                                    <div class="description-services"  id="{$item.id}description" name="{$item.id}description">
	                                    		{$item.description}
	                                   	</div>
	                                </div>
	                                <div class="col-md-4 col-xs-8 ta-right">
	                                    <div class="price-services" id="{$item.id}price">${$item.price|string_format:"%.2f"}</div>
	                                </div>
	                            </div>
	                            <div class="ta-right">
	                                <button type="button" class="btn btn-sm btn-primary" onclick="view_service('{$item.id}')">View</button>
	                                <button type="button" class="btn btn-sm btn-dark" onClick="edit_service('{$item.id}')">Edit</button>
                                    <button type="button" class="btn btn-sm btn-md btn-delete" onClick="delete_service('{$item.id}')">Delete</button>

	                            </div>
	                        </div>
	                    </li>
	                {/foreach}

            </ul>
             <div class="clear"></div>
            <div class="row">
                <div class="col-lg-12 pull-right">
                    <ul class="pagination"></ul>
                </div>
            </div>
            <div class="clear"></div>
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>



