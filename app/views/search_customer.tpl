<script src="{$webroot_resources}/js/search_customer.js"></script>
<!-- <script src="{$webroot_resources}/js/reservation.js"></script> -->

<form id="frmReservation" name="frmReservation" style="display: none;">
                              <input type="text" name="txtReservationId" id="txtReservationId"/>
                              <input type="text" name="txtLastName" id="txtLastName"/>
                              <input type="text" name="txtFirstName" id="txtFirstName"/>
                              <input type="text" name="txtMobile" id="txtMobile"/>
                              <input type="text" name="txtEmail" id="txtEmail"/> <!-- added -->
                              <input type="text" name="txtNotes" id="txtNotes"/>
                              <input type="text" name="txtDate" id="txtDate"/>
                              <input type="text" name="txtTime" id="txtTime"/>
                              <input type="text" name="txtProducts" id="txtProducts"/>
                              <input type="text" name="txtSpecialist" id="txtSpecialist"/>
                              <input type="text" name="txtSpecialistFirstName" id="txtSpecialistFirstName"/>
                              <input type="text" name="txtBranch" id="txtBranch"/>
                              <input type="text" name="txtStatus" id="txtStatus"/>
                              <input type="text" name="txtMerchantId" id="txtMerchantId"/>

                              <input type="text" name="txtURL" id="txtURL" value="{$webroot}/"/>
                            </form>

                <section id="content">
                        <section class="vbox">
                              <section class="scrollable">
                                        
                                        <header class="header bg-darkblue b-light">
                                          <div class="row">
                                                   <div class="col-md-7 col-xs-7">
                                                      <div class="breadtitle-holder">
                                                          <div class="breadtitle">
                                                              <i class="fa fa-user titleFA"></i> 
                                                              <p class="headeerpage-title">Customer</p>
                                                          </div>
                                                      </div>
                                                   </div>
                                                    <div class="col-md-5 col-xs-5 ta-right">
                                                          <div id="search-find">
                  	                                        <form method="post" id="frmSearchNameMobile">
                  	                                            <div class="input-group">
                                          											      <input type="text" class="form-control" id="search" placeholder="Search for name or mobile #" name="search_value" style="font-weight: bold;">
                                          											      <span class="input-group-btn">
                                          											        <button class="btn btn-default" type="submit" id="searchCustomer" name="searchCustomer" style="font-weight: bold;"><strong>Find appointments</strong></button>
                                          											      </span>
                                          											    </div>
                                          									 </form>
                                                          </div>
                                                     </div>
                                               </div>
                                          </header>
                                        
                                          <!-- <div class="row"> -->
                                              	<table id="example2" class="waitlist-table">
                                                      <thead>
                                                      <tr>
                                                        <th style="color: #FFF;" class="waitlist-table-td-th">Customer Name</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Contact No</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Date</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Time</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Service/s</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Status</th>
                                                        <th class="waitlist-table-td-th" style="color: #FFF;">Actions</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody id="customerData">
                                                      <!-- <button id="editBtn" class="btn btn-default" type="button" data-first="'.$c['first_name'].'" data-last="'.$c['last_name'].'" data-mobile="'.$c['mobile'].'" data-date="'.$c['reservation_date'].'" data-time="'.$c['reservation_time'].'" data-status="'.$c['status'].'" data-specialist="'.$c['specialist_first_name'].'" data-note="'.$c['notes'].'" data-reservation-id="'.$c['id'].'">Edit</button> -->
                                                      </tbody> 
                                                </table>
                                          <!-- </div> -->
                                </section>
                          </section>
                     </section>

                <!-- /.content -->
               
            </section>
        </section>
    </section>