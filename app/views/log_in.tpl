<!-- Main content -->
 <div class="container">  
    <section class="content">
        <div class="row">
            <div class="login-position">
                <div class="col-lg-8 col-md-7 col-sm-6 col-xs-6">
                    <img src="{$webroot_resources}/images/logo-login.png" class="logo-login" />
                    <div class="login-logo-subtitle">Your online reservation solution for better service to make <br />your business grow</div>
                </div>
             
                 <div class="col-lg-4 col-md-5 col-sm-6 col-sm-6">
                       <div class="login-box-header"> Sign in</div>
                       <div class="login-box-body"> 
                           
                        
                            <form action="" method="post">
                                <div class="form-group has-feedback">
                                    <input type="text" id="username" name="username" class="form-control login-input" placeholder="Username">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password"name="password" id="password" class="form-control login-input" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                     <div class="col-xs-12">
                                        <div class="checkbox icheck">
                                            <label style="padding-left: 0px !important;">
                                                <input type="checkbox"> Remember Me
                                            </label>
                                        </div>
                                     </div>

                                    <div class="col-xs-5">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat"> Sign In</button>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-xs-7">
                                        <div class="ta-left">
                                            <!-- <a href="forgotpassword.php">I forgot my password</a> -->
                                            <button type="button" class="btn" data-toggle="modal" data-target="#forgotPassword" style="background: transparent;font-weight: normal;">I forgot my password</button>
                                           <!--  <a data-toggle="modal" data-target="#forgotPassword">I forgot my password</a> -->
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>
                         </div>
                     <!-- /.login-box-body -->
                </div>
            </div>
        </div>
    </section>
 </div>






 <div id="forgotPassword" class="modal fade" role="dialog">
  <div class="modal-dialog2">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgot Password</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                      <form action="" method="post" id="frmForgotPassword" name="frmForgotPassword">
                          <div class="col-md-10 col-sm-9">
                                    <div class="form-group has-feedback">
                                        <input type="text" id="email_address" name="email_address" class="form-control login-input" placeholder="Email Address">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                          </div>
                          <div class="col-md-2 col-sm-3">
                            <button id="btnResetPass" name="btnResetPass" type="submit" class="btn btn-primary">Submit</button>
                          </div>
                      </form>
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
           </div>
  </div>
</div>

