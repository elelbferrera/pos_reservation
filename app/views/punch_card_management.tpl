<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/punchcard.js"></script>

<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/deals.png" alt="" style="height: 30px;width: 30px;" />List of Punch Card</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             Punch Card</h4>
             
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterPunchCard(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New Punch Card </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>   
                	<th width="10%">Punch Code</th>    
                    <th width="10%">Start Date</th>
                    <th width="10%">End Date</th>
                    <th width="10%">No. Of Holes</th>
                    <th width="10%">Description</th>
                    <th width="10%">Create Date</th>
                    <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                
                {foreach from=$pcs item=item}
                    <tr>
                    	<td>{$item.punch_code}</td> 
                        
                        <td>{$item.start_date}</td> 
                        <td>{$item.end_date}</td>
                        <td>{$item.number_of_punch}</td>  
                        <td>{$item.description}</td> 
                        <td>{$item.create_date}</td>
                        <td>
                            <a onclick="EditPunchCard('{$item.punch_id}','{$item.merchant_id}','{$item.punch_code}', '{$item.number_of_punch}', '{$item.start_date}', '{$item.end_date}', '{$item.description}', '{$item.cost}', '{$item.pin}')">Edit</a>
                             <a href="#" onclick="DeletePunchCard('{$item.punch_id}')">Delete</a>
                        </td>
                    </tr>
                    <div style="display: none;">
                    <input type="text" name="detail_id_{$item.punch_id}" id="detail_id_{$item.punch_id}" value='{$item.details}'/>
                    </div>
                {/foreach}    
        
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgPunchCard" title="New Punch Card" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabPunchCard1" class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Punch Card Information</a></li>
           <!--     <li id="tabAgent2"><a href="#tab2" class="glyphicons adress_book" data-toggle="tab"><i></i>DBA Address</a></li>
                <li id="tabAgent3"><a href="#tab3" class="glyphicons home" data-toggle="tab"><i></i>Mailing Address</a></li>
               -->
              </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           
        <form id="frmRegisterPunchCard" name="frmRegisterPunchCard">
            <input type="hidden"  id = "txtMerchantId" name = "txtMerchantId"  value="-1"/>  
            <input type="hidden"  id = "txtPunchCardId" name = "txtPunchCardId"/>
            <input type="hidden"  id="txtPunchCardDetail" name="txtPunchCardDetail" />
            
            <div id ="divPunchCardStep1"> 
            <table width="100%">
                <tbody>
                	<tr id="trPunchCode" name="trPunchCode">
                        <td class="tbl_label">Punch Code:</td>
                        <td>
                            <input type="text" readonly="readonly" name="txtPunchCode" id="txtPunchCode" size="29" maxlength="20" />
                         </td>
                    </tr>
                   	<tr>
                        <td class="tbl_label">Number of Holes:</td>
                        <td>
                            <input type="text" name="txtNumberOfPunch" id="txtNumberOfPunch" onkeypress="return isNumberKey(event)" size="10" maxlength="2"  checked/>
                         </td>
                    </tr>  
                    <tr>
                        <td class="tbl_label">Start Date:</td>
                        <td>
                            <input type="text" name="txtStartDate" id="txtStartDate" size="29" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">End Date:</td>
                        <td>
                            <input type="text" name="txtEndDate" id="txtEndDate" size="29" maxlength="12" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Description:</td>
                        <td>
                            <textarea name="txtDescription" id="txtDescription" size="40" maxlength="300"> </textarea> 
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Cost:</td>
                        <td>
                            <input type="text" name="txtCost" id="txtCost" size="29" maxlength="12" onkeypress="return isNumberKey(event)" />
                         </td>
                    </tr>
                    <tr>
                        <td class="tbl_label">Pin:</td>
                        <td>
                            <input type="text" name="txtPin" id="txtPin" size="29" maxlength="12" onkeypress="return isNumberKey(event)" />
                         </td>
                    </tr>
                </tbody>
            </table>
           <br>
           
           	<!-- Enter No. Of Hole<input type="text" name="txtNoOfHole" id="txtNoOfHole" />
           	Enter Description<input type="text" name="txtHoleDescription" id="txtHoleDescription" /> -->
           
           
            <table id="tblPunchCard" width="100%"  class="table table-striped table-bordered table-condensed">
	            <thead>
	                <tr>   
	                	<th width="10%">Freebie Description</th>    
	                    <th width="10%">Hole No.</th>
	                    <th width="10%">Actions</th>
	                </tr>
	            </thead>
	            <tbody>
	            	
	            </tbody>
            </table>
            <br>
            <input type="button" class="btn btn-info" name="btnAddDetail" id="btnAddDetail" value="Add Freebie"/>
            
            
            
            </div>

            
            </div>
            

            <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnRegisterPunchCard" id="btnRegisterPunchCard" value="Register Punch Card" class="btn btn-info"/>
                <input type="button"  name="btnUpdatePunchCard" style="display:none" id="btnUpdatePunchCard" value="Update Punch Card" class="btn btn-info"/>
                <input type="button"  name="btnCancelPunchCard" id="btnCancelPunchCard" value="Cancel" class="btn btn-danger btn-small"/>
            </div>

          
        </form> 
        </div>   
        </div> 
        </div>
        </div>

		<div id="dlgPunchCardDetail" name="dlgPunchCardDetail" title="Punch Card Freebies" style="display:none">
			<div class="widget widget-tabs widget-tabs-double">
				<div class="widget-head">
	              <ul>
	                <li class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Freebies</a></li>
	              </ul>
	            </div>    
	           <div class="widget-body">   
	           	<div class="tab-content">  
	           		 <table width="100%">
                		<tbody>
		                	<tr>
		                        <td class="tbl_label">Freebie Description:</td>
		                        <td>
		                            <textarea name="txtFreebieDescription" id="txtFreebieDescription" size="29" maxlength="60"> </textarea>
		                         </td>
		                    </tr>  
		                    <tr>
		                        <td class="tbl_label">Hole Position:</td>
		                        <td>
		                            <input type="text" name="txtDetailNoOfHole" id="txtDetailNoOfHole" onkeypress="return isNumberKey(event)" size="10" maxlength="4"  checked/>
		                         </td>
		                    </tr>  
                		</tbody>
                	</table>
	           	</div>
	           </div>
			</div>
			<div align="right">
            	<input type="button"  name="btnCreateDetail" id="btnCreateDetail" value="Add" class="btn btn-info"/>
			</div>
		</div>
        {$_messages_}

    </div>
</div>
</div>


        
