<script type="text/javascript" src="{$webroot_resources}/js/admin.js"></script>
<section class="vbox">
<section class="scrollable">
	<section class="hbox stretch">
		

  	<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
		<section class="vbox">

			<header class="header bg-primary lter text-center clearfix">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-arrow-left"></i> </button>
                    <div class="btn-group hidden-nav-xs">
                       <a href="{$webroot}/admin/merchant" class="btn btn-primary"> Back to list</a>
                    </div>
                </div>
            </header>


			<section>
				<!-- <div class="newmerchant-btn">
					<a href="{$webroot}/admin/merchant" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to list</a>
				</div> -->
				
				<div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color: #262626; padding: 55px;">
					<div class="sidebar-waitingtitle ta-center" style="color: #ffffff;font-size: 25px;line-height: 25px;">Total Merchants Registered</div>
					<div class="sidebar-numbers ta-center" style="color: #ffffff;font-size: 60px;">
					{$data|@count gt 0}
					</div>
				</div>

			</section>
		</section>
	</aside>
		
 <!-- .content -->
	<section id="content">  	
		<section class="vbox">

				 <header class="header bg-darkblue b-light">
                    <div class="row">
                         <div class="col-md-12 col-xs-12">
                            <div class="breadtitle-holder">
                                <div class="breadtitle">
                                    <i class="fa fa-user titleFA"></i> 
                                    <p class="headeerpage-title">Edit Merchant</p>
                                </div>
                            </div>
                         </div>

                    </div>
                </header>


		<!-- 	<div class="admin-pageheader">
				<div class="row">
					<div class="col-xs-6">
						<h2><i class="fa fa-user"></i> Edit Merchant</h2>
					</div>
					<div class="col-xs-6 ta-right">
						<div>
							<button type="submit" id="btnEditMerchant" class="btn btn-primary">Save Information</button>
							<a type="button" class="btn btn-md btn-dark" href="{$webroot}/admin/merchant">Cancel</a>    
						</div>
					</div>
				</div>
			</div> -->
			
			<div class="scrollable merchantForm-wrap">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-info" data-toggle="tab">Information</a></li>
						<li><a href="#tab-address" data-toggle="tab">Address</a></li>
						<li><a href="#tab-contact" data-toggle="tab">Contact</a></li>
					</ul>
					<form name="frmEditMerchant" id="frmEditMerchant">
						{foreach from=$merchant item=merchant}
						<input type="hidden" name="merchantid" id="mc-merchantid" value="{$merchant.id}">
						<input type="hidden" name="loggeduser" id="mc-loggeduser" value="{php}echo $_SESSION['username'];{/php}">
						<div class="tab-content">
							<div class="tab-pane active" id="tab-info">
								<div class="form-group">
									<label>Business Name *</label>
									<input type="text" name="businessname" id="mc-businessname" class="form-control required" placeholder="Business Name" value="{$merchant.business_name}">
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label><input type="checkbox" name="status" id="mc-status" value="A" {if $merchant.status eq 'A'}checked{/if}> Enable in Marketplace</label>
									</div>
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label><input type="checkbox" name="sms" id="mc-sms" value="1" {if $merchant.is_sms_on eq '1'}checked{/if}> SMS Notification</label>
									</div>
								</div>
								<div class="form-group">
									<label>Timezone *</label>
									<select class="form-control required" name="timezone" id="mc-timezone">
										{foreach from=$timezone item=timezone}
										{if $timezone.name eq $merchant.timezone}
										<option value="{$timezone.name}" selected>{$timezone.description}</option>
										{else}
										<option value="{$timezone.name}">{$timezone.description}</option>
										{/if}
										{/foreach}
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" id="mc-description" class="form-control textarea-noresize" rows="6">{$merchant.description}</textarea>
								</div>
							</div>

							<div class="tab-pane" id="tab-address">
								<div class="form-group">
									<label>Address 1 *</label>
									<input type="text" name="address1" id="mc-address1" class="form-control required" value="{$merchant.address1}">
								</div>
								<div class="form-group">
									<label>Address 2</label>
									<input type="text" name="address2" id="mc-address2" class="form-control" value="{$merchant.address2}">
								</div>
								<div class="row">
									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>Country *</label>
											<select class="form-control required" name="country" id="mc-country">
												{foreach from=$country item=country}
												{if $country.name eq $merchant.country}
												<option data-id="{$country.id}" value="{$country.name}" selected>{$country.name}</option>
												{else}
												<option data-id="{$country.id}" value="{$country.name}">{$country.name}</option>
												{/if}
												{/foreach}
											</select>
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>State *</label>
											<input type="hidden" name="state" id="state" value="{$merchant.state}" class="required">
											<select class="form-control" name="mc-state" id="mc-state" data-currentState="{$merchant.state}">
											</select>
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>City *</label>
											<input type="text" name="city" id="mc-city" class="form-control required" placeholder="City" value="{$merchant.city}">
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>Zip Code *</label>
											<input type="text" name="zip" id="mc-zip" class="form-control required" placeholder="Zip Code" value="{$merchant.zip}">
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab-contact">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Phone 1 *</label>
											<input type="text" name="phone1" id="mc-phone1" class="form-control required" placeholder="Phone 1" value="{$merchant.phone1}">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Phone 2</label>
											<input type="text" name="phone2" id="mc-phone2" class="form-control" placeholder="Phone 2" value="{$merchant.phone2}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Fax</label>
											<input type="text" name="fax" id="mc-fax" class="form-control" placeholder="Fax" value="{$merchant.fax}">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Email</label>
											<input type="email" name="email" id="mc-email" class="form-control" placeholder="Email" value="{$merchant.email}">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{/foreach}
				</form>

					<div class="row">
				<div class="col-md-12 ta-right"  style="margin-top:20px;">
					<button type="submit" id="btnEditMerchant" class="btn btn-primary">Save Information</button>
					<a type="button" class="btn btn-md btn-dark" href="{$webroot}/admin/merchant">Cancel</a> 
				</div>
			</div>

			</div>

		


		</section>
	</section>
  


 	</section>
  </section>
</section>