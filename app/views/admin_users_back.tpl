<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>

<div id="content">
<h3>User Master List</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             Home< User List </h4>
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterUser(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                 Register User </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>       
                    <th width="10%">ID</th>
                    <th width="10%">First Name</th>
                    <th width="10%">Last Name</th>
                    <th width="15%">User Type</th>
                    <th width="10%">UserName</th>
                    <th width="15%">Email</th>
                    <th width="5%">Status</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
            {foreach from=$users item=item}
             <tr>    
                <td>{$item.id}</td> 
                <td>{$item.fname}</td> 
                <td>{$item.lname}</td> 
                <td>{$item.user_type_desc}</td>
                <td>{$item.username}</td>
                <td>{$item.email_address}</td>
                <td>{$item.status}</td>
                <td>{$item.button}</td>  
            </tr>
            {/foreach}      
            
    
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgUser" title="Register User" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabClient1" class="active"><a href="#tab1" class="glyphicons notes" data-toggle="tab"><i></i>User Information</a></li>
             </ul>
            </div>
           <div class="widget-body">
           <div class="tab-content">   
        <form id="frmNewUser" name="frmNewUser">
        <input type="hidden"  id = "txtID" name = "txtID"/>
        <div id ="divUser">
        <table width="100   %">
            <tbody>
              
               
               <tr>
                    <td class="tbl_label">First Name:</td>
                    <td>
                        <input type="text" name="txtFirstName" id="txtFirstName" size="29" maxlength="20" />
                     </td>
                </tr>
                 <tr>
                    <td class="tbl_label">Last Name:</td>
                    <td>
                        <input type="text" name="txtLastName" id="txtLastName" size="29" maxlength="20" />
                     </td> 
                </tr>
               
               
                <tr>
                    <td class="tbl_label">Email:</td>
                    <td>
                        <input type="text" name="txtEmail" id="txtEmail" size="29" maxlength="30" />
                     </td>
                     
                </tr> 
                <tr>
                    <td class="tbl_label">Mobile:</td>
                    <td>
                        <input type="text" name="txtMobile" id="txtMobile" size="29" maxlength="20" />
                     </td>
                     
                </tr> 
                
               
                 <tr>
                    <td class="tbl_label">Username:</td>
                    <td>
                        <input type="text" name="txtUsername" id="txtUsername" size="29" maxlength="20" />
                     </td>
                </tr>
                <tr>
                    <td id='lblPassword' name='lblPassword' class="tbl_label">Password:</td>
                    <td>
                        <input type="password" name="txtPassword" id="txtPassword" size="29" maxlength="20" />
                     </td>
                </tr>
                
               <tr>
                                                                                                            
               <td  class="tbl_label">User Type:</td>
                  <td><strong>
                    {html_options id="user_type" name="user_type" options="$user_types" selected=6}
                  </strong></td>
                     
                </tr>
               <tr>
                 <td style="display:none;" id='lblUser_center' name='lblUser_center' class="tbl_label">Center:</td>
                    <td>
                         {html_options style="display:none;" id="user_center" name="user_center" options="$center_list"}
                    </td>
               </tr>                          
                
                <tr>
                    <td class="tbl_label"></td>
                    <td>
                       <label> <input type="checkbox" id="is_center" name="is_center">Is Center</label>
                     </td>
                </tr>
                
               <tr>
               
               
             
            </tbody>  
        </table>
         
        </div>
        </div>

        <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnSaveUser" id="btnSaveUser" value="Add User" class="btn btn-info"/>
                 <input type="button"  name="btnCancelUser" id="btnCancelUser" value="Cancel" class="btn btn-danger btn-small"/>
               
            </div>
            <div id="divEdit" align="right" style="display:none">
                <input type="button"  name="btnEditUser" id="btnEditUser" value="Update User" class="btn btn-info"/>
                 <input type="button"  name="btnCancelEditUser" id="btnCancelEditUser" value="Cancel" class="btn btn-danger btn-small"/>
            </div>
           
        </form>
        </div>
        </div>
        </div>
        </div>
        
         
        
        
        
        
        {$_messages_}
    
    
    </div>
</div>
</div>


        
