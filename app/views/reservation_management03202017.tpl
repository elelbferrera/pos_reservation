<!-- <script type="text/javascript" src="{$webroot_resources}/js/jquery.supercal.js"></script> -->
<script type="text/javascript" src="{$webroot_resources}/js/reservation.js"></script>	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->

<style type="text/css">
	{literal}
		.fc-view, .fc-view>table {
		    width: 1500px;
		}
	{/literal}
</style>


{foreach from=$reservations item=item}
  	<div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
  	<div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
  	<div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
  	<div style="display: none" id="{$item.id}mobile" name="{$item.id}mobile">{$item.mobile}</div>
  	<div style="display: none" id="{$item.id}reservation_date" name="{$item.id}reservation_date">{$item.reservation_date}</div>
  	<div style="display: none" id="{$item.id}reservation_time" name="{$item.id}reservation_time">{$item.reservation_time}</div>

	<div style="display: none" id="{$item.id}sale_person_ids" name="{$item.id}sale_person_ids">{$item.sale_person_ids}</div>  
	<div style="display: none" id="{$item.id}product_ids" name="{$item.id}product_ids">{$item.product_ids}</div> 
	<div style="display: none" id="{$item.id}branch_id" name="{$item.id}branch_id">{$item.branch_id}</div>
	<div style="display: none" id="{$item.id}reservation_status_id" name="{$item.id}reservation_status_id">{$item.reservation_status_id}</div>
	<div style="display: none" id="{$item.id}notes" name="{$item.id}notes">{$item.notes}</div> 	
{/foreach}


<form id="frmReservation" name="frmReservation" style="display: none;">
	<input type="text" name="txtReservationId" id="txtReservationId"/>
	<input type="text" name="txtLastName" id="txtLastName"/>
	<input type="text" name="txtFirstName" id="txtFirstName"/>
	<input type="text" name="txtMobile" id="txtMobile"/>
	<input type="text" name="txtNotes" id="txtNotes"/>
	<input type="text" name="txtDate" id="txtDate"/>
	<input type="text" name="txtTime" id="txtTime"/>
	<input type="text" name="txtProducts" id="txtProducts"/>
	<input type="text" name="txtSpecialist" id="txtSpecialist"/>
	<input type="text" name="txtBranch" id="txtBranch"/>
	<input type="text" name="txtStatus" id="txtStatus"/>

	<input type="text" name="txtURL" id="txtURL" value="{$webroot}/"/>
	<input type="text" name="txtEmail" id="txtEmail"/>
</form>
	
<input type="hidden" name="txtSelectedDate" id="txtSelectedDate" value ="{$date_selected}"/>
<script>
var date_select = "{$date_selected}";
{literal}
	(function(factory) {
	if(typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else {
		factory(jQuery);
	}
}(function($) {
	var defaults = {
		todayButton: true,		// Show the button to reset to today's date?
		showInput: true,		// Show input
		weekStart: 1,			// Start day of the week. 0 is Sunday, 6 for Saturday, 1 for Monday (default)
		widget: true,
		cellRatio: 1,
		// format: 'd/m/y',
		format: 'Y-m-d',
		footer: false,
		dayHeader: true,
		mode: 'widget',			// 'widget' (default), 'tiny', 'popup', 'page'
		animDuration: 200,
		transition: '',
		tableClasses: 'table table-condensed',
		hidden: true,
		setOnMonthChange: true,
		condensed: false,
	};
	
	var now = new Date(date_select);
	// alert(date_select);


	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

	// Get the number of days in the current month (or next month if delta == 1, or prev month if delta == -1, etc)
	Date.prototype.daysInMonth = function(delta) {
		delta = delta === undefined ? 0 : delta;

		return new Date(this.getFullYear(), this.getMonth() + 1 + delta, 0).getDate();
	}

	// Is this date today? Can also test against another date when specifying `now`
	Date.prototype.isDay = function(day) {
		if(day === undefined) {
			day = new Date();
		}

		return this.getFullYear() == day.getFullYear() 
			&& this.getMonth() == day.getMonth() 
			&& this.getDate() == day.getDate();
	}

	Date.prototype.isValid = function() {
		return Object.prototype.toString.call(this) === "[object Date]" && !isNaN(this.getTime());
	}

	$.fn.supercal = function(method) {
		// Private methods
		var pMethods = {
			// alert(date_select);
			drawCalendar: function(selectedDate, replace) {
				var options = $(this).data('options');

				selectedDate = selectedDate || now;

				if(replace !== undefined && replace == true) {
					this.empty();
				}

				pMethods.drawHeader(selectedDate, options).appendTo(this);

				var month = pMethods
					.drawMonth(selectedDate, options)
					.addClass('current');

				$('<div />')
					.addClass('supercal-month')
					.html(month)
					.appendTo(this);

				pMethods.drawFooter(selectedDate, options).appendTo(this);

				$(this).data('supercal', true);
				$(this).data('date', selectedDate);
				$(this).data('element', this);

				return this;
			},
			drawPopupCalendar: function(selectedDate, replace) {
				var options = $(this).data('options');

				selectedDate = selectedDate || now;

				var container, calendar;

				if($(this).parent('.supercal-popup-wrapper').length) {
					container = $(this).parent();
					calendar = $(this).parent().find('.supercal-popup');
					calendar.empty();
				} else {
					container = $('<div />').addClass('supercal-popup-wrapper');
					calendar = $('<div />')
						.addClass('supercal supercal-popup')
						.width($(this).outerWidth(true));

					$(this).wrap(container);
				}

				$(this).after(calendar);

				if(options.hidden) {
					calendar.hide();
				}

				pMethods.drawHeader(selectedDate, options).appendTo(calendar);

				var month = pMethods
					.drawMonth(selectedDate, options)
					.addClass('current');

				$('<div />')
					.addClass('supercal-month')
					.html(month)
					.appendTo(calendar);

				calendar.data('supercal', true);
				calendar.data('date', selectedDate);
				calendar.data('element', this);
				calendar.data('options', options);

				$(this).parent().wrap($('<div />').addClass('supercal-affix'));
				
				return this;
			},
			drawHeader: function(date, options) {
				var header = $('<div />').addClass('supercal-header');
				var monthNames = options.shortMonths ? shortMonths : months;

				$('<button />')
					.addClass('prev-month change-month btn')
					.html('&laquo;')
					.appendTo(header);

				$('<button />')
					.addClass('next-month change-month btn')
					.html('&raquo;')
					.appendTo(header);

				$('<span />')
					.addClass('month')
					.append('<div>' + monthNames[date.getMonth()] + ' ' + date.getFullYear() + '</div>')
					.appendTo(header);

				return header;
			},
			drawMonth: function(date, options) {
				date = date || now;
				// date = new(date_select);
				localTime = date.getTime();
				localOffset = date.getTimezoneOffset() * 60000;

				utc = localTime + localOffset;
				offset = 5.5;  
				newdate = utc + (3600000*offset);
				date = new Date(newdate); 

				var monthStart = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0);
				var days = [];
				var rows = [];
				var table = $('<table />').addClass(options.tableClasses);

				var numPrevDays = monthStart.getDay() - options.weekStart;
				var numCurrentDays = date.daysInMonth();
				var numNextDays = 42 - numPrevDays - numCurrentDays;

				var daysInLastMonth = date.daysInMonth(-1);

				// Header
				if(options.dayHeader) {
					var tableHeader = $('<tr />');

					for(var i = 0; i <= 6; i++) {
						var day = i + options.weekStart;

						if(day > 6) {
							day = i - 6;
						}

						$('<th />')
							.text(shortDays[day])
							.appendTo(tableHeader);
					}

					table.append(tableHeader);
				}

				// Add previous month's days
				for(var i = 1; i <= numPrevDays; i++) {
					var day = (daysInLastMonth - numPrevDays) + i;

					days.push({
						date: new Date(date.getFullYear(), date.getMonth() - 1, day, 0, 0, 0),
						displayNumber: day,
						classes: 'month-prev'
					});
				}

				// Add current month's days
				for(var i = 1; i <= numCurrentDays; i++) {
					var day = {
						date: new Date(date.getFullYear(), date.getMonth(), i, 0, 0, 0),
						displayNumber: i,
						classes: ''
					};

					// If this date is today's date, add the `today` class
					day.classes = day.date.isDay() ? 'today' : '';

					// If this date is the one selected, add the `selected` class
					day.classes += day.date.isDay(date) ? ' selected' : '';

					days.push(day);		// Add date to array
				}

				// Add next month's days
				for(var i = 1; i <= numNextDays; i++) {
					days.push({
						date: new Date(date.getFullYear(), date.getMonth() + 1, i, 0, 0, 0),
						displayNumber: i,
						classes: 'month-next'
					});
				}

				// Slice array into 7s
				rows = [
					days.slice(0, 7),
					days.slice(7, 14),
					days.slice(14, 21),
					days.slice(21, 28),
					days.slice(28, 35),
					days.slice(35)
				];

				// Append arrays to table
				for(var row = 0; row < 6; row++) {
					var tr = $('<tr />');

					for(var col = 0; col < 7; col++) {
						var cell = rows[row][col];

						$('<td />')
							.data('date', cell.date)
							.text(cell.displayNumber)
							.addClass(cell.classes)
							.appendTo(tr);
					}

					tr.appendTo(table);
				}

				return table;
			},
			drawFooter: function(date, options) {
				var footer = $('<div />').addClass('supercal-footer input-prepend');

				if(options.footer == false) {
					footer.hide();
				}

				if(options.todayButton) {
					$('<button />')
						.text('Today')
						.addClass('btn supercal-today btn btn-sm btn-primary todaybtn')
						.attr('type', 'button')
						.appendTo(footer);
				}

				if(options.showInput) {
					$('<span />')
						.text(pMethods.formatDate(date, options))
						.addClass('supercal-input uneditable-input span2')
						.attr('id', 'dateFieldReservation')
						.appendTo(footer);
						
					$('<input />')
					.prop('type', 'hidden')
					.val(pMethods.formatDate(date, options))
					.attr('id', 'txtDateFieldReservation')
					.attr('name', 'txtDateFieldReservation')
					.appendTo(footer);
					
					
					
				}

				$('<input />')
					.prop('type', 'hidden')
					.val(parseInt(date.getTime() / 1000), 10)
					.appendTo(footer);

				return footer;
			},
			// Split out into helper object
			formatDate: function(date, options) {		// Verrrry primitive date format function. Does what it needs to do...
				return options.format
					.replace('d', ('0' + date.getDate()).substr(-2))
					.replace('m', ('0' + (date.getMonth() + 1)).substr(-2))
					.replace('y', date.getFullYear().toString().substr(-2))
					.replace('Y', date.getFullYear());
			}
		};

		var methods = {
			init: function(settings) {
				var options = $.extend({}, defaults, settings);

				// Events
				if(!$(document).data('supercal-events')) {
					$(document).on('click.supercal', '.supercal .change-month', function(e) {
							e.preventDefault();
							e.stopPropagation();
							// alert('month');
							methods.changeMonth.apply($(this).closest('.supercal'), [ $(this).hasClass('next-month') ? 1 : -1, $(this).closest('.supercal').data('options') ]);
						})
						.on('click.supercal', '.supercal-today', function() {
							// alert('today');
							methods.changeMonth.apply($(this).closest('.supercal'), [ now, $(this).closest('.supercal').data('options') ]);
						})
						.on('click.supercal', '.supercal table.current td', function() {
							var container = $(this).closest('.supercal');
							var table = $(this).closest('table');

							table.find('.selected').removeClass('selected');

							$(this).addClass('selected');
							// alert($(this).data('date'));

							container.find('.supercal-footer').replaceWith(pMethods.drawFooter($(this).data('date'), $(this).closest('.supercal').data('options')));
							container.data('date', $(this).data('date'));
						})
						// Popups
						.on('click.supercal', '.supercal-popup-trigger', function(e) {
							$(this).parent('.supercal-popup-wrapper').addClass('supercal-open').find('.supercal-popup').show();

							// Close other calendars
							$('.supercal-popup-wrapper.supercal-open').not($(this).parent()).removeClass('supercal-open').find('.supercal-popup').hide();
						})
						.on('click.supercal', function(e) {
							var target = $(e.target);

							if(!target.closest('.supercal-popup-wrapper').length) {
								$('.supercal-popup-wrapper.supercal-open').removeClass('supercal-open').find('.supercal-popup').hide();
							}
							// alert('lems');
						})
						.on('click.supercal', '.supercal td', function() {
							var thisDate = $(this).data('date');
							var originalElement = $(this).closest('.supercal').data('element');
							var formattedDate = pMethods.formatDate($(this).data('date'), $(this).closest('.supercal').data('options'));

							$(originalElement).trigger('dateselect', [ thisDate ]);
							
							// Set date on input element if it exists
							$(originalElement).children('.supercal-popup-trigger').val(formattedDate).trigger('change');

							// showloading();
							// window.location.href = $('#txtURL').val() +"reservation/management/"+formattedDate;

							showloading();
							$('#calendar').fullCalendar( 'gotoDate', formattedDate );
							$.postJSON("?action=get_refresh_events&date="+formattedDate, "", function(data) {
									$("#calendar").fullCalendar('removeEvents');
									$("#calendar").fullCalendar('addEventSource', data.message);
									$("#calendar").fullCalendar('rerenderEvents');
					            closeloading();
					            $("#txtSelectedDate").val(formattedDate);
					        });
						});

					$(document).data('supercal-events', true);
				}

				return this.each(function() {
					if($(this).is(':input')) {
						var element = $('<div />');

						$(this).addClass('supercal-target').after(element);
					} else {
						var element = this;
					}

					$(element).addClass('supercal ' + options.transition);
					$(element).data('options', options);

					if(options.transition) {
						$(element).addClass('transition');
					}

					if(options.condensed) {
						$(element).addClass('condensed');
					}

					switch(options.mode) {
						case 'popup':
							$(element).addClass('supercal-popup-trigger');

							pMethods.drawPopupCalendar.apply(element, options.date);
						break;
						case 'widget':
						default:
							pMethods.drawCalendar.call(element, options.date);
					}
				});
			},
			changeMonth: function(month, options) {
				var newDay, newDate, direction, newCalendar;

				var container = $(this);
				var calendar = $(this).find('table');

				var currentDate = container.data('date');
				var calWidth = calendar.outerWidth(true);
				var calHeight = calendar.outerHeight(true);

				calendar.parent().height(calHeight);		// Set height of calendar's container to height of table

				if(typeof month === 'number') {
					direction = month > 0 ? 1 : -1;
					newDay = Math.min(currentDate.daysInMonth(month), currentDate.getDate());		// 31st of March clamped to 28th Feb, for example
					newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + direction, newDay);
				} else if(month instanceof Date) {
					direction = (month > currentDate) ? 1 : -1;
					newDate = now;
					// alert('month');
				}

				calendar.stop(true, true);
				container.data('date', newDate);
				newCalendar = pMethods.drawMonth(newDate, options).addClass('current');

				switch(options.transition) {
					case 'fade':
						calendar.fadeOut(options.animDuration, function() {
							$(this).replaceWith(newCalendar.hide().fadeIn(options.animDuration));
						});
					break;
					case 'crossfade':		// Crossfade
						calendar.removeClass('current').after(newCalendar);
						calendar.animate({ opacity: 0 }, options.animDuration);

						newCalendar.css({ opacity: 0, position: 'absolute', top: 0 }).animate({ opacity: 1 }, options.animDuration);
					break;
					case 'carousel-horizontal':
						calendar.css({ position: 'absolute' }).animate({ left: -(calWidth * direction) }).after(newCalendar);

						newCalendar.css({ left: direction * calWidth, position: 'absolute' }).animate({ left: 0 });
					break;
					case 'carousel-vertical':		// Vertical slide
						calendar.css({ position: 'absolute' }).animate({ top: -(calHeight * direction) }).after(newCalendar);

						newCalendar.css({ top: direction * calHeight, position: 'absolute' }).animate({ top: 0 });
					break;
					default:		// No transition - default
						calendar.replaceWith(newCalendar);
					break;
				}

				// Remove old calendar
				newCalendar.promise().done(function() {
					container.find('table').not(newCalendar).remove();
				});

				// Update header and footer
				container.find('.supercal-header').replaceWith(pMethods.drawHeader(newDate, options));
				container.find('.supercal-footer').replaceWith(pMethods.drawFooter(newDate, options));

				// Set date on input element if it exists
				if(options.setOnMonthChange) {
					container.prev('.supercal-target').val($(this).data('date')).trigger('change');
				}

				var thisDate = $(this).data('date');
				var originalElement = $(this).closest('.supercal').data('element');
				var formattedDate = pMethods.formatDate($(this).data('date'), $(this).closest('.supercal').data('options'));

				$(originalElement).trigger('dateselect', [ thisDate ]);
				
				// Set date on input element if it exists
				$(originalElement).children('.supercal-popup-trigger').val(formattedDate).trigger('change');

				// showloading();
				// window.location.href = $('#txtURL').val() +"reservation/management/"+formattedDate;
				 var date = new Date(newDate);
				 // alert(date);
				localTime = date.getTime();
				localOffset = date.getTimezoneOffset() * 60000;

				utc = localTime + localOffset;
				offset = 5.5;  
				newdate = utc + (3600000*offset);
				formattedDate = new Date(newdate); 

				// alert(formattedDate);
				// return false;
				var formattedDate = pMethods.formatDate(formattedDate, $(this).closest('.supercal').data('options'));
				//alert(today);
				showloading();
				$('#calendar').fullCalendar( 'gotoDate', formattedDate );
				$.postJSON("?action=get_refresh_events&date="+formattedDate, "", function(data) {
						$("#calendar").fullCalendar('removeEvents');
						$("#calendar").fullCalendar('addEventSource', data.message);
						$("#calendar").fullCalendar('rerenderEvents');
		            closeloading();
		            $("#txtSelectedDate").val(formattedDate);
		        });

				// alert('month');
			},
			date: function() {		// Return current selected date
				if($(this).next('.supercal').length) {
					
					return $(this).next('.supercal').data('date');
					
				} else if($(this).data('supercal')) {
					
					return $(this).data('date');
				}
				// alert('lems');

				return false;
			}
		};

		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if(typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		}
	};
}));
{/literal}	 
</script>


<script>
var datefilter = "{$date_selected}";
var start_time = "{$business_start_time}";
var end_time = "{$business_end_time}";

var screen_ht = $(window).height();
// alert(datefilter);
// alert(end_time);
{literal}

$(function() { // document ready
	
	// alert(datefilter);
	$('.example1').supercal({
         // transition: 'carousel-horizontal',
         // format: 'Y-m-d',
         // defaultDate: datefilter,
   	});
	
	 var date = new Date(datefilter);

	localTime = date.getTime();
	localOffset = date.getTimezoneOffset() * 60000;

	utc = localTime + localOffset;
	offset = 5.5;  
	newdate = utc + (3600000*offset);
	date = new Date(newdate); 

	// alert(date);

	var d = date.getDate(),
    m = date.getMonth(),
    y = date.getFullYear();

	$('#calendar').fullCalendar({
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      height: 600,
      // minTime: "06:00:00",
      minTime: start_time,
	  // maxTime: "23:00:00",
	  maxTime: end_time,
      defaultView: 'agendaDay',
      slotDuration: '00:05:00',
      // defaultDate: new Date(y, m, d),
      allDaySlot: false,
      defaultDate:  datefilter,
      editable: true,
      selectable: true,
      eventLimit: true, // allow "more" link when too many events
      dragScroll: true,
      eventOverlap: true,
      // header: {
        // left: 'prev,next',
        // center: 'title',
        // right: 'agendaDay,agendaWeek,month'
      // },
      views: {
        agendaTwoDay: {
          type: 'agenda',
          duration: { days: 2 },

          // views that are more than a day will NOT do this behavior by default
          // so, we need to explicitly enable it
          groupByResource: true

          //// uncomment this line to group by day FIRST with resources underneath
          //groupByDateAndResource: true
        }
      },
	 viewRender: function(view, element){
	 	// alert(view.start);
	 	var date = new Date(view.start);

		localTime = date.getTime();
		localOffset = date.getTimezoneOffset() * 60000;

		utc = localTime + localOffset;
		offset = 5.5;  
		newdate = utc + (3600000*offset);
		date = new Date(newdate); 


	 	var d = date.getDate();
	 	var m = date.getMonth();
	 	var y = date.getFullYear();
	 	
	 	//alert(formatDate(view.start, 'yyyy-MM-dd'));
	 },
      //// uncomment this line to hide the all-day slot
      //allDaySlot: false,
      
	{/literal}
		
      {$resources_info}
      {$events}
    {literal}
    
      
      eventDrop: function(event, delta, revertFunc) {

        //alert(event.title + " was dropped on " + event.start.format() +" "+ event.resourceId);


		var date = event.start.format();
		date = date.split("T");

        if (!confirm("Are you sure to want change this schedule?")) {
            revertFunc();
        }else{
        	// alert(event.id)
        	//editfromDrop(event.id, date[0], date[1], event.resourceId);
        	editfromDrop(event.id, date[0], date[1], event.resourceId, event);


        	// var i = update_me();
        	// alert(i);
        	// alert(update_me());
        	// return false;
        	if(update_me()==1)
        	{
	    		// var url = window.location.href;
	            // url = url.replace("#",""); 
	            // window.location.href = window.location.href;
        	}else{
        		revertFunc();
        	}
        }

      },
	  editable: true,
      droppable: true,
      select: function(start, end, jsEvent, view, resource) {
      	// alert(resource.id);
      	
      	// add(start, resource.id);
        // console.log(
          // 'select',
          // start.format(),
          // end.format(),
          // resource ? resource.id : '(no resource)'
        // );
      },
      dayClick: function(date, jsEvent, view, resource) {
      	var datetime = new Date(date);

      	var date = new Date(date);

		localTime = date.getTime();
		localOffset = date.getTimezoneOffset() * 60000;

		utc = localTime + localOffset;
		offset = 5.5;  
		newdate = utc + (3600000*offset);
		date = new Date(newdate); 
		
		// var moment = $('#calendar').fullCalendar('getTime');

		// alert(moment);
		// die();

		// return false;

      	//add(date, resource.id); //remove
      	add(date, resource.id, datetime, resource.title); //remove
      	
      	// alert('lems');
        // console.log(
          // 'dayClick',
          // date.format(),
          // resource ? resource.id : '(no resource)'
        // );
      },
      eventClick: function(event, delta, resource){
      	edit(event.id, event);
      },
      
    });

    $(".fc-right").hide(); 
 });
{/literal}

</script>


<section class="vbox">
<section>
	<section class="hbox stretch">
		

  	<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
  		<!-- <br /> -->
                    <section class="vbox">

<!--                         <header class="header bg-primary lter text-center clearfix">
                            <div class="btn-group" onclick="add()">
                                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                <div class="btn-group hidden-nav-xs">
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#makereservation1"> Add New Reservation </button>
                                </div>
                            </div> -->
                        </header>
                        <section>
                            <div class="example1" style="margin:0 auto"></div>
                            <script></script>
                            <!-- <button id="btnrefresh" name="btnrefresh" type="button" class="btn btn-md btn-primary" style="width: 100%;">Refresh</button> -->

	                            <div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color:#bb2e2e;margin-top:20px;">
	                            	  <div class="sidebar-waitingtitle ta-center" style="color:#FFF;">Total Waiting</div>
	                                <div id="divTotalWaiting" name="divTotalWaiting" class="sidebar-numbers ta-center" style="color:#FFF;">{$waiting_count}</div>
	                            </div>
                        </section>
                    </section>
                </aside>
		
 <!-- .content -->
  <section id="content">  	
        <section class="vbox">
            <!-- <header class="header bg-darkblue b-light">
                <div class="row">
                     <div class="col-md-6 col-xs-3">
                        <div class="breadtitle-holder">
                            <div class="breadtitle">
                                <i class="fa fa-calendar titleFA"></i> 
                                <p class="headeerpage-title">Reservation</p>
                            </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-9 ta-right">
                        <div class="breadtitle-holder2">
                            
                             <div class="btn-group" onclick="add()">
                                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                    <div class="btn-group hidden-nav-xs">
                                        <button type="button" class="btn btn-sm btn-primary">Create Reservation</button>
                                  </div>
                             </div>
                        </div>
                     </div>
                </div>
            </header> -->
            
                 
            
            
            <!-- <section class="scrollable wrapper"> -->
            <section class="wrapper">
                    <div id="calendar"></div>


            </section>
        </section>
  </section>
  


 	</section>
  </section>
</section>


<!-- Modal -->

<div id="dlgEditReservationStep1" name="dlgEditReservationStep1" class="modal fade" role="dialog">
  <div class="modal-dialog">
  	   <input type="hidden" name="txtSpecialistId" id="txtSpecialistId">
      <div class="modal-header bg-primary">
      	  
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" name="lblTitleCaption" id="lblTitleCaption">Edit a Reservation</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>


      <!-- Modal content-->
      <div class="modal-content">

      	  	<!-- <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section" name="lblSelectedSpecialist" id="lblSelectedSpecialist"> Select Specialist</span>
                  </div>
            </div> -->

            <form class="form-horizontal form-label-left" id="frmSearch" name="frmSearch">

<!--             <div class="row">
                 <div class="col-md-6 col-sm-6 col-xs-6">   
                 	<div class="form-group">
                        <label>Search Customer</label>
                        <div class="input-group">
                            <input type="text" name="txtMobileSearch" id="txtMobileSearch" class="form-control" placeholder="Mobile Number"> 
                            <span class="input-group-btn"> 
                            	<button class="btn btn-primary" name="btnSearchCustomer" id="btnSearchCustomer">Search</button>
                            </span> 
                       </div>
                  	</div>
                 </div>
	        </div> -->




	          <div class="row" style="display: none;">
	                    <div class="col-md-6 col-sm-6 col-xs-6">
	                    	 <div class="form-group" id="divStatus" name="divStatus" style="display: none;">
	                                <label>Status</label>
	                                    <select name="txtSourceStatus" id="txtSourceStatus" class="form-control m-b">
	                                    	<!-- <option value="-1">NEW</option> -->
	                                        {foreach from=$status item=item}
	                                        	<option value="{$item.id}" {if $item.name eq 'SCHEDULED'} selected="selected"  {/if}>{$item.name}</option>
	                                        {/foreach}
	                                    </select>
	                         </div>
	                    	 
	                         <div class="form-group" style="display: none;">
	                              <label>Set Date: <span class="req">*</span></label>
	                              <div class="input-group date">
	                                  <div class="input-group-addon">
	                                      <i class="fa fa-calendar"></i>
	                                  </div>
	                                  <input type="text" class="form-control pull-right datepicker" id="txtSourceDate" name="txtSourceDate">
	                              </div>
	                          </div>

	                         <div class="bootstrap-timepicker" style="display: none;">
	                            <div class="form-group">
	                                  <label>Set Time:</label>
	                                  <div class="input-group">
	                                    <input type="text" class="form-control timepicker" id="txtSourceTime" name="txtSourceTime">
	                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                                  </div>
	                            </div>
	                        </div>

	                        <!-- <div class="form-group">
	                            <label for="repeatReservation">Repeat:</label>
	                            <select class="form-control" name="repeatReservation">
	                              <option selected="selected" value="">- No Repeat -</option>
	                              <option value="Every Monday">Every Monday</option>
	                              <option value="Every Tuesday">Every Tuesday</option>
	                              <option value="Every Wednesday">Every Wednesday</option>
	                              <option value="Every Thursday">Every Thursday</option>
	                              <option value="Every Friday">Every Friday</option>
	                              <option value="Every Saturday">Every Saturday</option>
	                              <option value="Every Sunday">Every Sunday</option>
	                            </select>
	                        </div> -->
	                    </div>

	                    <!-- <div class="col-md-12">
	                        <div class="form-group">
	                           <label>Reservation Note / Message:</label><br />
	                           <textarea name="txtSourceNotes" id="txtSourceNotes" size="75" maxlength="400" style="width:100%; min-height:75px;"> </textarea>
	                         </div>
	                     </div> -->
	          </div>

	          <!-- Modal content-->
            <!-- <div class="modal-content"> -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <span class="step_descr section" name="lblSelectedSpecialist" id="lblSelectedSpecialist">Scheduling with Barry on February 20, 2017 at 08:00 AM</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                      <div class="division_content">
                          <div class="form-group" id="divSearchCustomer" name="divSearchCustomer">
                              <label>Search Customer</label>
                                <div class="input-group">
                                    <input type="text" name="txtMobileSearch" id="txtMobileSearch" class="form-control" placeholder="Mobile Number"> 
                                    <span class="input-group-btn"> 
                                      <button class="btn btn-primary" name="btnSearchCustomer" id="btnSearchCustomer">Search</button>
                                    </span>
                               </div>
                               <label name="NewClient" class="newclient" id="lblNewCustomer" name="lblNewCustomer" onclick="NewCustomer()" >New Customer</label>
                          </div>

                          


						<!-- <div class="row"> -->
						<div id="divNewCustomer" name="divNewCustomer" style="display: none;">
			        	    <div class="form-group">
		                          <label>First Name</label>
		                          <input type="text" id="txtSourceFirstName" name="txtSourceFirstName" placeholder="First Name" required="required" class="form-control">
		                      </div>
		                      
		                	<div class="form-group">
		                          <label>Last Name</label>
		                          <input type="text" id="txtSourceLastName" name="txtSourceLastName" placeholder="Last Name" required="required" class="form-control">
		                    </div>
		                    
		                    <div class="form-group">
	                               <label>Mobile</label>
	                               <input type="text" name="txtSourceMobile"  data-mask="999-999-9999" data-mask-reverse="true" id="txtSourceMobile" class="form-control" placeholder="Mobile">
	                         </div>

	                         <div class="form-group"> 
                                   <label>Email</label>
                                   <input type="text" name="txtSourceEmail" id="txtSourceEmail" class="form-control" placeholder="Email">
                             </div>
				        </div>


	                      <div class="client-info" id="divCustomerInformation" name="divCustomerInformation" style="display: none;">
	                         <label name="lblSourceFullName" id="lblSourceFullName" class="fullname">Lem Cabuhat</label> <br />
	                         <label><span>Mobile -</span> <span id="lblSourceMobile" name="lblSourceMobile">9238928392</span></label><br/>
	                         <div id="rsrvationDateHolder" hidden>

	                          <div class="col-sm-12"><label>Appointment Time</label></div>
	                         <div class="col-sm-8">
                        <div class="bootstrap-timepicker">
	                         <div class="form-group">
	                          <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="updateReservationTime" id="updateReservationTime" disabled>
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    <input type="hidden" id="check_edit_time">
                                  </div>
                                  </div>
	                      </div>
	                      </div>
	                      <div class="col-sm-4">
	                      	      	<button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="edit_reservation_time();" id="editRsrvBtn">Edit</button>
                                  </div>

	                          <div class="col-sm-12"><label>Appointment Date</label></div>
	                         <div class="col-sm-8">
	                         <div class="form-group">
	                              <!-- <label>Reservation Date<span class="req">*</span></label> -->
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" name="updateReservationDate" id="updateReservationDate" class="form-control pull-right datepicker" disabled>
                                      <input type="hidden" id="check_edit">
                                  </div>
                                  
                                  
                              </div>
                              
	                      </div>
	                      <div class="col-sm-4">
	                      	      	<button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="edit_reservation();" id="editRsrvBtn">Edit</button>
                                  </div>
                                  </div>
	                      </div>



                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-6">
                      <div class="division_content2">
                          <div class="form-group">
                               <label>Reservation Note / Message:</label><br />
                               <textarea name="txtSourceNotes" id="txtSourceNotes" size="75" maxlength="400" style="width:100%; min-height:75px;"> </textarea>
                          </div>
                          <div class="row" id="divServices" name="divServices">
                           

                          </div>
                      </div> 
                    </div>


                   
                </div>
            <!-- </div> -->

          </form>
      </div>
      


    <!-- /. Modal content-->
       <div class="modal-footer">
       		<!-- <button type="button" class="btn btn-primary" onclick="GoToSecondPage()">Next</button> -->
       		<button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button>
            <!-- <a data-toggle="modal" href="#makereservation2" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
      </div>

      
  </div>
</div>






<!-- Modal -->
<div id="dlgEditReservationStep2" name="dlgEditReservationStep2" class="modal fade" role="dialog" >
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reservation</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->

      </div>

      <!-- Modal content-->	
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Select Services</span>
                      <!-- <p>Search Service Name: <input type="text" ng-model="services"></p> -->
                  </div>
              </div>
              
              <!-- <div id="divServices" name="divServices">
		          
	          </div> -->
              

      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#makereservation3" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <!-- <button type="button" class="btn btn-primary" onclick="GoToFinalPage()">Next</button> -->
            <!-- <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button> -->
            <button type="button" class="btn btn-dark" onclick="GoToFirstPage()">Previous</button>
            <!-- <a data-toggle="modal" href="#makereservation1" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
      </div>

      
  </div>
</div>




<!-- Modal -->
<div  id="dlgEditReservationStep3" name="dlgEditReservationStep3" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Specialist</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Specialist</span>
                  </div>
              </div>
              <div class="row" id="divSpecialist" name="divSpecialist">
              	{foreach from=$specialist item=item}
              	  <div class="col-md-6 col-sm-6">
                        <label style="margin-right: 5px; font-size: 14px;">
                        	<input type="checkbox" value="{$item.id}" id="specialist[]" name="specialist[]" class="flat-red"> 
                        	{$item.first_name} {$item.last_name}
                        </label> 
                  </div>
               {/foreach}
              
                  
              </div> 
      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Finish</a>
            <a data-toggle="modal" href="#makereservation2" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
            <!-- <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button> -->
            <button type="button" class="btn btn-dark"  onclick="GoToPreviousSecondPage()">Previous</button>
      </div>
  </div>
</div>

<!-- Modal -->
<div id="EditReservation" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reservation</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
            <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section" name="lblUpdateSelectedSpecialist" id="lblUpdateSelectedSpecialist"> Select Specialist</span>
                      <!-- <p>Search Service Name: <input type="text" ng-model="services"></p> -->
                  </div>
            </div>

                <div class="row">
                      <div class="col-md-5 col-sm-5 col-xs-6">
                              <div class="division_content">
                                    <div class="client-info" style="border-top: 0px solid #E5E5E5 !important;">
                                       <input type="hidden" name="txtUpdateReservationId" id="txtUpdateReservationId"/>
                                       <!-- <label style="margin-bottom:0px;">Customer:</label><br /> -->
                                       <label name="lblUpdateCustomerName" id="lblUpdateCustomerName" class="fullname" style="margin-top: 0px;">Lem Cabuhat</label> <br />
                                       <!-- <label><span>Home Phone -</span> 9238928392</label><br /> -->
                                       <label><span>Mobile: </span><span id="lblUpdateContactNumber" name="lblUpdateContactNumber">9238928392</span></label><br />
                                       <label><span id="lbl" >Status: </span><span id="lblUpdateStatus" name="lblUpdateStatus">NA</span></label><br />
                                       <div class="form-group">
                                         <label><span>Services:</span></label><br />
                                         <label id="lblServices" name="lblServices" style="width:100%;">Services Test</label>
                                       </div>
                                       <div class="form-group">
                                         <label><span>Reservation Note/Message:</span></label><br />
                                         <label id="lblUpdateNotes" name="lblUpdateNotes" style="width:100%;">Testing Note</label>
                                       </div>
                                    </div>
                              </div>
                        </div>
             
                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <div class="division_content2">
                                <div class="row">
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button id="btnEdit" name="btnEdit" onclick="editButton()" type="button" class="btn btn-md btn-primary" style="width: 100%; background-color: #bb2e2e">Edit</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnCancel" name="btnCancel" onclick="change_reservation_status('CANCELLED')" class="btn btn-md btn-primary" style="width: 100%; background-color: #bb2e2e;">Cancel Reservation</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnCheckIn" name="btnCheckIn" onclick="change_reservation_status('CHECKED IN')" class="btn btn-md btn-primary" style="width: 100%;">Checked In</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnConfirm" name="btnConfirm" onclick="change_reservation_status('CONFIRMED')"  class="btn btn-md btn-primary" style="width: 100%;">Confirm</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnDone" name="btnDone" onclick="change_reservation_status('DONE')"  class="btn btn-md btn-primary" style="width: 100%;">Done</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnSendSMSShow" name="btnSendSMSShow" onclick="show_send_sms()"  class="btn btn-md btn-primary" style="width: 100%;">Send SMS</button>
                                   </div>

                                   <!-- <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Cancel Standing</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Reschedule</button>
                                   </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Ticket Notes</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Appointment Log</button>
                                   </div> -->
                                </div>

                                <!-- <div class="todayAppointment">
                                  <div class="row todayAppointment_killrow">
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">Service</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">When</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">with</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">Schedule By</div>
                                  </div>
                                  <div class="todayAppointment_list">
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                     <button type="button" class="btn btn-md btn-primary">Print</button>
                                     <button type="button" class="btn btn-md btn-primary">Email</button>
                                  </div>
                                </div> -->


                            </div>

                        </div>


                   </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
           </div>
  </div>
</div>


<div id="dlgMessageCustomer" name="dlgMessageCustomer" class="modal fade" role="dialog">
 <form id="frmSendMessage" name="frmSendMessage">
 	
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Alert SMS</h4>
                <p id="pSendTo" name="pSendTo">Send Message to customer right now</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Message</label>
                             <textarea rows="2" cols="50" id="txtMessage" name="txtMessage"  class="form-control" style="width:100%;">In a few minutes your reservation to 'Beauty Spa' available, please confirm if you still want to avail our services.</textarea>
                        </div>
                     </div>
                </div> 
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Send it now</a> -->
                <button type="button" class="btn btn-md btn-primary" name="btnSendSMS" id="btnSendSMS" onclick="send_sms()">Send SMS</button> 
                <button class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
  </form>
</div>




