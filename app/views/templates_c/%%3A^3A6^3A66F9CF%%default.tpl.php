<?php /* Smarty version 2.6.26, created on 2017-05-20 14:12:50
         compiled from default.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'default.tpl', 145, false),)), $this); ?>
<!DOCTYPE html>
<html lang="en" class="app js no-touch no-android chrome no-firefox no-iemobile no-ie no-ie10 no-ie11 no-ios no-ios7 ipad">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>GO3 Reservation</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/favicon.ico">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/font.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/bootstrap_calendar.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/go3reservation.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/fuelux.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/custom.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/custom.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/jquery.paginate.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/bootstrap-timepicker.min.css">
    <link href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/datatables/jquery.dataTables.css" rel="stylesheet">
    
    <link href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/calendar/fullcalendar_002.css" rel="stylesheet" media="print">
    <link href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/calendar/scheduler.css" rel="stylesheet">

     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery-2.2.3.min.js"></script>
    
    <!-- <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> -->
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.paginate.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/app.v1.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/fuelux.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/parsley.min.js"></script>
   <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.flot.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.flot.resize.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.flot.grow.js"></script> 
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/demo.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/bootstrap_calendar.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.sortable.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/app.plugin.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.smartWizard.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/custom.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/bootstrap-colorpicker.min.js"></script>
   
    
    
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.tinycarousel.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.paginate.js"></script>
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/daterangepicker.js"></script>
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/fullcalendar.min.js"></script>
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/calendar/fullcalendar.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/calendar/scheduler.js"></script>
    


    <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.dataTables.min.js"></script>
    <!-- <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/dataTables.bootstrap.min.js"></script> -->

	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/public.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    
    <style type="text/css">
        <?php echo '
            td span.month, td span.year{
                padding: 5px;
            }
        '; ?>

    </style>
    

</head>

<body class="" style="">
    <section class="vbox">
        <!-- .header -->
         <header class="bg-dark dk header navbar navbar-fixed-top-xs">
            <div class="navbar-header aside-lg aside-md aside-sm">
                <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html"> <i class="fa fa-bars"></i> </a>
                <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/reservation/management" class="navbar-brand" ><img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/logo.png" class="m-r-sm">Reservation</a>
                <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> <i class="fa fa-cog"></i> </a>
            </div>
            <?php if ($this->_tpl_vars['userType'] != 1): ?>
            <ul class="nav navbar-nav hidden-xs">
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/waiting_list/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'waiting_list' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-book"></i> Waiting List </a> </li>
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/reservation/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'reservation' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-calendar"></i> Reservation</a> </li>
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/category/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'category' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-list-alt"></i> Categories </a> </li>
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/service/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'service' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-list-alt"></i> Services </a> </li>
                <!-- <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/branch/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'branch' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-list-alt"></i> Branch </a> </li> -->
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/specialist/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'specialist' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-users"></i> Specialist</a> </li>
                <!-- <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant_user/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'merchant_user' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-users"></i> Users</a> </li> -->
                <li>  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/history/management" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'history' ) !== false): ?> dker <?php endif; ?>"> <i class="fa fa-clock-o"></i>History</a> </li>
            </ul>
            <?php else: ?>
            <ul class="nav navbar-nav hidden-xs">
                <li>
                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/merchant" class="<?php if (strpos ( $this->_tpl_vars['action_tpl'] , 'admin' ) !== false): ?> dker <?php endif; ?>">
                        <i class="fa fa-users"></i> Merchants
                    </a>            
                </li>
            </ul>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                <li class="hidden-xs">
                    <!-- <a href="#" class="dropdown-toggle dk" data-toggle="dropdown"> <i class="fa fa-bell"></i> <span class="badge badge-sm up bg-stanadard m-l-n-sm count" style="display: inline-block;">3</span> </a> -->
                    <section class="dropdown-menu aside-xl">
                        <section class="panel bg-white">
                            <!-- <header class="panel-heading b-light bg-light"> <strong>You have <span class="count" style="display: inline;">3</span> notifications</strong> </header>
                            <div class="list-group list-group-alt animated fadeInRight">
                                <a href="#" class="media list-group-item" style="display: block;"><span class="pull-left thumb-sm text-center"><i class="fa fa-envelope-o fa-2x text-success"></i></span><span class="media-body block m-b-none">Sophi sent you a email<br><small class="text-muted">1 minutes ago</small></span></a>
                                <a href="#" class="media list-group-item"> <span class="pull-left thumb-sm"> <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/avatar.jpg" alt="John said" class="img-circle"> </span> <span class="media-body block m-b-none"> Use awesome animate.css<br> <small class="text-muted">10 minutes ago</small> </span> </a>
                                <a href="#" class="media list-group-item"> <span class="media-body block m-b-none"> 1.0 initial released<br> <small class="text-muted">1 hour ago</small> </span> </a>
                            </div>
                            <footer class="panel-footer text-sm"> <a href="#" class="pull-right"><i class="fa fa-cog"></i></a> 
                            <a href="">See all the notifications</a> </footer> -->
                        </section>
                    </section>
                </li>
             <!--    <li class="dropdown hidden-xs"> <a href="http://flatfull.com/themes/note/index.html#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
                    <section class="dropdown-menu aside-xl animated fadeInUp">
                        <section class="panel bg-white">
                            <form role="search">
                                <div class="form-group wrapper m-b-none">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button> </span> </div>
                                </div>
                            </form>
                        </section>
                    </section>
                </li> -->
                <li>
                	<p class="datetime_m"><span style="font-weight:bold;">Date Today:</span> <?php echo ((is_array($_tmp=$this->_tpl_vars['current_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")); ?>
<br/>
                	   <span style="font-weight:bold;">Current Time:</span><?php echo $this->_tpl_vars['current_time']; ?>

                    </p>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> 
                    <?php if ($this->_tpl_vars['profile']['logo'] == ''): ?>
                        <img src="<?php echo $this->_tpl_vars['webroot']; ?>
/images/store-avatar.png"/></span>
                    <?php else: ?>
                        <img src="<?php echo $this->_tpl_vars['profile']['logo']; ?>
" /></span>
                    <?php endif; ?>
                    <!-- <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/avatar.jpg"> </span>  -->
					<span class="merchant_name">
                        <?php 
    						print_r($_SESSION['fname']." ". $_SESSION['lname']);
    					 ?>
                    </span>
                     <b class="caret"></b> </a>
                    <ul class="dropdown-menu animated fadeInRight"> <span class="arrow top"></span>
                    <?php 
                        if($_SESSION['user_type'] != '1'){
                     ?>
                        <li> <a  href="<?php echo $this->_tpl_vars['webroot']; ?>
/setting/management">Settings</a> </li>
                        <li> <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/my_profile">Profile</a> </li>
                    <?php 
                        }
                     ?>
                        <!-- <li><a href="#"> <span class="badge bg-stanadard pull-right">3</span> Notifications </a></li> -->
                        <!-- <li> <a href="">Help</a> </li> -->
                        <!-- <li> <a href="lockpage.php" data-toggle="ajaxModal">Lock</a> </li> -->
                        <li class="divider"></li>
                        <li> <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/log/out">Logout</a> </li>
                    </ul>
                </li>
            </ul>
        </header>
        <!-- /.header -->
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <!-- <aside class="bg-dark lter aside-lg hidden-print hidden-xs" id="nav">
                    <section class="vbox">
                        <header class="header bg-primary lter text-center clearfix">
                            <div class="btn-group"> -->
                                <!-- <button type="button" class="btn btn-sm btn-dark btn-icon" onclick="add_service()" title="New project"><i class="fa fa-plus"></i></button> -->
                                <!-- <div class="btn-group hidden-nav-xs"> -->
                                    <!-- <button type="button" class="btn btn-sm btn-primary" onclick="add_service()"> Add New Reservation </button> -->
                                <!-- </div>
                            </div>
                        </header>
                        <section>
                            <div class="sidebar-titleholder">
                                <div class="sidebar-title ta-center">Analytics</div>
                            </div>
                            
                            <?php $_from = $this->_tpl_vars['analytics']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	                            <div class="sidebar-module">
	                                <div class="sidebar-waitingtitle ta-center">Total <?php echo $this->_tpl_vars['item']['name']; ?>
</div>
	                                <div class="sidebar-numbers ta-center"><?php echo $this->_tpl_vars['item']['total']; ?>
</div>
	                            </div>
                            <?php endforeach; endif; unset($_from); ?>
                        </section>
                     
                       
                    </section>
                </aside> -->              
                  <!-- /.aside -->

                 <!-- .content -->
                
                	<!-- contents should be in here  -->
                	
                	<!-- <div> -->
                		 <?php $this->assign('class', $this->_tpl_vars['meta']['class']); ?>
                <?php if ($this->_tpl_vars['action_tpl'] != ''): ?>
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['action_tpl']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <?php endif; ?>
                	<!-- </div> -->
                
                <!-- /.content -->
               
            </section>
        </section>
    </section>



  <!-- .footer -->
        <!-- Bootstrap -->
    <!-- App -->

    











<!-- Modal -->
<div id="dlgViewService" name="dlgViewService" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="row">
            <div class="col-md-4">
                <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/servicebg-modal.jpg" class="servicesbg-modal" />
            </div>
            <div class="col-md-8">
                <div class="service-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row">
                         <div class="col-md-6">
                            <div class="title-services" id="view_product_name" name="view_product_name">Deluxe Pedicure</div>
                            <div class="description-services-modal" id="view_description" name="view_description">The same application is applied as the European Pedicure with a soft sugar scrub in place of the light sea salt for the same application is applied as the European Pedicure with a soft sugar scrub in place of the light sea salt for</div><br />
                            <p><span>Price</span>: <label id="view_price" name="view_price">45.00</label></p>
                            <p><span>Cost</span>: <label id="view_cost" name="view_cost">5.00</label></p>
                            <p style="display: none;"><span>Type</span>: <label id="view_product_type" name="view_product_type">Inventory</label></p>
                            <p style="display: none;"> id="inventory_quantity" name="inventory_quantity"><span>Quantity</span>: <label id="view_quantity" name="view_quantity">5</label></p>
                            <p id="minutes_of_service" name="minutes_of_service"><span>Duration in Minutes</span>: <label id="view_minutes" name="view_minutes">5</label> min.</p>
                            <p><span>Category</span>: <label id="category" name="category">5</label></p>
                            
                         </div>
                         <div class="col-md-6 ta-right">
                            <div class="price-services" id="view_big_price" name="view_big_price">$45</div>
                         </div>
                         <div class="clear"></div>
                         <div class="ta-right">
                              <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='servicesprocess.php'">Edit Service</button> -->
                              <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /. Modal content-->

  </div>
</div>




<!-- Modal -->
<div id="dlgEditService" name="dlgEditService" class="modal fade" role="dialog">
<form id="frmEditService" name="frmEditService">	
	<input type="hidden" name="txtProductId" id="txtProductId">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Service Information</h4>
                <p>Enter required information</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
            <div class="alert alert-danger error-prompt" role="alert" hidden>
                        <div class="error-holder">
                         
                        </div>
                    </div>
                    
                <div class="row">
                    
                         <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Service Name</label>
                                <input type="text" name="txtName" id="txtName" class="form-control" placeholder="Services"> 
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                 <textarea rows="5" cols="50" id="txtDescription" name="txtDescription"  class="form-control" style="width:100%;"></textarea>
                            </div>
                         </div>
                         <div class="col-md-3 col-xs-3">
                            <div class="form-group">
                                <label>Cost</label>
                                <input type="text" name="txtCost" id="txtCost" class="form-control" placeholder="Cost"> 
                            </div>
                            
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" id="txtPrice" name="txtPrice" class="form-control" placeholder="Price"> 
                            </div>
                            
                            <div class="form-group" style="display: none;" >
                                <label>Product Type</label>
								<select id="txtProductType" name="txtProductType" class="form-control m-b">
								  <!-- <option value="SERVICE">SERVICE</option> -->
								  <!-- <option value="NON-INVENTORY">NON-INVENTORY</option>
								  <option value="INVENTORY">INVENTORY</option> -->
								  
								  
								  
								</select>
                            </div>
                            
                             
                         </div>
                         <div class="col-md-3 col-xs-3">
                            
                            <div class="form-group" id="divMinutes" name="divMinutes" style="display:none;">
                                <label>Duration in Minutes</label>
                                <div id="MySpinner" class="spinner input-group" data-min="1" data-max="180">
                                    <input type="text" class="form-control spinner-input" name="txtMinutesOfService" id="txtMinutesOfService" value="1" name="spinner" maxlength="4">
                                    <div class="btn-group btn-group-vertical input-group-btn">
                                        <button type="button" class="btn btn-default spinner-up"> <i class="fa fa-chevron-up text-muted"></i> </button>
                                        <button type="button" class="btn btn-default spinner-down"> <i class="fa fa-chevron-down text-muted"></i> </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group" id="divQuantity" name="divQuantity" style="display:none;">
                                <label>Available Quantity</label>
                                <input class="form-control" type="text" placeholder="Quantity" name="txtQuantity" id="txtQuantity" value="0">
                            </div>

                         </div>
                         
                         <div class="col-md-6 col-xs-6">
                         	<div class="form-group">
                                <label>Category</label>
								<select id="txtCategory" name="txtCategory" class="form-control m-b">
								  <?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
								  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
								  <?php endforeach; endif; unset($_from); ?>
								</select>
                            </div>
                         </div>
                   
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" id="btnUpdateService" name="btnUpdateService">Save Service</button>
                <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='serviceslist.php'">Save Service</button> -->
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</form>
</div>

<div id="dlgLoading" name="dlgLoading" class="modal fade" role="dialog">
  <div class="modal-dialog3" style="width:100px;">
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;margin: 180px auto !important;">
        <div class="row">
            <div class="col-md-12 col-xs-12">
               <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/loading.gif" class="servicesbg-modal" />
                <p>Please wait...</p>
            </div>
           <!--  <div class="col-md-8 col-xs-8">
                <div class="service-content">
                    <div class="success-title">Go3Reservation</div>
                    <p>Please wait...</p>
                </div>
            </div> -->
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>

<!-- make it dynamic content sir lems :p Modal -->
<div id="dlg_message" name="dlg_message" class="modal fade" role="dialog">
  <div class="modal-dialog2">
<div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" style="padding-top: 20px;color: #000;position: absolute;z-index: 999; font-size: 35px;float: left;right: 10px;">&times;</button>
        </div>
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-3">
                <!-- <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/update.png" class="servicesbg-modal" /> -->

                <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/success.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-9">
                <div class="service-content">
                     <div class="success-title" id="lbltitle" name="lbltitle">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessage" name="dlg_lblMessage">
                    		Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
            </div>
            <!-- <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button> -->
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>

<!-- make it dynamic content sir lems :p Modal -->
<div id="dlg_messageerror" name="dlg_messageerror" class="modal fade" role="dialog">
  <div class="modal-dialog2">
    <!-- <div class="modal-header bg-primary"> -->
    <!-- </div> -->
 <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" style="padding-top: 20px;color: #000;position: absolute;z-index: 999; font-size: 35px;float: left;right: 10px;">&times;</button>
        </div>

         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-3">
                <!-- <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/update.png" class="servicesbg-modal" /> -->
                <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/error.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-9">
                <div class="service-content">
                     <div class="success-title" id="lbltitleerror" name="lbltitleerror">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessageerror" name="dlg_lblMessagerrror">
                    		Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
                
            </div>

            <!--<button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>-->
        </div>

    </div>

    <!-- /. Modal content-->
  </div>
</div>






 <!-- /.footer -->

 <!-- Modal -->
<div id="EditReservation" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reservation</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
            <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section" name="lblUpdateSelectedSpecialist" id="lblUpdateSelectedSpecialist"> Select Specialist</span>
                      <!-- <p>Search Service Name: <input type="text" ng-model="services"></p> -->
                  </div>
            </div>

                <div class="row">
                      <div class="col-md-5 col-sm-5 col-xs-6">
                              <div class="division_content">
                                    <div class="client-info" style="border-top: 0px solid #E5E5E5 !important;">
                                       <input type="hidden" name="txtUpdateReservationId" id="txtUpdateReservationId"/>
                                       <!-- <label style="margin-bottom:0px;">Customer:</label><br /> -->
                                       <label name="lblUpdateCustomerName" id="lblUpdateCustomerName" class="fullname" style="margin-top: 0px;">Lem Cabuhat</label> <br />
                                       <!-- <label><span>Home Phone -</span> 9238928392</label><br /> -->
                                       <label><span>Mobile: </span><span id="lblUpdateContactNumber" name="lblUpdateContactNumber">9238928392</span></label><br />
                                       <label><span id="lbl" >Status: </span><span id="lblUpdateStatus" name="lblUpdateStatus">NA</span></label><br />
                                       <div class="form-group">
                                         <label><span>Services:</span></label><br />
                                         <label id="lblServices" name="lblServices" style="width:100%;">Services Test</label>
                                       </div>
                                       <div class="form-group">
                                         <label><span>Reservation Note/Message:</span></label><br />
                                         <label id="lblUpdateNotes" name="lblUpdateNotes" style="width:100%;">Testing Note</label>
                                       </div>
                                    </div>
                              </div>
                        </div>
             
                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <div class="division_content2">
                                <div class="row">
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button id="btnEdit" name="btnEdit" onclick="editButton()" type="button" class="btn btn-md btn-primary" style="width: 100%; background-color: #bb2e2e">Edit</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnCancel" name="btnCancel" onclick="change_reservation_status('CANCELLED')" class="btn btn-md btn-primary" style="width: 100%; background-color: #bb2e2e;">Cancel Reservation</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnCheckIn" name="btnCheckIn" onclick="change_reservation_status('CHECKED IN')" class="btn btn-md btn-primary" style="width: 100%;">Checked In</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnConfirm" name="btnConfirm" onclick="change_reservation_status('CONFIRMED')"  class="btn btn-md btn-primary" style="width: 100%;">Confirm</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnDone" name="btnDone" onclick="change_reservation_status('DONE')"  class="btn btn-md btn-primary" style="width: 100%;">Done</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" id="btnSendSMSShow" name="btnSendSMSShow" onclick="show_send_sms()"  class="btn btn-md btn-primary" style="width: 100%;">Send SMS</button>
                                   </div>

                                   <!-- <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Cancel Standing</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Reschedule</button>
                                   </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Ticket Notes</button>
                                   </div>
                                   <div class="col-md-4 col-sm-4 col-xs-4">
                                     <button type="button" class="btn btn-md btn-primary" style="width: 100%;">Appointment Log</button>
                                   </div> -->
                                </div>

                                <!-- <div class="todayAppointment">
                                  <div class="row todayAppointment_killrow">
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">Service</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">When</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">with</div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_th">Schedule By</div>
                                  </div>
                                  <div class="todayAppointment_list">
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                     <div class="row todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                    <div class="row todayAppointment_odd todayAppointment_killrow">
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Pedicure</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Feb 15, 2017 8:30 AM</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">New Employee</div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 todayAppointment_td">Patrick Nguyen</div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                     <button type="button" class="btn btn-md btn-primary">Print</button>
                                     <button type="button" class="btn btn-md btn-primary">Email</button>
                                  </div>
                                </div> -->


                            </div>

                        </div>


                   </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
           </div>
  </div>
</div>

<!-- Modal -->

<div id="dlgEditReservationStep1" name="dlgEditReservationStep1" class="modal fade" role="dialog">
  <div class="modal-dialog">
       <input type="hidden" name="txtSpecialistId" id="txtSpecialistId">
      <div class="modal-header bg-primary">
          
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" name="lblTitleCaption" id="lblTitleCaption">Edit a Reservation</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>


      <!-- Modal content-->
      <div class="modal-content">
            <div class="alert alert-danger error-prompt" role="alert" hidden>
                <div class="error-holder">
                  <!-- <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  <span class="error-msg">Enter a valid email address</span> -->
                </div>
            </div>
            <!-- <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section" name="lblSelectedSpecialist" id="lblSelectedSpecialist"> Select Specialist</span>
                  </div>
            </div> -->

            <form class="form-horizontal form-label-left" id="frmSearch" name="frmSearch">

<!--             <div class="row">
                 <div class="col-md-6 col-sm-6 col-xs-6">   
                    <div class="form-group">
                        <label>Search Customer</label>
                        <div class="input-group">
                            <input type="text" name="txtMobileSearch" id="txtMobileSearch" class="form-control" placeholder="Mobile Number"> 
                            <span class="input-group-btn"> 
                                <button class="btn btn-primary" name="btnSearchCustomer" id="btnSearchCustomer">Search</button>
                            </span> 
                       </div>
                    </div>
                 </div>
            </div> -->




              <div class="row" style="display: none;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                             <div class="form-group" id="divStatus" name="divStatus" style="display: none;">
                                    <label>Status</label>
                                        <select name="txtSourceStatus" id="txtSourceStatus" class="form-control m-b">
                                            <!-- <option value="-1">NEW</option> -->
                                            <?php $_from = $this->_tpl_vars['status']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                                <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['name'] == 'SCHEDULED'): ?> selected="selected"  <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </select>
                             </div>
                             
                             <div class="form-group" style="display: none;">
                                  <label>Set Date: <span class="req">*</span></label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right datepicker" id="txtSourceDate" name="txtSourceDate">
                                  </div>
                              </div>

                             <div class="bootstrap-timepicker" style="display: none;">
                                <div class="form-group">
                                      <label>Set Time:</label>
                                      <div class="input-group">
                                        <input type="text" class="form-control timepicker" id="txtSourceTime" name="txtSourceTime">
                                        <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                      </div>
                                </div>
                            </div>

                            <div class="bootstrap-timepicker" style="display: none;">
                                <div class="form-group">
                                      <label>Set End Time:</label>
                                      <div class="input-group">
                                        <input type="text" class="form-control timepicker" id="txtSourceEndTime" name="txtSourceEndTime">
                                        <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                      </div>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label for="repeatReservation">Repeat:</label>
                                <select class="form-control" name="repeatReservation">
                                  <option selected="selected" value="">- No Repeat -</option>
                                  <option value="Every Monday">Every Monday</option>
                                  <option value="Every Tuesday">Every Tuesday</option>
                                  <option value="Every Wednesday">Every Wednesday</option>
                                  <option value="Every Thursday">Every Thursday</option>
                                  <option value="Every Friday">Every Friday</option>
                                  <option value="Every Saturday">Every Saturday</option>
                                  <option value="Every Sunday">Every Sunday</option>
                                </select>
                            </div> -->
                        </div>

                        <!-- <div class="col-md-12">
                            <div class="form-group">
                               <label>Reservation Note / Message:</label><br />
                               <textarea name="txtSourceNotes" id="txtSourceNotes" size="75" maxlength="400" style="width:100%; min-height:75px;"> </textarea>
                             </div>
                         </div> -->
              </div>

              <!-- Modal content-->
            <!-- <div class="modal-content"> -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <span class="step_descr section" name="lblSelectedSpecialist" id="lblSelectedSpecialist">Scheduling with Barry on February 20, 2017 at 08:00 AM</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                      <div class="division_content">
                          <div class="form-group" id="divSearchCustomer" name="divSearchCustomer">
                              <label>Search Customer</label>
                                <div class="input-group">
                                    <input type="text" name="txtMobileSearch" id="txtMobileSearch" class="form-control" placeholder="Mobile Number"> 
                                    <span class="input-group-btn"> 
                                      <button class="btn btn-primary" name="btnSearchCustomer" id="btnSearchCustomer">Search</button>
                                    </span>
                               </div>
                               <label name="NewClient" class="newclient" id="lblNewCustomer" name="lblNewCustomer" onclick="NewCustomer()" >New Customer</label>
                          </div>

                        <!-- <div class="row"> -->
                        <div id="divNewCustomer" name="divNewCustomer" style="display: none;">
                            <div class="form-group">
                                  <label>First Name</label>
                                  <input type="text" id="txtSourceFirstName" name="txtSourceFirstName" placeholder="First Name" required="required" class="form-control">
                              </div>
                              
                            <div class="form-group">
                                  <label>Last Name</label>
                                  <input type="text" id="txtSourceLastName" name="txtSourceLastName" placeholder="Last Name" required="required" class="form-control">
                            </div>
                            
                            <div class="form-group">
                                   <label>Mobile</label>
                                   <input type="text" name="txtSourceMobile"  data-mask="999-999-9999" data-mask-reverse="true" id="txtSourceMobile" class="form-control" placeholder="Mobile">
                             </div>
                             <!-- added -->
                             <div class="form-group"> 
                                   <label>Email</label>
                                   <input type="text" name="txtSourceEmail" id="txtSourceEmail" class="form-control" placeholder="Email">
                             </div>
                        </div>


                          <div class="client-info" id="divCustomerInformation" name="divCustomerInformation" style="display: none; border-top: 0px solid #E5E5E5;">
                             <label name="lblSourceFullName" id="lblSourceFullName" class="fullname">Lem Cabuhat</label> <br />
                             <label><span>Mobile -</span> <span id="lblSourceMobile" name="lblSourceMobile">9238928392</span></label><br/><br/>
                             <div class="col-sm-12"><button class="btn btn-default" type="button" onclick="change_date_time()">Change Date & Time</button></div>
                             <input type="hidden" id="check_edit">
                             <input type="hidden" id="check_edit_time">
                              <!-- <div id="rsrvationDateHolder" hidden>

                              <div class="col-sm-12"><label>Appointment Time</label></div>
                             <div class="col-sm-8">
                        <div class="bootstrap-timepicker">
                             <div class="form-group">
                              <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="updateReservationTime" id="updateReservationTime" disabled>
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    <input type="hidden" id="check_edit_time">
                                  </div>
                                  </div>
                          </div>
                          </div>
                          <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="edit_reservation_time();" id="editRsrvBtn">Edit</button>
                                  </div>

                              <div class="col-sm-12"><label>Appointment Date</label></div>
                             <div class="col-sm-8">
                             <div class="form-group">
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" name="updateReservationDate" id="updateReservationDate" class="form-control pull-right datepicker" disabled>
                                      <input type="hidden" id="check_edit">
                                  </div>
                                  
                                  
                              </div>
                              
                          </div>
                          <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="edit_reservation();" id="editRsrvBtn">Edit</button>
                                  </div>
                            </div> -->
                          </div>


                      </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-6">
                      <div class="division_content2">
                          <div class="form-group">
                               <label>Reservation Note / Message:</label><br />
                               <textarea name="txtSourceNotes" id="txtSourceNotes" size="75" maxlength="400" style="width:100%; min-height:10px; line-height: 10px;"> </textarea>
                          </div>
                          <div class="row" id="divServices" name="divServices">
                           

                          </div>
                      </div> 
                    </div>


                   
                </div>
            <!-- </div> -->

          </form>
      </div>
      


    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <button type="button" class="btn btn-primary" onclick="GoToSecondPage()">Next</button> -->
            <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button>
            <!-- <a data-toggle="modal" href="#makereservation2" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
      </div>

      
  </div>
</div>

<!-- Modal -->
<div  id="dlgEditReservationStep3" name="dlgEditReservationStep3" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Specialist</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Specialist</span>
                  </div>
              </div>
              <div class="row" id="divSpecialist" name="divSpecialist">
                <?php $_from = $this->_tpl_vars['specialist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                  <div class="col-md-6 col-sm-6">
                        <label style="margin-right: 5px; font-size: 14px;">
                            <input data-encode='<?php echo $this->_tpl_vars['item']['encoded_sched']; ?>
' data-off="1" type="checkbox" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" id="specialist[]" name="specialist[]" class="flat-red"> 
                            <?php echo $this->_tpl_vars['item']['first_name']; ?>
 <?php echo $this->_tpl_vars['item']['last_name']; ?>

                        </label> 
                  </div>
               <?php endforeach; endif; unset($_from); ?>
              
                  
              </div> 
      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Finish</a>
            <a data-toggle="modal" href="#makereservation2" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
            <!-- <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button> -->
            <button type="button" class="btn btn-dark"  onclick="GoToPreviousSecondPage()">Previous</button>
      </div>
  </div>
</div>

<div id="dlgMessageCustomer" name="dlgMessageCustomer" class="modal fade" role="dialog">
 <form id="frmSendMessage" name="frmSendMessage">
    
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Alert SMS</h4>
                <p id="pSendTo" name="pSendTo">Send Message to customer right now</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Message</label>
                             <textarea rows="2" cols="50" id="txtMessage" name="txtMessage"  class="form-control" style="width:100%;">In a few minutes your reservation to 'Beauty Spa' available, please confirm if you still want to avail our services.</textarea>
                        </div>
                     </div>
                </div> 
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Send it now</a> -->
                <button type="button" class="btn btn-md btn-primary" name="btnSendSMS" id="btnSendSMS" onclick="send_sms()">Send SMS</button> 
                <button class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
  </form>
</div>

 <!-- Modal -->
<div id="editDateTimeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reservation</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
            <div class="alert alert-danger error-prompt" role="alert" hidden>
                <div class="error-holder">
                  <!-- <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  <span class="error-msg">Enter a valid email address</span> -->
                </div>
            </div>
            <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section" name="lblUpdateSelectedSpecialist" id="lblUpdateSelectedSpecialist">Change Date and Time of Reservation</span>
                  </div>
            </div>

                <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                              <div class="division_content">
                                    <div style="border-top: 0px solid #E5E5E5 !important;">
                                       <div class="form-group">
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="hidden" name="updateReservationTime2" id="updateReservationTime2" class="form-control pull-right datepicker">
                                      <input onkeydown="return false" type="text" name="updateReservationDate2" id="updateReservationDate2" class="form-control pull-right updateReservationDate2">
                                  </div>
                              </div>
                              <button id="" onclick="showSuggestions()" class="btn btn-primary" type="button">Check for available time.</button>
                                    </div>
                              </div>
                        </div>
             
                        <div class="col-md-6 col-sm-6 col-xs-6" id="suggest">
                            <div class="division_content2">
                                   <ul id="ulListReserve" class="list">
                                   </ul>
                                   <ul class="pagination pull-right"></ul>
                            </div>
                        </div>
                   </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
           </div>
  </div>
</div>



</body>

</html>