<?php /* Smarty version 2.6.26, created on 2017-04-24 22:38:18
         compiled from log_forgotpassword.tpl */ ?>
<!-- Main content -->
 <div class="container">  
    <section class="content">
        <div class="row">
            <div class="login-position">
                <div class="col-lg-8 col-sm-8">
                    <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/logo-login.png" />
                    <div class="login-logo-subtitle">Your online reservation solution for better service to make <br />your business grow</div>
                </div>
             
                 <div class="col-lg-4 col-sm-4">
                       <div class="login-box-header"> Reset Password</div>
                       <div class="login-box-body"> 
                           
                        
                            <form action="" method="post" id="frmNewPassword">
                                <div class="form-group has-feedback">
                                	<input type="hidden" name="code" value="<?php echo $this->_tpl_vars['data']; ?>
">
                                	<input type="password" id="new_password" name="new_password" class="form-control login-input" placeholder="New Password" value="">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password"name="confirm_new_password" id="confirm_new_password" class="form-control login-input" placeholder="Confirm New Password" value="">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                     

                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                                        
                                    </div>
                                    
                                </div>
                         </div>
                     <!-- /.login-box-body -->
                </div>
            </div>
        </div>
    </section>
 </div>
