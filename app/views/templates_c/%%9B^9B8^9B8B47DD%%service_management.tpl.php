<?php /* Smarty version 2.6.26, created on 2017-05-19 18:18:54
         compiled from service_management.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'service_management.tpl', 51, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/service.js"></script>
 <section id="content">
    <section class="vbox" id="paginateDiv">
       <header class="header bg-darkblue b-b b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">Services Offer</p>
                                            </div>
                                        </div>
                                     </div>
                                     <!--<div class="col-md-1 col-xs-3 ta-right">
                                         <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div> 
                                    </div>-->
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New Product and Service" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add_service()">Add New Service</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                      <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search product or category" />
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable wrapper">
        	<ul id="example"  class="list paginated-list">
	
	            	<?php $_from = $this->_tpl_vars['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	                     <li class="col-lg-4 col-md-6 col-xs-12">
	                        <div class="services-holder">
	                            <div class="row">
	                                <div class="col-md-8 col-xs-4">
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
cost" name="<?php echo $this->_tpl_vars['item']['id']; ?>
cost"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['cost'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</div>
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_type" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_type"><?php echo $this->_tpl_vars['item']['product_type']; ?>
</div>
	                                	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
minutes" name="<?php echo $this->_tpl_vars['item']['id']; ?>
minutes"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['minutes_of_product'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.0f") : smarty_modifier_string_format($_tmp, "%.0f")); ?>
</div>
	                                	
	                                    <div class="title-services product-name" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_name"><?php echo $this->_tpl_vars['item']['name']; ?>
</div>
                                        <div class="title-services category-name" id="<?php echo $this->_tpl_vars['item']['id']; ?>
category_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
category_name" style="font-weight: 400; font-size: 16px;"><?php echo $this->_tpl_vars['item']['category_name']; ?>
</div>
	                                    <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
quantity" name="<?php echo $this->_tpl_vars['item']['id']; ?>
quantity"><?php echo $this->_tpl_vars['item']['quantity']; ?>
</div>
	                                    <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
category_id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
category_id"><?php echo $this->_tpl_vars['item']['category_id']; ?>
</div>
	                                    <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
category_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
category_name"><?php echo $this->_tpl_vars['item']['category_name']; ?>
</div>
	                                    <div class="description-services"  id="<?php echo $this->_tpl_vars['item']['id']; ?>
description" name="<?php echo $this->_tpl_vars['item']['id']; ?>
description">
	                                    		<?php echo $this->_tpl_vars['item']['description']; ?>

	                                   	</div>
	                                </div>
	                                <div class="col-md-4 col-xs-8 ta-right">
	                                    <div class="price-services" id="<?php echo $this->_tpl_vars['item']['id']; ?>
price">$<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['price'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</div>
	                                </div>
	                            </div>
	                            <div class="ta-right">
	                                <button type="button" class="btn btn-sm btn-primary" onclick="view_service('<?php echo $this->_tpl_vars['item']['id']; ?>
')">View</button>
	                                <button type="button" class="btn btn-sm btn-dark" onClick="edit_service('<?php echo $this->_tpl_vars['item']['id']; ?>
')">Edit</button>
                                    <button type="button" class="btn btn-sm btn-md btn-delete" onClick="delete_service('<?php echo $this->_tpl_vars['item']['id']; ?>
')">Delete</button>

	                            </div>
	                        </div>
	                    </li>
	                <?php endforeach; endif; unset($_from); ?>

            </ul>
             <div class="clear"></div>
            <div class="row">
                <div class="col-lg-12 pull-right">
                    <ul class="pagination"></ul>
                </div>
            </div>
            <div class="clear"></div>
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>


