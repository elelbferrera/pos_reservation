<?php /* Smarty version 2.6.26, created on 2017-05-11 07:33:50
         compiled from admin_newmerchant.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'admin_newmerchant.tpl', 28, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/admin.js"></script>
<section class="vbox">
<section class="scrollable">
	<section class="hbox stretch">
		

  	<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
		<section class="vbox">

			<header class="header bg-primary lter text-center clearfix">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-arrow-left"></i> </button>
                    <div class="btn-group hidden-nav-xs">
                       <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/merchant" class="btn btn-primary"> Back to list</a>
                    </div>
                </div>
            </header>


			<section>
<!-- 				<div class="newmerchant-btn">
					<a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/merchant" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to list</a>
				</div> -->
				
				<div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color: #262626; padding: 55px;">
					<div class="sidebar-waitingtitle ta-center" style="color: #ffffff;font-size: 25px;line-height: 25px;">Total Merchants Registered</div>
					<div class="sidebar-numbers ta-center" style="color: #ffffff;font-size: 60px;">
					<?php echo count($this->_tpl_vars['data']); ?>

					</div>
				</div>

			</section>
		</section>
	</aside>
		
 <!-- .content -->
	<section id="content">  	
		<section class="vbox">


		      <header class="header bg-darkblue b-light">
                    <div class="row">
                         <div class="col-md-12 col-xs-12">
                            <div class="breadtitle-holder">
                                <div class="breadtitle">
                                    <i class="fa fa-user titleFA"></i> 
                                    <p class="headeerpage-title">New Merchant</p>
                                </div>
                            </div>
                         </div>

                    </div>
                </header>


<!-- 
			<div class="admin-pageheader">
				<div class="row">
					<div class="col-xs-6">
						<h2><i class="fa fa-user"></i> New Merchant</h2>
					</div>
					<div class="col-xs-6 ta-right">
						<div>
							<button type="submit" id="btnNewMerchant" class="btn btn-primary">Save Information</button>
							<a type="button" class="btn btn-md btn-dark" href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/merchant">Cancel</a>    
						</div>
					</div>
				</div>
			</div> -->
			
			<div class="scrollable merchantForm-wrap">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-info" data-toggle="tab">Information</a></li>
						<li><a href="#tab-address" data-toggle="tab">Address</a></li>
						<li><a href="#tab-contact" data-toggle="tab">Contact</a></li>
						<li><a href="#tab-user" data-toggle="tab">User</a></li>
					</ul>
					<form name="frmNewMerchant" id="frmNewMerchant">
						<input type="hidden" name="loggeduser" id="mc-loggeduser" value="<?php echo $_SESSION['username']; ?>">
						<div class="tab-content">
							<div class="tab-pane active" id="tab-info">
								<div class="form-group">
									<label>Business Name *</label>
									<input type="text" name="businessname" id="mc-businessname" class="form-control required" placeholder="Business Name">
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label><input type="checkbox" name="status" id="mc-status" value="A" checked> Enable in Marketplace</label>
									</div>
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label><input type="checkbox" name="sms" id="mc-sms" value="1" checked> SMS Notification</label>
									</div>
								</div>
								<div class="form-group">
									<label>Timezone *</label>
									<select class="form-control required" name="timezone" id="mc-timezone">
										<?php $_from = $this->_tpl_vars['timezone']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timezone']):
?>
										<option value="<?php echo $this->_tpl_vars['timezone']['name']; ?>
"><?php echo $this->_tpl_vars['timezone']['description']; ?>
</option>
										<?php endforeach; endif; unset($_from); ?>
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" id="mc-description" class="form-control textarea-noresize" rows="6"></textarea>
								</div>
							</div>

							<div class="tab-pane" id="tab-address">
								<div class="form-group">
									<label>Address 1 *</label>
									<input type="text" name="address1" id="mc-address1" class="form-control required">
								</div>
								<div class="form-group">
									<label>Address 2</label>
									<input type="text" name="address2" id="mc-address2" class="form-control">
								</div>
								<div class="row">
									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>Country *</label>
											<select class="form-control" name="country" id="mc-country">
												<?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['country']):
?>
												<option data-id="<?php echo $this->_tpl_vars['country']['id']; ?>
" value="<?php echo $this->_tpl_vars['country']['name']; ?>
"><?php echo $this->_tpl_vars['country']['name']; ?>
</option>
												<?php endforeach; endif; unset($_from); ?>
											</select>
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>State *</label>
											<input type="hidden" name="state" id="state" value="" class="required">
											<select class="form-control" name="mc-state" id="mc-state" data-currentState="">
											</select>
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>City *</label>
											<input type="text" name="city" id="mc-city" class="form-control required" placeholder="City">
										</div>
									</div>

									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<label>Zip Code *</label>
											<input type="text" name="zip" id="mc-zip" class="form-control required" placeholder="Zip Code">
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab-contact">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Phone 1 *</label>
											<input type="text" name="phone1" id="mc-phone1" class="form-control required" placeholder="Phone 1">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Phone 2</label>
											<input type="text" name="phone2" id="mc-phone2" class="form-control" placeholder="Phone 2">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Fax</label>
											<input type="text" name="fax" id="mc-fax" class="form-control" placeholder="Fax">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Email</label>
											<input type="email" name="email" id="mc-email" class="form-control" placeholder="Email">
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab-user">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Username *</label>
											<input type="text" name="username" id="mc-username" class="form-control required" placeholder="Username">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Email *</label>
											<input type="email" name="usremail" id="mc-usremail" class="form-control required" placeholder="Email">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>First Name *</label>
											<input type="text" name="firstname" id="mc-firstname" class="form-control required" placeholder="First Name">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Last Name *</label>
											<input type="text" name="lastname" id="mc-lastname" class="form-control required" placeholder="Last Name">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Password *</label>
											<input type="password" name="password" id="mc-password" class="form-control required" placeholder="Password">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>

					<div class="row">
				<div class="col-md-12 ta-right">
						<button type="submit" id="btnNewMerchant" class="btn btn-primary">Save Information</button>
						<a type="button" class="btn btn-md btn-dark" href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/merchant">Cancel</a>    
				</div>
			</div>

			
			</div>
		
		</section>
	</section>
  


 	</section>
  </section>
</section>