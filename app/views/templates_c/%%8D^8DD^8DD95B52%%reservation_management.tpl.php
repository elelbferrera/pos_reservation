<?php /* Smarty version 2.6.26, created on 2017-05-20 11:52:22
         compiled from reservation_management.tpl */ ?>
<!-- <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.supercal.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/reservation.js"></script>	 -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->
<style type="text/css">
	<?php echo '
		.fc-view, .fc-view>table {
		    width: 1800px;
		}
		.highlight{
			background-color: #ddd;
		}
	'; ?>

</style>



<?php $_from = $this->_tpl_vars['reservations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
id"><?php echo $this->_tpl_vars['item']['id']; ?>
</div>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name"><?php echo $this->_tpl_vars['item']['last_name']; ?>
</div>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name"><?php echo $this->_tpl_vars['item']['first_name']; ?>
</div>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
mobile" name="<?php echo $this->_tpl_vars['item']['id']; ?>
mobile"><?php echo $this->_tpl_vars['item']['mobile']; ?>
</div>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_date" name="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_date"><?php echo $this->_tpl_vars['item']['reservation_date']; ?>
</div>
  	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_time" name="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_time"><?php echo $this->_tpl_vars['item']['reservation_time']; ?>
</div>

	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
sale_person_ids" name="<?php echo $this->_tpl_vars['item']['id']; ?>
sale_person_ids"><?php echo $this->_tpl_vars['item']['sale_person_ids']; ?>
</div>  
	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_ids" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_ids"><?php echo $this->_tpl_vars['item']['product_ids']; ?>
</div> 
	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
branch_id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
branch_id"><?php echo $this->_tpl_vars['item']['branch_id']; ?>
</div>
	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_status_id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
reservation_status_id"><?php echo $this->_tpl_vars['item']['reservation_status_id']; ?>
</div>
	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
notes" name="<?php echo $this->_tpl_vars['item']['id']; ?>
notes"><?php echo $this->_tpl_vars['item']['notes']; ?>
</div> 	
<?php endforeach; endif; unset($_from); ?>


<form id="frmReservation" name="frmReservation" style="display: none;">
	<input type="text" name="txtReservationId" id="txtReservationId"/>
	<input type="text" name="txtLastName" id="txtLastName"/>
	<input type="text" name="txtFirstName" id="txtFirstName"/>
	<input type="text" name="txtMobile" id="txtMobile"/>
	<input type="text" name="txtNotes" id="txtNotes"/>
	<input type="text" name="txtDate" id="txtDate"/>
	<input type="text" name="txtTime" id="txtTime"/>
	<input type="text" name="txtProducts" id="txtProducts"/>
	<input type="text" name="txtSpecialist" id="txtSpecialist"/>
	<input type="text" name="txtBranch" id="txtBranch"/>
	<input type="text" name="txtStatus" id="txtStatus"/>

	<input type="text" name="txtURL" id="txtURL" value="<?php echo $this->_tpl_vars['webroot']; ?>
/"/>
	<input type="text" name="txtEmail" id="txtEmail"/>
</form>
	
<input type="hidden" name="txtSelectedDate" id="txtSelectedDate" value ="<?php echo $this->_tpl_vars['date_selected']; ?>
"/>
<script>
var date_select = "<?php echo $this->_tpl_vars['date_selected']; ?>
";
<?php echo '
	(function(factory) {
	if(typeof define === \'function\' && define.amd) {
		define([\'jquery\'], factory);
	} else {
		factory(jQuery);
	}
}(function($) {
	var defaults = {
		todayButton: true,		// Show the button to reset to today\'s date?
		showInput: true,		// Show input
		weekStart: 0,			// Start day of the week. 0 is Sunday, 6 for Saturday, 1 for Monday (default)
		widget: true,
		cellRatio: 1,
		// format: \'d/m/y\',
		format: \'Y-m-d\',
		footer: false,
		dayHeader: true,
		mode: \'widget\',			// \'widget\' (default), \'tiny\', \'popup\', \'page\'
		animDuration: 200,
		transition: \'\',
		tableClasses: \'table table-condensed\',
		hidden: true,
		setOnMonthChange: true,
		condensed: false,
	};
	
	var now = new Date(date_select);
	// alert(date_select);


	var months = [\'January\', \'February\', \'March\', \'April\', \'May\', \'June\', \'July\', \'August\', \'September\', \'October\', \'November\', \'December\'];
	var shortMonths = [\'Jan\', \'Feb\', \'Mar\', \'Apr\', \'May\', \'Jun\', \'Jul\', \'Aug\', \'Sep\', \'Oct\', \'Nov\', \'Dec\'];
	var days = [\'Sunday\', \'Monday\', \'Tuesday\', \'Wednesday\', \'Thursday\', \'Friday\', \'Saturday\'];
	var shortDays = [\'Sun\', \'Mon\', \'Tue\', \'Wed\', \'Thu\', \'Fri\', \'Sat\'];

	// Get the number of days in the current month (or next month if delta == 1, or prev month if delta == -1, etc)
	Date.prototype.daysInMonth = function(delta) {
		delta = delta === undefined ? 0 : delta;

		return new Date(this.getFullYear(), this.getMonth() + 1 + delta, 0).getDate();
	}

	// Is this date today? Can also test against another date when specifying `now`
	Date.prototype.isDay = function(day) {
		if(day === undefined) {
			day = new Date();
		}

		return this.getFullYear() == day.getFullYear() 
			&& this.getMonth() == day.getMonth() 
			&& this.getDate() == day.getDate();
	}

	Date.prototype.isValid = function() {
		return Object.prototype.toString.call(this) === "[object Date]" && !isNaN(this.getTime());
	}

	$.fn.supercal = function(method) {
		// Private methods
		var pMethods = {
			// alert(date_select);
			drawCalendar: function(selectedDate, replace) {
				var options = $(this).data(\'options\');

				selectedDate = selectedDate || now;

				if(replace !== undefined && replace == true) {
					this.empty();
				}

				pMethods.drawHeader(selectedDate, options).appendTo(this);

				var month = pMethods
					.drawMonth(selectedDate, options)
					.addClass(\'current\');

				$(\'<div />\')
					.addClass(\'supercal-month\')
					.html(month)
					.appendTo(this);

				pMethods.drawFooter(selectedDate, options).appendTo(this);

				$(this).data(\'supercal\', true);
				$(this).data(\'date\', selectedDate);
				$(this).data(\'element\', this);

				return this;
			},
			drawPopupCalendar: function(selectedDate, replace) {
				var options = $(this).data(\'options\');

				selectedDate = selectedDate || now;

				var container, calendar;

				if($(this).parent(\'.supercal-popup-wrapper\').length) {
					container = $(this).parent();
					calendar = $(this).parent().find(\'.supercal-popup\');
					calendar.empty();
				} else {
					container = $(\'<div />\').addClass(\'supercal-popup-wrapper\');
					calendar = $(\'<div />\')
						.addClass(\'supercal supercal-popup\')
						.width($(this).outerWidth(true));

					$(this).wrap(container);
				}

				$(this).after(calendar);

				if(options.hidden) {
					calendar.hide();
				}

				pMethods.drawHeader(selectedDate, options).appendTo(calendar);

				var month = pMethods
					.drawMonth(selectedDate, options)
					.addClass(\'current\');

				$(\'<div />\')
					.addClass(\'supercal-month\')
					.html(month)
					.appendTo(calendar);

				calendar.data(\'supercal\', true);
				calendar.data(\'date\', selectedDate);
				calendar.data(\'element\', this);
				calendar.data(\'options\', options);

				$(this).parent().wrap($(\'<div />\').addClass(\'supercal-affix\'));
				
				return this;
			},
			drawHeader: function(date, options) {
				var header = $(\'<div />\').addClass(\'supercal-header\');
				var monthNames = options.shortMonths ? shortMonths : months;

				$(\'<button />\')
					.addClass(\'prev-month change-month btn\')
					.html(\'&laquo;\')
					.appendTo(header);

				$(\'<button />\')
					.addClass(\'next-month change-month btn\')
					.html(\'&raquo;\')
					.appendTo(header);

				$(\'<span />\')
					.addClass(\'month\')
					.append(\'<div>\' + monthNames[date.getMonth()] + \' \' + date.getFullYear() + \'</div>\')
					.appendTo(header);

				return header;
			},
			drawMonth: function(date, options) {
				date = date || now;
				// date = new(date_select);
				localTime = date.getTime();
				localOffset = date.getTimezoneOffset() * 60000;

				utc = localTime + localOffset;
				offset = 5.5;  
				newdate = utc + (3600000*offset);
				date = new Date(newdate); 

				var monthStart = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0);
				var days = [];
				var rows = [];
				var table = $(\'<table />\').addClass(options.tableClasses);

				var numPrevDays = monthStart.getDay() - options.weekStart;
				var numCurrentDays = date.daysInMonth();
				var numNextDays = 42 - numPrevDays - numCurrentDays;

				var daysInLastMonth = date.daysInMonth(-1);

				// Header
				if(options.dayHeader) {
					var tableHeader = $(\'<tr />\');

					for(var i = 0; i <= 6; i++) {
						var day = i + options.weekStart;

						if(day > 6) {
							day = i - 6;
						}

						$(\'<th />\')
							.text(shortDays[day])
							.appendTo(tableHeader);
					}

					table.append(tableHeader);
				}

				// Add previous month\'s days
				for(var i = 1; i <= numPrevDays; i++) {
					var day = (daysInLastMonth - numPrevDays) + i;

					days.push({
						date: new Date(date.getFullYear(), date.getMonth() - 1, day, 0, 0, 0),
						displayNumber: day,
						classes: \'month-prev\'
					});
				}

				// Add current month\'s days
				for(var i = 1; i <= numCurrentDays; i++) {
					var day = {
						date: new Date(date.getFullYear(), date.getMonth(), i, 0, 0, 0),
						displayNumber: i,
						classes: \'\'
					};

					// If this date is today\'s date, add the `today` class
					day.classes = day.date.isDay() ? \'today\' : \'\';

					// If this date is the one selected, add the `selected` class
					day.classes += day.date.isDay(date) ? \' selected\' : \'\';

					days.push(day);		// Add date to array
				}

				// Add next month\'s days
				for(var i = 1; i <= numNextDays; i++) {
					days.push({
						date: new Date(date.getFullYear(), date.getMonth() + 1, i, 0, 0, 0),
						displayNumber: i,
						classes: \'month-next\'
					});
				}

				// Slice array into 7s
				rows = [
					days.slice(0, 7),
					days.slice(7, 14),
					days.slice(14, 21),
					days.slice(21, 28),
					days.slice(28, 35),
					days.slice(35)
				];

				// Append arrays to table
				for(var row = 0; row < 6; row++) {
					var tr = $(\'<tr />\');

					for(var col = 0; col < 7; col++) {
						var cell = rows[row][col];

						$(\'<td />\')
							.data(\'date\', cell.date)
							.text(cell.displayNumber)
							.addClass(cell.classes)
							.appendTo(tr);
					}

					tr.appendTo(table);
				}

				return table;
			},
			drawFooter: function(date, options) {
				var formattedDateFooter = new Date(date_select);

			 	var d = formattedDateFooter.getUTCDate();
			 	if(d <= 9)
			 	{
			 		d = "0" + d;
			 	}
			 	var m = formattedDateFooter.getUTCMonth() + 1;

			 	if (m <= 9) {
			 		m = "0" + m;
			 	}
			 	var y = formattedDateFooter.getFullYear();

				var footer = $(\'<div />\').addClass(\'supercal-footer input-prepend\');

				if(options.footer == false) {
					/*footer.hide();*/
				}

				if(options.todayButton) {
					$(\'<button />\')
						.text(\'Today\')
						.addClass(\'btn supercal-today btn btn-sm btn-primary todaybtn\')
						.attr(\'type\', \'button\')
						.appendTo(footer);
				}

				if(options.showInput) {
					$(\'<span />\')
						/*.text(pMethods.formatDate(date, options))*/
						.text(m+"-"+d+"-"+y)
						.addClass(\'supercal-input uneditable-input span2\')
						.attr(\'id\', \'dateFieldReservation\')
						.appendTo(footer);
						
					$(\'<input />\')
					.prop(\'type\', \'hidden\')
					.val(pMethods.formatDate(date, options))
					.attr(\'id\', \'txtDateFieldReservation\')
					.attr(\'name\', \'txtDateFieldReservation\')
					.appendTo(footer);
					
					
					
				}

				$(\'<input />\')
					.prop(\'type\', \'hidden\')
					.val(parseInt(date.getTime() / 1000), 10)
					.appendTo(footer);

				return footer;
			},
			// Split out into helper object
			formatDate: function(date, options) {		// Verrrry primitive date format function. Does what it needs to do...
				return options.format
					.replace(\'d\', (\'0\' + date.getDate()).substr(-2))
					.replace(\'m\', (\'0\' + (date.getMonth() + 1)).substr(-2))
					.replace(\'y\', date.getFullYear().toString().substr(-2))
					.replace(\'Y\', date.getFullYear());
			}
		};

		var methods = {
			init: function(settings) {
				var options = $.extend({}, defaults, settings);

				// Events
				if(!$(document).data(\'supercal-events\')) {
					$(document).on(\'click.supercal\', \'.supercal .change-month\', function(e) {
							e.preventDefault();
							e.stopPropagation();
							// alert(\'month\');
							methods.changeMonth.apply($(this).closest(\'.supercal\'), [ $(this).hasClass(\'next-month\') ? 1 : -1, $(this).closest(\'.supercal\').data(\'options\') ]);
						})
						.on(\'click.supercal\', \'.supercal-today\', function() {
							// alert(\'today\');
							methods.changeMonth.apply($(this).closest(\'.supercal\'), [ now, $(this).closest(\'.supercal\').data(\'options\') ]);
						})
						.on(\'click.supercal\', \'.supercal table.current td\', function() {
							var container = $(this).closest(\'.supercal\');
							var table = $(this).closest(\'table\');

							table.find(\'.selected\').removeClass(\'selected\');

							$(this).addClass(\'selected\');
							// alert($(this).data(\'date\'));

							container.find(\'.supercal-footer\').replaceWith(pMethods.drawFooter($(this).data(\'date\'), $(this).closest(\'.supercal\').data(\'options\')));
							container.data(\'date\', $(this).data(\'date\'));
						})
						// Popups
						.on(\'click.supercal\', \'.supercal-popup-trigger\', function(e) {
							$(this).parent(\'.supercal-popup-wrapper\').addClass(\'supercal-open\').find(\'.supercal-popup\').show();

							// Close other calendars
							$(\'.supercal-popup-wrapper.supercal-open\').not($(this).parent()).removeClass(\'supercal-open\').find(\'.supercal-popup\').hide();
						})
						.on(\'click.supercal\', function(e) {
							var target = $(e.target);

							if(!target.closest(\'.supercal-popup-wrapper\').length) {
								$(\'.supercal-popup-wrapper.supercal-open\').removeClass(\'supercal-open\').find(\'.supercal-popup\').hide();
							}
							// alert(\'lems\');
						})
						.on(\'click.supercal\', \'.supercal td\', function() {
							var thisDate = $(this).data(\'date\');
							var originalElement = $(this).closest(\'.supercal\').data(\'element\');
							var formattedDate = pMethods.formatDate($(this).data(\'date\'), $(this).closest(\'.supercal\').data(\'options\'));

							$(originalElement).trigger(\'dateselect\', [ thisDate ]);
							
							// Set date on input element if it exists
							$(originalElement).children(\'.supercal-popup-trigger\').val(formattedDate).trigger(\'change\');

							// showloading();
							// window.location.href = $(\'#txtURL\').val() +"reservation/management/"+formattedDate;

							showloading();
							$(\'#calendar\').fullCalendar( \'gotoDate\', formattedDate );
							$.postJSON("?action=get_refresh_events&date="+formattedDate, "", function(data) {
									$(\'#calendar\').fullCalendar(\'option\', \'minTime\', data.time.start_time);
									$(\'#calendar\').fullCalendar(\'option\', \'maxTime\', data.time.end_time);
									$(\'.fc-today-button\').remove();
									$(\'.fc-button-group\').remove();
									$("#calendar").fullCalendar(\'removeEvents\');
									$("#calendar").fullCalendar(\'addEventSource\', data.message);
									$("#calendar").fullCalendar(\'rerenderEvents\');

									for (var key in data.specialist_id) {
										$("#calendar").fullCalendar(\'removeResource\', data.specialist_id[key]);
									}

									for (var key in data.resources_info) {
										$(\'#calendar\').fullCalendar(\'addResource\', data.resources_info[key]);
									}

					            closeloading();
					            $("#txtSelectedDate").val(formattedDate);

					             //---
					            var date = new Date(thisDate);
					            localTime = date.getTime();
								localOffset = date.getTimezoneOffset() * 60000;

								utc = localTime + localOffset;
								offset = 5.5;  
								newdate = utc + (3600000*offset);
								formattedDate = new Date(newdate); 
					            
					            var body_height = $(\'.fc-time-grid\').height();

							 	$(\'[data-encode]\').each(function(key, value){
							 		//console.log(value);
									var elementNum = key + 2;
									//console.log(elementNum);
									//$(\'.fc-content-skeleton tr td:nth-child(\'+elementNum+\')\').addClass(\'fc-disabled\');
									var sched = jQuery.parseJSON($(this).attr(\'data-encode\'));
									var day = formattedDate.getDay();
									var day_word = "";
									var is_off = "";
									
									if (day == 0) {
										is_off = sched[6].is_off;
										day_word = sched[6].day;

									} else if (day > 0) {
										is_off = sched[day-1].is_off;
										day_word = sched[day-1].day;
									}
									
									if (is_off == 1) {
										$(\'.fc-content-skeleton tr td:nth-child(\'+elementNum+\')\').addClass(\'fc-disabled\').append(\'&nbsp;\').css(\'height\', body_height);
									}
								});
					        });
						});

					$(document).data(\'supercal-events\', true);
				}

				return this.each(function() {
					if($(this).is(\':input\')) {
						var element = $(\'<div />\');

						$(this).addClass(\'supercal-target\').after(element);
					} else {
						var element = this;
					}

					$(element).addClass(\'supercal \' + options.transition);
					$(element).data(\'options\', options);

					if(options.transition) {
						$(element).addClass(\'transition\');
					}

					if(options.condensed) {
						$(element).addClass(\'condensed\');
					}

					switch(options.mode) {
						case \'popup\':
							$(element).addClass(\'supercal-popup-trigger\');

							pMethods.drawPopupCalendar.apply(element, options.date);
						break;
						case \'widget\':
						default:
							pMethods.drawCalendar.call(element, options.date);
					}
				});
			},
			changeMonth: function(month, options) {
				var newDay, newDate, direction, newCalendar;

				var container = $(this);
				var calendar = $(this).find(\'table\');

				var currentDate = container.data(\'date\');
				var calWidth = calendar.outerWidth(true);
				var calHeight = calendar.outerHeight(true);

				calendar.parent().height(calHeight);		// Set height of calendar\'s container to height of table

				if(typeof month === \'number\') {
					direction = month > 0 ? 1 : -1;
					newDay = Math.min(currentDate.daysInMonth(month), currentDate.getDate());		// 31st of March clamped to 28th Feb, for example
					newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + direction, newDay);
				} else if(month instanceof Date) {
					direction = (month > currentDate) ? 1 : -1;
					newDate = now;
					// alert(\'month\');
				}

				calendar.stop(true, true);
				container.data(\'date\', newDate);
				newCalendar = pMethods.drawMonth(newDate, options).addClass(\'current\');

				switch(options.transition) {
					case \'fade\':
						calendar.fadeOut(options.animDuration, function() {
							$(this).replaceWith(newCalendar.hide().fadeIn(options.animDuration));
						});
					break;
					case \'crossfade\':		// Crossfade
						calendar.removeClass(\'current\').after(newCalendar);
						calendar.animate({ opacity: 0 }, options.animDuration);

						newCalendar.css({ opacity: 0, position: \'absolute\', top: 0 }).animate({ opacity: 1 }, options.animDuration);
					break;
					case \'carousel-horizontal\':
						calendar.css({ position: \'absolute\' }).animate({ left: -(calWidth * direction) }).after(newCalendar);

						newCalendar.css({ left: direction * calWidth, position: \'absolute\' }).animate({ left: 0 });
					break;
					case \'carousel-vertical\':		// Vertical slide
						calendar.css({ position: \'absolute\' }).animate({ top: -(calHeight * direction) }).after(newCalendar);

						newCalendar.css({ top: direction * calHeight, position: \'absolute\' }).animate({ top: 0 });
					break;
					default:		// No transition - default
						calendar.replaceWith(newCalendar);
					break;
				}

				// Remove old calendar
				newCalendar.promise().done(function() {
					container.find(\'table\').not(newCalendar).remove();
				});

				// Update header and footer
				container.find(\'.supercal-header\').replaceWith(pMethods.drawHeader(newDate, options));
				container.find(\'.supercal-footer\').replaceWith(pMethods.drawFooter(newDate, options));

				// Set date on input element if it exists
				if(options.setOnMonthChange) {
					container.prev(\'.supercal-target\').val($(this).data(\'date\')).trigger(\'change\');
				}

				var thisDate = $(this).data(\'date\');
				var originalElement = $(this).closest(\'.supercal\').data(\'element\');
				var formattedDate = pMethods.formatDate($(this).data(\'date\'), $(this).closest(\'.supercal\').data(\'options\'));

				$(originalElement).trigger(\'dateselect\', [ thisDate ]);
				
				// Set date on input element if it exists
				$(originalElement).children(\'.supercal-popup-trigger\').val(formattedDate).trigger(\'change\');

				// showloading();
				// window.location.href = $(\'#txtURL\').val() +"reservation/management/"+formattedDate;
				 var date = new Date(newDate);
				 // alert(date);
				localTime = date.getTime();
				localOffset = date.getTimezoneOffset() * 60000;

				utc = localTime + localOffset;
				offset = 5.5;  
				newdate = utc + (3600000*offset);
				formattedDate = new Date(newdate); 

				// alert(formattedDate);
				// return false;
				var formattedDate = pMethods.formatDate(formattedDate, $(this).closest(\'.supercal\').data(\'options\'));
				//alert(today);
				showloading();
				$(\'#calendar\').fullCalendar( \'gotoDate\', formattedDate );
				$.postJSON("?action=get_refresh_events&date="+formattedDate, "", function(data) {
						$("#calendar").fullCalendar(\'removeEvents\');
						$("#calendar").fullCalendar(\'addEventSource\', data.message);
						$("#calendar").fullCalendar(\'rerenderEvents\');
		            closeloading();
		            $("#txtSelectedDate").val(formattedDate);
		        });

				// alert(\'month\');
			},
			date: function() {		// Return current selected date
				if($(this).next(\'.supercal\').length) {
					
					return $(this).next(\'.supercal\').data(\'date\');
					
				} else if($(this).data(\'supercal\')) {
					
					return $(this).data(\'date\');
				}
				// alert(\'lems\');

				return false;
			}
		};

		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if(typeof method === \'object\' || !method) {
			return methods.init.apply(this, arguments);
		}
	};
}));
'; ?>
	 
</script>


<script>
var datefilter = "<?php echo $this->_tpl_vars['date_selected']; ?>
";
var start_time = "<?php echo $this->_tpl_vars['business_start_time']; ?>
";
var end_time = "<?php echo $this->_tpl_vars['business_end_time']; ?>
";

/*console.log(start_time);*/

var screen_ht = $(window).height();
var calendar_ht = screen_ht - 150;
// alert(datefilter);
// alert(end_time);
<?php echo '

$(function() { // document ready
	
	// alert(datefilter);
	$(\'.example1\').supercal({
         // transition: \'carousel-horizontal\',
         // format: \'Y-m-d\',
         // defaultDate: datefilter,
   	});
	
	 var date = new Date(datefilter);

	localTime = date.getTime();
	localOffset = date.getTimezoneOffset() * 60000;

	utc = localTime + localOffset;
	offset = 5.5;  
	newdate = utc + (3600000*offset);
	date = new Date(newdate); 

	// alert(date);

	var d = date.getDate(),
    m = date.getMonth(),
    y = date.getFullYear();

	$(\'#calendar\').fullCalendar({
      schedulerLicenseKey: \'GPL-My-Project-Is-Open-Source\',
      height: calendar_ht,
      // minTime: "06:00:00",
      minTime: start_time,
	  // maxTime: "23:00:00",
	  maxTime: end_time,
      defaultView: \'agendaDay\',
      slotDuration: \'00:05:00\',
      // defaultDate: new Date(y, m, d),
      allDaySlot: false,
      defaultDate:  datefilter,
      editable: true,
      selectable: true,
      eventLimit: true, // allow "more" link when too many events
      dragScroll: true,
      eventOverlap: true,
      // header: {
        // left: \'prev,next\',
        // center: \'title\',
        // right: \'agendaDay,agendaWeek,month\'
      // },
      views: {
        agendaTwoDay: {
          type: \'agenda\',
          duration: { days: 2 },

          // views that are more than a day will NOT do this behavior by default
          // so, we need to explicitly enable it
          groupByResource: true

          //// uncomment this line to group by day FIRST with resources underneath
          //groupByDateAndResource: true
        }
      },
	 viewRender: function(view, element){
	 	// alert(view.start);
	 	var date = new Date(view.start);

		localTime = date.getTime();
		localOffset = date.getTimezoneOffset() * 60000;

		utc = localTime + localOffset;
		offset = 5.5;  
		newdate = utc + (3600000*offset);
		date = new Date(newdate); 


	 	var d = date.getDate();
	 	var m = date.getMonth();
	 	var y = date.getFullYear();
	 	
	 	//alert(formatDate(view.start, \'yyyy-MM-dd\'));
	 },
      //// uncomment this line to hide the all-day slot
      //allDaySlot: false,
      
	'; ?>

		
      <?php echo $this->_tpl_vars['resources_info']; ?>

      <?php echo $this->_tpl_vars['events']; ?>

      //<?php echo $this->_tpl_vars['businessHours']; ?>

    <?php echo '
    
      
      eventDrop: function(event, delta, revertFunc) {

        //alert(event.title + " was dropped on " + event.start.format() +" "+ event.resourceId);


		var date = event.start.format();
		date = date.split("T");

        if (!confirm("Are you sure to want change this schedule?")) {
            revertFunc();
        }else{
        	// alert(event.id)
        	//editfromDrop(event.id, date[0], date[1], event.resourceId);
        	editfromDrop(event.id, date[0], date[1], event.resourceId, event);


        	// var i = update_me();
        	// alert(i);
        	// alert(update_me());
        	// return false;
        	if(update_me()==1)
        	{
	    		// var url = window.location.href;
	            // url = url.replace("#",""); 
	            // window.location.href = window.location.href;
        	}else{
        		revertFunc();
        	}
        }

      },
	  editable: true,
      droppable: true,
      select: function(start, end, jsEvent, view, resource) {
      	// alert(resource.id);
      	
      	// add(start, resource.id);
        // console.log(
          // \'select\',
          // start.format(),
          // end.format(),
          // resource ? resource.id : \'(no resource)\'
        // );
      },
      dayClick: function(date, jsEvent, view, resource) {
      	console.log(date);
      	var datetime = new Date(date);

      	var date = new Date(date);

		localTime = date.getTime();
		localOffset = date.getTimezoneOffset() * 60000;

		utc = localTime + localOffset;
		offset = 5.5;  
		newdate = utc + (3600000*offset);
		date = new Date(newdate); 
		console.log(date);
		
		// var moment = $(\'#calendar\').fullCalendar(\'getTime\');

		// alert(moment);
		// die();

		// return false;

      	//add(date, resource.id); //remove
      	var d = date.getDay();

      	if (resource.id !== \'-1\') {

	      	switch (d) {
		 		case (1): {
		 			var sched = resource.MONDAY;
		 			break;
		 		} 
		 		case (2): {
		 			var sched = resource.TUESDAY;
		 			break;
		 		}
		 		case (3): {
		 			var sched = resource.WEDNESDAY;
		 			break;
		 		}
		 		case (4): {
		 			var sched = resource.THURSDAY;
		 			break;
		 		}
		 		case (5): {
		 			var sched = resource.FRIDAY;
		 			break;
		 		}
		 		case (6): {
		 			var sched = resource.SATURDAY;
		 			break;
		 		}
		 		case (0): {
		 			var sched = resource.SUNDAY;
		 			break;
		 		}
		 		default: break;
		 	}
		} else {
			sched = "";
		}
      	add(date, resource.id, datetime, resource.title, sched); //remove
      	
      	// alert(\'lems\');
        // console.log(
          // \'dayClick\',
          // date.format(),
          // resource ? resource.id : \'(no resource)\'
        // );
      },
      eventClick: function(event, delta, resource){
      	edit(event.id, event);
      },
      
    });

    $(".fc-right").hide(); 
 });
'; ?>


</script>


<section class="vbox">
<section class="scrollable">
	<section class="hbox stretch">
		

  			<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
  		<!-- <br /> -->
                    <section class="vbox">
<!--                         <header class="header bg-primary lter text-center clearfix">
                            <div class="btn-group" onclick="add()">
                                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                <div class="btn-group hidden-nav-xs">
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#makereservation1"> Add New Reservation </button>
                                </div>
                            </div>
                        </header> -->
		                        <section>
		                        	<div style="margin-top: 4px;margin-bottom: 4px;">
				                        <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/search/customer"><button id="findApptBtn" type="button" class="btn btn-primary" style="width: 90%;">Find appointments</button></a>
				                    	</div>
		                            <div class="example1" style="margin:0 auto"></div>
		                            <script></script>
		                            <div class="calendar-week bg-dark lter">
		                            	
		                            	<div style="margin-left: 5px !important;">
		                            		<span class="supercal-input uneditable-input span2">Skip by:</span>
			                            	<button type="button" class="btn btn-primary" onclick="setWeek(1)">+1</button>
			                            	<button type="button" class="btn btn-primary" onclick="setWeek(2)">+2</button>
			                            	<button type="button" class="btn btn-primary" onclick="setWeek(3)">+3</button>
			                            	<button type="button" class="btn btn-primary" onclick="setWeek(4)">+4</button>
			                            	<span class="supercal-input uneditable-input span2">Week/s</span>
			                            </div>
			                            
		                            </div>
		                            <!-- <button id="btnrefresh" name="btnrefresh" type="button" class="btn btn-md btn-primary" style="width: 100%;">Refresh</button> -->

			                            <div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color:#bb2e2e;">
			                            	  <div class="sidebar-waitingtitle ta-center" style="color:#FFF;">Total Waiting</div>
			                                <div id="divTotalWaiting" name="divTotalWaiting" class="sidebar-numbers ta-center" style="color:#FFF;"><?php echo $this->_tpl_vars['waiting_count']; ?>
</div>
			                            </div>
		                        </section>
		                        <!--  <footer class="footer lt hidden-xs b-t b-light">
                           
			                            <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-default btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
			                          
                 				</footer> -->
  
                    </section>
                </aside>
		
				 <!-- .content -->
				  <section id="content">  	
				        <section class="vbox">
				            <!-- <header class="header bg-darkblue b-light">
				                <div class="row">
				                     <div class="col-md-6 col-xs-3">
				                        <div class="breadtitle-holder">
				                            <div class="breadtitle">
				                                <i class="fa fa-calendar titleFA"></i> 
				                                <p class="headeerpage-title">Reservation</p>
				                            </div>
				                        </div>
				                     </div>
				                     <div class="col-md-6 col-xs-9 ta-right">
				                        <div class="breadtitle-holder2">
				                            
				                             <div class="btn-group" onclick="add()">
				                                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
				                                    <div class="btn-group hidden-nav-xs">
				                                        <button type="button" class="btn btn-sm btn-primary">Create Reservation</button>
				                                  </div>
				                             </div>
				                        </div>
				                     </div>
				                </div>
				            </header> -->
				            
				                 
				            
				            
								        	<section class="scrollable-fullcalendar wrapper">
								           <!--  <section class="wrapper"> -->
								                    <div id="calendar"></div>
								            </section>
				        </section>
				  </section>
				  


 	</section>
  </section>
</section>

<!-- Modal -->
<div id="dlgEditReservationStep2" name="dlgEditReservationStep2" class="modal fade" role="dialog" >
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reservation</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->

      </div>

      <!-- Modal content-->	
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Select Services</span>
                      <!-- <p>Search Service Name: <input type="text" ng-model="services"></p> -->
                  </div>
              </div>
              
              <!-- <div id="divServices" name="divServices">
		          
	          </div> -->
              

      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#makereservation3" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <!-- <button type="button" class="btn btn-primary" onclick="GoToFinalPage()">Next</button> -->
            <!-- <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button> -->
            <button type="button" class="btn btn-dark" onclick="GoToFirstPage()">Previous</button>
            <!-- <a data-toggle="modal" href="#makereservation1" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
      </div>

      
  </div>
</div>



<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/reservation.js"></script>	