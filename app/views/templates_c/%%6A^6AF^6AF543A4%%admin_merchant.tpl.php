<?php /* Smarty version 2.6.26, created on 2017-05-11 05:25:43
         compiled from admin_merchant.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'admin_merchant.tpl', 29, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/admin.js"></script>	

<section class="vbox">
<section>
	<section class="hbox stretch">
		

  	<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
		<section class="vbox">

			 <header class="header bg-primary lter text-center clearfix">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                    <div class="btn-group hidden-nav-xs">
                         <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/newmerchant"><button id="findApptBtn" type="button" class="btn btn-primary" style="width: 90%;">Add New</button></a>
                    </div>
                </div>
            </header>


			<section>
			<!-- 	<div class="newmerchant-btn">
					<a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/newmerchant" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a>
				</div> -->
				
				<div class="sidebar-module" style="border-bottom: 0px solid #495f74; background-color: #262626; padding: 55px;">
					<div class="sidebar-waitingtitle ta-center" style="color: #ffffff;font-size: 25px;line-height: 25px;">Total Merchants Registered</div>
					<div class="sidebar-numbers ta-center" style="color: #ffffff;font-size: 60px;">
					<?php echo count($this->_tpl_vars['data']); ?>

					</div>
				</div>

			</section>
		</section>
	</aside>
		
 <!-- .content -->
	<section id="content">  	
		<section class="vbox">
			<div class="merchantList-wrap">
				<table id="merchant-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th hidden>ID</th>
							<th>Business Name</th>
							<th>Date Registered</th>
							<th class="no-sort">Marketplace Status</th>
							<th class="no-sort"></th>
						</tr>
					</thead>
					<tbody>
						<?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['merchant']):
?>
						<tr>
							<td hidden><?php echo $this->_tpl_vars['merchant']['id']; ?>
</td>
							<td><?php echo $this->_tpl_vars['merchant']['business_name']; ?>
</td>
							<td><?php echo $this->_tpl_vars['merchant']['create_date']; ?>
</td>
							<td><?php if ($this->_tpl_vars['merchant']['status'] == 'A'): ?><span class="merchant-status-enabled">Enabled</span><?php else: ?><span class="merchant-status-disabled">Disabled</span><?php endif; ?></td>
							<td class="ta-right"><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/editmerchant?merchantid=<?php echo $this->_tpl_vars['merchant']['id']; ?>
" class="merchant-edit btn btn-sm btn-default"><i class="fa fa-pencil"></i> EDIT</a></td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
					</tbody>
				</table>
			</div>
		</section>
	</section>
  


 	</section>
  </section>
</section>


<?php echo '
<script>
	$(document).ready(function() {
		$(\'#merchant-list\').DataTable({
			"searching": false,
			"iDisplayLength": 5,
			"bLengthChange": false,
			"aoColumnDefs": [ {
				\'bSortable\': false,
				\'aTargets\': [ "no-sort" ] 
			} ]
		});
	});
</script>
'; ?>