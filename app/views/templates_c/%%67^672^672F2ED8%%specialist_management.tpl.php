<?php /* Smarty version 2.6.26, created on 2017-05-11 20:21:59
         compiled from specialist_management.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'specialist_management.tpl', 105, false),array('modifier', 'replace', 'specialist_management.tpl', 158, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/specialist.js"></script>
<style type="text/css">
  <?php echo '
    .datepicker{
      z-index: 10000 !important;
    }
  '; ?>

</style>
<input type="hidden" value="<?php echo $this->_tpl_vars['webroot']; ?>
" name="txtURL" id="txtURL">
                 <!-- .content -->
                   <section id="content">
                        <section class="vbox" id="paginateDiv">
                            <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-3 col-xs-3">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-users titleFA"></i> 
                                                <p class="headeerpage-title">Specialist</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-6 col-xs-6 ta-right">
                                        <div class="breadtitle-holder2">
                                            <!-- <div class="btn-group" style="margin-right:10px;">
                                                      <select class="form-control" name="repeatReservation">
                                                      <option selected="selected" value="">- sort by branch -</option>
                                                      <option>Bulacan Branch</option>
                                                      <option>Ortigas</option>
                                                      <option>Cubao</option>
                                                      <option>US</option>
                                                  </select>
                                            </div> -->
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary" onclick="add()">New Specialist</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                     <div class="col-md-3 col-xs-3 ta-right">
                                        <div class="breadtitle-holder3">
                                            <input type="text" class="search search-paginate" placeholder="Search specialist name" />
                                        </div>
                                     </div>
                                </div>
                            </header>
                            <section class="scrollable wrapper">
                                 <!-- <div id="example" class="overview"> -->
                                        
                                        <!-- <div class="viewport">
                                            <ul class="overview"> -->
                                            <ul id="example" class="overview list paginated-list">
                                            <?php $_from = $this->_tpl_vars['specialist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                            
                                                <li class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="row">
                                                        <div class="specialist-header-list">Specialist ID #<?php echo $this->_tpl_vars['item']['id']; ?>
</div>
                                                        <div class="specialist-body-list">
                                                            <div class="row">
                                                                <div class="col-md-4  col-sm-4 col-xs-4">
                                                                	  <?php if ($this->_tpl_vars['item']['profile_picture'] == ''): ?>
                                                                	  	<img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/avatar_default.jpg" class="specialist-avatar" />
                                                                	  <?php else: ?>
                                                                	  	<img src="<?php echo $this->_tpl_vars['item']['profile_picture']; ?>
" class="specialist-avatar" />
                                                                	  <?php endif; ?>	
                                                                      <div class="clear"></div>
                                                                      <!--    <div class="ta-center" style="margin-top:10px;">
                                                                             <div class="price-services">14</div>
                                                                             <p>Total Service today</p>
                                                                         </div> -->
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
id"><?php echo $this->_tpl_vars['item']['id']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name"><?php echo $this->_tpl_vars['item']['last_name']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
branch_id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
branch_id"><?php echo $this->_tpl_vars['item']['branch_id']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name"><?php echo $this->_tpl_vars['item']['first_name']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
middle_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
middle_name"><?php echo $this->_tpl_vars['item']['middle_name']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
birth_date" name="<?php echo $this->_tpl_vars['item']['id']; ?>
birth_date"><?php echo $this->_tpl_vars['item']['birth_date']; ?>
</div>
                                                                         <?php if ($this->_tpl_vars['item']['profile_picture'] == ''): ?>
                                                                         	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
profile_picture" name="<?php echo $this->_tpl_vars['item']['id']; ?>
profile_picture"><?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/avatar_default.jpg</div>
                                                                         <?php else: ?>
                                                                         	<div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
profile_picture" name="<?php echo $this->_tpl_vars['item']['id']; ?>
profile_picture"><?php echo $this->_tpl_vars['item']['profile_picture']; ?>
</div>
                                                                         <?php endif; ?>
                                                                         
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
gender" name="<?php echo $this->_tpl_vars['item']['id']; ?>
gender"><?php echo $this->_tpl_vars['item']['gender']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
city" name="<?php echo $this->_tpl_vars['item']['id']; ?>
city"><?php echo $this->_tpl_vars['item']['city']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
address" name="<?php echo $this->_tpl_vars['item']['id']; ?>
address"><?php echo $this->_tpl_vars['item']['address']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
state" name="<?php echo $this->_tpl_vars['item']['id']; ?>
state"><?php echo $this->_tpl_vars['item']['state']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
country" name="<?php echo $this->_tpl_vars['item']['id']; ?>
country"><?php echo $this->_tpl_vars['item']['country']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
schedule" name="<?php echo $this->_tpl_vars['item']['id']; ?>
schedule"><?php echo $this->_tpl_vars['item']['schedule']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
start_time" name="<?php echo $this->_tpl_vars['item']['id']; ?>
start_time"><?php echo $this->_tpl_vars['item']['start_time']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
end_time" name="<?php echo $this->_tpl_vars['item']['id']; ?>
end_time"><?php echo $this->_tpl_vars['item']['end_time']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
start_date" name="<?php echo $this->_tpl_vars['item']['id']; ?>
start_date"><?php echo $this->_tpl_vars['item']['start_date']; ?>
</div>
                                                                         <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
end_date" name="<?php echo $this->_tpl_vars['item']['id']; ?>
end_date"><?php echo $this->_tpl_vars['item']['end_date']; ?>
</div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-8 ta-right">
                                                                    <div class="specialist-name"><?php echo $this->_tpl_vars['item']['first_name']; ?>
 <?php echo $this->_tpl_vars['item']['last_name']; ?>
<br />
                                                                          
                                                                          <?php $this->assign('productids', ""); ?>
                                                                          <p>
                                                                          Services: 
                                                                          <!-- <span>  -->
                                                                          <?php $_from = $this->_tpl_vars['item']['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['product']):
?>
                                                                          		
                                                                          		<?php $this->assign('productids', ((is_array($_tmp=$this->_tpl_vars['productids'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['product']['id']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['product']['id']))); ?>
                                                                          		<?php $this->assign('productids', ((is_array($_tmp=$this->_tpl_vars['productids'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ','))); ?>
                                                                          		
                                                                          		<?php echo $this->_tpl_vars['product']['name']; ?>
,
                                                                          <?php endforeach; endif; unset($_from); ?>	
                                                                             </p> 
                                                                          
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
services" name="<?php echo $this->_tpl_vars['item']['id']; ?>
services"><?php echo $this->_tpl_vars['productids']; ?>
</div>
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
contact_number" name="<?php echo $this->_tpl_vars['item']['id']; ?>
contact_number"><?php echo $this->_tpl_vars['item']['contact_number']; ?>
</div>
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
email_address" name="<?php echo $this->_tpl_vars['item']['id']; ?>
email_address"><?php echo $this->_tpl_vars['item']['email_address']; ?>
</div>

                                                                          <?php $this->assign('new_day', ""); ?>
                                                                          <?php $this->assign('new_start_time', ""); ?>
                                                                          <?php $this->assign('new_end_time', ""); ?>
                                                                          <?php $this->assign('new_is_off', ""); ?>

                                                                          <?php $_from = $this->_tpl_vars['item']['new_schedule']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sched']):
?>
                                                                             <?php $this->assign('new_day', ((is_array($_tmp=$this->_tpl_vars['new_day'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sched']['day']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sched']['day']))); ?>
                                                                             <?php $this->assign('new_day', ((is_array($_tmp=$this->_tpl_vars['new_day'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ','))); ?>

                                                                             <?php $this->assign('new_start_time', ((is_array($_tmp=$this->_tpl_vars['new_start_time'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sched']['start_time']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sched']['start_time']))); ?>
                                                                             <?php $this->assign('new_start_time', ((is_array($_tmp=$this->_tpl_vars['new_start_time'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ','))); ?>
                                                                             
                                                                             <?php $this->assign('new_end_time', ((is_array($_tmp=$this->_tpl_vars['new_end_time'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sched']['end_time']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sched']['end_time']))); ?>
                                                                             <?php $this->assign('new_end_time', ((is_array($_tmp=$this->_tpl_vars['new_end_time'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ','))); ?>

                                                                             <?php $this->assign('new_is_off', ((is_array($_tmp=$this->_tpl_vars['new_is_off'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sched']['is_off']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sched']['is_off']))); ?>
                                                                             <?php $this->assign('new_is_off', ((is_array($_tmp=$this->_tpl_vars['new_is_off'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ','))); ?>
                                                                          <?php endforeach; endif; unset($_from); ?>  

                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
new_day" name="<?php echo $this->_tpl_vars['item']['id']; ?>
new_day"><?php echo $this->_tpl_vars['new_day']; ?>
</div>
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
new_start_time" name="<?php echo $this->_tpl_vars['item']['id']; ?>
new_start_time"><?php echo $this->_tpl_vars['new_start_time']; ?>
</div>
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
new_end_time" name="<?php echo $this->_tpl_vars['item']['id']; ?>
new_end_time"><?php echo $this->_tpl_vars['new_end_time']; ?>
</div>
                                                                          <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
new_is_off" name="<?php echo $this->_tpl_vars['item']['id']; ?>
new_is_off"><?php echo $this->_tpl_vars['new_is_off']; ?>
</div>
                                                                            
                                                                          <!-- </span> -->
                                                                          <br />

                                                                      
                                                                          
                                                                          <!-- <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#newspecialist">View</button> -->
                                                                          <button type="button" class="btn btn-md btn-dark" onclick="edit('<?php echo $this->_tpl_vars['item']['id']; ?>
')">Edit</button>
                                                                          <button type="button" class="btn btn-sm btn-md btn-delete" onClick="delete_specialist('<?php echo $this->_tpl_vars['item']['id']; ?>
')">Delete</button>
                                                                         
                                                                         <!--  <div class="specialist-status-onservice col-sm-7">On Service</div> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- <div class="row">
                                                            	<div class="col-md-12 col-sm-12 col-xs-12">
                                                            		<div class="specialist-name">
                                                            			<p>Schedule: <span>
                                                                  <?php echo ((is_array($_tmp=$this->_tpl_vars['item']['schedule'])) ? $this->_run_mod_handler('replace', true, $_tmp, ',', ', ') : smarty_modifier_replace($_tmp, ',', ', ')); ?>

                                                                   </span><br /></p>
                                                            			<p>Schedule Time: <span> <?php echo $this->_tpl_vars['item']['start_time']; ?>
 - <?php echo $this->_tpl_vars['item']['end_time']; ?>
</span><br /></p>
                                                            		</div>
                                                            	</div>		
                                                            	
                                                            </div> -->
                                                           
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php endforeach; endif; unset($_from); ?>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-lg-12 pull-right">
                                                    <ul class="pagination"></ul>
                                                </div>
                                            </div>
                            </section>
                        </section>
                  </section>
                <!-- /.content -->
               
         </section>
        </section>
    </section>









<div id="dlgEditSpecialist" name="dlgEditSpecialist" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Specialist Information</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
      <div class="alert alert-danger error-prompt" role="alert" hidden>
          <div class="error-holder">
           
          </div>
      </div>
                    
      <div class="row">
             <form role="form" id="frmSpecialist" name="frmSpecialist" enctype="multipart/form-data">
             	<input type="hidden" id="txtSpecialistId" name="txtSpecialistId">
             	<input type="hidden" id="txtDays" name="txtDays">
             	<input type="hidden" id="txtServices" name="txtServices">
             	<input type="hidden" id="txtStartTime" name="txtStartTime">
             	<input type="hidden" id="txtEndTime" name="txtEndTime">
                 <div class="col-md-3 col-xs-3">
                    <div class="ta-center" style="margin-top:20px;">
                          <img id="imgProfile" name="imgProfile" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/specialist-avatar-new.png" class="specialist-avatar specialist-avatar-add" /><br />
                          <input type="file" name="file" id="file" class="inputfile"   /> 
                          <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Upload Photo</label>  
                          <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule"><i class="fa fa-calendar"></i> Days of Schedule</button>
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setservices"><i class="fa fa"></i> Services</button>
                    </div>
                 </div>
                 <div class="col-md-9 col-xs-9">
                    <div class="row" style="display:none;">
                       <div class="col-md-4 col-xs-4 col-xs-offset-8">
                             <div class="form-group">
                                <label>Branch</label>
                                    <select name="txtBranch" id="txtBranch" class="form-control m-b">
                                        <?php $_from = $this->_tpl_vars['branches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['branch_name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>First Name <span class="req">*</span></label>
                                <input type="text" id="txtFirstName" name="txtFirstName" class="form-control" placeholder="First name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" id="txtMiddleName" name="txtMiddleName" class="form-control" placeholder="Middle name"> 
                              </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                             <div class="form-group">
                                <label>Last Name <span class="req">*</span></label>
                                <input type="text" id="txtLastName" name="txtLastName" class="form-control" placeholder="Last name"> 
                              </div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Gender</label>
                                    <select name="txtGender" id="txtGender" class="form-control m-b">
                                        <option value="MALE">MALE</option>
                                        <option value="FEMALE">FEMALE</option>
                                    </select>
                              </div>
                        </div>
                        <!-- <div class="col-md-4 col-xs-4">
                              <div class="form-group">
                                <label>Status</label>
                                    <select name="branch" id="assignbranch" class="form-control m-b">
                                        <option>Single</option>
                                        <option>Married</option>
                                    </select>
                              </div>
                        </div> -->
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                  <label>Birth Date: <span class="req">*</span></label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right datepicker" id="txtBirthDate" name="txtBirthDate">
                                      <!-- <input type="text" class="form-control pull-right datepicker" id="txtDateBirth" name="txtDateBirth"> -->
                                  </div>
                              </div>
                        </div>
                        
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                  <label>Contact Number: <span class="req">*</span></label>
                                  <div class="form-group">
                                      
                                      <input type="text" id="txtContactNumber" name="txtContactNumber"  class="form-control" placeholder="Contact Number">
                                      <!-- <input type="text" class="form-control pull-right datepicker" id="txtDateBirth" name="txtDateBirth"> -->
                                  </div>
                              </div>
                        </div>
                        
                    </div>

                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                          <div class="form-group">
                                <label>Address</label>
                                <textarea rows="2" cols="50" id="txtAddress" name="txtAddress"  class="form-control"></textarea>
                           </div>
                      </div>
                    </div>


                     <div class="row">
                           <div class="col-md-4 col-xs-4">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" id="txtCity" name="txtCity" class="form-control" placeholder="City">
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4" style="display: none;">
                                  <div class="form-group">
                                    <label>Country</label>
                                        <select name="txtCountry" id="txtCountry" class="form-control m-b">
                                            <?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        		<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
" <?php if ($this->_tpl_vars['item']['name'] == 'United States'): ?> selected="selected"   <?php endif; ?> ><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                        	<?php endforeach; endif; unset($_from); ?>
                                        </select>
                                  </div>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                 <div class="form-group">
                                    <label>State</label>
                                        <input type="text" id="txtState" name="txtState" class="form-control" placeholder="State">
                                  </div>
                            </div>
                            
                            <div class="col-md-4 col-xs-4">
                                 <div class="form-group">
                                      <label>Start Date <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtStartDate" name="txtStartDate">
                                      </div>
                                 </div>
                            </div>
                    </div>
                    
                    <div class="row">
                       <div class="col-md-12 col-xs-12">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="text" id="txtEmailAddress" name="txtEmailAddress" class="form-control" placeholder="Email Address">
                              </div>
                        </div>
                    </div>

                    <div class="row">
                          <!-- <div class="col-md-6 col-xs-6">
                                 <div class="form-group">
                                      <label>Start Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtStartDate" name="txtStartDate">
                                      </div>
                                 </div>
                            </div> -->
                            <div class="col-md-6 col-xs-6" style="display: none;">
                                  <div class="form-group">
                                      <label>End Date: <span class="req">*</span></label>
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right datepicker" id="txtEndDate" name="txtEndDate">
                                      </div>
                                  </div>
                            </div>
                    </div>

                 </div>
             
	  </div>
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
                   <button type="submit" class="btn btn-primary" id="btnSaveSpecialist" name="btnSaveSpecialist">Save Information</button>
                   <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>

	</form>

  </div>
</div>


<div id="setservices" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Service</h4>
                
        </div>
        
        <div class="modal-content">
             <div class="row">
             	<div class="col-lg-12">
              <ul class="nav nav-tabs" role="tablist">
                <?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                  <?php if ($this->_tpl_vars['key'] == 0): ?>
                  <li role="presentation" class="active"><a href="#cat<?php echo $this->_tpl_vars['key']; ?>
" aria-controls="cat<?php echo $this->_tpl_vars['key']; ?>
" role="tab" data-toggle="tab"><strong><?php echo $this->_tpl_vars['item']['name']; ?>
</strong></a></li>
                  <?php else: ?>
                  <li role="presentation"><a href="#cat<?php echo $this->_tpl_vars['key']; ?>
" aria-controls="cat<?php echo $this->_tpl_vars['key']; ?>
" role="tab" data-toggle="tab"><strong><?php echo $this->_tpl_vars['item']['name']; ?>
</strong></a></li>
                  <?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
              </ul>
              <div class="tab-content">
             <?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item2']):
?>
              <?php if ($this->_tpl_vars['key'] == 0): ?>
              <div role="tabpanel" class="tab-pane active" id="cat<?php echo $this->_tpl_vars['key']; ?>
">
              <?php else: ?>
              <div role="tabpanel" class="tab-pane" id="cat<?php echo $this->_tpl_vars['key']; ?>
">
              <?php endif; ?>

              <?php $_from = $this->_tpl_vars['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <?php if ($this->_tpl_vars['item']['category_name'] == $this->_tpl_vars['item2']['name']): ?>
                <div class="col-md-4 col-sm-4 col-xs-4">
                          <label style="margin-right: 5px; font-size: 14px;">
                            <input type="checkbox" name="services[]" id="services[]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" class="flat-red"> <?php echo $this->_tpl_vars['item']['name']; ?>
</label>
                    </div>

                <?php endif; ?>
              <?php endforeach; endif; unset($_from); ?>
              </div>
             <?php endforeach; endif; unset($_from); ?>
             </div>
             </div>
             </div>
		</div>
		
		<!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnCheckAllService" id="btnCheckAllService">Check All Services</button>
                <button class="btn btn-primary" name="btnUncheckAllService" id="btnUncheckAllService">Uncheck All Services</button>
                <button class="btn btn-primary" name="btnSetService" id="btnSetService">Set Service</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
		
	</div>
</div>

<!-- Modal -->
<div id="setschedule" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <input type="hidden" id="business_hours" name="business_hours" value='<?php echo $this->_tpl_vars['business_hours']; ?>
'>

            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Schedule</h4>
                <p>Create Schedule for new Specialist</p>
            </div>




            <!-- Modal content-->
            <div class="modal-content">
                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                      <div class="col-md-6 col-sm-6">
                             <div class="col-md-12 col-sm-12">
                                        <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Monday" class="flat-red"> Monday</label>
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                       <label>End Time:</label>
                                      <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                     </div>
                              </div>
                      </div>  
                      <div class="col-md-6 col-sm-6">
                          <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Tuesday"  class="flat-red"> Tuesday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                              <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                        <div class="input-group">
                                          <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                          <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                               <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                        <div class="input-group">
                                          <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                          <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                      </div>



                  </div>

                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                      <div class="col-md-6 col-sm-6">
                         <div class="col-md-12 col-sm-12">
                                <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Wednesday"  class="flat-red"> Wednesday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                               <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                          <div class="col-md-12 col-sm-12">
                                <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Thursday"  class="flat-red"> Thursday</label>
                          </div>
                          <div class="col-md-6 col-sm-6">   
                             <label>Start Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          
                                          <div class="input-group">
                                            <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                                </div>
                          </div>
                          <div class="col-md-6 col-sm-6">  
                               <label>End Time:</label>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                          <div class="input-group">
                                            <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                          </div>
                                    </div>
                               </div>
                          </div>
                      </div>        
                  </div>




                  <div class="row" style="border-bottom: 1px solid #DDD;margin-top: 10px;">
                          <div class="col-md-6 col-sm-6">
                             <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Friday" class="flat-red"> Friday</label>
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                      <div class="form-group">
                                            
                                            <div class="input-group">
                                              <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                              <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                            </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                   <label>End Time:</label>
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>  
                          </div>
                          <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-12 col-sm-12">
                                      <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Saturday" class="flat-red"> Saturday</label>
                                </div>
                                  <div class="col-md-6 col-sm-6">   
                                     <label>Start Time:</label>
                                      <div class="bootstrap-timepicker">
                                          <div class="form-group">
                                                <div class="input-group">
                                                  <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6">  
                                       <label>End Time:</label>
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                  <div class="input-group">
                                                    <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                  </div>
                                            </div>
                                        </div>
                                  </div> 
                             </div>        
                         
                   </div>


                    <div class="row" style="margin-top: 10px;">
                          <div class="col-md-6 col-sm-6">
                                <div class="col-md-12 col-sm-12">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Sunday" class="flat-red"> Sunday</label> 
                              </div>
                              <div class="col-md-6 col-sm-6">   
                                 <label>Start Time:</label>
                                 <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceStartTime" name="txtSourceStartTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-6">  
                                   <label>End Time:</label>
                                   <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                              <div class="input-group">
                                                <input type="text" id="txtSourceEndTime" name="txtSourceEndTime[]" class="form-control timepicker">
                                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                              </div>
                                        </div>
                                    </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-6">
                          </div>
                     </div>







                  

               

                <!-- <div class="row" style="margin-top:20px;">
                      <div class="col-md-6 col-xs-6">
                         <div class="bootstrap-timepicker">
                            <div class="form-group">
                                  <label>Start Time:</label>
                                  <div class="input-group">
                                    <input type="text" id="txtSourceStartTime" name="txtSourceStartTime" class="form-control timepicker">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                  </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-xs-6">
                          <div class="bootstrap-timepicker">
                              <div class="form-group">
                                    <label>End Time:</label>
                                    <div class="input-group">
                                      <input type="text" id="txtSourceEndTime" name="txtSourceEndTime" class="form-control timepicker">
                                      <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    </div>
                              </div>
                          </div>
                      </div>
                </div> -->

          </div>


            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnSetSchedule" id="btnSetSchedule">Set Schedule</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</div>