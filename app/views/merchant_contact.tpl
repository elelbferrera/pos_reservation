    <script type="text/javascript" src="{$webroot_resources}/js/merchant.js">
    	
    	
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchant
                    <small>Summary</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/merchant/management"><i class="fa fa-dashboard"></i> Merchant</a></li>
                        <li class="active">Merchants</li>
                        <li class="active">Summary</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-sm-3 sidelink-wrap">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="{$webroot}/merchant/summary/{$partner_id}"><i class="fa fa-list-alt" aria-hidden="true"></i> Summary</a></li>
                                    <li><a href="{$webroot}/merchant/profile/{$partner_id}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                    <li><a href="{$webroot}/merchant/contact/{$partner_id}" class="selected"><i class="fa fa-users" aria-hidden="true"></i> Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                        	<form role="form" method="post" name="frmUpdateMerchant" id="frmUpdateMerchant" enctype="multipart/form-data">
                            <div class="box" id="merchant-info-wrap" name="merchant-info-wrap">
                                <div class="box-header with-border" >
                                	<!-- <h3 class="box-title">Company Profile</h3> -->
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                    	<div class="box-header with-border">
                                			<h3 class="box-title">Contact Information</h3>
                                		</div>
                                		
                                		<div class="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">First Name:</label>
                                                    <input type="text" class="form-control" id="txtFirstName" name="txtFirstName" value="{$merchant.first_name}" placeholder="Enter First Name">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Last Name:</label>
                                                    <input type="text" class="form-control" id="txtLastName" name="txtLastName" value="{$merchant.last_name}" placeholder="Enter Last Name">
                                                </div>
                                            </div>
                                		</div>
                                		
                                		<div class="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Position:</label>
                                                    <input type="text" class="form-control" id="txtPosition" value="{$merchant.position}" name="txtPosition" placeholder="Enter Position">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Mobile Number:</label>
                                                    <input type="text" class="form-control" id="txtMobileNumber"  value="{$merchant.mobile_number}" name="txtMobileNumber" placeholder="Enter Your Mobile Number">
                                                </div>
                                            </div>
                                		</div>
                                		
                                		<div class="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Office Number:</label>
                                                    <input type="text" class="form-control" id="txtOfficeNumber" value="{$merchant.office_number}" name="txtOfficeNumber" placeholder="Enter Position">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Extension:</label>
                                                    <input type="text" class="form-control" id="txtExtension" name="txtExtension"  value="{$merchant.extension}" placeholder="Enter Extension">
                                                </div>
                                            </div>
                                		</div>        
                                		
                                		<div class="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Email Address:</label>
                                                    <input type="text" class="form-control" id="txtContactEmail" value="{$merchant.contact_email}" name="txtContactEmail" placeholder="Enter Contact Email Address">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Fax:</label>
                                                    <input type="text" class="form-control" id="txtContactFax" value="{$merchant.contact_fax}" name="txtContactFax" placeholder="Enter Contact Fax">
                                                </div>
                                            </div>
                                		</div>    
                                        
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <!-- <div class="box-footer ta-center">
                                        
                                        <button type="submit" class="btn btn-primary" id="btnUpdateMerchant" name="btnUpdateMerchant">	
                                        	Update Merchant Info
                                        </button>
                                    </div> -->
                                
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                            
                            </form>
                        </div>
                        
                        
                        
                        
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->