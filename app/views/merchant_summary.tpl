      <script type="text/javascript" src="{$webroot_resources}/js/merchant.js">
    	
    	
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchant
                    <small>Summary</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/merchant/management"><i class="fa fa-dashboard"></i> Merchant</a></li>
                        <li class="active">Merchants</li>
                        <li class="active">Summary</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-sm-3 sidelink-wrap">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="{$webroot}/merchant/summary/{$partner_id}" class="selected"><i class="fa fa-list-alt" aria-hidden="true"></i> Summary</a></li>
                                    <li><a href="{$webroot}/merchant/profile/{$partner_id}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                    <li><a href="{$webroot}/merchant/contact/{$partner_id}"><i class="fa fa-users" aria-hidden="true"></i> Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-9 merchantinfo-wrap">
                            <div class="row">
                            	<div class="col-sm-6">
                                	<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Profile</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                            	<div class="col-sm-4 summary-title">
                                                	First Name
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.first_name}
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Last Name
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.last_name}
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Company Name
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.company_name}
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	DBA
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.dba}
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Contact No.
                                                </div>
                                                <div class="col-sm-8">
                                                	+{$merchant.phone1}
                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Email
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.email}	
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                	Address
                                                </div>
                                                <div class="col-sm-8">
                                                	{$merchant.address1}
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                	<!--<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Invoices</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-4 summary-title">
                                                    Paid
                                                </div>
                                                <div class="col-sm-8">
                                                    1 ($100.50 USD)
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                    Unpaid
                                                </div>
                                                <div class="col-sm-8">
                                                    2 ($10.48 USD)
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                    Income
                                                </div>
                                                <div class="col-sm-8">
                                                    $80.02 USD
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                        		<a href="#"><i class="fa fa-file-text"></i> Create Invoice</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Attachments</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-md-6 col-lg-4">
                                                	<a href="#" class="summary-attachment"><i class="fa fa-file"></i>application.pdf</a>
                                                </div>
                                                <div class="col-md-6 col-lg-4">
                                                	<a href="#" class="summary-attachment"><i class="fa fa-file"></i>sample.doc</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                            	<a href="#"><i class="fa fa-plus-circle"></i> Add File</a>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="col-sm-4">
                                	<!--<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Products</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-4 summary-title">
                                                    Products
                                                </div>
                                                <div class="col-sm-8">
                                                    0 (Total)
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                            	<a href="product-add.php"><i class="fa fa-plus-circle"></i> Add New Product</a>
                                            </div>
                                            <div>
                                            	<a href="#"><i class="fa fa-search"></i> View Products</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Notes</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-12">
                                                    N/A
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="col-sm-12">
                                	<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Products</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-12">
                                                    <table id="merchantlist" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Application ID.</th>
                                                                <th>Product</th>
                                                                
                                                                <th>Status</th>
                                                                <th class="no-sort"></th>
                                                                <th class="no-sort"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        	
                                                        	{foreach from=$histories item=item}
                                                            <tr>
                                                                <td>{$item.create_date|date_format:"%Y-%m-%d"}</td>
                                                                <td>{$item.order_id}</td>
                                                                <td>{$item.name}</td>
                                                                
                                                                <td>{$item.status}</td>
                                                                <td class="icon-td">
                                                                	{if $item.status eq 'Pending'}
                                                                		<a target="_blank" href="{$webroot}/merchant/generate_pdf_doc/{$partner_id}&order_id={$item.order_id}"><i class="fa fa-file-pdf-o"></i></a>
                                                                	{/if}
                                                                </td>
                                                                <td class="icon-td">
	                                                                {if $item.status eq 'Pending'}
	                                                                	<a href="#" class="sendmodal" onclick="SendPDFDoc('{$partner_id}','{$item.order_id}');"><i class="fa fa-send"></i></a>
	                                                                {else}
	                                                                	
	                                                                {/if}
                                                                </td>
                                                            </tr>
                                                            {/foreach}
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->