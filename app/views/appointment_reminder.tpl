<style type="text/css">
{literal}
  @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

  .appspinner input {
    text-align: right;
  }

  .input-group-btn-vertical {
    position: relative;
    white-space: nowrap;
    width: 2%;
    vertical-align: middle;
    display: table-cell;

  }

  .input-group-btn-vertical > .btn {
    display: block;
    float: none;
    width: 100%;
    max-width: 100%;
    padding: 8px;
    margin-left: -1px;
    position: relative;
    border-radius: 0;
    height: 20.5px;
  }

  .input-group-btn-vertical > .btn:first-child {
    border-top-right-radius: 4px;
    margin-bottom: 0;
  }

  .input-group-btn-vertical > .btn:last-child {
    margin-top: -1px;
    border-bottom-right-radius: 4px;
  }

  .input-group-btn-vertical i {
    position: absolute;
    top: 0;
    left: 4px;
  }

    select{
      direction: rtl;
    }
{/literal}
</style>

<!-- .content -->
<section id="content">
          <section class="vbox">  
                 <header class="header bg-darkblue b-light">
                  <div class="row">
                       <div class="col-md-12 col-xs-12">
                          <div class="breadtitle-holder">
                              <div class="breadtitle">
                                  <i class="fa fa-bell titleFA"></i> 
                                  <p class="headeerpage-title">Appointment Reminders Settings</p>
                              </div>
                          </div>
                       </div>
                     
                  </div>
              </header>

              <section class="scrollable wrapper">
                <form role="form" id="frmAppointmentReminder" name="frmAppointmentReminder" enctype="multipart/form-data"></form>
                  <div style="width:40%; margin:0 auto; background-color:#FFF; border:1px solid #ddd;border-radius: 15px; margin-top:25px;">
                      <div class="row">
                          <div class="col-md-12 col-xs-12">
                              <div style="background-color:#25313E; color:#FFF !important;">
                                 <div style="color:#FFF; font-size:20px; font-weight:400; padding:10px;">Send Notifications to clients</div>
                              </div>
                               <div class="form-group" style="padding:15px;margin-bottom: 0px;">
                                  <div class="row">
                                      <label class="col-sm-3 control-label">Email: On/Off</label>
                                      <div class="col-sm-9">
                                          <label class="switch">
                                              <input type="checkbox" id="email_notif" {if $reminder.is_notification_on==1 } checked="checked" {/if}> <span></span> </label>
                                      </div>
                                  </div>
                              </div>
                              <div style="background-color:#25313E; color:#FFF !important;">
                                 <div style="color:#FFF; font-size:20px; font-weight:400; padding:10px;">Message Template:</div>
                              </div>
                              <div class="form-group" style="padding:15px;;margin-bottom: 0px;">
                                 <div class="row">
                                      <div class="form-group">
                                          <div class="col-sm-12 control-label">
                                              <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#previewMessage"><i class="fa fa-search"></i> View</button>
                                               <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#editMessage"><i class="fa fa-edit"></i> Edit</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div style="background-color:#25313E; color:#FFF !important;">
                                 <div style="color:#FFF; font-size:20px; font-weight:400; padding:10px;">When To Send Reminders?</div>
                              </div>
                              <div class="form-group" style="padding:15px;;margin-bottom: 0px;">
                                      <p>Send appointment reminders</p>
                                      <div class="row">
                                          <div class="form-group">
                                              <div class="col-sm-12">
                                                <div class="input-group appspinner">
                                                    <input type="text" class="form-control bfh-number rounded" id="txtFrequencyValue" name="txtFrequencyValue" style="font-style:italic;" value="{if $reminder.frequency_value} {$reminder.frequency_value} {else} 1 {/if}" min="1" max="365">
                                                    <div class="input-group-btn-vertical">
                                                      <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                                      <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                                    </div>
                                                  </div>
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <div class="col-sm-12">
                                              
                                                    <select name="txtFrequencyOption" id="type" class="form-control m-b" style="margin-top:5px;" placeholder="day/hour/mins">
                                                        <option {if $reminder.frequency_option=='days' } selected {/if}>Days</option>
                                                        <option {if $reminder.frequency_option=='hours' } selected {/if}>Hours</option>
                                                        <option {if $reminder.frequency_option=='minutes' } selected {/if}>Min</option>
                                                    </select>

                                                     <p>before the appointment</p>

                                              </div>
                                          </div>
                                      </div>
                                      <button type="submit" class="btn btn-md btn-primary" id="btnSubmitAppointment" data-dismiss="modal">Set Reminder</button>
                              </div>

                          </div>
                      </div>
                  </div>
                </form>
              </section>

         </section>
</section>
<!-- /.content -->
               
        </section>
    </section>
</section>
    
<!-- Modal -->
<div id="previewMessage" class="modal fade" role="dialog">
      <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Message Preview</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
              <h2>Sublect: <span style="font-size:14px;" id="subject-title"></span></h2>
              <div id="content-msg" style="padding:30px; border:1px solid #ddd;"></div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
           </div>
      </div>
</div>
<!-- Modal -->

<div id="editMessage" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Message Preview</h4>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Subject</label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" name="subject" {if $reminder.subject} value="{$reminder.subject}" {/if}>
                    </div>
                </div><br><br>
                <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a data-edit="fontName Serif" style="font-family:&#39;Serif&#39;">Serif</a></li>
                                                            <li><a data-edit="fontName Sans" style="font-family:&#39;Sans&#39;">Sans</a></li>
                                                            <li><a data-edit="fontName Arial" style="font-family:&#39;Arial&#39;">Arial</a></li>
                                                            <li><a data-edit="fontName Arial Black" style="font-family:&#39;Arial Black&#39;">Arial Black</a></li>
                                                            <li><a data-edit="fontName Courier" style="font-family:&#39;Courier&#39;">Courier</a></li>
                                                            <li><a data-edit="fontName Courier New" style="font-family:&#39;Courier New&#39;">Courier New</a></li>
                                                            <li><a data-edit="fontName Comic Sans MS" style="font-family:&#39;Comic Sans MS&#39;">Comic Sans MS</a></li>
                                                            <li><a data-edit="fontName Helvetica" style="font-family:&#39;Helvetica&#39;">Helvetica</a></li>
                                                            <li><a data-edit="fontName Impact" style="font-family:&#39;Impact&#39;">Impact</a></li>
                                                            <li><a data-edit="fontName Lucida Grande" style="font-family:&#39;Lucida Grande&#39;">Lucida Grande</a></li>
                                                            <li><a data-edit="fontName Lucida Sans" style="font-family:&#39;Lucida Sans&#39;">Lucida Sans</a></li>
                                                            <li><a data-edit="fontName Tahoma" style="font-family:&#39;Tahoma&#39;">Tahoma</a></li>
                                                            <li><a data-edit="fontName Times" style="font-family:&#39;Times&#39;">Times</a></li>
                                                            <li><a data-edit="fontName Times New Roman" style="font-family:&#39;Times New Roman&#39;">Times New Roman</a></li>
                                                            <li><a data-edit="fontName Verdana" style="font-family:&#39;Verdana&#39;">Verdana</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                                            <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                                            <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="fa fa-strikethrough"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="fa fa-list-ul"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="fa fa-list-ol"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="fa fa-indent"></i></a> 
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a> 
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="fa fa-link"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="input-group m-l-xs m-r-xs">
                                                                <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-default btn-sm" type="button">Add</button>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <a class="btn btn-default btn-sm" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="fa fa-cut"></i></a> 
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm" title="" id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="fa fa-picture-o"></i></a>
                                                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 35px; height: 30px;"> 
                                                    </div>
                                                    <div class="btn-group"> 
                                                        <a class="btn btn-default btn-sm" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a> 
                                                        <a class="btn btn-default btn-sm" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a> 
                                                    </div>
                                                </div>
                                                <div id="editor" class="form-control" style="overflow:scroll;height:500px;max-height:500px" contenteditable="true">
                                                        {if $reminder.html_template} {$reminder.html_template} {else} 

                                                        Hi [first_name], <br ><br >


                                                        We just wanted to remind you that you have an appointment coming up with us. Please take a moment to review the details of your appointment and contact us if you need to make any changes. <br ><br >
                                                        <b>Appointment Details:</b><br ><br >
                                                        [appointment_date]
                                                        for [services_list]<br >
                                                        We look forward to seeing you at your upcoming appointment. <br ><br ><br >
                                                        Thank you,<br >
                                                        <b>[company_name]</b><br >
                                                        [company_phone]<br >
                                                        [company_email]<br ><br >

                                                        [company_address]<br >
                                                        [company_city], [company_state] [company_postal_code]

                                                        {/if}
                                                </div>
                                
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button type="button" class="btn btn-md btn-primary" id="btnUpdateMessage" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
          </div>
  </div>
</div>

 <script src="{$webroot_resources}/js/appointment.js"></script>