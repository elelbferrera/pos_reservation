    <script type="text/javascript" src="{$webroot_resources}/js/merchant.js">
    	
    	
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchant
                    <small>Summary</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/merchant/management"><i class="fa fa-dashboard"></i> Merchant</a></li>
                        <li class="active">Merchants</li>
                        <li class="active">Summary</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-sm-3 sidelink-wrap">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="{$webroot}/merchant/summary/{$partner_id}"><i class="fa fa-list-alt" aria-hidden="true"></i> Summary</a></li>
                                    <li><a href="{$webroot}/merchant/profile/{$partner_id}" class="selected"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                    <li><a href="{$webroot}/merchant/contact/{$partner_id}"><i class="fa fa-users" aria-hidden="true"></i> Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                        	<form role="form" method="post" name="frmUpdateMerchant" id="frmUpdateMerchant" enctype="multipart/form-data">
                            <div class="box" id="merchant-info-wrap" name="merchant-info-wrap">
                                <div class="box-header with-border" >
                                	<!-- <h3 class="box-title">Company Profile</h3> -->
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                    	<div class="row">
                                            <!-- <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label for="processor">Processor:</label>
	                                                <input type="text" class="form-control" id="processor" placeholder="Enter processor">
	                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="merchantID">Merchant ID:</label>
                                                    <input type="text" class="form-control" id="merchantID" placeholder="Enter merchant id">
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12"><h4>Company Profile:</h4></div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Processor:</label>
                                                    <input type="text" value="{$merchant.merchant_processor}" class="form-control" id="txtProcessor" name="txtProcessor" placeholder="Enter processor">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">Merchant ID:</label>
                                                    <input type="text" value="{$merchant.merchant_id}" class="form-control" id="txtMerchantID" name="txtMerchantID" placeholder="Enter Merchant ID">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-header with-border">
                                			<h3 class="box-title">Company Profile</h3>
                                		</div>
                                		
                                		<div class ="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Company Name:</label>
                                                    <input type="text" value="{$merchant.company_name}" class="form-control" id="txtCompanyName" name="txtCompanyName" placeholder="Enter Company Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">DBA:</label>
                                                    <input type="text" value="{$merchant.dba}" class="form-control" id="txtDBA" name="txtDBA" placeholder="Enter DBA">
                                                </div>
                                            </div>	
                                		</div>
                                		
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text" value="{$merchant.address1}" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" value="{$merchant.address2}" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" value="{$merchant.city}" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text" value="{$merchant.state}" class="form-control" id="txtState" name="txtState" placeholder="Enter state">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text"  value="{$merchant.zip}"class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountryUpdate" name="txtCountryUpdate">
									                  {foreach from=$country item=item}
									                  	<option value="{$item.name}" {if $item.name eq $merchant.country_name}  selected="selected"   {/if}  >{$item.name}</option>
									                  {/foreach}
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" value="{$merchant.email}" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" value="{$merchant.phone1}" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" value="{$merchant.phone2}" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Fax:</label>
                                                    <input type="text" value="{$merchant.fax}" class="form-control" id="txtFax" name="txtFax" placeholder="Enter Fax">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: none">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <!-- <option selected="selected">Alabama</option>
									                  <option>Alaska</option>
									                  <option>California</option>
									                  <option>Delaware</option>
									                  <option>Tennessee</option>
									                  <option>Texas</option>
									                  <option>Washington</option> -->
									                  {foreach from=$partner_type item=item}
									                  	<option value="{$item.id}"{if $item.name eq 'MERCHANT'} selected="selected" {/if}>{$item.name}</option>
									                  {/foreach}
									                </select>

                                                </div>
                                            </div>
                                            {foreach from=$documents item=d}
	                                        <div class="col-md-12">
	                                        	
	                                            <div class="form-group">
	                                                <label>{$d.name}:</label>
	                                                <!-- <input type="file" id="fileUploadVoidCheck" name="fileUploadVoidCheck"> -->
	                                               	<div>
	                                               		<a href="{$d.document_image}" target="_blank"> <img style="max-width: 250px;" src="{$d.document_image}" />
	                                               		</a>
	                                               	</div>
	                                            </div>
	                                        </div>
	                                        {/foreach}
                                        </div> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <!-- <div class="box-footer ta-center">
                                        
                                        <button type="submit" class="btn btn-primary" id="btnUpdateMerchant" name="btnUpdateMerchant">	
                                        	Update Merchant Info
                                        </button>
                                    </div> -->
                                
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                            
                            </form>
                        </div>
                        
                        
                        
                        
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->