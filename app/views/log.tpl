<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GO3 Reservation</title>
    <link rel="shortcut icon" href="{$webroot_resources}/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{$webroot_resources}/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{$webroot_resources}/dist/css/AdminLTE.css">
    <!-- iCheck -->
  	<link rel="stylesheet" href="{$webroot_resources}/plugins/iCheck/square/pink.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{$webroot_resources}/assets/css/custom.css">
    	<link rel="stylesheet" href="{$webroot_resources}/plugins/pace/pace.min.css">
	<!-- PACE -->
	<script src="{$webroot_resources}/plugins/pace/pace.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>    

<script type="javascript">
{literal}
	$(document).ajaxStart(function() {
		Pace.restart();
	});
	$('.ajax').click(function() {
		$.ajax({
			url : '#',
			success : function(result) {
				// $('.ajax-content').html('<hr>Ajax Request Completed !');
			}
		});
	});
{/literal}
	
</script>
    
    
    
<body class="hold-transition login-page">
	


<!-- <script>
{literal}
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});
{/literal}
</script> -->
</body>
</html>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GO3 Reservation</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{$webroot_resources}/css/go3reservation.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">

<div id="dlg_message" name="dlg_message" class="modal fade" role="dialog">
  <div class="modal-dialog2">
<div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" style="padding-top: 20px;color: #000;position: absolute;z-index: 999; font-size: 35px;float: left;right: 10px;">&times;</button>
        </div>
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-3">
                <!-- <img src="{$webroot_resources}/images/update.png" class="servicesbg-modal" /> -->

                <img src="{$webroot_resources}/images/success.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-9">
                <div class="service-content">
                     <div class="success-title" id="lbltitle" name="lbltitle">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessage" name="dlg_lblMessage">
                            Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
            </div>
            <!-- <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button> -->
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>

<!-- make it dynamic content sir lems :p Modal -->
<div id="dlg_messageerror" name="dlg_messageerror" class="modal fade" role="dialog">
  <div class="modal-dialog2">
    <!-- <div class="modal-header bg-primary"> -->
    <!-- </div> -->
 <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" style="padding-top: 20px;color: #000;position: absolute;z-index: 999; font-size: 35px;float: left;right: 10px;">&times;</button>
        </div>

         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-3">
                <!-- <img src="{$webroot_resources}/images/update.png" class="servicesbg-modal" /> -->
                <img src="{$webroot_resources}/images/error.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-9">
                <div class="service-content">
                     <div class="success-title" id="lbltitleerror" name="lbltitleerror">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessageerror" name="dlg_lblMessagerrror">
                            Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
                
            </div>

            <!--<button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>-->
        </div>

    </div>

    <!-- /. Modal content-->
  </div>
</div>

{if $action_tpl != ''}{include file="$action_tpl"}{/if}


   
<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="{$webroot_resources}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{$webroot_resources}/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="{$webroot_resources}/plugins/iCheck/icheck.min.js"></script>

<script src="{$webroot_resources}/js/public.js"></script>
 
<script src="{$webroot_resources}/js/log_in.js"></script>

<script>
{literal}
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-pink',
            radioClass: 'iradio_square-pink',
            increaseArea: '20%' // optional
        });
    });
{/literal}
</script>
 


</body>
</html>
