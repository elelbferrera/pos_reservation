    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    User Type
                    <!--<small>Optional description</small>-->
                    <a href="{$webroot}/admin/new_profile">
                    	<button class="btn btn-primary" type="button">Add New Profile</button>
                    </a>
					<a href="{$webroot}/admin/new_module">
                    	<button class="btn btn-primary" type="button">Add New Module</button>
                    </a>
                    </h1>
                    
                    
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Admin</li>
                        <li class="active">ACL</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                	
                    <!-- Your Page Content Here -->
                    	
                        <br>
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">User Type Profile</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                	
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th width="70%">Profile Name</th>
												<th width="30%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									        {foreach from=$profile_list item=item}
									                <tr>
									                    <td>{$item.description}</td>
									                    <td>
									                        <input class="btn btn-warning" type="button" onClick="window.location.href='edit_profile/{$item.id}'" value="Edit" />
									                        <!--<input style="{$rem_btn.delete_partner}" rel="{$item.id}" class="ordBtn delBtnProfile" type="button" value="Delete" />-->
									                        {if $item.create_by neq "DEF" }
									                            <input type="button" class="btn btn-danger" value="Delete" id="{$item.id}" onClick="deleteProfile('{$item.id}')" />       
									                        {/if}
									                    </td>
									                </tr>
									        {/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    