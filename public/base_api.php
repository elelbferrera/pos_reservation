<?php

require_once './prerun.php';


class base_api {
    protected $parameters = array();
    protected $responses = array();
    protected $instance;
    protected $db_credentials;
    protected $env;
    protected $db;
    protected $logs;
    protected $agent_code = 'O01023';
    public $method;
    public $return_mode;
    protected $default_response_mode = 'json';
    
    public function __construct() {
        $this->return_mode = $this->default_response_mode;
		
        if (isset($_SERVER['QUERY_STRING']) && trim($_SERVER['QUERY_STRING']) != '') {
            parse_str($_SERVER['QUERY_STRING'], $this->parameters);
        }else{
        	$this->parameters = $_POST;
        }
		

        //$env = (PATH_SEPARATOR == ':')? 'prod':'dev';
        $env = ENV;
		
        $this->instance = $this;
        $this->load_libraries();
        $this->set_paths();
        settings::setEnv(strtoupper($env));
        $this->db_credentials = array(
            'host'      => settings::$_db['host'],
            'user'      => settings::$_db['user'],
            'password'  => settings::$_db['pass'],
            'db'        => settings::$_db['name'],
        );
        $this->responses = array(
            'UNKNOWN_ERROR'             => array('1', 'Unknown error'),
            'DATABASE_ERROR'            => array('2', 'Unable to load database'),
            'INVALID_PARAMETER_COUNT'   => array('3', 'Invalid parameter count'),
            'INVALID_PARAMETER_VALUE'   => array('4', 'Invalid parameter values'),
            'INVALID_METHOD'            => array('5', 'Invalid method name'),
        );
		
        if (!$this->db_check()) {
            return $this->failed('DATABASE_ERROR');
        }
		
        //$this->set_timezone('America/New_York', $this->db);
        $this->statuses = array(
            0 => 'Pending',
            1 => 'Submitted',
            2 => 'Failed/Declined',
            3 => 'Paid',
            5 => 'Completed(Test)',
        );
        $server_vars = array(
            'host'  => isset($_SERVER['HTTP_HOST'])? $_SERVER['HTTP_HOST']: '',
            'ip'    => isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR']: '',
            'port'  => isset($_SERVER['REMOTE_PORT'])? $_SERVER['REMOTE_PORT']: '',
            'query' => isset($_SERVER['QUERY_STRING'])? $_SERVER['QUERY_STRING']: '',
            'time'  => isset($_SERVER['REQUEST_TIME_FLOAT'])? date('r', $_SERVER['REQUEST_TIME_FLOAT']): '',
        );
        $this->logs['server_vars'] = serialize($server_vars);
    }
    
    protected function set_timezone($time_zone, $db = NULL) {
        date_default_timezone_set($time_zone);
        if ($db !== NULL) {
            $dateTimeZoneGmt = new DateTimeZone("GMT");
            $dateTimeZoneCustom = new DateTimeZone($time_zone);
            $dateTimeGmt = new DateTime(date('Y-m-d'), $dateTimeZoneGmt);
            $timeOffset = $dateTimeZoneCustom->getOffset($dateTimeGmt) / 3600;
            $plus = ((int) $timeOffset > 0) ? '+' : '';
            $tz = $plus . $timeOffset;
            $db->query("SET time_zone = '{$tz}:00'");
        }
    }
        
    protected function getUserPermissions($username) {
        $result = $this->db->fetchPairs("
            SELECT  res.path, res.path as path2
            FROM    ez_resources res 
            INNER JOIN ez_permissions perm on res.id=perm.resource_id 
            INNER JOIN ez_users usr on usr.id=perm.user_id 
            WHERE   res.status=1
            AND     usr.status=1
            AND     perm.status=1
            AND usr.username=" . $this->db->db_escape($username, true)
            );
        return $result;
    }
    
    protected function _param_count_check($num) {
        for ($i = 1; $i <= $num; $i++) {
            if (!isset($this->parameters['P'. str_pad($i,2, '0', STR_PAD_LEFT)] )) {
                return FALSE;
            }
        }
        return TRUE;
    }    
    
    protected function xml_generator($array_data, $numerical_tag = 'Record') {
        if (isset($_GET['return_mode']) && (strtolower($_GET['return_mode']) == 'json' || strtolower($_GET['return_mode']) == 'xml')) {
            $this->return_mode = strtolower($_GET['return_mode']);
        }
        if ($this->return_mode == 'json') {
            $output = json_encode($array_data);
            header('Content-type: application/json'); 
        } else {
            $xml_data = new SimpleXMLElement("<?xml version=\"1.0\"?><EZKardTransaction/>");
            $this->array_to_xml($array_data, $xml_data);
            $output = $xml_data->asXML();
            header('Content-type: text/xml'); 
        }
        $this->logs['output_response'] = $output;
        $this->write_logs();
        echo $output;
    }   
    
    protected function array_to_xml($info, &$xml_info, $numerical_tag = 'Record') {
        foreach($info as $key => $value) {
            if(is_array($value)) {
                $subnode = is_numeric($key)? $xml_info->addChild($numerical_tag): $xml_info->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
 
    protected function write_logs() {
        if (is_array($this->logs)) {
            $this->logs['timestamp'] = date('Y-m-d H:i:s');
            if (!(isset($this->logs['time_spent']) && $this->logs['time_spent'] != '' && $this->logs['time_spent'] != 0)) {
                $this->logs['time_spent'] = mt_rand(1, 5000) / 3600;
            }
            foreach ($this->logs as $key => $value) {
                if ($key != 'time_spent' && $key != 'timestamp') {
                    $this->logs[$key] = xorry::encode($value, $this->logs['timestamp'] . $this->logs['time_spent']);//$crypt->encrypt(md5('.'.$timestamp.'.'), $value);
                }
            }
            $this->db->insert('api_calls', $this->logs);
            
        } else {
            return FALSE; //todo: check here
        }
    }
    
    protected function success($responsemsg) {
        $data = array();
        if (!is_null($responsemsg)) {
            $data['respmsg'] = $responsemsg;
        }
        $data['respcode']  = '0000';
        $data['status'] = 'success';
        $this->xml_generator($data);
        exit;
    }
    
    protected function trim_all($arr) {
        if (is_array($arr) && count($arr) > 0) {
            foreach ($arr as $key => $val) {
                if (!is_numeric($val)) {
                    $arr[$key] = trim($val);
                }
            }
        }
        return $arr;
    }
    
    protected function failed($error_code = 1) {
        if (is_array($error_code)) {
            $data = array(
                'respmsg'   => $error_code[1],
                'respcode'  => $error_code[0] == ''? '911': $error_code[0],
            );
        } else {
            if (!isset($this->responses[$error_code])) {
                $data = array(
                    'respmsg'   => $error_code,
                    'respcode'  => '911',
                );
            } else {
                $data = array(
                    'respmsg'   => $this->responses[$error_code][1],
                    'respcode'  => $this->responses[$error_code][0],
                );
            }
        }
        $data['respcode'] =  str_pad($data['respcode'], 3, '0', STR_PAD_LEFT);
        $data['status'] = 'failed';
        $this->xml_generator($data);
        exit;
    }
    
    protected function load_libraries() {
        define('BASEPATH', dirname(__FILE__));
        $lib_path = dirname(__DIR__) .'/app/libraries/';
        set_include_path(get_include_path() . PATH_SEPARATOR . '.' . PATH_SEPARATOR . $lib_path);
        
        
        require_once $lib_path.PROGRAM.'_settings.php';
        require_once 'db.php';
        require_once 'xorry.php';
    }
    
    //PP 05/20/2014 added for webroot
    private function set_paths() {
        
        define('CASHADVANCE_SUBFOLDER', '/pos_reservation');
        $sub_dir = CASHADVANCE_SUBFOLDER;
        
        $this->doc_root = trim(dirname(dirname(__FILE__)), ' /') . '/app';
        if (PATH_SEPARATOR == ':') {
            $this->doc_root = '/' . $this->doc_root;
        }
        define('DOCROOT', $this->doc_root);
        
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            define('PROTOCOL' , 'https://');
        } else {
            define('PROTOCOL' , 'http://');
        }
        define('WEBROOT', PROTOCOL . $_SERVER['SERVER_NAME'] . $sub_dir);
        define('MANGO_NETWORK_ID', '1');
        define('MANGO_CONFIRMATION_NUMBER', '1');
        define('MANGO_SITE_ID', '1');
        define('SYSTEM_ADMIN', 'System Admin');
        define('SFTP_URL', '23.253.195.156');
        //define('SFTP_USER', 'jeff.ramos');
        define('SFTP_USER', 'qgls');
        //define('SFTP_PASS', 'zb30MIvNeK');
        define('SFTP_PASS', '2MMdvmLj');
        define('DOWNLOAD_FILENAME', 'customer.csv');
        define('DOWNLOAD_FILENAME_WSC', 'customer.txt');
        define('WSC_BRANCH_CODE', '1');
        define('WSC_BANK_CODE', 'RB');
        
        define('BEC_EMAIL', 'markjeffrey.ramos@pyxpay.com');
        set_include_path(get_include_path() . PATH_SEPARATOR . DOCROOT  . PATH_SEPARATOR . DOCROOT . '/libraries');
    }
    protected function db_check() {
        if (isset($this->db_credentials) && is_array($this->db_credentials)) {
            $this->db = db::getInstance($this->db_credentials);
            if ($this->db->getStatus() == 0) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
        return TRUE;
    }    
}

