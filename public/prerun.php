<?php
$root_path = dirname(__FILE__);

if (!file_exists('c:/Windows/hh.exe') && !file_exists('/etc/hh.exe')) {
    define('ENV', 'PROD');
    define('IS_PRODUCTION', TRUE);
    
    define('DOMAIN', 'na');
    define('PROGRAM', 'pos_reservation');
    define('PORTAL_SUBDOMAIN', '');
    define('CORE_SUBDOMAIN', ''); 
    define('HOST', 'smtp.gmail.com');
    define('PORT', '465');
    define('EMAIL_USERNAME', 'lem@go3.ph');
    define('EMAIL_PASSWORD', 'lemslems');
    define('NEWLINE',"\n"); 
    define('CUSTOMER_USER_TYPE_ID', serialize(array('3')));
    define('AGENT_USER_TYPE_ID', serialize(array('6')));
    define('MERCHANT_USER_TYPE_ID', serialize(array('2','8','7')));
    define('ISO_USER_TYPE_ID', serialize(array('4','5')));
    define('ADMIN_USER_TYPE_ID', serialize(array('1','9')));


	
	define('PHONEPREFIX', '1');
} else {
    // define('HOST', 'mail.go3rewards.com');
    // define('PORT', '26');
    // define('EMAIL_USERNAME','admin@go3rewards.com');
    // define('EMAIL_PASSWORD', 'admin2016go3');
    define('API', "");
	define('PHONEPREFIX', '1');
	define('HOST', 'smtp.gmail.com');
    define('PORT', '465');
    define('EMAIL_USERNAME', 'lem@go3.ph');
    define('EMAIL_PASSWORD', 'lemslems');
    define('PORTAL_SUBDOMAIN', '');
    define('CORE_SUBDOMAIN', '');
    
    define('ENV', 'DEV');
    define('IS_PRODUCTION', FALSE);
    define('NEWLINE',"\r\n");
    
    define('CUSTOMER_USER_TYPE_ID', serialize(array('3')));
    define('AGENT_USER_TYPE_ID', serialize(array('6')));
    define('MERCHANT_USER_TYPE_ID', serialize(array('2','8','7')));
    define('ISO_USER_TYPE_ID', serialize(array('4','5')));
    define('ADMIN_USER_TYPE_ID', serialize(array('1','9')));
    
    
    $config_file = (dirname($root_path) . '/program.php');
    if (!file_exists($config_file)) {
        $config_file = (dirname(dirname($config_file)) . '/program.php');
    }
    if (!file_exists($config_file)) {
        exit($config_file);
    }
    require_once $config_file;
    
   
}
$paths = array(
    // '/application/config/',
    '/../app/libraries/',
    '/',
);

foreach ($paths as $path) {
    $settings_file = $root_path . $path . strtolower(PROGRAM) . '_settings.php';
    if (file_exists($settings_file)) {

        require_once $settings_file;
        settings::setEnv(ENV);

        return true;
    }
}

exit('Invalid configuration');