$(document).ready(function() {
	/*$('#example').paginate();*/
	var disList = new List('paginateDiv', {
		  valueNames: ['category-name'],
	});
	var itemCount = disList.matchingItems.length;

	if(itemCount % 6 === 0){
		itemCount = (itemCount - 5);
	}else if(itemCount % 6 === 5){
		itemCount = (itemCount - 4);
	}else if(itemCount % 6 === 4){
		itemCount = (itemCount - 3);
	}else if(itemCount % 6 === 3){
		itemCount = (itemCount - 2);
	}else if(itemCount % 6 === 2){
		itemCount = (itemCount - 1);
	}else if(itemCount % 6 === 1){
		itemCount;
	}
	
	if(location.search){
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['category-name'],
		  page: 6,
		  pagination: true,
		  i: itemCount
		});
	}else{
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['category-name'],
		  page: 6,
		  pagination: true
		});
	}
	
	$('#btnUpdateCategory').click(function (){
		$('input, textarea').removeClass('input-error');
		var valid = true;		

		var description = $('#txtDescription').val();
		var name = $('#txtName').val();
		
		if(name.trim()=="")
		{
			/*CustomAlert("Enter category name.", "Invalid Field");
			return false;*/
			$("#txtName").addClass('input-error');
    		valid = false;
		}
		
		if(description.trim()=="")
		{
			$("#txtDescription").addClass('input-error');
    		valid = false;
			/*CustomAlert("Enter category description.", "Invalid Field");
			return false;*/
		}

		if (!valid) {
    		return false;
    	}
		
		var postdata = $("#frmEditCategory").serialize();
		if($('#txtCategoryId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
	            	var delay=1000; //3 second
	                setTimeout(function() {
	                    window.location.href = data.redirect_url;    
	                }, delay);
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error updating");
	            }
	            
	        });
	        
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
	            	var delay=1000; //3 second
	                setTimeout(function() {
	                    window.location.href = data.redirect_url +'?saved=1';   
	                }, delay);
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error Saving");
	            }
	            
	        });
	        
	    }
	    return false;
	});
	
});


function add_category()
{	
	$('input, textarea').removeClass('input-error');	
	$('#txtCategoryId').val("");
	$('#txtName').val("");
	$('#txtDescription').val("");

	
	$('#dlgEditCategory').modal('show');
}


function edit_category(id)
{
	$('input, textarea').removeClass('input-error');

	var name = document.getElementById(id+'name').innerHTML;
	
	var description = document.getElementById(id+'description').innerHTML;
	
	
	$('#txtName').val(name.trim());
	$('#txtDescription').val(description.trim());
	
	$('#txtCategoryId').val(id);
	
	$('#dlgEditCategory').modal('show');
}

function delete_category(id)
{
	if (!confirm("Are you sure to want to delete this category?")) {
	    return false;
	}else{
		var postdata = "category_id="+id;
		showloading();
		$.postJSON("?action=delete_category", postdata, function(data) {
	     	if(data.success)
	        {
	        	closeloading();
	        	//CustomAlert(data.message, "Record Deleted");
	        	var delay=1000; //3 second
                setTimeout(function() {
                    window.location.href = data.redirect_url;    
                }, delay);
	        }else{
	        	closeloading();
	        	CustomAlert(data.message, "Error Saving", "error");
	        }
	        
	    });
	}

}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});