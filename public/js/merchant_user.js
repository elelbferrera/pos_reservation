$(document).ready(function() {
	
	$('#btnSaveUser').click(function (){
		
		var username = $('#txtUsername').val();
		var user_id = $('#txtUserId').val();
		var last_name = $('#txtLastName').val();
		var first_name = $('#txtFirstName').val();
		var position = $('#txtPosition').val();
		
		var email_address = $('#txtEmailAddress').val();
		var password =  $('#txtPassword').val();
		var r_password =  $('#txtRepeatPassword').val();
		
		if(username.trim()=="")
		{
			CustomAlert("Username is required.", "Unable to Save User", "error");
			return false;
		}
		
		if(last_name.trim()=="")
		{
			CustomAlert("Last Name is required.", "Unable to Save User", "error");
			return false;
		}
		if(first_name=="")
		{
			CustomAlert("First Name is required.", "Unable to Save User", "error");
			return false;
		}
		
		if(email_address.trim()=="")
		{
			CustomAlert("Email Address is required.", "Unable to Save User", "error");
			return false;
		}

		if(position.trim()=="")
		{
			CustomAlert("Position is required.", "Unable to Save User", "error");
			return false;
		}
		
		if(password.trim() !== r_password.trim())
		{
			CustomAlert("Password and Repeat password should be the same.", "Unable to Save User", "error");
			return false;
		}
		
		
		var postdata = $("#frmUser").serialize();
		
		if($('#txtUserId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	
	            	CustomAlert(data.message, "Save User");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save User", "error");
	            }
	            closeloading();
	        });
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	CustomAlert(data.message, "Save User");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save User", "error");
	            }
	            closeloading();
	        });
	        
	    }
	    return false;
	});
	
});


function add()
{
	
	$('#txtLastName').val("");
	$('#txtFirstName').val("");
	
	$('#txtUserId').val("-1");
	$('#txtEmailAddress').val("");
	$('#txtPassword').val("");
	$('#txtRepeatPassword').val("");
	$('#txtPosition').val("");
	$('#txtUsername').val("");

	$('#divPassword').show();
	$('#divUsername').show();
	$('#divRepeatPassword').show();
	$('.modal-title').text('Register New Account User');
	$('#dlgEditUser').modal('show');
}

function edit(id)
{
	
	$('#divPassword').hide();
	$('#divUsername').hide();
	$('#divRepeatPassword').hide();
	
	var username = document.getElementById(id+'username').innerHTML;
	var user_id = id;
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var email_address = document.getElementById(id+'email_address').innerHTML;
	var position = document.getElementById(id+'position').innerHTML;
	
	$('#txtUserId').val(id);
	
	
	$('#txtUsername').val(username);
	
	$('#txtLastName').val(last_name);
	$('#txtFirstName').val(first_name);
	$('#txtEmailAddress').val(email_address);
	$('#txtPosition').val(position);
	
	$('.modal-title').text('Edit User');
	$('#dlgEditUser').modal('show');
}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});