$(document).ready(function() {
	// $('#slider1').tinycarousel();

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	            $('#imgProfile').attr('src', e.target.result);
	        }
	        
	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#file").change(function(){
		readURL(this);
	});

	var disList = new List('paginateDiv', {
	  valueNames: ['specialist-name']
	});

	var itemCount = disList.matchingItems.length;

	if(itemCount % 6 === 0){
		itemCount = (itemCount - 5);
	}else if(itemCount % 6 === 5){
		itemCount = (itemCount - 4);
	}else if(itemCount % 6 === 4){
		itemCount = (itemCount - 3);
	}else if(itemCount % 6 === 3){
		itemCount = (itemCount - 2);
	}else if(itemCount % 6 === 2){
		itemCount = (itemCount - 1);
	}else if(itemCount % 6 === 1){
		itemCount;
	}

	if(location.search){
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['specialist-name'],
		  page: 6,
		  pagination: true,
		  i: itemCount
		});
	}else{
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['specialist-name'],
		  page: 6,
		  pagination: true
		});
	}
	

	/*$('#example').paginate();*/
	$('#txtContactNumber').mask("999-999-9999");
	    $('.datepicker').datepicker({
           	autoclose: true,
        });
        // autoclose: true,
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false,
          showSeconds: false
        });
        
	     // $('.timepicker').timepicker({
	        // use24hours: true
	    // });
	    
	    // $('.timepicker').timepicker({
	        // use24hours: true,
	        // format: 'HH:mm'
	    // });

	$('#btnCheckAllService').click(function() {
		 $("input[name='services[]']").prop('checked', true);
	});

	$('#btnUncheckAllService').click(function() {
		 $("input[name='services[]']").prop('checked', false);
	});
	
	
	$('#btnSetService').click(function (){
		var checkboxes = document.getElementsByName("days[]");
		var services ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				services = services + checkboxes[i].value +",";
			}
		}
	
		services = services.substr(0, services.length-1);
		$('#txtServices').val(services);
	
		$('#setservices').modal('hide');
	});
	

	$('#btnSetSchedule').click(function (){
		var checkboxes = document.getElementsByName("days[]");
		var days ="";
	    
	    var arrStartTime = document.getElementsByName("txtSourceStartTime[]");
		var starttime ="";

		var arrEndTime = document.getElementsByName("txtSourceEndTime[]");
		var endtime ="";

	    for (var i=0; i<checkboxes.length; i++) {
	    	if (checkboxes[i].checked == true) {
		   		days = days + (checkboxes[i].value).toUpperCase() +",";
		   		starttime = starttime + convertTo24Hour(arrStartTime[i].value) +",";
		   		endtime = endtime + convertTo24Hour(arrEndTime[i].value) +",";
		   	}	
		}
		
		days = days.substr(0, days.length-1);
		starttime = starttime.substr(0, starttime.length-1);
		endtime = endtime.substr(0, endtime.length-1);

		$('#txtDays').val(days);
		
		$('#txtStartTime').val(starttime);
		$('#txtEndTime').val(endtime);
		
		$('#setschedule').modal('hide');
	});
	
	// $('#btnSaveSpecialist').click(function (){
	$('#frmSpecialist').submit(function(){
		
		var first_name = $('#txtFirstName').val();
		var middle_name = $('#txtMiddleName').val();
		var last_name = $('#txtLastName').val();
		var gender = $('#txtGender').val();
		var birth_date = $('#txtBirthDate').val();
		var address = $('#txtAddress').val();
		var city = $('#txtCity').val();
		var country = $('#txtCountry').val();
		var state = $('#txtState').val();
		var start_date = $('#txtStartDate').val();
		var end_date = $('#txtEndDate').val();
		var contact_number = $('#txtContactNumber').val();

		$(".error-prompt").hide();
		$(".error-holder").html();
		var valid = true; // checker of validation
		var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
		var error_return = '';
		
		if(first_name.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> First Name is required</span><br>';
			valid = false;
			/*CustomAlert("First Name is required.", "Unable Save Specialist", "error");
			return false;*/
		}
		
		if(last_name.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Last Name is required</span><br>';
			valid = false;
			/*CustomAlert("Last Name is required.", "Unable Save Specialist", "error");
			return false;*/
		}
		if(gender== null)
		{
			error_return += error_default+'<span class="error-msg"> Gender is required</span><br>';
			valid = false;
			/*CustomAlert("Gender is required.", "Unable Save Specialist", "error");
			return false;*/
		}

		if(birth_date.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Birthdate is required</span><br>';
			valid = false;
		}

		if(start_date.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Startdate is required</span><br>';
			valid = false;
		}

		if(contact_number.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Contact Number is required</span><br>';
			valid = false;
		}

		if (!valid) {
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
		}

		// if(birth_date =="")
		// {
		// 	CustomAlert("Birth date is required.", "Save Specialist", "error");
		// 	return false;
		// }
		// if(city.trim()=="")
		// {
		// 	CustomAlert("City is required.", "Unable Save Specialist", "error");
		// 	return false;
		// }
		// if(country=="")
		// {
			// CustomAlert("Country is required.", "Unable Save Specialist", "error");
			// return false;
		// }
		
		// if(state.trim()=="")
		// {
		// 	CustomAlert("State is required.", "Unable Save Specialist", "error");
		// 	return false;
		// }
		
		var checkboxes = document.getElementsByName("days[]");
		var days ="";

		var arrStartTime = document.getElementsByName("txtSourceStartTime[]");
		var starttime ="";

		var arrEndTime = document.getElementsByName("txtSourceEndTime[]");
		var endtime ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				days = days + (checkboxes[i].value).toUpperCase() +",";
		   		starttime = starttime + convertTo24Hour(arrStartTime[i].value) +",";
		   		endtime = endtime + convertTo24Hour(arrEndTime[i].value) +",";
			}
		}

	    if(days.length==0)
		{
			/*CustomAlert("Please select atleast one day on days of the week.", "Unable Save Specialist", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Please select atleast one day on days of the week</span><br>';
			valid = false;
		}
		
		days = days.substr(0, days.length-1);
		starttime = starttime.substr(0, starttime.length-1);
		endtime = endtime.substr(0, endtime.length-1);

		$('#txtDays').val(days);
		$('#txtStartTime').val(starttime);
		$('#txtEndTime').val(endtime);
		
		var checkboxes = document.getElementsByName("services[]");
		var services ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				services = services + checkboxes[i].value +",";
			}
		}
		
		if(services.length==0)
		{
			/*CustomAlert("Please select atleast one service.", "Unable Save Specialist", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Please select atleast one service</span><br>';
			valid = false;
		}

		if (!valid) {
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
		}
		
		services = services.substr(0, services.length-1);
		$('#txtServices').val(services);
		
		
		
		/*$('#txtStartTime').val(convertTo24Hour($('#txtSourceStartTime').val()));
		$('#txtEndTime').val(convertTo24Hour($('#txtSourceEndTime').val()));*/
		
		var postdata = $("#frmSpecialist").serialize();
		
		
		
		if($('#txtSpecialistId').val()>0)
		{
			// showloading();
	        // $.postJSON("?action=update", postdata, function(data) {
	            // if(data.success)
	            // {
	            	// CustomAlert(data.message, "Save Specialist");
	            	// window.location.href = window.location.href;
	            // }else{
	            	// CustomAlert(data.message, "Unable Save Specialist", "error");
	            // }
	            // closeloading();
	        // });
	         showloading();
	         $.ajax({
	            url: "?action=update", // Url to which the request is send
	            type: "POST",             // Type of request to be send, called as method
	            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
	            dataType: 'json',
	            contentType: false,       // The content type used when sending data to the server.
	            cache: false,             // To unable request pages to be cached
	            processData:false,        // To send DOMDocument or non processed data file it is set to false
	            success: function(data)   // A function to be called if request succeeds
	            {
	                
	                if(data.success)
	                {
	                	//CustomAlert(data.message, "Save Specialist");
	                    var delay=1000; //3 second
	                    setTimeout(function() {
	                        //window.location.href = window.location.href;    
	                        var url = $("#txtURL").val();
	                        window.location.href = url + "/specialist/management"    
		
	                    }, delay);
	                }else{
	                	CustomAlert(data.message, "Unable Save Specialist", "error");
	                }
	                closeloading();
	            }
        	});
	        
	        
	    }else{
	    	// showloading();
	    	// $.postJSON("?action=add", postdata, function(data) {
	            // if(data.success)
	            // {
	            	// CustomAlert(data.message, "Save Specialist");
	            	// window.location.href = window.location.href;
	            // }else{
	            	// CustomAlert(data.message, "Unable Save Specialist", "error");
	            // }
	            // closeloading();
	        // });
	        showloading();
	         $.ajax({
	            url: "?action=add", // Url to which the request is send
	            type: "POST",             // Type of request to be send, called as method
	            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
	            dataType: 'json',
	            contentType: false,       // The content type used when sending data to the server.
	            cache: false,             // To unable request pages to be cached
	            processData:false,        // To send DOMDocument or non processed data file it is set to false
	            success: function(data)   // A function to be called if request succeeds
	            {
	                
	                if(data.success)
	                {
	                	//CustomAlert(data.message, "Save Specialist");
	                   	var delay=1000; //3 second
		                setTimeout(function() {
		                    window.location.href = data.redirect_url +'?saved=1';     
		                }, delay);
	                }else{
	                	CustomAlert(data.message, "Unable Save Specialist", "error");
	                }
	                closeloading();
	                    
	            }
        	});
	        
	    }
	    return false;
	});

});


function add()
{
    
	$(".error-prompt").hide();
	$(".error-holder").html();

	var business_hours = jQuery.parseJSON($('#business_hours').val());
	var start_time = document.getElementsByName("txtSourceStartTime[]");
	var end_time = document.getElementsByName("txtSourceEndTime[]");
	var days = document.getElementsByName("days[]");
	for (var a=0; a<business_hours.length; a++) {
		console.log(business_hours[a].is_close)
		days[a].checked = false;

		start_time[a].value = convertToAMPM(business_hours[a].start_time);
		end_time[a].value = convertToAMPM(business_hours[a].end_time);

		if (business_hours[a].is_close == 1) {
			days[a].disabled = true;
			start_time[a].disabled = true;
			end_time[a].disabled = true;
		}
	}
		
	// $('#divMinutes').show();
	// $('#divQuantity').hide();
	// $('#txtCost').val(0);
	// $('#txtPrice').val(0);
	// $('#txtProductId').val("");
	// $('#txtProductType').val("SERVICE");
	// $('#txtDescription').val("");
	// $('#txtMinutesOfService').val(1);
	// $('#txtQuantity').val(0);
	$('#txtSpecialistId').val("-1");
	$('#txtLastName').val("");
	$('#txtFirstName').val("");
	$('#txtMiddleName').val("");
	$('#txtBirthDate').val("");
	// $('#txtBranch').val("");
	$('#txtGender').val("FEMALE");
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtStartDate').val("");
	$('#txtEndDate').val("");
	$('#txtContactNumber').val("");
	$('#txtEmailAddress').val("");
	
	$('#txtStartTime').val("");
	$('#txtEndTime').val("");
	$('#imgProfile').attr('src', window.location.origin + '/public/images/avatar_default.jpg')
	/*var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}*/
	
	var checkboxes = document.getElementsByName("services[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	
	
	$('#dlgEditSpecialist').modal('show');
}

function edit(id)
{
	var business_hours = jQuery.parseJSON($('#business_hours').val());
	
	var start_time = document.getElementsByName("txtSourceStartTime[]");
	var end_time = document.getElementsByName("txtSourceEndTime[]");
	var days = document.getElementsByName("days[]");
	for (var a=0; a<business_hours.length; a++) {
		console.log(business_hours[a].is_close)
		days[a].checked = false;

		start_time[a].value = convertToAMPM(business_hours[a].start_time);
		end_time[a].value = convertToAMPM(business_hours[a].end_time);

		if (business_hours[a].is_close == 1) {
			days[a].disabled = true;
			start_time[a].disabled = true;
			end_time[a].disabled = true;
		}
	}

	var last_name = document.getElementById(id+'last_name').innerHTML;
	var branch_id = document.getElementById(id+'branch_id').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var middle_name = document.getElementById(id+'middle_name').innerHTML;
	var birth_date = document.getElementById(id+'birth_date').innerHTML;
	var profile_picture = document.getElementById(id+'profile_picture').innerHTML;
	var gender = document.getElementById(id+'gender').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var schedule = document.getElementById(id+'schedule').innerHTML;
	var start_time = document.getElementById(id+'start_time').innerHTML;
	var end_time = document.getElementById(id+'end_time').innerHTML;
	var start_date = document.getElementById(id+'start_date').innerHTML;
	var end_date = document.getElementById(id+'end_date').innerHTML;
	var new_day = document.getElementById(id+'new_day').innerHTML;
	var new_start_time = document.getElementById(id+'new_start_time').innerHTML;
	var new_end_time = document.getElementById(id+'new_end_time').innerHTML;
	var new_is_off = document.getElementById(id+'new_is_off').innerHTML;
	
	var services = document.getElementById(id+'services').innerHTML;
	var contact_number = document.getElementById(id+'contact_number').innerHTML;
	var email_address = document.getElementById(id+'email_address').innerHTML;
	
	// $("#imgProfile").empty();
// create variable holding the image src from the href link
	$("#imgProfile").attr('src', profile_picture);
	
	var d = new moment(end_time, 'HH:mm:ss');
	// alert(d);
	// return false;
	
	/*end_time = convertToAMPM(end_time);
	start_time = convertToAMPM(start_time);*/
	
	$('#txtEmailAddress').val(email_address);
	$('#txtContactNumber').val(contact_number.substring(0,3) + '-' + contact_number.substring(3,6) + '-' + contact_number.substring(6,10));
	
	/*$('#txtSourceStartTime').val(start_time);
	$('#txtSourceEndTime').val(end_time);*/
	
	$('#txtSpecialistId').val(id);
	$('#txtLastName').val(last_name);
	$('#txtFirstName').val(first_name);
	$('#txtMiddleName').val(middle_name);
	$('#txtBirthDate').val(birth_date.replace(" 00:00:00", ""));
	$('#txtBranch').val(branch_id);
	$('#txtGender').val(gender);
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtState').val(state);
	$('#txtStartDate').val(start_date.replace(" 00:00:00", ""));
	$('#txtEndDate').val(end_date.replace(" 00:00:00", ""));
	
	var days = new_day;
	new_is_off = new_is_off.split(",");
	new_start_time = new_start_time.split(",");
	new_end_time = new_end_time.split(",");
	
	var checkboxes = document.getElementsByName("days[]");
	var checkStartTime = document.getElementsByName("txtSourceStartTime[]");
	var checkEndTime = document.getElementsByName("txtSourceEndTime[]");
	
	for (var i=0; i<checkboxes.length; i++) {
		if (new_is_off[i] == 0) {
			checkboxes[i].checked = true;
			checkStartTime[i].value = convertToAMPM(new_start_time[i]);
			checkEndTime[i].value = convertToAMPM(new_end_time[i]);
		} else {
			checkboxes[i].checked = false;
			checkboxes[i].after(' Day Off - ');
		}
	     /*var n = days.includes((checkboxes[i].value).toUpperCase()+",");

	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }*/
	}
	
	var checkboxes = document.getElementsByName("services[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = services.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	
	$('#dlgEditSpecialist').modal('show');
}

function delete_specialist(id)
{
	if (!confirm("Are you sure to want to delete this specialist?")) {
	    return false;
	}else{
		var postdata = "specialist_id="+id;
		showloading();
		$.postJSON("?action=delete_specialist", postdata, function(data) {
	     	if(data.success)
	        {
	        	closeloading();
	        	//CustomAlert(data.message, "Specialist Deleted");
	        	var delay=1000; //3 second
                setTimeout(function() {
                    window.location.href = data.redirect_url;    
                }, delay);
	        }else{
	        	closeloading();
	        	CustomAlert(data.message, "Error Saving", "error");
	        }
	        
	    });
	}

}


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});