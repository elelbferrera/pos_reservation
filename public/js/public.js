$(document).ready(function() {
	
});


function CloseMeNow(me)
{
	$("[id=tPicker]").removeClass('open').addClass('');
}


function convertTo24Hour(time) {
    var hours = parseInt(time.substr(0, 2));
    var minutes = time.substr(3, 2);
    if(time.indexOf('AM') != -1 && hours == 12) {
        time = time.replace('12', '0');
    }
    if(time.indexOf('PM')  != -1 && hours < 12) {
        time = time.replace(hours, (hours + 12));
        hours = hours + 12;
    }
    if((time.length)>5)
    {
    	time = time.substring(0,6);
    }
   
    //return time.replace(/(AM|PM)/, '');
    // if(minutes.length > 1)
    // {
    	// alert(minutes.length);
    	// minutes = "0" + minutes;
    // }
    if(hours<10)
    {
    	hours = "0" + hours; 
    }
    
    return hours + ":" + minutes;
}

function convertToAMPM(time){
	var meridian = "PM";
	var hours = parseInt(time.substr(0,2));
	
	var is_alreadyPM = false;
	if(hours>12)
	{
		hours = hours - 12;
		is_alreadyPM = true;
	}
	
	if(!is_alreadyPM)
	{
		if(hours == 0)
		{
			meridian = "AM";
		}
		
		if(hours <= 12)
		{
			meridian = "AM";
		}
	}
	
	if(hours <= 9)
	{
		hours = "0"+ hours;  
	}
	
	var minutes = time.substr(3,2);
	// alert(time);
	time = hours + ":" +  minutes + " " + meridian;
	
	return time;
	
}




function showloading()
{
	$('#dlgLoading').css('z-index', '999999');
	$('#dlgLoading').modal('show');
}


function closeloading()
{
	$('#dlgLoading').modal('hide');
}

function CustomAlert(msg, title, error)
{
	if(error==undefined)
	{
		error ="";
	}
	
    document.getElementById('dlg_lblMessage'+error).innerHTML = msg;
    document.getElementById('lbltitle'+error).innerHTML = title; 
	$('#dlg_message'+error).modal('show');
    return false;
}

function isANumber(sText) {
    var ValidChars = "0123456789.";
    var IsNumber=true;
    var Char;
    for (i = 0; i < sText.length && IsNumber == true; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
    }
    if ($.trim(sText) == '') {
        IsNumber = false;
    }
    return IsNumber;
}

jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});