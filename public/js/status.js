$(document).ready(function() {
	
	    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();
	
	$('#btnSaveStatus').click(function (){
		
		var name = $('#txtName').val();
		var id = $('#txtStatusId').val();
		var description = $('#txtDescription').val();
		var color = $('#txtColor').val();
		

		
		if(name.trim()=="")
		{
			CustomAlert("Name is required.", "Unable to Save Status", "error");
			return false;
		}
		
		if(color.trim()=="")
		{
			CustomAlert("Please pick a color.", "Unable to Save Status", "error");
			return false;
		}
		
		var postdata = $("#frmStatus").serialize();
		
		if($('#txtStatusId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	
	            	CustomAlert(data.message, "Save Status");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save Status", "error");
	            }
	            closeloading();
	        });
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	CustomAlert(data.message, "Save Status");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save Status", "error");
	            }
	            closeloading();
	        });
	        
	    }
	    return false;
	});
	
});


function add()
{
	
	$('#txtName').val("");
	$('#txtDescription').val("");
	$('#txtColor').val("");
	
	$('#dlgEditStatus').modal('show');
}

function edit(id)
{
	
	var name = document.getElementById(id+'name').innerHTML;
	var status_id = id;
	var description = document.getElementById(id+'description').innerHTML;
	var color = document.getElementById(id+'color').innerHTML;
	
	
	$('#txtStatusId').val(id);
	
	
	$('#txtName').val(name);
	
	$('#txtDescription').val(description.trim());
	$('#txtColor').val(color);
	
	$('#dlgEditStatus').modal('show');
}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});