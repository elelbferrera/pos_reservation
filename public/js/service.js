$(document).ready(function() {
	/*$('#example').paginate();*/

	var disList = new List('paginateDiv', {
	  valueNames: ['product-name', 'category-name']
	});
	var itemCount = disList.matchingItems.length;

	if(itemCount % 6 === 0){
		itemCount = (itemCount - 5);
	}else if(itemCount % 6 === 5){
		itemCount = (itemCount - 4);
	}else if(itemCount % 6 === 4){
		itemCount = (itemCount - 3);
	}else if(itemCount % 6 === 3){
		itemCount = (itemCount - 2);
	}else if(itemCount % 6 === 2){
		itemCount = (itemCount - 1);
	}else if(itemCount % 6 === 1){
		itemCount;
	}

	if(location.search){
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['product-name', 'category-name'],
		  page: 6,
		  pagination: true,
		  i: itemCount
		});
	}else{
		var monkeyList = new List('paginateDiv', {
		  valueNames: ['product-name', 'category-name'],
		  page: 6,
		  pagination: true
		});
	}

	var monkeyList2 = new List('paginated-table', {
	  valueNames: ['name'],
	  page: 3,
	  pagination: true
	});
	
	$('#btnUpdateService').click(function (){
		var cost = $('#txtCost').val();
		var price = $('#txtPrice').val();
		var product_type = $('#txtProductType').val();
		var description = $('#txtDescription').val();
		var minutes = $('#txtMinutesOfService').val();
		var name = $('#txtName').val();
		var quantity = $('#txtQuantity').val();

		$(".error-prompt").hide();
		$("#error-holder").html();
		var valid = true; // checker of validation
		var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
		var error_return = '';
		
		if(name.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Enter product/service name</span><br>';
			valid = false;
			/*CustomAlert("Enter product/service name.", "Invalid Field");
			return false;*/
		}
		
		if(description.trim()=="")
		{
			error_return += error_default+'<span class="error-msg"> Enter product description</span><br>';
			valid = false;
			/*CustomAlert("Enter product description.", "Invalid Field");
			return false;*/
		}
		if(!isANumber(cost))
		{
			error_return += error_default+'<span class="error-msg"> Enter a correct cost</span><br>';
			valid = false;
			/*CustomAlert("Enter a correct cost.", "Invalid Field");
			return false;*/
		}
		if(!isANumber(price))
		{
			error_return += error_default+'<span class="error-msg"> Enter a correct price</span><br>';
			valid = false;
			/*CustomAlert("Enter a correct price.", "Invalid Field");
			return false;*/
		}
		
		// if(product_type=="SERVICE")
		// {
// 			
		// }
		
		if(product_type=="INVENTORY")
		{
			if(!isANumber(quantity))
			{	
				error_return += error_default+'<span class="error-msg"> Enter a correct quantity</span><br>';
				valid = false;
				/*CustomAlert("Enter a correct quantity", "Invalid Field");
				return false;*/
			}
		}
		
		if (!valid) {
    		$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
    	}

		
		
		
		var postdata = $("#frmEditService").serialize();
		if($('#txtProductId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
	            	var delay=1000; //3 second
	                setTimeout(function() {
	                    window.location.href = data.redirect_url;    
	                }, delay);
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error updating");
	            }
	            
	        });
	        
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
					var delay=1000; //3 second
	                setTimeout(function() {
	                    window.location.href = data.redirect_url +'?saved=1';  
	                }, delay);
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error Saving");
	            }
	            
	        });
	        
	    }
	    return false;
	});
	
	$('#txtProductType').change(function (){
		var product_type = $('#txtProductType').val();	
		
		if(product_type=="SERVICE")
		{
			$('#divMinutes').show();
			$('#divQuantity').hide();
		}
		
		if(product_type=="INVENTORY")
		{
			$('#divMinutes').hide();
			$('#divQuantity').show();
		}
		
		if(product_type =="NON-INVENTORY")
		{
			$('#divMinutes').hide();
			$('#divQuantity').hide();
		}
		
	});
	
});


function add_service()
{
	$(".error-prompt").hide();
	$(".error-holder").html();

	$('#divMinutes').show();
	$('#divQuantity').hide();
	$('#txtCost').val(0);
	$('#txtPrice').val(0);
	$('#txtProductId').val("");
	$('#txtName').val("");
	$('#txtProductType').val("SERVICE");
	$('#txtDescription').val("");
	$('#txtMinutesOfService').val(1);
	$('#txtQuantity').val(0);
	
	$('#dlgEditService').modal('show');
}

function view_service(id)
{
	var cost = document.getElementById(id+'cost').innerHTML;
	var price = document.getElementById(id+'price').innerHTML;
	var product_type = document.getElementById(id+'product_type').innerHTML;
	var minutes = document.getElementById(id+'minutes').innerHTML;
	var name = document.getElementById(id+'product_name').innerHTML;
	var quantity = document.getElementById(id+'quantity').innerHTML;
	var description = document.getElementById(id+'description').innerHTML;
	
	document.getElementById('view_product_name').innerHTML = name;
	document.getElementById('view_description').innerHTML = description;
	
	document.getElementById('view_price').innerHTML = price;
	document.getElementById('view_cost').innerHTML = '$'+cost;
	document.getElementById('view_product_type').innerHTML = product_type;
	document.getElementById('view_quantity').innerHTML = quantity;
	document.getElementById('view_minutes').innerHTML = parseInt(minutes);
	document.getElementById('view_big_price').innerHTML = price;
	
	// if(product_type =="INVENTORY")
	// {
		// $('#inventory_quantity').show();
		// $('#minutes_of_service').hide();
	// }
	// if(product_type == "SERVICE")
	// {
		$('#inventory_quantity').hide();
		$('#minutes_of_service').show();
	// }
	// if(product_type == "NON-INVENTORY")
	// {
		// $('#inventory_quantity').hide();
		// $('#minutes_of_service').hide();
	// }
	
	
	$('#dlgViewService').modal('show');
}

function edit_service(id)
{
	var cost = document.getElementById(id+'cost').innerHTML;
	var price = document.getElementById(id+'price').innerHTML;
	var product_type = document.getElementById(id+'product_type').innerHTML;
	var minutes = document.getElementById(id+'minutes').innerHTML;
	var name = document.getElementById(id+'product_name').innerHTML;
	var quantity = document.getElementById(id+'quantity').innerHTML;
	var description = document.getElementById(id+'description').innerHTML;
	var category_id = document.getElementById(id+'category_id').innerHTML;
	var category_name = document.getElementById(id+'category_name').innerHTML;
	
	// if(product_type=="SERVICE")
	// {
		$('#divMinutes').show();
		$('#divQuantity').hide();
	// }
// 	
	// if(product_type=="INVENTORY")
	// {
		// $('#divMinutes').hide();
		// $('#divQuantity').show();
	// }
// 	
	// if(product_type =="NON-INVENTORY")
	// {
		// $('#divMinutes').hide();
		// $('#divQuantity').hide();
	// }
	
	
	$('#txtCost').val(cost);
	$('#txtName').val(name);
	$('#txtPrice').val(price.replace("$", ""));
	$('#txtProductId').val(id);
	$('#txtProductType').val(product_type);
	$('#txtDescription').val(description.trim());
	$('#txtMinutesOfService').val(minutes);
	$('#txtQuantity').val(quantity);
	$('#txtCategory').val(category_id);
	
	$('#dlgEditService').modal('show');
}

function delete_service(id)
{
	if (!confirm("Are you sure to want to delete this service?")) {
	    return false;
	}else{
		var postdata = "product_id="+id;
		showloading();
		$.postJSON("?action=delete_service", postdata, function(data) {
	     	if(data.success)
	        {
	        	closeloading();
	        	//CustomAlert(data.message, "Service Deleted");
	        	var delay=1000; //3 second
                setTimeout(function() {
                    window.location.href = data.redirect_url;    
                }, delay);
	        }else{
	        	closeloading();
	        	CustomAlert(data.message, "Error Deleting", "error");
	        }
	        
	    });
	}

}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});