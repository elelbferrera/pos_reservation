$(document).ready(function() {
	$( "#txtContactNumber" ).autocomplete({
		delay: 1000,
	  	source: function(request, response) {
        	$.getJSON('?action=search_all_customer', { term: request.term }, response);
    	},
    	minLength: 12,
	 	select: function( event, ui ) {
		  	var contact_number = $('#txtContactNumber').val();	
		  	console.log("do not show mobile");
		  	event.preventDefault();
		  	var item = ui.item['value'].split(",");

		  	$('#txtFirstName').val(item[0]);
		  	$('#txtLastName').val(item[1]);
		  	$('#txtEmailAddress').val(item[2]);
	  	},
		focus: function(event, ui) {
			event.preventDefault();
			return false;
		},
		search: function( event, ui ) {
			$(this).addClass('customer-loader');
			/*$(this).data('ui-autocomplete')._renderItem  = function (ul, item) {
		      return $("<li>")
		        .addClass("Please work")
		        .attr("data-value", item.value)
		        .append("Loading")
		        .appendTo(ul);
		    }*/
		},
		response: function(event, ui) {
			$(this).removeClass('customer-loader');
		}
	});
	$( "#txtContactNumber" ).autocomplete( "option", "appendTo", ".eventInsForm" );
	
	/*$('#example2').dataTable({
	      "paging": true,
	      "lengthChange": false,
	      "searching": false,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false
	});*/
	var monkeyList = new List('paginate-table', {
	  valueNames: ['customer-name'],
	  page: 3,
	  pagination: true
	});

	$('#txtContactNumber').mask("999-999-9999");
	
	// $('#btnUpdateCheckIn').click(function (){
		
		
	// });

	$('#txtStatus').change(function (){
		var status = $('#txtStatus').val();
		if(status =="CANCELLED")
		{
			/*$('#btnFinalizeFirst').show();*/
			$('#btnSelectService').hide();
		}else{
			$('#btnFinalizeFirst').hide();
			$('#btnSelectService').show();
		}
		
	});
	
	$('#btnSendSMS').click(function()
	{
		var message = $('#txtMessage').val();

		if(message.trim() =="")
		{
			CustomAlert("Unable to send SMS. Please enter your message.", "Error Sending",  "error");
			return false;
		}

		var postdata = $("#frmSendMessage").serialize();

		showloading();
    	$.postJSON("?action=send_sms", postdata, function(data) {
            if(data.success)
            {
            	closeloading();
            	$('#dlgMessageCustomer').modal('hide');
            	CustomAlert(data.message, "Message Sent");
            	var delay=1000; //3 second
	            setTimeout(function() {
	                $('#dlg_message').modal('hide');
	            }, delay);
            	// window.location.href = window.location.href;
            }else{
            	closeloading();
            	CustomAlert(data.message, "Error Sending", "error");
            	var delay=1000; //3 second
	            setTimeout(function() {
	                $('#dlg_messageerror').modal('hide');
	            }, delay);
            }
        });
		return false;

	});


});

function updateBasedFromStatus() {
	if ($('#txtStatus').val() == 'SEATED') {
		gotoSpecialist();
		$('#dlgEditCheckIn').modal('hide');
	} else {
		finalize();
	}
}


function gotoServices()
{	
	$('input').removeClass('input-error');

	var status = $('#txtStatus').val();
	if(status =="SEATED")
	{
		$('#btnSpecialist').show();
		$('#btnUpdateCheckIn').hide();
		
	}else{
		var first_name		= $("#txtFirstName").val();
		var last_name 		= $("#txtLastName").val();
		var contact_number	= $("#txtContactNumber").val();

		var valid = true;

		if(first_name.trim()=="")
    	{
    		/*CustomAlert("First name is required.",  "Unable to Select Service", "error");
    		return false;*/
    		$("#txtFirstName").addClass('input-error');
    		valid = false;
    	}

		if(last_name.trim() == "")
    	{
    		/*CustomAlert("Last name is required.", "Unable to Select Service", "error");
    		return false;*/
    		$("#txtLastName").addClass('input-error');
    		valid = false;
    	}
    	
    	if(contact_number.trim() =="")
    	{
    		/*CustomAlert("Contact Number is required.",  "Unable to Select Service", "error");
    		return false;*/
    		$("#txtContactNumber").addClass('input-error');
    		valid = false;
    	}

    	if (!valid) {
    		return false;
    	}
    	
		$('#btnSpecialist').hide();
		$('#btnUpdateCheckIn').show();
	}
	
	$('#dlgEditCheckInService').modal('show');
	$('#dlgEditCheckIn').modal('hide');
	return false;
}

function gotoSpecialist()
{

	var checkboxes = document.getElementsByName("services[]");
	var services ="";
    for (var i=0; i<checkboxes.length; i++) {
	   	if(checkboxes[i].checked)
	   	{  
			services = services + checkboxes[i].value +",";
		}
	}
	
	if(services.length==0)
	{
		CustomAlert("Please select atleast one services.", "Unable to Save Wait List", "error");
		return false;
	}

	services = services.substr(0, services.length-1);
	// $('#txtProducts').val(services);	

	

	showloading();
	var postdata =  "&services="+services+"&date="+$('#txtSourceDate').val();
	$.postJSON("?action=get_salon_specialist", postdata, function(data) {
	    if(data.success)
	    {
	    	// url = url.replace("#","");
			// txtSourceFirstName
			// txtSourceLastName
			// $("#txtSourceFirstName").val(data.first_name);
			// $("#txtSourceLastName").val(data.last_name);
			// $("#txtSourceMobile").val(data.mobile);
			document.getElementById("divSpecialist").innerHTML = data.message;
	    }else{
	    	// CustomAlert(data.message, "Unable to Find Customer", "error");
	  //   	$("#txtSourceFirstName").val("");
			// $("#txtSourceLastName").val("");
			// $("#txtSourceMobile").val("");
			document.getElementById("divSpecialist").innerHTML = data.message;
	    }
	    closeloading();
	});
	// return false;


	$('#dlgEditCheckInService').modal('hide');
	$('#dlgEditCheckInSpecialist').modal('show');
	return false;
}

function goBackToServices()
{
	$('#dlgEditCheckInService').modal('show');
	$('#dlgEditCheckInSpecialist').modal('hide');
	return false;
}

function gotoInitial()
{
	$('#dlgEditCheckInService').modal('hide');
	$('#dlgEditCheckIn').modal('show');
	return false;
}

function finalize()
{

		$(".error-prompt").hide();
		$("#error-holder").html();
		var valid = true; // checker of validation
		var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
		var error_return = '';
		

		var checkboxes = document.getElementsByName("services[]");
		var services ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				services = services + checkboxes[i].value +",";
			}
		}
		
		if(services.length==0)
		{
			/*CustomAlert("Please select atleast one services.", "Unable to Save Wait List", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Service is required</span><br>';
			valid = false;
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
		}

		services = services.substr(0, services.length-1);

		$('#txtProducts').val(services);	


		var checkboxes = document.getElementsByName("specialist[]");
		var specialist ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				specialist = specialist + checkboxes[i].value +",";
			}
		}
		
		if(specialist.length==0)
		{
			// CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
			// return false;
		}else{
			specialist = specialist.substr(0, specialist.length-1);	
		}
		
		
		$('#txtSpecialist').val(specialist);

		var postdata = $("#frmCheckInStatus").serialize();

		if($('#txtCheckInId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
	            	window.location.href = window.location.href;
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error updating", "error");
	            } 
	        });
	        
	    }else{
	    	
	    	var last_name = $('#txtFirstName').val();
	    	var first_name = $('#txtLastName').val();
	    	var contact_number = $('#txtContactNumber').val();
	    	
	    	contact_number.replace("-", "");
	    	contact_number.replace("-", "");
	    	
	    	if(last_name.trim() == "")
	    	{
	    		CustomAlert("Last name is required.", "Unable to Save", "error");
	    		return false;
	    	}
	    	
	    	if(first_name.trim()=="")
	    	{
	    		CustomAlert("First name is required.",  "Unable to Save", "error");
	    		return false;
	    	}
	    	
	    	if(contact_number.trim() =="")
	    	{
	    		CustomAlert("Contact Number is required.",  "Unable to Save", "error");
	    		return false;
	    	}
	    	
	    	
	    	
	    	postdata = postdata + "&txtContactNumber=" + contact_number;
	    	
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	closeloading();
	            	//CustomAlert(data.message, "Record Save");
	            	window.location.href = window.location.href;
	            }else{
	            	closeloading();
	            	CustomAlert(data.message, "Error Saving", "Error");
	            }
	            
	        });
	        
	    }
	    return false;
}


function show_send_sms(id)
{
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var contact_number = document.getElementById(id+'contact_number').innerHTML;

	document.getElementById('pSendTo').innerHTML = "Sending message to " + first_name + " " + last_name;

	$('#txtMessageLastName').val(last_name.trim());
	$('#txtMessageFirstName').val(first_name.trim());
	$('#txtMessageContactNumber').val(contact_number);
	$('#txtMessage').val("");
	
	$('#dlgMessageCustomer').modal('show');
}

function add_waiting()
{	
	$('#txtContactNumber').unmask();
	$('#btnUpdateFirst').hide();	
	$('#txtLastName').val("");
	$('#txtFirstName').val("");
	$('#txtContactNumber').val("");
	$('#txtStatus').val("WAITING");
	$('#txtCheckInId').val("");
	$('#txtCheckInId').val("");

	$("input[name='services[]']").removeAttr("checked");
	
	$('#divStatus').hide();
	
	 $("#txtLastName").prop('readonly', false);
	 $("#txtFirstName").prop('readonly', false);
	 $("#txtContactNumber").prop('readonly', false);
	 $('#txtContactNumber').mask("999-999-9999", {selectOnFocus: true});
	 $("#txtEmailAddress").prop('readonly', false);
	
	$('#dlgEditCheckIn').modal('show');
}


function edit_waiting(id)
{
	$('#btnUpdateFirst').show();
	$('input').removeClass('input-error');
	
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var contact_number = document.getElementById(id+'contact_number').innerHTML;
	var status = document.getElementById(id+'status').innerHTML;
	var emal_address = document.getElementById(id+'email_address').innerHTML;

	var product_ids = document.getElementById(id+'product_ids').innerHTML+",";

	var checkboxes = document.getElementsByName("services[]");
	for (var i=0; i<checkboxes.length; i++) {
	     if(product_ids.includes(","))
		 {
	     	var n = product_ids.includes(checkboxes[i].value+",");
	     }else{
	     	var n = product_ids.includes(checkboxes[i].value);
	     }
	     
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}

	
	
	$('#divStatus').show();
	$('#txtLastName').val(last_name.trim());
	$('#txtFirstName').val(first_name.trim());
	$('#txtContactNumber').val(contact_number);
	$('#txtStatus').val(status);
	$('#txtEmailAddress').val(emal_address);
	$('#txtCheckInId').val(id);
	
	$("#txtLastName").prop('readonly', true);
 	$("#txtFirstName").prop('readonly', true);
 	$("#txtContactNumber").prop('readonly', true);
	
	
	$('#dlgEditCheckIn').modal('show');
}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});