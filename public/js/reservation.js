$(document).ready(function() {

	window.onload = startInterval;

	var date = new Date();

 	var d = date.getDate();
 	if(d <= 9)
 	{
 		d = "0" + d;
 	}
 	var m = date.getMonth() + 1;
 	var y = date.getFullYear();

 	//var body_height = $('.fc-time-grid').height();
 	var body_height = $('.fc-bg').height();

 	$('[data-encode]').each(function(key, value){
 		var elementNum = key + 2;
		var sched = jQuery.parseJSON($(this).attr('data-encode'));
		var day = date.getDay();
		var day_word = "";
		var is_off = "";

		if (day == 0) {
			is_off = sched[6].is_off;
			day_word = sched[6].day;

		} else if (day > 0) {
			is_off = sched[day-1].is_off;
			day_word = sched[day-1].day;
		}

		if (is_off == 1) {
			$('.fc-content-skeleton tr td:nth-child('+elementNum+')').addClass('fc-disabled').append('&nbsp;').css('height', body_height);
		}
	});

	$('#btnrefresh').click(function (){
		// $('#calendar').fullCalendar('removeEvents');
		showloading();
		$.postJSON("?action=get_refresh_events&date=2017-02-21", "", function(data) {
				$("#calendar").fullCalendar('removeResource', 4);

				$("#calendar").fullCalendar('removeEvents');
				$("#calendar").fullCalendar('addEventSource', data.message);
				$("#calendar").fullCalendar('rerenderEvents');

            closeloading();
        });


		// $('#calendar').fullCalendar({
  //   		events: ''
		// });

	});

    $('#txtMobileSearch').mask("999-999-9999", {selectOnFocus: true});
    $('#txtSourceMobile').mask("999-999-9999", {selectOnFocus: true});
    
    $('.datepicker').datepicker({
       	autoclose: true,
    });

    $('.updateReservationDate2').datepicker({
    	autoclose: true,
    	format : "mm-dd-yyyy",
    	startDate: "0d"
    });

    $(".timepicker").timepicker({
      showInputs: false
    });
	    
   $('#btnSearchCustomer').click(function (){
   		$(".error-prompt").hide();
		$(".error-holder").html();

		var valid = true; // checker of validation
		var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
		var error_return = '';

   	 	$('#rsrvationDateHolder').hide();
   	
   		//frmSearch
   		var postdata = $("#frmSearch").serialize();
   		showloading();
   		$.postJSON("?action=search", postdata, function(data) {
            if(data.success)
            {
            	// url = url.replace("#","");
				// txtSourceFirstName
				// txtSourceLastName
				$("#txtSourceFirstName").val(data.first_name);
				$("#txtSourceLastName").val(data.last_name);
				// $("#txtSourceMobile").val(data.mobile);
				$("#txtSourceEmail").val(data.email_address);

				$("#txtSourceMobile").val($('#txtMobileSearch').val());
				document.getElementById("lblSourceMobile").innerHTML = $('#txtMobileSearch').val();
				document.getElementById("lblSourceFullName").innerHTML = data.first_name + " " + data.last_name;
				$('#divCustomerInformation').show();
				$('#divNewCutomer').hide();
				// $('#divSearchCustomer').show();
				document.getElementById('lblNewCustomer').innerHTML = "New Customer";
            }else{
            	//CustomAlert(data.message, "Unable to Find Customer", "error");
            	$("#txtSourceFirstName").val("");
				$("#txtSourceLastName").val("");
				$("#txtSourceMobile").val("");
				$("#txtSourceEmail").val("");
				error_return += error_default+'<span class="error-msg"> Unable to Find Customer</span><br>';
				valid = false;
				$(".error-holder").html(error_return);
				$(".error-prompt").show();

				var exist_search_no = $('#txtMobileSearch').val();
				
				$('#txtSourceMobile').val(exist_search_no).html(exist_search_no);
				$('#divCustomerInformation').hide();
				document.getElementById('lblNewCustomer').innerHTML = "Existing Customer";
				$('#divCustomerInformation').hide();
				$("#txtSourceFirstName").val("");
				$("#txtSourceLastName").val("");
				
				$("#txtSourceEmail").val("");
				$('#divNewCustomer').show();
				//return false;
            }

            $("#txtMobileSearch").val("");
            closeloading();
        });
        return false;
   });
	
	$('#btnFinish').click(function (){
		
		var branch_id = $('#txtSourceBranch').val();
		var first_name = $('#txtSourceFirstName').val();
		var last_name = $('#txtSourceLastName').val();

		var mobile = $('#txtSourceMobile').val();
		var status_id = $('#txtSourceStatus').val();
		// var source_date = $('#txtSourceDate').val();
		var source_date = ($('#check_edit').val() == "1") ? $('#updateReservationDate2').val() : $('#txtSourceDate').val();
		var source_time = ($('#check_edit_time').val() == "1") ? $('#updateReservationTime2').val() : $('#txtSourceTime').val();
		//var source_time = $('#txtSourceTime').val();


		// alert(source_date);
		// return false;
		// var new_date = new Date(source_date);

		// var d = new_date.getUTCDate();
	 // 	if(d <= 9)
	 // 	{
	 // 		d = "0" + d;
	 // 	}
	 // 	var m = new_date.getMonth() + 1;

	 // 	if (m < 10) {
	 // 		m = "0" + m;
	 // 	}
	 // 	var y = new_date.getFullYear();

	 	// source_date = y+'-'+m+'-'+d;
		
		var notes = $('#txtSourceNotes').val();
		var email = $('#txtSourceEmail').val();
		
		
		// var opening_time = $('#txtOpeningTime').val();
		// var closing_time = $('#txtClosingTime').val();
		
		// if(branch_id.trim()=="")
		// {
			// CustomAlert("Branch is required.", "Unable to Save Reservation", "error");
			// return false;
		// }
		$(".error-prompt").hide();
		$(".error-holder").html();

		var valid = true; // checker of validation
		var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
		var error_return = '';

		var checkboxes = document.getElementsByName("services[]");
		var services ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				services = services + checkboxes[i].value +",";
			}
		}	
		
		services = services.substr(0, services.length-1);

		if(services.length==0)
		{
			//CustomAlert("Please select atleast one services.", "Unable to Save Reservation", "error");
			error_return += error_default+'<span class="error-msg"> Service is required</span><br>';
			valid = false;
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
		}


		$('#txtProducts').val(services);
		
		var checkboxes = document.getElementsByName("specialist[]");
		var specialist ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				specialist = specialist + checkboxes[i].value +",";
			}
		}
		
		if(specialist.length==0)
		{
			// CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
			// return false;
		}else{
			specialist = specialist.substr(0, specialist.length-1);	
		}
		// alert(specialist);
		// return false;
		
		$('#txtSpecialist').val(specialist);
		
		// alert(convertTo24Hour(source_time));
		// alert(source_time);
		// return false;
		
		$('#txtTime').val(convertTo24Hour(source_time));
		


		// date = source_date.split("-");
		var new_date = source_date.split("-");
		source_date = new_date[2] + "-" + new_date[0] + "-" + new_date[1];

		
		$('#txtDate').val(source_date);
		// $('#txtBranch').val(branch_id);

		$('#txtBranch').val("-1");		
		$('#txtLastName').val(last_name);
		$('#txtFirstName').val(first_name);
		
		mobile = mobile.replace("-", "");
		mobile = mobile.replace("-", "");
		
		$('#txtMobile').val(mobile);
		$('#txtNotes').val(notes);
		$('#txtStatus').val(status_id);
		$('#txtEmail').val(email);
		
		
		var postdata = $("#frmReservation").serialize();


		var first_name = $('#txtSourceFirstName').val();
		var last_name = $('#txtSourceLastName').val();

		var mobile = $('#txtSourceMobile').val();
		var email = $('#txtSourceEmail').val();
		var status_id = $('#txtSourceStatus').val();
		var source_date = ($('#check_edit').val() == "1") ? $('#updateReservationDate2').val() : $('#txtSourceDate').val();
		var source_time = ($('#check_edit_time').val() == "1") ? $('#updateReservationTime2').val() : $('#txtSourceTime').val();
		/*var source_date = $('#txtSourceDate').val();
		var source_time = $('#txtSourceTime').val();*/

		var notes = $('#txtSourceNotes').val();

		if(first_name.trim()=="")
		{
			/*CustomAlert("First Name is required.", "Unable to Save Reservation", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> First Name is required</span><br>';
			valid = false;
		}
		if(last_name.trim()=="")
		{
			/*CustomAlert("Last Name is required.", "Unable to Save Reservation", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Last Name is required</span><br>';
			valid = false;
		}
		
		if(mobile.trim()=="")
		{
			/*CustomAlert("Mobile no. is required.", "Unable to Save Reservation", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Mobile no. is required</span><br>';
			valid = false;
		}
		
		if(source_date=="")
		{
			/*CustomAlert("Date is required.", "Unable to Save Reservation", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Date is required</span><br>';
			valid = false;
		}
		
		if(source_time=="")
		{
			/*CustomAlert("Time is required.", "Unable to Save Reservation", "error");
			return false;*/
			error_return += error_default+'<span class="error-msg"> Time is required</span><br>';
			valid = false;
		}

		if (!valid) {
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
			return false;
		}
		
		
		if($('#txtReservationId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	$('#check_edit').val("");
	            	$('#check_edit_time').val("");
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	
	            	/*CustomAlert(data.message, "Update Reservation");*/
	            	
	            	var date = $('#txtSelectedDate').val();
				    $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
							$("#calendar").fullCalendar('removeEvents');
							$("#calendar").fullCalendar('addEventSource', data.message);
							$("#calendar").fullCalendar('rerenderEvents');
			            // closeloading();
			        });
				    closeloading();
				    $('#dlgEditReservationStep1').modal('hide');
            		$('#editDateTimeModal').modal('hide');
				    
				    var delay=1000; //3 second
		            setTimeout(function() {
		                $('#dlg_message').modal('hide');
		            }, delay);

				    $('#dlgEditReservationStep1').modal('hide');
            		$('#editDateTimeModal').modal('hide');
	            	// window.location.href = url;
	            }else{
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
					valid = false;
					$(".error-holder").html(error_return);
					$(".error-prompt").show();
	            	//CustomAlert(data.message, "Unable to Update Reservation", "error");
	            	//window.location.href = url;
	      //       	var date = $('#txtSelectedDate').val();
				   //  $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
							// $("#calendar").fullCalendar('removeEvents');
							// $("#calendar").fullCalendar('addEventSource', data.message);
							// $("#calendar").fullCalendar('rerenderEvents');
			    //         // closeloading();
			    //     });
			    //     $('#dlgEditReservationStep1').modal('show');
	            }
	            closeloading();
	            return false;
	        });
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	    		console.log(data);
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	//CustomAlert(data.message, "Reservation Created");
	            	var date = $('#txtSelectedDate').val();
				    $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
							$("#calendar").fullCalendar('removeEvents');
							$("#calendar").fullCalendar('addEventSource', data.message);
							$("#calendar").fullCalendar('rerenderEvents');
			            // closeloading();
			        });
			        closeloading();
					var delay=1000; //3 second
					setTimeout(function() {
						$('#dlg_message').modal('hide');
					}, delay);
			        $('#dlgEditReservationStep1').modal('hide');
	            	// window.location.href = url;
	            }else{
	            	//CustomAlert(data.message, "Unable to Save Reservation", "error");
	            	error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
					valid = false;
					$(".error-holder").html(error_return);
					$(".error-prompt").show();
	            }
	            /*$(".error-prompt").hide();
				$(".error-holder").html();*/
	            closeloading();
	            return false;
	        });
	        
	    }
	    
	});
	
});


function update_me()
{
	var first_name = $('#txtSourceFirstName').val();
		var last_name = $('#txtSourceLastName').val();

		var mobile = $('#txtSourceMobile').val();
		var status_id = $('#txtSourceStatus').val();
		var source_date = $('#txtSourceDate').val();
		var source_time = $('#txtSourceTime').val();
		
		var notes = $('#txtSourceNotes').val();
		
		// var opening_time = $('#txtOpeningTime').val();
		// var closing_time = $('#txtClosingTime').val();
		
		// if(branch_id.trim()=="")
		// {
			// CustomAlert("Branch is required.", "Unable to Save Reservation", "error");
			// return false;
		// }
		// var checkboxes = document.getElementsByName("services[]");
		// var services ="";
	 //    for (var i=0; i<checkboxes.length; i++) {
		//    	if(checkboxes[i].checked)
		//    	{  
		// 		services = services + checkboxes[i].value +",";
		// 	}
		// }	
		
		// services = services.substr(0, services.length-1);
		// $('#txtProducts').val(services);
		
		var checkboxes = document.getElementsByName("specialist[]");
		// alert(checkboxes);
		// return false;
		var specialist ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				specialist = specialist + checkboxes[i].value +",";
			}
		}
		
		if(specialist.length==0)
		{
			// CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
			// return false;
		}else{
			specialist = specialist.substr(0, specialist.length-1);	
		}
		
		// alert(specialist);
		// return false;
		$('#txtSpecialist').val(specialist);
		
		// alert(convertTo24Hour(source_time));
		// alert(source_time);
		// return false;
		
		$('#txtTime').val(convertTo24Hour(source_time));
		
		
		$('#txtDate').val(source_date);
		// $('#txtBranch').val(branch_id);

		$('#txtBranch').val("-1");		
		$('#txtLastName').val(last_name);
		$('#txtFirstName').val(first_name);
		
		mobile = mobile.replace("-", "");
		mobile = mobile.replace("-", "");
		
		$('#txtMobile').val(mobile);
		$('#txtNotes').val(notes);
		$('#txtStatus').val(status_id);
		
		
		var postdata = $("#frmReservation").serialize();
		
		
		
		if($('#txtReservationId').val()>0)
		{
			showloading();
			// alert(postdata);
			// return false;
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	if (data.success == "na") {
	            		closeloading();
	            		CustomAlert("Specialist Off Duty", "Unable to Update Reservation", "error");
	            	} else {
		            	var url = window.location.href;
		            	url = url.replace("#",""); 

		            	/*CustomAlert(data.message, "Update Reservation");*/
		            	var date = $('#txtSelectedDate').val();
					    $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
								$("#calendar").fullCalendar('removeEvents');
								$("#calendar").fullCalendar('addEventSource', data.message);
								$("#calendar").fullCalendar('rerenderEvents');
				            // closeloading();
				        });
					    closeloading();

					    var delay=1000; //3 second
			            setTimeout(function() {
			                $('#dlg_message').modal('hide');
			            }, delay);

					    $('#dlgEditReservationStep1').modal('hide');

		            	// closeloading();
		            	
		            	// window.location.href = url;
		            }
	            }else{
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	closeloading();
	            	CustomAlert(data.message, "Unable to Update Reservation", "error");
	            	/*error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
					valid = false;
					$(".error-holder").html(error_return);
					$(".error-prompt").show();*/
	            	// window.location.href = url;
	            	return 0;
	            }
	            closeloading();
	            return false;
	        });
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	// closeloading();
	            	//CustomAlert(data.message, "Reservation Created");
	            	var date = $('#txtSelectedDate').val();
				    $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
							$("#calendar").fullCalendar('removeEvents');
							$("#calendar").fullCalendar('addEventSource', data.message);
							$("#calendar").fullCalendar('rerenderEvents');
			            // closeloading();
			        });
				    closeloading();

				    var delay=1000; //3 second
		            setTimeout(function() {
		                $('#dlg_message').modal('hide');
		            }, delay);

				    $('#dlgEditReservationStep1').modal('hide');

	            	// window.location.href = url;
	            	return 1;
	            }else{
	            	closeloading();
	            	//CustomAlert(data.message, "Unable to Save Reservation", "error");
	            	error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
					valid = false;
					$(".error-holder").html(error_return);
					$(".error-prompt").show();
	            	return 0;
	            }
	            
	            return false;
	        });
	        
	    }
}


function add(datetime, specialist_id, current_time, specialist_name, sched)
{
	$(".error-prompt").hide();
	$(".error-holder").html();
	if (sched !== "") {
		var sched = sched.split(",");

		if (sched[2] == 1) {
			CustomAlert("Specialist is off duty.", "Not available", "error");
			return false;
		} /*else {
			var hours = current_time.getUTCHours();
	 	
		 	if(hours < 10)
		 	{
		 		hours = "0" + hours;
		 	}
		 	
		 	var minutes = current_time.getUTCMinutes();
		 	if(minutes < 10)
		 	{
		 		minutes = "0" + minutes;
		 	}
		 	
		 	var selected_time = hours + ":" + minutes;
		 
		 	if (selected_time < sched[0] || selected_time > sched[1]) {
		 		alert('error'); return false;
		 	}
		}*/
	}

	$('#check_edit').val(0);
	$('#check_edit_time').val(0);
	$('#divNewCustomer').hide();
	$('#divCustomerInformation').hide();
	$('#divSearchCustomer').show();
	document.getElementById('lblNewCustomer').innerHTML = "New Customer";

	$('#txtSourceTime').val("");
	$('#txtSourceDate').val("");
	$('#txtSourceBranchId').val("");
	$('#txtReservationId').val("-1");
	$('#txtSourceLastName').val("");
	$('#txtSourceFirstName').val("");
	$('#txtSourceMobile').val("");
	$('#txtSourceNotes').val("");

	$('#txtSourceStatus').val("1"); //SCHEDULED

	$('#txtSpecialistId').val(specialist_id);


	// alert(specialist_id);


	


	// $('#txtSpecialist').val(specialist);

	showloading();
	var postdata =  "&specialist="+specialist_id;
	$.postJSON("?action=get_services_by_specialist", postdata, function(data) {
		closeloading();
	    // if(data.success)
	    // {
			document.getElementById("divServices").innerHTML = data.message;
	    // }else{
			// document.getElementById("divServices").innerHTML = data.message;
	    // }
	    
	});

	

	if(datetime != undefined)
	{
		
		var date = new Date(datetime);

		/*var d = date.getDate();*/
	 	var d = date.getUTCDate();
	 	if(d <= 9)
	 	{
	 		d = "0" + d;
	 	}
	 	var m = date.getMonth() + 1;
	 	var y = date.getFullYear();
	 	//var time = date.getTime();
	 	// date.getTime
	 	// var str = datetime.toString;
	 	// var n = str.  
	 	//alert(date.getUTCHours());
	 	// var hours = date.getUTCHours();
	 	var hours = current_time.getUTCHours();
	 	
	 	
	 	if(hours < 10)
	 	{
	 		hours = "0" + hours;
	 	}
	 	
	 	// var minutes = date.getUTCMinutes();
	 	var minutes = current_time.getUTCMinutes();
	 	if(minutes < 10)
	 	{
	 		minutes = "0" + minutes;
	 	}
	 	
	 	var time = hours + ":" + minutes;
	 	
	 	// alert(time);

	 	if (time >= sched[1]) {
	 		CustomAlert("Specialist is unavailable on selected time.", "Specialist", "error");
	 		return false;
	 	}
	 	
	 	time = convertToAMPM(time);
	 	
	 	if(m < 10)
	 	{
	 		m = "0" + m;
	 	}
	 	
	 	// var datenow = y + "-" + m + "-" + d;
	 	var datenow = m + "-" + d + "-" + y;

	 	// alert(datenow);
	 	// return false;
	 	
	 	$('#txtSourceTime').val(time);
		$('#txtSourceDate').val(datenow);
		
	}else{
		datenow ="";
		time ="";
	}
	var checkboxes = document.getElementsByName("services[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	var checkboxes = document.getElementsByName("specialist[]");
    for (var i=0; i<checkboxes.length; i++) {
	     
	     if(specialist_id == undefined)
	     {
	     	checkboxes[i].checked = false;
	     }else{
	     	if(specialist_id == checkboxes[i].value)
	     	{
	     		checkboxes[i].checked = true;
	     	}else{
	     		checkboxes[i].checked = false;
	     	}
	     }
	}
	
	var new_date = new Date(datenow);
	// alert(datenow);
	

	m = parseInt(m);
	// var month_name = function(dt){
	var month_name = function(dt){
		mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	  	// return mlist[dt.getMonth()];
	  	return mlist[dt-1];
	}

	// var m = month_name(new Date(datenow));
	var m = month_name(m);
	// alert(m);
	// return false;
    // alert(new_date.getDate());
    // return false;
    var sched_caption = " Scheduling with " + specialist_name + " on ";
	// sched_caption = sched_caption + m + " " + new_date.getUTCDate() + ", " + new_date.getFullYear() + " at " + time;
	sched_caption = sched_caption + m + " " + d + ", " + y + " at " + time;

	document.getElementById('lblTitleCaption').innerHTML =  "New Reservation";
	

	document.getElementById('lblSelectedSpecialist').innerHTML =  sched_caption;

	var date_today = new Date();
	date_today.setHours(0,0,0,0);
	var selected_date = new Date(new_date.getFullYear()+"-"+m+"-"+new_date.getUTCDate());
	var d = date_today.getUTCDate();
 	if(d <= 9)
 	{
 		d = "0" + d;
 	}
 	var m = date_today.getMonth() + 1;
 	if(m < 10)
 	{
 		m = "0" + m;
 	}
	 	
 	var y = date_today.getFullYear();

 	date_today = new Date(y + "-" + m + "-" + d);

 	//orig code for disabling date
 	// if (selected_date < date_today) {
 	// 	CustomAlert("Unable to create new reservation.", "Date disabled");
 	// 	return false;
 	// }
 	//

 	//for disabling date if date and time already passed - el
 	var selectedTime = new Date(current_time - 8*60*60*1000);
 	var myTime = new Date();
 	console.log(selectedTime + " -- " + myTime);
 	

 	if (selectedTime < myTime) {
 		CustomAlert("Unable to create new reservation.", "Date disabled");
 		return false;
 	}
 	// end

	$('#dlgEditReservationStep1').modal('show');
}


function editfromDrop(id, reservation_date, reservation_time, sale_person_ids, event)
{
	var reservation_id = id;

	// alert(id);
	// var last_name = document.getElementById(id+'last_name').innerHTML;
	// var first_name = document.getElementById(id+'first_name').innerHTML;
	// var mobile = document.getElementById(id+'mobile').innerHTML;
	// // var reservation_date = document.getElementById(id+'reservation_date').innerHTML;
	// // var reservation_time = document.getElementById(id+'reservation_time').innerHTML;
	// var product_ids = document.getElementById(id+'product_ids').innerHTML;
	// // alert(product_ids);
	// // return false;
	// // var sale_person_ids = document.getElementById(id+'sale_person_ids').innerHTML;
	// var branch_id = document.getElementById(id+'branch_id').innerHTML;
	// var reservation_status_id = document.getElementById(id+'reservation_status_id').innerHTML;
	// var notes = document.getElementById(id+'notes').innerHTML;

	var last_name = event.last_name;
	var first_name = event.first_name;
	var mobile = event.mobile;
	var product_ids = event.product_ids;
	var branch_id = event.branch_id;
	var reservation_status_id = event.reservation_status_id;
	var notes = event.notes;
	
	// alert(sale_person_ids);
	// return false;
	$('#txtSpecialistId').val('');


	// alert(product_ids);
	product_ids = product_ids.substr(0, product_ids.length-1);	
	$('#txtProducts').val(product_ids);


	// alert(sale_person_ids);
	// return false;
	$('#txtSpecialist').val(sale_person_ids);
	
	var reservation_date = reservation_date.substring(0, 10); 
	
	time = convertTo24Hour(reservation_time);
	
	// alert(time);
	// return false;
	$('#txtReservationId').val(reservation_id);
	$('#txtSourceTime').val(time);
	$('#txtSourceDate').val(reservation_date);
	$('#txtSourceBranchId').val(branch_id);
	$('#txtSourceStatus').val(reservation_status_id);
	$('#txtReservationId').val(reservation_id);
	$('#txtSourceLastName').val(last_name);
	$('#txtSourceFirstName').val(first_name);
	
	// $('#txtSourceMobile').masked(mobile);
	$('#txtSourceMobile').val(mobile);



	//$('#txtSourceMobile').mask("999-999-9999");
	$('#txtSourceNotes').val(notes.trim());
	
	
	
	// var product_ids = store_day+",";
	
	var checkboxes = document.getElementsByName("services[]");
	for (var i=0; i<checkboxes.length; i++) {
	     if(product_ids.includes(","))
		 {
	     	var n = product_ids.includes(checkboxes[i].value+",");
	     }else{
	     	var n = product_ids.includes(checkboxes[i].value);
	     }
	     
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}


	
	sale_person_ids = "," + sale_person_ids + ",";
	var checkboxes = document.getElementsByName("specialist[]");
	for (var i=0; i<checkboxes.length; i++) {

		 if(sale_person_ids.includes(","))
		 {
	     	var n = sale_person_ids.includes("," + checkboxes[i].value + ",");
	  //    	alert("lems1");
			// return false;
	     }else{
	     	var n = sale_person_ids.includes(checkboxes[i].value);
	     	// alert("lems2");
			// return false;
	     }
	     if(n)
	     {
	     	// alert("check");
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	

	// $("#btnFinish").trigger("click");
}





function edit(id, event)
{
	console.log(event);
	$(".error-prompt").hide();
	$(".error-holder").html();
	// alert(specialist_name);
	// return false;
	$('#divNewCutomer').show();
	$('#divCustomerInformation').show();	
	$('#divSearchCustomer').hide();
	var reservation_id = id;
	// var last_name = document.getElementById(id+'last_name').innerHTML;
	// var first_name = document.getElementById(id+'first_name').innerHTML;
	// var mobile = document.getElementById(id+'mobile').innerHTML;
	// var reservation_date = document.getElementById(id+'reservation_date').innerHTML;
	// var reservation_time = document.getElementById(id+'reservation_time').innerHTML;
	// var product_ids = document.getElementById(id+'product_ids').innerHTML;
	// var sale_person_ids = document.getElementById(id+'sale_person_ids').innerHTML;
	// var branch_id = document.getElementById(id+'branch_id').innerHTML;
	// var reservation_status_id = document.getElementById(id+'reservation_status_id').innerHTML;
	// var notes = document.getElementById(id+'notes').innerHTML;

	var last_name = event.last_name;
	var first_name = event.first_name;
	var mobile = event.mobile;
	var reservation_date = event.reservation_date;
	var reservation_time = event.reservation_time;
	var reservation_end_time = event.reservation_end_time;
	var product_ids = event.product_ids;
	var sale_person_ids = event.sale_person_ids;
	var branch_id = event.branch_id;
	var reservation_status_id = event.reservation_status_id;
	var notes = event.notes;
	
	// alert(d);
	$('#txtSpecialistId').val("");
	// return false;
	// alert(event.status);
	// return false;
	
	var reservation_date = reservation_date.substring(0, 10); 
	
	time = convertToAMPM(reservation_time);
	console.log(reservation_end_time);
	end_time = convertToAMPM(reservation_end_time);
	mobile = mobile.substring(0,3) + '-' + mobile.substring(3,6) + '-' + mobile.substring(6,10);
	
	// alert(time);
	// return false;
	$('#txtReservationId').val(reservation_id);
	$('#txtSourceTime').val(time);
	$('#txtSourceDate').val(reservation_date);
	$('#txtSourceBranchId').val(branch_id);
	$('#txtSourceStatus').val(reservation_status_id);
	$('#txtReservationId').val(reservation_id);
	$('#txtSourceLastName').val(last_name);
	$('#txtSourceFirstName').val(first_name);
	$('#txtSourceMobile').val(mobile);
	$('#txtSourceNotes').val(notes.trim());

	$('#txtUpdateReservationId').val(reservation_id);
	document.getElementById('lblUpdateCustomerName').innerHTML = first_name + " " + last_name;
	document.getElementById('lblUpdateContactNumber').innerHTML = mobile;
	var status = $("#txtSourceStatus option:selected").text();
	document.getElementById('lblUpdateStatus').innerHTML = status;
	document.getElementById('lblUpdateNotes').innerHTML = notes.trim();

	document.getElementById('lblSourceFullName').innerHTML = first_name + " " + last_name;
	document.getElementById('lblSourceMobile').innerHTML = mobile;

	if(event.status =="SCHEDULED")
	{
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#btnCheckIn').show();
		$('#btnConfirm').show();
		$('#btnDone').show();
		$('#btnSendSMS').show();
	}

	if(event.status =="CANCELLED" || event.status=="DONE")
	{
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnCheckIn').hide();
		$('#btnConfirm').hide();
		$('#btnDone').hide();
		$('#btnSendSMS').hide();
	}

	if(event.status =="CHECKED IN")
	{
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#btnCheckIn').hide();
		$('#btnConfirm').show();
		$('#btnDone').show();
		$('#btnSendSMS').show();
	}

	if(event.status =="CONFIRMED")
	{
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#btnCheckIn').hide();
		$('#btnConfirm').hide();
		$('#btnDone').show();
		$('#btnSendSMS').show();
	}

	var new_date = new Date(reservation_date);

	var month_name = function(dt){
		mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	  	return mlist[dt.getMonth()];
	}

	var m = month_name(new Date(reservation_date));
	// used UTCDATE on date
	// alert(m);
	// return false;
    // alert(new_date.getDate());
    // return false;
    var  specialist_name = event.specialist;
    
    if(specialist_name == 'NA'){
    	var sched_caption = " Scheduled on ";
    }else{
    	var sched_caption = " Scheduled with " + specialist_name + " on ";
    }
    
	sched_caption = sched_caption + m + " " + new_date.getUTCDate() + ", " + new_date.getFullYear() + " at " + time  + ' - ' + end_time;
	
	$(".datepicker").datepicker({
		 beforeShowDay: function(date) {
		 var selectedDate =	new_date.getUTCDate();
	     var hilightedDays = [selectedDate];
	     console.log(selectedDate);
	     console.log(date.getDate());
	       if (hilightedDays.indexOf(date.getDate())) {
	          return {classes: 'highlight'};
	       }
	    }
	})

	document.getElementById('lblSelectedSpecialist').innerHTML =  sched_caption;
	document.getElementById('lblUpdateSelectedSpecialist').innerHTML =  sched_caption;
	document.getElementById('lblServices').innerHTML =  event.services;
	


	showloading();
	var postdata =  "&specialist="+sale_person_ids +"&product_ids="+product_ids;
	// alert(postdata);
	// return
	$.postJSON("?action=get_services_by_specialist", postdata, function(data) {
	    // if(data.success)
	    // {
			document.getElementById("divServices").innerHTML = data.message;
	    // }else{
			// document.getElementById("divServices").innerHTML = data.message;
	    // }
	    closeloading();
	});
	
	
	// var product_ids = store_day+",";
	
	var checkboxes = document.getElementsByName("services[]");
	for (var i=0; i<checkboxes.length; i++) {
	     if(product_ids.includes(","))
		 {
	     	var n = product_ids.includes(checkboxes[i].value+",");
	     }else{
	     	var n = product_ids.includes(checkboxes[i].value);
	     }
	     
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	var checkboxes = document.getElementsByName("specialist[]");
	for (var i=0; i<checkboxes.length; i++) {
		 if(sale_person_ids.includes(","))
		 {
	     	var n = sale_person_ids.includes(checkboxes[i].value+",");
	     }else{
	     	/*var n = sale_person_ids.includes(checkboxes[i].value);*/
	     	var n = false;
	     	if (sale_person_ids == checkboxes[i].value) {
	     		n = true;
	     	}
	     }
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	// $('#dlgEditReservationStep1').modal('show');
	$('#divNewCutomer').hide();
	$('#EditReservation').modal('show');
}


function HelloLems(id)
{
	alert(id);
}

function GoToPreviousSecondPage()
{
	$('#dlgEditReservationStep2').modal('show');
	$('#dlgEditReservationStep3').modal('hide');
}

function GoToSecondPage()
{

	var checkboxes = document.getElementsByName("specialist[]");
	var specialist ="";
	for (var i=0; i<checkboxes.length; i++) {
	   	if(checkboxes[i].checked)
	   	{  
			specialist = specialist + checkboxes[i].value +",";
		}
	}

	if(specialist.length==0)
	{
		// CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
		// return false;
	}else{
		specialist = specialist.substr(0, specialist.length-1);	
	}


	// $('#txtSpecialist').val(specialist);

	showloading();
	var postdata =  "&specialist="+specialist;
	$.postJSON("?action=get_services_by_specialist", postdata, function(data) {
	    // if(data.success)
	    // {
			document.getElementById("divServices").innerHTML = data.message;
	    // }else{
			// document.getElementById("divServices").innerHTML = data.message;
	    // }
	    closeloading();
	});



	var first_name = $('#txtSourceFirstName').val();
	var last_name = $('#txtSourceLastName').val();

	var mobile = $('#txtSourceMobile').val();
	var status_id = $('#txtSourceStatus').val();
	var source_date = $('#txtSourceDate').val();
	var source_time = $('#txtSourceTime').val();

	var notes = $('#txtSourceNotes').val();

	if(first_name.trim()=="")
	{
		CustomAlert("First Name is required.", "Unable to Save Reservation", "error");
		return false;
	}
	if(last_name.trim()=="")
	{
		CustomAlert("Last Name is required.", "Unable to Save Reservation", "error");
		return false;
	}
	
	if(mobile.trim()=="")
	{
		CustomAlert("Mobile no. is required.", "Unable to Save Reservation", "error");
		return false;
	}
	
	if(source_date=="")
	{
		CustomAlert("Date is required.", "Unable to Save Reservation", "error");
		return false;
	}
	
	if(source_time=="")
	{
		CustomAlert("Time is required.", "Unable to Save Reservation", "error");
		return false;
	}




	$('#dlgEditReservationStep2').modal('show');
	$('#dlgEditReservationStep1').modal('hide');
}

function GoToFinalPage()
{
	var checkboxes = document.getElementsByName("services[]");
	var services ="";
    for (var i=0; i<checkboxes.length; i++) {
	   	if(checkboxes[i].checked)
	   	{  
			services = services + checkboxes[i].value +",";
		}
	}
	
	if(services.length==0)
	{
		CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
		return false;
	}



	services = services.substr(0, services.length-1);
	$('#txtProducts').val(services);	

	

	showloading();
	var postdata =  "&services="+services+"&date="+$('#txtSourceDate').val();
	$.postJSON("?action=get_salon_specialist", postdata, function(data) {
	    if(data.success)
	    {
	    	// url = url.replace("#","");
			// txtSourceFirstName
			// txtSourceLastName
			// $("#txtSourceFirstName").val(data.first_name);
			// $("#txtSourceLastName").val(data.last_name);
			// $("#txtSourceMobile").val(data.mobile);
			document.getElementById("divSpecialist").innerHTML = data.message;
			


	    }else{
	    	// CustomAlert(data.message, "Unable to Find Customer", "error");
	  //   	$("#txtSourceFirstName").val("");
			// $("#txtSourceLastName").val("");
			// $("#txtSourceMobile").val("");
			document.getElementById("divSpecialist").innerHTML = data.message;
	    }

		
		// if(id>0)
		// {
			
			
			var sale_person_ids = $('#txtSpecialistId').val(); 

			// alert(sale_person_ids);
			// return false;
			if(sale_person_ids.trim() !=="")
			{
				var checkboxes = document.getElementsByName("specialist[]");
				for (var i=0; i<checkboxes.length; i++) {
					 if(sale_person_ids.includes(","))
					 {
				     	var n = sale_person_ids.includes(checkboxes[i].value+",");
				     }else{
				     	var n = sale_person_ids.includes(checkboxes[i].value);
				     }
				     if(n)
				     {
				     	checkboxes[i].checked = true;	
				     }else{
				     	checkboxes[i].checked = false;
				     }
				}
			}

			if($('#txtReservationId').val()>0)
			{
				var id = $('#txtReservationId').val();
				var sale_person_ids = document.getElementById(id+'sale_person_ids').innerHTML;
				var checkboxes = document.getElementsByName("specialist[]");
					for (var i=0; i<checkboxes.length; i++) {
						 if(sale_person_ids.includes(","))
						 {
					     	var n = sale_person_ids.includes(checkboxes[i].value+",");
					     }else{
					     	var n = sale_person_ids.includes(checkboxes[i].value);
					     }
					     if(n)
					     {
					     	checkboxes[i].checked = true;	
					     }else{
					     	checkboxes[i].checked = false;
					     }
					}
			}
			// }

	    closeloading();
	});


	



	$('#dlgEditReservationStep3').modal('show');
	$('#dlgEditReservationStep2').modal('hide');
}
function GoToFirstPage()
{
	$('#dlgEditReservationStep2').modal('hide');
	$('#dlgEditReservationStep1').modal('show');
}

function startInterval() {
    setInterval("startTime();",1000*5);
}

function startTime() {
    //var now = new Date();
    // document.getElementById('divCurrentTime').innerHTML = now.getHours() + ":" + now.getMinutes() + ":" +now.getSeconds();
    postdata ="";
    document.getElementById('divTotalWaiting').innerHTML ="....";
    $.postJSON("?action=get_total_waiting", postdata, function(data) {
        document.getElementById('divTotalWaiting').innerHTML = data.count;
        
        return false;
    });
}

function change_reservation_status(status)
{

	var reservation_id = $('#txtReservationId').val();
	
	showloading();
	var postdata =  "&reservation_id="+reservation_id+"&status="+status;
	$.postJSON("?action=change_reservation_status", postdata, function(data) {
	    if(data.success)
	    {
	    	/*CustomAlert(data.message, "Reservation Updated");*/
	    	// window.location.href = window.location.href;
	    	var date = $('#txtSelectedDate').val();
		    $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
					$("#calendar").fullCalendar('removeEvents');
					$("#calendar").fullCalendar('addEventSource', data.message);
					$("#calendar").fullCalendar('rerenderEvents');
	            // closeloading();
	        });
		    var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_message').modal('hide');
            }, delay);

	        $('#dlgEditReservationStep1').modal('hide');
	        $('#EditReservation').modal('hide');

	    }else{
	    	error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
			valid = false;
			$(".error-holder").html(error_return);
			$(".error-prompt").show();
	    	//CustomAlert(data.message, "Unable to Update Reservation", "error");
	    }
	    closeloading();
	});
}

function NewCustomer()
{

	var customer_label = document.getElementById('lblNewCustomer').innerHTML;
	
	if(customer_label=="New Customer")
	{
		$('#divNewCustomer').show();
		$('#divCustomerInformation').hide();
		// $('#divSearchCustomer').hide();
		$("#txtSourceFirstName").val("");
		$("#txtSourceLastName").val("");
		$("#txtSourceMobile").val("");
		$("#txtSourceEmail").val("");

		// $('#txtSourceMobile").val("");
		// $('#txtSourceFirstName").val("");
		// $('#txtSourceLastName").val("");	
		document.getElementById('lblNewCustomer').innerHTML = "Existing Customer";
	}else{
		$('#divNewCustomer').hide();
		$('#divCustomerInformation').hide();
		// $('#divSearchCustomer').show();
		document.getElementById('lblNewCustomer').innerHTML = "New Customer";
	}
}

function editButton()
{

	// var postdata =  "&specialist="+specialist_id;
	// $.postJSON("?action=get_services_by_specialist", postdata, function(data) {
	//     // if(data.success)
	//     // {
	// 		document.getElementById("divServices").innerHTML = data.message;
	//     // }else{
	// 		// document.getElementById("divServices").innerHTML = data.message;
	//     // }
	//     closeloading();
	// });


	var reservation_id = $('#txtUpdateReservationId').val();

	var date = $('#txtSourceDate').val();

	var new_date = new Date(date);
	
	var d = new_date.getUTCDate();
 	if(d <= 9)
 	{
 		d = "0" + d;
 	}
 	var m = new_date.getMonth() + 1;
 	if (m < 10) {
	 		m = "0" + m;
	 	}

 	var y = new_date.getFullYear();

   	$('#check_edit').val('1'); //edit trigger
   	$('#divNewCustomer').hide();
    $('#updateReservationDate').prop("disabled", true);
    
    $('#updateReservationDate').val(date);
    $('#updateReservationDate2').val(m+'-'+d+'-'+y); //added

    $('.updateReservationDate2').datepicker('update');

    /*edit time*/
	var time = convertTo24Hour($('#txtSourceTime').val());
		
	$('#updateReservationTime').val(time);
	//$('#check_edit_time').val('1');
	/*end time*/
    
    $('#rsrvationDateHolder').show();



	$('#dlgEditReservationStep1').modal('show');
	$('#EditReservation').modal('hide');
	// var event =$('#calendar').fullCalendar('clientEvents', reservation_id)[0];

	// alert(event.date);
	// alert(event.time);
}

function show_send_sms()
{
	$('#txtMessage').val("");
	$('#dlgMessageCustomer').modal('show');
	$('#EditReservation').modal('hide');
}

function send_sms()
{

	var last_name = $('#txtSourceLastName').val();
	var first_name = $('#txtSourceFirstName').val();
	var mobile = $('#txtSourceMobile').val();

	var message = $('#txtMessage').val();
	// alert();
	// return false;

	showloading();
	var postdata =  "&txtSourceLastName="+last_name+"&txtSourceFirstName="+first_name;
	postdata = postdata + "&txtSourceMobile="+mobile+"&txtMessage="+message;
	$.postJSON("?action=send_sms", postdata, function(data) {
	    if(data.success)
	    {
	    	CustomAlert(data.message, "Sending Message Done");
	    	// window.location.href = window.location.href;
	    // 	var date = $('#txtSelectedDate').val();
		   //  $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
					// $("#calendar").fullCalendar('removeEvents');
					// $("#calendar").fullCalendar('addEventSource', data.message);
					// $("#calendar").fullCalendar('rerenderEvents');
	    //         // closeloading();
	    //     });
		    var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_message').modal('hide');
            }, delay);
	        $('#dlgMessageCustomer').modal('hide');

	    }else{
	    	CustomAlert(data.message, "Unable to Send SMS", "error");
	    	var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_messageerror').modal('hide');
            }, delay);
	    }
	    closeloading();
	});
	return false;


}

function edit_reservation() {
    $('#updateReservationDate').prop("disabled", false);
}

function edit_reservation_time() {
	$('#updateReservationTime').prop("disabled", false);
}

function showSuggestions() {
	$(".error-prompt").hide();
	$(".error-holder").html();
	var valid = true; // checker of validation
	var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
	var error_return = '';

	$('#ulListReserve').paginate();
     $('#ulListReserve').data('paginate').kill()
  document.getElementById('ulListReserve').innerHTML = "";
  var date = $("#updateReservationDate2").val();

  if(date.trim()=="") {
    /*CustomAlert("Enter your preferred date.", "Unable to reserve");
    return false;*/
    error_return += error_default+'<span class="error-msg"> Enter your preferred date</span><br>';
	valid = false;
	$(".error-holder").html(error_return);
	$(".error-prompt").show();
	return false;
  }

  $('#txtDate').val(date);

  var checkboxes = document.getElementsByName("services[]");
  var services ="";
    
  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      services = services + checkboxes[i].value +",";
    }
  }
    
  if(services.length==0) {
   /* CustomAlert("Please select atleast one service", "Unable to reserve");
    return false;*/
    error_return += error_default+'<span class="error-msg"> Service is required</span><br>';
	valid = false;
	$(".error-holder").html(error_return);
	$(".error-prompt").show();
  }

  if (!valid) {
  	return false;
  }
    
  services = services.substr(0, services.length-1);
  $('#txtProducts').val(services);

  var checkboxes = document.getElementsByName("specialist[]");
  var specialist = "";

  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      specialist = specialist + checkboxes[i].value +",";
    }
  }
    
  if(specialist.length==0) {
    // CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
    // return false;
  } else{
    specialist = specialist.substr(0, specialist.length-1); 
  }

  $('#txtSpecialist').val(specialist);

  var postdata = $("#frmReservation").serialize();  
  console.log(postdata);

  $.postJSON("?action=show_suggestion", postdata, function(data) {
    
   if(data.success) {
      if (data.suggestions == "") {
        $('#ulListReserve').html("No time available.");
      } else {
        document.getElementById('ulListReserve').innerHTML = data.suggestions;
        closeloading();
		
		$('#ulListReserve li').css('list-style','none');
		var  monkeyList = new List('suggest', {
		  valueNames: ['suggestion-title'],
		  page: 8,
		  pagination: true
		});
      }
        


      /*$('#suggest').show();*/

    } else{
      var url = window.location.href;
	    url = url.replace("#",""); 
	    /*CustomAlert(data.message, "Unable to Update Reservation", "error");*/
	    closeloading();
	    error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
		valid = false;
		$(".error-holder").html(error_return);
		$(".error-prompt").show();
    }
  });
          
  return false;
}

function change_date_time() {
  $("#dlgEditReservationStep1").modal("hide");
  $('#ulListReserve').paginate();
  $('#ulListReserve').data('paginate').kill()
  $('#ulListReserve').html("");
  $("#editDateTimeModal").modal("show");
}

function ReserveMe(suggest_id)
{
  var content =  $('#txtData'+suggest_id).val();

  var conts = content.split("|");

  // alert(conts[0]);
  // return false;

  // alert(content);
  // return false;
  $('#check_edit_time').val('1');

  $('#updateReservationTime2').val(conts[0]);
  $('#updateReservationDate2').val(conts[1]);

  $("input[name='specialist[]']").removeAttr("checked");

  var checkboxes = document.getElementsByName("specialist[]");

  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].value == conts[2]) {  
      checkboxes[i].checked = true;
    }
  }
  //$('#txtSpecialist').val(conts[2]);


  var checkboxes = document.getElementsByName("services[]");
  var services ="";
    for (var i=0; i<checkboxes.length; i++) {
      // 
      if(checkboxes[i].value == conts[3])
      {
        checkboxes[i].checked = true;
      }
      // alert(checkboxes[i].value);
  }

  //console.log(conts); return false;
  // $('#txtFirstName').val("");
  $( "#btnFinish" ).trigger( "click" );
  return false;
}

//Add week
function setWeek(week) {
	var dateMoveWeek = new Date($('#txtSelectedDate').val());
	dateMoveWeek.setDate(dateMoveWeek.getUTCDate() + week*7);

	var dateCurrentSelected = new Date($('#txtSelectedDate').val());
	
	if (dateCurrentSelected.getMonth() < dateMoveWeek.getMonth()) {
		$('.supercal .next-month.change-month').trigger('click');
	}

	$('.supercal-month td:not(.month-prev)').each(function(index, element) {
		if (element.innerHTML == dateMoveWeek.getDate()) {
			$(this).addClass('sup'); 
			$(this).trigger('click'); return false;
		} 
		//console.log(element);
	});

	var setSelectedDate = '';
	
	var y = dateMoveWeek.getFullYear();
	var m = dateMoveWeek.getMonth()+1;
	if (m < 9) {
		m = '0'+m;
	}

	var d = dateMoveWeek.getDate();

	if (d < 9) {
		d = '0'+d;
	}
	setSelectedDate = y + '-' + m + '-' + d;

	$('#txtSelectedDate').val(setSelectedDate);

	return false;

	//console.log($('#txtSelectedDate').val());
	var selected_date = new Date($('#txtSelectedDate').val());
	
	selected_date.setDate(selected_date.getUTCDate() + 7);
	$('#txtSelectedDate').val('2017-04-30');
	
	$('#calendar').fullCalendar( 'gotoDate', selected_date );
	$.postJSON("?action=get_refresh_events&date="+selected_date, "", function(data) {
		console.log(data);
		$("#calendar").fullCalendar('removeEvents');
		$("#calendar").fullCalendar('addEventSource', data.message);
		$("#calendar").fullCalendar('rerenderEvents');

	    // closeloading();
	});
}


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});