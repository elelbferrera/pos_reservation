$(document).ready(function() {
	// $('#slider1').tinycarousel();
	
		// $('#example').paginate();
	    // $('.datepicker').datepicker({
           	// autoclose: true,
        // });
        // autoclose: true,
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
        
	     // $('.timepicker').timepicker({
	        // use24hours: true
	    // });
	    
	    // $('.timepicker').timepicker({
	        // use24hours: true,
	        // format: 'HH:mm'
	    // });
	
	$('#btnSetSchedule').click(function (){
		var checkboxes = document.getElementsByName("days[]");
		var days ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				days = days + checkboxes[i].value +",";
			}
		}
		
		days = days.substr(0, days.length-1);
		$('#txtDays').val(days);
		
		$('#setschedule').modal('hide');
		
	});

	
	$('#btnSaveBranch').click(function (){
		
		var branch_name = $('#txtBranchName').val();
		var branch_id = $('#txtBranchId').val();
		
		var contact_no = $('#txtContactNumber').val();
		
		var address = $('#txtAddress').val();
		var city = $('#txtCity').val();
		var country = $('#txtCountry').val();
		var state = $('#txtState').val();
		var zip = $('#txtZip').val();
		
		var opening_time = $('#txtOpeningTime').val();
		var closing_time = $('#txtClosingTime').val();
		
		if(branch_name.trim()=="")
		{
			CustomAlert("Branch Name is required.", "Unable to Save Branch", "error");
			return false;
		}
		
		if(city.trim()=="")
		{
			CustomAlert("City is required.", "Unable to Save Branch", "error");
			return false;
		}
		if(country=="")
		{
			CustomAlert("Country is required.", "Unable to Save Branch", "error");
			return false;
		}
		
		if(state.trim()=="")
		{
			CustomAlert("State is required.", "Unable to Save Branch", "error");
			return false;
		}
		
		if(zip.trim()=="")
		{
			CustomAlert("Zip is required.", "Unable to Save Branch", "error");
			return false;
		}
		
		if(contact_no.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Unable to Save Branch", "error");
			return false;
		}
		
		var checkboxes = document.getElementsByName("days[]");
		var days ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				days = days + checkboxes[i].value +",";
			}
		}
		
		if(days.length==0)
		{
			CustomAlert("Please select atleast one day on days of the week.", "Unable to Save Branch", "error");
			return false;
		}
		
		days = days.substr(0, days.length-1);
		$('#txtDays').val(days);
		
		
		
		$('#txtOpeningTime').val(convertTo24Hour(opening_time));
		$('#txtClosingTime').val(convertTo24Hour(closing_time));
		
		var postdata = $("#frmBranch").serialize();
		
		
		
		if($('#txtBranchId').val()>0)
		{
			showloading();
	        $.postJSON("?action=update", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	
	            	CustomAlert(data.message, "Save Branch");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save Branch", "error");
	            }
	            closeloading();
	        });
	    }else{
	    	showloading();
	    	$.postJSON("?action=add", postdata, function(data) {
	            if(data.success)
	            {
	            	var url = window.location.href;
	            	url = url.replace("#",""); 
	            	CustomAlert(data.message, "Save Branch");
	            	window.location.href = url;
	            }else{
	            	CustomAlert(data.message, "Unable to Save Branch", "error");
	            }
	            closeloading();
	        });
	        
	    }
	    return false;
	});
	
});


function add()
{
		
	// $('#divMinutes').show();
	// $('#divQuantity').hide();
	// $('#txtCost').val(0);
	// $('#txtPrice').val(0);
	// $('#txtProductId').val("");
	// $('#txtProductType').val("SERVICE");
	// $('#txtDescription').val("");
	// $('#txtMinutesOfService').val(1);
	// $('#txtQuantity').val(0);
	
	// $('#txtSpecialistId').val("-1");
	// $('#txtLastName').val("");
	// $('#txtFirstName').val("");
	// $('#txtMiddleName').val("");
	// $('#txtBirthDate').val("");
	// $('#txtGender').val("FEMALE");
	// $('#txtAddress').val("");
	// $('#txtCity').val("");
	// $('#txtCountry').val("");
	// $('#txtState').val("");
	// $('#txtStartDate').val("");
	// $('#txtEndDate').val("");
// 	
	// $('#txtStartTime').val("");
	// $('#txtEndTime').val("");
	
	$('#txtOpeningTime').val("");
	$('#txtClosingTime').val("");
	
	$('#txtBranchId').val("-1");
	
	
	$('#txtBranchName').val("");
	
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtZip').val("");
	$('#txtContactNumber').val("");
	$('#txtLongitude').val("");
	$('#txtLatitude').val("");
	
	var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	
	
	$('#dlgEditBranch').modal('show');
}

function edit(id)
{
	var branch_name = document.getElementById(id+'branch_name').innerHTML;
	var branch_id = id;
	var longitude = document.getElementById(id+'longitude').innerHTML;
	var latitude = document.getElementById(id+'latitude').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var store_day = document.getElementById(id+'store_day').innerHTML;
	var zip = document.getElementById(id+'zip').innerHTML;
	var contact_no = document.getElementById(id+'contact_number').innerHTML;
	var opening_time = document.getElementById(id+'store_start_time').innerHTML;
	var closing_time = document.getElementById(id+'store_end_time').innerHTML;
	
	
	// alert(d);
	// return false;
	
	opening_time = convertToAMPM(opening_time);
	closing_time = convertToAMPM(closing_time);
	
	$('#txtOpeningTime').val(opening_time);
	$('#txtClosingTime').val(closing_time);
	
	$('#txtBranchId').val(id);
	
	
	$('#txtBranchName').val(branch_name);
	
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtZip').val(zip);
	$('#txtContactNumber').val(contact_no);
	$('#txtLongitude').val(longitude);
	$('#txtLatitude').val(latitude);
	
	var days = store_day+",";
	
	var checkboxes = document.getElementsByName("days[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = days.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	$('#dlgEditBranch').modal('show');
}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});