$(document).ready(function() {

	// $('#tbUser').dataTable( {
	// 				"sPaginationType": "full_numbers"
	// } );

	// $('#example').paginate();
	$('.datepicker').datepicker({
        autoclose: true,
    });
 	$('#txtSourceDate').change(function()
 	{
 		var url =$('#txtURL').val();
 		url = url + "/" + $('#txtSourceDate').val();
 		window.location.href = url;

	});

	var monkeyList = new List('paginate-table', {
	  valueNames: ['customer-name'],
	  page: 7,
	  pagination: true
	});

	/*$('#tbUser').dataTable({
	      "paging": true,
	      "lengthChange": false,
	      "searching": false,
	      "ordering": true,
	      "info": true,
	      "autoWidth": true
	});*/

	

});


function show_send_sms(id)
{
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var contact_number = document.getElementById(id+'contact_number').innerHTML;

	document.getElementById('pSendTo').innerHTML = "Sending message to " + first_name + " " + last_name;

	$('#txtMessageLastName').val(last_name.trim());
	$('#txtMessageFirstName').val(first_name.trim());
	$('#txtMessageContactNumber').val(contact_number);
	$('#txtMessage').val("");
	
	$('#dlgMessageCustomer').modal('show');
}

function add_waiting()
{		
	$('#txtLastName').val("");
	$('#txtFirstName').val("");
	$('#txtContactNumber').val("");
	$('#txtStatus').val("WAITING");
	$('#txtCheckInId').val("");
	
	$('#divStatus').hide();
	
	 $("#txtLastName").prop('readonly', false);
	 $("#txtFirstName").prop('readonly', false);
	 $("#txtContactNumber").prop('readonly', false);
	
	$('#dlgEditCheckIn').modal('show');
}


function edit_waiting(id)
{
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var contact_number = document.getElementById(id+'contact_number').innerHTML;
	var status = document.getElementById(id+'status').innerHTML;
	
	
	$('#divStatus').show();
	$('#txtLastName').val(last_name.trim());
	$('#txtFirstName').val(first_name.trim());
	$('#txtContactNumber').val(contact_number);
	$('#txtStatus').val(status);
	$('#txtCheckInId').val(id);
	
	$("#txtLastName").prop('readonly', true);
 	$("#txtFirstName").prop('readonly', true);
 	$("#txtContactNumber").prop('readonly', true);
	
	
	$('#dlgEditCheckIn').modal('show');
}





jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});