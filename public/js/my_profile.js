$(document).ready(function() {

		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('.profile-avatar').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$('#txtContactNo').mask("999-999-9999");
		    
		$("#file").change(function(){
		    readURL(this);
		});
		
		$('#chkMonday').change(function(){
			if(this.checked)
			{
				$('#divMonday').hide();
			}else{
				$('#divMonday').show();
			}	
		});
		
		$('#chkTuesday').change(function(){
			if(this.checked)
			{
				$('#divTuesday').hide();
			}else{
				$('#divTuesday').show();
			}	
		});
		
		$('#chkWednesday').change(function(){
			if(this.checked)
			{
				$('#divWednesday').hide();
			}else{
				$('#divWednesday').show();
			}	
		});
		
		$('#chkThursday').change(function(){
			if(this.checked)
			{
				$('#divThursday').hide();
			}else{
				$('#divThursday').show();
			}	
		});
		
		$('#chkFriday').change(function(){
			if(this.checked)
			{
				$('#divFriday').hide();
			}else{
				$('#divFriday').show();
			}	
		});
		
		$('#chkSaturday').change(function(){
			if(this.checked)
			{
				$('#divSaturday').hide();
			}else{
				$('#divSaturday').show();
			}	
		});
	
		$('#chkSunday').change(function(){
			if(this.checked)
			{
				$('#divSunday').hide();
			}else{
				$('#divSunday').show();
			}	
		});
	
	
	
		$('#example').paginate();
	    $('.datepicker').datepicker({
           	autoclose: true,
        });
        // autoclose: true,
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
        
        // $('#btnSetTime').click(function (){
        	// alert("HELLO");
        	// return false;
        // });
        
        
        
	     // $('.timepicker').timepicker({
	        // use24hours: true
	    // });
	    
	    // $('.timepicker').timepicker({
	        // use24hours: true,
	        // format: 'HH:mm'
	    // });
	

	$('#btnSetSchedule').click(function (){
		var checkboxes = document.getElementsByName("days[]");
		var days ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				days = days + checkboxes[i].value +",";
			}
		}
		
		var starttime = convertTo24Hour($('#txtSourceStartTime').val());
		var end_time = convertTo24Hour($('#txtSourceEndTime').val());
		
		
		
		days = days.substr(0, days.length-1);
		$('#txtDays').val(days);
		
		$('#txtStartTime').val(starttime);
		$('#txtEndTime').val(end_time);
		
		$('#setschedule').modal('hide');
		
	});
	
	// $('#btnSaveSpecialist').click(function (){
	$('#frmMerchantProfile').submit(function(){

		var merchant_name = $('#txtMerchantName').val();
    	var description = $('#txtDescription').val();
    	var address = $('#txtAddress').val();
    	var city = $('#txtCity').val();
    	var state = $('#txtState').val();
    	var zip = $('#txtZip').val();
    	var first_name = $('#txtFirstName').val();
    	var last_name = $('#txtLastName').val();
    	var contact_no = $('#txtContactNo').val();
    	var email = $('#txtEmail').val();
    	var longitude = $('#txtLongitude').val();
    	var latitude = $('#txtLatitude').val();
    	
    	
    	if(merchant_name.trim()=="")
    	{
    		CustomAlert("Enter your merchant name.", "Unable to save information");
    		return false;
    	}
    	
    	if(description.trim()=="")
    	{
    		CustomAlert("Enter your description", "Unable to save information");
    		return false;
    	}

    	if(address.trim()=="")
    	{
    		CustomAlert("Enter your address", "Unable to save information");
    		return false;
    	}

		if(city.trim()=="")
    	{
    		CustomAlert("Enter your city.", "Unable to save information");
    		return false;
    	}
    	
    	if(state.trim()=="")
    	{
    		CustomAlert("Enter your state", "Unable to save information");
    		return false;
    	}

    	if(zip.trim()=="")
    	{
    		CustomAlert("Enter your zip code", "Unable to save information");
    		return false;
    	}

    	if(first_name.trim()=="")
    	{
    		CustomAlert("Enter your first name.", "Unable to save information");
    		return false;
    	}
    	
    	if(last_name.trim()=="")
    	{
    		CustomAlert("Enter your last name", "Unable to save information");
    		return false;
    	}

    	if(contact_no.trim()=="")
    	{
    		CustomAlert("Enter your contact number", "Unable to save information");
    		return false;
    	}

    	if(email.trim()=="")
    	{
    		CustomAlert("Enter your email.", "Unable to save information");
    		return false;
    	}
    	
    	if(longitude.trim()=="")
    	{
    		CustomAlert("Enter longitude", "Unable to save information");
    		return false;
    	}

    	if(latitude.trim()=="")
    	{
    		CustomAlert("Enter latitude", "Unable to save information");
    		return false;
    	}
			
		     showloading();
	         $.ajax({
	            url: "?action=update_my_profile", // Url to which the request is send
	            type: "POST",             // Type of request to be send, called as method
	            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
	            dataType: 'json',
	            contentType: false,       // The content type used when sending data to the server.
	            cache: false,             // To unable request pages to be cached
	            processData:false,        // To send DOMDocument or non processed data file it is set to false
	            success: function(data)   // A function to be called if request succeeds
	            {
	                
	                if(data.success)
	                {
	                	CustomAlert(data.message, "Save Merchant Profile");
	                    var delay=1000; //3 second
	                    setTimeout(function() {
	                        window.location.href = window.location.href;    
	                    }, delay);
	                    
	                }else{
	                	CustomAlert(data.message, "Save Merchant Profile", "error");
	                }
	                closeloading();
	            }
        	});
	        return false;
	});
	
});


function add()
{
		
	// $('#divMinutes').show();
	// $('#divQuantity').hide();
	// $('#txtCost').val(0);
	// $('#txtPrice').val(0);
	// $('#txtProductId').val("");
	// $('#txtProductType').val("SERVICE");
	// $('#txtDescription').val("");
	// $('#txtMinutesOfService').val(1);
	// $('#txtQuantity').val(0);
	$('#txtSpecialistId').val("-1");
	$('#txtLastName').val("");
	$('#txtFirstName').val("");
	$('#txtMiddleName').val("");
	$('#txtBirthDate').val("");
	// $('#txtBranch').val("");
	$('#txtGender').val("FEMALE");
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtStartDate').val("");
	$('#txtEndDate').val("");
	
	$('#txtStartTime').val("");
	$('#txtEndTime').val("");
	var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	
	
	$('#dlgEditSpecialist').modal('show');
}

function edit(id)
{
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var branch_id = document.getElementById(id+'branch_id').innerHTML;
	var first_name = document.getElementById(id+'first_name').innerHTML;
	var last_name = document.getElementById(id+'last_name').innerHTML;
	var middle_name = document.getElementById(id+'middle_name').innerHTML;
	var birth_date = document.getElementById(id+'birth_date').innerHTML;
	var profile_picture = document.getElementById(id+'profile_picture').innerHTML;
	var gender = document.getElementById(id+'gender').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var schedule = document.getElementById(id+'schedule').innerHTML;
	var start_time = document.getElementById(id+'start_time').innerHTML;
	var end_time = document.getElementById(id+'end_time').innerHTML;
	var start_date = document.getElementById(id+'start_date').innerHTML;
	var end_date = document.getElementById(id+'end_date').innerHTML;
	
	// $("#imgProfile").empty();
// create variable holding the image src from the href link
	$("#imgProfile").attr('src', profile_picture);
	
	
	var d = new moment(end_time, 'HH:mm:ss');
	// alert(d);
	// return false;
	
	end_time = convertToAMPM(end_time);
	start_time = convertToAMPM(start_time);
	
	$('#txtSourceStartTime').val(start_time);
	$('#txtSourceEndTime').val(end_time);
	
	$('#txtSpecialistId').val(id);
	$('#txtLastName').val(last_name);
	$('#txtFirstName').val(first_name);
	$('#txtMiddleName').val(middle_name);
	$('#txtBirthDate').val(birth_date.replace(" 00:00:00", ""));
	$('#txtBranch').val(branch_id);
	$('#txtGender').val(gender);
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtState').val(state);
	$('#txtStartDate').val(start_date.replace(" 00:00:00", ""));
	$('#txtEndDate').val(end_date.replace(" 00:00:00", ""));
	
	var days = schedule+",";
	
	var checkboxes = document.getElementsByName("days[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = days.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	$('#dlgEditSpecialist').modal('show');
}


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});