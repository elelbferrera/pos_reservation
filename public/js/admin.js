var api_link = "http://go3mobileapp.com/v2/public/pos_api.php";
$(document).ready(function(){
	$('#mc-phone1').mask("999-999-9999");
	$('#mc-phone2').mask("999-999-9999");
	// $('#mc-fax').mask("999-999-9999");
});

function countryState(objVal){
	$.postJSON(api_link+"?method=listCountryState&P01="+objVal+"", "", function(data) {
		if(data.status == 'success'){
			var ctr = data.respmsg.length;
			var currentState = $('#mc-state').attr('data-currentState');
			$('#mc-state option').remove();
			if (ctr != '' || ctr != 0){
				if (currentState == ''){
					$('#mc-state').append('<option value="" selected disabled>- Select State -</option>');
				} else {
					$('#mc-state').append('<option value="'+currentState+'" selected disabled>-- '+currentState+'</option>');
				}
				
				for (var i = 0; i < ctr; i++){
					$('#mc-state').append('<option value="'+data.respmsg[i].name+'">'+data.respmsg[i].name+'</option>');
				}	
			} else {
				$('#mc-state').append('<option value="" selected disabled>- No State Available -</option>');
			}

		} else {
			console.log(data);
		}

	});
}

$(document).ready(function() {
	
	var countryID = $('#mc-country option:selected').attr('data-id');
	countryState(countryID);
	
	
	$('#mc-country').change(function (){
		var countryID = $('#mc-country option:selected').attr('data-id');
		countryState(countryID);
	});
	
	$('#mc-state').change(function (){
		$('#state').val($(this).val());
	});
	
});

$(document).on('click','#btnNewMerchant', function() {
	var allow = false;
	var postdata = $("#frmNewMerchant").serialize();
	
	$("#frmNewMerchant .required").each(function (){
		if ($(this).val().length === 0){
			allow = false;
			CustomAlert("Please fill all the requred(*) fields.", "Empty Required Field");
			return false;
		} else {
			allow = true;
		}
	});
	
	if (allow) {	
		showloading();
		$.ajax({
			url: api_link+"?method=adminNewMerchant",
			data: postdata,
			dataType: "JSON",
			type: "POST",
			success: function(data){
				if(data.respcode == '0000') {
					//console.log(data);
					closeloading();
					CustomAlert(data.respmsg.message, "Record Save");
					setTimeout(function() {
						window.location.href = data.respmsg.redirect_url;    
					}, 1000);
				} else {
					closeloading();
					CustomAlert(data.respmsg, "Error Saving");
				}
			},
			error: function(xhr,status,error){
				console.log(xhr);
			}
		});
	}
});

$(document).on('click','#btnEditMerchant', function() {
	var allow = false;
	var postdata = $("#frmEditMerchant").serialize();
	
	$("#frmEditMerchant .required").each(function (){
		if ($(this).val().length === 0){
			allow = false;
			CustomAlert("Please fill all the requred(*) fields.", "Empty Required Field");
			return false;
		} else {
			allow = true;
		}
	});
	
	if(allow){
		showloading();
		$.ajax({
			url: api_link+"?method=editMerchantInfo",
			data: postdata,
			dataType: "JSON",
			type: "POST",
			success: function(data){
				if(data.respcode == '0000') {
					closeloading();
					CustomAlert(data.respmsg.message, "Record Save");
					setTimeout(function() {
						window.location.href = data.respmsg.redirect_url;    
					}, 1000);
				} else {
					closeloading();
					CustomAlert(data.respmsg, "Error updating");
				}
			},
			error: function(xhr,status,error){
				console.log(xhr);
			}
		});	
	}
});


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});