$(document).ready(function(){
  
  var editor = $('div#editor').html();
  var subject = $('input[name=subject]').val();

  $('div#content-msg').html(editor);
  $('span#subject-title').text(subject);

    $('#btnUpdateMessage').on('click', function(){
      
      var updatedMsg = $('div#editor').html();
      var subjectTitle = $('input[name=subject]').val();

      $('div#content-msg').html(updatedMsg);
      $('span#subject-title').text(subjectTitle);

    });

    $('#btnSubmitAppointment').click(function(){

      if($("#email_notif").is(':checked')){
        var emailNotification = "1";
      }else{
        var emailNotification = "0";
      }

      var editor = $('div#editor').html();
      var updatedMsg = '<html><body>' + editor + '</body></html>';
      var freqValue = $('input[name=txtFrequencyValue]').val();
      var freqOption = $('select[name=txtFrequencyOption] option:selected').text();
      var subjectTitle = $('input[name=subject]').val();
     
      if(updatedMsg == "")
      {
        CustomAlert("Message is required.", "Unable to set appointment");
        return false; 
      }
      
      if(subjectTitle.trim() == "")
      {
        CustomAlert("Subject is required", "Unable to set appointment");
        return false;
      }
      
      
      if(freqValue.trim()=="")
      {
        CustomAlert("Value is required", "Unable to set appointment");
        return false;
      }

      if(freqOption.trim()=='Days')
      {
        freqOption= 'days';
      }else if(freqOption.trim()=='Hours')
      {
        freqOption= 'hours';
      }else if(freqOption.trim()=='Min')
      {
        freqOption= 'minutes';
      }

      $.ajax({
          url: "?action=set_reminder",
          method: "POST",
          data: {
            txtNotification: emailNotification,
            txtSubject: subjectTitle,
            txtTemplate: updatedMsg,
            txtValue: freqValue,
            txtOption: freqOption
          },
          beforeSend: function(){
            showloading();
          }, 
          success: function(data){
            if(data.success)
            {
              closeloading();
              CustomAlert(data.message, "Appointment Reminder");
              setTimeout(function(){
            
                 window.location.href =data.redirect;
              }, 3000);

            }else{
              closeloading();
              CustomAlert(data.message, "Unable to set appointment");
            }
          }
      });

      return false;
    
    });

    $('select[name=txtFrequencyOption]').on('change', function(){
      var option = $(this).val();
      // console.log(option);

      if(option == 'Days'){
        $('input[name=txtFrequencyValue]').attr('min', '1');
        $('input[name=txtFrequencyValue]').attr('max', '365');
      }else if(option == 'Hours'){
        $('input[name=txtFrequencyValue]').attr('min', '1');
        $('input[name=txtFrequencyValue]').attr('max', '23');
      }else if(option == 'Min'){
        $('input[name=txtFrequencyValue]').attr('min', '1');
        $('input[name=txtFrequencyValue]').attr('max', '59');
      }

    });

    $('.appspinner .btn:first-of-type').on('click', function() {
      var btn = $(this);
      var input = btn.closest('.appspinner').find('input');
      if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {    
        input.val(parseInt(input.val(), 10) + 1);
      } else {
        btn.next("disabled", true);
      }
    });
    $('.appspinner .btn:last-of-type').on('click', function() {
      var btn = $(this);
      var input = btn.closest('.appspinner').find('input');
      if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {    
        input.val(parseInt(input.val(), 10) - 1);
      } else {
        btn.prev("disabled", true);
      }
    });

});