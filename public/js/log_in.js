$(document).ready(function() {
	$('#frmForgotPassword').submit(function (e){
		e.preventDefault();
		var email_address = $('#email_address').val();

		if (email_address.trim()=="") {
			CustomAlert("Email address is required.", "Unable to Reset Password", "error");
			return false;
		}

		var postdata = "email_address="+email_address;
		showloading();
		$.postJSON("?action=forgot_password", postdata, function(data) {
			if(data.success) {
	        	closeloading();
	        	$('#forgotPassword').modal('hide');

	        	CustomAlert("A link has been sent to your email.", "Reset Password");

	        	var delay=3000; //3 second
	            setTimeout(function() {
	                $('#dlg_message').modal('hide');
	            }, delay);
	        } else {
	        	$('#forgotPassword').modal('hide');

	        	CustomAlert("Email not found.", "Error", "error");
	        }
	    });

	    return false;
	});

	$('#frmNewPassword').submit(function (e){
		e.preventDefault();
		var new_password 		 = $('#new_password').val();
		var confirm_new_password = $('#confirm_new_password').val();

		if (new_password.trim()=="") {
			CustomAlert("Password is required.", "Unable to Reset Password", "error");
			return false;
		}

		if (confirm_new_password.trim()=="") {
			CustomAlert("Confirmed Password is required.", "Unable to Reset Password", "error");
			return false;
		}

		if (new_password !== confirm_new_password) {
			CustomAlert("Passwords do not match", "Unable to Reset Password", "error");
			return false;
		}

		var postdata = $("#frmNewPassword").serialize();

		showloading();
		$.postJSON("?action=forgot_password", postdata, function(data) {
			if(data.success) {
				closeloading();

				var getUrl = window.location;
				var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
				
	        	CustomAlert(data.message, "Success");
	        	var delay=3000; //3 second
	            setTimeout(function() {
	                $('#dlg_message').modal('hide');
	                window.location.href = baseUrl + "/log/in";
	            }, delay);
	        } else {
	        	closeloading();
	        	CustomAlert(data.message, "Error", "error");
	        }
	    });

	    return false;
	});


});


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});