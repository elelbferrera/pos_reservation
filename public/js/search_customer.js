$(document).ready(function() {
    $('.datepicker').datepicker({
        autoclose: true,
    });

    $(".timepicker").timepicker({
      showInputs: false
    });

    $('.updateReservationDate2').datepicker({
      autoclose: true,
      format : "mm-dd-yyyy",
      startDate: "0d"
    });
    
    //$('#ulListReserve').paginate();
    
  $('#frmSearchNameMobile').submit(function (e){
   	e.preventDefault();
 		var postdata = $("#frmSearchNameMobile").serialize();

 		showloading();

 		$.postJSON("?action=search_customer", postdata, function(data) {
      if(data.success) {
        if (data.customer =="") {
          CustomAlert("No customer found.", "Unable to Search customer", "error");
          $("#search").val('');
          document.getElementById('customerData').innerHTML = "";
          $('#customerData').paginate();
          $('#customerData').data('paginate').kill()
      return false;
        } else {
          $('#customerData').paginate();
          $('#customerData').data('paginate').kill()
          document.getElementById('customerData').innerHTML = data.customer;
        $('#customerData').paginate();
        }
      } 
      closeloading();
    });
    
    return false;
  });

  $("#customerData").on("click", "#editBtn", function() {
    $('#divCustomerInformation').show();  
    $('#divSearchCustomer').hide();

    var reservation_id        = $(this).data("reservation-id");
    var last_name             = $(this).data("last");
    var first_name            = $(this).data("first");
    var mobile                = $(this).data("mobile");
    var reservation_date      = $(this).data("date");
    var reservation_time      = $(this).data("time");
    var product_ids           = $(this).data("product-id");
    var sale_person_ids       = $(this).data("sale-person-id");
    var branch_id             = $(this).data("branch-id");
    var reservation_status_id = $(this).data("reservation-status-id");
    var notes                 = $(this).data("note");
    var status                = $(this).data("status");
    var product_name          = $(this).data("product-name");
    var merchant_id           = $(this).data("merchant-id");
    
    $('#txtSpecialistId').val("");

    var reservation_date      = reservation_date.substring(0, 10); 
  
    time                      = convertToAMPM(reservation_time);

    $('#txtReservationId').val(reservation_id);
    $('#txtMerchantId').val(merchant_id);
    $('#txtSourceTime').val(time);
    $('#txtSourceDate').val(reservation_date);
    $('#txtSourceBranchId').val(branch_id);
    $('#txtSourceStatus').val(reservation_status_id);
    $('#txtReservationId').val(reservation_id);
    $('#txtSourceLastName').val(last_name);
    $('#txtSourceFirstName').val(first_name);
    $('#txtSourceMobile').val(mobile);
    $('#txtSourceNotes').val(notes.trim());

    $('#txtUpdateReservationId').val(reservation_id);
    document.getElementById('lblUpdateCustomerName').innerHTML = first_name + " " + last_name;
    document.getElementById('lblUpdateContactNumber').innerHTML = mobile;
    var status = $("#txtSourceStatus option:selected").text();
    document.getElementById('lblUpdateStatus').innerHTML = status;
    document.getElementById('lblUpdateNotes').innerHTML = notes.trim();

    document.getElementById('lblSourceFullName').innerHTML = first_name + " " + last_name;
    document.getElementById('lblSourceMobile').innerHTML = mobile;

    if(status =="SCHEDULED")
    {
      $('#btnEdit').show();
      $('#btnCancel').show();
      $('#btnCheckIn').show();
      $('#btnConfirm').show();
      $('#btnDone').show();
      $('#btnSendSMS').show();
    }

    if(status =="CANCELLED" || status=="DONE")
    {
      $('#btnEdit').hide();
      $('#btnCancel').hide();
      $('#btnCheckIn').hide();
      $('#btnConfirm').hide();
      $('#btnDone').hide();
      $('#btnSendSMS').hide();
    }

    if(status =="CHECKED IN")
    {
      $('#btnEdit').show();
      $('#btnCancel').show();
      $('#btnCheckIn').hide();
      $('#btnConfirm').show();
      $('#btnDone').show();
      $('#btnSendSMS').show();
    }

    if(status =="CONFIRMED")
    {
      $('#btnEdit').show();
      $('#btnCancel').show();
      $('#btnCheckIn').hide();
      $('#btnConfirm').hide();
      $('#btnDone').show();
      $('#btnSendSMS').show();
    }

    var new_date = new Date(reservation_date);

    var month_name = function(dt) {
      mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
      return mlist[dt.getMonth()];
    }

    var m = month_name(new Date(reservation_date));

    var specialist_name = $(this).data("specialist");
    var sched_caption   = " Scheduled with " + specialist_name + " on ";
    sched_caption       = sched_caption + m + " " + new_date.getDate() + ", " + new_date.getFullYear() + " at " + time;

    document.getElementById('lblSelectedSpecialist').innerHTML =  sched_caption;
    document.getElementById('lblUpdateSelectedSpecialist').innerHTML =  sched_caption;
    document.getElementById('lblServices').innerHTML =  product_name;

    showloading();
    var postdata =  "&specialist="+sale_person_ids +"&product_ids="+product_ids;

    $.postJSON("?action=get_services_by_specialist", postdata, function(data) {
      document.getElementById("divServices").innerHTML = data.message;
      closeloading();
    });

    var checkboxes = document.getElementsByName("services[]");
    for (var i=0; i<checkboxes.length; i++) {
      if (product_ids.includes(",")) {
        var n = product_ids.includes(checkboxes[i].value+",");
      } else {
        var n = product_ids.includes(checkboxes[i].value);
      }
       
      if(n) {
        checkboxes[i].checked = true; 
      } else{
        checkboxes[i].checked = false;
      }
    }
  
    var checkboxes = document.getElementsByName("specialist[]");
    
    for (var i=0; i<checkboxes.length; i++) {
      if (String(sale_person_ids).includes(",")) {
        var n = String(sale_person_ids).includes(checkboxes[i].value+",");
      } else {
        var n = String(sale_person_ids).includes(checkboxes[i].value);
      }

      if(n) {
        checkboxes[i].checked = true; 
      } else{
        checkboxes[i].checked = false;
      }
    }
    
    $('#EditReservation').modal('show');
    
  });

  /*Edit*/
  $('#btnFinish').click(function (){
    var branch_id   = $('#txtSourceBranch').val();
    var first_name  = $('#txtSourceFirstName').val();
    var last_name   = $('#txtSourceLastName').val();
    var mobile      = $('#txtSourceMobile').val();
    var status_id   = $('#txtSourceStatus').val();
    var source_date = ($('#check_edit').val() == "1") ? $('#updateReservationDate2').val() : $('#txtSourceDate').val();
    var source_time = ($('#check_edit_time').val() == "1") ? $('#updateReservationTime2').val() : $('#txtSourceTime').val();
    var notes       = $('#txtSourceNotes').val();
    var email       = $('#txtSourceEmail').val();
    var checkboxes  = document.getElementsByName("services[]");
    var services    ="";

    var new_date = new Date(source_date);

    var d = new_date.getUTCDate();
    if(d <= 9)
    {
      d = "0" + d;
    }
    var m = new_date.getMonth() + 1;

    if (m < 10) {
      m = "0" + m;
    }
    var y = new_date.getFullYear();

    source_date = y+'-'+m+'-'+d;

    for (var i=0; i<checkboxes.length; i++) {
      if(checkboxes[i].checked) {  
        services = services + checkboxes[i].value +",";
      } 
    } 
    
    services = services.substr(0, services.length-1);

    if(services.length==0) {
      CustomAlert("Please select atleast one services.", "Unable to Save Reservation", "error");
      return false;
    }

    $('#txtProducts').val(services);
    
    var checkboxes = document.getElementsByName("specialist[]");
    var specialist = "";

    for (var i=0; i<checkboxes.length; i++) {
      if(checkboxes[i].checked) {  
        specialist = specialist + checkboxes[i].value +",";
      }
    }

    if(specialist.length==0) {
      // CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
      // return false;
    } else{
      specialist = specialist.substr(0, specialist.length-1); 
    }

    $('#txtSpecialist').val(specialist);
    $('#txtTime').val(convertTo24Hour(source_time));
    $('#txtDate').val(source_date);

    $('#txtBranch').val("-1");    
    $('#txtLastName').val(last_name);
    $('#txtFirstName').val(first_name);
    
    mobile = mobile.replace("-", "");
    mobile = mobile.replace("-", "");
    
    $('#txtMobile').val(mobile);
    $('#txtNotes').val(notes);
    $('#txtStatus').val(status_id);
    $('#txtEmail').val(email); 
    
    var postdata    = $("#frmReservation").serialize();

    var first_name  = $('#txtSourceFirstName').val();
    var last_name   = $('#txtSourceLastName').val();

    var mobile      = $('#txtSourceMobile').val();
    var email       = $('#txtSourceEmail').val();
    var status_id   = $('#txtSourceStatus').val();

    var source_date = $('#txtSourceDate').val();
    var source_time = $('#txtSourceTime').val();
    var notes       = $('#txtSourceNotes').val();

    if(first_name.trim()=="")
    {
      CustomAlert("First Name is required.", "Unable to Save Reservation", "error");
      return false;
    }

    if(last_name.trim()=="")
    {
      CustomAlert("Last Name is required.", "Unable to Save Reservation", "error");
      return false;
    }
    
    if(mobile.trim()=="")
    {
      CustomAlert("Mobile no. is required.", "Unable to Save Reservation", "error");
      return false;
    }

    if(source_date=="")
    {
      CustomAlert("Date is required.", "Unable to Save Reservation", "error");
      return false;
    }
    
    if(source_time=="")
    {
      CustomAlert("Time is required.", "Unable to Save Reservation", "error");
      return false;
    }
    
    
    if($('#txtReservationId').val()>0) {
      showloading();
          $.postJSON("?action=update", postdata, function(data) {
              if(data.success)
              {
                $('#check_edit').val("");
                var url = window.location.href;
                url = url.replace("#",""); 
                
                CustomAlert(data.message, "Update Reservation");

                $( "#searchCustomer" ).trigger( "click" );

                $('#dlgEditReservationStep1').modal('hide');
              $('#editDateTimeModal').modal('hide');

              var delay=1000; //3 second
                setTimeout(function() {
                    $('#dlg_message').modal('hide');
                }, delay);

              $('#dlgEditReservationStep1').modal('hide');
              $('#editDateTimeModal').modal('hide');
                // window.location.href = url;
              }else{
                var url = window.location.href;
                url = url.replace("#",""); 
                CustomAlert(data.message, "Unable to Update Reservation", "error");
              }
              closeloading();

              return false;
          });
      }else{
        showloading();
        $.postJSON("?action=add", postdata, function(data) {
              if(data.success)
              {
                var url = window.location.href;
                url = url.replace("#",""); 
                CustomAlert(data.message, "Reservation Created");
                var date = $('#txtSelectedDate').val();
            $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
              $("#calendar").fullCalendar('removeEvents');
              $("#calendar").fullCalendar('addEventSource', data.message);
              $("#calendar").fullCalendar('rerenderEvents');
                  // closeloading();
              });
              closeloading();
          var delay=1000; //3 second
          setTimeout(function() {
            $('#dlg_message').modal('hide');
          }, delay);
              $('#dlgEditReservationStep1').modal('hide');
                // window.location.href = url;
              }else{
                CustomAlert(data.message, "Unable to Save Reservation", "error");
              }
              closeloading();
              return false;
          });
          
      }
      
  });
  
});

function editButton()
{
  var reservation_id = $('#txtUpdateReservationId').val();
  var date           = $('#txtSourceDate').val();

  var new_date = new Date(date);
  
  var d = new_date.getUTCDate();
  if(d <= 9)
  {
    d = "0" + d;
  }
  var m = new_date.getMonth() + 1;
  if (m < 10) {
      m = "0" + m;
    }

  var y = new_date.getFullYear();

  $('#check_edit').val('1'); 
  $('#divNewCustomer').hide();
  $('#updateReservationDate').prop("disabled", true);
  
  //$('#updateReservationDate').val(date);

  $('#updateReservationDate2').val(m+'-'+d+'-'+y); //added
  $('.updateReservationDate2').datepicker('update');


  /*edit time*/
  var time = convertTo24Hour($('#txtSourceTime').val());

  //added
  /*$('#ulListReserve').paginate();*/
  $('#ulListReserve').html("");
    
  $('#updateReservationTime').val(time);
  $('#check_edit_time').val('1');
  
  $('#rsrvationDateHolder').show();
  $('#dlgEditReservationStep1').modal('show');
  $('#EditReservation').modal('hide');
}

function change_reservation_status(status)
{

  var reservation_id = $('#txtReservationId').val();
  
  showloading();
  var postdata =  "&reservation_id="+reservation_id+"&status="+status;
  $.postJSON("?action=change_reservation_status", postdata, function(data) {
      if(data.success)
      {
        CustomAlert(data.message, "Reservation Updated");
        /*window.location.href = window.location.href;*/
        $( "#searchCustomer" ).trigger( "click" );
        var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_message').modal('hide');
            }, delay);

          $('#dlgEditReservationStep1').modal('hide');
          $('#EditReservation').modal('hide');

      }else{
        CustomAlert(data.message, "Unable to Update Reservation", "error");
      }
      closeloading();
  });
}

function show_send_sms()
{
  $('#txtMessage').val("");
  $('#dlgMessageCustomer').modal('show');

  $('#EditReservation').modal('hide');
}

function send_sms()
{

  var last_name = $('#txtSourceLastName').val();
  var first_name = $('#txtSourceFirstName').val();
  var mobile = $('#txtSourceMobile').val();

  var message = $('#txtMessage').val();
  // alert();
  // return false;

  showloading();
  var postdata =  "&txtSourceLastName="+last_name+"&txtSourceFirstName="+first_name;
  postdata = postdata + "&txtSourceMobile="+mobile+"&txtMessage="+message;
  $.postJSON("?action=send_sms", postdata, function(data) {
      if(data.success)
      {
        CustomAlert(data.message, "Sending Message Done");
        // window.location.href = window.location.href;
      //  var date = $('#txtSelectedDate').val();
       //  $.postJSON("?action=get_refresh_events&date="+date, "", function(data) {
          // $("#calendar").fullCalendar('removeEvents');
          // $("#calendar").fullCalendar('addEventSource', data.message);
          // $("#calendar").fullCalendar('rerenderEvents');
      //         // closeloading();
      //     });
        var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_message').modal('hide');
            }, delay);
          $('#dlgMessageCustomer').modal('hide');

      }else{
        CustomAlert(data.message, "Unable to Send SMS", "error");
        var delay=1000; //3 second
            setTimeout(function() {
                $('#dlg_messageerror').modal('hide');
            }, delay);
      }
      closeloading();
  });
  return false;


}

function edit_reservation() {
  $('#updateReservationDate').prop("disabled", false);
}

function edit_reservation_time() {
  $('#updateReservationTime').prop("disabled", false);
}

function change_date_time() {
  $("#dlgEditReservationStep1").modal("hide");
  $('#ulListReserve').paginate();
  $('#ulListReserve').data('paginate').kill()
  $("#editDateTimeModal").modal("show");
}

/*function showSuggestions() {
  $('#ulListReserve').paginate();
     $('#ulListReserve').data('paginate').kill()
  document.getElementById('ulListReserve').innerHTML = "";
  var date = $("#updateReservationDate2").val();

  var new_date = new Date(date);

  var d = new_date.getUTCDate();
  if(d <= 9)
  {
    d = "0" + d;
  }
  var m = new_date.getMonth() + 1;

  if (m < 10) {
    m = "0" + m;
  }
  var y = new_date.getFullYear();

  date = y+'-'+m+'-'+d;

  if(date.trim()=="") {
    CustomAlert("Enter your preferred date.", "Unable to reserve");
    return false;
  }

  $('#txtDate').val(date);

  var checkboxes = document.getElementsByName("services[]");
  var services ="";
    
  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      services = services + checkboxes[i].value +",";
    }
  }
    
  if(services.length==0) {
    CustomAlert("Please select atleast one service", "Unable to reserve");
    return false;
  }
    
  services = services.substr(0, services.length-1);
  $('#txtProducts').val(services);

  var checkboxes = document.getElementsByName("specialist[]");
  var specialist = "";

  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      specialist = specialist + checkboxes[i].value +",";
    }
  }
    
  if(specialist.length==0) {
    // CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
    // return false;
  } else{
    specialist = specialist.substr(0, specialist.length-1); 
  }

  $('#txtSpecialist').val(specialist);

  var postdata = $("#frmReservation").serialize();  

  showloading();

  $.postJSON("?action=show_suggestion", postdata, function(data) {
    
    if(data.success) {
      if (data.suggestions == "") {
        $('#ulListReserve').html("No time available.");
      } else {
        $('#ulListReserve').paginate();   
        $('#ulListReserve').data('paginate').kill()
        document.getElementById('ulListReserve').innerHTML = data.suggestions;
        closeloading();
      $('#ulListReserve').paginate();
      }
        


      //$('#suggest').show();

    } else{
      var url = window.location.href;
                url = url.replace("#",""); 
                CustomAlert(data.message, "Unable to Update Reservation", "error");
                closeloading();
    }
  });
          
  return false;
} */

function showSuggestions() {
  $(".error-prompt").hide();
  $(".error-holder").html();
  var valid = true; // checker of validation
  var error_default = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
  var error_return = '';

  $('#ulListReserve').paginate();
     $('#ulListReserve').data('paginate').kill()
  document.getElementById('ulListReserve').innerHTML = "";
  var date = $("#updateReservationDate2").val();

  if(date.trim()=="") {
    /*CustomAlert("Enter your preferred date.", "Unable to reserve");
    return false;*/
    error_return += error_default+'<span class="error-msg"> Enter your preferred date</span><br>';
  valid = false;
  $(".error-holder").html(error_return);
  $(".error-prompt").show();
  return false;
  }

  $('#txtDate').val(date);

  var checkboxes = document.getElementsByName("services[]");
  var services ="";
    
  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      services = services + checkboxes[i].value +",";
    }
  }
    
  if(services.length==0) {
   /* CustomAlert("Please select atleast one service", "Unable to reserve");
    return false;*/
    error_return += error_default+'<span class="error-msg"> Service is required</span><br>';
  valid = false;
  $(".error-holder").html(error_return);
  $(".error-prompt").show();
  }

  if (!valid) {
    return false;
  }
    
  services = services.substr(0, services.length-1);
  $('#txtProducts').val(services);

  var checkboxes = document.getElementsByName("specialist[]");
  var specialist = "";

  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].checked) {  
      specialist = specialist + checkboxes[i].value +",";
    }
  }
    
  if(specialist.length==0) {
    // CustomAlert("Please select atleast one services.", "Unable to Save Branch", "error");
    // return false;
  } else{
    specialist = specialist.substr(0, specialist.length-1); 
  }

  $('#txtSpecialist').val(specialist);

  var postdata = $("#frmReservation").serialize();  

  $.postJSON("?action=show_suggestion", postdata, function(data) {
    
   if(data.success) {
      if (data.suggestions == "") {
        $('#ulListReserve').html("No time available.");
      } else {
        document.getElementById('ulListReserve').innerHTML = data.suggestions;
        closeloading();
    
    $('#ulListReserve li').css('list-style','none');
    var  monkeyList = new List('suggest', {
      valueNames: ['suggestion-title'],
      page: 8,
      pagination: true
    });
      }
        


      /*$('#suggest').show();*/

    } else{
      var url = window.location.href;
      url = url.replace("#",""); 
      /*CustomAlert(data.message, "Unable to Update Reservation", "error");*/
      closeloading();
      error_return += error_default+'<span class="error-msg"> '+data.message+'</span><br>';
    valid = false;
    $(".error-holder").html(error_return);
    $(".error-prompt").show();
    }
  });
          
  return false;
}

function ReserveMe(suggest_id)
{
  var content =  $('#txtData'+suggest_id).val();

  var conts = content.split("|");

  // alert(conts[0]);
  // return false;

  // alert(content);
  // return false;
  $('#updateReservationTime2').val(conts[0]);
  $('#updateReservationDate2').val(conts[1]);

  $("input[name='specialist[]']").removeAttr("checked");

  var checkboxes = document.getElementsByName("specialist[]");

  for (var i=0; i<checkboxes.length; i++) {
    if(checkboxes[i].value == conts[2]) {  
      checkboxes[i].checked = true;
    }
  }
  //$('#txtSpecialist').val(conts[2]);


  var checkboxes = document.getElementsByName("services[]");
  var services ="";
    for (var i=0; i<checkboxes.length; i++) {
      // 
      if(checkboxes[i].value == conts[3])
      {
        checkboxes[i].checked = true;
      }
      // alert(checkboxes[i].value);
  }

  //console.log(conts); return false;
  // $('#txtFirstName').val("");
  $( "#btnFinish" ).trigger( "click" );
  return false;
}

jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});