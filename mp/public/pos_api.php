<?php
require_once './base_api.php';

class pos_api extends base_api {
    protected $backend;
	
    public function __construct() {
        parent::__construct();
		
        require_once 'backend_'.PROGRAM.'.php';
        $this->backend = new backend($this->db);
    }
	
	//contact_number, verfication_code, new_password
	public function reset_password()
	{
		$params = $this->parameters;
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Invalid contact number.");
		}
		
		if(!isset($params['verification_code']))
		{
			return $this->failed("Invalid verification code.");
		}
		
		if(!isset($params['new_password']))
		{
			return $this->failed("Please enter a new password.");
		}
		
		
		return $this->process_method('reset_password', $params);
	}
	
	
	
	//contact_number
	public function forgot_password()
	{
		$params = $this->parameters;
		//send verification code to sms
	
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Invalid contact number.");
		}
		
		return $this->process_method('forgot_password', $params);
	}
	
	
	
	//session_id, merchant_id
	protected function check_if_favorite()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			return $this->process_method('check_if_favorite', $params);
		}
	}
	//session_id
	protected function list_my_favorite()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			return $this->process_method('list_my_favorite', $params);
		}
	}
	
	//session_id, favorite_id
	protected function remove_as_favorite()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['favorite_id']))
		{
			return $this->failed("Invalid favorite id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = "CUSTOMER";
			
			return $this->process_method('remove_as_favorite', $params);
		}
	}
	
	
	
	//session_id, merchant_id
	protected function save_as_favorite()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			 
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = "CUSTOMER";
			
			return $this->process_method('save_as_favorite', $params);
		}
	}
	
	
	
	
	
	//session_id, email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender
	
	protected function update_my_customer_information()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Invalid contact number");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Invalid last name");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Invalid first name");
		}
		
		if(!isset($params['gender']))
		{
			return $this->failed("Invalid gender");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			 
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = "CUSTOMER";
			
			return $this->process_method('update_my_customer_information', $params);
		}
	}
	
	//session_id
	protected function get_customer_check_ins()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
				
		// if(!isset($params['merchant_id']))
		// {
			// return $this->failed("Please check your merchant id.");
		// }
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			return $this->process_method('get_customer_check_ins', $params);
		}
	}
	
	
	
	//session_id
	protected function get_my_customer_reservation()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
				
		// if(!isset($params['merchant_id']))
		// {
			// return $this->failed("Please check your merchant id.");
		// }
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			return $this->process_method('get_my_customer_reservation', $params);
		}
	}
	
	//session_id, merchant_id, comment
	protected function create_my_review()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
				
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Please check your merchant id.");
		}
		
		if(!isset($params['comment']))
		{
			return $this->failed("Please check your comment/review.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type_desc'] !="CUSTOMER")
			{
				return $this->failed("Invalid session id. Please check your access");
			}
			
			//merchant_id, name, description, create_date, create_by
			$customer_id= $info['partner_id'];
			$params['customer_id'] = $customer_id;
			
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = "CUSTOMER";
			
			return $this->process_method('create_my_review', $params);
		}
	}	
	
	
	//merchant_id
	protected function get_time_zone()
	{
		$params = $this->parameters;
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		return $this->process_method('get_time_zone', $params);
	}
	
	
	//merchant_id
	protected function get_all_merchant_details()
	{
		$params = $this->parameters;
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		return $this->process_method('get_all_merchant_details', $params);
	}
	
	//id, status = SEATED or CANCELLED, update_date = current date of the merchant app, update_time
	protected function update_waiting_status()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['status']))
		{
			return $this->failed("Pleas enter your status.");
		}
		
		if(!isset($params['id']))
		{
			return $this->failed("Please enter the id of the waiting list.");
		}
		
		if(!in_array($params['status'],array("CANCELLED", "SEATED")))
		{
			return $this->failed("Invalid status. Please check.");
		}
		
		if(!isset($params['update_time']))
		{
			return $this->failed("Invalid update time. Please check.");
		}
		
		$time_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['update_time']);
		if(!$time_valid)
		{
			return $this->failed("Invalid update time. Please check your time format.");
		}
		
		if(!isset($params['update_date']))
		{
			return $this->failed("Invalid update date. Please check.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			$create_by = $info['username'];
			$params['update_by'] = $create_by;
			
			return $this->process_method('update_waiting_status', $params);
		}
	}
	
	
	//session_id, create_date, is_display_all_status
	protected function get_waiting_list_per_merchant()
	{
		$params = $this->parameters;
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		if(!isset($params['create_date']))
		{
			return $this->failed("Enter your current date.");
		}
		
		if(!isset($params['is_display_all_status']))
		{
			$params['is_display_all_status'] = 0;
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			return $this->process_method('get_waiting_list_per_merchant', $params);
		}
	}
	
	//session_id, last_name, first_name, contact_number, check_in_time, create_date
	protected function check_in_for_waiting_list()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Please enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Please enter your first name.");
		}
		
		if(!isset($params['check_in_time']))
		{
			return $this->failed("Please enter your check in time.");
		}
		
		$time_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['check_in_time']);
		if(!$time_valid)
		{
			return $this->failed("Invalid check-in time. Please check your time format.");
		}
		
		if(!isset($params['create_date']))
		{
			return $this->failed("Please enter your check in date.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			$create_by = $info['username'];
			// $params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			
			return $this->process_method('check_in_for_waiting_list', $params);
		}
	}	
	
	//merchant_id, last_name, first_name, contact_number, merchant_id, check_in_time, create_date, create_by
	protected function online_check_in_for_waiting_list()
	{
		$params = $this->parameters;
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Please enter your last name.");
		}
		if(!isset($params['first_name']))
		{
			return $this->failed("Please enter your first name.");
		}
		
		if(!isset($params['check_in_time']))
		{
			return $this->failed("Please enter your check in time.");
		}
		
		$time_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['check_in_time']);
		if(!$time_valid)
		{
			return $this->failed("Invalid check-in time. Please check your time format.");
		}
		
		if(!isset($params['create_date']))
		{
			return $this->failed("Please enter your check in date.");
		}
		$params['create_by'] = "CUSTOMER";
		
    	unset($params['method']);
		return $this->process_method('check_in_for_waiting_list', $params);
	}
	
	
	//P01 email/username, P02 = password
   protected function login_customer()
   {
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
		}else{
			$params = $this->parameters;
			$params=  array(
				'username' => $params['P01'],
				'password' => $params['P02'],
			);
			return $this->process_method('login_customer', $params);
		}	
   }
	
	//username, password, email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender
	public function online_register_customer()
	{
		$params = $this->parameters;
		if(!isset($params['username']))
		{
			return $this->failed("Invalid username.");
		}
		
		if(!isset($params['password']))
		{
			return $this->failed("Invalid password.");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Invalid contact number");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Invalid last name");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Invalid first name");
		}
		
		if(!isset($params['gender']))
		{
			return $this->failed("Invalid gender");
		}
		
		return $this->process_method('register_customer', $params);
	}
	
	
	//session_id, service_id, reservation_date, reservation_time
	public function get_specialist_for_reservation()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['reservation_date']))
		{
			return $this->failed("Invalid reservation date.");
		}
		
		if(!isset($params['reservation_time']))
		{
			return $this->failed("Invalid reservation time.");
		}
		
		if(!isset($params['service_id']))
		{
			$params['service_id'] =-1;
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];

			$merchant_id= $info['partner_id'];
			
			$params['merchant_id'] = $merchant_id;
			return $this->process_method('get_specialist_for_reservation', $params);
		}
		
	}
	
	
	
	//merchant_id, service_id, reservation_date, reservation_time = should be military time
	public function online_get_specialist_for_reservation()
	{
		$params = $this->parameters;
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		if(!isset($params['reservation_date']))
		{
			return $this->failed("Invalid reservation date.");
		}
		
		if(!isset($params['reservation_time']))
		{
			return $this->failed("Invalid reservation time.");
		}
		
		if(!isset($params['service_id']))
		{
			$params['service_id'] =-1;
		}
		
    	unset($params['method']);
		return $this->process_method('get_specialist_for_reservation', $params);
	
	}
	
	
	
	//session_id
	public function get_product_categories()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];

			$merchant_id= $info['partner_id'];
			
			$params['merchant_id'] = $merchant_id;
			return $this->process_method('get_product_categories', $params);
		}
	}
	
	//session_id, id of category
	public function delete_product_category()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$create_by = $info['username'];
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			
			return $this->process_method('delete_product_category', $params);
		}
	}
	
	//session_id, id, name, description
	public function update_product_category()
	{
		//id, merchant_id, name, description, update_date, update_by
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['name']))
		{
			return $this->failed("Invalid name for product category.");
		}
		
		if(!isset($params['description']))
		{
			return $this->failed("Please enter a short description of the product category.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			$create_by = $info['username'];
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			
			
			return $this->process_method('update_product_category', $params);
		}
	}
	
	//session_id, name, description
	public function create_product_category()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['name']))
		{
			return $this->failed("Invalid name for product category.");
		}
		
		if(!isset($params['description']))
		{
			return $this->failed("Please enter a short description of the product category.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//merchant_id, name, description, create_date, create_by
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			
			
			return $this->process_method('create_product_category', $params);
		}
	}
	
	
	
	//session_id
	public function get_merchant_profile()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			return $this->process_method('get_merchant_profile', $params);
		}
	}
	
	
	//session_id
	
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status, logo, description
	
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	
	//business_hours
	//monday_is_close, monday_start_time, monday_end_time
	//tuesday_is_close, tuesday_start_time, tuesday_end_time
	//wednesday_is_close, wednesday_start_time, wednesday_end_time
	//thursday_is_close, thursday_start_time, thursday_end_time
	//friday_is_close, friday_start_time, friday_end_time,
	//saturday_is_close, saturday_start_time, saturday_end_time
	//sunday_is_close, sunday_start_time, sunday_end_time
	protected function update_merchant_profile()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid Session Id");
		}
		
		if(!isset($params['business_name']))
		{
			return $this->failed("Enter a business name.");
		}
		// if(!isset($params['dba']))
		// {
			// return $this->failed("Enter a dba.");
		// }
		// if(!isset($params['processor']))
		// {
			// return $this->failed("Enter a processor.");
		// }
		// if(!isset($params['mid']))
		// {
			// return $this->failed("Enter an mid.");
		// }
		
		if(!isset($params['address1']))
		{
			return $this->failed("Enter an address.");
		}
		if(!isset($params['city']))
		{
			return $this->failed("Enter a city.");
		}
		if(!isset($params['state']))
		{
			return $this->failed("Enter a state.");
		}
		if(!isset($params['zip']))
		{
			return $this->failed("Enter a zip.");
		}
		// if(!isset($params['country']))
		// {
			// return $this->failed("Enter a country.");
		// }
		if(!isset($params['email']))
		{
			return $this->failed("Enter an email.");
		}
		if(!isset($params['phone1']))
		{
			return $this->failed("Enter a phone1.");
		}
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter a contact first name.");
		}
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter a contact last name.");
		}
		// if(!isset($params['contact_email']))
		// {
			// return $this->failed("Enter a contact email.");
		// }
		
		if(!isset($params['monday_is_close']))
		{
			return $this->failed("Please complete details for Monday");
		}
		if(!$params['monday_is_close'])
		{
			if(!isset($params['monday_start_time']))
			{
				return $this->failed("Please complete details for Monday");
			}
			if(!isset($params['monday_end_time']))
			{
				return $this->failed("Please complete details for Monday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['monday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Monday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['monday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Monday schedule.");
			}
			
			if( strtotime($params['monday_start_time'])>=strtotime($params['monday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Monday schedule.");
			}
		}else{
			$params['monday_start_time'] ="";
			$params['monday_end_time'] ="";
		}

		
		if(!isset($params['tuesday_is_close']))
		{
			return $this->failed("Please complete details for Tuesday");
		}
		
		if(!$params['tuesday_is_close'])
		{
			if(!isset($params['tuesday_start_time']))
			{
				return $this->failed("Please complete details for Tuesday");
			}
			if(!isset($params['tuesday_end_time']))
			{
				return $this->failed("Please complete details for Tuesday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['tuesday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Tuesday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['tuesday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Tuesday schedule.");
			}
			
			if( strtotime($params['tuesday_start_time'])>=strtotime($params['tuesday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Tuesday schedule.");
			}
		}else{
			$params['tuesday_start_time'] ="";
			$params['tuesday_end_time'] ="";
		}
		
		
		if(!isset($params['wednesday_is_close']))
		{
			return $this->failed("Please complete details for Wednesday");
		}
		if(!$params['wednesday_is_close'])
		{
			if(!isset($params['wednesday_start_time']))
			{
				return $this->failed("Please complete details for Wednesday");
			}
			if(!isset($params['tuesday_end_time']))
			{
				return $this->failed("Please complete details for Wednesday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['wednesday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Wednesday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['wednesday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Wednesday schedule.");
			}
			
			if( strtotime($params['wednesday_start_time'])>=strtotime($params['wednesday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Wednesday schedule.");
			}
		}else{
			$params['wednesday_start_time'] ="";
			$params['wednesday_end_time'] ="";
		}
		
		if(!isset($params['thursday_is_close']))
		{
			return $this->failed("Please complete details for Thursday");
		}
		
		if(!$params['thursday_is_close'])
		{
			if(!isset($params['thursday_start_time']))
			{
				return $this->failed("Please complete details for Thursday");
			}
			if(!isset($params['thursday_end_time']))
			{
				return $this->failed("Please complete details for Thursday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['thursday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Thursday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['thursday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Thursday schedule.");
			}
			
			if( strtotime($params['thursday_start_time'])>=strtotime($params['thursday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Thursday schedule.");
			}
		}else{
			$params['thursday_start_time'] ="";
			$params['thursday_end_time'] ="";
		}
				
		if(!isset($params['friday_is_close']))
		{
			return $this->failed("Please complete details for Friday");
		}
		
		if(!$params['friday_is_close'])
		{
			if(!isset($params['friday_start_time']))
			{
				return $this->failed("Please complete details for Friday");
			}
			if(!isset($params['thursday_end_time']))
			{
				return $this->failed("Please complete details for Friday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['friday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Friday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['friday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Friday schedule.");
			}
			
			if( strtotime($params['friday_start_time'])>=strtotime($params['friday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Friday schedule.");
			}
		}else{
			$params['friday_start_time'] ="";
			$params['friday_end_time'] ="";
		}
		
		
		if(!isset($params['saturday_is_close']))
		{
			return $this->failed("Please complete details for Saturday");
		}
		
		if(!$params['saturday_is_close'])
		{
			if(!isset($params['saturday_start_time']))
			{
				return $this->failed("Please complete details for Satuday");
			}
			if(!isset($params['thursday_end_time']))
			{
				return $this->failed("Please complete details for Saturday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['saturday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Saturday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['saturday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Saturday schedule.");
			}
			
			if( strtotime($params['saturday_start_time'])>=strtotime($params['saturday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Saturday schedule.");
			}
		}else{
			$params['saturday_start_time'] ="";
			$params['saturday_end_time'] ="";
		}
		
		if(!isset($params['sunday_is_close']))
		{
			return $this->failed("Please complete details for Sunday");
		}
		
		if(!$params['sunday_is_close'])
		{
			if(!isset($params['sunday_start_time']))
			{
				return $this->failed("Please complete details for Sunday");
			}
			if(!isset($params['thursday_end_time']))
			{
				return $this->failed("Please complete details for Sunday");
			}
			
			$time_start_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['sunday_start_time']);
			if(!$time_start_valid)
			{
				return $this->failed("Invalid start time for Sunday schedule.");
			}
			$time_end_valid = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $params['sunday_end_time']);
			if(!$time_end_valid)
			{
				return $this->failed("Invalid end time for Sunday schedule.");
			}
			
			if( strtotime($params['sunday_start_time'])>=strtotime($params['sunday_end_time']) )
			{
			 	return $this->failed("Invalid start time/end time for Sunday schedule.");
			}
		}else{
			$params['sunday_start_time'] ="";
			$params['sunday_end_time'] ="";
		}

		
		
		
		
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			$info = $response['ResponseMessage'];
			
			unset($params['method']);
			unset($params['session_id']);
			
			
			$merchant_id= $info['partner_id'];
			
			
			$create_by = $info['username'];
			// if($info['user_type_desc']!="SUPER ADMIN")
			// {
				// return $this->failed("Your not authorize to update merchant info.");	
			// }
			
			
			
			//merchant
			//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
			//fax, create_date, create_by, status
			
			//merchant_contact
			//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
			//website, create_date, create_by, status
			
			//merchant_mailing_address
			//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status
			
			
			
			$insert_data = array(
				'dba' => isset($params['dba']) ? $params['dba'] : '',
				'processor' => isset($params['processor']) ? $params['processor'] : '',
				'mid' => isset($params['mid']) ? $params['mid'] : '',
				
				// 'dba' => $params['dba'],
				// 'processor' => $params['processor'],
				// 'mid' => $params['mid'],
				
				'logo' => isset($params['logo']) ? $params['logo'] : '',
				'description' => isset($params['description']) ? $params['description'] : '',
				'id' => $merchant_id,
				'first_name' => $params['first_name'],
				'last_name' => $params['last_name'],
				'position' => isset($params['position']) ? $params['position'] : '',
				'mobile_number' => isset($params['mobile_number']) ? $params['mobile_number'] : '',
				'business_phone1' => isset($params['business_phone1']) ? $params['business_phone1'] : '',
				'business_phone2' => isset($params['business_phone2']) ? $params['business_phone2'] : '',
				'extension' => isset($params['extension']) ? $params['extension'] : '',
				'contact_fax' => isset($params['contact_fax']) ? $params['contact_fax'] : '',
				// 'contact_email' => $params['contact_email'],
				'contact_email' => isset($params['contact_email']) ? $params['contact_email'] : '',
				'website' => isset($params['website']) ? $params['website'] : '',
				
				'mailing_country' => isset($params['mailing_country']) ? $params['mailing_country'] : '',
				'mailing_address' => isset($params['mailing_address']) ? $params['mailing_address'] : '',
				'mailing_city' => isset($params['mailing_city']) ? $params['mailing_city'] : '',
				'mailing_state' => isset($params['mailing_state']) ? $params['mailing_state'] : '',
				'mailing_zip' => isset($params['mailing_zip']) ? $params['mailing_zip'] : '',
				
				
				
				'business_name' => $params['business_name'],
				
				'address1' => $params['address1'],
				'address2' => isset($params['address2']) ? $params['address2'] : '',
				'city' => $params['city'],
				'state' => $params['state'],
				'zip' => $params['zip'],
				'country' => isset($params['country']) ? $params['country'] : '',
				'email' => $params['email'],
				'phone1' => $params['phone1'],
				'phone2' => isset($params['phone2']) ? $params['phone2'] : '',
				'fax' => isset($params['fax']) ? $params['fax'] : '',
				'update_date' => date('Y-m-d H:i:s'),
				'update_by' => $create_by,
				'status' => 'A',		
				
				'longitude' => isset($params['longitude']) ? $params['longitude'] : '',
				'latitude' => isset($params['latitude']) ? $params['latitude'] : '',
				
				
				
				'sunday_is_close' => $params['sunday_is_close'],
				'sunday_start_time' => $params['sunday_start_time'],
				'sunday_end_time' => $params['sunday_end_time'],
		
				'monday_is_close' => $params['monday_is_close'],
				'monday_start_time' => $params['monday_start_time'],
				'monday_end_time' => $params['monday_end_time'],
				
				'tuesday_is_close' => $params['tuesday_is_close'],
				'tuesday_start_time' => $params['tuesday_start_time'],
				'tuesday_end_time' => $params['tuesday_end_time'],
				
				'wednesday_is_close' => $params['wednesday_is_close'],
				'wednesday_start_time' => $params['wednesday_start_time'],
				'wednesday_end_time' => $params['wednesday_end_time'],
				
				'thursday_is_close' => $params['thursday_is_close'],
				'thursday_start_time' => $params['thursday_start_time'],
				'thursday_end_time' => $params['thursday_end_time'],
				
				'friday_is_close' => $params['friday_is_close'],
				'friday_start_time' => $params['friday_start_time'],
				'friday_end_time' => $params['friday_end_time'],
				
				'saturday_is_close' => $params['saturday_is_close'],
				'saturday_start_time' => $params['saturday_start_time'],
				'saturday_end_time' => $params['saturday_end_time'],
				
			);
			return $this->process_method('update_merchant_profile', $insert_data);
			
		}
	}
	
	//session_id
	public function get_total_analytics()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			return $this->process_method('get_total_analytics', $params);
		}
	}
	
	//session_id, mobile
	public function search_customer_by_mobile()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['mobile']))
		{
			return $this->failed("Please enter a mobile no.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			return $this->process_method('search_customer_by_mobile', $params);
		}
		
	}
	
	//session_id, date yyyy-mm-dd format, branch_id if blank then all branch
	public function get_salon_specialist_per_merchant_per_day()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['date']))
		{
			return $this->failed("Please enter a date.");
		}
		
		if(!isset($params['branch_id']) || empty($params['branch_id']))
		{
			$params['branch_id'] = -1;
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			
			return $this->process_method('get_salon_specialist_per_merchant_per_day', $params);
		}
	}


	
	public function get_countries()
	{
		return $this->process_method('get_countries', "");
	}
	
	//user_id, last_name, first_name, email_address,
	public function update_user_information()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter your first name.");
		}
		
		if(!isset($params['email_address']))
		{
			return $this->failed("Enter your email address.");
		}
	
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			//user_id, last_name, first_name, email_address, update_date, update_by	
			
			$create_by = $info['username'];
			$user_id= $info['user_id'];
			$params['user_id'] = $params['user_id'];
			$params['last_name'] = $params['last_name'];
			$params['first_name]'] = $params['first_name'];
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			$params['email_address'] = $params['email_address'];
			
			
			return $this->process_method('update_user_information', $params);
		}
	}
	
	//session_id
	public function get_merchant_users()
	{
		$params = $this->parameters;
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			
			if($info['user_type']!=7) //merchant super admin
			{
				return $this->failed("You are not authorized.");	
			}
			
			//merchant_id, username, password, last_name, first_name, email_address, create_by, create_date, status
			
			$create_by = $info['username'];
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			
			return $this->process_method('get_merchant_users', $params);
		}
		
	}
	
	//session_id, password, new_password
	public function update_my_password()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['password']))
		{
			return $this->failed("Enter your current password.");
		}
		
		if(!isset($params['new_password']))
		{
			return $this->failed("Enter your new password.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			
			
			//merchant_id, username, password, last_name, first_name, email_address, create_by, create_date, status
			
			$create_by = $info['username'];
			$user_id= $info['user_id'];
			$params['user_id'] = $user_id;
			$params['password'] = $params['password'];
			$params['new_password]'] = $params['new_password'];
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			
			
			
			return $this->process_method('update_my_password', $params);
		}
		
		
	}
	
	//session_id, username, password, last_name, first_name, email_address
	public function merchant_create_user()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['username']))
		{
			return $this->failed("Enter your  username.");
		}
		
		if(!isset($params['password']))
		{
			return $this->failed("Enter your password.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter your first name.");
		}
		
		if(!isset($params['email_address']))
		{
			return $this->failed("Enter your email address.");
		}

		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['user_type']!=7) //merchant super admin
			{
				return $this->failed("You are not authorized to create a user.");	
			}
			
			//merchant_id, username, password, last_name, first_name, email_address, create_by, create_date, status
			
			$create_by = $info['username'];
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			
			return $this->process_method('merchant_create_user', $params);
		}
	}
	
	
	//merchant_id, reservation_date, reservation_time, branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited 
	public function online_create_reservation()
	{
		$params = $this->parameters;
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		if(!isset($params['reservation_date']))
		{
			return $this->failed("Invalid reservation date.");
		}
		
		if(!isset($params['reservation_time']))
		{
			return $this->failed("Invalid reservation time.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter your first name.");
		}
		
		if(!isset($params['mobile']))
		{
			return $this->failed("Enter your mobile number.");
		}
		
		if(!isset($params['branch_id']))
		{
			return $this->failed("Enter your branch id.");
		}
		
		if(!isset($params['product_id']))
		{
			return $this->failed("Please select the products you want to avail.");
		}
		
        	// create_date, create_by, status
        	// unset($params['method']);
			// unset($params['session_id']);
		unset($params['method']);
		
		$create_by = "ONLINE";
		$params['create_date'] = date('Y-m-d H:i:s');
		
		$merchant_id= $params['merchant_id'];
		$params['merchant_id'] = $merchant_id;
		$params['create_date'] = date('Y-m-d H:i:s');
		$params['create_by'] = $create_by;
		$params['status'] = 'A';
		
		return $this->process_method('create_reservation_via_merchant', $params);
		
		
	}
	
	
	public function online_get_merchants()
	{
		$params = $this->parameters;
		if(!isset($params['search_value']))
		{
			$params['search_value'] ="";
		}
		return $this->process_method('get_merchant', $params);
	}
	
	
	//session_id, reservation_date, reservation_time, branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited 
	public function update_reservation()
	{
		$params = $this->parameters;
	
		if(!isset($params['id']))
		{
			return $this->failed("Invalid reservation id.");
		}
		
		if(!isset($params['reservation_status_id']))
		{
			return $this->failed("Invalid reservation status.");
		}
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['reservation_date']))
		{
			return $this->failed("Invalid reservation date.");
		}
		
		if(!isset($params['reservation_time']))
		{
			return $this->failed("Invalid reservation time.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter your first name.");
		}
		
		if(!isset($params['mobile']))
		{
			return $this->failed("Enter your mobile number.");
		}
		
		if(!isset($params['branch_id']))
		{
			return $this->failed("Enter your branch id.");
		}
		
		if(!isset($params['product_id']))
		{
			return $this->failed("Please select the products you want to avail.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	// create_date, create_by, status
        	// unset($params['method']);
			// unset($params['session_id']);
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			$params['status'] = 'A';
			
			
			return $this->process_method('update_reservation', $params);
			
		}
		
	}
	
	//session_id, date
	public function get_reservations_per_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['date']))
		{
			return $this->failed("Invalid date.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			
			// print_r($info);
			// die();
			
			$merchant_id= $info['partner_id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'date' => $params['date'],
			);
			return $this->process_method('get_reservations_per_merchant', $params);
		}
	}
	
	
	//session_id
	public function get_reservation_status_per_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
// 			
			// print_r($info);
			// die();
			
			$merchant_id= $info['partner_id'];
			return $this->process_method('get_reservation_status_per_merchant', $merchant_id);
		}
	} 
	
	
	//session_id, id ,name, description, color, 
	//create_date, create_by, status
	protected function update_reservation_status()
	{
		$params = $this->parameters;
		
		if(!isset($params['id']))
		{
			return $this->failed("Enter id of reservation status.");
		}	
		// if(!isset($params['name']))
		// {
			// return $this->failed("Invalid name of reservation status.");
		// }
		if(!isset($params['color']))
		{
			return $this->failed("Please select a color code.");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {

			$response = $this->backend->get_response();
							
            unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			// $params['status'] = 'A';
			
			$params['name'] = isset($params['name']) ? $params['name'] : "";
			$params['description'] = isset($params['description']) ? $params['description'] : "";
			$params['color'] = isset($params['color']) ? $params['color'] : "";
			
			
            return $this->process_method('update_reservation_status', $params);
        }
		
	}
	
	
	//session_id, name, description, color, 
	//create_date, create_by, status
	protected function _create_reservation_status() //deferred
	{
		$params = $this->parameters;	
		if(!isset($params['name']))
		{
			return $this->failed("Invalid name of reservation status.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {

			$response = $this->backend->get_response();
							
            unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			$params['description'] = isset($params['description']) ? $params['description'] : "";
			$params['color'] = isset($params['color']) ? $params['color'] : "";
			
			
            return $this->process_method('create_reservation_status', $params);
        }
		
	}
	
	
	
	//id, branch_id, last_name, first_name, middle_name, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time
	protected function update_salon_specialist()
	{
		$params = $this->parameters;
		
		// if(!isset($params['branch_id']))
		// {
			// return $this->failed("Invalid Branch Id");
		// }
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter first name.");
		}
		
		if(!isset($params['start_date']))
		{
			return $this->failed("Enter start date.");
		}
		
		// if(!isset($params['end_date']))
		// {
			// return $this->failed("Enter end date.");
		// }

		// if(!isset($params['commission_id']))
		// {
			// return $this->failed("Enter a commission id.");
		// }
		
		if(!isset($params['schedule']))
		{
			return $this->failed("Enter a schedule.");
		}
		if(!isset($params['start_time']))
		{
			return $this->failed("Enter a start time.");
		}
		if(!isset($params['end_time']))
		{
			return $this->failed("Enter a end time.");
		}
		
		if(!isset($params['services']))
		{
			return $this->failed("Enter or select a services specialty.");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Enter contact number.");
		}
		
		if(!isset($params['email_address']))
		{
			return $this->failed("Enter email address.");
		}
		
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			$params['status'] = 'A';
			
			$params['commission_id'] = isset($params['commission_id']) ? $params['commission_id'] : -1;
			$params['gender'] = isset($params['gender']) ? $params['gender'] : "";
			$params['city'] = isset($params['city']) ? $params['city'] : "";
			$params['address'] = isset($params['address']) ? $params['address'] : "";
			$params['state'] = isset($params['state']) ? $params['state'] : "";
			$params['country'] = isset($params['country']) ? $params['country'] : "";

			return $this->process_method('update_salon_specialist', $params);
		}
	}
	
	
	//session_id, id, longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time
	//store_day, contact_number
	protected function update_merchant_branch()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid Session Id");
		}
		
		if(!isset($params['branch_name']))
		{
			return $this->failed("Enter a branch name.");
		}
		
		if(!isset($params['country']))
		{
			return $this->failed("Enter a country.");
		}
		if(!isset($params['address']))
		{
			return $this->failed("Enter an address.");
		}
		if(!isset($params['state']))
		{
			return $this->failed("Enter a state.");
		}
		
		if(!isset($params['zip']))
		{
			return $this->failed("Enter a zip.");
		}
		
		if(!isset($params['city']))
		{
			return $this->failed("Enter a city.");
		}
		
		if(!isset($params['store_start_time']))
		{
			return $this->failed("Enter a store time for opening.");
		}
		
		if(!isset($params['store_end_time']))
		{
			return $this->failed("Enter a store time for closing.");
		}
		
		if(!isset($params['store_day']))
		{
			return $this->failed("Enter a store day(s).");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Enter a contact number.");
		}
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			unset($params['method']);
			unset($params['session_id']);
			
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			
			$merchant_id= $info['partner_id'];
			
			$params['merchant_id'] = $merchant_id;
			$params['update_date'] = date('Y-m-d H:i:s');
			$params['update_by'] = $create_by;
			$params['status'] = 'A';
			$params['longitude'] = isset($params['longitude']) ? $params['longitude'] : 0;
			$params['latitude'] = isset($params['latitude']) ? $params['latitude'] : 0;
			
			// if($info['user_type_desc']!="SUPER ADMIN")
			// {
				// return $this->failed("Your not authorize to create a merchant.");	
			// }
			
			return $this->process_method('update_merchant_branch', $params);
		}
		
	}
	
	//session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//product_type
	protected function update_product()
	{
		$params = $this->parameters;
		if(!isset($params['id']))
		{
			return $this->failed("Invalid product id of service.");
		}	
		if(!isset($params['name']))
		{
			return $this->failed("Invalid name of service.");
		}
		
		if(!is_numeric($params['price']))
		{
			return $this->failed("Enter a valid price of service.");
		}
		
		if(!is_numeric($params['minutes_of_product']))
		{
			return $this->failed("Enter how many minutes is the services to be done");
		}
		
		if(!isset($params['category_id']) || $params['category_id'] <=0)
		{
			return $this->failed("Enter your category_id");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
			$response = $this->backend->get_response();
							
            unset($params['method']);
			unset($params['session_id']);
			
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			$params['update_date'] = date('Y-m-d H:i:s');
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['update_by'] = $create_by;
			// $params['status'] = 'A';
			
			$params['cost'] = isset($params['cost']) ? $params['cost'] : 0;
			$params['quantity'] = isset($params['quantity']) ? $params['quantity'] : 0;
			//$params['is_inventory'] = isset($params['is_inventory']) ? $params['is_inventory'] : 0;
			$params['product_type'] = isset($params['product_type']) ? $params['product_type'] : "SERVICE";
			$params['category_id'] = isset($params['category_id']) ? $params['category_id'] : -1;
			
            return $this->process_method('update_product', $params);
        }
	}
	
	//session_id
	public function get_branch_per_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			
			$merchant_id= $info['partner_id'];
			return $this->process_method('get_branch_per_merchant', $merchant_id);
		}
	}
	
	
	//session_id, category_id 
	public function get_products_per_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['category_id']))
		{
			$params['category_id'] = -1;
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
        	unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
// 			
			// print_r($info);
			// die();
			
			$merchant_id= $info['partner_id'];
			
			$params['merchant_id'] = $merchant_id;
			//return $this->process_method('get_products_per_merchant', $merchant_id);
			return $this->process_method('get_products_per_merchant', $params);
		}
	} 
	
	
	//session_id
	public function get_salon_specialist_per_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			
			
			$merchant_id= $info['partner_id'];
			return $this->process_method('get_salon_specialist_per_merchant', $merchant_id);
		}
	} 
	
	
	//session_id, reservation_date, reservation_time, branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited 
	public function create_reservation_via_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid session id.");
		}
		
		if(!isset($params['reservation_date']))
		{
			return $this->failed("Invalid reservation date.");
		}
		
		if(!isset($params['reservation_time']))
		{
			return $this->failed("Invalid reservation time.");
		}
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter your last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter your first name.");
		}
		
		if(!isset($params['mobile']))
		{
			return $this->failed("Enter your mobile number.");
		}
		
		if(!isset($params['branch_id']))
		{
			return $this->failed("Enter your branch id.");
		}
		
		if(!isset($params['product_id']))
		{
			return $this->failed("Please select the products you want to avail.");
		}
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	// create_date, create_by, status
        	// unset($params['method']);
			// unset($params['session_id']);
			unset($params['method']);
			unset($params['session_id']);
			$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			
			return $this->process_method('create_reservation_via_merchant', $params);
			
		}
		
	}
	
	//session_id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//product_type, create_date, create_by, status
	protected function register_product()
	{
		
		$params = $this->parameters;	
		if(!isset($params['name']))
		{
			return $this->failed("Invalid name of service.");
		}
		
		if(!is_numeric($params['price']))
		{
			return $this->failed("Enter a valid price of service.");
		}
		
		if(!is_numeric($params['minutes_of_product']))
		{
			return $this->failed("Enter how many minutes is the services to be done");
		}
		
		if(!isset($params['category_id']) || $params['category_id'] <=0)
		{
			return $this->failed("Enter your category_id");
		}
		
		
		if (! (strlen($this->parameters['session_id']))) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            	
			$response = $this->backend->get_response();
							
            unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			$params['cost'] = isset($params['cost']) ? $params['cost'] : 0;
			$params['quantity'] = isset($params['quantity']) ? $params['quantity'] : 0;
			//$params['is_inventory'] = isset($params['is_inventory']) ? $params['is_inventory'] : 0;
			$params['product_type'] = isset($params['product_type']) ? $params['product_type'] : "SERVICE";
			$params['category_id'] = isset($params['category_id']) ? $params['category_id'] : -1;
			
			
            return $this->process_method('register_product', $params);
        }
	}
	
	
	protected function get_merchant()
	{
			$params = $this->parameters;
            $params = array(
        		'search_value' => isset($params['search_value']) ? $params['search_value'] : "",
            );
            return $this->process_method('get_merchant', $params);
        // }
	}
	
	//branch_id, last_name, first_name, middle_name, create_date, create_by, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time
	//contact_number, email_address
	protected function register_salon_specialist()
	{
		$params = $this->parameters;
		
		// if(!isset($params['branch_id']))
		// {
			// return $this->failed("Invalid Branch Id");
		// }
		
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter last name.");
		}
		
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter first name.");
		}
		
		
		
		if(!isset($params['start_date']))
		{
			return $this->failed("Enter start date.");
		}
		
		// if(!isset($params['end_date']))
		// {
			// return $this->failed("Enter end date.");
		// }
		
		// if(!isset($params['commission_id']))
		// {
			// return $this->failed("Enter a commission id.");
		// }
		
		if(!isset($params['schedule']))
		{
			return $this->failed("Enter a schedule.");
		}
		if(!isset($params['start_time']))
		{
			return $this->failed("Enter a start time.");
		}
		if(!isset($params['end_time']))
		{
			return $this->failed("Enter a end time.");
		}
		
		if(!isset($params['services']))
		{
			return $this->failed("Enter or select a services specialty.");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Enter contact number.");
		}
		
		if(!isset($params['email_address']))
		{
			return $this->failed("Enter email address.");
		}
		
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			unset($params['method']);
			unset($params['session_id']);
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			$params['create_date'] = date('Y-m-d H:i:s');
			
			$merchant_id= $info['partner_id'];
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			
			$params['commission_id'] = isset($params['commission_id']) ? $params['commission_id'] : -1;
			$params['gender'] = isset($params['gender']) ? $params['gender'] : "";
			$params['city'] = isset($params['city']) ? $params['city'] : "";
			$params['address'] = isset($params['address']) ? $params['address'] : "";
			$params['state'] = isset($params['state']) ? $params['state'] : "";
			$params['country'] = isset($params['country']) ? $params['country'] : "";
			

			return $this->process_method('register_salon_specialist', $params);
		}
	}
	
	
	//session_id longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time
	//store_day, contact_number
	protected function register_merchant_branch()
	{
		$params = $this->parameters;
		
		
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid Session Id");
		}
		
		if(!isset($params['branch_name']))
		{
			return $this->failed("Enter a branch name.");
		}
		
		if(!isset($params['country']))
		{
			return $this->failed("Enter a country.");
		}
		if(!isset($params['address']))
		{
			return $this->failed("Enter an address.");
		}
		if(!isset($params['state']))
		{
			return $this->failed("Enter a state.");
		}
		
		if(!isset($params['zip']))
		{
			return $this->failed("Enter a zip.");
		}
		
		if(!isset($params['city']))
		{
			return $this->failed("Enter a city.");
		}
		
		if(!isset($params['store_start_time']))
		{
			return $this->failed("Enter a store time for opening.");
		}
		
		if(!isset($params['store_end_time']))
		{
			return $this->failed("Enter a store time for closing.");
		}
		
		if(!isset($params['store_day']))
		{
			return $this->failed("Enter a store day(s).");
		}
		
		if(!isset($params['contact_number']))
		{
			return $this->failed("Enter a contact number.");
		}
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			unset($params['method']);
			unset($params['session_id']);
			
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			
			$merchant_id= $info['partner_id'];
			
			$params['merchant_id'] = $merchant_id;
			$params['create_date'] = date('Y-m-d H:i:s');
			$params['create_by'] = $create_by;
			$params['status'] = 'A';
			$params['longitude'] = isset($params['logitude']) ? $params['longitude'] : 0;
			$params['latitude'] = isset($params['latitude']) ? $params['latitude'] : 0;
			
			// if($info['user_type_desc']!="SUPER ADMIN")
			// {
				// return $this->failed("Your not authorize to create a merchant.");	
			// }
			
			return $this->process_method('register_merchant_branch', $params);
		}
		
	}
	
	
	//session_id
	
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status
	
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	protected function register_merchant()
	{
		$params = $this->parameters;
		
		if(!isset($params['session_id']))
		{
			return $this->failed("Invalid Session Id");
		}
		
		if(!isset($params['business_name']))
		{
			return $this->failed("Enter a business name.");
		}
		if(!isset($params['dba']))
		{
			return $this->failed("Enter a dba.");
		}
		if(!isset($params['processor']))
		{
			return $this->failed("Enter a processor.");
		}
		if(!isset($params['mid']))
		{
			return $this->failed("Enter an mid.");
		}
		
		if(!isset($params['address1']))
		{
			return $this->failed("Enter an address.");
		}
		if(!isset($params['city']))
		{
			return $this->failed("Enter a city.");
		}
		if(!isset($params['state']))
		{
			return $this->failed("Enter a state.");
		}
		if(!isset($params['zip']))
		{
			return $this->failed("Enter a zip.");
		}
		if(!isset($params['country']))
		{
			return $this->failed("Enter a country.");
		}
		if(!isset($params['email']))
		{
			return $this->failed("Enter an email.");
		}
		if(!isset($params['phone1']))
		{
			return $this->failed("Enter a phone1.");
		}
		if(!isset($params['first_name']))
		{
			return $this->failed("Enter a contact first name.");
		}
		if(!isset($params['last_name']))
		{
			return $this->failed("Enter a contact last name.");
		}
		if(!isset($params['contact_email']))
		{
			return $this->failed("Enter a contact email.");
		}
		
		if (!$this->backend->validate_session(array('session_id' => $this->parameters['session_id']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			
			$info = $response['ResponseMessage'];
			$create_by = $info['username'];
			if($info['user_type_desc']!="SUPER ADMIN")
			{
				return $this->failed("Your not authorize to create a merchant.");	
			}
			
			//merchant
			//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
			//fax, create_date, create_by, status
			
			//merchant_contact
			//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
			//website, create_date, create_by, status
			
			//merchant_mailing_address
			//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status
			
			
			
			$insert_data = array(
				'first_name' => $params['first_name'],
				'last_name' => $params['last_name'],
				'position' => isset($params['position']) ? $params['position'] : '',
				'mobile_number' => isset($params['mobile_number']) ? $params['mobile_number'] : '',
				'business_phone1' => isset($params['business_phone1']) ? $params['business_phone1'] : '',
				'business_phone2' => isset($params['business_phone2']) ? $params['business_phone2'] : '',
				'extension' => isset($params['extension']) ? $params['extension'] : '',
				'contact_fax' => isset($params['contact_fax']) ? $params['contact_fax'] : '',
				'contact_email' => $params['contact_email'],
				'website' => isset($params['website']) ? $params['website'] : '',
				
				'mailing_country' => isset($params['mailing_country']) ? $params['mailing_country'] : '',
				'mailing_address' => isset($params['mailing_address']) ? $params['mailing_address'] : '',
				'mailing_city' => isset($params['mailing_city']) ? $params['mailing_city'] : '',
				'mailing_state' => isset($params['mailing_state']) ? $params['mailing_state'] : '',
				'mailing_zip' => isset($params['mailing_zip']) ? $params['mailing_zip'] : '',
				
				
				
				'business_name' => $params['business_name'],
				'dba' => $params['dba'],
				'processor' => $params['processor'],
				'mid' => $params['mid'],
				'address1' => $params['address1'],
				'address2' => isset($params['address2']) ? $params['address2'] : '',
				'city' => $params['city'],
				'state' => $params['state'],
				'zip' => $params['zip'],
				'country' => $params['country'],
				'email' => $params['email'],
				'phone1' => $params['phone1'],
				'phone2' => isset($params['phone2']) ? $params['phone2'] : '',
				'fax' => isset($params['fax']) ? $params['fax'] : '',
				'create_date' => date('Y-m-d H:i:s'),
				'create_by' => $create_by,
				'status' => 'A',
			);
			return $this->process_method('register_merchant', $insert_data);
			
		}
	}
	
	
	
	
	//P01 = username
	protected function _forgot_password()
	{
		if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
		}else{
			$params = array(
				'username' =>$this->parameters['P01']
			);
			return $this->process_method('forgot_password', $params);
		}
	}
	
	//P01 = session_id, P02 = merchant_id 
	protected function api_get_merchant_customer_balance()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="customers")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			$params = array(
				'customer_id' => $merchant_id,
				'merchant_id' => $this->parameters['P02'],
			);
			return $this->process_method('get_merchant_customer_balance', $params);
		}
	}
	
	//P01 = session_id of merchant, 
	//P02 = limit
   //P03 = page number
   //P04 = start_date yyyy-mm-dd
   //P05 = end_date yyyy-mm-dd
   //P06 = transaction_type  optional
   //DEALS, PUNCHCARD, REDEEM , DEDUCT AMOUNT, ADD AMOUNT, CONVERT, ADD POINTS
   protected function get_transactions_per_merchant()
   {
   	   	if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];

			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];

			if($this->parameters['P03']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P03'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}

			$params = array(
				'merchant_id' => $merchant_id, 
				'limit' => $this->parameters['P02'],
				'page_number' => $this->parameters['P03'],
			);
			
			if(isset($this->parameters['P04']))
			{
				$params['start_date'] = $this->parameters['P04'];	
			}else{
				$params['start_date']="";
			}
				
			if(isset($this->parameters['P05']))
			{
				$params['end_date'] = $this->parameters['P05'];	
			}else{
				$params['end_date']="";
			}
			
			if(isset($this->parameters['P06']))
			{
				$params['transaction_type'] = $this->parameters['P06'];	
			}else{
				$params['transaction_type'] = "";
			}
			
			
			return $this->process_method('get_transactions_per_merchant', $params);
		}
   }
	
	
	//P01 = session, P02 = email of customer, P03 = password of customer
	protected function validate_customer_password()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			
			$params = array(
				'email' => $this->parameters['P02'],
				'password' => $this->parameters['P03'],
			);
			
			return $this->process_method('validate_customer_password', $params);
		}
	}
	
	//P01 = session id, P02 = country_id
	protected function get_states()
	{
		if(isset($this->parameters['P01']))
		{
			if (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
	            $response = $this->backend->get_response();
	            return $this->failed($response['ResponseMessage']);
	        }else{
	        	$params = array(
					'country_id' => $this->parameters['P02'],
				);
				
				return $this->process_method('get_states', $params);
	        }
		}else{
			if(!isset($this->parameters['P02']))
			{
				return $this->failed("Invalid country selection.");
			}
			
			$params = array(
				'country_id' => $this->parameters['P02'],
			);
			
			return $this->process_method('get_states', $params);
		} 
	}
	
	// //P01 = session_id - just to extend session
	// protected function get_countries()
	// {
// 		
		// if(isset($this->parameters['P01']))
		// {
			// if (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
	            // $response = $this->backend->get_response();
	            // return $this->failed($response['ResponseMessage']);
	        // }else{
	        	// return $this->process_method('api_get_countries', '');	
	        // }
		// }else{
			// return $this->process_method('api_get_countries', '');
		// } 
	// }
	
	
	//session_id, mobile, password
	protected function merchant_activate_customer()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			$this->parameters['P02'] = str_replace("-","", $this->parameters['P02']);
			$this->parameters['P02'] = str_replace("+","", $this->parameters['P02']);
			
			$params = array(
				'merchant_id' => $merchant_id,
				'mobile' => $this->parameters['P02'],
				'password' => $this->parameters['P03']
			);
			return $this->process_method('merchant_activate_customer', $params);
		}
	}
	
	//session_id, mobile
	protected function merchant_search_customer()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			$this->parameters['P02'] = str_replace("-","", $this->parameters['P02']);
			$this->parameters['P02'] = str_replace("+","", $this->parameters['P02']);
			
			$params = array(
				'merchant_id' => $merchant_id,
				'mobile' => $this->parameters['P02'],
			);
			return $this->process_method('merchant_search_customer', $params);
		}
	}
	
	
	//P01 = session_id, P02 = customer_id 
	protected function get_merchant_customer_balance()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			$params = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $this->parameters['P02'],
			);
			return $this->process_method('get_merchant_customer_balance', $params);
		}
	}
	
	//P01 = session_id, P02 = username,  P03 = password, P04 = last_name, P05 = first_name, P06 = email
	protected function register_merchant_user()
	{
		if (!$this->_param_count_check(6)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
						
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			$params = array(
				'username' => $this->parameters['P02'],
			    'password' =>  $this->parameters['P03'], //encryption happens on creation of user method
			    'last_name' =>  $this->parameters['P04'],
			    'first_name' =>  $this->parameters['P05'],
		        'email' =>  $this->parameters['P06'],
		        'merchant_id' => $merchant_id,
			);
			return $this->process_method('register_merchant_user', $params);
		}
	}
	
	//P01 = session_id, amount, mobile or email of customer, description
	protected function redeem_points()
	{
		if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			if(!is_numeric($this->parameters['P02']))
			{
				return $this->failed("Enter a valid amount.");
			}
			
			$params = array(
				'merchant_id' => $merchant_id,
				'points' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'], //mobile or email	
				'user_info' => serialize($info),
				'description' => $this->parameters['P04'],
				'password' => isset($this->parameters['P05']) ? $this->parameters['P05'] : '',
			);
			return $this->process_method('redeem_points', $params);
		}
	}
	
	//P01 = session_id, amount, mobile or email of customer
	protected function sale_transaction()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			if(!is_numeric($this->parameters['P02']))
			{
				return $this->failed("Enter a valid amount.");
			}
			
			$params = array(
				'merchant_id' => $merchant_id,
				'amount' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'], //mobile or email	
				'user_info' => serialize($info),
				
				'password' => isset($this->parameters['P04']) ? $this->parameters['P04'] : '',
			);
			
			return $this->process_method('sale_transaction', $params);
		}
	}
	
	//P01 = session_id, amount, mobile or email of customer
	protected function reload_transaction()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			if(!is_numeric($this->parameters['P02']))
			{
				return $this->failed("Enter a valid amount.");
			}
			
			$params = array(
				'merchant_id' => $merchant_id,
				'amount' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'], //mobile or email	
				'user_info' => serialize($info),
				'password' => isset($this->parameters['P04']) ? $this->parameters['P04'] : '',
			);
			
			return $this->process_method('reload_transaction', $params);
		}
	}
	
	
	//P01 = session id of customer
	//P02 = page number
	//P03 = limit no of rows
	//P04 = merchant_id
	protected function my_transactions_per_merchant()
	{
		if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="customers")
			{
				return $this->failed("Invalid session for customers.");
			}
			$customer_id = $info['id'];
			
			if($this->parameters['P02']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P02'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}
			
			if($this->parameters['P03']=="")
			{
				$limit =10;
			}
			else{
				$limit = $this->parameters['P03'];
			}
			
			if(!is_numeric($this->parameters['P04']))
			{
				return $this->failed("Enter a valid merchant id.");
			}
			
			$params = array(
				'customer_id' => $customer_id,
				'page_number' => $this->parameters['P02'],
				'limit' => $limit,
				'merchant_id' => $this->parameters['P04']
			);
			
			return $this->process_method('my_transactions_per_merchant', $params);
		}   
	}
	
	
	
	//P01 = session_id, P02 = points, P03 = customer_ref email or mobile
	protected function convert_customer_points()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			
			if(!is_numeric($this->parameters['P02']))
			{
				return $this->failed("Invalid points.");
			}
			
			if($this->parameters['P03'] == "")
			{
				return $this->failed("Please supply a valid mobile/email of customer");
			}
			
			
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'points' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'],
				'password' => isset($this->parameters['P04']) ? $this->parameters['P04'] : '',
			);
			
			return $this->process_method('convert_customer_points', $params);
		}
	}
	
	
	//P01 = session_id, P02 = punch_code, P03 = hole_no, P04 = pin, P05 = customer_ref email or mobile
	protected function claim_punch_card_freebie()
	{
		if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			
			if($this->parameters['P02']=="")
			{
				return $this->failed("Invalid Punch Code.");
			}
			
			if(!is_numeric($this->parameters['P03']))
			{
				return $this->failed("Invalid hole no.");
			}
			
			if(!is_numeric($this->parameters['P04']))
			{
				return $this->failed("Supply a valid pin.");
			}
			
			if($this->parameters['P05'] == "")
			{
				return $this->failed("Please supply a valid mobile/email of customer");
			}
			
			
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'punch_code' => $this->parameters['P02'],
				'hole_no' => $this->parameters['P03'],
				'pin' => $this->parameters['P04'],
				'customer_ref' => $this->parameters['P05'],
				'password' => isset($this->parameters['P06']) ?  $this->parameters['P06'] : '',
			);
			
			return $this->process_method('claim_punch_card_freebie', $params);
		}
	}
	
	
	//P01 = session_id of merchant, P02 = punch_code, customer_ref email or mobile, 
	protected function my_merchant_get_punchcard_information()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			
			if($this->parameters['P02']=="")
			{
				return $this->failed("Invalid Punch Code.");
			}
			
			if($this->parameters['P03'] =="")
			{
				return $this->failed("Invalid customer email/mobile");
			}
			
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'punch_code' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'],
			);
			
			return $this->process_method('my_merchant_get_punchcard_information', $params);
		}
	}
	
	//P01 = session_id of merchant,
	protected function my_merchant_get_punch_card()
	{
		if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
			);
			
			return $this->process_method('get_merchant_punch_card', $params);
		}
	}
	
	//P01 = session_id of merchant
	protected function my_merchant_get_deals()
	{
		if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
			);
			
			return $this->process_method('get_merchant_deals', $params);
		}
	}

	
	//P01 = merchant_id, P02 = session id of customer can be blank for the purpose of extending customer session
	protected function get_merchant_rewards()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }else{
        	if($this->parameters['P01'] =="" )
			{
				return $this->failed("Supply a valid merchant id.");
			}	
        	$params = array(
				'merchant_id' => $this->parameters['P01'],
			);
			
			if($this->parameters['P02'] !="" )
			{
				if (!$this->backend->validate_session(array('session_id' => $this->parameters['P02']))) {
            		$response = $this->backend->get_response();
            		return $this->failed($response['ResponseMessage']);
            	}
			}	
			
			
			return $this->process_method('get_merchant_rewards', $params);
        }
	}
	
	
	
	//P01 = merchant_id, P02 = session id of customer can be blank for the purpose of extending customer session
	protected function get_merchant_punch_card()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }else{
        	if($this->parameters['P01'] =="" )
			{
				return $this->failed("Supply a valid merchant id.");
			}	
        	$params = array(
				'merchant_id' => $this->parameters['P01'],
			);
			
			if($this->parameters['P02'] !="" )
			{
				if (!$this->backend->validate_session(array('session_id' => $this->parameters['P02']))) {
            		$response = $this->backend->get_response();
            		return $this->failed($response['ResponseMessage']);
            	}
			}	
			
			
			return $this->process_method('get_merchant_punch_card', $params);
        }
	}
	
	//session_id, punch_code
	protected function get_punch_card_status()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="customers")
			{
				return $this->failed("Invalid session for customers.");
			}
			$customer_id = $info['id'];
			
			$params = array(
				'customer_id' => $customer_id,
				'punch_code' => $this->parameters['P02']
			);
			
			return $this->process_method('get_punch_card_status', $params);
		}
	}
	
	
	//P01 = session_id , P02 = transaction_id
   protected function void_punch_card_transaction()
   {
   	   	if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'transaction_id' => $this->parameters['P02'],
				'password' => isset($this->parameters['P03']) ? $this->parameters['P03'] : '',
			);
			
			return $this->process_method('void_punch_card_transaction', $params);
		}   	
   }
	
	//P01 = session_id, amount, mobile or email of customer, punch_card_code
	protected function create_punch_card_transaction()
	{
		if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			if(!is_numeric($this->parameters['P02']))
			{
				return $this->failed("Enter a valid amount.");
			}
			
			
			$params = array(
				'merchant_id' => $merchant_id,
				'amount' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P03'], //mobile or email	
				'user_info' => serialize($info),
				'punch_card_code' => $this->parameters['P04'],
				'password' => isset($this->parameters['P05']) ? $this->parameters['P05'] : '',
			);
			
			return $this->process_method('create_punch_card_transaction', $params);
		}
	}
	
	
	
	//P01 = session id of customer
	//P02 = page number
	//P03 = limit no of rows
	protected function get_my_transactions()
	{
		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="customers")
			{
				return $this->failed("Invalid session for customers.");
			}
			$customer_id = $info['id'];
			
			if($this->parameters['P02']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P02'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}
			
			if($this->parameters['P03']=="")
			{
				$limit =10;
			}
			else{
				$limit = $this->parameters['P03'];
			}
			
			$params = array(
				'customer_id' => $customer_id,
				'page_number' => $this->parameters['P02'],
				'limit' => $limit,
			);
			
			return $this->process_method('get_my_transactions', $params);
		}   
	}
	
	
	//P01 = session_id , P02 = transaction_id
   protected function void_deal_transaction()
   {
   	   	if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'transaction_id' => $this->parameters['P02'],
				'password' => isset($this->parameters['P03']) ? $this->parameters['P03'] :'',
			);
			
			return $this->process_method('void_deal_transaction', $params);
		}   	
   }
	
	
	//P01 = session_id, amount, P02 = mobile or email of customer, P03 = deal_code, redeem_type
	//add redeem_by points or amount or other
	//remove amount
	
	protected function create_deal_transaction()
	{
		if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
			// if(!is_numeric($this->parameters['P02']))
			// {
				// return $this->failed("Enter a valid amount.");
			// }
			$redeem_by_transaction = array('points','amount','other');
			
			if(!in_array($this->parameters['P04'], $redeem_by_transaction))
			{
				return $this->failed("Invalid redeem type. Choices are, points, amount or other.");
			}
			
			
			$params = array(
				'merchant_id' => $merchant_id,
				//'amount' => $this->parameters['P02'],
				'customer_ref' => $this->parameters['P02'], //mobile or email	
				'user_info' => serialize($info),
				'deal_code' => $this->parameters['P03'],
				'redeem_type' => $this->parameters['P04'],
				'password' => isset($this->parameters['P05']) ? $this->parameters['P05'] : '',
			);
			
			return $this->process_method('create_deal_transaction', $params);
		}
	}
	
	
	
	
	
	//P01 = merchant_id, P02 = session id of customer can be blank for the purpose of extending customer session
	protected function get_merchant_deals()
	{
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }else{
        	if($this->parameters['P01'] =="" )
			{
				return $this->failed("Supply a valid merchant id.");
			}	
        	$params = array(
				'merchant_id' => $this->parameters['P01'],
			);
			
			if($this->parameters['P02'] !="" )
			{
				if (!$this->backend->validate_session(array('session_id' => $this->parameters['P02']))) {
            		$response = $this->backend->get_response();
            		return $this->failed($response['ResponseMessage']);
            	}
			}	
			
			
			return $this->process_method('get_merchant_deals', $params);
        }
	}
	
	
	
	//new API going up
	
   //P01 = session_id of customer
   //P02 = page number
   //P03 = limit
   //P04 = value 
   protected function get_my_merchants()
   {
   		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="customers")
			{
				return $this->failed("Invalid session for customers.");
			}
			$customer_id = $info['id'];
			
			if($this->parameters['P02']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P02'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}
			
			if($this->parameters['P03']=="")
			{
				$limit =10;
			}
			else{
				$limit = $this->parameters['P03'];
			}
			
			$params = array(
				'customer_id' => $customer_id,
				'page_number' => $this->parameters['P02'],
				'limit' => $limit,
				'value' => isset($this->parameters['P04']) ? $this->parameters['P04'] : "",
			);
			
			return $this->process_method('api_get_my_merchants', $params);
		}   
   } 
	
   protected function process_method($method, $params) {
        $result = $this->backend->$method($params);
        $response = $this->backend->get_response();
		
		// if($method =="create_transaction")
		// {
			// print_r($result);
			// die();
		// }
        if (!$result) {
            return $this->failed($response['ResponseMessage']);
        } else {
            return $this->success($response['ResponseMessage']);
        }
    }
   
   //P01 = session_id , P02 = transaction_id
   protected function void_transaction()
   {
   	   	if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'transaction_id' => $this->parameters['P02'],
				'password' => isset($this->parameters['P03']) ? $this->parameters['P03'] :'',
			);
			
			$this->backend->get_transaction_type($this->parameters['P02'], $merchant_id);
			$trans_type = $this->backend->get_response();
			$trans_type = $trans_type['ResponseMessage'];
			
			
			if($trans_type =="ADD POINTS")
			{
				return $this->process_method('void_transaction', $params);
			}
			
			if($trans_type =="CONVERT")
			{
				//no voiding of redeem
				return $this->process_method('void_convert_customer_points', $params);
			}
			
			if($trans_type =="ADD AMOUNT")
			{
				return $this->process_method("void_reload_transaction", $params);
			}
			
			if($trans_type =="DEDUCT AMOUNT")
			{
				return $this->process_method("void_sale_transaction", $params);
			}
			
			
			if($trans_type =="REDEEM")
			{
				//no voiding of redeem
				return $this->process_method("void_redeem_points", $params);
			}

			if($trans_type =="PUNCHCARD")
			{
				return $this->process_method("void_punch_card_transaction", $params);
			}
			
			//if($trans_type =="DEALS")
			if($trans_type =="PROMOTION")
			{
				return $this->process_method("void_deal_transaction", $params);
			}
			
			return $this->failed("Could not find transaction type.");
		}   	
   }
   
   //P01 = session_id of merchant, P02 - customer_id from get_all_customers_per_merchant, P03 = limit
   //P04 = page number
   //P05 = start_date yyyy-mm-dd
   //P06 = end_date yyyy-mm-dd
   //P07 = transaction_type 
   //DEALS, PUNCHCARD, REDEEM , DEDUCT AMOUNT, ADD AMOUNT, CONVERT, ADD POINTS
   protected function get_customer_transactions_per_merchant()
   {
   	   	if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];

			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];

			if($this->parameters['P04']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P04'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}

			$params = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $this->parameters['P02'], 
				'limit' => $this->parameters['P03'],
				'page_number' => $this->parameters['P04'],
			);
			
			if(isset($this->parameters['P05']))
			{
				$params['start_date'] = $this->parameters['P05'];	
			}else{
				$params['start_date']="";
			}
				
			if(isset($this->parameters['P06']))
			{
				$params['end_date'] = $this->parameters['P06'];	
			}else{
				$params['end_date']="";
			}
			
			if(isset($this->parameters['P07']))
			{
				$params['transaction_type'] = $this->parameters['P07'];	
			}else{
				$params['transaction_type'] = "";
			}
			
			
			return $this->process_method('get_customer_transactions_per_merchant', $params);
		}
   }
   
   //P01 = session_id, P02 = limit records, P03 = search wild card from last name, middle name, last name, mobile
   protected function get_all_customers_per_merchant()
   {
   		if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'limit' => $this->parameters['P02'], 
				'search_value' => $this->parameters['P03'],
			);
			
			return $this->process_method('get_all_customers_per_merchant', $params);
		}
   }
   
   //P01 sessionid,  P02 amount, P03 customer mobile or email, P04  = transaction description
    protected function create_transaction()
	{
		if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			$params = array(
				'merchant_id' => $merchant_id,
				'customer_ref' => $this->parameters['P03'], //mobile or email
				'amount' => $this->parameters['P02'],
				'user_info' => serialize($info),
				'description' => $this->parameters['P04'],
				'password' => isset($this->parameters['P05']) ? $this->parameters['P05'] : '',
			);
			
			return $this->process_method('create_transaction', $params);
		}
	}
   
   
   //            'merchant_code' => $params['merchant_code'],
            // 'merchant_name' => $params['merchant_name'],
            // 'merchant_logo' => '',
            
           	// 'last_name' => $params['last_name'],
	        // 'first_name' => $params['first_name'],
            // 'business_type_id' => -1, //txtBusinessType
            // 'address' => '',
            // 'city' => isset($params['city']) ? $params['city'] : "",
            // 'state' => isset($params['state']) ? $params['state'] : "",
            // 'zip' => isset($params['zip']) ? $params['zip'] : "",
            // 'country' => $params['country'],
            // 'email' => $params['email'],
            // 'telephone' => $params['telephone'],
            // 'mobile' => isset($params['mobile']) ? $params['mobile'] : "",
            // 'fax' => isset($params['fax']) ? $params['fax'] : "",
    
    //P01 = session id
    //P02 = merchant_code, P03 = merchant_name, P04 = merchant_logo url,
    //P05 = last_name, P06 = first_name, P07 = business_type_id, P08 = address,
    //P09 = city, P10 = state, P11 = zip, P12 = country, P13 = email,  P14 = telephone, 
    //P15 = mobile, P16 = fax
    
	protected function update_merchant_profile_1()
	{
		if (!$this->_param_count_check(16)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			if($info['profile'] !="merchants")
			{
				return $this->failed("Invalid session for merchant.");
			}
			$merchant_id = $info['id'];
			
            $params = array(
                'id' => $merchant_id,
				'merchant_code' => $this->parameters['P02'],
				'merchant_name' => $this->parameters['P03'],
				'merchant_logo' => $this->parameters['P04'],
				'last_name' => $this->parameters['P05'],
				'first_name' => $this->parameters['P06'],
				'business_type_id' => $this->parameters['P07'],
				'address' => $this->parameters['P08'],
				'city' => $this->parameters['P09'],
				'state' => $this->parameters['P10'],
				'zip' => $this->parameters['P11'],
				'country' => $this->parameters['P12'],
				'email' => $this->parameters['P13'],
				'telephone' => $this->parameters['P14'],
				'mobile' => $this->parameters['P15'],
				'fax' => $this->parameters['P16'],
				'from_api' => 'YES',
            );
            return $this->process_method('update_merchant', $params);
        }
	}
	
   
   //P01 = business_type default -1 for all types of business
   //P02 = session_id of customer 
   //P03 = page number
   //P04 = limit
   //P05 = wildcard search for merchant name and zip code
   protected function get_merchants()
   {
   		if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } else {
        	
        	if(($this->parameters['P02']) !="")
			{
	        	if (!$this->backend->validate_session(array('session_id' => $this->parameters['P02']))) {
		            $response = $this->backend->get_response();
		            return $this->failed($response['ResponseMessage']);
	            }
				$response = $this->backend->get_response();
				$info = $response['ResponseMessage'];
				
				if($info['profile'] !="customers")
				{
					return $this->failed("Invalid session for customers.");
				}
				$customer_id = $info['id'];
			}else{
				$customer_id="";
			}
			
			if($this->parameters['P03']=="")
			{
				return $this->failed("Enter a valid page number.");
			}
			if($this->parameters['P03'] < 0)
			{
				return $this->failed("Enter a valid page number.");
			}
			
			if($this->parameters['P04']=="")
			{
				$limit =10;
			}
			else{
				$limit = $this->parameters['P04'];
			}
			
			// if($this->parameters['P05'] !="" )
			// {
				// if (!$this->backend->validate_session(array('session_id' => $this->parameters['P05']))) {
            		// $response = $this->backend->get_response();
            		// return $this->failed($response['ResponseMessage']);
            	// }
			// }	
			
        	$params= array(
        		'business_type_id' => $this->parameters['P01'],
        		//'customer_id' => $this->parameters['P02'],
        		'customer_id' => $customer_id,
        		'page_number' => $this->parameters['P03'],
        		'limit' => $limit,
        		'value' => isset($this->parameters['P05']) ? $this->parameters['P05'] : "", //return once app has been modified
        		//'value' => '',
			);
			return $this->process_method('api_get_all_merchants', $params);
		}
   }
   
   //P01 session_id, P02 last_name, P03 first_name, P04 middle_name, P05 mobile, P06 telephone,
	//P07 state, P08 city, P09 zip, P10 country, P11 email, P12 address, P13 udf1, P14 udf2, P15 udf3, 
	
	//P17 - date of birth
   protected function update_customer()
   {
        if (!$this->_param_count_check(15)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
        	$response = $this->backend->get_response();
			$info = $response['ResponseMessage'];
			
			$profile_id = $info['id'];
			$merchant_id = $info['merchant_id'];
			
			$this->parameters['P05'] = str_replace("-","", $this->parameters['P05']);
			$this->parameters['P05'] = str_replace("+","", $this->parameters['P05']);
			
			$this->parameters['P06'] = str_replace("-","", $this->parameters['P06']);
			$this->parameters['P06'] = str_replace("+","", $this->parameters['P06']);
			
			if($this->parameters['P02'] == "")
			{
				return $this->failed("Enter a last name.");
			}
			
			if($this->parameters['P03'] == "")
			{
				return $this->failed("Enter a first name.");
			}
			
			if($this->parameters['P11'] == "")
			{
				return $this->failed("Enter an email.");
			}
			
            $params = array(
                'id' => $profile_id,
               	'merchant_id' => $merchant_id,
                'last_name' => $this->parameters['P02'],
                'first_name' => $this->parameters['P03'],
                'middle_name' => $this->parameters['P04'],
                'mobile' => $this->parameters['P05'],
                'telephone' => $this->parameters['P06'],
                'state' => $this->parameters['P07'],
                'city' => $this->parameters['P08'],
                'zip' => $this->parameters['P09'],
                'country' => $this->parameters['P10'],
                'email' => $this->parameters['P11'],
                'address' => $this->parameters['P12'],
                'udf1' => $this->parameters['P13'],
                'udf2' => $this->parameters['P14'],
                'udf3' => $this->parameters['P15'],
                'date_of_birth' => isset($this->parameters['P17']) ? $this->parameters['P17'] : '',
                'from_api' => 'YES',
            );
            return $this->process_method('update_customer', $params);
        }
   }
   
   //P01 session_id, P02 username, P03 old password, P04 new password
   protected function change_password()
   {
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
            
        } else {
            $params = array(
                'username' => $this->parameters['P02'],
                'old_pw' => $this->parameters['P03'],
                'new_pw' => $this->parameters['P04'],
            );
            return $this->process_method('change_password', $params);
        }   		
   }
   
   //P01 email/username, P02 = password
   protected function login()
   {
		if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
		}else{
			$params = $this->parameters;
			$params=  array(
				'username' => $params['P01'],
				'password' => $params['P02'],
			);
			return $this->process_method('login', $params);
		}	
   }
   
   //P01 session id   
   protected function logout()
   {
   		if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
		}else{
			$params = $this->parameters;
			$params=  array(
				'session_id' => $params['P01'],
			);
			return $this->process_method('logout', $params);
		}
   }
    
 	//last_name, first_name, middle_name, mobile, telephone,
	//state, city, zip, country, email, address, udf1, udf2, udf3, password
	//is_merchant default is 0
	//P16 session id for Merchant
	//P17 date of birth
   protected function register_customer()
   {
		if (!$this->_param_count_check(16)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
		}
        else {
        	
        	$params = $this->parameters;
            
        	if($params['P16'] != "")
			{
				if (!$this->backend->validate_session(array('session_id' => $this->parameters['P16']))) {
		            $response = $this->backend->get_response();
		            return $this->failed($response['ResponseMessage']);
		        } else {
		        	$response = $this->backend->get_response();
					$info = $response['ResponseMessage'];
					if($info['profile'] !="merchants")
					{
						return $this->failed("Invalid session for merchant.");
					}
					$merchant_id = $info['id'];
				}
			}else{
				$merchant_id = -1;
			}
			
			if($params['P04'] == "")
			{
				return $this->failed("Enter a valid mobile number.");
			}
			
			$params['P04'] = str_replace("-","", $params['P04']);
			$params['P04'] = str_replace("+","", $params['P04']);
			
			$params['P05'] = str_replace("-","", $params['P05']);
			$params['P05'] = str_replace("+","", $params['P05']);
			
			if(!is_numeric($params['P15']))
			{
				return $this->failed("Password must be numeric.");
			}
			
			if($params['P01'] == "")
			{
				return $this->failed("Enter a last name.");
			}
			
			if($params['P02'] == "")
			{
				return $this->failed("Enter a first name.");
			}
			
			if($params['P10'] == "")
			{
				return $this->failed("Enter an email.");
			}
			
			
			
			$params = array(
            	'last_name' =>  $params['P01'],   //P01
            	'first_name' =>  $params['P02'],  //P02
            	'middle_name' =>  $params['P03'], //P03
            	'mobile' =>  $params['P04'],		//P04
            	'telephone' =>  $params['P05'],  //P05
            	'state' =>  $params['P06'],		//P06
            	'city' =>  $params['P07'],		//P07
            	'zip' =>  $params['P08'],		//P08
            	'country' =>  $params['P09'],	//P09
            	'email' => $params['P10'],		//P10
            	'address' => $params['P11'],	//P11
            	'udf1' => $params['P12'],		//P12
            	'udf2' => $params['P13'],		//P13
            	'udf3' => $params['P14'],		//P14
            	'from_api' => 'YES',
            	'password' => $params['P15'], //P15
            	'merchant_id' => $merchant_id,
            	'date_of_birth' => isset($params['P17']) ? $params['P17'] : '',
             );
            return $this->process_method('register_customer', $params);
        }
   }
  
    public function execute() {
        if (isset($_REQUEST['method'])) {
            $method = strtolower($_REQUEST['method']);
            if (method_exists($this->instance, $method)) {
                $this->method = $method;
                $this->$method();
                return TRUE;
            }
        }
        return $this->failed('INVALID_METHOD');
    }

}

$instance = new pos_api();
$instance->execute();
