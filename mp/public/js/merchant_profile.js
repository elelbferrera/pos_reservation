$(document).ready(function() {
	
	$('#btnFavorite').click(function(){
		LoadingAlert("Adding as favorite");
		var postdata = $("#frmFavorite").serialize();	

    	$.postJSON("?action=save_as_favorite", postdata, function(data) {
            if(data.success)
            {
            	CloseLoading();
            	CustomAlert(data.message, "Success");
				setTimeout(function(){
				//   window.location.reload(1);
					window.location.href =window.location.href;
				}, 2000);
            	// window.location.href = data.redirect;
            }else{
            	CloseLoading();
            	CustomAlert(data.message, "Failed");
            }
        });
	    return false;
	});
	$('#btnSubmitReview').click(function(){
		var review = $('#txtReviewMessage').val();
		
		if(review=="")
		{
			CustomAlert("Enter your comment/review.", "Unable to submit review");
			return false;
		}
		
		LoadingAlert("Submitting Review");
		var postdata = $("#frmReview").serialize();	

    	$.postJSON("?action=create_review", postdata, function(data) {
            if(data.success)
            {
            	CloseLoading();
            	// $('#checkin').hide();
            	// $('#divReservationTitle').hide();
            	// $('#divSuccessReservation').show();
            	
            	CustomAlert(data.message, "Success");
				setTimeout(function(){
				   //window.location.reload(1);
				   window.location.href =window.location.href;
				}, 2000);
            	// window.location.href = data.redirect;
            }else{
            	CloseLoading();
            	CustomAlert(data.message, "Failed");
            }
        });
	    return false;
	});
	
	$('#txtContactNumber').mask("999-999-9999");
	$('#btnCheckMeIn').click(function(){
	    	
	    	var last_name = $('#txtLastName').val();
	    	var first_name = $('#txtFirstName').val();
	    	var contact_number = $('#txtContactNumber').val();
	    	
	    	if(first_name.trim()=="")
	    	{
	    		CustomAlert("Enter your first name", "Unable to Check In");
	    		return false;
	    	}
	    	
	    	if(last_name.trim()=="")
	    	{
	    		CustomAlert("Enter your last name.", "Unable to Check In");
	    		return false;
	    	}
	    	
	    	if(contact_number.trim()=="")
	    	{
	    		CustomAlert("Enter your contact number", "Unable to Check In");
	    		return false;
	    	}

        var checkboxes = document.getElementsByName("chkServices[]");
        var services ="";
          for (var i=0; i<checkboxes.length; i++) {
            if(checkboxes[i].checked)
            {  
            services = services + checkboxes[i].value +",";
          }
        }
        services = services.substr(0, services.length-1);
        $('#txtServices').val(services);




        // alert($('#txtServices').val());
        // return false;
	    	
	    LoadingAlert("Check In");
			var postdata = $("#frmCheckIn").serialize();	
			
			contact_number = contact_number.replace("-","");	
			contact_number = contact_number.replace("-","");	
			postdata = postdata + "&txtContactNumber="+ contact_number; 
	    	$.postJSON("?action=check_me_in", postdata, function(data) {
	            if(data.success)
	            {
	            	CloseLoading();
	            	// $('#checkin').hide();
                $('#servicesmodal').hide();
	            	// $('#divReservationTitle').hide();
	            	// $('#divSuccessReservation').show();
	            	$('#txtLastName').val('');
                $('#txtFirstName').val('');
                $('#txtContactNumber').val('');
                $("input[name='chkServices[]']").removeAttr('checked');
	            	CustomAlert(data.message, "Success");
	            	// window.location.href = data.redirect;
	            }else{
	            	CloseLoading();
	            	CustomAlert(data.message, "Unable to login");
	            }
	        });
	        
	    return false;
	    	
	    	
	});

});

function initMap() {
	
    // var chicago = new google.maps.LatLng(41.850, -87.650);
    var lat = $("#txtLatitude").val();
    var lon = $("#txtLongitude").val();
    

    var city = $("#txtCity").val();
    var state = $("#txtState").val();
    var zip = $("#txtZip").val();
    var address = $("#txtAddress").val();
    var business_name = $("#txtBusinessName").val();
    
    
    var chicago = new google.maps.LatLng(lat, lon);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: chicago,
      zoom: 10,
    });
    var myLatLng = {lat: parseFloat($("#txtLatitude").val()), lng: parseFloat($("#txtLongitude").val())};

    
	  var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    title: city + ', ' + state + ' ' + zip + ' ' + address
	  });

    

    // var coordInfoWindow = new google.maps.InfoWindow();
    // coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
    // coordInfoWindow.setPosition(chicago);
    // coordInfoWindow.open(map);
// 
    // map.addListener('zoom_changed', function() {
      // coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
      // coordInfoWindow.open(map);
    // });
  }

  var TILE_SIZE = 656;

  function createInfoWindowContent(latLng, zoom) {
    var scale = 1 << zoom;

    var worldCoordinate = project(latLng);

    var pixelCoordinate = new google.maps.Point(
        Math.floor(worldCoordinate.x * scale),
        Math.floor(worldCoordinate.y * scale));

    var tileCoordinate = new google.maps.Point(
        Math.floor(worldCoordinate.x * scale / TILE_SIZE),
        Math.floor(worldCoordinate.y * scale / TILE_SIZE));
        
    var city = $("#txtCity").val();
    var state = $("#txtState").val();
    var zip = $("#txtZip").val();
    var address = $("#txtAddress").val();
    var business_name = $("#txtBusinessName").val();

    return [
      'Business Name: ' + business_name,
      'Address: ' + city + ', ' + state  ,
      address + ' ' + zip,
      // 'LatLng: ' + latLng,
      // 'Zoom level: ' + zoom,
      // 'World Coordinate: ' + worldCoordinate,
      // 'Pixel Coordinate: ' + pixelCoordinate,
      // 'Tile Coordinate: ' + tileCoordinate
    ].join('<br>');
  }

  // The mapping between latitude, longitude and pixels is defined by the web
  // mercator projection.
  function project(latLng) {
    var siny = Math.sin(latLng.lat() * Math.PI / 180);

    // Truncating to 0.9999 effectively limits latitude to 89.189. This is
    // about a third of a tile past the edge of the world tile.
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);

    return new google.maps.Point(
        TILE_SIZE * (0.5 + latLng.lng() / 360),
        TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
  }




jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});