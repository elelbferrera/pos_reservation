$(document).ready(function() {
	
	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	            $('.profile-dp').attr('src', e.target.result);
	        }
	        
	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#file").change(function(){
	    readURL(this);
	});
	
	$('#btnResetNow').click(function(){
		var contact_number = $('#txtContactNumber').val();
		if(contact_number.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Unable to reset");
			return false;	
		}
		
		var verify_code = $('#txtVerificationCode').val();
		if(verify_code.trim() == "")
		{
			CustomAlert("Enter your SMS verification code", "Unable to reset");
			return false;
		}
		
		
		var new_password = $('#txtNewPassword').val();
		if(new_password.trim()=="")
		{
			CustomAlert("Enter your new password", "Unable to reset");
			return false;
		}  
		
		
		var repeat_password = $('#txtRepeatPassword').val();
		if(new_password.trim()!=repeat_password.trim())
		{
			CustomAlert("New Password and Repeat Password must be the same.", "Unable to reset");
			return false;
		}  
		
		
		LoadingAlert("Resetting password");
		var postdata = $("#frmReset").serialize();
		contact_number = contact_number.replace("-", "");
		contact_number = contact_number.replace("-", "");	
		postdata = postdata + "&txtContactNumber="+contact_number;
		
    	$.postJSON("?action=reset_me_now", postdata, function(data) {
            if(data.success)
            {
            	CloseLoading();
            	CustomAlert(data.message, "Reset Password");
            	// window.location.href = data.redirect;
            	setTimeout(function(){
				   //window.location.reload(1);
				   window.location.href =data.redirect;
				}, 2000);
            }else{
            	CloseLoading();
            	CustomAlert(data.message, "Unable to reset");
            }
        });
	    return false;
		
	});
	
	$('#btnGeneratePasscode').click(function(){
		var contact_number = $('#txtContactNumber').val();
		if(contact_number.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Incomplete Fields");
			return false;	
		}
		
		LoadingAlert("Sending verification code");
		var postdata = $("#frmForgot").serialize();
		contact_number = contact_number.replace("-", "");
		contact_number = contact_number.replace("-", "");	
		postdata = postdata + "&txtContactNumber="+contact_number;
		
    	$.postJSON("?action=forgot_me_now", postdata, function(data) {
            if(data.success)
            {
            	CloseLoading();
            	CustomAlert(data.message, "Forgot Password");
            	// window.location.href = data.redirect;
            	setTimeout(function(){
				   //window.location.reload(1);
				   window.location.href =data.redirect;
				}, 2000);
            }else{
            	CloseLoading();
            	CustomAlert(data.message, "Unable to register customer");
            }
        });
	    return false;
		
	});
	
	$('#frmRegisterMe').submit(function(){
		var first_name = $('#txtFirstName').val();
		if(first_name.trim()=="")
		{
			CustomAlert("First Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var last_name = $('#txtLastName').val();
		if(last_name.trim()=="")
		{
			CustomAlert("Last Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var contact_number = $('#txtContactNumber').val();
		if(contact_number.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Incomplete Fields");
			return false;	
		}
		
		var zip = $('#txtZip').val();
		if(zip.trim()=="")
		{
			CustomAlert("Zip is required.", "Incomplete Fields");
			return false;	
		}
		
		LoadingAlert("Updating customer information");
		// var postdata = $("#frmRegisterMe").serialize();
		// contact_number = contact_number.replace("-", "");
		// contact_number = contact_number.replace("-", "");	
		// postdata = postdata + "&txtContactNumber="+contact_number;
		
    	$.ajax({
            url: "?action=update_me", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            dataType: 'json',
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {  
                if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	CloseLoading();
	            	CustomAlert(data.message, "Update Customer");
	            	// $('#divSignup').hide();
	            	// $('#divSuccess').show();
	            	// window.location.href = data.redirect;
	            	setTimeout(function(){
					   //window.location.reload(1);
					   window.location.href =data.redirect;
					}, 2000);
	            	
	            }else{
	            	CloseLoading();
	            	CustomAlert(data.message, "Unable to register customer");
	            }
            }
    	});
        return false;
		
		
	});
	
	
	$('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
	
	
	$('#transaction').paginate();
	$('#book').paginate();
	$('#Favorite').paginate();
	
	
	$('#btnLogin').click(function(){
		
		var username = $('#txtUsername').val();
		if(username.trim()=="")
		{
			CustomAlert("Enter your username", "Incomplete Fields");
			return false;	
		}
		
		var password = $('#txtPassword').val();
		if(password.trim()=="")
		{
			CustomAlert("Enter your password.", "Incomplete Fields");
		}
		
		
		LoadingAlert("Logging In");
		var postdata = $("#frmLogin").serialize();	
	    	$.postJSON("?action=logmein", postdata, function(data) {
	            if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	CloseLoading();
	            	window.location.href = data.redirect;
	            	
	            }else{
	            	CloseLoading();
	            	CustomAlert(data.message, "Unable to login");
	            }
	        });
	        
	    return false;
		
	});
	
	
	$('#txtContactNumber').mask("999-999-9999");
	$('#txtZip').mask("99999");
	$('#btnRegister').click(function(){
		// LoadingAlert("Hello");
		// return false;
		
		// $('#divSuccess').show();
		// return false;
		var ischeck = $('#chkRegister').is(":checked");

		
		var first_name = $('#txtFirstName').val();
		if(first_name.trim()=="")
		{
			CustomAlert("First Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var last_name = $('#txtLastName').val();
		if(last_name.trim()=="")
		{
			CustomAlert("Last Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var contact_number = $('#txtContactNumber').val();
		if(contact_number.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Incomplete Fields");
			return false;	
		}
		
		var zip = $('#txtZip').val();
		if(zip.trim()=="")
		{
			CustomAlert("Zip is required.", "Incomplete Fields");
			return false;	
		}
		
		var username = $('#txtUsername').val();
		if(username.trim()=="")
		{
			CustomAlert("Username is required.", "Incomplete Fields");
			return false;	
		}
		
		var password = $('#txtPassword').val();
		if(password.trim()=="")
		{
			CustomAlert("Password is required.", "Incomplete Fields");
			return false;	
		}
		
		var repeat_password = $('#txtRepeatPassword').val();
		if(repeat_password.trim()!=password.trim())
		{
			CustomAlert("Password and Repeat Password should be equal.", "Incomplete Fields");
			return false;	
		}
		
		if(!ischeck)
		{
			CustomAlert("Please check the agree to terms and condition.");
			return false;
		}
		
		LoadingAlert("Registration");
		var postdata = $("#frmRegisterMe").serialize();
		contact_number = contact_number.replace("-", "");
		contact_number = contact_number.replace("-", "");	
		postdata = postdata + "&txtContactNumber="+contact_number;
		
	    	$.postJSON("?action=register_me", postdata, function(data) {
	            if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	CloseLoading();
	            	CustomAlert(data.message, "Register Customer");
	            	$('#divSignup').hide();
	            	$('#divSuccess').show();
	            	// window.location.href = data.redirect;
	            	
	            }else{
	            	CloseLoading();
	            	CustomAlert(data.message, "Unable to register customer");
	            }
	        });
	        
	    return false;
	});
});


function add()
{
	
	$('#txtOpeningTime').val("");
	$('#txtClosingTime').val("");
	
	$('#txtBranchId').val("-1");
	
	
	$('#txtBranchName').val("");
	
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtZip').val("");
	$('#txtContactNumber').val("");
	$('#txtLongitude').val("");
	$('#txtLatitude').val("");
	
	var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	$('#dlgEditBranch').modal('show');
}

function edit(id)
{
	var branch_name = document.getElementById(id+'branch_name').innerHTML;
	var branch_id = id;
	var longitude = document.getElementById(id+'longitude').innerHTML;
	var latitude = document.getElementById(id+'latitude').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var store_day = document.getElementById(id+'store_day').innerHTML;
	var zip = document.getElementById(id+'zip').innerHTML;
	var contact_no = document.getElementById(id+'contact_number').innerHTML;
	var opening_time = document.getElementById(id+'store_start_time').innerHTML;
	var closing_time = document.getElementById(id+'store_end_time').innerHTML;
	
	
	// alert(d);
	// return false;
	
	opening_time = convertToAMPM(opening_time);
	closing_time = convertToAMPM(closing_time);
	
	$('#txtOpeningTime').val(opening_time);
	$('#txtClosingTime').val(closing_time);
	
	$('#txtBranchId').val(id);
	
	
	$('#txtBranchName').val(branch_name);
	
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtZip').val(zip);
	$('#txtContactNumber').val(contact_no);
	$('#txtLongitude').val(longitude);
	$('#txtLatitude').val(latitude);
	
	var days = store_day+",";
	
	var checkboxes = document.getElementsByName("days[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = days.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	$('#dlgEditBranch').modal('show');
}


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});