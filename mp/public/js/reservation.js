$(document).ready(function() {


	$("#txtDropSpecialist").change(function() {
  		// alert($('#txtMerchantId').val());
  		LoadingAlert("Loading Services");	
  		postdata = "&merchant_id=" + $('#txtMerchantId').val() + "&specialist_id=" +$('#txtDropSpecialist').val();
	    	$.postJSON("?action=get_services", postdata, function(data) {
	            if(data.success)
	            {
	            	document.getElementById('services-cont').innerHTML = data.message;
	            	CloseLoading();	
	            }else{
	            	document.getElementById('services-cont').innerHTML = data.message;
	            	CloseLoading();
	            }
	           // $("#divSpecialist").show();
	        });
	});

	$('#ulListReserve').paginate();
	
	$('#txtDate').on("propertychange change keyup paste input", function() {
     	var checkboxes = document.getElementsByName("chkServices[]");
		// var services ="";
	 //    for (var i=0; i<checkboxes.length; i++) {
		//    	if(checkboxes[i].checked)
		//    	{  
		// 		services = services + checkboxes[i].value +",";
		// 	}
		// }
  //   	services = services.substr(0, services.length-1);
		// $('#txtServices').val(services);

		// var total_count=$('#txtInc').val()
		
		// total_count = parseInt(total_count) + 1;
		// $('#txtInc').val(total_count);
		// if(total_count == 3)
		// {
		// 	LoadingAlert("Loading Specialilst");	
		// 	postdata = "&services=" + services + "&date=" +$('#txtDate').val() + "&merchant_id=" +$('#txtMerchantId').val();
			
	 //    	$.postJSON("?action=get_salon_specialist", postdata, function(data) {
	 //            if(data.success)
	 //            {
	 //            	document.getElementById('ulSpecialist').innerHTML = data.message;
	 //            	CloseLoading();	
	 //            }else{
	 //            	document.getElementById('ulSpecialist').innerHTML = data.message;
	 //            	CloseLoading();
	 //            }
	 //            $("#divSpecialist").show();
	 //        });
	 //        $('#txtInc').val("0");
		// }

	});


	
	$('#txtDate').datepicker({
		autoclose: true,
    	format : "mm-dd-yyyy",
    	startDate: "0d"
    });

    $('#txtDate').datepicker('update');
    
        //Timepicker
    $(".timepicker").timepicker({
      showInputs: false,
      // autoclose: true,
      

    });
    
    $('#btnSetTime').click(function(){
    	$('#tPicker').removeClass('open').addClass('');
	});
    
    $('#btnSubmit').click(function(){
    	
    	var last_name = $('#txtLastName').val();
    	var first_name = $('#txtFirstName').val();
    	var contact_number = $('#txtContactNumber').val();
    	var date = $('#txtDate').val();
    	var time = $('#txtTime').val();

    	/*var new_date = new Date(date);

		var d = new_date.getDate();
	 	if(d <= 9)
	 	{
	 		d = "0" + d;
	 	}
	 	var m = new_date.getMonth() + 1;

	 	if (m < 10) {
	 		m = "0" + m;
	 	}
	 	var y = new_date.getFullYear();

	 	$('#txtDate').val(y+'-'+m+'-'+d);*/
	 	var new_date = date.split("-");
	 	new_date = new_date[2] + "-" + new_date[0] + "-" + new_date[1];
    	
    	
    	if(last_name.trim()=="")
    	{
    		CustomAlert("Enter your last name.", "Unable to reserve");
    		return false;
    	}
    	
    	if(first_name.trim()=="")
    	{
    		CustomAlert("Enter your first name", "Unable to reserve");
    		return false;
    	}
    	
    	if(contact_number.trim()=="")
    	{
    		CustomAlert("Enter your contact number", "Unable to reserve");
    		return false;
    	}
    	
    	if(date.trim()=="")
    	{
    		CustomAlert("Enter your preferred date.", "Unable to reserve");
    		return false;
    	}
    	
    	if(time.trim()=="")
    	{
    		CustomAlert("Enter your preferred time.", "Unable to reserve");
    		return false;
    	}

	    var checkboxes = document.getElementsByName("chkServices[]");
		var services ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				services = services + checkboxes[i].value +",";
			}
		}
	    
	    if(services.length==0)
		{
			CustomAlert("Please select atleast one service", "Unable to reserve");
			return false;
		}
    	
    	services = services.substr(0, services.length-1);
		$('#txtServices').val(services);
		
		// var checkboxes = document.getElementsByName("chkSpecialist[]");
		// var specialist ="";
	 //    for (var i=0; i<checkboxes.length; i++) {
		//    	if(checkboxes[i].checked)
		//    	{  
		// 		specialist = specialist + checkboxes[i].value +",";
		// 	}
		// }
		
		// if(specialist.length > 0)
		// {
		// 	specialist = specialist.substr(0, specialist.length-1);
		// }
		
		var specialist = "";
		if($('#txtDropSpecialist').val()>0)
		{
			specialist = $('#txtDropSpecialist').val();
		}

		$('#txtSpecialist').val(specialist);
		
		
		LoadingAlert("Creating Reservation");
		var postdata = $("#frmReservation").serialize();	

		var contact_number = $('#txtContactNumber').val();
		contact_number = contact_number.replace("-","");
		contact_number = contact_number.replace("-","");
		postdata = postdata + "&txtTime=" + convertTo24Hour(time);
		postdata = postdata + "&txtContactNumber=" + contact_number;
		postdata = postdata + "&txtDateNew=" + new_date;

		//register customer
		var email = $("#txtEmail").val();
		var baseUrl = $("#txtWebRoot").val();
		postdata = postdata + "&txtMiddleName=&txtState=&txtAddress=&txtZip=&txtCity=";
		postdata = postdata + "&txtGender=&txtBirthDate=&txtUsername=&txtPassword=&txtEmailAddress="+email;
	    	//check if contact number already exists
	    	$.postJSON("?action=check_contact_number", postdata, function(data) {
	       	console.log(data);
	            if(data.success == "true")
	            {
	            	//update -- if contact number exist
			    	$.postJSON(baseUrl + "/customer/sign_up?action=update_contact", postdata, function(data) {
			            if(data.success)
			            {
			            	
			            	
			            }else{
			            	CloseLoading();
			            	CustomAlert(data.message, "Unable to register customer");
			            }
			        });
			    	// return false;
	            	
	            }else{

	            	//add -- if contact number does not exist
			    	$.postJSON(baseUrl + "/customer/sign_up?action=register_me", postdata, function(data) {
			            if(data.success)
			            {
			            	
			            	
			            }else{
			            	CloseLoading();
			            	CustomAlert(data.message, "Unable to register customer");
			            }
			        });
			    	// return false;

	            }
	        });

	    	// return false;

	    //end register
		
    	$.postJSON("?action=create_reservation", postdata, function(data) {
            if(data.success)
            {
            	
            	// var url = window.location.href;
            	// url = url.replace("#",""); 
            	CloseLoading();
            	$('#merchant-profile').hide();
            	$('#divReservationTitle').hide();
            	$('#divSuccessReservation').show();

				var delay=5000; //3 second
				setTimeout(function() {
					// $('#dlg_message').modal('hide');
					// $('#merchant-profile').show();
     //        		$('#divReservationTitle').show();
     //        		$('#divSuccessReservation').hide();
     				window.location.href = window.location.href;
				}, delay);

            	
            	// CustomAlert(data.message, "Success");
            	// window.location.href = data.redirect;
            	
            	
            }else{
            	CloseLoading();
            	if(data.message =="Conflict")
            	{
            		CustomAlert("Time is unavailable. Please check suggested schedule.", "Reservation");
            		// document.getElementById('ulListReserve').innerHTML = data.suggestions;
            		// $('#ulListReserve').paginate();

            		$('#ulListReserve').data('paginate').kill()
            		document.getElementById('ulListReserve').innerHTML = data.suggestions;
            		// document.getElementById('ulListReserve').innerHTML ="";
            		// $('#ulListReserve').append(data.suggestions);
        			$('#ulListReserve').paginate();
            		$('#divSuggestion').show();
            		//ulListReserve innerHTML
            		//divSuggestion show

            	}else{
            		CustomAlert(data.message, "Unable to Reserve" ,"error");	
            	}
            	
            }
        });
	        
	    return false;
    });
       
    
    
	// var checkboxes = document.getElementsByName("chkServices[]");
	// var services ="";
    // for (var i=0; i<checkboxes.length; i++) {
	   	// if(checkboxes[i].checked)
	   	// {  
			// services = services + checkboxes[i].value +",";
		// }
	// }
// 	
	// if(services.length==0)
	// {
		// CustomAlert("Please select atleast one services.", "Unable to reserve");
		// return false;
	// }
// 	
	// services = services.substr(0, services.length-1);
	// $('#txtServices').val(services);
// 	
	// var checkboxes = document.getElementsByName("specialist[]");
	// var specialist ="";
    // for (var i=0; i<checkboxes.length; i++) {
	   	// if(checkboxes[i].checked)
	   	// {  
			// specialist = specialist + checkboxes[i].value +",";
		// }
	// }
    // specialist = specialist.substr(0, specialist.length-1);
	// $('#txtSpecialist').val(specialist);
    
    

        
    // autoclose: true,
    //Timepicker
    // $(".timepicker").timepicker({
      // showInputs: false,
      // autoclose: true,
    // });
// 	
// 	
	// $('#btnGoToStep2').click(function(){
		// $("#step1").hide();
		// $("#step2").show();
	// });
// 	
	// $('#btnBackStep1').click(function(){
		// $("#step2").hide();
		// $("#step1").show();
	// });
// 	
	// $('#btnGoToStep3').click(function(){
		// $("#step2").hide();
		// $("#step3").show();	
	// });
// 	
	// $('#btnBackStep2').click(function(){
		// $("#step2").show();
		// $("#step3").hide();
	// });
	
	
	$('#btnLogin').click(function(){
		
		var username = $('#txtUsername').val();
		if(username.trim()=="")
		{
			CustomAlert("Enter your username", "Incomplete Fields");
			return false;	
		}
		
		var password = $('#txtPassword').val();
		if(password.trim()=="")
		{
			CustomAlert("Enter your password.", "Incomplete Fields");
		}
		
		
		LoadingAlert("Logging In");
		var postdata = $("#frmLogin").serialize();	
	    	$.postJSON("?action=logmein", postdata, function(data) {
	            if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	CloseLoading();
	            	window.location.href = data.redirect;
	            	
	            }else{
	            	CustomAlert(data.message, "Unable to login");
	            }
	        });
	        
	    return false;
		
	});
	
	
	$('#txtContactNumber').mask("999-999-9999");
	// $('#txtZip').mask("999999");
	$('#btnRegister').click(function(){
		// LoadingAlert("Hello");
		// return false;
		
		// $('#divSuccess').show();
		// return false;
		var ischeck = $('#chkRegister').is(":checked");

		
		var first_name = $('#txtFirstName').val();
		if(first_name.trim()=="")
		{
			CustomAlert("First Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var last_name = $('#txtLastName').val();
		if(last_name.trim()=="")
		{
			CustomAlert("Last Name is required.", "Incomplete Fields");
			return false;	
		}
		
		var contact_number = $('#txtContactNumber').val();
		if(contact_number.trim()=="")
		{
			CustomAlert("Contact Number is required.", "Incomplete Fields");
			return false;	
		}
		
		var zip = $('#txtZip').val();
		if(zip.trim()=="")
		{
			CustomAlert("Zip is required.", "Incomplete Fields");
			return false;	
		}
		
		var username = $('#txtUsername').val();
		if(username.trim()=="")
		{
			CustomAlert("Username is required.", "Incomplete Fields");
			return false;	
		}
		
		var password = $('#txtPassword').val();
		if(password.trim()=="")
		{
			CustomAlert("Password is required.", "Incomplete Fields");
			return false;	
		}
		
		var repeat_password = $('#txtRepeatPassword').val();
		if(repeat_password.trim()!=password.trim())
		{
			CustomAlert("Password and Repeat Password should be equal.", "Incomplete Fields");
			return false;	
		}
		
		if(!ischeck)
		{
			CustomAlert("Please check the agree to terms and condition.");
			return false;
		}
		
		LoadingAlert("Registration");
		var postdata = $("#frmRegisterMe").serialize();
		contact_number = contact_number.replace("-","");	
		contact_number = contact_number.replace("-","");	
		postdata = postdata + "&txtContactNumber="+ contact_number;
	    	$.postJSON("?action=register_me", postdata, function(data) {
	            if(data.success)
	            {
	            	// var url = window.location.href;
	            	// url = url.replace("#",""); 
	            	CloseLoading();
	            	CustomAlert(data.message, "Register Customer");
	            	$('#divSignup').hide();
	            	$('#divSuccess').show();
	            	// window.location.href = data.redirect;
	            	
	            }else{
	            	CloseLoading();
	            	CustomAlert(data.message, "Unable to register customer");
	            }
	        });
	        
	    return false;
	});

	/// if get parameters exist

	var params = location.search;
    var lastName = getParameterByName('last_name', params);
    var firstName = getParameterByName('first_name', params);
    var emailAdd = getParameterByName('email_address', params);
    var contactNo = getParameterByName('phone_number', params);
    
    if(params != ''){
      // $('header').hide();
      // $('footer#footer').hide();
      // $('.side-display').hide();
      // $('#main-page').hide();
      // $('#merchant-profile').css('margin-top', '-100px');

      $('#txtFirstName').val(firstName).attr('readonly', 'readonly');
      $('#txtLastName').val(lastName).attr('readonly', 'readonly');
      $('#txtEmail').val(emailAdd).attr('readonly', 'readonly');
      $('#txtContactNumber').val(contactNo).attr('readonly', 'readonly');
    }
});

function getParameterByName( name,href )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( href );
  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}


function add()
{
	
	$('#txtOpeningTime').val("");
	$('#txtClosingTime').val("");
	
	$('#txtBranchId').val("-1");
	
	
	$('#txtBranchName').val("");
	
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtZip').val("");
	$('#txtContactNumber').val("");
	$('#txtLongitude').val("");
	$('#txtLatitude').val("");
	
	var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	$('#dlgEditBranch').modal('show');
}

function edit(id)
{
	var branch_name = document.getElementById(id+'branch_name').innerHTML;
	var branch_id = id;
	var longitude = document.getElementById(id+'longitude').innerHTML;
	var latitude = document.getElementById(id+'latitude').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var store_day = document.getElementById(id+'store_day').innerHTML;
	var zip = document.getElementById(id+'zip').innerHTML;
	var contact_no = document.getElementById(id+'contact_number').innerHTML;
	var opening_time = document.getElementById(id+'store_start_time').innerHTML;
	var closing_time = document.getElementById(id+'store_end_time').innerHTML;
	
	
	// alert(d);
	// return false;
	
	opening_time = convertToAMPM(opening_time);
	closing_time = convertToAMPM(closing_time);
	
	$('#txtOpeningTime').val(opening_time);
	$('#txtClosingTime').val(closing_time);
	
	$('#txtBranchId').val(id);
	
	
	$('#txtBranchName').val(branch_name);
	
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtZip').val(zip);
	$('#txtContactNumber').val(contact_no);
	$('#txtLongitude').val(longitude);
	$('#txtLatitude').val(latitude);
	
	var days = store_day+",";
	
	var checkboxes = document.getElementsByName("days[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = days.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	$('#dlgEditBranch').modal('show');
}
function check_service_change(el){
  // if(el.checked){
  // 		if($('#txtDate').val().trim()!="")
  // 		{
	 //  		var checkboxes = document.getElementsByName("chkServices[]");
		// 	var services ="";
		//     for (var i=0; i<checkboxes.length; i++) {
		// 	   	if(checkboxes[i].checked)
		// 	   	{  
		// 			services = services + checkboxes[i].value +",";
		// 		}
		// 	}
	 //    	services = services.substr(0, services.length-1);
		// 	$('#txtServices').val(services);

		// 	var total_count=$('#txtInc').val()

		// 	LoadingAlert("Loading Specialilst");	
		// 	postdata = "&services=" + services + "&date=" +$('#txtDate').val() + "&merchant_id=" +$('#txtMerchantId').val();
			
	 //    	$.postJSON("?action=get_salon_specialist", postdata, function(data) {
	 //            if(data.success)
	 //            {
	 //            	document.getElementById('ulSpecialist').innerHTML = data.message;
	 //            	CloseLoading();	
	 //            }else{
	 //            	document.getElementById('ulSpecialist').innerHTML = data.message;
	 //            	CloseLoading();
	 //            }
	 //            $("#divSpecialist").show();
	 //        });	
		// }
  // }
}

function ReserveMe(suggest_id)
{
	var content =  $('#txtData'+suggest_id).val();

	var conts = content.split("|");

	// alert(conts[0]);
	// return false;

	// alert(content);
	// return false;
	var date = conts[1].split("-");
	$('#txtTime').val(conts[0]);
	$('#txtDate').val(date[1]+"-"+date[2]+"-"+date[0]);
	$('#txtDropSpecialist').val(conts[2]);


	var checkboxes = document.getElementsByName("chkServices[]");
	var services ="";
    for (var i=0; i<checkboxes.length; i++) {
   	 	// 
   	 	if(checkboxes[i].value == conts[3])
   	 	{
   	 		checkboxes[i].checked = true;
   	 	}
   	 	// alert(checkboxes[i].value);
	}

	// $('#txtFirstName').val("");
	$( "#btnSubmit" ).trigger( "click" );
	return false;
}

function select_specialist(specialist_id) {
	$('#divSuggestion').remove();
	$('.coupon_content').remove();
	$('#txtDropSpecialist').val(specialist_id);
	$('.open2').trigger('click');
}

jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});