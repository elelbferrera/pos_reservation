$(document).ready(function() {
	
	$('#txtValue').keydown(function(event){ 
	    var keyCode = (event.keyCode ? event.keyCode : event.which);   
	    if (keyCode == 13) {
	        $('#btnSearch').trigger('click');
	    }
	});
	
	$('#btnSearch').click(function(){
		var url = $('#txtURL').val();
		var search = $('#txtValue').val();
		var redirect_url = url + '/' + search;
		
		if(search.trim()=="")
		{
			var redirect_url = url + '/allstores';
			/*CustomAlert("Enter a keyword to search", "Invalid Keyword");
			return false;*/
		}
		
		window.location.href = redirect_url;

	});

	if (window.location.href.substr(window.location.href.lastIndexOf('/') + 1) !== 'allstores') {
		$('#example').paginate();
	}
	
	/*$('#example').paginate();*/
	
	 $("#range_1").ionRangeSlider({
      min: 0,
      max: 5000,
      from: 1000,
      to: 4000,
      type: 'double',
      step: 1,
      prefix: "$",
      prettify: false,
      hasGrid: true
    });
    
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) { 
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
	
	$('#btnSetSchedule').click(function (){
		var checkboxes = document.getElementsByName("days[]");
		var days ="";
	    for (var i=0; i<checkboxes.length; i++) {
		   	if(checkboxes[i].checked)
		   	{  
				days = days + checkboxes[i].value +",";
			}
		}
		
		days = days.substr(0, days.length-1);
		$('#txtDays').val(days);
		
		$('#setschedule').modal('hide');
		
	});

});


function add()
{
	
	$('#txtOpeningTime').val("");
	$('#txtClosingTime').val("");
	
	$('#txtBranchId').val("-1");
	
	
	$('#txtBranchName').val("");
	
	$('#txtAddress').val("");
	$('#txtCity').val("");
	$('#txtCountry').val("");
	$('#txtState').val("");
	$('#txtZip').val("");
	$('#txtContactNumber').val("");
	$('#txtLongitude').val("");
	$('#txtLatitude').val("");
	
	var checkboxes = document.getElementsByName("days[]");
    for (var i=0; i<checkboxes.length; i++) {
	     checkboxes[i].checked = false;
	}
	
	$('#dlgEditBranch').modal('show');
}

function edit(id)
{
	var branch_name = document.getElementById(id+'branch_name').innerHTML;
	var branch_id = id;
	var longitude = document.getElementById(id+'longitude').innerHTML;
	var latitude = document.getElementById(id+'latitude').innerHTML;
	var city = document.getElementById(id+'city').innerHTML;
	var address = document.getElementById(id+'address').innerHTML;
	var state = document.getElementById(id+'state').innerHTML;
	var country = document.getElementById(id+'country').innerHTML;
	var store_day = document.getElementById(id+'store_day').innerHTML;
	var zip = document.getElementById(id+'zip').innerHTML;
	var contact_no = document.getElementById(id+'contact_number').innerHTML;
	var opening_time = document.getElementById(id+'store_start_time').innerHTML;
	var closing_time = document.getElementById(id+'store_end_time').innerHTML;
	
	
	// alert(d);
	// return false;
	
	opening_time = convertToAMPM(opening_time);
	closing_time = convertToAMPM(closing_time);
	
	$('#txtOpeningTime').val(opening_time);
	$('#txtClosingTime').val(closing_time);
	
	$('#txtBranchId').val(id);
	
	
	$('#txtBranchName').val(branch_name);
	
	$('#txtAddress').val(address.trim());
	$('#txtCity').val(city.trim());
	$('#txtCountry').val(country);
	$('#txtState').val(state);
	$('#txtZip').val(zip);
	$('#txtContactNumber').val(contact_no);
	$('#txtLongitude').val(longitude);
	$('#txtLatitude').val(latitude);
	
	var days = store_day+",";
	
	var checkboxes = document.getElementsByName("days[]");
	for (var i=0; i<checkboxes.length; i++) {
	     var n = days.includes(checkboxes[i].value+",");
	     if(n)
	     {
	     	checkboxes[i].checked = true;	
	     }else{
	     	checkboxes[i].checked = false;
	     }
	}
	
	$('#dlgEditBranch').modal('show');
}


jQuery.extend({
   postJSON: function( url, data, callback) {
      return jQuery.post(url, data, callback, "json");
   }
});