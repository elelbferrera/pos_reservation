<?php
require_once './prerun.php';

$bootstrap = new bootstrap();
$bootstrap->start();
class bootstrap {
    private $uri_arr;
    private $uri_fullpath;
    private $uri_basepath;
    private $doc_root;
    private $api_class = 'api';
    private $meta;
    private $db;
    private $force_https = false;
    
    public function start() {
        $this->set_uri_vars();
        if (!$this->check_protocol()) {
            return false;
        }
		
		define('API_URL', "http://go3mobileapp.com/v2/pos_api.php");
		
        $this->configure();
        $this->set_paths();

        // if ($this->meta['check_db'] && $this->db_check() == FALSE) {
            // echo "<b>Error: Unable to connect to the database.</b>";
            // exit;
        // }
        if ($this->meta['class']==$this->api_class && $this->meta['action'] != 'index') {
            $this->runAPI();
        } else {
            session_start();
            $result = $this->execute();
            if (null !== $result && $result != '') {
                echo "<b>Error: $result</b>";
            }
        }
		
		
    }
    
    private function runAPI() {
        require_once $this->api_class . '.php';
        if (class_exists($this->api_class)) {
            parse_str($this->meta['params'][0], $params);
            $this->meta['params'] = $params;
            $class_instance = new $this->api_class($this->meta);
            if (method_exists($class_instance, $this->meta['action'])) {
                if (method_exists($class_instance, '_beforeAction')) {
                    if (!$class_instance->_beforeAction()) {
                        if (method_exists($class_instance, '_afterAction')) {
                            $class_instance->_afterAction();
                        }
                        return NULL;
                    }
                }
                $action = $this->meta['action'];
                $class_instance->$action();
                if (method_exists($class_instance, '_afterAction')) {
                    $class_instance->_afterAction();
                }
                return NULL;
            } else {
                //todo: return invalid function called
                return NULL;
            }
        } else {
            //todo: system error api not found
        }
    }
    
    private function check_protocol() {
        
        ini_set('display_errors', false);
        if ($this->force_https == TRUE && IS_PRODUCTION && !(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            header('location: https://' . $_SERVER['SERVER_NAME'] .'/'. $this->uri_fullpath);
            return false;
        } elseif (!IS_PRODUCTION) {
            ini_set('display_errors', true);
        }
        return true;
    }
    
    private function configure() {
        $menu = array(
        	'sign_application' => 'sign_application',
            'iso' => 'iso/management',
            'sub_iso' => 'sub_iso/management',
            
            'agent' => array(
            	'add' => 'Add New Agent',
            	'management' => 'Agents'),
            
            'merchant' => array(
            	'add' => 'Add New Merchant',
            	'management' => 'Merchants',
				'application' => 'Applications',
				),
            
            'merchant_branch' => 'branch/management',
            'rewards' => 'rewards/management',
            'punch_card' => 'punch_card/management',
            'coupon' => 'coupon/management',
            'signup' => 'customer_signup',
            'customer' => 'customer/management',
            'merchant_profile' => 'merchant_profile',
//            'user' => 'users',
            'changepassword' => 'changepassword',
            'reports' => array(
                'sr'  => 'Report #1',
            ),
            'admin'   => array(
                'users'             =>  'System Users',
                'acl'               => 'Access Control List',
            ),
            'logout' => 'log/out',
        );
        
        $menu_label = array(
        	'sign_application'  => 'Sign Application',
            'iso'  => 'ISO',
            'sub_iso'  => 'Sub ISO',
            'agent'  => 'Agents',
            'merchant'  => 'Merchants',
            'merchant_branch' => 'Merchant Branches',
            'rewards'  => 'Rewards',
            'punch_card' => 'Punch Card',
            'coupon' => 'Deals',     
            'signup' => 'Customer Signup',
            'customer'  => 'List of Customers',     
//            'user' => 'Users',
			'merchant_profile' => 'My Profile',
            'changepassword'     => 'Change Password',
            'reports'       => 'Reports',
            'admin'         => 'Admin',
            'logout'        => 'Log Out',
        );
        $public_class = array('market_place','log','viewimg','calculator', 'customer', 'termsandconditions', 'privacypolicy');//, 'defaultcontroller');
		
		
        define('SUBFOLDER', '/v2/mp');
        define('SYSTEM_ADMIN', 'System Admin');
        $sub_dir = SUBFOLDER;
        
        $db_credentials = array(
            'host'      => settings::$_db['host'],
            'user'      => settings::$_db['user'],
            'password'  => settings::$_db['pass'],
            'db'        => settings::$_db['name'],
        );  
// 		

        $params = array(
            'site_name'             => 'Go3 Rewards',
            'menu'                  => $menu,
            '404_page'              => 'defaultcontroller/index',
            'accessdenied_page'     => 'defaultcontroller/accessdenied',
            'home_page' =>  'market_place/',
            //'home_page'             => 'merchant/',
            //'home_page'             => 'reservation/management',
            'max_session_minutes'   => 30,
            'db_credentials'        => $db_credentials,
            'timezone'              => 'America/New_York',
            'controllers_path'      => 'controllers',
            'public_class'          => $public_class,
            'check_db'              => true,
            'sub_dir'               => $sub_dir,
            'menu_label'            => $menu_label,
        );        
        $params['params'] = array();
        if ($this->uri_basepath == '') {
            $params['class'] = 'defaultcontroller';
            $params['action'] = 'index';
        } else {
            $params['class'] = $this->uri_arr[0];
            if (isset($this->uri_arr[1])) {
                $params['action'] = $this->uri_arr[1];
            } else {
                $params['action'] = 'index';
            }
            if (isset($_SERVER['QUERY_STRING']) && trim($_SERVER['QUERY_STRING']) != '') {
                $params['params'] = array(trim($_SERVER['QUERY_STRING']));
            } elseif (isset($this->uri_arr[2])) {
                $params['params'] = array_slice($this->uri_arr, 2);
            }
        }


        $this->meta = $params;
        
        
    }
    
    private function set_paths() {
        $this->doc_root = trim(dirname(dirname(__FILE__)), ' /') . '/app';
        if (PATH_SEPARATOR == ':') {
            $this->doc_root = '/' . $this->doc_root;
        }
        define('DOCROOT', $this->doc_root);
        
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            define('PROTOCOL' , 'https://');
        } else {
            define('PROTOCOL' , 'http://');
        }
        define('WEBROOT', PROTOCOL . $_SERVER['SERVER_NAME'] . $this->meta['sub_dir']);
		
        
        set_include_path(get_include_path() . PATH_SEPARATOR . DOCROOT  . PATH_SEPARATOR . DOCROOT . '/libraries');
    }
    
    private function set_uri_vars() {
          
        if(IS_PRODUCTION){
           //LC-01/02/2016
        /* need to revise lines of code to make it work on other server */
            $server_info = $_SERVER['PHP_SELF'].$_SERVER['ORIG_PATH_INFO'];
            $uri = explode('/', trim($server_info, '/'));
        }else{
            $uri = explode('/', trim($_SERVER['PHP_SELF'], '/'));    
        }
        
        for ($i = 0; $i <= count($uri); $i++) {
            if (strtolower($uri[$i]) == 'index.php') {
                $this->uri_arr = array_slice($uri, $i + 1);
                $this->uri_fullpath = implode('/', array_slice($uri, $i + 1));
                $this->uri_basepath = trim(implode('/', array_slice($uri, $i + 1, 2)), ' /');
                break;
            }
        }
    }
    

    public function redirect($path, $alert_message = null) {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->response = array(
                'message' => $alert_message,
                'success' => false,
                'redirect'=> WEBROOT .'/'. $path,
            );
            header('Content-type: application/json');
            echo json_encode($this->response);
            exit;
        } else {
            $message = '';
            if ($alert_message != null) {
                $message = 'alert("' . $alert_message . '");';
            }
            $path = trim($path, '/');
            $message .= 'window.location.href="' . WEBROOT .'/'. $path .'";';
            echo '<script>' . $message . '</script>';
            exit;
        }
    }
    
    private function _isSessionValid() {
        //todo: create a condition to skip setting session timer last_access (e.g. recurrent ajax calls like balance check,etc)
        $time = time();
        if ($time - $_SESSION['last_access'] > $this->meta['max_session_minutes'] * 60) {
            return false;
        } else {
            return TRUE;
        }
    }

    private function getUserPermissions($username) {
//        return array(
//            'home'                      => 'home', 
//            'merchants/registration'    => 'merchants/registration', 
//            'merchants/management'      => 'merchants/management', 
//            'cash_advances/pending'     => 'cash_advances/pending', 
//            //LC-09/11/2013 
//            'cash_advances/validated'     => 'cash_advances/validated', 
//            'cash_advances/processed'   => 'cash_advances/processed', 
//            'cash_advances/declined'    => 'cash_advances/declined', 
//            'setting/changepassword'    => 'setting/changepassword', 
//            'cash_advances/savecomment'    => 'cash_advances/savecomment',
//        );
//        $result = $this->db->fetchPairs("
//            SELECT  res.path, res.path as path2
//            FROM    ez_resources res 
//            INNER JOIN ez_permissions perm on res.id=perm.resource_id 
//            INNER JOIN ez_users usr on usr.id=perm.user_id 
//            WHERE   res.status=1
//            AND     usr.status=1
//            AND     perm.status=1
//            AND usr.username=" . $this->db->db_escape($username, true)
//            );
//        return $result;
    }
    
    private function db_check() {
        require_once('db.php');
        if (isset($this->meta['db_credentials'])) {
            $this->db = db::getInstance($this->meta['db_credentials']);
            if ($this->db->getStatus() == 0) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
        return TRUE;
    }
    
    public function execute() {
    	
		// print_r($class_file);
		// die();
		
		// print_R($this->meta['class']);
		// die();
		
		if($this->meta['class'] =="defaultcontroller")
		{
			$alert_message ="";
			return $this->redirect('marketplace/in' . base64_encode($this->uri_fullpath), $alert_message);	
		}

		
		if(in_array($this->meta['class'] ,array("marketplace","merchant","customer","waitinglist"))){
		//if($this->meta['class']=="marketplace"){
			require_once('template.php');
			$class_file = DOCROOT . '/' . $this->meta['controllers_path'] . '/' .  $this->meta['class'] . '.php';
			// print_r($class_file);
			// die();
			 
            if (file_exists($class_file)) {
                require_once($class_file);
                if (class_exists($this->meta['class'])) {
                    $this->meta['doc_root'] = DOCROOT;
                    $class_instance = new $this->meta['class']($this->meta);
                    if (method_exists($class_instance, $this->meta['action'])) {
                        $class_instance->_beforeAction();
                        $action = $this->meta['action'];
                        $class_instance->$action($this->meta['params']);
                        $class_instance->_afterAction();
                        return NULL;
                    } else {
                        $this->meta['404_page'] = $this->meta['class'] . '/index';
                    }
                }
            }
		}
		//elseif (!in_array($this->meta['class'], $this->meta['public_class']) && (!isset($_SESSION['username']) || !$this->_isSessionValid() /* || !$this->isUserActive()*/ )) {
			elseif (!in_array($this->meta['class'], $this->meta['public_class']) && (!isset($_SESSION['username']) /* || !$this->isUserActive()*/ )) {
            // if (isset($_SESSION['sessionid'])) $alert_message = 'Session expired.'; else $alert_message = '';
            // session_destroy();
            //return $this->redirect('log/in/' . base64_encode($this->uri_fullpath), $alert_message);
           
           	// print_r("hello");
			// die();
			if(isset($_SESSION['session_id']))
			{	
				
			}else{
				$alert_message ="";
				return $this->redirect('marketplace/in' . base64_encode($this->uri_fullpath), $alert_message);	
			}
			
			
            
        } else {
        	
			
            require_once('template.php');

            if (!in_array($this->meta['class'], $this->meta['public_class']) && isset($_SESSION['username']) && $this->_isSessionValid()) {
                //$this->meta['user_permissions'] = $this->getUserPermissions($_SESSION['username']);
                // $this->meta['user_permissions'] = $_SESSION['permissions'];
                
				
                foreach ($this->meta['menu'] as $key=> $val) {
                    
                    
                    
                    if (is_array($val) && count($val) > 0) {
                        foreach ($val as $key2=>$val2) {
                            if (!isset($this->meta['user_permissions'][$key . '/' . $key2])) {
                                unset($this->meta['menu'][$key][$key2]);
                            }
                        }
                        if (! (count($this->meta['menu'][$key]) > 0)) {
                            unset($this->meta['menu'][$key]);
                        }
                    }
                }
               
                if ($this->uri_basepath != '' && !(isset($this->meta['user_permissions'][$this->uri_basepath]))) {
                    return $this->_showAccessDenied();
                }
            }
            $_SESSION['last_access'] = time();
            $class_file = DOCROOT . '/' . $this->meta['controllers_path'] . '/' .  $this->meta['class'] . '.php';
			
			 
            if (file_exists($class_file)) {
                require_once($class_file);
                if (class_exists($this->meta['class'])) {
                    $this->meta['doc_root'] = DOCROOT;
                    $class_instance = new $this->meta['class']($this->meta);
                    if (method_exists($class_instance, $this->meta['action'])) {
                        $class_instance->_beforeAction();
                        $action = $this->meta['action'];
                        $class_instance->$action($this->meta['params']);
                        $class_instance->_afterAction();
                        return NULL;
                    } else {
                        $this->meta['404_page'] = $this->meta['class'] . '/index';
                    }
                }
            }
            return $this->_show404();
        }
    }

    private function _show404() {
        if (isset($this->meta['404_page'])) {
            $ex = explode('/', trim($this->meta['404_page'], ' /'));
            $class = $ex[0];
            if (count($ex) < 2) {
                $ex[1] = 'index';
            }
            $class_file = DOCROOT . '/' . $this->meta['controllers_path'] . '/' .  $class . '.php';
            require_once($class_file);
            $meta = array('class' => $ex[0], 'action' => $ex[1], 'menu' => $this->meta['menu'], 'site_name' => $this->meta['site_name'], 'timezone' => $this->meta['timezone'], 'db_credentials' => $this->meta['db_credentials'], 'menu_label' => $this->meta['menu_label']);
            $class_instance = new $class($meta);
            if (method_exists($class_instance, $ex[1])) {
                $class_instance->_beforeAction();
                $action = $ex[1];
                $class_instance->$action(array());
                $class_instance->showError("Error 404: The page you're looking for could not be found, or is not yet available.");
                $class_instance->_afterAction();
                return NULL;
            }
        }
        return 'Page not found.';
    }
            
    private function _showAccessDenied($additional_path = NULL) {
        if (isset($this->meta['accessdenied_page'])) {
            $ex = explode('/', trim($this->meta['accessdenied_page'] . $additional_path, ' /'));
            $class = $ex[0];
            if (count($ex) < 2) {
                $ex[1] = 'index';
            }
            $class_file = DOCROOT . '/' . $this->meta['controllers_path'] . '/' .  $class . '.php';
            require_once($class_file);
            $meta = array('class' => $ex[0], 'action' => $ex[1], 'menu' => $this->meta['menu'], 'site_name' => $this->meta['site_name'], 'timezone' => $this->meta['timezone'], 'db_credentials' => $this->meta['db_credentials']);
            $class_instance = new $class($meta);
            if (method_exists($class_instance, $ex[1])) {
                $class_instance->_beforeAction();
                $action = $ex[1];
                $class_instance->$action(array());
                $class_instance->showError("Error: Access to the page requested is denied.");
                $class_instance->_afterAction();
                return NULL;
            }
        }
        return 'Access to the page requested is denied.';
    }
}
