<?php
require_once './base_api.php';

class api extends base_api {
    protected $backend;
    
	protected $hello;
	
    public function __construct() {
        parent::__construct();
		
        require_once 'backend_'.PROGRAM.'.php';
        $this->backend = new backend($this->db);
    }
    
    protected function process_method($method, $params) {
        $result = $this->backend->$method($params);
        $response = $this->backend->get_response();
        if (!$result) {
            return $this->failed($response['ResponseMessage']);
        } else {
            return $this->success($response['ResponseMessage']);
        }
    }
    
    protected function get_document_types()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_document_types());
        }            
        
    }   
   
    protected function get_countries()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_countries());
        }            
    }   
    
    protected function get_capture_types()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            return $this->success($this->backend->get_capture_types());
        }            
    }   

    // parameters P01=session_id, P02=customer_ref, P03=customer_id, P04=uploaded_by, P05=doc_type1, P06=capture_type1, P07=document_id_no1, P08=doc_type2, P09=capture_type2, P10=document_id_no2
    protected function process_kyc_upload() {
        if (!$this->_param_count_check(10)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');   
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_ref'     => $this->parameters['P02'],
                'customer_id'      => $this->parameters['P03'],
                'uploaded_by'      => $this->parameters['P04'],
                'doc_type1'        => $this->parameters['P05'],
                'capture_type1'    => $this->parameters['P06'],
                'document_id_no1'  => $this->parameters['P07'],
                'doc_type2'        => $this->parameters['P08'],
                'capture_type2'    => $this->parameters['P09'],
                'document_id_no2'  => $this->parameters['P10'],
            );
            return $this->process_method('process_kyc_upload', $params);
        }
    }
    
    protected function process_salary_upload() {
        if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');   
       
        } else {
            $params = array(
                'customer_ref'     => $this->parameters['P02'],
                'customer_id'      => $this->parameters['P05'],
                'uploaded_by'      => $this->parameters['P04'],
                'capture_type'    => $this->parameters['P03'],
            );
            return $this->process_method('process_salary_upload', $params);
        }
    }
    
    
    
    // parameters P01 - customer_id, P02- amount.  P03- created_by    
    protected function register_kyc_document()
    {
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');   
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'document_type_id'  => $this->parameters['P02'],
                'customer_id'       => $this->parameters['P03'],
                'path_filename'     => $this->parameters['P04'],
                'capture_type'      => $this->parameters['P05'],
                'uploaded_by'      => $this->parameters['P06'],
            );
            return $this->process_method('register_kyc', $params);
        }
    }

    
    // parameters P01 - customer_id, P02- amount.  P03- created_by    
    protected function pay_loan()
    {
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');   
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'amount' => $this->parameters['P03'],
                'created_by' => $this->parameters['P04'],
            );
            return $this->process_method('pay_loan', $params);
        }
    }
    
    //parameter P01-customer_id
    protected function get_kyc_documents()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );
            return $this->process_method('get_kyc_documents', $params);
        }
    }
    
    //parameter P01-customer_id
    protected function view_transaction_history()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );            
            return $this->process_method('view_transaction_history', $params);
        }
    }
    
    //parameter P01-customer_id
    protected function view_credit_limit_history()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );
            return $this->process_method('view_credit_limit_history', $params);
        }
    }
    
    //parameter P01-customer_id
    protected function view_payment_history()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );
            return $this->process_method('view_payments', $params);
        }
    }
    
    //parameter P01-customer_id
    protected function view_loan_history_by_customer_id()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );            
            return $this->process_method('view_loan_history', $params);
        } 
    }
    
    //parameter P01-status
    protected function view_loan_history_by_status()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && $this->parameters['P02'] >=-2 && $this->parameters['P02'] <=2 ) ) {
            return $this->failed('INVALID_PARAMETER_VALUE');
       
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'status' => $this->parameters['P02'],
            );
            return $this->process_method('view_loans', $params);
        } 
    }
    
    //parameter P01-customer_id
    protected function get_customer_info()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
            );
            return $this->process_method('get_customer_info', $params);
        }  
    }
    
    //parameter P01 -> customerid, P02 -> New Loan Limit, P03 -> Created by
    protected function set_loan_amount_limit()
    {
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'credit_limit' => $this->parameters['P03'],
                'created_by' => $this->parameters['P04'],
            );
            return $this->process_method('set_credit_limit', $params);
        }  
    }
    
    protected function register_customer()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params= $this->parameters;
            $paramCustomer = array(
                    'fname' => isset($params['P02']) && $params['P02'] != ''? $params['P02']: null,
                    'mname' => isset($params['P03']) && $params['P03'] != ''? $params['P03']: null,
                    'lname' => isset($params['P04']) && $params['P04'] != ''? $params['P04']: null,
                    'title' => isset($params['P22']) && $params['P22'] != ''? $params['P22']: null,
                    'gender' => isset($params['P23']) && $params['P23'] != ''? $params['P23']: null,
                    'linked_cardnumber' => isset($params['P05']) && $params['P05'] != ''? $params['P05']: null,
                    'mobile_number' => isset($params['P06']) && $params['P06'] != ''? $params['P06']: null,
                    'email_address' => isset($params['P07']) && $params['P07'] != ''? $params['P07']: null,
                    'language' => isset($params['P08']) && $params['P08'] != ''? $params['P08']: null,
                    'dob' => isset($params['P09']) && $params['P09'] != ''? $params['P09']: null,
                    'street' => isset($params['P10']) && $params['P10'] != ''? $params['P10']: null,
                    'city' => isset($params['P11']) && $params['P11'] != ''? $params['P11']: null,
                    'country' => isset($params['P12']) && $params['P12'] != ''? $params['P12']: null,
                    'pobox' => isset($params['P13']) && $params['P13'] != ''? $params['P13']: null,
                    'telephone1' => isset($params['P14']) && $params['P14'] != ''? $params['P14']: null,
                    'telephone2' => isset($params['P15']) && $params['P15'] != ''? $params['P15']: null,
                    'national_insurance_no' => isset($params['P16']) && $params['P16'] != ''? $params['P16']: null,
                    'employer_id' => isset($params['P17']) && $params['P17'] != ''? $params['P17']: null,
                    'contact_via_email' => isset($params['P18']) && $params['P18'] != ''? $params['P18']: 0,
                    'contact_via_sms' => isset($params['P19']) && $params['P19'] != ''? $params['P19']: 0,
                    'created_by' => isset($params['P20']) && $params['P20'] != ''? $params['P20']: null,                
                    'credit_limit' => isset($params['P21']) && $params['P21'] != ''? $params['P21']: null,
                );
            return $this->process_method('register_customer', $paramCustomer);
        }   
    }
    
    
    public function create_loan(){
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'amount' => $this->parameters['P03'],
                'created_by' => $this->parameters['P04'],
                'salary_deduction' => isset($this->parameters['P05'])? $this->parameters['P05']: 1,
            );
            return $this->process_method('new_loan', $params);
        }
    }

    protected function create_comment(){
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
            
        } else {
            $params = array(
                'loan_id' => $this->parameters['P02'],
                'comments' => $this->parameters['P03'],
                'user' => $this->parameters['P04'],
            );
            return $this->process_method('add_comment', $params);
        }
    }
    
    protected function set_due_date() {
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'due_date' => $this->parameters['P03']
            );
            return $this->process_method('save_due_date', $params);
        } 
    }
    
    protected function send_due_notification()
    {                       
        $message="";       
        $pastdue=$this->parameters['P01'];
        switch ($pastdue) {
            case 3:
                $message="Mango would like to remind you that your payment will be due on ";
                break;
            case 7:
                $message="Your loan payment is 7 days past due. Please contact Mango";    
                break;
            case 14:
                $message="Your loan account is seriously in arrears. Please contact Mango immediately";
                break; 
            case 30:
                $message="Your loan account is delinquent. Please contact Mango today to avoid an action being taken against you";
                break;
            default:
                return $this->failed('Invalid past due.');    
                die();
        }              
        $params = array('past_due'=>$pastdue
        ,'message' => $message);
        $this->process_method('send_notification_payment_due', $params);        
    }
    
    
    //validated <=0 it is declined, 1= validate
    protected function validate_loan()
    {
        if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (!(strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'loan_id' => $this->parameters['P03'],
                'validated_by' => $this->parameters['P04'],
                'validated' => $this->parameters['P05'],
//                'due_date' => $this->parameters['P06'],
            );
            return $this->process_method('validate_loan', $params);
        }       
    }
    
    //approved <=0 it is declined, 2= approved
    protected function approve_loan()
    {
        if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
            
        } else {
            $params = array(
                'customer_id' => $this->parameters['P02'],
                'loan_id' => $this->parameters['P03'],
                'approved_by' => $this->parameters['P04'],
                'approved' => $this->parameters['P05'],
            );
            return $this->process_method('validate_loan', $params);
        }       
    }
    
    
    
    protected function get_user_information()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                           
        } else {
            $params = array(
                'username' => $this->parameters['P02'],
            );
            return $this->process_method('get_user_details', $params);
        } 
    }
    
     
    //JR-2014/05/22
    protected function get_users()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
        $this->success($this->backend->get_users_list());
        $response = $this->backend->get_response(); 
        return $response;    
        }            
    } 
    
    
    protected function get_customers_by_createby(){
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                           
            
        } else {
            $params = array(
                'username' => $this->parameters['P02'],
            );
            return $this->process_method('get_users_customers', $params);
        }  
    }
   
   
  
    
    
    protected function delete_employer()
    {   
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                           
        
        } else {
            $params = array(
                'employer_id' => $this->parameters['P02'],
            );
            return $this->process_method('delete_employer', $params);
        }            
    }
    
    protected function get_employers_customers()
    {   
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(
                'employer_id' => $this->parameters['P02'],
            );
            return $this->process_method('get_employers_customers', $params);
        }            
    }
    //PP 05/16/2014
    protected function register_employer()
    {   
        if (!$this->_param_count_check(14)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 &&  strlen($this->parameters['P12']) > 0 && strlen($this->parameters['P13']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(
                        'name' => $this->parameters['P02'],
                        'address' => $this->parameters['P03'],
                        'address_line1' =>  $this->parameters['P04'],
                        'address_line2' =>  $this->parameters['P05'],
                        'city' =>  $this->parameters['P06'],
                        'country' =>  $this->parameters['P07'],
                        'zip' =>  $this->parameters['P08'],
                        'phone1' =>  $this->parameters['P09'],
                        'phone2' =>  $this->parameters['P10'],
                        'company_url' =>  $this->parameters['P11'],
                        'has_multiple_approver' =>  $this->parameters['P12'],
                        'status' => $this->parameters['P13'],
            );
            return $this->process_method('register_employer', $params);
        }            
    }

 protected function update_employer()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0  && strlen($this->parameters['P13']) > 0 && strlen($this->parameters['P14']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(
                        'id' => $this->parameters['P02'],
                        'name' => $this->parameters['P03'],
                        'address' => $this->parameters['P04'],
                        'address_line1' =>  $this->parameters['P05'],
                        'address_line2' =>  $this->parameters['P06'],
                        'city' =>  $this->parameters['P07'],
                        'country' =>  $this->parameters['P08'],
                        'zip' =>  $this->parameters['P09'],
                        'phone1' =>  $this->parameters['P10'],
                        'phone2' =>  $this->parameters['P11'],
                        'company_url' =>  $this->parameters['P12'],
                        'has_multiple_approver' =>  $this->parameters['P13'],
                        'status' => $this->parameters['P14'],
            );
            return $this->process_method('update_employer', $params);
        }            
    }
    
   
    
    
    //PP 05/16/2014
    protected function register_approver()
    {   
        if (!$this->_param_count_check(14)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                        
               'employer_id'      => $this->parameters['P02'],
               'last_name'     => $this->parameters['P03'],
               'first_name'       => $this->parameters['P04'],
               'department'       => $this->parameters['P05'],
               'employee_no'       => $this->parameters['P06'],
               'phone1'       => $this->parameters['P07'],
               'phone2'       => $this->parameters['P08'],
               'fax'       => $this->parameters['P09'],
               'email_address'       => $this->parameters['P10'],
               'status'       => $this->parameters['P11'],
               'job_title' => $this->parameters['P12'],
                        
            );
            return $this->process_method('register_approver', $params);
        }            
    }
    
       //PP 05/16/2014 
    protected function update_approver()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0 && strlen($this->parameters['P12']) > 0  )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(
                        'id' => $this->parameters['P02'],
                        'employer_id'      => $this->parameters['P03'],
                        'last_name'     => $this->parameters['P04'],
                        'first_name'       => $this->parameters['P05'],
                        'department'       => $this->parameters['P06'],
                        'employee_no'       => $this->parameters['P07'],
                        'phone1'       => $this->parameters['P08'],
                        'phone2'       => $this->parameters['P09'],
                        'fax'       => $this->parameters['P10'],
                        'email_address'       => $this->parameters['P11'],
                        'status'       => $this->parameters['P12'],
                        'job_title' => $this->parameters['P13'],
            );
            return $this->process_method('update_approver', $params);
        }            
    }
    
    //PP 05/16/2014 
    protected function get_approvers()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
           // return $this->success($this->backend->get_employers_detailed());
           $params = array(
                        'employer_id'      => $this->parameters['P02'],
            );
          
            $this->success($this->backend->get_approvers($params));
            $response = $this->backend->get_response(); 
            return $response;
            
            
           
        }            
    }  
    
     //PP 05/16/2014 
    protected function get_comments()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
           // return $this->success($this->backend->get_employers_detailed());
           $params = array(
                        'loan_id'      => $this->parameters['P02'],
            );
            $this->success($this->backend->get_comments($params));
              
        $response = $this->backend->get_response(); 
           return $response;
           
        }            
    }  
    
    
    
    
    
    
    
    //PP 05/16/2014
     protected function register_agent()
    {   
        if (!$this->_param_count_check(15)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0  && strlen($this->parameters['P13']) > 0 && strlen($this->parameters['P14']) > 0 && strlen($this->parameters['P15']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                        

                    'agent_name'         => $this->parameters['P02'],
                    'agent_address'         => $this->parameters['P03'],
                    'address_line1' => $this->parameters['P04'],
                    'address_line2' => $this->parameters['P05'],
                    'city'       => $this->parameters['P06'],
                    'country'       => $this->parameters['P07'],
                    'zip'       => $this->parameters['P08'],
                    'phone1'       => $this->parameters['P09'],
                    'phone2'       => $this->parameters['P10'],
                    'tax_number'      => $this->parameters['P11'],
                    'company_url'     => $this->parameters['P12'],
                    'has_multiple_agent'       => $this->parameters['P13'],
                    'agent_account_type_id'       => $this->parameters['P14'],
                    'agent_status'       => $this->parameters['P15'],
            );
            return $this->process_method('register_agent', $params);
        }            
    }
   
   
   
    
     //PP 05/16/2014 
    protected function update_agent()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0 && strlen($this->parameters['P12']) > 0  )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(
                    'id' => $this->parameters['P02'],
                    'agent_name'         => $this->parameters['P03'],
                    'agent_address'         => $this->parameters['P04'],
                    'address_line1' => $this->parameters['P05'],
                    'address_line2' => $this->parameters['P06'],
                    'city'       => $this->parameters['P07'],
                    'country'       => $this->parameters['P08'],
                    'zip'       => $this->parameters['P09'],
                    'phone1'       => $this->parameters['P10'],
                    'phone2'       => $this->parameters['P11'],
                    'tax_number'      => $this->parameters['P12'],
                    'company_url'     => $this->parameters['P13'],
                    'has_multiple_agent'       => $this->parameters['P14'],
                    'agent_account_type_id'       => $this->parameters['P15'],
                    'agent_status'       => $this->parameters['P16'],
            );
            return $this->process_method('update_agent', $params);
        }            
    }
    
    //PP 05/16/2014
    protected function register_sub_agent()
    {   
        if (!$this->_param_count_check(10)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0  )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                        
               
            'agent_id'        => $this->parameters['P02'],
            'first_name'         =>  $this->parameters['P03'],
            'last_name'         =>  $this->parameters['P04'],
            'employee_number' =>  $this->parameters['P05'],
            'phone1'       =>  $this->parameters['P06'],
            'phone2'         =>  $this->parameters['P07'],
            'email_address'       =>  $this->parameters['P08'],
            'reference' =>  $this->parameters['P09'],
            'job_title' =>  $this->parameters['P10'],
                        
            );
            return $this->process_method('register_sub_agent', $params);
        }            
    }
    
     //PP 05/16/2014 
    protected function update_sub_agent()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0  && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0 && strlen($this->parameters['P09']) > 0 && strlen($this->parameters['P10']) > 0 && strlen($this->parameters['P11']) > 0 && strlen($this->parameters['P12']) > 0 && strlen($this->parameters['P13']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(
                    'id' => $this->parameters['P02'],
                    'first_name' => $this->parameters['P03'],
                    'last_name' => $this->parameters['P04'],
                    'job_title' =>$this->parameters['P05'],
                    'employee_number' =>$this->parameters['P06'],
                    'phone1' =>$this->parameters['P07'],
                    'phone2' =>$this->parameters['P08'],
                    'email_address' =>$this->parameters['P09'],
                    'reference' =>$this->parameters['P10'],
            );
            return $this->process_method('update_sub_agent', $params);
        }            
    }
     
     
      //PP 05/16/2014 
    protected function update_loan_status()
    {   
        if (!$this->_param_count_check(6)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0  && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                    
                    'loan_id' =>$this->parameters['P02'],
                    'status' => $this->parameters['P03'],
                    'customer_id' =>$this->parameters['P04'],
                    'user' => $this->parameters['P05'],
                    'reason'      =>  $this->parameters['P06'],  
            );
            
            return $this->process_method('update_loan_status', $params);
        }            
    }
     
   //PP 05/16/2014 
    protected function get_sub_agents()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
           // return $this->success($this->backend->get_employers_detailed());
           $params = array(
                        'agent_id'      => $this->parameters['P02'],
            );
            $this->success($this->backend->get_sub_agents($params));
            $response = $this->backend->get_response(); 
           return $response;
        }            
    }     
    
    
    
    
    //PP 05/19/2014
    
    protected function add_agency_fee()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (!(strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0  )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                        
               
            'agent_id'        => $this->parameters['P02'],
            'pay_scale_amount' => $this->parameters['P03'],
                        
            );
            return $this->process_method('add_agent_fee', $params);
        }            
    }
    
     //PP 05/16/2014 
    protected function get_agency_fee()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
           // return $this->success($this->backend->get_employers_detailed());
           $params = array(
                        'agent_id'      => $this->parameters['P02'],
            );
            $this->success($this->backend->get_agent_fees($params));
            $response = $this->backend->get_response(); 
           return $response;
           
        }            
    }  
    
    
     //PP 06/24/2014
    protected function saveVerification()
    {   
        if (!$this->_param_count_check(9)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0   )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          

        } else {
            $params = array(                        
                    'customer_id'=>$this->parameters['P02'],
                    'loan_id' => $this->parameters['P03'],
                    'doc_type_id'  => $this->parameters['P04'],
                    'doc_type_name' => $this->parameters['P05'],
                    'fc_card_number' => $this->parameters['P06'],
                    'expiration_date' => $this->parameters['P07'],
                    'id_number' => $this->parameters['P08'],
                    'created_by'=> $this->parameters['P09'],
            
                        
            );
            return $this->process_method('save_verification', $params);
        }            
    }
    
    //PP 06/24/2014
        protected function customer_verification()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {   
        
         $params = array(                        
                    'loan_id' => $this->parameters['P02'],
                    );
            
            return $this->success($this->backend->customer_verification($params));
        }            
    } 
    
    
    
    //username, password
    protected function login() {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } else {
            $params = array(
                'username' => $this->parameters['P01'],
                'password' => $this->parameters['P02'],
            );
            return $this->process_method('login', $params);
        }
    }
    
    //session_id
    protected function logout() {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) == 32)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } else {
            $params = array(
                'session_id' => $this->parameters['P01'],
            );
            return $this->process_method('logout', $params);
        }
    }
    
    public function execute() {
        if (isset($_REQUEST['method'])) {
            $method = strtolower($_REQUEST['method']);
            if (method_exists($this->instance, $method)) {
                $this->method = $method;
                $this->$method();
                return TRUE;
            }
        }
        return $this->failed('INVALID_METHOD');
    }
    
    protected function create_payment_textfile(){
        /*if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = array(
                'startdate' => date('Y-m-d')
            );
            return $this->process_method('create_payment_textfile', $params);
        }*/
        
        /*if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->create_payment_textfile());
        }
        */
        $params = array(
                'startDate' => date('Y-m-d'),
                'biller_code' => 'BEC'
        );
        return $this->process_method('create_payment_textfile', $params);            
    }
    
    protected function download_customer_file(){
        $params = array(
                'startdate' => date('Y-m-d')
        );
        return $this->process_method('download_customer_file', $params);            
    }
      
    protected function read_customer_file(){
        $params = array(
                'startdate' => date('Y-m-d')
        );
        return $this->process_method('read_customer_file', $params);            
    }
    
    protected function get_all_profile()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_all_profile());
        }            
    }
    
    protected function get_all_profile_selected()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
             return $this->success($this->backend->get_all_profile($this->parameters['P02']));
        }            
    }
    
    protected function get_user_type_by_username()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = $this->parameters['P02'];
            return $this->process_method('get_user_type_by_username', $params);
        }            
    }
    
    protected function get_all_resources()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = "";
            return $this->process_method('get_all_resources', $params);
        }            
    }
    
    protected function add_module()
    {   
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'resource'      => $this->parameters['P02'],
                'description'   => $this->parameters['P03'],
        );
            return $this->process_method('add_module', $params);
        }            
    }
    
    protected function update_module()
    {   
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'value'      => $this->parameters['P02'],
                'description'   => $this->parameters['P03'],
                'id'            => $this->parameters['P04'],
        );
            return $this->process_method('update_module', $params);
        }            
    }
    
    protected function delete_module()
    {   
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'id'    => $this->parameters['P02'],
        );
            return $this->process_method('delete_module', $params);
        }            
    }
    
    protected function get_all_modules_list()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_all_modules_list());
        }            
    }
    
    protected function get_all_modules_list_selected()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
             return $this->success($this->backend->get_all_modules_list_selected($this->parameters['P02']));
        }            
    }
    
    protected function add_profile()
    {   
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0&& strlen($this->parameters['P04']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            
            $data = array(
                    '0' => 13,
                    '1' => 19,
                    '2' => 17,
                    '3' => 10,
                    '4' => 8
            );
            
            //$query = http_build_query(array('aParam' => $data));
            
            $params = array(                        
                'description'   => $this->parameters['P02'],
                //'module_access' => $data,//$this->parameters['P03'],
                'module_access' => $this->parameters['P03'],
                'created_by'    => $this->parameters['P04'],
        );
            return $this->process_method('add_profile', $params);
        }            
    }
    
    protected function edit_profile()
    {   
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0&& strlen($this->parameters['P04']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            
            $data = array(
                    '0' => 13,
                    '1' => 19,
                    '2' => 17,
                    '3' => 10,
                    '4' => 8
            );
            
            $params = array(                        
                'resource_id'   => $this->parameters['P02'],
                //'module_access' => $data,//$this->parameters['P03'],
                'module_access' => $this->parameters['P03'],
                'user_type_id'    => $this->parameters['P04'],
        );
            return $this->process_method('edit_profile', $params);
        }            
    }
    
    protected function delete_profile()
    {   
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'id'    => $this->parameters['P02'],
        );
            return $this->process_method('delete_profile', $params);
        }            
    }
    
    protected function get_transaction_fee()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_system_setting('transaction_fee'));
        }            
    }
    
    protected function add_payment()
    {   
        if (!$this->_param_count_check(5)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (!(strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'account_no'      => $this->parameters['P02'],
                'payment_amount'   => $this->parameters['P03'],
                'transaction_fee'   => $this->parameters['P04'],
                'total_amount'   => $this->parameters['P05'],
                'username'   => $this->parameters['P06'],
                'biller_code'   => $this->parameters['P07'],
        );
            return $this->process_method('add_payment', $params);
        }            
    }
    
    protected function search_customer(){
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (!(strlen($this->parameters['P01']) > 0 && (strlen($this->parameters['P02']) > 0  || strlen($this->parameters['P03']) > 0 || strlen($this->parameters['P04']) > 0))){
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'customer_name' => $this->parameters['P02'],
                'account_no' => $this->parameters['P03'],
                'phone_number' => $this->parameters['P04'],
            );
            //return $this->success($params);
            return $this->process_method('search_customer', $params);
        }   
    }
    
    protected function search_customer_ex(){
        if (!$this->_param_count_check(6)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (!(strlen($this->parameters['P01']) > 0 && (strlen($this->parameters['P02']) > 0  || strlen($this->parameters['P03']) > 0 || strlen($this->parameters['P04']) > 0 || strlen($this->parameters['P05']) > 0|| strlen($this->parameters['P06']) > 0 ))){
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'first_name' => $this->parameters['P02'],
                'last_name' => $this->parameters['P03'],
                'account_no' => $this->parameters['P04'],
                'phone_number' => $this->parameters['P05'],
                'biller_code' => $this->parameters['P06'],
            );
            //return $this->success($params);
            return $this->process_method('search_customer_ex', $params);
        }   
    }
    
    protected function add_customer()
    {   
        if (!$this->_param_count_check(8)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0  && strlen($this->parameters['P06']) > 0  && strlen($this->parameters['P07']) > 0  && strlen($this->parameters['P08']) > 0 ))        {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {
            $params = array(                        
                'account_no'      => $this->parameters['P02'],
                'customer_name'   => $this->parameters['P03'],
                'phone_number'    => $this->parameters['P04'],
                'house_number'    => $this->parameters['P05'],
                'street_name'     => $this->parameters['P06'],
                'apartment_id'    => $this->parameters['P07'],
                'zip_code'        => $this->parameters['P08'],
        );
            return $this->process_method('add_customer', $params);
        }            
    }
    
    protected function getPaymentsForExport(){
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'startDate'    => $this->parameters['P02'],
            );
            return $this->process_method('getPaymentsForExport', $params);
        }   
    }    
    
    protected function generate_payment_report(){
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0  && strlen($this->parameters['P03']) > 0)){
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'startDate'    => $this->parameters['P02'],
                'endDate'    => $this->parameters['P03'],
            );
            return $this->process_method('generate_payment_report', $params);
        }   
    }
    
    protected function getSystemSetting(){
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 )) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = null;
            return $this->process_method('getSystemSetting', $params);
            //return $this->success($this->backend->getSystemSetting());
        }   
    }
    
    protected function update_setting(){
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array (
                'id'        => $this->parameters['P02'],
                'set_value' => $this->parameters['P03'],
            );
            return $this->process_method('update_setting', $params);
            //return $this->success($this->backend->getSystemSetting());
        }   
    }
    
    protected function get_user_types()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_user_types());
        }            
    }
    
    protected function get_user_types_selected()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->success($this->backend->get_user_types_selected($this->parameters['P02']));
        }            
    }
    
    protected function get_users_detailed()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            //return $this->success($this->backend->get_users_detailed());
            $params = null;
            return $this->process_method('get_users_detailed', $params);
        }            
    }
    
    protected function get_users_detailed_selected()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = $this->parameters['P02'];
            return $this->process_method('get_users_detailed_selected', $params);
        }            
    }    
    
    protected function reset_user_password()
    {
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        } elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
           $params = array(
                       'user_id' => $this->parameters['P02'],
                       'username' =>  $this->parameters['P03'],
                       'email_address' => $this->parameters['P04'],
                       
            );
            $this->success($this->backend->reset_user_password($params));
          return $response;
             $response = $this->backend->get_response(); 
           
        }            
    }
    
    protected function change_password()
    {
        if (!$this->_param_count_check(4)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
            
        } else {
            $params = array(
                'username' => $this->parameters['P02'],
                'old_pw' => $this->parameters['P03'],
                'new_pw' => $this->parameters['P04'],
            );
            return $this->process_method('change_password', $params);
        }       
    }
    protected function delete_user()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = array(
                'user_id' => $this->parameters['P02'],
            );
            return $this->process_method('delete_user', $params);
        } 
    }
    
     protected function register_user()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                           
        } else {
            $params = array(
                    'fname'         => $this->parameters['P02'],
                    'lname'         => $this->parameters['P03'],
                    'email_address' => $this->parameters['P04'],
                    'mobile_number' => $this->parameters['P05'],
                    'address'       => $this->parameters['P06'],
                    'username'      => $this->parameters['P07'],
                    'user_type'     => $this->parameters['P08'],
                    'created_by'    => $this->parameters['P09'],
                    
                     /*
                    'fname'         => $params['fname'],
                    'lname'         => $params['lname'],
                    'email_address' => $params['email_address'],
                    'mobile_number' => $params['mobile_number'],
                    'address'       => $params['address'],
                    'username'      => $params['username'],
                    'user_type'     => $params['user_type'],
                    'created_by'    => $params['created_by'],
                    'password'      => md5($this->salt . $generated_password . $this->salt),
                    //JR-2014/03/26         
                    'customers_id'  => $params['customers_id'],
                    'agent_id' => $params['agent_id'],
                    */
                    
                );
            return $this->process_method('register_user', $params);
        }   
    }  
    
    
    protected function update_user()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0 && strlen($this->parameters['P06']) > 0 && strlen($this->parameters['P07']) > 0 && strlen($this->parameters['P08']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                           
        } else {
            $params = array(
                    'user_id'         => $this->parameters['P02'],
                    'fname'         => $this->parameters['P03'],
                    'lname'         => $this->parameters['P04'],
                    'email_address' => $this->parameters['P05'],
                    'mobile_number' => $this->parameters['P06'],
                    'address'       => $this->parameters['P07'],
                    'user_type'     => $this->parameters['P08'],
                );
            return $this->process_method('update_user', $params);
        }   
    }
    
     protected function get_all_customers()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = null;
            return $this->process_method('get_all_customers', $params);
        }            
    }  
    
    protected function get_user_info()
    {
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = array (
                'username'  => $this->parameters['P02'],
                'return_value'  => $this->parameters['P03'],
            );
            //return $this->process_method('get_user_info', $params);
            return $this->success($this->backend->get_user_info($params));
        }            
    }
    
    protected function payment_email_notification()
    {
        if (!$this->_param_count_check(3)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0 && strlen($this->parameters['P03']) > 0 && strlen($this->parameters['P04']) > 0 && strlen($this->parameters['P05']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = array (
                'email_address' =>  $this->parameters['P02'],
                'name'          =>  $this->parameters['P03'],
                'pathfile'      =>  $this->parameters['P04'],
                'filename'      =>  $this->parameters['P05']
            );
            //return $this->process_method('get_user_info', $params);
            return $this->success($this->backend->payment_email_notification($params));
        }            
    }
    
    protected function getPaymentStatus(){
        if (!$this->_param_count_check(2)){
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);
        } else {
            $params = $this->parameters['P02'];
            return $this->process_method('getPaymentStatus', $params);
            //return $this->success($this->backend->getSystemSetting());
        }   
    }     
	
	protected function check_if_account_exists()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = array (
                'account_no' =>  $this->parameters['P02'],
            );
            //return $this->process_method('get_user_info', $params);
            return $this->success($this->backend->check_if_account_exists($params));
        }            
    }
    
    protected function get_payment_status()
    {
        if (!$this->_param_count_check(2)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0 && strlen($this->parameters['P02']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            $params = array (
                'reference_no' =>  $this->parameters['P02'],
            );
            //return $this->process_method('get_user_info', $params);
            return $this->success($this->backend->get_payment_status($params));
        }            
    }
    
    protected function get_biller_list()
    {
        if (!$this->_param_count_check(1)) {
            return $this->failed('INVALID_PARAMETER_COUNT');
        } elseif (! (strlen($this->parameters['P01']) > 0)) {
            return $this->failed('INVALID_PARAMETER_VALUE');
        }elseif (!$this->backend->validate_session(array('session_id' => $this->parameters['P01']))) {
            $response = $this->backend->get_response();
            return $this->failed($response['ResponseMessage']);                          
        } else {                                         
            return $this->process_method('get_biller_list', false);
        }            
    }
    
     protected function download_customer_file_wsc(){
        //$params = array(
        //        'startdate' => date('Y-m-d')
        //);
        return $this->process_method('download_customer_file_wsc', '');            
    }
      
    protected function read_customer_file_wsc(){
        $params = array(
                'startdate' => date('Y-m-d')
        );
        return $this->process_method('read_customer_file_wsc', $params);            
    }
    
     protected function create_transaction_report_file(){
        $params = array(
                'startDate' => date('Y-m-d'),
                'biller_code' => 'BEC'
        );
        return $this->process_method('create_transaction_report_file', $params);            
    }
    
    protected function create_payment_textfile_wsc(){
        $params = array(
                'startDate' => date('Y-m-d'),
                'biller_code' => 'WSC'
        );
        return $this->process_method('create_payment_textfile_wsc', $params);            
    }
    
    protected function create_payment_textfile_btc(){
        $params = array(
                'startDate' => date('Y-m-d'),
                'biller_code' => 'BTC'
        );
        return $this->process_method('create_payment_textfile_btc', $params);            
    }
    

    
}

$instance = new api();
$instance->execute();
