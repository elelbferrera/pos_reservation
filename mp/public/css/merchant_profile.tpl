{include file="header.tpl"}

<input type="hidden" name="txtLongitude" id="txtLongitude" value="{$m.longitude}">
<input type="hidden" name="txtLatitude" id="txtLatitude" value="{$m.latitude}">

<input type="hidden" name="txtCity" id="txtCity" value="{$m.city}">
<input type="hidden" name="txtZip" id="txtZip" value="{$m.zip}">
<input type="hidden" name="txtState" id="txtState" value="{$m.state}">

<input type="hidden" name="txtAddress" id="txtAddress" value="{$m.address1}">
<input type="hidden" name="txtBusinessName" id="txtBusinessName" value="{$m.business_name}">

    <section id="main-page" class="cd-container">
          <div class="container">
            <div class="row">
                  <div class="col-lg-2 col-sm-4 col-xs-12">
                      <div class="logo-merchant">
                      	 {if $m.logo eq ''}
                          	<img src="{$webroot_resources}/images/profile/merchant-logo-default.png" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                         {else}
                         	<img src="{$m.logo}" class="img-responsive" />
                         {/if}
                      </div>                   
                  </div>
                  <div class="col-lg-10 col-sm-8">
                      <div class="merchant-title">{$m.business_name}
                         <div class="col-sm-2 pull-right">
                            {php} if(isset($_SESSION['session_id'])){ {/php}
                            	{if $favorite_id > 0}
                            		<a href="#" id="btnFavorite" name="btnFavorite" class="tabbtn btn btn-primary btn-mark"><i class="fa fa-heart"></i></a>
                            	{else}
                            		<a href="#" id="btnFavorite" name="btnFavorite" class="tabbtn btn btn-info btn-mark"><i class="fa fa-heart"></i></a>
                            	{/if}
                            {php} } {/php}
                            <!-- <a href="" class="tabbtn btn btn-primary btn-mark"><i class="fa fa-bookmark"></i></a><br /> -->
                            <form id="frmFavorite" name="frmFavorite">
                            	<input type="hidden" name="txtMerchantIdFavorite" id="txtMerchantIdFavorite" value="{$m.id}"/>
                            	<input type="hidden" name="txtFavoriteId" id="txtFavoriteId" value="{$favorite_id}"/>
                            	
                            </form>
                            <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark">Check In</a> -->
                         </div>
                      </div>
                      <div class="merchant-about">{$m.address1} {$m.city} {$m.state} {$m.zip} </div>
                      <div class="merchant-about">{$m.phone1}</div>
                      <!-- <div class="merchant-about">Fax: 401-886-9201</div> -->
                      <div class="merchant-rating"> </div>
                      	<!-- <img src="{$webroot_resources}/images/profile/star-rate5.png" class="img-responsive" />  | -->
                      <!-- <a href="#" id="seeallreview" data-toggle="modal" data-target="#allreview"> &nbsp;&nbsp; View all review &nbsp;&nbsp; </a>  / -->
                      {php} if(isset($_SESSION['session_id'])){ {/php}
                      	<a href="#" id="writereview" data-toggle="modal" data-target="#writereviewpage">Write a review</a>
                      {php} } {/php}
                      <div class="merchant-miles"></div>
                      <div class="clearboth"></div>
                      <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark">Check Me In</a><br> -->
                      <a href="{$webroot}/merchant/reservation/{$m.id}" class="tabbtn btn btn-primary btn-reservation col-lg-4 col-sm-6 col-xs-12" id="set-reservation"><i class="fa fa-calendar"></i> Click Here to Create Reservation</a> <br />
                      <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark"> Click Here for Same Day Check In</a> -->
                      <div style="clear:both;"></div>

                      <a style="color:#FFF; background-color:#34536d;border-color: #274258;" href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-info btn-mark btn-reservation col-lg-4 col-sm-6 col-xs-12" id="set-reservation"><i class="fa fa-check-circle"></i> Click Here for Same Day Check In</a>

                  </div>
            </div>
        </div>
    </section><!--/#main-slider-->
    
     

    <section id="merchant-profile">
        <div class="container">
            <div class="row detail-content">
                <div class="col-lg-7 col-sm-7 col-xs-12">
    	                  <div class="about-title wow fadeInLeft">Find Us here</div>
    	                            	
						 <div style="padding-bottom: 250px"  id="map" name="map">
	      					               
                         </div>
                         
          
          
                          <!--
                         <div class="background-white p20 wow fadeInLeft">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.5608798519556!2d-74.01407328449916!3d40.70566777933266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a16bbacf60d%3A0x44070e4c624cc9ec!2s50+Broad+St%2C+New+York%2C+NY+10004%2C+USA!5e0!3m2!1sen!2sph!4v1479356200584" width="600" height="300" frameborder="0" style="border:0; width:100%; " allowfullscreen></iframe>
                         </div> -->
                        

                         
                        <!-- <div class="about-title wow fadeInLeft">Our Gallery</div> -->
                         <!-- <div class="background-white p20 wow fadeInLeft">

                        <div id="portfolio"> -->
                          <!-- <div class="portfolio-items"> -->
                          	
                <!-- <div class="portfolio-item creative">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/01.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 1</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/02.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 2</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/03.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 3</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/04.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 4</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/05.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 5</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/06.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 5</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/07.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 7</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/08.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 8</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->
            <!-- </div> -->

            <!-- </div>



                         </div> -->
                         
                        <div class="about-title wow fadeInLeft">Services We offer</div>
                        <div class="background-white p20 wow fadeInLeft">
                              <!-- <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex . </p> -->
                             <div class="row">
                                <ul class="services">
                                	{foreach from=$m.service_category item=item}
                                    	<li class="col-lg-6 col-sm-6 col-xs-12"><i class="fa fa-angle-right"></i> <b>{$item.name}</b>
                                    	<p>{$item.description}</p></li>
                                    {/foreach}
                                </ul>
                             </div>
                        </div>
                        <div class="about-title wow fadeInLeft">Reviews</div>
                        <div class="background-white p20 wow fadeInLeft">
                        	
                            <div class="row">
                                <!-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                    <div class="review">
                                        <img src="{$webroot_resources}/images/review/01.jpg" class="img-responsive profile" />
                                    </div>
                                </div> -->
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="review">
                                    	{foreach from=$m.reviews item=item}
                                        <span class="review-title"><h2>{$item.first_name} {$item.last_name}</h2></span><br />
	                                        <div class="review-overall-rating">
	                                            <!-- <span class="overall-rating-title">Total Score:</span>
	                                            <i class="fa fa-star"></i>
	                                            <i class="fa fa-star"></i>
	                                            <i class="fa fa-star-half-empty"></i>
	                                            <i class="fa fa-bookmark"></i> -->
	                                        </div>
	                                         <p><i class="fa fa-star"></i>{$item.comment}</p>
                                         {/foreach}
                                    </div>
                                </div>
                            </div>
                            <!-- 
                             <div class="row">
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    <div class="review">
                                        <img src="images/review/02.jpg" class="img-responsive profile" />
                                    </div>
                                </div>
                                <div class="col-lg-9 col-sm-9 col-xs-9">
                                    <div class="review">
                                        <span class="review-title"><h2>ALISON MARTAJ</h2></span><br />
                                        <div class="review-overall-rating">
                                            <span class="overall-rating-title">Total Score:</span>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-empty"></i>
                                        </div>
                                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>
                            </div>
                         -->
                        </div>


                </div>
                <div class="col-lg-5 col-sm-5 col-xs-12">
                    <div class="about-title wow fadeInRight">About <span>{$m.business_name}</span></div>
                    <div class="background-white p20 wow fadeInRight">
                            <div class="detail-vcard">
                                <div class="detail-logo">
                                   {if $m.logo eq ''}
                                      <img src="{$webroot_resources}/images/profile/merchant-logo-default.png" class="img-responsive" />
                                   {else}
                                      <img src="{$m.logo}" class="img-responsive" style="height: 200px; width: 100%;" />
                                   {/if}
                                </div>
                                <div class="detail-contact">
                                    <div class="detail-address"><i class="fa fa-location-arrow"></i>{$m.address1} {$m.city} {$m.state} {$m.zip} </div>
                                    <div class="detail-contact-phone"><i class="fa fa-phone"></i><a href="#">{$m.phone1}</a></div>
                                    <div class="detail-contact-email"><i class="fa fa-envelope-o"></i><a href="#">{$m.email}</a></div>
                                    <!-- <div class="detail-contact-website"><i class="fa fa-globe"></i><a href="#">www.sampelcompany.com</a></div> -->
                                </div>
                            </div>
                          

                            <div class="detail-description">
                                <p> {$m.description} </p>
                            </div>
                           
                             <!-- <div class="detail-follow">
                                <div style="margin-bottom:10px;">
                                    <h5>Check us on</h5>
                                    <div class="follow-wrapper">
                                        <a href="#" class="follow-btn facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="follow-btn youtube"><i class="fa fa-youtube"></i></a>
                                        <a href="#" class="follow-btn twitter"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="follow-btn google-plus"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                               <div style="margin-bottom:10px;">
                                    <h5>We Do Accept</h5>
                                    <div class="follow-wrapper">
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-credit-card"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-paypal"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-discover"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-visa"></i></span>
                                    </div>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <h5>People love us</h5>
                                    <div class="follow-wrapper">
                                        <span class="detail-overview-hearts"><i class="fa fa-heart"></i> 213 people</span>
                                    </div>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <h5>Review on us</h5>
                                    <div class="follow-wrapper">
                                        <span class="detail-overview-rating"><i class="fa fa-star"></i> 4.5k reviews</span>
                                    </div>
                                </div>
                            </div> -->
                     </div>

                     <!-- <div class="about-title wow fadeInRight">Promo on hot</div>
                     <div class="background-white p20 wow fadeInRight"> -->

                             <!-- <div id="carousel-testimonial" class="carousel slide text-center" data-ride="carousel"> -->
                                <!-- Wrapper for slides -->
                                <!-- <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                          <div class="coupon_content">
                                              <div class="title-coupon">Get 20% off on Nail Spa</div>
                                              <p>Lorem ipsum dolor sit amet, eum an aliquando theophrastus definitionem, et eam option offendit. Vix facilis ceteros ne</p>
                                          </div>
                                    </div>
                                    <div class="item">
                                          <div class="coupon_content">
                                              <div class="title-coupon">Get 20% off on Nail Spa</div>
                                              <p>Lorem ipsum dolor sit amet, eum an aliquando theophrastus definitionem, et eam option offendit. Vix facilis ceteros ne</p>
                                          </div>
                                    </div>
                                </div> -->

                                <!-- Controls -->
                                <!-- <div class="btns">
                                    <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="prev">
                                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="next">
                                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div> -->
                            <!-- </div>
                     </div> -->

                     <div class="about-title wow fadeInRight">Business Hours</div>
                     <div class="background-white p20 wow fadeInRight">
                        <div class="working-hours">
                        	{foreach from=$m.business_hours item=item}
	                        	<div class="day clearfix">
	                                <span class="name">{$item.day}</span>
	                                
	                                {if $item.is_close}
	                                	<span class="hours">CLOSE</span>
	                                {else}
	                                	<span class="hours">{$item.start_time} - {$item.end_time}</span>
	                                {/if}
	                                
	                            </div>
                        	{/foreach}
                        </div>
                     </div>

                </div>
            </div>
        </div>
    </section>


<a href="#0" class="cd-top">Top</a>


<div id="writereviewpage" name="writereviewpage" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <form id="frmReview" name="frmReview">
            	<input type="hidden" name="txtMerchantId" name="txtMerchantId" value="{$m.id}">
            <div class="modal-content">
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title modal-title-custom"><label>Write Review</label></h5>
                    <div class="row header-review">
                        <div class="col-md-2 col-sm-2 col-xs-3">
                         {if $m.logo eq ''}
                          	<img src="{$webroot_resources}/images/profile/merchant-logo-default.png" class="img-responsive writereview-profile" />
                         {else}
                         	<img src="{$m.logo}" class="img-responsive writereview-profile" />
                         {/if}
                             
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-9">
                             <div class="modal-review-storetitle">{$m.business_name}</div>
                             <p><i class="fa fa-map-marker"></i> {$m.state} {$m.city} {$m.zip} {$m.address1}</p>
                        </div>

                    </div>
                  </div>
                  <div class="modal-body">
                        <div class="form-group">
                            <!-- <label>Rate it</label><br /> -->
                            <!-- <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                            </div> -->
                                <span class="rating">
                                        <!-- <input type="radio" class="rating-input"
                                    id="rating-input-1-5" name="rating-input-1"/>
                                        <label for="rating-input-1-5" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-4" name="rating-input-1"/>
                                        <label for="rating-input-1-4" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-3" name="rating-input-1"/>
                                        <label for="rating-input-1-3" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-2" name="rating-input-1"/>
                                        <label for="rating-input-1-2" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-1" name="rating-input-1"/>
                                        <label for="rating-input-1-1" class="rating-star"></label> -->
                                </span>
                        </div>
                         <div class="form-group">
                            <textarea name="txtReviewMessage" id="txtReviewMessage" class="form-control" rows="4" placeholder="Write a review"></textarea>
                        </div>

                        <p><i class="fa fa-globe"></i> Your review will be posted publicly on the web, under <b>{$m.business_name}</b></p>
                  </div>
                  <div class="modal-footer"> 
                         <div class="col-md-4 col-sm-4 sm-col pull-left" style="display: inline-flex; padding-left:0px;">
                                <button class="tabbtn btn btn-success" id="btnSubmitReview" name="btnSubmitReview"><i class="fa fa-check-circle"></i> Publish</button>
                                <a href="#" class="tabbtn btn btn-dark" id="#cancelreserve" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel</a>
                         </div>
                 </div>
            </div>
            </form>
       </div>
</div>



<div id="servicesmodal" name="servicesmodal" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <!-- <form id="frmCheckIn" name="frmCheckIn"> -->
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>  
                    Check Me In
                    <div class="modal-review-storetitle">{$m.business_name}</div>
                  </div>
                  <div class="modal-body">
                     <div class="form-group ">
                       <h4>Select services to avail</h4>
                       <div class="row">
                       <div class="col-lg-12">
                         <ul class="nav nav-tabs" role="tablist">
                {foreach from=$m.service_category key=key item=item}
                  {if $key==0}
                  <li role="presentation" class="active"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {else}
                  <li role="presentation"><a href="#cat{$key}" aria-controls="cat{$key}" role="tab" data-toggle="tab"><strong>{$item.name}</strong></a></li>
                  {/if}
                {/foreach}
              </ul>
              <div class="tab-content">
             {foreach from=$m.service_category key=key item=item2}
              {if $key==0}
              <div role="tabpanel" class="tab-pane active" id="cat{$key}">
              {else}
              <div role="tabpanel" class="tab-pane" id="cat{$key}">
              {/if}
              <!-- <ul class="services-choice"> -->
              {foreach from=$m.services item=item}
                {if $item.category_id == $item2.id}
               <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left:0px;">
                                   <div class="agreement">
                                    <label style="display:inline-flex;">
                                      <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="{$item.id}" />
                                      <div style="padding: 5px;">{$item.name} 
                                      <!-- <span>${$item.price}</span> -->
                                      </div></label>
                                   </div>
                             </div>

                {/if}
              {/foreach}
             <!--  </ul> -->
              </div>
             {/foreach}
             </div>
             </div>
                     </div>
                    </div>
                  </div>
                  <div class="modal-footer"> 
                     <div class="col-md-5 col-sm-4 sm-col pull-right" style="padding-left:0px;padding-right: 0px;">
                        <a data-toggle="modal" href="#checkin" class="btn btn-primary" data-dismiss="modal">Previous</a>
                      <button type="button" class="tabbtn btn btn-success" id="btnCheckMeIn" name="btnCheckMeIn"><i class="fa fa-check-circle"></i> Check Me In</button>
                     </div>
                 </div>
        <!-- </form> -->
            </div>
       </div>
</div>




<div id="checkin" name="checkin" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            	<form id="frmCheckIn" name="frmCheckIn">
            		<input type="hidden" name="txtMerchantId" id="txtMerchantId" value="{$m.id}">
            		<input type="hidden" name="txtDate" id="txtDate" value="{$smarty.now|date_format:"%Y-%m-%d"}">
            		<input type="hidden" name="txtTime" id="txtTime" value="{$smarty.now|date_format:"%k:%M"}">
                <input type="hidden" name="txtServices" id="txtServices" value="">
                <input type="hidden" name="txtSpecialist" id="txtSpecialist" value="">
            		
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>	
                    Check Me In
                    <div class="modal-review-storetitle">{$m.business_name}</div>
                  </div>
                  <div class="modal-body">
                  	<h4>We need you Personal Information</h4>
                         <div class="form-group">
                            <label>* First name</label>
                            <input type="text" value="{php} isset($_SESSION['customer_information']['first_name']) ? print_r($_SESSION['customer_information']['first_name']) : ""; {/php}" name="txtFirstName" id="txtFirstName" class="form-control" placeholder="First Name">
                         </div>
                         
                         <div class="form-group">
                            <label>* Last name</label>
                            <input type="text" value="{php} isset($_SESSION['customer_information']['last_name']) ? print_r($_SESSION['customer_information']['last_name']) : ""; {/php}" name="txtLastName" id="txtLastName" class="form-control" placeholder="Last Name">
                         </div>
                         <div class="form-group">
                            <label>Contact Number</label>
                            <input type="text" value="{php} isset($_SESSION['customer_information']['mobile_number']) ? print_r($_SESSION['customer_information']['mobile_number']) : ""; {/php}" name="txtContactNumber" id="txtContactNumber" class="form-control" placeholder="Contact Number">
                         </div>
                         <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" value="{php} isset($_SESSION['customer_information']['email_address']) ? print_r($_SESSION['customer_information']['email_address']) : ""; {/php}" name="txtEmailAddress" id="txtEmailAddress" class="form-control" placeholder="Email Address">
                         </div>

                  </div>
                  <div class="modal-footer"> 
                         <div class="col-md-4 col-sm-4 sm-col pull-right" style="padding-left:0px;padding-right: 0px;">
	                          <a data-toggle="modal" href="#servicesmodal" class="btn btn-primary" data-dismiss="modal">Next</a>
	                    	  <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>

                         <!--    <button class="tabbtn btn btn-success" id="btnCheckMeIn" name="btnCheckMeIn"><i class="fa fa-check-circle"></i> Check Me In</a> -->
                         </div>
                 </div>
				</form>
            </div>
       </div>
</div>

{include file="footer.tpl"}
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeTQ090cdN-02VxWGWsl-1gN_A6jk9MBY&callback=initMap">
</script>
<script src="{$webroot_resources}/js/merchant_profile.js"></script>

