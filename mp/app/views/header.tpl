<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Go3Reservation</title>
	<!-- core CSS -->
    <link href="{$webroot_resources}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/font-awesome.min.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/animate.min.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/main.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/owl.carousel.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/owl.transitions.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/prettyPhoto.css" rel="stylesheet">
     <link rel="stylesheet" href="{$webroot_resources}/dist/css/AdminLTE.min.css">



    <link href="{$webroot_resources}/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="{$webroot_resources}/css/jquery.datepicker.css">
    <link href="{$webroot_resources}/css/jquery.paginate.css" rel="stylesheet" type="text/css">
    <link href="{$webroot_resources}/css/ion.rangeSlider.css" rel="stylesheet" >
    <link href="{$webroot_resources}/css/ion.rangeSlider.skinNice.css" rel="stylesheet">
    
     <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.datepicker.css"> -->
    
    <!-- bootstrap datepicker -->
    
    <!-- Bootstrap time Picker -->
    
    
    

    
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{$webroot_resources}/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{$webroot_resources}/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{$webroot_resources}/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{$webroot_resources}/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="{$webroot_resources}/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage">
{php} if(!$_GET){ {/php}
    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{$webroot}/marketplace/in"><img src="{$webroot_resources}/images/logo.png" alt="logo" class="img-responsive"></a>
                </div>
				
				{php} 
                if(!isset($_SESSION['session_id']))
                { {/php}
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                    	<li><a href="{$webroot}/marketplace/in">Home</a></li>
                        <li><a href="{$webroot}/customer/login">Sign In</a></li>
                        <li><a href="{$webroot}/customer/sign_up">Register</a></li>         
                    </ul>
                </div>
                {php} }else{{/php}
		            <div class="collapse navbar-collapse navbar-right">
	                    <ul class="nav navbar-nav">
	                       <li class="dropdown">
	                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
	                            	<span class="thumb-sm avatar pull-left"> 
	                            		<img src="{$webroot_resources}/images/avatar.jpg"> </span>{php} echo $_SESSION['customer_information']['first_name']." ".$_SESSION['customer_information']['last_name'];{/php}<b class="caret"></b> </a>
	                            	<ul class="dropdown-menu animated fadeInDown"> <span class="arrow top"></span>
	                                <li> <a href="{$webroot}/marketplace/in">Home</a> </li>
	                                <li> <a href="{$webroot}/customer/my_profile">Profile</a> </li>
	                                <li> <a href="{$webroot}/customer/logout">Logout</a> </li>
	                            </ul>
	                        </li>
	                    </ul>
	                </div>
	                
                {php}
                	}
                {/php}
                
                
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
{php} } {/php}