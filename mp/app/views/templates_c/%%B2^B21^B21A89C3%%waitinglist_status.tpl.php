<?php /* Smarty version 2.6.26, created on 2017-03-19 22:38:05
         compiled from waitinglist_status.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'waitinglist_status.tpl', 4, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<section id="main-page" style="padding-top: 10px; padding-bottom: 10px; min-height:150px;"></section><!--/#main-slider-->
<?php if (count($this->_tpl_vars['waiting_lists']) > 0): ?>
<section id="merchant-profile">
        <div class="container">
            <div class="row profile-top-space">
                <div class="col-lg-12 col-sm-12 col-xs-8 nospace">
                     <div class="profile-name">
                       <div class="col-sm-3 col-xs-3 pull-right">
                           <h2 class="numberinrow"><?php echo count($this->_tpl_vars['waiting_lists']); ?>
</h2>
                           <p class="rowdesc">Total List In Waiting</p>
                       </div>
                     </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-3 col-xs-12" style="margin-bottom:20px;">
                     <!-- table -->
                              <table id="tbUser" class="merchant-table">
                                    <tr class="header bg-gray b-b b-light">
                                      <th style="width: 20%;" class="waitlist-table-td-th">Customer Name</th>
                                      <th style="width: 30%;" class="waitlist-table-td-th">Check In Time</th>
                                      <th class="waitlist-table-td-th">Waiting Time</th>
                                      <th class="waitlist-table-td-th">Services</th>
                                    </tr>

                                   <tbody>

               <?php $_from = $this->_tpl_vars['waiting_lists']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <?php  
                  $check_mobile = $_GET['mobile'];
                  $this->assign('mobile', $check_mobile);
                 ?>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
wait_id" name="<?php echo $this->_tpl_vars['item']['id']; ?>
wait_id"><?php echo $this->_tpl_vars['item']['id']; ?>
</div>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
first_name"><?php echo $this->_tpl_vars['item']['first_name']; ?>
</div>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name" name="<?php echo $this->_tpl_vars['item']['id']; ?>
last_name"><?php echo $this->_tpl_vars['item']['last_name']; ?>
</div>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
status" name="<?php echo $this->_tpl_vars['item']['id']; ?>
status"><?php echo $this->_tpl_vars['item']['status']; ?>
</div>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
services" name="<?php echo $this->_tpl_vars['item']['id']; ?>
services"><?php echo $this->_tpl_vars['item']['services']; ?>
</div>
            <div style="display: none" id="<?php echo $this->_tpl_vars['item']['id']; ?>
product_ids" name="<?php echo $this->_tpl_vars['item']['id']; ?>
product_ids"><?php echo $this->_tpl_vars['item']['product_ids']; ?>
</div>
                      <tr <?php if ($this->_tpl_vars['mobile'] == $this->_tpl_vars['item']['contact_number']): ?>  style="background-color: yellow;" <?php endif; ?>>
                        <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['first_name']; ?>
 <?php echo $this->_tpl_vars['item']['last_name']; ?>
</td>
                        <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['normal_time']; ?>
</td>
                        
                        <?php if ($this->_tpl_vars['item']['status'] != 'WAITING'): ?>
                          <td class="waitlist-table-td-th">00:00</td>
                        <?php else: ?>
                          <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['waiting_time']; ?>
</td>
                        <?php endif; ?>
                        <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['services']; ?>
</td>
                        <!-- <td class="waitlist-table-td-th"><?php echo $this->_tpl_vars['item']['status']; ?>
</td> -->
                      </tr>
                   <?php endforeach; endif; unset($_from); ?>
                </tbody>
                                
                              </table>
                              <!-- /. table -->
                </div>
            </div>
                

        </div>
    </section>
<?php else: ?>

    <section id="waiting-list">
      <div class="container">
        <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-8 nospace">
          <h1>No Customer Waiting.</h1>
        </div>
        </div>
      </div>
    </section>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>