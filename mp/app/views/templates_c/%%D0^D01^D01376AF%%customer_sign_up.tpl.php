<?php /* Smarty version 2.6.26, created on 2017-05-17 04:11:24
         compiled from customer_sign_up.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<section id="main-page" style="padding-top: 10px; padding-bottom: 10px;">
          <div id="divSignup" name="divSignup" class="container">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Your Information</h2>
                
                <!-- <p class="text-center wow fadeInDown" style="line-height:17px;">View past reservation, easy booking, rate merchant, write reviews, get discount code promo and more</a></p> -->
					<form name="frmRegisterMe" id="frmRegisterMe">
                          <div class="login-wrap2">
                            <div class="login-html" style="padding:0px;">
                                <input id="tab-1" type="radio" name="tab" class="sign-in"><label for="tab-1" class="tab" style="display:none;">Sign In</label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up" checked><label for="tab-2" class="tab" style="display:none;">Sign Up</label>
                                  <div class="login-form">
                                     

                                     <div class="sign-up-htm">
                                        

                                         <div class="row">
                                              <div class="col-sm-6">
                                                     <div class="group">
                                                        <label for="user" class="label2">First Name *</label>
                                                        <input id="txtFirstName" name="txtFirstName" type="text" class="input" required="required">
                                                     </div>
                                                     <div class="group">
                                                        <label for="user" class="label2">Last Name *</label>
                                                        <input id="txtLastName" name="txtLastName" type="text" class="input">
                                                     </div>
                                              </div>
                                              <div class="col-sm-6">    
                                                     <div class="group">
                                                        <label for="user" class="label2">Middle Name</label>
                                                        <input id="txtMiddleName" name="txtMiddleName" type="text" class="input">
                                                     </div>
                                                     <div class="group">
                                                        <label for="user" class="label2">Contact Number *</label>
                                                        <input id="txtContactNumber" name="txtContactNumber"  type="text" class="input">
                                                      </div>
                                              </div>
                                              
                                               <div class="col-sm-12">
                                                    <div class="group">
                                                        <label for="user" class="label2">State</label>
                                                        <input id="txtState" name="txtState" type="text" class="input">
                                                    </div>
                                                    
                                              </div>
                                               <div class="col-sm-12">
                                                     <div class="group">
                                                        <label for="user" class="label2">Address</label>
                                                        <input id="txtAddress" name="txtAddress" type="text" class="input">
                                                    </div>
                                              </div>
                                              
                                               <div class="col-sm-6">
                                                      <div class="group">
                                                         <label for="user" class="label2">ZIP *</label>
                                                         <input id="txtZip" name="txtZip" type="text" class="input">
                                                      </div>
                                                      <div class="form-group group">
                                                          <label for="user">Gender</label>
                                                          <select class="form-control" name="txtGender" id="txtGender" style="background: rgba(255, 255, 255, 0.34); color:#FFF;     border: none;border-radius: 5px;">
                                                              <option value="Male" style="color:#000;">Male</option>
                                                              <option value="Female" style="color:#000;">Female</option>
                                                          </select>
                                                      </div>
                                              </div>
                                              <div class="col-sm-6">
                                                     <div class="group">
                                                        <label for="user" class="label2">City</label>
                                                        <input id="txtCity" name="txtCity" type="text" class="input">
                                                    </div>
                                                    <div class="form-group group">
                                                        <label>Birthdate</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                               <input type="text" class="form-control input" name="txtBirthDate" id="txtBirthDate" data-select="datepicker" style="border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 5px;border-bottom-right-radius: 5px; padding: 0px;
padding-left: 15px;">
                                                        </div> 
                                                     </div>
                                                     
                                              </div>
                                             
                                             <div class="col-sm-12">
                                                    <div class="group">
                                                         <div class="hr"></div>
                                                    </div>
                                                    
                                              </div>
                                            
                                              
                                              <div class="col-sm-6">
                                                    <div class="group">
                                                        <label for="pass" class="label2">Email Address </label>
                                                        <input id="txtEmailAddress" name="txtEmailAddress" type="text" class="input">
                                                    </div>
                                                    <div class="group">
                                                        <label for="pass" class="label2">Username *</label>
                                                        <input id="txtUsername" name="txtUsername" type="text" class="input">
                                                    </div>
                                                     
                                              </div>
                                              <div class="col-sm-6">
                                                    <div class="group">
                                                        <label for="pass" class="label2">Password *</label>
                                                        <input id="txtPassword" name="txtPassword" type="password" class="input" data-type="password">
                                                    </div>
                                                     
                                                    <div class="group">
                                                        <label for="pass" class="label2">Repeat Password</label>
                                                        <input id="txtRepeatPassword" name="txtRepeatPassword" type="password" class="input" data-type="password">
                                                    </div>
                                              </div>
                                              <div class="col-sm-12">
                                                   <div class="hr"></div>
                                                         <div class="group">
                                                            <input id="chkRegister" name="chkRegister" type="checkbox" class="check">
                                                            <label for="chkRegister" style="line-height: 17px;">
                                                            	
                                                            	<span class="icon"></span>  
                                                            	By Registering I agree to the <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/termsandconditions">Terms and Conditions</a> and <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/privacypolicy">Privacy Policy</a>
                                                            	</label>
                                                        </div>
                                                        <div class="group">
                                                         	<p class="text-left wow fadeInDown"></p>
                                                        </div> 
                                                        <div class="group">
                                                            <input type="submit" id="btnRegister" name="btnRegister" class="button" value="Register">
                                                        </div>
                                                       
                                                        <div class="foot-lnk">
                                                            <label for="tab-2"><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/login">Already Member?</a></label>
                                                        </div>
                                              </div>
                                         </div>
                                        <!-- <p>Sign Up using your social media account</p>
                                        <div class="set-2">
                                            <ul>
                                                <li><a href="#" class="twitter-big">Twitter</a></li>
                                                <li><a href="#" class="facebook-big">Facebook</a></li>
                                                <li><a href="#" class="gplus-big">GPlus</a></li>
                                            </ul>
                                        </div> -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

					</form>
            </div>
        </div>
        
        
        <div id="divSuccess" name="divSuccess" class="container" style="display: none;">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Successful Registration</h2>
                <p class="text-center wow fadeInDown" style="line-height:17px;"><br /> 
                 <i class="fa fa-check-circle-o" style="font-size:68px; margin-top:20px;"></i> <br />
                  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/login" >Click Here to Log In</a>
                 </p>
            </div>
        </div>
    </section><!--/#main-slider-->
    

    
    
 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
 <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/datepicker/bootstrap-datepicker.js"></script>
 <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/timepicker/bootstrap-timepicker.min.js"></script>
 <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/customer.js"></script>
 