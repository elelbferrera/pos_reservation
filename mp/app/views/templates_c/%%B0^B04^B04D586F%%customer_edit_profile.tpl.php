<?php /* Smarty version 2.6.26, created on 2017-05-19 03:58:12
         compiled from customer_edit_profile.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<style type="text/css">
  <?php echo '
    .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
  }
    .inputfile + label {
    font-size: 1em;
    font-weight: 400;
    display: inline-block;
    cursor: pointer;
/*    padding: 10px 15px;*/
    margin-top: 5px;
  }

  '; ?>

</style>



 <section id="main-page" style="padding-top: 10px; padding-bottom: 10px; min-height:150px;"></section><!--/#main-slider-->
   <div class="container">
      <section class="content">
          <div id="divSignup" name="divSignup">
          <form name="frmRegisterMe" id="frmRegisterMe">
            <div class="row">
                        <div class="col-md-3">  
                            <div class="box box-primary customer_myprofile">
                              <div class="box-body box-profile">
                                    <?php if ($this->_tpl_vars['customer']['photo'] == ''): ?>
                                      <img id="myImg" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/dist/img/user4-128x128.jpg" class="profile-user-img img-responsive img-circle" />
                                    <?php else: ?>
                                      <img id="myImg" src="<?php echo $this->_tpl_vars['customer']['photo']; ?>
" class="profile-user-img img-responsive img-circle" />
                                    <?php endif; ?>
                                     <h3 class="profile-username text-center"><?php echo $this->_tpl_vars['customer']['first_name']; ?>
 <?php echo $this->_tpl_vars['customer']['last_name']; ?>
</h3>
                                     <p class="text-muted text-center">Your Profile Picture</p>
                                    <!--  <input type='file' /> -->
                                    <!--  <div class="group">
                                      <label for="user" class="label2"></label>
                                     </div>-->
                                    <div class="form-group">
                                        <label for="user" class="label2"></label>  
                                        <input type="file" name="file" id="file" class="inputfile" value="$customer.photo"  />
                                        <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary" style="width: 100%;">
                                        <i class="fa fa-upload"></i> Change Photo</label>  
                                    </div> 
                               
                              </div>
                            </div>
                        </div>
                      
                        <div class="col-md-9">

                          <div class="row">
                               <div class="col-md-10">
                                    <h3 class="section-title wow fadeInDown">Your Information</h3>
                               </div>
                               <div class="col-md-2">
                                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/my_profile" class="btn btn-primary btn-block"><i class="fa fa-mail-reply"></i> back</a>
                               </div>
                          </div>


                            <div class="box box-primary">
                            <div class="box-body box-profile">
                            <form name="frmRegisterMe" id="frmRegisterMe">


                                          <div class="row">
                                              <div class="col-sm-6">
                                                     <div class="form-group">
                                                        <label for="user" class="label2">First Name *</label>
                                                        <input id="txtFirstName" name="txtFirstName" value="<?php echo $this->_tpl_vars['customer']['first_name']; ?>
" type="text" class="form-control" required="required">
                                                     </div>
                                                     <div class="form-group">
                                                        <label for="user" class="label2">Last Name *</label>
                                                        <input id="txtLastName" name="txtLastName"  value="<?php echo $this->_tpl_vars['customer']['last_name']; ?>
" type="text" class="form-control">
                                                     </div>
                                              </div>
                                              <div class="col-sm-6">    
                                                     <div class="form-group">
                                                        <label for="user" class="label2">Middle Name</label>
                                                        <input id="txtMiddleName" name="txtMiddleName"  value="<?php echo $this->_tpl_vars['customer']['middle_name']; ?>
" type="text" class="form-control">
                                                     </div>
                                                     <div class="form-group">
                                                        <label for="user" class="label2">Contact Number *</label>
                                                        <input id="txtContactNumber" name="txtContactNumber"  type="text" class="form-control"  value="<?php echo $this->_tpl_vars['customer']['mobile_number']; ?>
" >
                                                      </div>
                                              </div>
                                              
                                               <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="user" class="label2">State</label>
                                                        <input id="txtState" name="txtState"  value="<?php echo $this->_tpl_vars['customer']['state']; ?>
" type="text" class="form-control">
                                                    </div>
                                                    
                                              </div>
                                               <div class="col-sm-12">
                                                     <div class="form-group">
                                                        <label for="user" class="label2">Address</label>
                                                        <input id="txtAddress" name="txtAddress" type="text" class="form-control"  value="<?php echo $this->_tpl_vars['customer']['address']; ?>
">
                                                    </div>
                                              </div>
                                              
                                               <div class="col-sm-6">
                                                      <div class="form-group">
                                                         <label for="user" class="label2">ZIP *</label>
                                                         <input id="txtZip" name="txtZip" type="text" class="form-control"  value="<?php echo $this->_tpl_vars['customer']['zip']; ?>
">
                                                      </div>
                                                      <div class="form-group">
                                                          <label for="user">Gender</label>
                                                          <select class="form-control" name="txtGender" id="txtGender" style="border-radius: 5px;">
                                                              <option value="Male" style="color:#000;" <?php if ($this->_tpl_vars['customer']['gender'] == 'Male'): ?> selected="selected" <?php endif; ?> >Male</option>
                                                              <option value="Female" style="color:#000;" <?php if ($this->_tpl_vars['customer']['gender'] == 'Female'): ?> selected="selected" <?php endif; ?>>Female</option>
                                                          </select>
                                                      </div>
                                              </div>
                                              <div class="col-sm-6">
                                                     <div class="form-group">
                                                        <label for="user" class="label2">City</label>
                                                        <input id="txtCity" name="txtCity" type="text" class="form-control" value="<?php echo $this->_tpl_vars['customer']['city']; ?>
">
                                                    </div>
                                                    <div class="form-group group">
                                                        <label>Birthdate</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                               <input type="text" class="form-control input" name="txtBirthDate" id="txtBirthDate" data-select="datepicker" style="border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 5px;border-bottom-right-radius: 5px; padding: 0px;
padding-left: 15px;"  value="<?php echo $this->_tpl_vars['customer']['birth_date']; ?>
">
                                                        </div> 
                                                     </div>
                                                     
                                              </div>

                                            
                                              
                                              <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="pass" class="label2">Email Address </label>
                                                        <input id="txtEmailAddress" name="txtEmailAddress"  value="<?php echo $this->_tpl_vars['customer']['email']; ?>
" type="text" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                            <input type="submit" id="btnUpdate" name="btnUpdate" class="btn btn-primary btn-lg" value="Update My Profile">
                                                   </div>
                                                     
                                              </div>
                                              
                                             
                                              
                                         </div>


                            </form>
                            </div>
                            </div>
                      

                        </div>
                    </div>
                    </form>
                </div>
            </div>
          </div>
      </section>
    </div>




        
        
        <div id="divSuccess" name="divSuccess" class="container" style="display: none;">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Successful Registration</h2>
                <p class="text-center wow fadeInDown" style="line-height:17px;"><br /> 
                 <i class="fa fa-check-circle-o" style="font-size:68px; margin-top:20px;"></i> <br />
                  <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/login" >Click Here to Log In</a>
                 </p>
            </div>
        </div>
    </section><!--/#main-slider-->
    

    
    
 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
 <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/customer.js"></script>
<script type="text/javascript">
<?php echo '
$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoaded(e) {
    $(\'#myImg\').attr(\'src\', e.target.result);
};
'; ?>

</script>