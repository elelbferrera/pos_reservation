<?php /* Smarty version 2.6.26, created on 2016-09-14 05:17:32
         compiled from partner_edit.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/partner.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div class="container">
    	<div class="row">
            <!-- Content Header (Page header / Title / Breadcrumbs) -->
            <section class="content-header">
                <h1>
                Partner
                <small>Profile</small>
                </h1> 
                <ol class="breadcrumb">
                    <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/management"><i class="fa fa-dashboard"></i>Partner</a></li>
                    <li class="active">Profile</li>
                </ol>
            </section>
            
            <!-- Main content -->
            <section class="content">
            	<div class="row">
                <!-- Your Page Content Here -->
                    <div class="col-sm-3">
                        <div class="sidelink">
                        	<!-- <h3 class="sidelink-title">Partner Profile</h3> -->
                            <ul class="sidelink-nav">
                            	<!-- <li><a href="summary.php">Summary</a></li> -->
                                <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/edit/<?php echo $this->_tpl_vars['partner_id']; ?>
" class="selected">Profile</a></li>
                                <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/contact/<?php echo $this->_tpl_vars['partner_id']; ?>
">Contacts</a></li>
                                <li><a href="#">Payment Info</a></li>
                                <li><a href="#">Products</a></li>
                                <li><a href="invoice.php">Invoices</a></li>
                                <li><a href="#">Attachments</a></li>
                                <li><a href="#">Notes</a></li>
                                <li><a href="#">Communication</a></li>
                                <li><a href="#">Log</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box-header with-border">
                            	<!-- <h3 class="box-title">Profile</h3> -->
                            </div>
                        	<!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" id="frmUpdatePartner" name="frmUpdatePartner">
                                <div class="box-body">
                                	<input type="hidden" id="txtPartnerID" name="txtPartnerID" value="<?php echo $this->_tpl_vars['partner_id']; ?>
">
                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Company Name:</label>
                                                    <input type="text" class="form-control" id="txtCompanyName" name="txtCompanyName" placeholder="Enter company name" value="<?php echo $this->_tpl_vars['partner_info']['company_name']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">DBA:</label>
                                                    <input type="text" class="form-control" id="txtDBA" name="txtDBA" placeholder="Enter DBA" value="<?php echo $this->_tpl_vars['partner_info']['dba']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1"  value="<?php echo $this->_tpl_vars['partner_info']['address1']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2"  value="<?php echo $this->_tpl_vars['partner_info']['address2']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city" value="<?php echo $this->_tpl_vars['partner_info']['city']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text" class="form-control" id="txtState" name="txtState" placeholder="Enter state" value="<?php echo $this->_tpl_vars['partner_info']['state']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip" value="<?php echo $this->_tpl_vars['partner_info']['zip']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
									                  <?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['partner_info']['country_id']): ?> selected="selected"  <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email" value="<?php echo $this->_tpl_vars['partner_info']['email']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1" value="<?php echo $this->_tpl_vars['partner_info']['phone1']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2"  value="<?php echo $this->_tpl_vars['partner_info']['phone2']; ?>
">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <?php $_from = $this->_tpl_vars['partner_type']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['partner_info']['partner_type_id']): ?> selected="selected"  <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>

                                                </div>
                                            </div>
                                        </div> 
                                </div>
                                <!-- /.box-body -->
                            
                                <div class="box-footer">
                                    <button type="submit" id="btnUpdatePartner" name="btnUpdatePartner" class="btn btn-primary">Update Partner</button>
                                </div>
                            </form>
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
</div>
<!-- /.content-wrapper -->