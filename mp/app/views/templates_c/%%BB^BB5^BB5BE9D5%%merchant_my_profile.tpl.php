<?php /* Smarty version 2.6.26, created on 2016-12-22 16:05:36
         compiled from merchant_my_profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip', 'merchant_my_profile.tpl', 87, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/my_profile.js"></script>
 <!-- .content -->
                      <section id="content">
                        <section class="vbox">

                           <header class="header bg-gray b-b b-light">
                                  <div class="row">
                                       <div class="col-md-6 col-xs-6">
                                          <div class="breadtitle-holder">
                                              <div class="breadtitle">
                                                  <i class="fa fa-user titleFA"></i> 
                                                  <p class="headeerpage-title">Merchant Profile</p>
                                              </div>
                                          </div>
                                       </div>
                                       <div class="col-md-6 col-xs-6 ta-right">
                                          <div class="breadtitle-holder2">
                                              <div class="btn-group">
                                                     <!-- <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-home"></i></button> -->
                                                      <div class="btn-group hidden-nav-xs">
                                                            <!-- <button type="button" class="btn btn-sm btn-primary" onClick="location.href='register-branch.php'">Register New Branch</button> -->
                                                      </div>
                                               </div>
                                             <!--   <div class="btn-group">
                                                  <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                      <div class="btn-group hidden-nav-xs">
                                                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                    </div>
                                               </div> -->
                                          </div>
                                       </div>
                                  </div>
                          </header>


                           
                            <section class="scrollable wrapper">
                                <div class="row">
                                     <form role="form" id="frmMerchantProfile" name="frmMerchantProfile" enctype="multipart/form-data">
                                         <div class="col-md-2 col-xs-2">
                                                   <label>Merchant Logo:</label><br />
                                                  	<?php if ($this->_tpl_vars['profile']['logo'] == ''): ?>
                                                  		<img src="<?php echo $this->_tpl_vars['webroot']; ?>
/images/store-avatar.png" class="specialist-avatar" /><br />
                                                  	<?php else: ?>
                                                  		<img src="<?php echo $this->_tpl_vars['profile']['logo']; ?>
" class="specialist-avatar" /><br />
                                                  	<?php endif; ?>
                                                  
                                                  <input type="file" name="file" id="file" class="inputfile" value="$profile.logo"  /> 
                                                  <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary">
                                                  	<i class="fa fa-upload"></i> Change Photo</label>  
                                                  <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                                         
                                         </div>
                                         <div class="col-md-10 col-xs-10">
                                            <section class="panel panel-default2">
                                                <header class="panel-heading bg-light">
                                                    <ul class="nav nav-tabs pull-right">
                                                        <li class="active">
                                                        	<!-- <a href="#information" data-toggle="tab"><i class="fa fa-file text-default"></i> Information</a></li> -->
                                                        <li>
                                                        	<!-- <a href="#businesshours" data-toggle="tab"><i class="fa fa-home text-default"></i> Business Hours</a>-->
                                                        </li>
                                                        <li>
                                                        	<!-- <a href="#branch" data-toggle="tab"><i class="fa fa-home text-default"></i> Branch</a> -->
                                                        </li>
                                                    </ul> <span class="hidden-sm">Merchant Information</span> </header>
                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="information">
                                                                  <div class="row">
                                                                      <div class="col-md-8 col-xs-8">
                                                                          <div class="form-group">
                                                                                <label>Merchant Name:</label>
                                                                                <input type="text" class="form-control" name="txtMerchantName" id="txtMerchantName" placeholder="Merchant name" value="<?php echo $this->_tpl_vars['profile']['business_name']; ?>
">
                                                                                <input type="hidden" class="form-control" name="txtDBA" id="txtDBA" placeholder="Merchant name" value="<?php echo $this->_tpl_vars['profile']['dba']; ?>
">
                                                                                <input type="hidden" class="form-control" name="txtProcessor" id="txtProcessor" placeholder="Merchant name" value="<?php echo $this->_tpl_vars['profile']['processor']; ?>
">
                                                                                <input type="hidden" class="form-control" name="txtMID" id="txtMID" placeholder="Merchant name" value="<?php echo $this->_tpl_vars['profile']['mid']; ?>
"> 
                                                                           </div>
                                                                      </div>
																	
                                                                  </div>

                                                                  <div class="row">
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <div class="form-group">
                                                                              <label>About</label>
                                                                              <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"><?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['description'])) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)); ?>
</textarea>
                                                                         </div>
                                                                    </div>
                                                                  </div>
                                                                  <div class="row">
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <div class="form-group">
                                                                              <label>Address</label>
                                                                              <textarea rows="1" cols="50" id="txtAddress" name="txtAddress" class="form-control"><?php echo ((is_array($_tmp=$this->_tpl_vars['profile']['address1'])) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)); ?>
</textarea>
                                                                         </div>
                                                                    </div>
                                                                  </div>
																  <div class="row">
																  	<!-- 'address1' => 'Lot 25 Blk 11 La Aldea Subd., Tabang',
																	'city' => 'Bulacan',
																	'state' => 'Guiguinto',
																	'zip' => '3015',
																	'country' => 'Philippines',
																	'email' => 'lemmuelcabuhat@gmail.com',
																	'phone1' => '639178580549',
																	'first_name' => 'Lemmuel Alvin',
																	'last_name' => 'Cabuhat',
																	'contact_email' => 'lemmuelcabuhat@gmail.com' -->
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label>City:</label>
                                                                                <input type="text" id="txtCity" name="txtCity" value="<?php echo $this->_tpl_vars['profile']['city']; ?>
" class="form-control" placeholder="City"> 
                                                                           </div>
                                                                      </div>
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label>State:</label>
                                                                                <input type="text" id="txtState" name="txtState" value="<?php echo $this->_tpl_vars['profile']['state']; ?>
" class="form-control" placeholder="State"> 
                                                                           </div>
                                                                      </div>
                                                                      <div class="col-md-3 col-xs-3">
                                                                          <div class="form-group">
                                                                                <label>Zip:</label>
                                                                                <input type="text" id="txtZip" name="txtZip" class="form-control" placeholder="Zip" value="<?php echo $this->_tpl_vars['profile']['zip']; ?>
"> 
                                                                           </div>
                                                                      </div>
																  	  <div class="col-md-3 col-xs-3">
	                                                                        <div class="form-group">
	                                                                          <label>Country</label>
	                                                                              <select name="txtCountry" id="txtCountry" class="form-control m-b">
											                                            <?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
											                                        		<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
" <?php if ($this->_tpl_vars['profile']['country'] == $this->_tpl_vars['item']['name']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
											                                        	<?php endforeach; endif; unset($_from); ?>
											                                        </select>
	                                                                        </div>
                                                                      </div>
																  	
																  </div>

                                                                  <div class="row">
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner First Name</label>
                                                                              <input type="text" class="form-control" placeholder="First name" name="txtFirstName" id="txtFirstName" value="<?php echo $this->_tpl_vars['profile']['first_name']; ?>
"> 
                                                                            </div>
                                                                      </div>
                                                                      <!-- <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner Middle Name</label>
                                                                              <input type="text" class="form-control" placeholder="Middle name" id="txtMiddleName" name="txtMiddleName" value="<?php echo $this->_tpl_vars['profile']['middle_name']; ?>
"> 
                                                                            </div>
                                                                      </div> -->
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner Last Name</label>
                                                                              <input type="text" class="form-control" placeholder="Last name" id="txtLastName" name="txtLastName"  value="<?php echo $this->_tpl_vars['profile']['last_name']; ?>
"> 
                                                                            </div>
                                                                      </div>
                                                                  </div>
                                                                  <div class="row">
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Contact No.</label>
                                                                              <input type="text" class="form-control" placeholder="Contact No." name="txtContactNo" id="txtContactNo"  value="<?php echo $this->_tpl_vars['profile']['phone1']; ?>
"> 
                                                                            </div>
                                                                      </div>
                                                                      <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Company Email</label>
                                                                              <input type="text" class="form-control" placeholder="Company Email" id="txtEmail" name="txtEmail"  value="<?php echo $this->_tpl_vars['profile']['email']; ?>
"> 
                                                                            </div>
                                                                      </div>
                                                                      <!-- <div class="col-md-4 col-xs-4">
                                                                           <div class="form-group">
                                                                              <label>Owner Email</label>
                                                                              <input type="text" class="form-control" placeholder="Owners Email" id="txtContactEmail" name="txtContactEmail"> 
                                                                            </div>
                                                                      </div> -->
                                                                  </div>

														</div>
                                                        <!-- end -->
                                                        <div class="tab-pane" id="businesshours">
                                                                    <div class="col-md-6 col-xs-6">
                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Monday</div>
                                                                                        <div class="sched-body">

                                                                                              <div class="row">
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>    
                                                                                              </div>
                                                                                         </div>
                                                                                    </div>

                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Wednesday</div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row">
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>

                                                                                     <div class="sched-holder">
                                                                                        <div class="sched-header">Friday</div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row">
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>

                                                                                    <div class="sched-holder">
                                                                                        <div class="sched-header">Sunday</div>
                                                                                        <div class="sched-body">
                                                                                              <div class="row">
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                      <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>Start Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                                  <div class="col-md-6 col-xs-6">
                                                                                                     <div class="bootstrap-timepicker">
                                                                                                          <div class="form-group">
                                                                                                                <label>End Time:</label>
                                                                                                                <div class="input-group">
                                                                                                                  <input type="text" class="form-control timepicker">
                                                                                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                </div>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  </div>
                                                                                              </div>
                                                                                         </div>
                                                                                     </div>



                                                                                </div>
                                                                                <div class="col-md-6 col-xs-6">
                                                                                                              <div class="sched-holder">
                                                                                                                  <div class="sched-header">Tuesday</div>
                                                                                                                  <div class="sched-body">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                                <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>Start Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                               <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>End Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                   </div>
                                                                                                               </div>

                                                                                                               <div class="sched-holder">
                                                                                                                  <div class="sched-header">Thursday</div>
                                                                                                                  <div class="sched-body">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                                <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>Start Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                               <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>End Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                   </div>
                                                                                                               </div>

                                                                                                              <div class="sched-holder">
                                                                                                                  <div class="sched-header">Saturday</div>
                                                                                                                  <div class="sched-body">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                                <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>Start Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6 col-xs-6">
                                                                                                                               <div class="bootstrap-timepicker">
                                                                                                                                    <div class="form-group">
                                                                                                                                          <label>End Time:</label>
                                                                                                                                          <div class="input-group">
                                                                                                                                            <input type="text" class="form-control timepicker">
                                                                                                                                            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                                                                                                                          </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                   </div>
                                                                                                               </div>
                                                                                             </div>
                                                        </div>
                                                        <!-- end -->
                                                        <div class="tab-pane" id="branch">
                                                              <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                           <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>


                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                               <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                      <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                             <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                           <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                              <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>

                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                               <div class="branch-show-holder">
                                                                  <div class="branch-show-header">Branch 1</div>
                                                                  <div class="branch-show-body">
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Branch Name</label>
                                                                                      <input type="text" class="form-control" placeholder="Branch name"> 
                                                                                  </div>
                                                                            </div>
                                                                           <div class="col-md-7 col-xs-7">
                                                                                 <div class="form-group">
                                                                                      <label>Address</label>
                                                                                      <input type="text" class="form-control" placeholder="Address"> 
                                                                                  </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-5 col-xs-5">
                                                                                 <div class="form-group">
                                                                                      <label>Decription</label>
                                                                                      <!-- <textarea rows="3" cols="50" id="txtDescription" name="txtDescription"  class="form-control"></textarea> -->
                                                                                  </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-xs-7">
                                                                                  <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" placeholder="City"> 
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="col-md-6 col-xs-6">
                                                                                           <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" placeholder="State"> 
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>

                                                                                  <div class="form-group">
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-md btn-dark" onClick="location.href='register-branch.php'">Edit Information</button>
                                                                                       </div>
                                                                                       <div class="btn-group">
                                                                                          <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                                                                              <div class="btn-group hidden-nav-xs">
                                                                                                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule">Business Hours</button>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  
                                                                            </div>
                                                                        </div>

                                                                       
                                                                   </div>
                                                               </div>
                                                               <!-- branch-show end -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>



                                             
                                         <div class="row">
                                              <div class="col-md-12 col-xs-12">
                                                      <div class="ta-right">
                                                          <button type="submit" class="btn btn-primary">Save Information</button>
                                                          <!-- <button type="button" class="btn btn-md btn-dark" onClick="location.href='profile.php'">Cancel</button> -->
                                                     </div>
                                              </div>
                                        </div>
                                           


                                         </div>






<!--                                          <div class="clear"></div> -->

                                     </form>
                                 </div>
                            </section>
                       </section>
                     </section>