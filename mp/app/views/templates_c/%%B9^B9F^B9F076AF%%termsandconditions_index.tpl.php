<?php /* Smarty version 2.6.26, created on 2017-05-15 08:37:05
         compiled from termsandconditions_index.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <section id="about" style="padding-bottom: 50px;background-color: #fafafa;background-image: url(<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/terms-bg.png);background-repeat: repeat-x;background-position: center bottom;">
        <div class="container">

            <div class="section-header section-header2">
            	<h3 class="column-title">Terms and Conditions </h3>

            	<p>By using Go3 Online Reservation operated by Go3 Solutions Inc. You agree to these terms and conditions, as they may be amended from time to time by Go3 Solutions Inc. The application is offered subject to your acceptance without modification, condition or reservation of all of the terms and conditions contained herein. Use this application at your uncertainty.</p><br /><br />

            	<h5>Information Collection and Use</h5>

            	<p>We may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. This may include personal information rest assured that it is only for providing the essentials for the improvement of the application.</p><br />

            	<h5>Log Data</h5>

            	<p>Just like any other application operators, we collect information that you send whenever you use our Application. Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, and the pages of our App that you visit, the time and date of your visit, the time spent on those pages and other statistics. This section is for businesses that use analytics or tracking services in websites or apps, like Google Analytics.</p><br />
                
                <h5>Communications</h5>

                <p>We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information. The Communications section is for businesses that may contact users via email, SMS messaging or other methods. </p>
<br />
                <h5>Security</h5>

                <p>We value the privacy & security of your Personal Information as much as you do but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>
<br />
                <h5>Acceptable Use</h5>

                <p>You must not use this app. that would cause, or may cause, disturbance to the app. Or deterioration of the availability or accessibility of the app; or in any way which is unlawful, illegal, fraudulent or harmful, or in any connection with this matter. You must not use this app. to replicate, cache, host, pass on, circulate, use, publish or disseminate any other material that would cause any trouble with the application.</p>
                <br /><br /><br /><br />

                <p>Contact Us<br />
If you have any questions about this Policy, please contact us:<br />
Tel: 888-377-3818 <br />
Support Email: support@go3solutions.com <br />
</p>

            </div>

            
        </div>
    </section><!--/#about-->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>