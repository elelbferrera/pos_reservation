<?php /* Smarty version 2.6.26, created on 2016-12-20 12:32:57
         compiled from log.tpl */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GO3 Reservation</title>
    <link rel="shortcut icon" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/dist/css/AdminLTE.css">
    <!-- iCheck -->
  	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/iCheck/square/blue.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/assets/css/custom.css">
    	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/pace/pace.min.css">
	<!-- PACE -->
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/pace/pace.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>    

<script type="javascript">
<?php echo '
	$(document).ajaxStart(function() {
		Pace.restart();
	});
	$(\'.ajax\').click(function() {
		$.ajax({
			url : \'#\',
			success : function(result) {
				// $(\'.ajax-content\').html(\'<hr>Ajax Request Completed !\');
			}
		});
	});
'; ?>

	
</script>
    
    
    
<body class="hold-transition login-page">
	


<script>
<?php echo '
	$(function () {
		$(\'input\').iCheck({
			checkboxClass: \'icheckbox_square-blue\',
			radioClass: \'iradio_square-blue\',
			increaseArea: \'20%\' // optional
		});
	});
'; ?>

</script>
</body>
</html>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GO3 Reservation</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/go3reservation.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">

<?php if ($this->_tpl_vars['action_tpl'] != ''): ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['action_tpl']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php endif; ?>


   
<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/iCheck/icheck.min.js"></script>
 


</body>
</html>