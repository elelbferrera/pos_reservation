<?php /* Smarty version 2.6.26, created on 2017-05-19 01:48:10
         compiled from customer_my_profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'customer_my_profile.tpl', 212, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>



















































 <section id="main-page" style="padding-top: 10px; padding-bottom: 10px; min-height:150px;"></section><!--/#main-slider-->

<section id="merchant-profile">




 <div class="container">
 <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary customer_myprofile">
            <div class="box-body box-profile">
              <?php if ($this->_tpl_vars['customer_info']['photo'] == ''): ?>
                <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/dist/img/user4-128x128.jpg" class="profile-user-img img-responsive img-circle" />
              <?php else: ?>
                <img src="<?php echo $this->_tpl_vars['customer_info']['photo']; ?>
" class="profile-user-img img-responsive img-circle" />
              <?php endif; ?>

              <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> -->

              <h3 class="profile-username text-center"><?php echo $this->_tpl_vars['customer_info']['first_name']; ?>
 <?php echo $this->_tpl_vars['customer_info']['last_name']; ?>
</h3>
              <p class="text-muted text-center"><?php if ($this->_tpl_vars['customer_info']['email'] == ''): ?> NA <?php else: ?> <?php echo $this->_tpl_vars['customer_info']['email']; ?>
 <?php endif; ?></p>
              <p class="text-muted text-center"><?php echo $this->_tpl_vars['mobile']; ?>
</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Reservations</b> <a class="pull-right"><?php echo $this->_tpl_vars['reservations_count']; ?>
</a>
                </li>
                <li class="list-group-item">
                  <b>Check In</b> <a class="pull-right"><?php echo $this->_tpl_vars['checkins_count']; ?>
</a>
                </li>
                <li class="list-group-item">
                  <b>Favorite</b> <a class="pull-right"><?php echo $this->_tpl_vars['favorites_count']; ?>
</a>
                </li>
              </ul>

              <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/edit_profile" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> <b>Edit Profile</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="customer_myprofile_dashboard"> 
            <div class="row">
                 <div class="col-md-9">
                      <h3 class="column-title" style="color:#FFF;">Dashboard</h3>
                 </div>
                 <div class="col-md-3">
                      <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/marketplace/in" class="btn btn-primary btn-block"><i class="fa fa-calendar"></i> <b>Make Reservation now</b></a>
                 </div>
            </div>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#reservations" data-toggle="tab">Reservations</a></li>
              <li><a href="#checkin" data-toggle="tab">Check Ins</a></li>
              <li><a href="#favorite" data-toggle="tab">Favorite</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="reservations">
                  
                  <ul id="transaction" class="timeline timeline-inverse">
                      <?php $_from = $this->_tpl_vars['reservations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-calendar"></i> <?php echo $this->_tpl_vars['item']['reservation_date']; ?>
</span>
                              <span class="time"><i class="fa fa-clock-o"></i> <?php echo $this->_tpl_vars['item']['asis_time']; ?>
</span>
                              <h3 class="timeline-header"><a href="#"><?php echo $this->_tpl_vars['item']['business_name']; ?>
</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      <?php if ($this->_tpl_vars['item']['logo'] == ''): ?> 
                                        <img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/nerchant-logo-default.png" alt="" />
                                      <?php else: ?>
                                        <img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['item']['logo']; ?>
" alt="" />
                                      <?php endif; ?>
                                   </div>
                                   <div class="col-md-9">
                                        <?php echo $this->_tpl_vars['item']['description']; ?>

                                        <hr>
                                        <b>Date:</b> <?php echo $this->_tpl_vars['item']['reservation_date']; ?>
<br />
                                        <b>Time:</b> <?php echo $this->_tpl_vars['item']['asis_time']; ?>
 <br />
                                        <b>Service/s Availed:</b> <?php echo $this->_tpl_vars['item']['product']; ?>

                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
<!--                                 <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       <?php endforeach; endif; unset($_from); ?>
                  </ul>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="checkin">
                  <ul id="book" class="timeline timeline-inverse">
                     <?php $_from = $this->_tpl_vars['check_ins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-calendar"></i> <?php echo $this->_tpl_vars['item']['create_date']; ?>
 </span>
                              <span class="time"><i class="fa fa-clock-o"></i><?php echo $this->_tpl_vars['item']['check_in_time']; ?>
 </span>
                              <h3 class="timeline-header"><a href="#"><?php echo $this->_tpl_vars['item']['business_name']; ?>
</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      <?php if ($this->_tpl_vars['item']['logo'] == ''): ?> 
                                        <img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/nerchant-logo-default.png" alt="" />
                                      <?php else: ?>
                                        <img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['item']['logo']; ?>
" alt="" />
                                      <?php endif; ?>
                                   </div>
                                   <div class="col-md-9">
                                        <?php echo $this->_tpl_vars['item']['description']; ?>

                                        <hr>
                                        <b>Check In Date:</b> <?php echo $this->_tpl_vars['item']['create_date']; ?>
 <br />
                                        <b><b>Check In Time:</b> <?php echo $this->_tpl_vars['item']['check_in_time']; ?>
 </label><br />
                                        <b<b>Check In Status:</b> <?php echo $this->_tpl_vars['item']['status']; ?>
 </label><br />
                                        <b>Service/s Availed:</b> <?php echo $this->_tpl_vars['item']['product']; ?>
 </label>
                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
 <!--                                <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       <?php endforeach; endif; unset($_from); ?>
                  </ul>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="favorite">
                  <ul id="book" class="timeline timeline-inverse">
                     <?php $_from = $this->_tpl_vars['favorites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-heart" style="color: #EB468A;"></i></span>
                              <h3 class="timeline-header"><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['business_name']; ?>
</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      <?php if ($this->_tpl_vars['item']['logo'] == ''): ?>  
                                        <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/nerchant-logo-default.png" alt="" /></a>
                                      <?php else: ?>
                                        <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><img class="group list-group-image img-responsive img-circle" src="<?php echo $this->_tpl_vars['item']['logo']; ?>
" alt="" /></a>
                                      <?php endif; ?>
                                   </div>
                                   <div class="col-md-9">
                                        <?php echo ((is_array($_tmp=$this->_tpl_vars['item']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 250) : smarty_modifier_truncate($_tmp, 250)); ?>
 <br /> <br />
                                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>:  <?php echo $this->_tpl_vars['item']['address1']; ?>
 <?php echo $this->_tpl_vars['item']['city']; ?>
 <?php echo $this->_tpl_vars['item']['state']; ?>
 <?php echo $this->_tpl_vars['item']['zip']; ?>

                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
 <!--                                <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       <?php endforeach; endif; unset($_from); ?>
                  </ul>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
   </section>
</div>















<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/customer.js"></script>