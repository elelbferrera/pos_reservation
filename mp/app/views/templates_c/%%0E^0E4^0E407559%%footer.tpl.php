<?php /* Smarty version 2.6.26, created on 2017-05-19 03:22:09
         compiled from footer.tpl */ ?>

<div id="dlgMessage" name="dlgMessage" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title modal-title-custom" id="lblMessageTitle" name="lblMessageTitle"><label>Message Title</label></h5>
                    <div class="row header-review">
                        <!-- <div class="col-md-2 col-sm-2 col-xs-3">
                             <img src="images/profile/merchant-logo-default.png" class="img-responsive writereview-profile" />
                        </div> -->
                        <!-- <div class="col-md-10 col-sm-10 col-xs-9">
                             <div class="modal-review-storetitle">Beauty Spa & Nail Salon</div>
                             <p><i class="fa fa-map-marker"></i>  Main Street. East Greenwich, RI 02818</p>
                        </div> -->

                    </div>
                  </div>
                  <div class="modal-body">
                        <div class="form-group">
                            <label id="lblMessage" name="lblMessage">Rate it</label><br />
                            
                        </div>
                  </div>
                  <div class="modal-footer"> 
                         <div class="col-md-4 col-sm-4 sm-col pull-left" style="display: inline-flex; padding-left:0px;">
                                <!-- <a href="#" class="tabbtn btn btn-success" id="#reserveprocess" data-toggle="modal" data-target="#summaryReservation"  data-dismiss="modal"><i class="fa fa-check-circle"></i> Publish</a> -->
                                <a href="#" class="tabbtn btn btn-success" id="#cancelreserve" data-dismiss="modal"><i class="fa fa-check-circle"></i> Ok</a>
                         </div>
                 </div>
            </div>
       </div>
</div>

<div id="dlgLoading" name="dlgLoading" class="modal fade">
      <div class="modal-dialog" >
            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 5px;">
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>
                    <div class="modal-review-storetitle" id="lblTitle" name="lblTitle" style="text-transform: uppercase; font-weight:bold;">Loading</div>
                  </div>
                  <div class="modal-body">
                      <p style="text-align:center; padding-left: 255px;"><img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/5.gif" class="img-responsive" /></p>
                  </div>
            </div>
       </div>
</div>


    <!--/#get-in-touch-->
  <?php  if(!$_GET){  ?>
	<footer id="footer">
        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                        <div class="footer-title">Contact Us</div>
                         <p>
Tel: 888-377-3818 <br >
Support Email: support@go3solutions.com </p>
                </div>
                 <div class="col-sm-6 col-xs-12" style="display:none">
                        <div class="footer-title">About Go3Reservation</div>
                         <p>GO3 Solutions Company is a full service end-to- end web development and mobile applications. With our team experts and professionals in their respective specialties in helping up your business establish and maintain its presence over the web. We understand the value of effective marketing. Our goal is to provide our clients with excellent business solutions to help you widen the extent of your business.</p>
                </div>
                 <div class="col-sm-3 col-xs-12">
                       <div class="footer-title">Quick Links</div>
                       <ul>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/termsandconditions">Terms of Use</a></li>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/privacypolicy">Private Policy</a></li>
                           <!-- <li><a href="">Contact Us</a></li> -->
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/customer/login">Login to my account</a></li>
                       </ul>
                </div>
                <div class="col-sm-3 col-xs-12"  style="display:none">
                      <div class="footer-title">Subscribe</div>
                      <p>Receive sale news and exclusive in-store discounts!</p>
                </div>
                 <div class="col-sm-3 col-xs-12" style="display:none">
                       <div class="footer-title">My account</div>
                       <ul>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/termsandconditions">My personal info</a></li>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/privacypolicy">My Transaction</a></li>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/privacypolicy">My Check In</a></li>
                           <li><a href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/privacypolicy">Favorite</a></li>
                       </ul>
                </div>
                <div class="col-sm-3 col-xs-12" style="display:none">
                        <div class="footer-title">Stay Connected</div>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                </div> 
            </div>


          
        </div>
        <div class="footer3">
             <div class="container">
                  <div class="row">
                    <div class="col-sm-12">
                        &copy; 2017 Go3Reservation. Designed by <a target="_blank" href="http://go3solutions.com/" title="GO3Solutions">GO3Solutions </a>
                    </div>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
  <?php  }  ?>

	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.js"></script> -->
    
    <!-- <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.datepicker.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/owl.carousel.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/mousescroll.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/smoothscroll.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.isotope.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.inview.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/wow.min.js"></script>
    <!-- <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/main.js"></script> -->
    
    
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.paginate.js"></script>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/ion.rangeSlider.min.js"></script>
    
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/public.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.maskedinput-1.2.2.js"></script>
    
    
    </body>
</html>