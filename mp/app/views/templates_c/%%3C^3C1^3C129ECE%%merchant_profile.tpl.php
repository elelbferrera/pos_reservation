<?php /* Smarty version 2.6.26, created on 2017-05-19 15:57:22
         compiled from merchant_profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'merchant_profile.tpl', 557, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>





<input type="hidden" name="txtLongitude" id="txtLongitude" value="<?php echo $this->_tpl_vars['m']['longitude']; ?>
">
<input type="hidden" name="txtLatitude" id="txtLatitude" value="<?php echo $this->_tpl_vars['m']['latitude']; ?>
">

<input type="hidden" name="txtCity" id="txtCity" value="<?php echo $this->_tpl_vars['m']['city']; ?>
">
<input type="hidden" name="txtZip" id="txtZip" value="<?php echo $this->_tpl_vars['m']['zip']; ?>
">
<input type="hidden" name="txtState" id="txtState" value="<?php echo $this->_tpl_vars['m']['state']; ?>
">

<input type="hidden" name="txtAddress" id="txtAddress" value="<?php echo $this->_tpl_vars['m']['address1']; ?>
">
<input type="hidden" name="txtBusinessName" id="txtBusinessName" value="<?php echo $this->_tpl_vars['m']['business_name']; ?>
">

    <section id="main-page" class="cd-container">
          <div class="container">
            <div class="row">
                  <div class="col-lg-2 col-sm-4 col-xs-12">
                      <div class="logo-merchant">
                      	 <?php if ($this->_tpl_vars['m']['logo'] == ''): ?>
                          	<img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/merchant-logo-default.png" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                         <?php else: ?>
                         	<img src="<?php echo $this->_tpl_vars['m']['logo']; ?>
" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                         <?php endif; ?>
                     
                      <?php  if(isset($_SESSION['session_id'])){  ?>
                            <a href="#" id="writereview" data-toggle="modal" data-target="#writereviewpage" class="tabbtn btn btn-primary btn-mark">Write a review</a>
                        <?php  }  ?>
                        <?php  if(isset($_SESSION['session_id'])){  ?>
                          <?php if ($this->_tpl_vars['favorite_id'] > 0): ?>
                            <a href="#" id="btnFavorite" name="btnFavorite" class="tabbtn btn btn-primary btn-mark"><i class="fa fa-heart"></i></a>
                          <?php else: ?>
                            <a href="#" id="btnFavorite" name="btnFavorite" class="tabbtn btn btn-info btn-mark"><i class="fa fa-heart"></i></a>
                          <?php endif; ?>
                        <?php  }  ?>
                        <!-- <a href="" class="tabbtn btn btn-primary btn-mark"><i class="fa fa-bookmark"></i></a><br /> -->
                        <form id="frmFavorite" name="frmFavorite">
                          <input type="hidden" name="txtMerchantIdFavorite" id="txtMerchantIdFavorite" value="<?php echo $this->_tpl_vars['m']['id']; ?>
"/>
                          <input type="hidden" name="txtFavoriteId" id="txtFavoriteId" value="<?php echo $this->_tpl_vars['favorite_id']; ?>
"/>
                          
                        </form>

 </div>

                  </div>
                  <div class="col-lg-10 col-sm-8 col-xs-12">
                      <div class="merchant-title"><?php echo $this->_tpl_vars['m']['business_name']; ?>
</div>
                         <!-- <div class="col-md-3 col-sm-12 col-xs-12 pull-right"> -->
                            
                            <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark">Check In</a> 
                         
                  </div>-->
                      <div class="merchant-about"><?php echo $this->_tpl_vars['m']['address1']; ?>
 <?php echo $this->_tpl_vars['m']['city']; ?>
 <?php echo $this->_tpl_vars['m']['state']; ?>
 <?php echo $this->_tpl_vars['m']['zip']; ?>
 </div>
                      <div class="merchant-about"><?php echo $this->_tpl_vars['m']['phone1']; ?>
</div>
                      <!-- <div class="merchant-about">Fax: 401-886-9201</div> -->
                      <div class="merchant-rating"> </div>
                      	<!-- <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/star-rate5.png" class="img-responsive" />  | -->
                      <!-- <a href="#" id="seeallreview" data-toggle="modal" data-target="#allreview"> &nbsp;&nbsp; View all review &nbsp;&nbsp; </a>  / -->
                    
                      <div class="merchant-miles"></div>
                      <div class="clearboth"></div>
                      <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark">Check Me In</a><br> -->
                      <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/reservation/<?php echo $this->_tpl_vars['m']['id']; ?>
" class="tabbtn btn btn-primary btn-reservation col-lg-4 col-sm-6 col-xs-12" id="set-reservation"><i class="fa fa-calendar"></i> Click Here to Create Reservation</a> <br />
                      <!-- <a href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-primary btn-mark"> Click Here for Same Day Check In</a> -->
                      <div style="clear:both;"></div>

                      <a style="color:#FFF; background-color:#34536d;border-color: #274258;" href="#" data-toggle="modal" data-target="#checkin" class="tabbtn btn btn-info btn-mark btn-reservation col-lg-4 col-sm-6 col-xs-12" id="set-reservation"><i class="fa fa-check-circle"></i> Same Day Check In</a>

                  </div>
            </div>
        </div>
    </section><!--/#main-slider-->
    
     

    <section id="merchant-profile">
        <div class="container">
            <div class="row detail-content">
                <div class="col-lg-7 col-sm-7 col-xs-12">
    	                  <div class="about-title wow fadeInLeft">Find Us here</div>     	
						                <div style="padding-bottom: 250px"  id="map" name="map"></div>
                         
          
          
                          <!--
                         <div class="background-white p20 wow fadeInLeft">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.5608798519556!2d-74.01407328449916!3d40.70566777933266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a16bbacf60d%3A0x44070e4c624cc9ec!2s50+Broad+St%2C+New+York%2C+NY+10004%2C+USA!5e0!3m2!1sen!2sph!4v1479356200584" width="600" height="300" frameborder="0" style="border:0; width:100%; " allowfullscreen></iframe>
                         </div> -->
                        

                         
                        <!-- <div class="about-title wow fadeInLeft">Our Gallery</div> -->
                         <!-- <div class="background-white p20 wow fadeInLeft">

                        <div id="portfolio"> -->
                          <!-- <div class="portfolio-items"> -->
                          	
                <!-- <div class="portfolio-item creative">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/01.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 1</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/02.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 2</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/03.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 3</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/04.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 4</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/05.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 5</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/06.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 5</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item creative portfolio">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/07.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 7</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->

                <!-- <div class="portfolio-item corporate">
                    <div class="portfolio-item-inner">
                        <img class="img-responsive" src="images/portfolio/08.jpg" alt="">
                        <div class="portfolio-info">
                            <h3>Portfolio Item 8</h3>
                            Lorem Ipsum Dolor Sit
                            <a class="preview" href="images/portfolio/full.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div> -->
                <!--/.portfolio-item-->
            <!-- </div> -->

            <!-- </div>



                         </div> -->
                         
                        <div class="about-title wow fadeInLeft">Services We offer</div>
                          <!--<div class="background-white p20 wow fadeInLeft">
                             <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex . </p> -->
                 <!--             <div class="row">
                                <ul class="services">
                                	
                                    	<li class="col-lg-6 col-sm-6 col-xs-12"><i class="fa fa-angle-right"></i> <b></b>
                                    	<p></p></li>
                            
                                </ul>
                             </div>
                        </div> -->


                        <div class="box box-solid" style="background: transparent; border-top: 0px solid transparent">
                            <div class="box-group" id="accordion">
                                <?php $_from = $this->_tpl_vars['m']['service_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>

                                    <div class="panel box" style="border-top: 0px solid transparent">
                                      <div class="box-header services-header">
                                        <h4 class="box-title">
                                          <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a>
                                        </h4>
                                      </div>
                                      <div id="<?php echo $this->_tpl_vars['k']; ?>
" class="panel-collapse collapse">
                                        <div class="box-body">
                                         <span style="font-style: italic;"><?php echo $this->_tpl_vars['item']['description']; ?>
</span>
                                        </div>
                                      </div>
                                    </div>
                                  <?php endforeach; endif; unset($_from); ?>
                            </div>
                        </div>







                  <!--       <div class="about-title wow fadeInLeft">Reviews</div> -->
                    <!--     <div class="background-white p20 wow fadeInLeft">
                        	
                            <div class="row"> -->
                                <!-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                    <div class="review">
                                        <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/review/01.jpg" class="img-responsive profile" />
                                    </div>
                                </div> -->
                            <!--     <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="review">
                                    	<?php $_from = $this->_tpl_vars['m']['reviews']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        <span class="review-title"><h2><?php echo $this->_tpl_vars['item']['first_name']; ?>
 <?php echo $this->_tpl_vars['item']['last_name']; ?>
</h2></span><br />
	                                        <div class="review-overall-rating"> -->
	                                            <!-- <span class="overall-rating-title">Total Score:</span>
	                                            <i class="fa fa-star"></i>
	                                            <i class="fa fa-star"></i>
	                                            <i class="fa fa-star-half-empty"></i>
	                                            <i class="fa fa-bookmark"></i> -->
	                                 <!--        </div>
	                                         <p><i class="fa fa-star"></i><?php echo $this->_tpl_vars['item']['comment']; ?>
</p>
                                         <?php endforeach; endif; unset($_from); ?>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 
                             <div class="row">
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    <div class="review">
                                        <img src="images/review/02.jpg" class="img-responsive profile" />
                                    </div>
                                </div>
                                <div class="col-lg-9 col-sm-9 col-xs-9">
                                    <div class="review">
                                        <span class="review-title"><h2>ALISON MARTAJ</h2></span><br />
                                        <div class="review-overall-rating">
                                            <span class="overall-rating-title">Total Score:</span>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-empty"></i>
                                        </div>
                                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>
                            </div>
                      
                        </div>   -->


                </div>
                <div class="col-lg-5 col-sm-5 col-xs-12">
                    <div class="about-title wow fadeInRight">About <span><?php echo $this->_tpl_vars['m']['business_name']; ?>
</span></div>
                    <div class="background-white p20 wow fadeInRight">
                            <div class="detail-vcard">
                                <div class="detail-logo">
                                   <?php if ($this->_tpl_vars['m']['logo'] == ''): ?>
                                      <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/merchant-logo-default.png" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                                   <?php else: ?>
                                      <img src="<?php echo $this->_tpl_vars['m']['logo']; ?>
" class="img-responsive" style="width: 100%; border-radius: 10px; border: 1px solid #25313E" />
                                   <?php endif; ?>
                                </div>
                                <div class="detail-contact">
                                    <div class="detail-address"><i class="fa fa-location-arrow"></i><?php echo $this->_tpl_vars['m']['address1']; ?>
 <?php echo $this->_tpl_vars['m']['city']; ?>
 <?php echo $this->_tpl_vars['m']['state']; ?>
 <?php echo $this->_tpl_vars['m']['zip']; ?>
 </div>
                                    <div class="detail-contact-phone"><i class="fa fa-phone"></i><a href="#"><?php echo $this->_tpl_vars['m']['phone1']; ?>
</a></div>
                                    <div class="detail-contact-email"><i class="fa fa-envelope-o"></i><a href="#"><?php echo $this->_tpl_vars['m']['email']; ?>
</a></div>
                                    <!-- <div class="detail-contact-website"><i class="fa fa-globe"></i><a href="#">www.sampelcompany.com</a></div> -->
                                </div>
                            </div>
                          

                            <div class="detail-description">
                                <p> <?php echo $this->_tpl_vars['m']['description']; ?>
 </p>
                            </div>
                           
                             <!-- <div class="detail-follow">
                                <div style="margin-bottom:10px;">
                                    <h5>Check us on</h5>
                                    <div class="follow-wrapper">
                                        <a href="#" class="follow-btn facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="follow-btn youtube"><i class="fa fa-youtube"></i></a>
                                        <a href="#" class="follow-btn twitter"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="follow-btn google-plus"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                               <div style="margin-bottom:10px;">
                                    <h5>We Do Accept</h5>
                                    <div class="follow-wrapper">
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-credit-card"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-paypal"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-discover"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-visa"></i></span>
                                    </div>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <h5>People love us</h5>
                                    <div class="follow-wrapper">
                                        <span class="detail-overview-hearts"><i class="fa fa-heart"></i> 213 people</span>
                                    </div>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <h5>Review on us</h5>
                                    <div class="follow-wrapper">
                                        <span class="detail-overview-rating"><i class="fa fa-star"></i> 4.5k reviews</span>
                                    </div>
                                </div>
                            </div> -->
                     </div>

                     <!-- <div class="about-title wow fadeInRight">Promo on hot</div>
                     <div class="background-white p20 wow fadeInRight"> -->

                             <!-- <div id="carousel-testimonial" class="carousel slide text-center" data-ride="carousel"> -->
                                <!-- Wrapper for slides -->
                                <!-- <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                          <div class="coupon_content">
                                              <div class="title-coupon">Get 20% off on Nail Spa</div>
                                              <p>Lorem ipsum dolor sit amet, eum an aliquando theophrastus definitionem, et eam option offendit. Vix facilis ceteros ne</p>
                                          </div>
                                    </div>
                                    <div class="item">
                                          <div class="coupon_content">
                                              <div class="title-coupon">Get 20% off on Nail Spa</div>
                                              <p>Lorem ipsum dolor sit amet, eum an aliquando theophrastus definitionem, et eam option offendit. Vix facilis ceteros ne</p>
                                          </div>
                                    </div>
                                </div> -->

                                <!-- Controls -->
                                <!-- <div class="btns">
                                    <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="prev">
                                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="next">
                                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div> -->
                            <!-- </div>
                     </div> -->

                     <div class="about-title wow fadeInRight">Business Hours</div>
                     <div class="background-white p20 wow fadeInRight">
                        <div class="working-hours">
                        	<?php $_from = $this->_tpl_vars['m']['business_hours']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	                        	<div class="day clearfix">
	                                <span class="name"><?php echo $this->_tpl_vars['item']['day']; ?>
</span>
	                                
	                                <?php if ($this->_tpl_vars['item']['is_close']): ?>
	                                	<span class="hours">CLOSE</span>
	                                <?php else: ?>
	                                	<span class="hours"><?php echo $this->_tpl_vars['item']['start_time']; ?>
 - <?php echo $this->_tpl_vars['item']['end_time']; ?>
</span>
	                                <?php endif; ?>
	                                
	                            </div>
                        	<?php endforeach; endif; unset($_from); ?>
                        </div>
                     </div>

                </div>
            </div>
        </div>
    </section>


<a href="#0" class="cd-top">Top</a>


<div id="writereviewpage" name="writereviewpage" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <form id="frmReview" name="frmReview">
            	<input type="hidden" name="txtMerchantId" name="txtMerchantId" value="<?php echo $this->_tpl_vars['m']['id']; ?>
">
            <div class="modal-content">
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title modal-title-custom"><label>Write Review</label></h5>
                    <div class="row header-review">
                        <div class="col-md-2 col-sm-2 col-xs-3">
                         <?php if ($this->_tpl_vars['m']['logo'] == ''): ?>
                          	<img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/merchant-logo-default.png" class="img-responsive writereview-profile" />
                         <?php else: ?>
                         	<img src="<?php echo $this->_tpl_vars['m']['logo']; ?>
" class="img-responsive writereview-profile" />
                         <?php endif; ?>
                             
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-9">
                             <div class="modal-review-storetitle"><?php echo $this->_tpl_vars['m']['business_name']; ?>
</div>
                             <p><i class="fa fa-map-marker"></i> <?php echo $this->_tpl_vars['m']['state']; ?>
 <?php echo $this->_tpl_vars['m']['city']; ?>
 <?php echo $this->_tpl_vars['m']['zip']; ?>
 <?php echo $this->_tpl_vars['m']['address1']; ?>
</p>
                        </div>

                    </div>
                  </div>
                  <div class="modal-body">
                        <div class="form-group">
                            <!-- <label>Rate it</label><br /> -->
                            <!-- <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                            </div> -->
                                <span class="rating">
                                        <!-- <input type="radio" class="rating-input"
                                    id="rating-input-1-5" name="rating-input-1"/>
                                        <label for="rating-input-1-5" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-4" name="rating-input-1"/>
                                        <label for="rating-input-1-4" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-3" name="rating-input-1"/>
                                        <label for="rating-input-1-3" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-2" name="rating-input-1"/>
                                        <label for="rating-input-1-2" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-1" name="rating-input-1"/>
                                        <label for="rating-input-1-1" class="rating-star"></label> -->
                                </span>
                        </div>
                         <div class="form-group">
                            <textarea name="txtReviewMessage" id="txtReviewMessage" class="form-control" rows="4" placeholder="Write a review"></textarea>
                        </div>

                        <p><i class="fa fa-globe"></i> Your review will be posted publicly on the web, under <b><?php echo $this->_tpl_vars['m']['business_name']; ?>
</b></p>
                  </div>
                  <div class="modal-footer"> 
                         <div class="col-md-4 col-sm-4 sm-col pull-left" style="display: inline-flex; padding-left:0px;">
                                <button class="tabbtn btn btn-success" id="btnSubmitReview" name="btnSubmitReview"><i class="fa fa-check-circle"></i> Publish</button>
                                <a href="#" class="tabbtn btn btn-dark" id="#cancelreserve" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel</a>
                         </div>
                 </div>
            </div>
            </form>
       </div>
</div>



<div id="servicesmodal" name="servicesmodal" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <!-- <form id="frmCheckIn" name="frmCheckIn"> -->
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>  
                    Check Me In
                    <div class="modal-review-storetitle"><?php echo $this->_tpl_vars['m']['business_name']; ?>
</div>
                  </div>
                  <div class="modal-body">
                     <div class="form-group ">
                       <h4>Select services to avail</h4>
                       <div class="row">
                       <div class="col-lg-12">
                         <ul class="nav nav-tabs" role="tablist">
                <?php $_from = $this->_tpl_vars['m']['service_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                  <?php if ($this->_tpl_vars['key'] == 0): ?>
                  <li role="presentation" class="active"><a href="#cat<?php echo $this->_tpl_vars['key']; ?>
" aria-controls="cat<?php echo $this->_tpl_vars['key']; ?>
" role="tab" data-toggle="tab"><strong><?php echo $this->_tpl_vars['item']['name']; ?>
</strong></a></li>
                  <?php else: ?>
                  <li role="presentation"><a href="#cat<?php echo $this->_tpl_vars['key']; ?>
" aria-controls="cat<?php echo $this->_tpl_vars['key']; ?>
" role="tab" data-toggle="tab"><strong><?php echo $this->_tpl_vars['item']['name']; ?>
</strong></a></li>
                  <?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
              </ul>
              <div class="tab-content">
             <?php $_from = $this->_tpl_vars['m']['service_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item2']):
?>
              <?php if ($this->_tpl_vars['key'] == 0): ?>
              <div role="tabpanel" class="tab-pane active" id="cat<?php echo $this->_tpl_vars['key']; ?>
">
              <?php else: ?>
              <div role="tabpanel" class="tab-pane" id="cat<?php echo $this->_tpl_vars['key']; ?>
">
              <?php endif; ?>
              <!-- <ul class="services-choice"> -->
              <?php $_from = $this->_tpl_vars['m']['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <?php if ($this->_tpl_vars['item']['category_id'] == $this->_tpl_vars['item2']['id']): ?>
               <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left:0px;">
                                   <div class="agreement">
                                    <label style="display:inline-flex;">
                                      <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" />
                                      <div style="padding: 5px;"><?php echo $this->_tpl_vars['item']['name']; ?>
 
                                      <!-- <span>$<?php echo $this->_tpl_vars['item']['price']; ?>
</span> -->
                                      </div></label>
                                   </div>
                             </div>

                <?php endif; ?>
              <?php endforeach; endif; unset($_from); ?>
             <!--  </ul> -->
              </div>
             <?php endforeach; endif; unset($_from); ?>
             </div>
             </div>
                     </div>
                    </div>
                  </div>
                  <div class="modal-footer"> 
                     <div class="col-md-5 col-sm-4 sm-col pull-right" style="padding-left:0px;padding-right: 0px;">
                        <a data-toggle="modal" href="#checkin" class="btn btn-primary" data-dismiss="modal">Previous</a>
                      <button type="button" class="tabbtn btn btn-success" id="btnCheckMeIn" name="btnCheckMeIn"><i class="fa fa-check-circle"></i> Check Me In</button>
                     </div>
                 </div>
        <!-- </form> -->
            </div>
       </div>
</div>




<div id="checkin" name="checkin" class="modal fade">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            	<form id="frmCheckIn" name="frmCheckIn">
            		<input type="hidden" name="txtMerchantId" id="txtMerchantId" value="<?php echo $this->_tpl_vars['m']['id']; ?>
">
            		<input type="hidden" name="txtDate" id="txtDate" value="<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
">
            		<input type="hidden" name="txtTime" id="txtTime" value="<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%k:%M") : smarty_modifier_date_format($_tmp, "%k:%M")); ?>
">
                <input type="hidden" name="txtServices" id="txtServices" value="">
                <input type="hidden" name="txtSpecialist" id="txtSpecialist" value="">
            		
                  <div class="modal-header modalreview-header">
                    <button type="button" class="close custom-modal-close" id="btnCloseAddpoints1" data-dismiss="modal">&times;</button>	
                    Check Me In
                    <div class="modal-review-storetitle"><?php echo $this->_tpl_vars['m']['business_name']; ?>
</div>
                  </div>
                  <div class="modal-body">
                  	<h4>We need you Personal Information</h4>
                         <div class="form-group">
                            <label>* First name</label>
                            <input type="text" value="<?php  isset($_SESSION['customer_information']['first_name']) ? print_r($_SESSION['customer_information']['first_name']) : "";  ?>" name="txtFirstName" id="txtFirstName" class="form-control" placeholder="First Name">
                         </div>
                         
                         <div class="form-group">
                            <label>* Last name</label>
                            <input type="text" value="<?php  isset($_SESSION['customer_information']['last_name']) ? print_r($_SESSION['customer_information']['last_name']) : "";  ?>" name="txtLastName" id="txtLastName" class="form-control" placeholder="Last Name">
                         </div>
                         <div class="form-group">
                            <label>Contact Number</label>
                            <input type="text" value="<?php  isset($_SESSION['customer_information']['mobile_number']) ? print_r($_SESSION['customer_information']['mobile_number']) : "";  ?>" name="txtContactNumber" id="txtContactNumber" class="form-control" placeholder="Contact Number">
                         </div>
                         <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" value="<?php  isset($_SESSION['customer_information']['email_address']) ? print_r($_SESSION['customer_information']['email_address']) : "";  ?>" name="txtEmailAddress" id="txtEmailAddress" class="form-control" placeholder="Email Address">
                         </div>

                  </div>
                  <div class="modal-footer"> 
                         <div class="col-md-4 col-sm-4 sm-col pull-right" style="padding-left:0px;padding-right: 0px;">
	                          <a data-toggle="modal" href="#servicesmodal" class="btn btn-primary" data-dismiss="modal">Next</a>
	                    	  <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>

                         <!--    <button class="tabbtn btn btn-success" id="btnCheckMeIn" name="btnCheckMeIn"><i class="fa fa-check-circle"></i> Check Me In</a> -->
                         </div>
                 </div>
				</form>
            </div>
       </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeTQ090cdN-02VxWGWsl-1gN_A6jk9MBY&callback=initMap">
</script>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/merchant_profile.js"></script>