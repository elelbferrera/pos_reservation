<?php /* Smarty version 2.6.26, created on 2017-05-15 22:49:18
         compiled from privacypolicy_index.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <section id="about" style="padding-bottom: 50px;background-color: #fafafa;background-image: url(<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/terms-bg.png);background-repeat: repeat-x;background-position: center bottom;">
        <div class="container">

            <div class="section-header section-header2" style="width: 70%; margin: 0;">
            	<h3 class="column-title">PRIVACY POLICY</h3>
            	<h5>Web Policies</h5><br />

            	<p>We do not gather personal/corporate e-mail addresses, or any trackable information of our site visitors, without permission. We do not use our site for commercial transactions. We may compile information on what pages you access or visit, with reliance on “cookies”. This information may be used to measure the usage, the number of visits, time spent on the site, the pages viewed, etc. We may use this information to measure our visitor’s usage and enhance the content of our website. It is used for internal review alone and not shared with any other organizations for commercial purposes, without first providing notification to you. Except as provided herein, we shall treat any data or information transmitted to us through this site as non-confidential and non-proprietary.</p><br />

            	

            </div>

            
        </div>
    </section><!--/#about-->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>