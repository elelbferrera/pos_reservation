<?php /* Smarty version 2.6.26, created on 2017-05-19 03:25:12
         compiled from merchant_reservation.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'merchant_reservation.tpl', 164, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/datepicker/datepicker3.css"> 
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/timepicker/bootstrap-timepicker.min.css">
<style>
<?php echo '
  .help-inline-error{ 
    margin-bottom: 10px; 
    color:red;
  }

  .panel{
    background: none!important;
    border:none!important;
    box-shadow: none!important;
    -webkit-box-shadow: none!important;
  }
'; ?>

</style>

<?php  if($_GET){  ?>
  <style>
  <?php echo '
    #merchant-profile{
      margin-top: -100px;
    }
  '; ?>

  </style>
<?php  }  ?> 

<?php  if(!$_GET){  ?>
    <section id="main-page" style="padding-top: 50px; padding-bottom: 10px;">
          <div class="container">
             <div class="section-reservation" id="divReservationTitle" name="divReservationTitle">
                <h2 class="section-title text-center wow fadeInDown">Reservation</h2>
                <p class="text-center wow fadeInDown">Make a reservation for <?php echo $this->_tpl_vars['m']['business_name']; ?>
</p>
            </div>
            
            <div class="section-reservation" id="divSuccessReservation" name="divSuccessRegistration" style="display:none">
                <h2 class="section-title text-center wow fadeInDown">Reservation has been completed to <?php echo $this->_tpl_vars['m']['business_name']; ?>
</h2>
                <p class="text-center wow fadeInDown" style="line-height:17px;">This page will be redirected in a few seconds.<br /> 
                 <i class="fa fa-check-circle-o" style="font-size:68px; margin-top:20px;"></i> <br />
                 <!-- <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/marketplace/in" >Click Here to Create a New Reservation</a> -->
                 <a href="" >Click Here to Create a New Reservation</a>
                 </p>
            </div>
            
        </div>
    </section><!--/#main-slider-->
<?php  }  ?>

    <form id="frmReservation" name="frmReservation">
      <input type="hidden"  name="txtMerchantId" id="txtMerchantId" value="<?php echo $this->_tpl_vars['m']['id']; ?>
"/>
      <input type="hidden"  name="txtServices" id="txtServices"/>
      <input type="hidden"  name="txtSpecialist" id="txtSpecialist"/>
      <input type="hidden"  name="txtWebRoot" id="txtWebRoot" value="<?php  echo WEBROOT  ?>" />

        <section id="merchant-profile" name="merchant-profile">

          <input type="hidden" value ="0" name="txtInc" id="txtInc">
          <div class="container">
            <div class="row detail-content" style="margin-top:20px;">
              <div class="panel panel-default col-lg-7 col-sm-7 col-xs-12">
                <div class="panel-body" style="padding:0 !important">
                      
                    <div id="step1" name="step1" class="frm background-white-border p20 wow fadeInRight">
                      <fieldset>
                        <div class="about-title2 wow fadeInRight"><span>01</span> Step 1</div>

                          <div class="row">
                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                   <div class="form-group">
                                      <label><strong>Appointment Date:</strong> <span class="req">*</span></label>
                                      <!-- <div class="input-group date">
                                          <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                          </div>
                                             <input type="text" class="form-control" name="date" id="date" data-select="datepicker">
                                      </div>  -->
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="txtDate" name="txtDate" readonly="">
                                       </div>
                                  </div>                            
                              </div>
                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                  <div class="bootstrap-timepicker">
                                      <div class="form-group">
                                            <label><strong>Appointment Time:</strong> <span class="req">*</span></label>

                                           <!--  <div class="input-group">
                                                  <input type="text" id="timepicker1" class="form-control" name="timepicker1">
                                                  <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                            </div> -->

                                            <div class="input-group">
                                                  <input type="text" id="txtTime" name="txtTime" class="form-control timepicker">
                                                  <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                  </div>
                                           </div>


                                      </div>
                                  </div>
                              </div>
                          </div>
                        

                        <div class="form-group">
                          <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12 sm-col">
                                <button class="btn btn-primary open1 btn-lg" type="button">Next <span class="fa fa-arrow-right"></span></button> 
                              </div>
                          </div>
                        </div>

                      </fieldset>
                    </div>

                    <div id="step2" name="step2" class="frm background-white-border p20 wow fadeInLeft" style="display: none;">
                      <fieldset>
                        <div class="about-title2 wow fadeInLeft"><span>02</span> Step 2</div>

                          <div class="form-group group">
                              <label style="margin-bottom: 10px;"><strong>Select Specialist:</strong></label>
                              <select class="form-control" name="txtDropSpecialist" id="txtDropSpecialist" style="border-radius: 5px;">
                                  <option value="-1" style="color:#000;">Any Specialist</option>
                                  <?php $_from = $this->_tpl_vars['m']['specialist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                    <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
" style="color:#000;"><?php echo $this->_tpl_vars['item']['first_name']; ?>
  <?php echo $this->_tpl_vars['item']['last_name']; ?>
</option>
                                  <?php endforeach; endif; unset($_from); ?>
                                  
                              </select>
                          </div>

                                                    <div class="form-group">
                            <label style="margin-bottom: 10px;"><strong>Services:</strong></label>
                            <div id="services-cont">
                              <ul class="nav nav-tabs" role="tablist">
                                <?php $_from = $this->_tpl_vars['m']['service_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
                                <?php if ($this->_tpl_vars['k'] == 0): ?>
                                <li role="presentation" class="active"><a href="#cat-<?php echo $this->_tpl_vars['k']; ?>
" aria-controls="cat-<?php echo $this->_tpl_vars['k']; ?>
" role="tab" data-toggle="tab"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></li>
                                <?php else: ?>
                                <li role="presentation"><a href="#cat-<?php echo $this->_tpl_vars['k']; ?>
" aria-controls="cat-<?php echo $this->_tpl_vars['k']; ?>
" role="tab" data-toggle="tab"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></li>
                                <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                              </ul>
                              <div class="tab-content">
                                <?php $_from = $this->_tpl_vars['m']['service_category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item2']):
?>
                                  <?php if ($this->_tpl_vars['k'] == 0): ?>
                                    <div role="tabpanel" class="tab-pane active" id="cat-<?php echo $this->_tpl_vars['k']; ?>
">
                                      <ul class="services-choice" name="ulServices" id="ulServices">
                                        <?php $_from = $this->_tpl_vars['m']['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        <?php if ($this->_tpl_vars['item']['category_id'] == $this->_tpl_vars['item2']['id']): ?>
                                          <li class="col-lg-6 col-sm-6 col-xs-12">
                                             <div class="agreement">
                                               <!-- <span> -->
                                                  
                                               <!-- </span> -->
                                              <label style="display:inline-flex;">
                                                <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" onchange="check_service_change(this)" />
                                                <div style="padding: 5px;"><?php echo $this->_tpl_vars['item']['name']; ?>
 <span>$<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['price'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</span></div></label>
                                             </div>
                                          </li>
                                          <?php endif; ?>
                                   <?php endforeach; endif; unset($_from); ?>
                                      </ul>
                                  <?php else: ?>
                                    <div role="tabpanel" class="tab-pane" id="cat-<?php echo $this->_tpl_vars['k']; ?>
">
                                      <ul class="services-choice" name="ulServices" id="ulServices">
                                        <?php $_from = $this->_tpl_vars['m']['services']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                        <?php if ($this->_tpl_vars['item']['category_id'] == $this->_tpl_vars['item2']['id']): ?>
                                          <li class="col-lg-6 col-sm-6 col-xs-12">
                                             <div class="agreement">
                                               <!-- <span> -->
                                                  
                                               <!-- </span> -->
                                              <label style="display:inline-flex;">
                                                <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" onchange="check_service_change(this)" />
                                                <div style="padding: 5px;"><?php echo $this->_tpl_vars['item']['name']; ?>
 <span>$<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['price'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</span></div></label>
                                             </div>
                                          </li>
                                          <?php endif; ?>
                                   <?php endforeach; endif; unset($_from); ?>
                                      </ul>
                                  <?php endif; ?>

                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                              </div>
                            </div>
                          </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 sm-col">
                                  <button class="btn btn-warning back2 btn-lg" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                                  <button class="btn btn-primary open2 btn-lg" type="button">Next <span class="fa fa-arrow-right"></span></button> 
                                </div>
                            </div>
                        </div>

                      </fieldset>
                    </div>

                    <div id="step4" name="step4" class="frm background-white-border p20 wow fadeInLeft" style="display: none;">
                      <fieldset>
                        <div class="about-title2 wow fadeInLeft"><span>03</span> Step 3</div>

                        <div class="row">
                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                   <div class="form-group">
                                      <label><strong>First name</strong></label>
                                      <input type="text" value="<?php  isset($_SESSION['customer_information']['first_name']) ? print_r($_SESSION['customer_information']['first_name']) : "";  ?>" name="txtFirstName" id="txtFirstName" class="form-control" placeholder="First Name">
                                   </div>
                              </div>
                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                   <div class="form-group">
                                      <label><strong>Last name</strong></label>
                                      <input type="text" value="<?php  isset($_SESSION['customer_information']['last_name']) ? print_r($_SESSION['customer_information']['last_name']) : "";  ?>"  name="txtLastName" id="txtLastName" class="form-control" placeholder="Last Name">
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                  <div class="form-group">
                                      <label><strong>Contact Number</strong></label>
                                      <input type="text"  value="<?php  isset($_SESSION['customer_information']['mobile_number']) ? print_r($_SESSION['customer_information']['mobile_number']) : "";  ?>"   name="txtContactNumber" id="txtContactNumber" class="form-control" placeholder="Contact Number">
                                      <p class="note-reservation">Please provide us your current contact number so we can contact you. Thank you</p>
                                   </div>
                              </div>

                              <div class="col-lg-6 col-sm-12 col-xs-12">
                                  <div class="form-group">
                                      <label><strong>Email Address</strong></label>
                                      <input type="text"  value="<?php  isset($_SESSION['customer_information']['email']) ? print_r($_SESSION['customer_information']['email']) : "";  ?>"   name="txtEmail" id="txtEmail" class="form-control" placeholder="Email Address">
                                      <p class="note-reservation">Please provide us your email so we can contact you. Thank you</p>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12 sm-col pull-left">
                                  <!-- <div class="form-group">
                                      <div class="agreement">
                                          <span>
                                              <input type="checkbox" name="check" id="check" value="ch1" />
                                          </span>
                                          <label>Reserve as Guest</label>
                                          <span>
                                              <input type="checkbox" name="check" id="check" value="ch2" />
                                          </span>
                                          <label>Reserve as Customer</label>
                                      </div>
                                  </div> -->
                              </div>
                         </div>
                         <div class="row">
                              <!-- <div class="col-md-12 col-sm-12 col-xs-12 sm-col pull-left" style="display: inline-flex;">
                                      <button class="tabbtn btn btn-primary" id="btnGoToStep2" name="btnGoToStep2">
                                        <i class="fa fa-arrow-circle-right"></i> proceed to next step
                                      </button>
                               </div> -->
                         </div>

                         <div class="form-group">
                            <label><strong>Reservation Note / Message:</strong></label>
                            <textarea name="txtNotes" id="txtNotes" class="form-control" rows="3" placeholder="Leave note"></textarea>
                            <p class="note-reservation">Note: Please write your requests.</p>
                         </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                              <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12 sm-col">
                                    <button class="btn btn-warning back3 btn-lg" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                                    <button class="tabbtn btn btn-primary btn-lg" id="btnSubmit" name="btnSubmit"><i class="fa fa-check-circle"></i> Reserve it now!</a>
                                    <img src="spinner.gif" alt="" id="loader" style="display: none">
                                  </div>
                              </div>
                        </div>

                      </fieldset>
                    </div>

                    <div id="step3" name="step3" class="frm background-white-border p20 wow fadeInLeft" style="display: none;">
                      <fieldset>
                        <div class="about-title2 wow fadeInLeft"><span>03</span> Log on</div>

                          <div class="row">
                               <div class="col-lg-6 col-sm-6 col-xs-12">
                                  <a href="reserve-register-guest.html">
                                      <div class="guest-btn">
                                          <div class="content">
                                              <h2>Reserve as Guest</h2>
                                              <p>reserve as a guess and proceed to your booking.</p>
                                              <a href="reserve-register-guest.html">Register <i class="fa fa-check-circle"></i></a>
                                          </div>
                                      </div>
                                  </a>
                               </div>
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                  <a href="reserve-register-customer.html">
                                       <div class="signup-btn">
                                          <div class="content">
                                              <h2>Sign up as Customer</h2>
                                              <p>sign up and proceed with your reservation here.</p>
                                              <a href="reserve-register-customer.html">Sign Up <i class="fa fa-check-circle"></i></a>
                                          </div>
                                      </div>
                                  </a>
                               </div>
                        </div>
                        
                        <div class="row spacing-form2">
                               <div class="col-lg-4 col-sm-4 col-xs-12">
                                  <div class="return-btn"></div>
                               </div>

                               <div class="col-lg-8 col-sm-8 col-xs-12">
                                  <div class="return-customer">
                                      <h2>Returning Customer</h2>
                                      <p>Sign your detail to reserve</p>
                                       <div class="form-group">
                                          <label>Username</label>
                                          <input type="text" name="name" class="form-control" placeholder="First Name">
                                       </div>
                                       <div class="form-group">
                                          <label>Password</label>
                                          <input type="password" name="name" class="form-control" placeholder="Email address">
                                       </div>
                                        <a href="#" class="tabbtn btn btn-primary" id="#reserveprocess"><i class="fa fa-lock"></i> sign in</a>
                                  </div>
                              </div>
                        </div>
                        
                        <div class="row spacing-form">
                          <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12 sm-col pull-left" style="display: inline-flex;">
                                    <button class="tabbtn btn btn-dark btn-lg" id="btnBackStep2" name="btnBackStep2" style="margin-right:10px;">
                                    <i class="fa fa-arrow-circle-left btn-lg"></i> back</button>
                              </div>
                          </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 sm-col">
                            <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                          </div>
                        </div>

                      </fieldset>
                    </div>
                  
                </div>
              </div>

              <?php  if(!$_GET){  ?>
              <div class="panel panel-default col-lg-5 col-sm-5 col-xs-12 side-display">
                <div class="panel-body" style="padding:0 !important">
                        
                  <div class="background-white-border p20 wow fadeInRight">
                      <div class="about-title2 wow fadeInRight"><?php echo $this->_tpl_vars['m']['business_name']; ?>
</div>
                        <div class="detail-vcard">
                            <div class="detail-logo">
                           <?php if ($this->_tpl_vars['m']['logo'] == ''): ?>
                              <img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/merchant-logo-default.png" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                           <?php else: ?>
                            <img src="<?php echo $this->_tpl_vars['m']['logo']; ?>
" class="img-responsive" style="border-radius: 10px; border: 1px solid #25313E" />
                           <?php endif; ?>
                            </div>
                            <div class="detail-contact">
                                <div class="detail-address"><i class="fa fa-location-arrow"></i><?php echo $this->_tpl_vars['m']['address1']; ?>
 <br><?php echo $this->_tpl_vars['m']['city']; ?>
, <?php echo $this->_tpl_vars['m']['state']; ?>
 <?php echo $this->_tpl_vars['m']['zip']; ?>
</div>
                                <div class="detail-contact-phone"><i class="fa fa-phone"></i><?php echo $this->_tpl_vars['m']['phone1']; ?>
</div>
                                <div class="detail-contact-email"><i class="fa fa-envelope-o"></i><?php echo $this->_tpl_vars['m']['email']; ?>
</div>
                                <!-- <div class="detail-contact-website"><i class="fa fa-globe"></i><a href="#">www.sampelcompany.com</a></div> -->
                            </div>
                        </div>
                        <div class="detail-follow">
                            <div style="margin-bottom:10px;">
                                    <!-- <h5>We Do Accept</h5>
                                    <div class="follow-wrapper">
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-credit-card"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-paypal"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-discover"></i></span>
                                        <span style="font-size:24px; color:#25313E;"><i class="fa fa-cc-visa"></i></span>
                                    </div> -->
                            </div>
                        </div>
                  </div>
                  
                  <div class="background-white-border p20 wow fadeInRight suggestion" id="divSuggestion" name="divSuggestion" style="display: none;">
                      <div class="about-title-support wow fadeInRight"><i class="fa fa-search"></i> Available time for reservation</div>
                      <ul id="ulListReserve" name="ulListReserve">
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="coupon_content suggestion-li">
                                  <div class="row">
                                      <div class="col-lg-8 col-sm-8 col-xs-12">
                                           <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                           <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                           <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                           <p>Body Treatment<br />Foot Spa </p>
                                      </div>
                                      <div class="col-lg-4 col-sm-4 col-xs-8">
                                           <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                      </div>
                                  </div>
                            </div>
                        </li>
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="coupon_content suggestion-li">
                                <div class="row">
                                    <div class="col-lg-8 col-sm-8 col-xs-12">
                                         <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                         <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                         <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                         <p>Body Treatment<br />Foot Spa </p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-8">
                                         <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                    </div>
                                </div>
                          </div>
                        </li>
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="coupon_content suggestion-li">
                                <div class="row">
                                    <div class="col-lg-8 col-sm-8 col-xs-12">
                                         <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                         <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                         <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                         <p>Body Treatment<br />Foot Spa </p>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-8">
                                         <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                    </div>
                                </div>
                          </div>
                        </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>
                          <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="coupon_content suggestion-li">
                                      <div class="row">
                                          <div class="col-lg-8 col-sm-8 col-xs-12">
                                               <div class="suggestion-title"><strong>Time</strong>: 02: 11pm</div>
                                               <div class="suggestion-title"><strong>Specialist</strong>: Arvin Libradilla</div>
                                               <div class="suggestion-title"><strong>Services:</strong><br /> </div>
                                               <p>Body Treatment<br />Foot Spa </p>
                                          </div>
                                          <div class="col-lg-4 col-sm-4 col-xs-8">
                                               <a href="" class="tabbtn btn btn-primary" id="reserveprocess" style="width:100%"><i class="fa fa-arrow-circle-right"></i> Reserve</a>
                                          </div>
                                      </div>
                              </div>
                          </li>

                      </ul>
                  </div>

                   <?php if ($this->_tpl_vars['m']['caption'] !== ""): ?>
                  <div class="background-white-border p20 wow fadeInRight">
                         <div class="about-title-support wow fadeInRight"><i class="fa fa-support"></i> Need help for reservation?</div>
                         <p><?php echo $this->_tpl_vars['m']['caption']; ?>
</p>
                         <!-- <p><i class="fa fa-phone"></i> 1- 555 - 555 - 555</p> -->
                  </div> 
                   <?php endif; ?>

                </div> 
              </div>
              <?php  }  ?>
            </div>
          </div>
        </section>
    </form>

<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/jquery.validate.js"></script>
<script type="text/javascript">
<?php echo '
    
    var v = jQuery("#frmReservation").validate({
      errorElement: "span",
      errorClass: "help-inline-error",
    });    

    $(".open1").click(function() {
      var date = $(\'#txtDate\').val();
      var time = $(\'#txtTime\').val();

      if(date.trim()=="")
      {
        CustomAlert("Enter your preferred date.", "Unable to reserve");
        return false;
      }

      if(time.trim()=="")
      {
        CustomAlert("Enter your preferred time.", "Unable to reserve");
        return false;
      } 

      if (v.form()) {
        $(".frm").hide("fast");
        $("#step2").show("slow");
      }

    });

    $(".open2").click(function() {
      var checkboxes = document.getElementsByName("chkServices[]");
      var services ="";
      for (var i=0; i<checkboxes.length; i++) {
          if(checkboxes[i].checked)
          {  
          services = services + checkboxes[i].value +",";
        }
      }
        
      if(services.length==0)
      {
        CustomAlert("Please select atleast one service", "Unable to reserve");
        return false;
      }

      var date = $(\'#txtDate\').val();
      var time = $(\'#txtTime\').val();
      
      var new_date = date.split("-");
          new_date = new_date[2] + "-" + new_date[0] + "-" + new_date[1];
          
      services = services.substr(0, services.length-1);

      $(\'#txtServices\').val(services);
      
      var specialist = "";
      if($(\'#txtDropSpecialist\').val()>0)
      {
        specialist = $(\'#txtDropSpecialist\').val();
      }

      $(\'#txtSpecialist\').val(specialist);
           
      var postdata = $("#frmReservation").serialize();
      postdata = postdata + "&txtTime=" + convertTo24Hour(time);
      postdata = postdata + "&txtDateNew=" + new_date;

      $.postJSON("?action=check_if_appointment_exist", postdata, function(data) {

          if (!data.success) {
            CloseLoading();
            CustomAlert(data.message, "");
            if (data.action != \'Closedate\') {
              $(\'#ulListReserve\').data(\'paginate\').kill();
              document.getElementById(\'ulListReserve\').innerHTML = data.suggestions;

              $(\'#ulListReserve\').paginate();
              $(\'#divSuggestion\').show();
            }
          } else {
            $(".frm").hide("fast");
            $("#step4").show("slow");
          }

      });

      return false;
    });
    
    // $(".open3").click(function() {
    //   if (v.form()) {
    //     $("#loader").show();
    //      setTimeout(function(){
    //        $("#frmReservation").html(\'<h2>Thanks for your time.</h2>\');
    //      }, 1000);
    //     return false;
    //   }
    // });
    
    $(".back2").click(function() {
      $(".frm").hide("fast");
      $("#step1").show("slow");
    });

    $(".back3").click(function() {
      $(".frm").hide("fast");
      $("#step2").show("slow");
    });

'; ?>

</script>

  
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/css/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/reservation.js"></script>