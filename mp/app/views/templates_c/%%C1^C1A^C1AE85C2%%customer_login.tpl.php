<?php /* Smarty version 2.6.26, created on 2017-05-17 04:09:52
         compiled from customer_login.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<section id="main-page" style="padding-top: 10px; padding-bottom: 10px;">
          <div class="container">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Returning Customer</h2>
                <p class="text-center wow fadeInDown">By logging in I agree to the <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/termsandconditions">Terms and Conditions</a> and <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/privacypolicy">Privacy Policy</a></p>
					<form id="frmLogin" name="frmLogin">
                          <div class="login-wrap">
                            <div class="login-html">
                                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"  style="display:none;">Sign In</label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab" style="display:none;">Sign Up</label>
                                <div class="login-form">
                                    <div class="sign-in-htm">
                                        <div class="group">
                                            <label for="user" class="label">Username</label>
                                            <input id="txtUsername" name="txtUsername" type="text" class="input">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Password</label>
                                            <input id="txtPassword" name="txtPassword" type="password" class="input" data-type="password">
                                        </div>
                                        <div class="group">
                                            <!-- <input id="check" type="checkbox" class="check" checked> -->
                                            <!-- <label for="check"><span class="icon"></span> Keep me Signed in</label> -->
                                        </div><br />
                                        <div class="group">
                                            <input type="submit" class="button" name="btnLogin" id="btnLogin" value="Sign In">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                            <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/customer/forgotpassword">Forgot Password?</a>
                                        </div>
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
					</form>

            </div>
        </div>
</section><!--/#main-slider-->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/customer.js"></script>