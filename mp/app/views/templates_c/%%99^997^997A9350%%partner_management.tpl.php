<?php /* Smarty version 2.6.26, created on 2016-09-05 02:35:22
         compiled from partner_management.tpl */ ?>
	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Partners
                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/add">
                    	<button class="btn btn-primary" type="button">Add New Partner</button>
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Entity</li>
                        <li class="active">Partner</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Partner List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>MBA</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                                <th>Mobile Phone</th>
                                                <th>State</th>
                                                <th>Country</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $_from = $this->_tpl_vars['partners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                            
	                                            <tr>
	                                            	<td><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/edit/<?php echo $this->_tpl_vars['item']['partner_id']; ?>
"><?php echo $this->_tpl_vars['item']['company_name']; ?>
</a></td>
	                                                <td><?php echo $this->_tpl_vars['item']['dba']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
	                                                <td>+<?php echo $this->_tpl_vars['item']['phone1']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['state']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['country_name']; ?>
</td>
	                                            </tr>
                                            
                                            <?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->