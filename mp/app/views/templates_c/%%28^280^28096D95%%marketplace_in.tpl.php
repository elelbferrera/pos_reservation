<?php /* Smarty version 2.6.26, created on 2017-05-19 04:19:46
         compiled from marketplace_in.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'marketplace_in.tpl', 145, false),)), $this); ?>
﻿<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <section id="main-page" style="padding-top: 30px; padding-bottom: 30px;" class="cd-container">
          <div class="container">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Make a reservation now</h2>
                <!-- <p class="text-center wow fadeInDown">Make a reservation now</p> -->
            </div>
        </div>
    </section><!--/#main-slider-->


    <section id="merchant-listing">
        <div class="container">
            <div class="row detail-content" style="margin-top:20px;">
                <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="refine-search background-white-border p20 wow fadeInRight">
                         <div class="title wow fadeInRight">Refine search</div>
                         <div class="refine-search-part">
                                <div class="form-group">
                                      <label>Services</label>
                                      <select class="form-control searchhome-holder-input" name="repeatReservation">
                                          <option selected="selected" value="">- Services -</option>
                                          <option value="Every Monday">Body Treatment</option>
                                          <option value="Every Tuesday">Foot Spa</option>
                                          <option value="Every Wednesday">Massage</option>
                                          <option value="Every Thursday">Nail Spa</option>
                                      </select>
                                </div>
                         </div>
                         <div class="refine-search-part">
                             <div class="form-group">
                                  <label>Distance</label>
                                  <select class="form-control searchhome-holder-input" name="repeatReservation">
                                      <option selected="selected" value="">- Distance -</option>
                                      <option value="Every Monday">Near Me</option>
                                      <option value="Every Monday">2 blocks</option>
                                      <option value="Every Tuesday">3 Miles</option>
                                      <option value="Every Wednesday">5 Miles</option>
                                  </select>
                            </div>
                         </div>
                        
                         <div class="refine-search-part">
                             <div class="form-group">
                                  <label>Price Range</label>
                                  <input id="range_1" type="text" name="range_1" value="">
                             </div>
                         </div>
                          <div class="refine-search-part">
                             <div class="form-group">
                                  <label>Star Rating</label> <br />
                                   <span class="rating">
                                        <input type="radio" class="rating-input"
                                    id="rating-input-1-5" name="rating-input-1"/>
                                        <label for="rating-input-1-5" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-4" name="rating-input-1"/>
                                        <label for="rating-input-1-4" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-3" name="rating-input-1"/>
                                        <label for="rating-input-1-3" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-2" name="rating-input-1"/>
                                        <label for="rating-input-1-2" class="rating-star"></label>
                                        <input type="radio" class="rating-input"
                                                id="rating-input-1-1" name="rating-input-1"/>
                                        <label for="rating-input-1-1" class="rating-star"></label>
                                </span>
                             </div>
                         </div>
                         <div class="refine-search-part">
                              <div class="form-group">
                                 <label>Payment Option Accepting</label><br />
                                        <div class="agreement">
                                            <span><input type="checkbox" name="check" id="check" value="ch1" /></span>
                                            <label> <span style="color:#25313E; line-height: 23px; display: inline-flex;"> Credit Card <i class="fa fa-credit-card" style="font-size:22px; margin-left: 5px; "></i></span></label>
                                        </div>
                                        <div class="agreement">
                                            <span><input type="checkbox" name="check" id="check" value="ch1" /></span>
                                            <label> <span style="color:#25313E; line-height: 23px; display: inline-flex;"> Paypal <i class="fa fa-paypal" style="font-size:22px; margin-left: 5px; "></i></span></label>
                                        </div>
                                        <div class="agreement">
                                            <span><input type="checkbox" name="check" id="check" value="ch1" /></span>
                                            <label> <span style="color:#25313E; line-height: 23px; display: inline-flex;"> Discover <i class="fa fa-cc-discover" style="font-size:22px; margin-left: 5px; "></i></span></label>
                                        </div>
                                        <div class="agreement">
                                            <span><input type="checkbox" name="check" id="check" value="ch1" /></span>
                                            <label> <span style="color:#25313E; line-height: 23px; display: inline-flex;"> Visa <i class="fa fa-cc-visa" style="font-size:22px; margin-left: 5px; "></i></span></label>
                                        </div>
                              </div>
                         </div>
                         <a href="#" class="tabbtn btn btn-primary" id="reserveprocess"><i class="fa fa-search"></i> find it</a>
                    </div>
                </div>
                 -->

                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <?php if (! empty ( $this->_tpl_vars['search_value'] ) && empty ( $this->_tpl_vars['merchants'] )): ?>
                    <h3>No results found.</h3>
                   <?php endif; ?>
                 </div>
                 
                 <input type="hidden" name="txtURL" id="txtURL" value="<?php echo $this->_tpl_vars['marketplace_url']; ?>
"/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="refine-search background-white-border p20 wow fadeInRight">
                        <div class="title wow fadeInRight">Search</div>
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                 <div class="form-group">
                                    <input type="text" name="txtValue" val id="txtValue" value="<?php echo $this->_tpl_vars['search_value']; ?>
" class="form-control" placeholder="Search">
                                    <p class="note-reservation">Search by City/State/Zip/Business Name</p>
                                 </div>
                             </div>
                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <a href="#" class="tabbtn btn btn-primary btn-lg" id="btnSearch" name="btnSearch">
                                	<i class="fa fa-search"></i> find stores
                                </a>
                             </div>
                        </div>
                  </div>
       


                    <div class="store-list-page">
                    	
	                   	<ul id="example">
	                   		<?php $_from = $this->_tpl_vars['merchants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                             <article class="wow fadeInDown">
	                                <header>
	                                    <h2><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['business_name']; ?>
</a></h2>
	                                     <figure>
                                        <?php if ($this->_tpl_vars['item']['logo'] == ''): ?>
                                          <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><img src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/profile/merchant-logo-default.png" width="128" height="102"></a>
                                        <?php else: ?>
                                          <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
"><img src="<?php echo $this->_tpl_vars['item']['logo']; ?>
" class="img-responsive" width="128" height="102" /></a>
                                        <?php endif; ?>
	                                        <!-- <figcaption>01</figcaption> -->
	                                     </figure>
	                                    <p><?php echo $this->_tpl_vars['item']['state']; ?>
 <?php echo $this->_tpl_vars['item']['city']; ?>
 <?php echo $this->_tpl_vars['item']['zip']; ?>
</p>
	                                    <p style="color:#25313E;"><b>Estimated Waiting Time for Check In:</b> <?php echo $this->_tpl_vars['item']['waiting_time']; ?>
</p>
	                                </header>
	                                <p>
	                                    <?php echo ((is_array($_tmp=$this->_tpl_vars['item']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 200, "...") : smarty_modifier_truncate($_tmp, 200, "...")); ?>

	                                </p>
	                                <footer>
	                                    <!-- <p class="rate-listing">User Rating <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p> -->
	                                    <p class="link-b"><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['item']['id']; ?>
">view details <i class="fa fa-arrow-right"></i></a></p>
	
	                                </footer>
	                            </article>
	                        </li>
	                        <?php endforeach; endif; unset($_from); ?>
	                     
	                    </ul>
                     
                    </div>
                
                
                </div>
            </div>
        </div>
    </section>
    
    <?php if (empty ( $this->_tpl_vars['show_all_store'] )): ?>
    <section id="work-process">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Reservation Process</h2>
                <p class="text-center wow fadeInDown">Your desired salons are now just a few clicks away with our reservation process. Our online reservation processing makes scheduling easy and simple, giving you, hassle free appointments which is much efficient than before.</p>
            </div>

            <div class="row text-center">
                <div class="col-md-4 col-md-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="icon-circle">
                            <span>1</span>
                            <i class="fa fa-search fa-2x"></i>
                        </div>
                        <h3>Search</h3>
                        <p class="text-center wow fadeInDown">Enter the name of the salon using the search engine on the main page. Then, search results will appear. List of salon will be shown on the same page, click on salon names and view. Each salon page has an informative subpages (general info, rates, map, services, guest comments, photo gallery).</p>
                    </div>
                </div>
                 <div class="col-md-4 col-md-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="icon-circle">
                            <span>2</span>
                            <i class="fa fa-user fa-2x"></i>
                        </div>
                        <h3>Request</h3>
                    </div>
                    <p class="text-center wow fadeInDown">Set  your appointment at a salon. Personal information such as your name, contact details, and etc. are required. </p>
                </div>
                 <div class="col-md-4 col-md-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="icon-circle">
                            <span>3</span>
                            <i class="fa fa-calendar fa-2x"></i>
                        </div>
                        <h3>Reserve</h3>
                        <p class="text-center wow fadeInDown">Once we’re done processing your request, we will notify you as soon as possible and mark your request as “reserved” – meaning, the salon guarantee your reservation.</p>
                    </div>
                </div>
              
            </div>
        </div>
    </section><!--/#work-process-->


    <section id="about" style="padding-bottom: 50px;background-color: #FFF;background-image: url(<?php echo $this->_tpl_vars['webroot_resources']; ?>
/images/about-bg.png);background-repeat: repeat-x;background-position: center bottom;">
        <div class="container">

            <div class="section-header section-header2">
                <h2 class="section-title wow fadeInDown">About Us</h2>
                <p class="wow fadeInDown">Go3 Online Reservation is a simple yet efficient online booking for salons which provides:</p>
                  <div class="row">
                      <div class="col-md-6 col-sm-6">
                          <ul class="nostyle">
                              <li><i class="fa fa-check-square"></i> Personalized booking page where you can book their own appointments.</li>
                              <li><i class="fa fa-check-square"></i> Get text and email appointment reminders.</li>
                              <li><i class="fa fa-check-square"></i> Surf the salons anywhere you want. </li>
                          </ul>
                      </div>
                  </div>
                <p class="wow fadeInDown">Book in a faster, simpler and smarter way!</p>

                <br /><br /><br />
                <div class="row">
                    <div class="col-sm-6 wow fadeInRight">
                        <h3 class="column-title">What We do</h3>
                        <p>GO3 Solutions Company is a full service end-to- end web development and mobile applications. With our team experts and professionals in their respective specialties in helping up your business establish and maintain its presence over the web. We understand the value of effective marketing. Our goal is to provide our clients with excellent business solutions to help you widen the extent of your business.</p><br /><br />
                        <p>We are determined to provide the most competitive price mixed with the highest possible quality of work in every engagement with our clients. Expand your business capabilities and start your E-commerce business with us!</p>
                    </div>
                </div>


            </div>

            
        </div>
    </section><!--/#about-->

    <?php endif; ?>

<a href="#0" class="cd-top">Top</a>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/marketplace.js"></script>