{include file="header.tpl"}



















































 <section id="main-page" style="padding-top: 10px; padding-bottom: 10px; min-height:150px;"></section><!--/#main-slider-->

<section id="merchant-profile">




 <div class="container">
 <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary customer_myprofile">
            <div class="box-body box-profile">
              {if $customer_info.photo eq ''}
                <img src="{$webroot_resources}/dist/img/user4-128x128.jpg" class="profile-user-img img-responsive img-circle" />
              {else}
                <img src="{$customer_info.photo}" class="profile-user-img img-responsive img-circle" />
              {/if}

              <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> -->

              <h3 class="profile-username text-center">{$customer_info.first_name} {$customer_info.last_name}</h3>
              <p class="text-muted text-center">{if $customer_info.email eq '' } NA {else} {$customer_info.email} {/if}</p>
              <p class="text-muted text-center">{$mobile}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Reservations</b> <a class="pull-right">{$reservations_count}</a>
                </li>
                <li class="list-group-item">
                  <b>Check In</b> <a class="pull-right">{$checkins_count}</a>
                </li>
                <li class="list-group-item">
                  <b>Favorite</b> <a class="pull-right">{$favorites_count}</a>
                </li>
              </ul>

              <a href="{$webroot}/customer/edit_profile" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> <b>Edit Profile</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="customer_myprofile_dashboard"> 
            <div class="row">
                 <div class="col-md-9">
                      <h3 class="column-title" style="color:#FFF;">Dashboard</h3>
                 </div>
                 <div class="col-md-3">
                      <a href="{$webroot}/marketplace/in" class="btn btn-primary btn-block"><i class="fa fa-calendar"></i> <b>Make Reservation now</b></a>
                 </div>
            </div>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#reservations" data-toggle="tab">Reservations</a></li>
              <li><a href="#checkin" data-toggle="tab">Check Ins</a></li>
              <li><a href="#favorite" data-toggle="tab">Favorite</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="reservations">
                  
                  <ul id="transaction" class="timeline timeline-inverse">
                      {foreach from=$reservations item=item}
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-calendar"></i> {$item.reservation_date}</span>
                              <span class="time"><i class="fa fa-clock-o"></i> {$item.asis_time}</span>
                              <h3 class="timeline-header"><a href="#">{$item.business_name}</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      {if $item.logo eq ''} 
                                        <img class="group list-group-image img-responsive img-circle" src="{$webroot_resources}/images/profile/nerchant-logo-default.png" alt="" />
                                      {else}
                                        <img class="group list-group-image img-responsive img-circle" src="{$item.logo}" alt="" />
                                      {/if}
                                   </div>
                                   <div class="col-md-9">
                                        {$item.description}
                                        <hr>
                                        <b>Date:</b> {$item.reservation_date}<br />
                                        <b>Time:</b> {$item.asis_time} <br />
                                        <b>Service/s Availed:</b> {$item.product}
                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
<!--                                 <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       {/foreach}
                  </ul>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="checkin">
                  <ul id="book" class="timeline timeline-inverse">
                     {foreach from=$check_ins item=item}
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-calendar"></i> {$item.create_date} </span>
                              <span class="time"><i class="fa fa-clock-o"></i>{$item.check_in_time} </span>
                              <h3 class="timeline-header"><a href="#">{$item.business_name}</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      {if $item.logo eq ''} 
                                        <img class="group list-group-image img-responsive img-circle" src="{$webroot_resources}/images/profile/nerchant-logo-default.png" alt="" />
                                      {else}
                                        <img class="group list-group-image img-responsive img-circle" src="{$item.logo}" alt="" />
                                      {/if}
                                   </div>
                                   <div class="col-md-9">
                                        {$item.description}
                                        <hr>
                                        <b>Check In Date:</b> {$item.create_date} <br />
                                        <b><b>Check In Time:</b> {$item.check_in_time} </label><br />
                                        <b<b>Check In Status:</b> {$item.status} </label><br />
                                        <b>Service/s Availed:</b> {$item.product} </label>
                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
 <!--                                <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       {/foreach}
                  </ul>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="favorite">
                  <ul id="book" class="timeline timeline-inverse">
                     {foreach from=$favorites item=item}
                      <li>
                             <div class="timeline-item">
                              <span class="time"><i class="fa fa-heart" style="color: #EB468A;"></i></span>
                              <h3 class="timeline-header"><a href="{$webroot}/merchant/profile/{$item.id}">{$item.business_name}</a></h3>
                              <div class="timeline-body">
                                <div class="row">
                                   <div class="col-md-3">
                                      {if $item.logo eq ''}  
                                        <a href="{$webroot}/merchant/profile/{$item.id}"><img class="group list-group-image img-responsive img-circle" src="{$webroot_resources}/images/profile/nerchant-logo-default.png" alt="" /></a>
                                      {else}
                                        <a href="{$webroot}/merchant/profile/{$item.id}"><img class="group list-group-image img-responsive img-circle" src="{$item.logo}" alt="" /></a>
                                      {/if}
                                   </div>
                                   <div class="col-md-9">
                                        {$item.description|truncate:250} <br /> <br />
                                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>:  {$item.address1} {$item.city} {$item.state} {$item.zip}
                                   </div>
                                </div>
                          
                              </div>
                              <div class="timeline-footer">
                                <a href="{$webroot}/merchant/profile/{$item.id}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View merchant profile</a>
 <!--                                <a class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a> -->
                              </div>
                            </div>
                      </li>
                       {/foreach}
                  </ul>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
   </section>
</div>















{include file="footer.tpl"}
<script src="{$webroot_resources}/js/customer.js"></script>