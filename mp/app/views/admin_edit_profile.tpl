<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    ACL
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/admin/acl"><i class="fa fa-dashboard"></i> Profiles</a></li>
                        <li class="active">ACL</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-header with-border">
                                	<h3 class="box-title">{$profile_info.profile_name} Profile</h3>
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="frmAdminEditProfile" method="post">
                                	<input type="hidden" name="id" id="id" value="{$profile_info.id}" />
                                	<input value="{$profile_info.profile_name}" class="inputbox" id="profile_name" name="profile_name" size="31" type="hidden"/>
                                    <div class="box-body">
                                    	{assign var='ctr' value=0}
                                    	{assign var='first' value='first'}
                                    	{foreach from=$module_list key=k item=mods}
	                                    		<div class="col-md-3">
		                                    	  <div class="form-group">
									                <label>
									                  <input type="checkbox" class="minimal-red" name="module_access[]" value="{$mods.id}" class="minimal-red" {if in_array($mods.id, $profile_info.module_access)} checked {/if}>
									                  {$mods.description}
									                </label>
									              </div>
									            </div>
	                                    
	                                    {/foreach} 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer">
                                        <button id="btnSaveEditProfile" type="submit" class="btn btn-primary">Save Profile</button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>