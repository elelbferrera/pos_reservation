{include file="header.tpl"}
<section id="main-page" style="padding-top: 10px; padding-bottom: 10px;">
          <div class="container">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Reset My Password</h2>
                <!-- <p class="text-center wow fadeInDown">By logging in I agree to the <a href="termsandcondition.html">Terms and Conditions</a> and <a href="privacy.html">Privacy Policy</a></p> -->
					<form id="frmReset" name="frmReset">
                          <div class="login-wrap">
                            <div class="login-html">
                                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"  style="display:none;">Sign In</label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab" style="display:none;">Sign Up</label>
                                <div class="login-form">
                                    <div class="sign-in-htm">
                                        <div class="group">
                                            <label for="user" class="label">Contact Number</label>
                                            <input id="txtContactNumber" name="txtContactNumber" type="text" class="input">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Verification Code</label>
                                            <input id="txtVerificationCode" name="txtVerificationCode" type="password" class="input" data-type="password">
                                        </div>
                                        
                                        <div class="group">
                                            <label for="pass" class="label">New Password</label>
                                            <input id="txtNewPassword" name="txtNewPassword" type="password" class="input" data-type="password">
                                        </div>
                                        
                                        
                                        <div class="group">
                                            <label for="pass" class="label">Re-Enter Password</label>
                                            <input id="txtRepeatPassword" name="txtRepeatPassword" type="password" class="input" data-type="password">
                                        </div>
                                        
                                        <div class="group">
                                            <!-- <input id="check" type="checkbox" class="check" checked> -->
                                            <!-- <label for="check"><span class="icon"></span> Keep me Signed in</label> -->
                                        </div><br />
                                        <div class="group">
                                            <input type="submit" class="button" name="btnResetNow" id="btnResetNow" value="Submit">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                            <!-- <a href="{$webroot}/customer/forgotpassword">Forgot Password?</a> -->
                                        </div>
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
					</form>

            </div>
        </div>
</section><!--/#main-slider-->
{include file="footer.tpl"}
<script src="{$webroot_resources}/js/customer.js"></script>