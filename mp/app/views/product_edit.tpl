<script type="text/javascript" src="{$webroot_resources}/js/product.js"></script>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Product
                    <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/product/management"><i class="fa fa-dashboard"></i> Product List</a></li>
                        <li class="active">Edit Product</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <!-- <div class="col-sm-3">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="summary.php">Summary</a></li>
                                    <li><a href="profile.php">Profile</a></li>
                                    <li><a href="#">Contacts</a></li>
                                    <li><a href="#">Payment Info</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">Invoices</a></li>
                                    <li><a href="#">Attachments</a></li>
                                    <li><a href="#">Notes</a></li>
                                    <li><a href="#">Communication</a></li>
                                    <li><a href="#">Log</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-header with-border">
                                	<!-- <h3 class="box-title">Company Profile</h3> -->
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frmRegisterProduct" id="frmRegisterProduct">
                                	<input type="hidden" name="txtProductID" id="txtProductID" value="{$product.id}"/>
                                	<input type="hidden" name="txtCustomFields" id="txtCustomFields" />
                                    <div class="box-body">
                                    	<div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Product Name:</label>
                                                    <input type="text" class="form-control" id="txtProductName" name="txtProductName" value="{$product.name}" placeholder="Enter product name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">Product Description:</label>
                                                    <input type="text" class="form-control" id="txtProductDescription" name="txtProductDescription" value="{$product.description}" placeholder="Enter product description">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="address1">Buy Rate:</label>
                                                    <input type="text" class="form-control" id="txtBuyRate" name="txtBuyRate" value="{$product.buy_rate}" placeholder="Enter buy rate">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12"><h5>Custom Fields:</h5>
                                             	<button class="btn btn-primary" id="btnCustomFields" name="btnCustomFields">
                                             		Add Custom Field
                                        		</button>
                                             </div>
                                        </div>
                                        
                                        <div class="row"> 
                                    	     <div class="col-md-12">
				                               <div class="box-body">
				                                    <table id="tblCustomFields" name="tblCustomFields" class="table table-bordered table-striped">
				                                        <thead>
				                                            <tr>
				                                                <th>Field Name</th>
				                                                <th>Field Type</th>
				                                                <th>Options</th>
				                                                <th>Actions</th>
				                                            </tr>
				                                        </thead>
									                        <tbody>
					                                            {foreach from=$custom_fields item=item}
						                                            <tr>
						                                            	<td>{$item.field_name}</td>
						                                                <td>{$item.field_type}</td>
						                                                <td>{$item.options}</td>
						                                                <td><input class="btn btn-danger" type="button" onclick="deleteRow(this)" value="Delete"></td>
						                                            </tr>
					                                            
					                                            {/foreach}
					                                        </tbody>
				                                        
				                                    </table>
				                                </div>
                                    	     </div>
                                        </div>
                                        
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer right">
                                        <button type="submit" class="btn btn-primary" id="btnCreateProduct" name="btnCreateProduct">
                                        	Update
                                        </button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->


