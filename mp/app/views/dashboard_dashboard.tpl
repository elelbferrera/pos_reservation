<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>

<div id="content">
<h3>System Statistics</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading"> Statistics </h4>
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            
        </div>                                                                                       
        
        <div class="widget-body">
        
        <div class="row-fluid row-merge border-bottom">
    <div class="span6">
    
    <!-- Inner -->
    <div class="innerAll">
    
        <!-- Row -->
        <div class="row-fluid">
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline"></span>
                    <span class="txt"><span class="count">1,566</span> New</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>   
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2 widget-stats-3">
                    <span class="sparkline success"></span>
                    <span class="txt"><span class="count">12,566</span> Active</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline danger"></span>
                    <span class="txt"><span class="count">536</span> Calling Stopped</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            
            
            
        </div>
        <!-- // Row END -->
    
    </div>
    <!-- // Inner END -->
    
    </div>
          <div class="span6">
    
    <!-- Inner -->
    <div class="innerAll">
    
        <!-- Row -->
        <div class="row-fluid">
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline"></span>
                    <span class="txt"><span class="count">2,566</span> Project Completed</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>   
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2 widget-stats-3">
                    <span class="sparkline success"></span>
                    <span class="txt"><span class="count">12,566</span> Active Universe</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline danger"></span>
                    <span class="txt"><span class="count">536</span> Data Pending</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            
            
        </div>
        <!-- // Row END -->
    
    </div>
    <!-- // Inner END -->
    
    </div>
  
</div>
<div class="separator bottom"></div>

        
        <div class="row-fluid row-merge border-bottom">
    <div class="span6">
    
    <!-- Inner -->
    <div class="innerAll">
    
        <!-- Row -->
        <div class="row-fluid">
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline danger"></span>
                    <span class="txt"><span class="count">1,566</span> Hold for data</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>   
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2 widget-stats-3">
                    <span class="sparkline danger"></span>
                    <span class="txt"><span class="count">0</span>Reject</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline success"></span>
                    <span class="txt"><span class="count">536</span> Billed</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            
            
            
        </div>
        <!-- // Row END -->
    
    </div>
    <!-- // Inner END -->
    
    </div>
          <div class="span6">
    
    <!-- Inner -->
    <div class="innerAll">
    
        <!-- Row -->
        <div class="row-fluid">
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2">
                    <span class="sparkline"></span>
                    <span class="txt"><span class="count">2,566</span> Active Completes</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>   
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2 widget-stats-3">
                    <span class="sparkline success"></span>
                    <span class="txt"><span class="count">1,353</span> Data Ready</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            <div class="span4">
            
                <!-- Stats Widget -->
                <a href="" class="widget-stats widget-stats-2  widget-stats-4">
                    <span class="sparkline danger"></span>
                    <span class="txt"><span class="count">536</span> Project Hold</span>
                    <div class="clearfix"></div>
                </a>
                <!-- // Stats Widget END -->
                
            </div>
            
            
        </div>
        <!-- // Row END -->
    
    </div>
    <!-- // Inner END -->
    
    </div>
  
</div>
<div class="separator bottom"></div>
</div>

<div class="row-fluid row-merge border-bottom">


    
<!---->
        </div>
                                                              

        
        {$_messages_}
    
    
    </div>
</div>
</div>


        
