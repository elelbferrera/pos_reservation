{include file="header.tpl"}

<section id="main-page" style="padding-top: 10px; padding-bottom: 10px; min-height:150px;"></section><!--/#main-slider-->
{if $waiting_lists|@count > 0}
<section id="merchant-profile">
        <div class="container">
            <div class="row profile-top-space">
                <div class="col-lg-12 col-sm-12 col-xs-8 nospace">
                     <div class="profile-name">
                       <div class="col-sm-3 col-xs-3 pull-right">
                           <h2 class="numberinrow">{$waiting_lists|@count}</h2>
                           <p class="rowdesc">Total List In Waiting</p>
                       </div>
                     </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-3 col-xs-12" style="margin-bottom:20px;">
                     <!-- table -->
                              <table id="tbUser" class="merchant-table">
                                    <tr class="header bg-gray b-b b-light">
                                      <th style="width: 20%;" class="waitlist-table-td-th">Customer Name</th>
                                      <th style="width: 30%;" class="waitlist-table-td-th">Check In Time</th>
                                      <th class="waitlist-table-td-th">Waiting Time</th>
                                      <th class="waitlist-table-td-th">Services</th>
                                    </tr>

                                   <tbody>

               {foreach from=$waiting_lists item=item}
                {php} 
                  $check_mobile = $_GET['mobile'];
                  $this->assign('mobile', $check_mobile);
                {/php}
            <div style="display: none" id="{$item.id}wait_id" name="{$item.id}wait_id">{$item.id}</div>
            <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
            <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
            <div style="display: none" id="{$item.id}status" name="{$item.id}status">{$item.status}</div>
            <div style="display: none" id="{$item.id}services" name="{$item.id}services">{$item.services}</div>
            <div style="display: none" id="{$item.id}product_ids" name="{$item.id}product_ids">{$item.product_ids}</div>
                      <tr {if $mobile == $item.contact_number }  style="background-color: yellow;" {/if}>
                        <td class="waitlist-table-td-th">{$item.first_name} {$item.last_name}</td>
                        <td class="waitlist-table-td-th">{$item.normal_time}</td>
                        
                        {if $item.status neq 'WAITING'}
                          <td class="waitlist-table-td-th">00:00</td>
                        {else}
                          <td class="waitlist-table-td-th">{$item.waiting_time}</td>
                        {/if}
                        <td class="waitlist-table-td-th">{$item.services}</td>
                        <!-- <td class="waitlist-table-td-th">{$item.status}</td> -->
                      </tr>
                   {/foreach}
                </tbody>
                                
                              </table>
                              <!-- /. table -->
                </div>
            </div>
                

        </div>
    </section>
{else}

    <section id="waiting-list">
      <div class="container">
        <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-8 nospace">
          <h1>No Customer Waiting.</h1>
        </div>
        </div>
      </div>
    </section>
{/if}

{include file="footer.tpl"}