{include file="header.tpl"}
<style type="text/css">
  {literal}
    .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
  }
    .inputfile + label {
    font-size: 1em;
    font-weight: 400;
    display: inline-block;
    cursor: pointer;
    padding: 10px 15px;
    margin-top: 5px;
  }

  {/literal}
</style>
<section id="main-page" style="padding-top: 10px; padding-bottom: 10px;">
          <div id="divSignup" name="divSignup" class="container">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Your Information</h2>
                
                <!-- <p class="text-center wow fadeInDown" style="line-height:17px;">View past reservation, easy booking, rate merchant, write reviews, get discount code promo and more</a></p> -->
					<form name="frmRegisterMe" id="frmRegisterMe">
                          <div class="login-wrap2">
                            <div class="login-html" style="padding:0px;">
                                <input id="tab-1" type="radio" name="tab" class="sign-in"><label for="tab-1" class="tab" style="display:none;">Sign In</label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up" checked><label for="tab-2" class="tab" style="display:none;">Sign Up</label>
                                  <div class="login-form">
                                     

                                     <div class="sign-up-htm">
                                         <div class="row">
                                            <div class="col-lg-6 col-sm-4 col-xs-6 nospace">
                                              <div class="profile-dp-holder">
                                                {if $customer.photo eq ''}
                                                  <img src="{$webroot_resources}/images/avatar.jpg" class="profile-dp img-responsive" />
                                                {else}
                                                  <img src="{$customer.photo}" class="profile-dp img-responsive" />
                                                {/if}

                                              </div>
                                            </div>
                                            <div class="col-sm-6">
                                               <div class="group">
                                                  <label for="user" class="label2"></label>
                                               </div>
                                            </div>
                                            <div class="col-sm-6">
                                               <div class="group">
                                                  <label for="user" class="label2"></label>
                                                  <input type="file" name="file" id="file" class="inputfile" value="$customer.photo"  /> 
                                                  <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-upload"></i> Change Photo</label>  
                                               </div>
                                            </div>

                                         </div>
                                         <br><br>

                                         <div class="row">
                                              <div class="col-sm-6">
                                                     <div class="group">
                                                        <label for="user" class="label2">First Name *</label>
                                                        <input id="txtFirstName" name="txtFirstName" value="{$customer.first_name}" type="text" class="input" required="required">
                                                     </div>
                                                     <div class="group">
                                                        <label for="user" class="label2">Last Name *</label>
                                                        <input id="txtLastName" name="txtLastName"  value="{$customer.last_name}" type="text" class="input">
                                                     </div>
                                              </div>
                                              <div class="col-sm-6">    
                                                     <div class="group">
                                                        <label for="user" class="label2">Middle Name</label>
                                                        <input id="txtMiddleName" name="txtMiddleName"  value="{$customer.middle_name}" type="text" class="input">
                                                     </div>
                                                     <div class="group">
                                                        <label for="user" class="label2">Contact Number *</label>
                                                        <input id="txtContactNumber" name="txtContactNumber"  type="text" class="input"  value="{$customer.mobile_number}" >
                                                      </div>
                                              </div>
                                              
                                               <div class="col-sm-12">
                                                    <div class="group">
                                                        <label for="user" class="label2">State</label>
                                                        <input id="txtState" name="txtState"  value="{$customer.state}" type="text" class="input">
                                                    </div>
                                                    
                                              </div>
                                               <div class="col-sm-12">
                                                     <div class="group">
                                                        <label for="user" class="label2">Address</label>
                                                        <input id="txtAddress" name="txtAddress" type="text" class="input"  value="{$customer.address}">
                                                    </div>
                                              </div>
                                              
                                               <div class="col-sm-6">
                                                      <div class="group">
                                                         <label for="user" class="label2">ZIP *</label>
                                                         <input id="txtZip" name="txtZip" type="text" class="input"  value="{$customer.zip}">
                                                      </div>
                                                      <div class="form-group group">
                                                          <label for="user">Gender</label>
                                                          <select class="form-control" name="txtGender" id="txtGender" style="background: rgba(255,255,255,.1); color:#FFF;     border: none;border-radius: 5px;">
                                                              <option value="Male" style="color:#000;" {if $customer.gender eq "Male" } selected="selected" {/if} >Male</option>
                                                              <option value="Female" style="color:#000;" {if $customer.gender eq "Female" } selected="selected" {/if}>Female</option>
                                                          </select>
                                                      </div>
                                              </div>
                                              <div class="col-sm-6">
                                                     <div class="group">
                                                        <label for="user" class="label2">City</label>
                                                        <input id="txtCity" name="txtCity" type="text" class="input" value="{$customer.city}">
                                                    </div>
                                                    <div class="form-group group">
                                                        <label>Birthdate</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                               <input type="text" class="form-control input" name="txtBirthDate" id="txtBirthDate" data-select="datepicker" style="border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 5px;border-bottom-right-radius: 5px; padding: 0px;
padding-left: 15px;"  value="{$customer.birth_date}">
                                                        </div> 
                                                     </div>
                                                     
                                              </div>
                                             
                                             <div class="col-sm-12">
                                                    <div class="group">
                                                         <div class="hr"></div>
                                                    </div>
                                                    
                                              </div>
                                            
                                              
                                              <div class="col-sm-12">
                                                    <div class="group">
                                                        <label for="pass" class="label2">Email Address </label>
                                                        <input id="txtEmailAddress" name="txtEmailAddress"  value="{$customer.email}" type="text" class="input">
                                                    </div>
                                                    <div class="group">
                                                            <input type="submit" id="btnUpdate" name="btnUpdate" class="button" value="Update My Profile">
                                                   </div>
                                              
                                                    <!-- <div class="group">
                                                        <label for="pass" class="label2">Username *</label>
                                                        <input id="txtUsername" name="txtUsername" type="text" class="input">
                                                    </div> -->
                                                     
                                              </div>
                                              
                                             
                                              
                                         </div>
                                        <!-- <p>Sign Up using your social media account</p>
                                        <div class="set-2">
                                            <ul>
                                                <li><a href="#" class="twitter-big">Twitter</a></li>
                                                <li><a href="#" class="facebook-big">Facebook</a></li>
                                                <li><a href="#" class="gplus-big">GPlus</a></li>
                                            </ul>
                                        </div> -->
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

					</form>
            </div>
        </div>
        
        
        <div id="divSuccess" name="divSuccess" class="container" style="display: none;">
             <div class="section-reservation">
                <h2 class="section-title text-center wow fadeInDown">Successful Registration</h2>
                <p class="text-center wow fadeInDown" style="line-height:17px;"><br /> 
                 <i class="fa fa-check-circle-o" style="font-size:68px; margin-top:20px;"></i> <br />
                  <a href="{$webroot}/customer/login" >Click Here to Log In</a>
                 </p>
            </div>
        </div>
    </section><!--/#main-slider-->
    

    
    
 {include file="footer.tpl"}
 <script src="{$webroot_resources}/js/customer.js"></script>
 