<script type="text/javascript" src="{$webroot_resources}/js/branch.js"></script>
 <section id="content">
    <section class="vbox">
       <header class="header bg-darkblue b-light">
                                <div class="row">
                                     <div class="col-md-5 col-xs-4">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">List of Branches</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-4 col-xs-3 ta-right">
                                        <!-- <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div> -->
                                    </div>
                                     <div class="col-md-3 col-xs-5 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-i	con" title="New Branch" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add()">Add New Branch</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable">
         
            <!-- table -->
	          <table id="tblBranch" class="waitlist-table">
	             
	                <tr class="header bg-gray b-b b-light">
	                  <th class="waitlist-table-td-th">Date Registered</th>
	                  <th class="waitlist-table-td-th" style="width:20%;">Branch Name</th>
	                  <th class="waitlist-table-td-th" style="width:20%;">Contact Number</th>
	                  <th class="waitlist-table-td-th">Address</th>
	                  <th class="waitlist-table-td-th">City</th>
	                  <th class="waitlist-table-td-th" style="text-align: center;">Action</th>
	                </tr>
					{foreach from=$branches item=item}
					<div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
					<div style="display: none" id="{$item.id}branch_name" name="{$item.id}branch_name">{$item.branch_name}</div>
					<div style="display: none" id="{$item.id}longitude" name="{$item.id}longitude">{$item.longitude}</div>
					<div style="display: none" id="{$item.id}latitude" name="{$item.id}latitude">{$item.latitude}</div>
					<div style="display: none" id="{$item.id}country" name="{$item.id}country">{$item.country}</div>
					<div style="display: none" id="{$item.id}address" name="{$item.id}address">{$item.address}</div>
					<div style="display: none" id="{$item.id}state" name="{$item.id}state">{$item.state}</div>
					<div style="display: none" id="{$item.id}zip" name="{$item.id}zip">{$item.zip}</div>
					<div style="display: none" id="{$item.id}city" name="{$item.id}city">{$item.city}</div>
					<div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
					<div style="display: none" id="{$item.id}store_start_time" name="{$item.id}store_start_time">{$item.store_start_time}</div>
					<div style="display: none" id="{$item.id}store_end_time" name="{$item.id}store_end_time">{$item.store_end_time}</div>
					<div style="display: none" id="{$item.id}store_day" name="{$item.id}store_day">{$item.store_day}</div>
					
	                <tr>
	                  <td class="waitlist-table-td-th">{$item.create_date}</td>
	                  <td class="waitlist-table-td-th">{$item.branch_name}</td>
	                  <td class="waitlist-table-td-th">{$item.contact_number}</td>
	                  <td class="waitlist-table-td-th">{$item.address}</td>
	                  <td class="waitlist-table-td-th">{$item.city}</td>
	                  <td style="text-align: center; font-size:25px;border-bottom: 1px solid #dddddd;"> 
	                  		<a onclick="edit('{$item.id}')" href="#"><i class="fa fa-edit"></i> </a> 
	                  		<!-- <a href="" data-toggle="modal" data-target="#question"><i class="fa fa-trash-o"></i> </a> -->
	                  </td>
	                </tr>
					{/foreach}
	          </table>
	          <!-- /. table -->
            
            
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>



<div id="dlgEditBranch" name="dlgEditBranch" class="modal fade" role="dialog">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Branch Information</h4>
                <!-- <p>Fill up all information neeeded</p> -->
            </div>
            <!-- Modal content-->
    <div class="modal-content">
      <div class="row">
             <form role="form" id="frmBranch" name="frmBranch">
             	<input type="hidden" id="txtBranchId" name="txtBranchId">
             	<input type="hidden" id="txtDays" name="txtDays">
             	
                 <!-- <div class="col-md-3 col-xs-3">
                    <div class="ta-center" style="margin-top:20px;">
                          <img src="{$webroot_resources}/images/specialist-avatar-new.png" class="specialist-avatar" /><br />
                          <input type="file" name="file" id="file" onchange="ImagePreview();" class="inputfile"   /> 
                          <label for="file" id="lblChooseFile" name="lblChooseFile" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Upload Photo</label>  
                          <input type="submit" name="btnChangePicture" id="btnChangePicture" value="Update Picture" class="btn btn-sm btn-primary" style="display: none; margin-left: 10px; margin-top: 10px;" >
                          <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule"><i class="fa fa-calendar"></i> Days of Schedule</button>
                    </div>
                 </div> -->
                 <div class="col-md-12 col-xs-12">
                 	<div class="row">
                 		<div class="col-md-12 col-xs-12">
                 			<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#setschedule"><i class="fa fa-calendar"></i> Days of Schedule</button>
                 		</div>
                 	</div>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                             <div class="form-group">
                                <label>Branch Name</label>
                                <input type="text" id="txtBranchName" name="txtBranchName" class="form-control" placeholder="Branch Name"> 
                              </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                              <div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" id="txtContactNumber" maxlength="10" name="txtContactNumber" class="form-control" placeholder="Contact Number">
                              </div>
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                             <div class="form-group">
                                <label>Longitude</label>
                                <input type="text" id="txtLongitude" name="txtLongitude" class="form-control" placeholder="Longitude"> 
                              </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                             <div class="form-group">
                                <label>Latitude</label>
                                <input type="text" id="txtLatitude" name="txtLatitude" class="form-control" placeholder="Latitude"> 
                              </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                  <label>Address</label>
                                  <textarea rows="2" cols="50" id="txtAddress" name="txtAddress"  class="form-control"></textarea>
                             </div>
                        </div>
                    </div>


                     <div class="row">
                           <div class="col-md-6 col-xs-6">
                                  <div class="form-group">
                                    <label>City</label>
                                    <input type="text" id="txtCity" name="txtCity" class="form-control" placeholder="City">
                                  </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                  <div class="form-group">
                                    <label>Country</label>
                                        <select name="txtCountry" id="txtCountry" class="form-control m-b">
                                        	{foreach from=$countries item=item}
                                        		<option value="{$item.name}">{$item.name}</option>
                                        	{/foreach}
                                        </select>
                                  </div>
                            </div>
                          
                    </div>
                    <div class="row">
                           <div class="col-md-6 col-xs-6">
                                  <div class="form-group">
                                    <label>State</label>
                                    <input type="text" id="txtState" name="txtState" class="form-control" placeholder="State">
                                  </div>
                            </div>
                              <div class="col-md-6 col-xs-6">
                                 <div class="form-group">
                                    <label>Zip</label>
                                        <input type="text" id="txtZip" name="txtZip" class="form-control" placeholder="Zip">
                                  </div>
                            </div>
                    </div>

                    <div class="row">
	                      <div class="col-md-6 col-xs-6">
	                         <div class="bootstrap-timepicker">
	                            <div class="form-group">
	                                  <label>Opening Time:</label>
	                                  <div class="input-group">
	                                    <input type="text" id="txtOpeningTime" name="txtOpeningTime" class="form-control timepicker">
	                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                                  </div>
	                            </div>
	                        </div>
	                      </div>
	
	                      <div class="col-md-6 col-xs-6">
	                          <div class="bootstrap-timepicker">
	                              <div class="form-group">
	                                    <label>Closing Time:</label>
	                                    <div class="input-group">
	                                      <input type="text" id="txtClosingTime" name="txtClosingTime" class="form-control timepicker">
	                                      <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                                    </div>
	                              </div>
	                          </div>
	                      </div>
                    </div>

                 </div>
             </form>
	  </div>
    </div>
    <!-- /. Modal content-->


       <!-- /. Modal content-->
           <div class="modal-footer">
                   <button class="btn btn-primary" id="btnSaveBranch" name="btnSaveBranch">Save Branch</button>
                   <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Cancel</button>
            </div>



  </div>
</div>

<!-- Modal -->
<div id="setschedule" class="modal fade" role="dialog">
  <div class="modal-dialog">



            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Set Schedule</h4>
            </div>




            <!-- Modal content-->
            <div class="modal-content">
                     <div class="row">
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Monday" class="flat-red"> Monday</label>
                          </div>
                      
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Tuesday"  class="flat-red"> Tuesday</label>
                          </div>
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Wednesday"  class="flat-red"> Wednesday</label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Thursday"  class="flat-red"> Thursday</label>
                          </div>
                      
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Friday" class="flat-red"> Friday</label>
                          </div>
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox"  name="days[]" id="days[]" value="Saturday" class="flat-red"> Saturday</label> 
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4 col-sm-4">
                                    <label style="margin-right: 5px; font-size: 14px;"><input type="checkbox" name="days[]" id="days[]" value="Sunday" class="flat-red"> Sunday</label> 
                          </div>
                      </div>


                <div class="row" style="margin-top:20px;">
                     
                </div>

          </div>


            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" name="btnSetSchedule" id="btnSetSchedule">Set Schedule</button>
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</div>