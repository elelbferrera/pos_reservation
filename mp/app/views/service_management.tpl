<script type="text/javascript" src="{$webroot_resources}/js/service.js"></script>
 <section id="content">
    <section class="vbox">
       <header class="header bg-gray b-b b-light">
                                <div class="row">
                                     <div class="col-md-5 col-xs-4">
                                        <div class="breadtitle-holder">
                                            <div class="breadtitle">
                                                <i class="fa fa-list-alt titleFA"></i> 
                                                <p class="headeerpage-title">Product and Services Offer</p>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-md-4 col-xs-3 ta-right">
                                        <!-- <div class="breadtitle-holder2 m-b-none">
                                            <div class="btn-group" style="margin-right:10px;">
                                                 <form role="search">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-search"></i></button> </span> 
                                                       </div>
                                                 </form>
                                            </div>
                                        </div> -->
                                    </div>
                                     <div class="col-md-3 col-xs-5 ta-right">
                                        <div class="breadtitle-holder2 m-b-none">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-sm btn-dark btn-icon" title="New Product and Service" onclick="add_service()"><i class="fa fa-plus"></i></button>
                                                    <div class="btn-group hidden-nav-xs">
                                                        <button type="button" class="btn btn-sm btn-primary"  onclick="add_service()">Add New Service</button>
                                                  </div>
                                             </div>
                                        </div>
                                     </div>
                                </div>
                            </header>
                            
        <section class="scrollable wrapper">
        	<ul id="example">
	
	            	{foreach from=$services item=item}
	                     <li class="col-lg-4 col-md-6 col-xs-12">
	                        <div class="services-holder">
	                            <div class="row">
	                                <div class="col-md-8 col-xs-4">
	                                	<div style="display: none" id="{$item.id}cost" name="{$item.id}cost">{$item.cost|string_format:"%.2f"}</div>
	                                	<div style="display: none" id="{$item.id}product_type" name="{$item.id}product_type">{$item.product_type}</div>
	                                	<div style="display: none" id="{$item.id}minutes" name="{$item.id}minutes">{$item.minutes_of_product}</div>
	                                	
	                                    <div class="title-services" id="{$item.id}product_name" name="{$item.id}product_name">{$item.name}</div>
	                                    <div style="display: none" id="{$item.id}quantity" name="{$item.id}quantity">{$item.quantity}</div>
	                                    <div class="description-services"  id="{$item.id}description" name="{$item.id}description">
	                                    		{$item.description}
	                                   	</div>
	                                </div>
	                                <div class="col-md-4 col-xs-8 ta-right">
	                                    <div class="price-services" id="{$item.id}price">${$item.price|string_format:"%.2f"}</div>
	                                </div>
	                            </div>
	                            <div class="ta-right">
	                                <button type="button" class="btn btn-sm btn-primary" onclick="view_service('{$item.id}')">View</button>
	                                <button type="button" class="btn btn-sm btn-dark" onClick="edit_service('{$item.id}')">Edit</button>
	                            </div>
	                        </div>
	                    </li>
	                {/foreach}

            </ul>
            <div class="clear"></div>
        </section>
                            <!-- pagination -->
                            <!-- /.pagination -->
	</section>
</section>



