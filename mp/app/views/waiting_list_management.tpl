<script type="text/javascript" src="{$webroot_resources}/js/waitlist.js"></script>







<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="waitlist-table">
                <thead>
                <tr>
                  <th style="width: 20%; color: #FFF;" class="waitlist-table-td-th">Customer Name</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Contact No</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Check In Time</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Waiting Time</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Status</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Send Message</th>
                  <th class="waitlist-table-td-th" style="color: #FFF;">Change Status</th>
                </tr>
                </thead>
                <tbody>
               {foreach from=$waiting_lists item=item}
            <div style="display: none" id="{$item.id}wait_id" name="{$item.id}wait_id">{$item.id}</div>
            <div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
            <div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
            <div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
            <div style="display: none" id="{$item.id}status" name="{$item.id}status">{$item.status}</div>
                      <tr>
                        <td class="waitlist-table-td-th">{$item.first_name} {$item.last_name}</td>
                        <td class="waitlist-table-td-th">{$item.contact_number}</td>
                        <td class="waitlist-table-td-th">{$item.normal_time}</td>
                        
                        {if $item.status neq 'WAITING'}
                          <td class="waitlist-table-td-th">00:00</td>
                        {else}
                          <td class="waitlist-table-td-th">{$item.waiting_time}</td>
                        {/if}
                        <td class="waitlist-table-td-th">{$item.status}</td>
                        

                        {if $item.status neq 'WAITING'}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon-hover.png'/>
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon-hover.png'/>
                            </td>
                              
                        {else}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon.png' onmouseover="this.src='{$webroot_resources}/images/message-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/message-icon.png';" onclick="show_send_sms({$item.id})" />
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon.png' onmouseover="this.src='{$webroot_resources}/images/done-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/done-icon.png';" onclick="edit_waiting('{$item.id}')"/>
                            </td>   
                        {/if}

                        
                      </tr>
                   {/foreach}
               
                </tbody>
               
              </table>
               
            </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <header class="header bg-gray b-b b-light" style="background: transparent;">
                  <div class="row">
                       <div class="col-md-2 col-xs-5 ta-right col-lg-offset-10">
                          <div class="breadtitle-holder2 m-b-none" onclick="add_waiting()">
                               <div class="btn-group">
                                   <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                      <div class="btn-group hidden-nav-xs">
                                          <button  class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#newservices">Add Waiting Customer</button>
                                    </div>
                               </div>
                          </div>
                       </div>
                  </div>
               </header>  

 </div>
  </div>
  </section>























 <!-- .content -->
    <!--   <section id="content">
        <section class="vbox">
        <section class="scrollable"> -->
              <!-- table -->
<!--               <table id="tbUser" class="merchant-table">
                 
                    <tr class="header bg-gray b-b b-light"> -->
                      <!-- <th class="waitlist-table-td-th">Service #</th> -->
                     <!--  <th style="width: 20%;" class="waitlist-table-td-th">Customer Name</th>
                      <th class="waitlist-table-td-th">Contact No</th>
                      <th class="waitlist-table-td-th">Check In Time</th>
                      <th class="waitlist-table-td-th">Waiting Time</th>
                      <th class="waitlist-table-td-th">Status</th>
                      <th class="waitlist-table-td-th">Send Message</th>
                      <th class="waitlist-table-td-th">Change Status</th>
                    </tr> -->
				
				<!-- 	{foreach from=$waiting_lists item=item}
						<div style="display: none" id="{$item.id}wait_id" name="{$item.id}wait_id">{$item.id}</div>
						<div style="display: none" id="{$item.id}contact_number" name="{$item.id}contact_number">{$item.contact_number}</div>
						<div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
						<div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
						<div style="display: none" id="{$item.id}status" name="{$item.id}status">{$item.status}</div>
	                    <tr>
	                      <td class="waitlist-table-td-th">{$item.first_name} {$item.last_name}</td>
	                      <td class="waitlist-table-td-th">{$item.contact_number}</td>
	                      <td class="waitlist-table-td-th">{$item.normal_time}</td>
	                      
                        {if $item.status neq 'WAITING'}
                          <td class="waitlist-table-td-th">00:00</td>
                        {else}
                          <td class="waitlist-table-td-th">{$item.waiting_time}</td>
                        {/if}
	                      <td class="waitlist-table-td-th">{$item.status}</td>
	                      

                        {if $item.status neq 'WAITING'}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon-hover.png'/>
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon-hover.png'/>
                            </td>
                              
                        {else}
                            <td class="ping waitlist-table-td-th">
                               <img src='{$webroot_resources}/images/message-icon.png' onmouseover="this.src='{$webroot_resources}/images/message-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/message-icon.png';" onclick="show_send_sms({$item.id})" />
                            </td>
                            <td class="ping waitlist-table-td-th">
                              <img src='{$webroot_resources}/images/done-icon.png' onmouseover="this.src='{$webroot_resources}/images/done-icon-hover.png';" onmouseout="this.src='{$webroot_resources}/images/done-icon.png';" onclick="edit_waiting('{$item.id}')"/>
                            </td>   
                        {/if}

	                      
	                    </tr>
	                 {/foreach}
              </table>
              <header class="header bg-gray b-b b-light" style="background: transparent;">
                  <div class="row">
                       <div class="col-md-2 col-xs-5 ta-right col-lg-offset-10">
                          <div class="breadtitle-holder2 m-b-none" onclick="add_waiting()">
                               <div class="btn-group">
                                   <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
                                      <div class="btn-group hidden-nav-xs">
                                          <button  class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#newservices">Add Waiting Customer</button>
                                    </div>
                               </div>
                          </div>
                       </div>
                  </div>
               </header> -->
              <!-- /. table -->
            <!--   </section>


       </section>
       
	      
     </section> -->
     

<!-- /.content -->

<div id="dlgEditCheckIn" name="dlgEditCheckIn" class="modal fade" role="dialog">
<form id="frmCheckInStatus" name="frmCheckInStatus">	
	<input type="hidden" name="txtCheckInId" id="txtCheckInId">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Check In</h4>
                <p>Enter required information</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" name="txtLastName" id="txtLastName" class="form-control" placeholder="Last Name" readonly="readonly"> 
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                             <input type="text" name="txtFirstName" id="txtFirstName" class="form-control" placeholder="First Name" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label>Contact Number</label>
                             <input type="text" name="txtContactNumber" id="txtContactNumber" class="form-control" placeholder="Contact Number" readonly="readonly">
                        </div>
                        <div class="form-group" id="divStatus" name="divStatus">
                            <label>Status</label>
							<select id="txtStatus" name="txtStatus" class="form-control m-b">
							  	<option value="WAITING">WAITING</option>
							  	<option value="CANCELLED">CANCELLED</option>
							  	<option value="SEATED">SEATED</option>
							</select>
                       </div> 
                     </div>
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" id="btnUpdateCheckIn" name="btnUpdateCheckIn">Save</button>
                <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='serviceslist.php'">Save Service</button> -->
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</form>
</div>

<!-- Modal -->
<div id="dlgMessageCustomer" name="dlgMessageCustomer" class="modal fade" role="dialog">
 <form id="frmSendMessage" name="frmSendMessage">
 	<input type="hidden" name="txtMessageLastName" id="txtMessageLastName">
 	<input type="hidden" name="txtMessageFirstName" id="txtMessageFirstName">
 	<input type="hidden" name="txtMessageContactNumber" id="txtMessageContactNumber">
 	
 	
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Alert SMS</h4>
                <p id="pSendTo" name="pSendTo">Send Message to customer right now</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Message</label>
                             <textarea rows="2" cols="50" id="txtMessage" name="txtMessage"  class="form-control" style="width:100%;">In a few minutes your reservation to 'Beauty Spa' available, please confirm if you still want to avail our services.</textarea>
                        </div>
                     </div>
                </div> 
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Send it now</a> -->
                <button  class="btn btn-md btn-primary" name="btnSendSMS" id="btnSendSMS">Send SMS</button> 
                <button class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
  </form>
</div>

