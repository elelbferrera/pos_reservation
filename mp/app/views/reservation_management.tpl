<script type="text/javascript" src="{$webroot_resources}/js/reservation.js"></script>

{foreach from=$reservations item=item}
  	<div style="display: none" id="{$item.id}id" name="{$item.id}id">{$item.id}</div>
  	<div style="display: none" id="{$item.id}last_name" name="{$item.id}last_name">{$item.last_name}</div>
  	<div style="display: none" id="{$item.id}first_name" name="{$item.id}first_name">{$item.first_name}</div>
  	<div style="display: none" id="{$item.id}mobile" name="{$item.id}mobile">{$item.mobile}</div>
  	<div style="display: none" id="{$item.id}reservation_date" name="{$item.id}reservation_date">{$item.reservation_date}</div>
  	<div style="display: none" id="{$item.id}reservation_time" name="{$item.id}reservation_time">{$item.reservation_time}</div>

	<div style="display: none" id="{$item.id}sale_person_ids" name="{$item.id}sale_person_ids">{$item.sale_person_ids}</div>  
	<div style="display: none" id="{$item.id}product_ids" name="{$item.id}product_ids">{$item.product_ids}</div> 
	<div style="display: none" id="{$item.id}branch_id" name="{$item.id}branch_id">{$item.branch_id}</div>
	<div style="display: none" id="{$item.id}reservation_status_id" name="{$item.id}reservation_status_id">{$item.reservation_status_id}</div>
	<div style="display: none" id="{$item.id}notes" name="{$item.id}notes">{$item.notes}</div> 	
{/foreach}


<form id="frmReservation" name="frmReservation" style="display: none;">
	<input type="text" name="txtReservationId" id="txtReservationId"/>
	<input type="text" name="txtLastName" id="txtLastName"/>
	<input type="text" name="txtFirstName" id="txtFirstName"/>
	<input type="text" name="txtMobile" id="txtMobile"/>
	<input type="text" name="txtNotes" id="txtNotes"/>
	<input type="text" name="txtDate" id="txtDate"/>
	<input type="text" name="txtTime" id="txtTime"/>
	<input type="text" name="txtProducts" id="txtProducts"/>
	<input type="text" name="txtSpecialist" id="txtSpecialist"/>
	<input type="text" name="txtBranch" id="txtBranch"/>
	<input type="text" name="txtStatus" id="txtStatus"/>
</form>


<script>
{literal}
$(function() { // document ready
 var date = new Date();
	var d = date.getDate(),
    m = date.getMonth(),
    y = date.getFullYear();

	$('#calendar').fullCalendar({
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      minTime: "06:00:00",
	  maxTime: "23:00:00",
      defaultView: 'agendaDay',
      defaultDate: new Date(y, m, d),
      editable: true,
      selectable: true,
      eventLimit: true, // allow "more" link when too many events
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'agendaDay,agendaWeek,month'
      },
      views: {
        agendaTwoDay: {
          type: 'agenda',
          duration: { days: 2 },

          // views that are more than a day will NOT do this behavior by default
          // so, we need to explicitly enable it
          groupByResource: true

          //// uncomment this line to group by day FIRST with resources underneath
          //groupByDateAndResource: true
        }
      },
	 viewRender: function(view, element){
	 	var date = new Date(view.start);
	 	var d = date.getDate();
	 	var m = date.getMonth();
	 	var y = date.getFullYear();
	 	
	 	//alert(formatDate(view.start, 'yyyy-MM-dd'));
	 },
      //// uncomment this line to hide the all-day slot
      //allDaySlot: false,
      
	{/literal}
		
      {$resources_info}
      {$events}
    {literal}
    
      
      eventDrop: function(event, delta, revertFunc) {

        //alert(event.title + " was dropped on " + event.start.format() +" "+ event.resourceId);
		var date = event.start.format();
		date = date.split("T");

        if (!confirm("Are you sure to want change this schedule?")) {
            revertFunc();
        }else{
        	editfromDrop(event.id, date[0], date[1], event.resourceId);
        }

      },
	  editable: true,
      droppable: true,
      select: function(start, end, jsEvent, view, resource) {
      	//alert(start);
        // console.log(
          // 'select',
          // start.format(),
          // end.format(),
          // resource ? resource.id : '(no resource)'
        // );
      },
      dayClick: function(date, jsEvent, view, resource) {
      	//alert('lems');
        // console.log(
          // 'dayClick',
          // date.format(),
          // resource ? resource.id : '(no resource)'
        // );
      },
      eventClick: function(event, delta, resource){
      	edit(event.id);
      },
      
    });
 });
{/literal}

</script>

 <!-- .content -->
  <section id="content">
        <section class="vbox">
            <header class="header bg-darkblue b-light">
                <div class="row">
                     <div class="col-md-6 col-xs-3">
                        <div class="breadtitle-holder">
                            <div class="breadtitle">
                                <i class="fa fa-calendar titleFA"></i> 
                                <p class="headeerpage-title">Reservation</p>
                            </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-9 ta-right">
                        <div class="breadtitle-holder2">
                            <!-- <div class="btn-group" style="margin-right:10px;">
                                <button type="button" class="btn btn-sm btn-info" onClick="location.href='employee-schedule.php'">Employee Schedule</button>
                            </div> -->
                             <div class="btn-group" onclick="add()">
                                <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-calendar"></i></button>
                                    <div class="btn-group hidden-nav-xs">
                                        <button type="button" class="btn btn-sm btn-primary">Create Reservation</button>
                                  </div>
                             </div>
                        </div>
                     </div>
                </div>
            </header>
            <section class="scrollable wrapper">
                    <div id="calendar"></div>


            </section>
        </section>
  </section>
  



<!-- Modal -->

<div id="dlgEditReservationStep1" name="dlgEditReservationStep1" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit a Reservation</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
          <div class="row">
               <form class="form-horizontal form-label-left" id="frmSearch" name="frmSearch">
                    <div class="col-md-6 col-sm-6 col-xs-6">    
                         <div class="form-group">
                                <label>Search Customer</label>
                                <div class="input-group">
                                    <input type="text" name="txtMobileSearch" id="txtMobileSearch" class="form-control" placeholder="Mobile Number"> 
                                    <span class="input-group-btn"> 
                                    	<button class="btn btn-primary btn-icon" name="btnSearchCustomer" id="btnSearchCustomer"><i class="fa fa-search"></i></button>
                                    </span> 
                               </div>
                          </div>
                    
                    	  <div class="form-group">
                                <label>Branch</label>
                                    <select name="txtSourceBranch" id="txtSourceBranch" class="form-control m-b">
                                        {foreach from=$branches item=item}
                                        	<option value="{$item.id}">{$item.branch_name}</option>
                                        {/foreach}
                                    </select>
                          </div>
                          <div class="form-group">
                              <label>First Name</label>
                              <input type="text" id="txtSourceFirstName" name="txtSourceFirstName" placeholder="First Name" required="required" class="form-control">
                          </div>
                          <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" id="txtSourceLastName" name="txtSourceLastName" placeholder="Last Name" required="required" class="form-control">
                          </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6">
                    	 <div class="form-group" id="divStatus" name="divStatus">
                                <label>Status</label>
                                    <select name="txtSourceStatus" id="txtSourceStatus" class="form-control m-b">
                                    	<option value="-1">NEW</option>
                                        {foreach from=$status item=item}
                                        	<option value="{$item.id}">{$item.name}</option>
                                        {/foreach}
                                    </select>
                         </div>
                    	 <div class="form-group">
                               <label>Mobile</label>
                               <input type="text" name="txtSourceMobile" id="txtSourceMobile" class="form-control" placeholder="Mobile">
                         </div>
                         <div class="form-group">
                              <label>Set Date: <span class="req">*</span></label>
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right datepicker" id="txtSourceDate" name="txtSourceDate">
                              </div>
                          </div>

                         <div class="bootstrap-timepicker">
                            <div class="form-group">
                                  <label>Set Time:</label>
                                  <div class="input-group">
                                    <input type="text" class="form-control timepicker" id="txtSourceTime" name="txtSourceTime">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                  </div>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="repeatReservation">Repeat:</label>
                            <select class="form-control" name="repeatReservation">
                              <option selected="selected" value="">- No Repeat -</option>
                              <option value="Every Monday">Every Monday</option>
                              <option value="Every Tuesday">Every Tuesday</option>
                              <option value="Every Wednesday">Every Wednesday</option>
                              <option value="Every Thursday">Every Thursday</option>
                              <option value="Every Friday">Every Friday</option>
                              <option value="Every Saturday">Every Saturday</option>
                              <option value="Every Sunday">Every Sunday</option>
                            </select>
                        </div> -->
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                           <label>Reservation Note / Message:</label><br />
                           <textarea name="txtSourceNotes" id="txtSourceNotes" size="75" maxlength="400" style="width:100%; min-height:75px;"> </textarea>
                         </div>
                     </div>

               </form>
          </div>
      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
       		<button type="button" class="btn btn-primary" onclick="GoToSecondPage()">Next</button>
            <!-- <a data-toggle="modal" href="#makereservation2" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
      </div>

      
  </div>
</div>






<!-- Modal -->
<div id="dlgEditReservationStep2" name="dlgEditReservationStep2" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Services</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Select Services</span>
                  </div>
              </div>
              
            
	          <div class="row">
				{foreach from=$services item=item}
				  <div class="col-md-4 col-sm-4">
				            <label style="margin-right: 5px; font-size: 14px;">
				            <input type="checkbox" id="services[]" value="{$item.id}" name="services[]" class="flat-red">{$item.name}</label> 
				            <span style="font-size:14px; color:#3b914d; font-weight:bold;">${$item.price|string_format:"%.2f"}</span>
				  </div>
				{/foreach}
				  
	          </div>
	          
              

      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#makereservation3" class="btn btn-primary" data-dismiss="modal">Next</a> -->
            <button type="button" class="btn btn-primary" onclick="GoToFinalPage()">Next</button>
            <button type="button" class="btn btn-dark" onclick="GoToFirstPage()">Previous</button>
            <!-- <a data-toggle="modal" href="#makereservation1" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
      </div>

      
  </div>
</div>




<!-- Modal -->
<div  id="dlgEditReservationStep3" name="dlgEditReservationStep3" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Specialist</h4>
          <!-- <p>Create Schedule for new Specialist</p> -->
      </div>

      <!-- Modal content-->
      <div class="modal-content">
         
              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <span class="step_descr section">Specialist</span>
                  </div>
              </div>
              <div class="row">
              	{foreach from=$specialist item=item}
              	  <div class="col-md-4 col-sm-4">
                        <label style="margin-right: 5px; font-size: 14px;">
                        	<input type="checkbox" value="{$item.id}" id="specialist[]" name="specialist[]" class="flat-red"> 
                        	{$item.first_name} {$item.last_name}
                        </label> 
                  </div>
               {/foreach}
              
                  
              </div> 
      </div>

    <!-- /. Modal content-->
       <div class="modal-footer">
            <!-- <a data-toggle="modal" href="#updateservices" class="btn btn-primary" data-dismiss="modal">Finish</a>
            <a data-toggle="modal" href="#makereservation2" class="btn btn-dark" data-dismiss="modal">Previous</a> -->
            <button type="button" class="btn btn-primary" id="btnFinish" name="btnFinish">Finish</button>
            <button type="button" class="btn btn-dark"  onclick="GoToPreviousSecondPage()">Previous</button>
      </div>
  </div>
</div>



