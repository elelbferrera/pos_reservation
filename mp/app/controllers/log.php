<?php
class log extends template {
	
    public function in($var) {
        //if (isset($_POST['submit'])) {
        

        if (isset($_POST['username']) && $_POST['username'] !=""  ) {
				// $login_params = array(
                    // 'username' => $_POST['username'],
                    // 'password' => $_POST['password']
                // );
                $login_params = array(
                    'P01' => $_POST['username'],
                    'P02' => $_POST['password']
                );

		
                //LC-09/11/2013
                $response = lib::getWsResponse(API_URL, 'login', $login_params);
				
				// print_r($response);
				// die();
                // $result = $this->backend->login($login_params);
                // $response = $this->backend->get_response();
				

				//$this->layout = "json";
				
				if (!(isset($response['respcode'], $response['respmsg']))) {
					// $this->layout = "json";
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	            } elseif (!($response['respcode'] == '0000')) {
	            	// $this->layout = "json";
	                $this->response['success'] = false;
	                $this->response['message'] = $response['respmsg'];
					
					
					echo ("<script language='javascript'>alert('{$response['respmsg']}')</script>");
					
	            } else {
	                if (isset($response['respmsg']['SessionID'], $response['respcode']) && $response['respcode'] == '0000') {
	                    
	                    $_SESSION['session_id'] = $response['respmsg']['SessionID'];
	                    $_SESSION['permissions'] = array_flip(explode(';', $response['respmsg']['Permissions']));
	                
	                    $_SESSION['user_type'] = $response['respmsg']['user_type'];
	                    $_SESSION['user_type_desc'] = $response['respmsg']['user_type_desc'];
	                    $_SESSION['fname'] = $response['respmsg']['fname'];
	                    $_SESSION['lname'] = $response['respmsg']['lname'];
						
						
						// print_r($_SESSION);
						// die();
				
					
	                    $_SESSION['user_info'] = $response['respmsg']['user_info'];
						
						
						if(isset($_SESSION['user_info']['id']))
						{
	                    	$_SESSION['user_info']['reference_id'] = $_SESSION['user_info']['id'];
						}
	
	                   
	                    $_SESSION['username'] = $_POST['username'];
	                    $_SESSION['last_access'] = time();
	
	                    if (count($var) > 0 && is_array($var) && $redirect = base64_decode($var[0])) {
	                        $path = $redirect;
	                    } else {                        
	                        $permissions=$_SESSION['permissions'];
	                        //$this->meta['home_page']='customers/homepage';
	                        $this->set_home_page();
	                        $meta=$this->meta['home_page'];                    
	                    /*     print_r($permissions);
	                        die();*/
	                        
	                        if(array_key_exists($meta,$permissions))
	                        {
	                            $path = $this->meta['home_page'];
	                        }else{
	                            $path="";    
	                        }
	                        
	                    }   
						
						

						
						// return 
	                    // $this->response = array(
	                        // 'success' => true,
	                        // 'message' => $path,
	                    // );
	                    
	                    return $this->redirect($path);
	                } else {
	                    $this->showError('Invalid username or password.');
	                    // $this->layout = "json";
	                    // $this->response = array(
	                        // 'success' => false,
	                        // 'message' => "Invalid username or password.",
	                    // );
	                }
                }
        }
            
        $this->view->assign('_data_', $_POST);
        // } 
        // elseif (isset($_SESSION['sessionid'], $_SESSION['username'])) {
            // $time = time();
            // if ($time - $_SESSION['last_access'] > $this->meta['max_session_minutes'] * 60) {
                // unset($_SESSION);
                // session_destroy();
                // session_regenerate_id(true);
            // } else {
                // $path = $this->meta['home_page'];
                // return $this->redirect($path);
                // exit;
            // }
        // } else {
            // if (isset($_SESSION['sessionid'])) {
                // unset($_SESSION['sessionid']);
            // }
        // }
    }
  
    public function out($var) {
        if (isset($_SESSION)) {
            if (isset($_SESSION['session_id'])) {
                $resp = lib::getWsResponse(API_URL, 'logout', array('session_id' => $_SESSION['session_id']));
				
                //$params= array('session_id' => $_SESSION['sessionid']);
                //$this->backend->logout($params);
            }
            unset($_SESSION);
            session_destroy();
            session_regenerate_id(true);
        }
        $this->redirect('log/in');
    }
    
    public function auto_generate_payment_file(){
        $download_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'downloads/';
        $time = $this->backend->getSystemSetting("set_code = 'payment_cutoff_time'");
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $cutoff_time = strtotime($row[0]['set_value']);
        $cutoff_time_start = date('H:i:s',$cutoff_time+1);
        $cutoff_time_end = date('H:i:s',$cutoff_time);
        $current_time = date('H:i:s');
        
        if ($current_time <= $cutoff_time_end){
            $params = array (
                'startDate'   => date('Y-m-d'),
            );
            //print_r($params);
            //die();
            $initial_filename="BEC_OMNI_";
            $creationdatetime=date('Y-m-d',strtotime(date('Y-m-d')));
            $realpath = $download_path;
            //$url =$realpath . $creationdatetime . "_PaymentFile.txt";
            $url =$realpath . $initial_filename . $creationdatetime . ".txt";
            
            if(file_exists($url))
            {
                //echo 'file already exists';
                //die();
                return false;
            }

            $customers = $this->backend->getPaymentsForExport($params);
            $response = $this->backend->get_response();
            $search_result = array();
            $ctr=1;
            $content="";                      
                  

            $spaces = str_pad("",64);
            $content="HBEC Payments{$creationdatetime}{$spaces}";
            $fp = fopen($url,"w");
            fwrite($fp,"{$content}\r\n");
            fclose($fp);

            $mangoNetworkId= str_pad(MANGO_NETWORK_ID,15);
            $mangoConfirmationNo= str_pad(MANGO_CONFIRMATION_NUMBER,16);
            $mangoSiteId= str_pad(MANGO_SITE_ID,10);
            //print_r(URL_API_MAIN);
            //die();
            $totalAmount=0;
            $strIDs="";
            foreach ($response['ResponseMessage'] as $key => $val) { 
            $date = new DateTime($val['transaction_date']);
            $transDate = $date->format('YmdHis');
            $sequenceno=str_pad($ctr,7,"0",STR_PAD_LEFT);
            $accountno= str_pad($val['account_no'],18,"0",STR_PAD_LEFT);
            $amount = str_pad($val['total_amount'],10,"0",STR_PAD_LEFT);

            $content="D{$mangoNetworkId}{$transDate}{$sequenceno}{$mangoConfirmationNo}{$accountno}{$amount}{$mangoSiteId}";
            $fp = fopen($url,"a+");
            fwrite($fp,"{$content}\r\n");
            fclose($fp);  
            $ctr++;

            $totalAmount = $totalAmount+$val['total_amount'];
            $strIDs.=$val['id'].",";
            }

            $ctr--;
            $totalRecord = str_pad($ctr,7,"0",STR_PAD_LEFT);
            $totalAmount = str_pad($totalAmount,12,"0",STR_PAD_LEFT);
            $spaces = str_pad("",71);

            $content="T{$totalRecord}{$totalAmount}{$spaces}";
            $fp = fopen($url,"a+");
            fwrite($fp,"{$content}\r\n");
            fclose($fp); 
        } else {
            //echo  $cutoff_time_end;
            //die();
        }
        
         
            
    }    
    
    public function set_home_page() {
        $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
		
		
        if ($usertype=='Admin'){
            
        } else if ($usertype=='Staff') {
            
        } else if ($usertype=='AGENT') {
            $this->meta['home_page']='merchant/add';
        } else if ($usertype=='Super Agent') {
                        
        } else if ($usertype=='Customer') {
            $this->meta['home_page']='customers/management';
        } else if ($usertype=='Super User') {
            $this->meta['home_page']='cash_advances/verified';
            
        } else {
            // Do nothing
        }
    }
}
