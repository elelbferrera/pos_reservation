<?php
class admin extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }

    // PARTNER LISTING
    public function acl()
    {
        $actions = array('delete_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $profile_list = $this->backend->get_all_profile();
        $this->view->assign('profile_list', $profile_list);

        // if($user_info['user_status'] == 'M')
        //     $where = 'pb.programid = '.$user_info['program_id'];

        // $result = $this->backend->get_program_balances($where);
        // $this->view->assign('programbalance', $result);
        // $this->view->assign('user_status', $user_info['user_status']);

        // $rem_buttons = $this->backend->remove_module_buttons($this->meta['class']);
        // $this->view->assign('rem_btn', $rem_buttons);
    }
    
    //NEW MODULE
    public function new_module()
    {
        $actions = array('update_module','add_module','delete_module');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $result = $this->backend->get_all_resources();
        $response = $this->backend->get_response();
        $search_result = $response['ResponseMessage'];
        $this->view->assign('result', $search_result);
    }
    
    protected function add_module(){
        $params = array(
            'resource' => $_POST['adminModuleValue'],
            'description' => $_POST['adminModuleName'],
        );    
        $payment = $this->backend->add_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
     protected function update_module()
    {
         $params = array(
            'id' => $_POST['admin_module_id'],
            'value' => $_POST['admin_module_value'],
        );    
        $this->backend->update_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
    protected function delete_module()
    {
         $params = array(
            'id' => $_GET['id'],
        );    
        $this->backend->delete_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }

    // NEW PROFILE
    public function new_profile()
    {
        $actions = array('add_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $module_list = $this->backend->get_all_modules_list();
        $this->view->assign('module_list', $module_list);
        
    }
    
    public function add_profile()
    {
        $profile_name = trim($_POST['profile_name']);

        $params = array(
            'description'  => $profile_name,
            'module_access' => $_POST['module_access'],
        );
        
        //print_r($params);
        //die();
        
        $result = $this->backend->add_profile($params);
        $response = $this->backend->get_response();
                                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'success' => true,
                'message' => "Successfully added a profile.",
            ); 
        }  
    }

    // EDIT PROFILE
    public function edit_profile($profile_id = null)
    {
        $actions = array('save_edit_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
//        
        if(is_null($profile_id))
            return FALSE;
            
        if(!is_numeric($profile_id[0]))
            return FALSE;    
            
        //print_r($profile_id);
        $module_list = $this->backend->get_all_modules_list();
        $this->view->assign('module_list', $module_list);

        $profile_info = $this->backend->get_all_profile('id = '.$this->db->db_escape($profile_id[0]));
        $profile_info['id'] = $profile_info[0]['id'];
        $profile_info['profile_name'] = $profile_info[0]['description'];
        
        $access = $this->backend->get_all_profile_access($profile_id[0]);
        
        $resource_id="";
        foreach($access as $data) {
            $resource_id .= $data['resource_id'] .",";
        }

       
        $profile_info['module_access'] = explode(',', $resource_id);
        $this->view->assign('profile_info', $profile_info);
    }
    
    protected function save_edit_profile()
    {
        $profile_name = trim($_POST['profile_name']);

        $params = array(
            'description'  => $profile_name,
            'module_access' => $_POST['module_access'],
            'id'            => $_POST['id']
        );
        
        //print_r($params);
        //die();

        $result = $this->backend->edit_profile($params);
        $response = $this->backend->get_response();
        
        
                                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'success' => true,
                'message' => "Successfully updated a profile.",
            ); 
            //die();
        }  
       
    }

    // DELETE PARTNER
   // public function delete_profile()
//    {
//        $this->layout = 'json';

//        $result = $this->backend->delete_profile($_POST['id']);
//        $response = $this->backend->get_response();
//                                                                 
//        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
//            $this->response['message'] = 'System error, unable to connect to database';
//        } elseif (!($response['ResponseCode'] == '0000')) {
//            $this->response['message'] = $response['ResponseMessage'];
//        } else {
//            $this->response = array(
//                'success' => true,
//                'message' => "Successfully deleted a program.",
//            );
//        }
//    }

    protected function delete_profile()
    {
         $params = array(
            'id' => $_GET['id'],
        );   
        $this->backend->delete_profile($params['id']);
        $response = $this->backend->get_response();
         if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'success' => true,
                'message' => "Successfully deleted a profile.",
            );
        }
    }
    
     /*protected function delete_module()
    {
         $params = array(
            'id' => $_GET['id'],
        );    
        $this->backend->delete_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }         */
    
}
