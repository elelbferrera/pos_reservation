<?php
class employers extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function registration() {
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            if (!(isset($_POST['employer_name'], $_POST['employer_address']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['employer_name']) != '' && trim($_POST['employer_address']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
                $reg_params = array(
                    'name' => $_POST['employer_name'],
                    'address' => $_POST['employer_address'],
                    'address_line1' =>  $_POST['address_line1'],
                    'address_line2' =>  $_POST['address_line2'],
                    'city' =>  $_POST['city'],
                    'country' =>  $_POST['country'],
                    'zip' =>  $_POST['zip'],
                    'phone1' =>  $_POST['phone1'],
                    'phone2' =>  $_POST['phone2'],
                    'company_url' =>  $_POST['company_url'],
                    'has_multiple_approver' =>  $_POST['has_multiple_approver'],
                    'status' =>  $_POST['status'],
                 );
                $result = $this->backend->register_employer($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'],
                    );
                    //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Created new employer '. $_POST['employer_name'],'Employer Registration',$datetime,$_SESSION['username'],'');
                }
            }
        }else
        {             
            $options = array(
                0 => 'No',
                1 => 'Yes',
            );
            
            $status = array(
                'A' => 'Active',
                'I' => 'Inactive',
                'P' => 'Pending',
            );
            $this->view->assign('options', $options);
            $this->view->assign('status', $status);
            
        }
    }
    
    protected function get_approvers()
    {        
        $id= $_GET['id'];
        $params = array(
        'employer_id' => $id,
        );
        $sub_agents_array= array();                    
        if($this->backend->get_approvers($params)){
            $response = $this->backend->get_response();
            $response = $response['ResponseMessage']; 
                foreach ($response as $row) 
                {
                    $sub_agents_array[]= array(
                        $row['first_name'],
                        $row['last_name'],
                        $row['employee_no'],
                        $row['phone1'],
                        $row['phone2'],
                        $row['email_address'],
                    );
                }
        }
        $this->response['success'] = true;
        $this->response['message'] = $sub_agents_array;
    }
    
    protected function register_approver()
    {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $params = array(
               'employer_id'      => $_POST['employer_id'], 
               'last_name'     => $_POST['last_name'],
               'first_name'       => $_POST['first_name'],
               'department'       => $_POST['department'],
               'employee_no'       => $_POST['employee_number'],
               'phone1'       => $_POST['phone_1'],
               'phone2'       => $_POST['phone_2'],
               'fax'       => $_POST['fax'],
               'email_address'       => $_POST['email_address'],
               'status'       => $_POST['status'],
               'job_title' => $_POST['job_title'],
        );
        $result = $this->backend->register_approver($params);
        $response = $this->backend->get_response();
                                                      
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';  
            die();
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            die();
        } else {
            $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
            );
			//JR-2014/04/02 logs
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Successfully added approver '.$_POST['first_name'].' '.$_POST['last_name'].' for employer id#'. $_POST['employer_id'],'Employer Management',$datetime,$_SESSION['username'],'');
            print_r(json_encode($this->response,true));
            die();
        }   
    }
    
    
    
 public function management() {
        $actions = array('delete_employer','get_approvers','register_approver');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        } else {
            
            
            $status = array(
                'A' => 'Active',
                'I' => 'Inactive'
            );
            $this->view->assign('status', $status);
            //$employers = $this->backend->get_employers_detailed();
            $this->backend->get_employers_detailed();
            $response = $this->backend->get_response();
            
            $employersList = $response['ResponseMessage'];
            foreach ($employersList as $row) 
            {                                          
                $params = array('employer_id' => $row['id']);
                $approver_array = array();
                if($this->backend->get_approvers($params)){
                    $response = $this->backend->get_response();
                    $response = $response['ResponseMessage']; 
                    
                    
                    foreach ($response as $row_sub) 
                    {
                        $approver_array[]= array(
                            $row_sub['first_name'],
                            $row_sub['last_name'],
                            $row_sub['job_title'],
                            $row_sub['employee_no'],
                            $row_sub['phone1'],
                            $row_sub['phone2'],
                            $row_sub['email_address'],
                        );
                    }
                    //print_r($sub_agents_array);
                }

//                print_r($row);
//                die();
        
                $trimmed = array(
                    'id' => $row['id'],
                    'name'            => $row['name'],
                    'address'     => $row['address'],
                    'city'      => $row['city'],
                    'country'    => $row['country'],
                    'zip'     => $row['zip'],
                    'phone1'     => $row['phone1'],
                    'approvers' => $approver_array,
                );
                $trimmed['employer_details'] = '(' . json_encode($trimmed) . ')';
                $sel_trans[] = $trimmed;
            }
            
            
            
            $this->view->assign('employers', $sel_trans);
        }
    }
    
    protected function delete_employer() {
        if (!(isset($_GET['id']) && trim($_GET['id']) != '')) {
            $this->response['message'] = 'Invalid employer ID';
        } else {
            $employer_id = $_GET['id'];
            $employer_id = substr($employer_id, 2);
            if (!(is_numeric($employer_id) && $employer_id > 0)) {
                $this->response['message'] = 'Invalid employer ID';
            } else {
                $params = array('employer_id' => $employer_id);
                $result = $this->backend->delete_employer($params);
                $response = $this->backend->get_response();
                if (!$result) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
					//JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Deleted employer with id#'. $employer_id,'Employer Management',$datetime,$_SESSION['username'],'');
                }
            }
        }
    }
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
}
