<?php
class setting extends template {
    protected $response;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'uploads/';
        $this->download_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'downloads/';
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
    public function system_config(){
        $actions = array('update_setting');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $result = $this->backend->getSystemSetting();
        $response = $this->backend->get_response();
        $search_result = $response['ResponseMessage'];
        $this->view->assign('result', $search_result);
           
    }
    
    protected function update_setting()
    {
         $params = array(
            'id' => $_POST['settingid'],
            'set_value' => $_POST['settingvalue'],
        );    
        $this->backend->update_setting($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
    public function user_management() {
        $actions = array('update_user', 'reset_password','load_agents');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        } else {
            
            $user_types = array(
                1 => 'Super Admin',
                2 => 'Other Admin',
                3 => 'Partner Admin',
                4 => 'Branch Admin',
                5 => 'Branch User',
            );
            $this->view->assign('user_types', $user_types);
            $this->backend->get_users_detailed();
            
            $response = $this->backend->get_response();
            $users = $response['ResponseMessage'];
            $this->view->assign('users', $users);
            

        }
    }
    
    protected function reset_password()
    {
        if (!(isset($_GET['id']) && trim($_GET['id']) != '')) {
            $this->response['message'] = 'Invalid user ID';
        } else {
            $user_id = $_GET['id'];
            $email = $_GET['email'];
            $username = $_GET['username'];
            
            $params = array('user_id' => $user_id
            ,'username' =>  $username
            ,'email_address' => $email
            );
            
            $result = $this->backend->reset_user_password($params);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
                );
                //JR-2014/04/02 logs
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Reset Password was performed for user '. $username ,'User Management',$datetime,$_SESSION['username'],'');

            }
        }

    }
    
    public function changepassword() {
        if (isset($_POST['current_pw'])) {
            $this->layout = 'json';
            $_POST = array_map('trim', $_POST);
            if (!(isset($_POST['current_pw'], $_POST['new_pw1'], $_POST['new_pw2']))) {
                $this->response['message'] = 'All fields are required';
            } elseif (!($_POST['current_pw'] !='' && $_POST['new_pw1'] !='' && $_POST['new_pw2'] !='')) {
                $this->response['message'] = 'All fields are required';
            } elseif ($_POST['new_pw1'] != $_POST['new_pw2']) {
                $this->response['message'] = 'New password does not match';
            } elseif (strlen($_POST['new_pw1']) < 6) {
                $this->response['message'] = 'Password should be at least 6 characters long';
            } else {
                $params = array(
                    'username' => $_SESSION['username'],
                    'old_pw' => $_POST['current_pw'],
                    'new_pw' => $_POST['new_pw1'],
                );
                $result = $this->backend->change_password($params);
                $response = $this->backend->get_response();
                if (!$result) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                }
            }
        }
    }
    
    public function user_registration() {
        $actions = array('get_agents');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $user_types = array(
                1 => 'Super Admin',
                2 => 'Other Admin',
                3 => 'Partner Admin',
                4 => 'Branch Admin',
                5 => 'Branch User',
        );
        
        $this->view->assign('user_types', $user_types);
        
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $_POST['username'], $_POST['user_type']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email_address']) != '' && trim($_POST['username']) != '' && trim($_POST['user_type']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
                $reg_params = array(
                    'fname' => $_POST['first_name'],
                    'lname' => $_POST['last_name'],
                    'email_address' => $_POST['email_address'],
                    'mobile_number' => $_POST['mobile_number'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    'user_type' => $_POST['user_type'],
                    'created_by' => $_SESSION['username'],
                 );
                $result = $this->backend->register_user($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'],
                    );
                    //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Created new user '. $_POST['first_name']. ' '. $_POST['last_name']. ' with user type id#'. $_POST['user_type'],'User Registration',$datetime,$_SESSION['username'],'');

                }
            }
        }
    }
    
    protected function update_user()
    {
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $_POST['user_type']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email_address']) != '' && trim($_POST['user_type']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
              
              if(!isset($_POST['agent']))
              {$agentid = -1;}
              else
              {$agentid = $_POST['agent'];}
                $reg_params = array(
                    'fname' => $_POST['first_name'],
                    'lname' => $_POST['last_name'],
                    'email_address' => $_POST['email_address'],
                    'mobile_number' => $_POST['mobile_number'],
                    'address' => $_POST['address'],
                    'user_type' => $_POST['user_type'],
                    'user_id' => $_POST['user_id'],
                    'status' => $_POST['userStatus']
                 );
                $result = $this->backend->update_user($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
                   //JR-2014/04/02 logs
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('User '.$_POST['first_name'].' '.$_POST['last_name']. ' was successfully updated' ,'User Management',$datetime,$_SESSION['username'],'');

                }
            }
         }
    }
    
}
