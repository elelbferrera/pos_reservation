<?php
class agents extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function registration()
    {
        $options = array(
            0 => 'No',
            1 => 'Yes',
        );
        
        $status = array(
            'A' => 'Active',
            'I' => 'Inactive',
            'P' => 'Pending',
        );
        
        $agent_types = $this->backend->get_agent_account_types();
        $this->view->assign('agent_types', $agent_types);
        $this->view->assign('options', $options);
        $this->view->assign('status', $status);
        
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';       
            
            $result = $this->backend->register_agent($_POST);
            $response = $this->backend->get_response();
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage']['message'],
                );
			//JR-2014/04/02 logs
                $datetime=$this->backend->get_server_date();
                $this->backend->save_to_action_logs('Successfully added agent '.$_POST['agency_name'],'Agent Registration',$datetime,$_SESSION['username'],'');
            }     
            
        }
    }                       

    public function management()
    {   
        $actions = array('update_agent','get_sub_agents','register_sub_agent','add_agent_fee');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }

        $options = array(
            0 => 'No',
            1 => 'Yes',
        );
        
        $status = array(
            'A' => 'Active',
            'I' => 'Inactive',
            'P' => 'Pending',
        );
        
        $agentorcontact = array(
            'AGENT' => 'AGENT',
            'CONTACT' => 'CONTACT',
        );
        
        
        $agent_types = $this->backend->get_agent_account_types();
        $this->view->assign('agent_types', $agent_types);
        $this->view->assign('options', $options);
        $this->view->assign('status', $status);
        $this->view->assign('agentorcontact', $agentorcontact);
        
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        } else {
            $this->backend->get_agents_list();
            $response = $this->backend->get_response();
            
            $agents = $response['ResponseMessage'];
            
            
            foreach ($agents as $row) 
            {
                //print_R($response);
             
                
                $params = array('agent_id' => $row['agent_id']);
                $sub_agents_array = array();
                if($this->backend->get_sub_agents($params)){
                    $response = $this->backend->get_response();
                    $response = $response['ResponseMessage']; 
                    
                    
                    foreach ($response as $row_sub) 
                    {
                        $sub_agents_array[]= array(
                            $row_sub['first_name'],
                            $row_sub['last_name'],
                            $row_sub['employee_number'],
                            $row_sub['phone1'],
                            $row_sub['phone2'],
                            $row_sub['email_address'],
                            $row_sub['reference'],
                        );
                    }
                    //print_r($sub_agents_array);
                }
                $paramfees = array('agent_id' => $row['agent_id']);
                $agent_fees = array();
                $agent_fees_out = array();
                if($this->backend->get_agent_fees($paramfees)){
                    $response_agent_fees = $this->backend->get_response();
                    $agent_fees = $response_agent_fees['ResponseMessage']; 
                     
                    foreach ($agent_fees as $row_fees) 
                    {     
                        $agent_fees_out[]= array(
                            $row_fees['pay_scale_amount'],
                            $row_fees['create_date'],
                        );
                    }

                }

//                print_r($row_fees);

        
                $trimmed = array(
                    'id' => $row['agent_id'],
                    'agent_name'            => $row['agent_name'],
                    'agent_address'     => $row['agent_address'],
                    'account_type_name'      => $row['account_type_name'],
                    'has_multiple_agent'    => $row['has_multiple_agent'],
                    'address_line1'     => $row['address_line1'],
                    'address_line2'     => $row['address_line2'],
                    'city'            => $row['city'],
                    'country'            => $row['country'],
                    'zip'           => $row['zip'],
                    'phone1' => $row['phone1'],
                    'phone2' => $row['phone2'],
                    'tax_number' => $row['tax_number'],
                    'company_url' => $row['company_url'],
                    'multiple_agent' => $row['multiple_agent'],
                    'agent_account_type_id' =>  $row['agent_account_type_id'],
                    'agent_status' => $row['agent_status'],             
                    'status' => $row['status'],
                    'sub_agents' => $sub_agents_array,
                    'agent_fees' =>$agent_fees_out,
                );
                $trimmed['agent_details'] = '(' . json_encode($trimmed) . ')';
              
                $sel_trans[] = $trimmed;
            }
            
            
            $this->view->assign('agents', $sel_trans);
        }
    }
    
    protected function get_sub_agents()
    {        
        $id= $_GET['id'];
        $params = array(
        'agent_id' => $id,
        );
        $sub_agents_array= array();                    
        if($this->backend->get_sub_agents($params)){
            $response = $this->backend->get_response();
            $response = $response['ResponseMessage']; 
                foreach ($response as $row) 
                {
                    $sub_agents_array[]= array(
                        $row['first_name'],
                        $row['last_name'],
                        $row['employee_number'],
                        $row['phone1'],
                        $row['phone2'],
                        $row['email_address'],
                    );
                }
        }
        $this->response['success'] = true;
        $this->response['message'] = $sub_agents_array;
    }
    
    protected function register_sub_agent()
    {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $params = array(
               'agent_id'      => $_POST['agent_forsubgent_id'], 
               'last_name'     => $_POST['last_name'],
               'first_name'       => $_POST['first_name'],
               'employee_number'       => $_POST['employee_number'],
               'phone1'       => $_POST['phone_1'],
               'phone2'       => $_POST['phone_2'],
               'email_address'       => $_POST['email_address'],
               'reference'       => $_POST['reference'],
        );
        $result = $this->backend->register_sub_agent($params);
        $response = $this->backend->get_response();

        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';  
            die();
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            die();
        } else {
            $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
            );
			//JR-2014/04/02 logs
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Sub-agent '.$_POST['first_name'].' '.$_POST['last_name']. ' was successfully added to Agent id#'. $_POST['agent_forsubgent_id'],'Agent Management',$datetime,$_SESSION['username'],'');
            print_r(json_encode($this->response,true));
            die();
        }   
    }
    
    protected function update_agent() {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $params = array(
               'agent_name'      => $_POST['agency_name'], 
               'agent_address'     => $_POST['agency_address'],
               'address_line1'       => $_POST['address1'],
               'address_line2'       => $_POST['address2'],
               'city'       => $_POST['city'],
               'country'       => $_POST['country'],
               'zip'       => $_POST['zip'],
               'phone1'       => $_POST['phone1'],
               'phone2'       => $_POST['phone2'],
               'tax_number'       => $_POST['tax'],
               'company_url'       => $_POST['company_url'],
               'has_multiple_agent'       => $_POST['hasagents'],
               'agent_account_type_id'       => $_POST['agenttype'],
               'agent_status'       => $_POST['agentstatus'],
               'id' => $_POST['agent_id']
        );
        $result = $this->backend->update_agent($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            
            $this->response['message'] = $response['ResponseMessage'];
            $this->response['success'] = true;
			//JR-2014/04/02 logs
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Agent '.$_POST['agency_name']. ' was successfully updated' ,'Agent Management',$datetime,$_SESSION['username'],'');
        }
    }
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    //PP 04/16/2014 Added for agent fee
protected function add_agent_fee()
    {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $params = array(
               'agent_id'      => $_GET['agent_id'], 
               'pay_scale_amount'       => $_POST['pay_scale_amount'],
        );
        $result = $this->backend->add_agent_fee($params);
        $response = $this->backend->get_response();

        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';  
            die();
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
            die();
        } else {
            $this->response = array(
                    'success' => true,
                    'message' => $response['ResponseMessage'],
            );
            //JR-2014/04/02 logs
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Agent Fee '.$_POST['pay_scale_amount']. ' was successfully added to Agent id#'. $_GET['agent_id'],'Agent Management',$datetime,$_SESSION['username'],'');
            print_r(json_encode($this->response,true));
            die();
        }   
    }
    
    
    
    

}  
?>
