_<?php
class customers extends template {
    protected $response;
    protected $kyc_upload_path;
    protected $salary_upload_path;
    protected $salary_upload_path_final;
    protected $salt;
    //protected $allowed_extensions = array('jpeg','jpg','gif','png','bmp');
    //protected $allowed_types = array('image/jpeg','image/gif','image/png','image/bmp');
    protected $allowed_extensions = array('jpeg','jpg','gif','png','bmp','pdf');
    protected $allowed_types = array('image/jpeg','image/gif','image/png','image/bmp','application/pdf');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->kyc_upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'kyc/temp/';
        $this->salary_upload_path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'salary/temp/';
        $this->salary_upload_path_final = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'salary/unverified/';
        $this->salt = '_kyc';
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    public function get_document_label()
    {          
        $id =  $_POST['id'];
        $result = $this->backend->get_document_label($id);
        $this->response = array(
        'success' => true,
        'message' =>$result);
    }
	
	//JR-2014/03/24
    protected function get_sub_agents_by_agentId(){
         $id = $_GET['id'];
//         /echo $id;
         $sub_agent = $this->backend->get_sub_agents_by_agentId($id,'');
         
         if (count($sub_agent) > 0){
             $option = "";
             foreach($sub_agent as $row){
                $option .= '<option value="' . $row['id'] .  '">' . $row['agent_name']  . '</option> ';
                
             }
             
             
             print_r($option);
             die();
            $this->response = array(
                'success'   => true,
                'data'      => $option,
            );
         }
         else{
             print_r("lems");
             die();
             $this->response = array(
                'success'   => false,
                'data'      => "No document found"
            );
         }
         
    }
    
    public function registration() {
		//JR-2014/03/24
        $actions = array('get_sub_agents_by_agentId');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $countries = $this->backend->get_countries();
        $this->view->assign('countries', $countries);
        $employers = $this->backend->get_employers();
        $this->view->assign('employers', $employers);
        $documents = $this->backend->get_document_types();
        $documents = array('' => '-SELECT-') + $documents;
        $this->view->assign('documents', $documents);
        $titles = array('Mr' => 'Mr', 'Ms' => 'Ms', 'Miss' => 'Miss');
        $this->view->assign('titles', $titles);
        $genders = array('Male' => ' Male', 'Female' => ' Female');
        $this->view->assign('genders', $genders);
        $languages = array('English' => 'English');
        $this->view->assign('languages', $languages);
        $methods_of_contact = array('sms' => ' SMS', 'email' => ' E-Mail');
        $this->view->assign('methods_of_contact', $methods_of_contact);
        $capture_method = array(1 => ' Camera', 2 => ' Scanner', 3 => ' Other');
        $this->view->assign('capture_method', $capture_method);
        $yes_or_no = array(1 => ' Yes', 2 => ' No');
        $this->view->assign('yes_or_no', $yes_or_no);
        
        $loan_terms = $this->backend->get_loan_terms();
        $this->view->assign('loan_terms', $loan_terms);
        
        $loan_request_amounts = $this->backend->get_loan_request_amounts();
        $this->view->assign('loan_request_amounts', $loan_request_amounts);
        
        $param = array('username' => $_SESSION['username']);
        $this->backend->get_user_details($param);
        $user_info = $this->backend->get_response();
        
        $this->view->assign('usertype', $user_info['ResponseMessage']['user_type']);
		
		//JR-2014/03/20 load customer payment cycle in drop down
        $pay_cycle = $this->backend->get_pay_cycle();
        $this->view->assign('pay_cycle', $pay_cycle);
        
        //JR-2014/03/24 getId if agent
        $tmp_agent_id = $this->backend->get_agent_id_in_user($_SESSION['username']);
        $agent_id = $this->backend->get_agent_by_type($tmp_agent_id, 'Agent');
        
        //JR-2014/03/21 load agents
        $agents = $this->backend->get_agents($agent_id);
        $this->view->assign('agents', $agents);
        
        //JR-2014/03/24 sub_agent
        //$sub_agents= $this->backend->get_sub_agents_by_agentId($agent_id,'pair');
        $this->view->assign('sub_agents', -1);
        
        
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            if (!(isset($_POST['first_name'], $_POST['last_name'], $_POST['cpno'], $_POST['street'],$_POST['tin'], $_POST['card_no']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['cpno']) != '' && 
            trim($_POST['street']) != '' && trim($_POST['nsi']) != '')) {
            //trim($_POST['street']) != '' && trim($_POST['nsi']) != '' && trim($_POST['card_no'])!='' )) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';                
            } elseif (!(is_numeric($_POST['cpno']) && strlen($_POST['cpno']) > 5)) {
                $this->response['message'] = 'The mobile number should be numeric.';
            }
//             elseif (isset($_POST['credit_limit']) && trim($_POST['credit_limit']) != '' && !(is_numeric($_POST['credit_limit']) && $_POST['credit_limit']) > 0) {
//                $this->response['message'] = 'The loan amount limit should be numeric.';
//            } 
            elseif(!(isset($_POST['methods'])))
            {
                $this->response['message'] = 'Select one preferred of contact.';
            }elseif(($_POST['email']=='' && in_array('email', $_POST['methods'])))
            {
                //$this->response['message'] = 'Email preferred contact cannot be checked. Must enter email address first.';
                $this->response['message'] = 'If you would like for us to communicate with you via email, please hit the Back button and enter your email address';
            }elseif(($_POST['title']=='Mr' && $_POST['gender'] =="Female"))
            {
                $this->response['message'] = 'Title Mr cannot be associated with Female gender.';
            }
            elseif((($_POST['title']=='Ms' || $_POST['title']=='Miss') && ($_POST['gender'] =="Male") ))
            {
                $this->response['message'] = 'Title Ms/Miss cannot be associated with Male gender.';
            }
            else {
                $reg_params = array(
                    'fname' => $_POST['first_name'],
                    'lname' => $_POST['last_name'],
                    'mname' => $_POST['middle_name'],
                    'title' => $_POST['title'],
                    'gender' => $_POST['gender'],
                    'linked_cardnumber' => $_POST['card_no'],
                    'mobile_number' =>  $_POST['cpno_prefix'].$_POST['cpno'],
                    'email_address' => $_POST['email'],
                    'language' => $_POST['language'],
                    'dob' => $_POST['dob'],
                    'street' => $_POST['street'],
                    'city' => $_POST['city'],
                    'country' => $_POST['country'],
                    'pobox' => $_POST['pobox'],
                    'telephone1' => $_POST['tel2_prefix'].$_POST['tel2'],
                    'telephone2' => $_POST['tel3_prefix'].$_POST['tel3'],
                    'national_insurance_no' => $_POST['nsi'],
                    'employer_id' => $_POST['employer'],
                    'contact_via_email' => (isset($_POST['methods']) && in_array('email', $_POST['methods']))? 1: 0,
                    'contact_via_sms' => (isset($_POST['methods']) && in_array('sms', $_POST['methods']))? 1: 0,
                    'created_by' => $_SESSION['username'],
                    'credit_limit' => $_POST['credit_limit'],
                    
                    //LC-12/12/2013
                    'employer_name' => $_POST['employer_name'],
//                    'first_cidoc' => $_POST['first_cidoc'],
//                    'second_cidoc' => $_POST['second_cidoc'],
//                    'first_cidno' => $_POST['first_cidno'],
//                    'second_cidno' => $_POST['second_cidno']
					//JR-2014/03/20 Added ZIP
					'zip' => $_POST['zip'],
					//JR-2014/03/20 Added Address Line 2
					'address_line_2' => $_POST['address_sec'],
					//JR-2014/03/24
					'agent_id' => $_POST['agent'],
					'sub_agent_id' => $_POST['sub_agent'],
                 );
                //LC-09/11/2013
                //$response = lib::getWsResponse($this->meta['remote_api'], 'merchant_registration', $reg_params);
                $result = $this->backend->register_customer($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    //JR-2014/03/18 Insert to User then get Id to add in Customer
					$users_type_id = $this->backend->get_user_types_ByDesc('Customer');
					//$this->response['message'] = $users_type_id;
					$customers_id = $this->backend->get_customers_id_by_mobile($_POST['cpno_prefix'].$_POST['cpno'] );
					//$this->response['message'] = $customers_id;
                            
                            $reg_params_user = array(
                                'fname' => $_POST['first_name'],
                                'lname' => $_POST['last_name'],
                                'email_address' => $_POST['email'],
                                'mobile_number' =>  $_POST['cpno_prefix'].$_POST['cpno'],
                                'address' => $_POST['street'].' '. $_POST['city'].' '.$_POST['country'],
                                'username' => $_POST['user_name'],
                                'user_type' => $users_type_id,
                                'created_by' => $_SESSION['username'],
                                'customers_id' => $customers_id,
                                'agent_id' => -1
                             );
                             
                            $result = $this->backend->register_user($reg_params_user);
                            
                        $suffix = md5($response['ResponseMessage']['customer_id'] . $this->salt);
                        $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'] . '. You are almost done finalizing your loan request! To complete the process we will ask for you to upload your personal documents.',
                        'cust_id' => $response['ResponseMessage']['customer_id'],
                        'suffix' => $suffix,
                    );
                }
            }
        }
    }
    
    public function management() {
		//JR-2014/03/24 added get_sub_agents_by_agentId
        $actions = array('view_document_image','inital_loan_request', 'make_payment', 'search_merchant','view_merchant','update_ca_limit','apply_loan', 'upload_kyc', 'submit_upload','save_due_date', 'update_card_number', 'get_repayment_amount','upload_salary','get_repayment_amount_cm','upload_salary_manage','submit_upload_salary','get_document_label','get_sub_agents_by_agentId');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $capture_method = array(1 => ' Camera', 2 => ' Scanner', 3 => ' Other');
        $this->view->assign('capture_method', $capture_method);
        
        $yes_or_no = array(1 => ' Yes', 2 => ' No');
        $this->view->assign('yes_or_no', $yes_or_no);
        
        $loan_terms = $this->backend->get_loan_terms();
        $this->view->assign('loan_terms', $loan_terms);
        
        $loan_request_amounts = $this->backend->get_loan_request_amounts();
        $this->view->assign('loan_request_amounts', $loan_request_amounts);
        
        //JR-2014/03/19 load customer payment cycle in drop down
        $pay_cycle = $this->backend->get_pay_cycle();
        $this->view->assign('pay_cycle', $pay_cycle);
        //default: display search form
    }
    
    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    protected function get_repayment_amount(){
        $_POST = $this->array_map_recursive('trim', $_POST);
        
         if (!(isset($_POST['loan_request_amount'],$_POST['term_request']) && is_numeric($_POST['loan_request_amount']) && is_numeric($_POST['term_request']) && $_POST['loan_request_amount'] > 0 && $_POST['term_request'] > 0)) {
            $this->response['message'] = $_POST;    
            $this->response['success'] = false;
         }else{
             $amount = $this->backend->get_repayment_amount($_POST['term_request'],$_POST['loan_request_amount']);
             $this->response['message'] = $amount;
             $this->response['success'] = true;
         }
    }
    
    protected function get_repayment_amount_cm(){
        $_POST = $this->array_map_recursive('trim', $_POST);
        
         if (!(isset($_POST['loan_request_amount_cm'],$_POST['term_request_cm']) && is_numeric($_POST['loan_request_amount_cm']) && is_numeric($_POST['term_request_cm']) && $_POST['loan_request_amount_cm'] > 0 && $_POST['term_request_cm'] > 0)) {
            $this->response['message'] = $_POST;    
            $this->response['success'] = false;
         }else{
             $amount = $this->backend->get_repayment_amount($_POST['term_request_cm'],$_POST['loan_request_amount_cm']);
             $this->response['message'] = $amount;
             $this->response['success'] = true;
         }
    }
    
    
    
    protected function inital_loan_request() {
        $_POST = $this->array_map_recursive('trim', $_POST);
        if (!(isset($_POST['cust_id']) && is_numeric($_POST['cust_id']) && $_POST['cust_id'] > 0) ) {
            $this->response['message'] = 'Invalid customer ID';
        } elseif (!(isset($_POST['loan_request']) && is_numeric($_POST['loan_request']) && $_POST['loan_request'] > 0) ) {
            $this->response['message'] = 'Invalid loan request';
        } elseif (!(isset($_POST['salary_deduction']) && is_numeric($_POST['salary_deduction']) && $_POST['salary_deduction'] > 0) ) {
            $this->response['message'] = 'Please specify if salary deduction or not';
        } else {
            $params = array(
                'customer_id' => $_POST['cust_id'],
                'amount' => $_POST['loan_request'],
                'created_by' => $_SESSION['username'],
                'salary_deduction' => $_POST['salary_deduction'],
                'loan_request_amounts_id' => $_POST['loan_request_amount'],
                'loan_terms_id' => $_POST['term_request'],
                'repayment_amount' => $_POST['repayment_amount'],
                
                'customer_ref'      => $_POST['customer_ref_salary'], 
                'capture_type'     => $_POST['capture_method3'],
                'uploaded_by'       => $_SESSION['username'],
				//JR-2014/03/20 Added is_visa_applicant and pay cycle
                'is_visa_applicant'       => $_POST['visa_applicant2'],
                'customers_pay_cycle_id' => $_POST['pay_cycle'],  
            );
            
        $result = $this->backend->process_salary_upload($params);
        $response = $this->backend->get_response();
            
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $result = $this->backend->new_loan($params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $this->response['message'] = 'Successfully submitted new loan request';
                $this->response['success'] = true;
            }        
        }    
        
        }
    }
    
    
    protected function submit_upload_salary() {
        $_POST = $this->array_map_recursive('trim', $_POST);
        $customer_ref = md5($_POST['merchant_card_id'] . $this->salt);
        $params = array(
               'customer_ref'      => $customer_ref, 
               'capture_type'     => $_POST['capture_method3'],
               'uploaded_by'       => $_SESSION['username'],
               'customer_id'       => $_POST['merchant_card_id'],
        );
        $result = $this->backend->process_salary_upload_manage($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $_GET['id'] = $_POST['merchant_card_id'];
            $this->view_merchant('Successfully processed salary documents');
            //$this->response['message'] = 'Successfully processed salary documents';
            //$this->response['success'] = true;
        }
    }
    
    protected function submit_upload() {
        $_POST = $this->array_map_recursive('trim', $_POST);   
    
        $noDocs = $_GET['docno'];  
        $params = array(
            'customer_ref'      => $_POST['customer_ref'],
            'customer_id'       => $_POST['cust_id'],
            'doc_type1'         => $_POST['doc_type'][0],
            'capture_type1'     => $_POST['capture_method1'],
            'document_id_no1'   => $_POST['customer_id'][0],
//            'doc_type2'         => $_POST['doc_type'][1],
//            'capture_type2'     => $_POST['capture_method2'],
//            'document_id_no2'   => $_POST['customer_id'][1],
            'uploaded_by'       => $_SESSION['username'],
            'doc_no' => $noDocs,
        );
        $result = $this->backend->process_kyc_upload($params);
        $response = $this->backend->get_response();
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response['message'] = 'Successfully processed uploaded documents';
            $this->response['success'] = true;
        }
    }
    
    //upload_salary_manage
    //md5($response['ResponseMessage']['customer_id'] . $this->salt) = $key
    protected function upload_salary_manage() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted:'.$extension;
                        $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file '.$extension;
                    } else {
                        $parts = explode('_', $key);                               
                        $pathPrefix = md5($parts[3].$this->salt)."_".$parts[2];
                        
                        $destination = $this->salary_upload_path . $pathPrefix. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        
                        $destinationFinal = $this->salary_upload_path_final . $pathPrefix. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->salary_upload_path . $pathPrefix)) @mkdir ($this->salary_upload_path. $pathPrefix);
                        if (!is_dir($this->salary_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } 
                        elseif(file_exists($destinationFinal))
                        {
                            $this->response['message'] = 'Salary has been uploaded and cannot be overwritten.';
                        } 
                        elseif (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }


    
    protected function upload_salary() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                    } else {
                        $parts = explode('_', $key);       
                        
                        $destination = $this->salary_upload_path . $parts[3]."_".$parts[2]. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->salary_upload_path . $parts[3]."_".$parts[2])) @mkdir ($this->salary_upload_path. $parts[3]."_".$parts[2]);
                        
                        if (!is_dir($this->salary_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        }    
                        elseif (!move_uploaded_file($upload['tmp_name'], $destination)) 
                        {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    
    
    
    protected function upload_kyc() {
        $this->layout = false;
        $max_upload_size = 2097152; //2MB
        //var_dump($max_upload_size);
        if (isset($_FILES) && is_array($_FILES)) {
            //var_dump($_FILES);
            foreach ($_FILES as $key => $upload) {
                if (!($upload['error'] == 0)) {
                    $this->response['message'] = 'Error in uploaded document';
                } elseif (!($upload['size'] <= $max_upload_size)) {
                    $this->response['message'] = 'Document exceeds 2 MB';
                } elseif (!(in_array($upload['type'], $this->allowed_types))) {
                    //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                } else {
                    $file_parts = explode('.', $upload['name']);
                    $extension = strtolower(end($file_parts));
                    if (!(in_array($extension, $this->allowed_extensions))) {
                        //$this->response['message'] = 'Invalid document, only images are accepted';
                    $this->response['message'] = 'That file type is not valid for this application process. Please use an image file or a .pdf file';
                    } else {
                        $parts = explode('_', $key);
                        
                        $destination = $this->kyc_upload_path . $parts[3]. DIRECTORY_SEPARATOR . $parts[1] . '_' . $parts[2] . '.' . $extension;
                        if (!is_dir($this->kyc_upload_path . $parts[3])) @mkdir ($this->kyc_upload_path. $parts[3]);
                        if (!is_dir($this->kyc_upload_path)) {
                            $this->response['message'] = 'Unable to create upload folder';
                        } elseif (!move_uploaded_file($upload['tmp_name'], $destination)) {
                            $this->response['message'] = 'Error in uploading document';
                        } else {
                            $this->response['message'] = '';
                            $this->response['success'] = true;
                        }
                    }
                }
                if ($this->response['message'] != '') {
                    
                    echo $this->response['message'];
                }
            }
        }
    }
    
    protected function save_due_date() {
        if (!(isset($_POST['duedate']) && is_numeric($_POST['duedate']) && $_POST['duedate'] >= 0)) {
            $this->response['message'] = 'Invalid due date.';
        } else {
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'due_date' => $_POST['duedate'],
            );
            $result = $this->backend->save_due_date($params);
            $response = $this->backend->get_response();
                                                                 
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully set due date.');
            }
        }
    }
    
    protected function update_ca_limit() {
        if (!(isset($_POST['new_ca_limit']) && is_numeric($_POST['new_ca_limit']) && $_POST['new_ca_limit'] >= 0)) {
            $this->response['message'] = 'Invalid loan amount limit.';
        } else {            
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'credit_limit' => $_POST['new_ca_limit'],
                'created_by' => $_SESSION['username']
            );
            $result = $this->backend->set_credit_limit($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully updated loan amount limit.');
            }
        }
    }
    
    protected function update_card_number() {
        if (!(isset($_POST['adjust_card_no']) && is_numeric($_POST['adjust_card_no']) && $_POST['adjust_card_no'] >= 0)) {
            $this->response['message'] = 'Invalid Card Number.';
        } else{
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'card_number' => $_POST['adjust_card_no'] 
            );
            
            $result = $this->backend->update_card_number($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully updated card number.');
            }
        }
    }
    
    
    protected function apply_loan() {
        if (!(isset($_POST['loan_amount']) && is_numeric($_POST['loan_amount']) && $_POST['loan_amount'] >= 0)) {
            $this->response['message'] = 'Invalid loan amount.';
        } else {
//           $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['merchant_card_id'],
//                'P03' => $_POST['new_ca_limit'],
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'adjust_cash_advance_limit', $api_params);
            
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'amount' => $_POST['loan_amount'],
                'created_by' => $_SESSION['username'],
                'salary_deduction' => $_POST['salary_deduction'],
                
                'loan_request_amounts_id' => $_POST['loan_request_amount_cm'],
                'loan_terms_id' => $_POST['term_request_cm'],
                'repayment_amount' => $_POST['repayment_amount'],
				//JR-2014/03/19 Added is_visa_applicant
                'is_visa_applicant'       => $_POST['visa_applicant'],
                //JR-2014/03/19 Added customer payment cycle
                'customers_pay_cycle_id' =>  $_POST['pay_cycle'],
            );
            $result = $this->backend->new_loan($params);
            $response = $this->backend->get_response();
            
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //$_GET['id'] = 'x' . $_POST['merchant_card_id'];
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Your loan application has been saved.');
            }
        }
    }
    
    protected function make_payment() {
        if (!(isset($_POST['payment_amount']) && is_numeric($_POST['payment_amount']) && $_POST['payment_amount'] >= 0)) {
            $this->response['message'] = 'Invalid payment amount.';
        } elseif (!(isset($_POST['date_paid']) && count(explode('/', $_POST['date_paid'])) == 3)) {
            $this->response['message'] = 'Invalid payment date.';
        } elseif (!(isset($_POST['payment_ref']) && trim($_POST['payment_ref']) != '')) {
            $this->response['message'] = 'Invalid payment reference.';
        } else {
            $date_paid = date('Y-m-d', strtotime($_POST['date_paid']));
//           $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['merchant_card_id'],
//                'P03' => $_POST['payment_amount'],
//                'P04' => $_POST['payment_ref'],
//                'P05' => $_POST['payment_method'],
//                'P06' => $date_paid,
//            );
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'make_payment', $api_params);
            $params = array(
                'customer_id' => $_POST['merchant_card_id'],
                'amount' => $_POST['payment_amount'],
                'created_by' => $_SESSION['username'],
                'method' =>  $_POST['payment_method'],
                'payment_reference' => $_POST['payment_ref']
            );
            $result = $this->backend->pay_loan($params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                //$_GET['id'] = 'x' . $_POST['merchant_card_id'];
                $_GET['id'] = $_POST['merchant_card_id'];
                $this->view_merchant('Successfully processed payment request to merchant.');
            }
        }
    }
    
    protected function search_merchant() {
        if (!isset($_POST['mobile_number'], $_POST['first_name'], $_POST['last_name'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['mobile_number']) !='' || trim($_POST['first_name']) != '' || trim($_POST['last_name'])!= '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {
//            $search_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => $_POST['first_name'],
//                'P03' => $_POST['last_name'],
//                'P04' => $_POST['mobile_number'],
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'search_merchants', $search_params);
            $search_params = array(
                'fname' => $_POST['first_name'],
                'lname' => $_POST['last_name'],
                'mobile_number' => $_POST['mobile_number'],
                'username' => $_SESSION['username'],
            );
            $result = $this->backend->search_customer($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else { 
                $search_result = array();
                foreach ($response['ResponseMessage'] as $key => $val) {
                    $button = '<input type="button" class="btnViewMerchant" value="View" id="'.$val['id'].'" />';
                    $search_result[] = array(
                        $val['fname'],
                        $val['lname'],
                        $val['mobile_number'],
                        str_repeat('xxxx ', 3) . substr($val['linked_cardnumber'], -4),
                        $button,
                    );
                }
                $this->response = array(
                    'success'   => true,
                    'data'      => $search_result,
                );
            }
        }        
    }
    
    public function view_merchant($message = null) {
        if (!(isset($_GET['id']))) {
            $this->response['message'] = 'Please select a valid merchant to view';
        } else {
//            $api_params = array(
//                'P01' => $_SESSION['sessionid'],
//                'P02' => substr($_GET['id'],1),
//            );
            
            //LC-09/11/2013
            //$response = lib::getWsResponse($this->meta['remote_api'], 'view_merchant_details', $api_params);
            
            $search_params = array(
                'customer_id' => $_GET['id'],
            );
            $result = $this->backend->get_customer_info($search_params);
            $response = $this->backend->get_response();
            
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                $profile = $response['ResponseMessage']['info'];
                $out_profile = array(
                    'card_id' => $profile['id'],
                    'merchant_fname' => $profile['fname'],
                    'merchant_mname' => $profile['mname'],
                    'merchant_lname' => $profile['lname'],
                    'merchant_lname' => $profile['lname'],
                    'national_insurance_no' => $profile['national_insurance_no'],
                    'merchant_mobilenumber' => $profile['mobile_number'],
                    'merchant_address' => $profile['street'] . ' ' . $profile['city'] . ' ' . $profile['country'],
                    'ca_limit' => number_format($profile['credit_limit'],2),
                    'prepaid_card' => $profile['linked_cardnumber'],
                    'card_balance' => number_format($profile['loan_balance'],2),
                    'due_date' => $profile['due_date']
                );
                $transactions = $response['ResponseMessage']['transactions'];
                $ca_history = $response['ResponseMessage']['loans'];
                $payment_history = $response['ResponseMessage']['payments'];
                $credit_limit_history = $response['ResponseMessage']['credit_limit'];
                $customer_documents = $response['ResponseMessage']['customer_documents'];
                $salary_documents = $response['ResponseMessage']['salary_documents'];
            

            
                $out_transactions = array();
                foreach ($transactions as $row) {
                    $out_transactions[] = array(
                        $row['transaction_date'],
                        $row['name'],
                        $row['is_debit'] == 1? number_format($row['amount'],2): '-',
                        $row['is_debit'] == 0? number_format($row['amount'],2): '-',
                        number_format($row['loan_balance'],2),
                    );
                }
                $out_ca_history = array();
                foreach ($ca_history as $row) {
                    $out_ca_history[] = array(
                        $row['created_on'],
                        number_format($row['amount'],2),
                    );
                }
                $out_payment_history = array();
                foreach ($payment_history as $row) {
                    $out_payment_history[] = array(
                        $row['date_paid'],
                        $row['date_paid'],
                        number_format($row['amount'],2),
                    );
                }
                
                $out_credit_limit_history = array();
                foreach ($credit_limit_history as $row) {
                    $out_credit_limit_history[] = array(
                        //$row['created_on'],
                        $row['date_created'],
                        number_format($row['credit_limit'],2),
                    );
                }
                
                $kyc_capture_types = $this->backend->get_capture_types();
                $out_customer_documents = array();
                foreach ($customer_documents as $row) {
                    $out_customer_documents[] = array(
                        $row['kyc_document'],
                        $row['document_id_no'],
                        $kyc_capture_types[$row['capture_type']],
//                        null,
                        $this->kyc_view_path(1, $row['kyc_document'] . '-1', $row['path_filename1'], $row['customer_id']) . $this->kyc_view_path(2, $row['kyc_document'].'-2', $row['path_filename2'], $row['customer_id']),
                    );
                }
                
                $out_salary_documents = array();
                $ctr=1;
                foreach ($salary_documents as $row) {
                    $out_salary_documents[] = array(
                        $this->salary_view_path($ctr, 'salary', $row['path_filename'], $row['customer_id']),
                        $row['date_uploaded'],
                    );
                    $ctr++;
                }
                
                $documents = $this->backend->get_document_types();
                $documents = array('' => '-SELECT-') + $documents;

                        
                $this->response = array(
                    'message'       => $message,
                    'success'       => true,
                    'profile'       => $out_profile,
                    'trans'         => $out_transactions,
                    'ca_history'    => $out_ca_history,
                    'payment_history' => $out_payment_history,
                    'credit_limit_history' => $out_credit_limit_history,
                    'customer_documents' => $out_customer_documents,
                    'salary_documents' => $out_salary_documents,
                    'documents' =>$documents,     
                );                     
            }
        }
    }
    
    protected function kyc_view_path($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            //return '<input type="button" class="kyc_viewer" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>&nbsp;';
            return '<a target="_blank" href="'.WEBROOT.'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'">'.'<input type="button" title="'.$doc_type.'" value="'.$page.'"/>&nbsp;</a>';
            
            //return '<a href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="'.$page.'"/>';
        } else {
            return '';
        }
    }
    
    protected function salary_view_path($page, $doc_type, $path, $cust_id) {
        if ($path != '') {
            return '<input type="button" class="salary_viewer" title="'.$doc_type.'" href="' . WEBROOT .'/customers/management?action=view_document_image&token='.urlencode($path).'&token2='.$cust_id.'" value="Salary'.$page.'"/>&nbsp;';
        } else {
            return '';
        }
    }
    
    
    protected function view_document_image() {

        $this->layout = false;
        if (!(isset($_GET['token']) && $_GET['token'] != '')) {
            echo 'Invalid request';
        } elseif (!(isset($_GET['token2']) && $_GET['token2'] != '')) {
            echo 'Invalid request';
        } elseif (false === ($actual_file = $this->backend->get_actual_document_pathname(array('token' => $_GET['token'], 'token2' => $_GET['token2'])))) {
            echo 'Unable to retrieve document';
        } elseif (!file_exists($actual_file)) {
            echo $actual_file;
        } else {
            $imageinfo = @getimagesize($actual_file); //check image size
            
            //print_r($imageinfo);
//            die();
//            if (!(in_array($imageinfo['mime'], $this->allowed_types))) {
//                echo 'Invalid image';
//            } else {
                $fp = fopen($actual_file, 'rb');
                header('Content-Type: '. $imageinfo['mime']);
                header('Content-Length: '. filesize($actual_file));
                fpassthru($fp);
                exit;
//            }
        }
    }
}