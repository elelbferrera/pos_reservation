<?php
class reports extends template {
    
    public function __construct($meta) {
        parent::__construct($meta);
    }
    
    public function tr() {
        $actions = array('generate_payment','generate_payment_daily','generate_payment_monthly','generate_payment_weekly');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    public function sr() {
        $actions = array();
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    protected function generate_payment_weekly() {
        $startdate = $_GET['startdate'];
        $enddate = $_GET['enddate'];
        //echo $startdate;
        //die();
        if (!isset($startdate, $enddate)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($startdate) !='' || trim($enddate) != '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $startdate,
                'endDate'   => $enddate,
            );
            
            //print_r($search_params);
            //die();
            
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                        //$button,
                    );
                }
                 
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
            }
        }        
    }
    
    protected function generate_payment() {
     
        if (!isset($_POST['tr_startdate'], $_POST['tr_startdate'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['tr_startdate']) !='' || trim($_POST['tr_startdate']) != '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $_POST['tr_startdate'],
                'endDate' => $_POST['tr_enddate'],
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                        //$button,
                    );
                }
                 
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
            }
        }        
    }
    
    protected function generate_payment_daily() {
        
        $date = $_GET['date'];
        //print_r($date);
        //die();
     
        if (!isset($date)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($date) !='')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $date,
                'endDate' => $date,
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                    );
                }
                
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
                 
                
            }
        }        
    } 
    
    protected function generate_payment_monthly() {
        
        $date = $_POST['month'];
        $start_date = date('Y',strtotime($date)).'-'.date('m',strtotime($date)) .'-'. '01'; 
        $tmp_end_date = date("Y-m-d", strtotime("+1 month", strtotime($start_date))); //start date + 1 month 
        $end_date = date("Y-m-d", strtotime("-1 day", strtotime($tmp_end_date))); // tmp_enddate - 1 day to get the last day of the current month
        //print_r($end_date);
        //die();
     
        if (!isset($date)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($date) !='')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $start_date,
                'endDate' => $end_date,
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                    );
                }
                
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
                 
                
            }
        }        
    }
    
    
   }
