<?php
    class coupon extends template {
      	protected $response;
  		protected $merchant_id;
		
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			$this->merchant_id = $_SESSION['user_info']['reference_id'];
        }
		
		public function management()
		{
			
			// print_r(uniqid(true));
			// die();
			$actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
			
            $this->backend->get_deals($this->merchant_id); //
            $deals = $this->backend->get_response();
            $deals = $deals['ResponseMessage'];
            
			
            $this->view->assign('deals', $deals);   
		}
		
		private function register()
		{
			$params = array(
				'merchant_id' => $this->merchant_id,
				'deal_code' => $_POST['txtDealCode'],

				
				'description' => $_POST['txtDescription'],
				'number_of_use' => $_POST['txtNumberOfUse'] =="" ? 0 : $_POST['txtNumberOfUse'],
				'required_points' => $_POST['txtRequiredPoints'],
				'required_amount' => $_POST['txtRequiredAmount'],
				'allow_gift' => isset($_POST['chkEnableRedeemByGift']) ? 1 : 0,
				'allow_other_payment' => isset($_POST['chkEnableRedeemByOther']) ? 1 : 0,
				'start_date' => $_POST['txtStartDate'],
				'end_date' => $_POST['txtEndDate'],
				'is_unlimited' => isset($_POST['chkIsUnlimited']) ? 1 : 0,
			);
			
			  $result = $this->backend->create_deals($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "New Promotion has been registered.",
	                );
	          }
		}
		private function update()
		{
				$params = array(
					'id' => $_POST['txtDealId'],
					'merchant_id' => $this->merchant_id,
					'deal_code' => $_POST['txtDealCode'],
					'description' => $_POST['txtDescription'],
					'number_of_use' => $_POST['txtNumberOfUse'],
					'required_points' => $_POST['txtRequiredPoints'],
					'required_amount' => $_POST['txtRequiredAmount'],
					'allow_gift' => isset($_POST['chkEnableRedeemByGift']) ? 1 : 0,
					'allow_other_payment' => isset($_POST['chkEnableRedeemByOther']) ? 1 : 0,
					'is_unlimited' => isset($_POST['chkIsUnlimited']) ? 1 : 0,
					'start_date' => $_POST['txtStartDate'],
					'end_date' => $_POST['txtEndDate'],
				);
			
			  $result = $this->backend->update_deal($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Promotion has been updated.",
	                );
	          }
		}
		
		private function delete()
		{
			$id = $_GET['id'];
            $result = $this->backend->delete_deal($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
		}
	}		
?>