<?php
    class customer extends template {
      	protected $response;
  		protected $merchant_id;
		
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            // $this->check_session();
			// $this->merchant_id = $_SESSION['user_info']['reference_id'];
			if(isset($_SESSION['username']))
			{
				
			}
        }
		
		public function resetpassword()
		{
			$actions = array("reset_me_now");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
		}
		
		
		public function forgotpassword()
		{
			$actions = array("forgot_me_now");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
		}
		
		private function forgot_me_now()
		{
			$params = array(
				'contact_number' => $_POST['txtContactNumber'],
			);
			$response = lib::getWsResponse(API_URL, 'forgot_password', $params);
			
			if (!(isset($response['respcode'], $response['respmsg']))) {
				// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	$response = $response['respmsg'];
                
				$this->response = array(
                    'success' => true,
                    'message' => $response,
                    'redirect' => WEBROOT."/customer/resetpassword",
                );
				
	        }
		}
		
		private function reset_me_now()
		{
			$params = array(
				'contact_number' => $_POST['txtContactNumber'],
				'verification_code' => $_POST['txtVerificationCode'],
				'new_password' => $_POST['txtNewPassword'],
			);
			$response = lib::getWsResponse(API_URL, 'reset_password', $params);
			
			if (!(isset($response['respcode'], $response['respmsg']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	$response = $response['respmsg'];
        
				$this->response = array(
                    'success' => true,
                    'message' => $response,
                    'redirect' => WEBROOT."/customer/login",
                );
				
	        }
		}
		
		
		private function update_me()
		{
			$sTempFileName="";
			if($_FILES)
	        {
				if (! $_FILES['file']['error'] && $_FILES['file']['size'] < (1024*2) * 1024) {
		                if (is_uploaded_file($_FILES['file']['tmp_name'])) {
		
		                    $aSize = getimagesize($_FILES['file']['tmp_name']); // try to obtain image info
		                    if (!$aSize) {
		                        @unlink($sTempFileName);
		                        return;
		                    }
		    
		                    // check for image type
		                    switch($aSize[2]) {
		                        case IMAGETYPE_JPEG:
		                            $sExt = '.jpg';
		                            break;
		                        case IMAGETYPE_PNG:
		                            $sExt = '.png';
		                            break;
		                        default:
		                            @unlink($sTempFileName);
		                            return;
		                    }
		                    
		                    //$sTempFileName = 'temp_images/' . md5(time().rand())."{$sExt}";
		                    $sTempFileName = 'images/uploads/' .time().rand()."{$sExt}";
		
		                    // move uploaded file into cache folder
		                    move_uploaded_file($_FILES['file']['tmp_name'], $sTempFileName);
		                    
		                    $sTempFileName = WEBROOT."/public/{$sTempFileName}";
		                  }
				}else{
					if(($_FILES['file']['tmp_name']!=""))
					{
						$this->response['success'] = false;
		            	$this->response['message'] = $_FILES['file']['tmp_name'];
						return;
					}
					
				}
	                  
			}

			$params = array(
				'session_id' => $_SESSION['session_id'],
				'photo' => $sTempFileName,
				'email' => $_POST['txtEmailAddress'],
				'contact_number' => str_replace('-','',$_POST['txtContactNumber']),
				'address' => $_POST['txtAddress'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country' => "United States",
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'birth_date' => $_POST['txtBirthDate'],
				'gender' => $_POST['txtGender'],
			);
			
			
			$response = lib::getWsResponse(API_URL, 'update_my_customer_information', $params);
			
			// print_r($params);
			// die();
			if (!(isset($response['respcode'], $response['respmsg']))) {
				// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	
				$params['mobile_number'] = $params['contact_number'];
				$_SESSION['customer_information'] =  $params;
		
				
	        	$this->response = array(
                    'success' => true,
                    'message' => $response['respmsg'],
                     'redirect' => WEBROOT."/customer/my_profile",
                );
	        }
		}

		private function update_contact()
		{
			$params = array(
				'email' => $_POST['txtEmailAddress'],
				'contact_number' => $_POST['txtContactNumber'],
				'address' => $_POST['txtAddress'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country' => "United States",
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'birth_date' => $_POST['txtBirthDate'],
				'gender' => $_POST['txtGender'],
			);
			
			
			$response = lib::getWsResponse(API_URL, 'update_my_customer_information_no_session', $params);
			
			// print_r($params);
			// die();
			if (!(isset($response['respcode'], $response['respmsg']))) {
				// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	
				$params['mobile_number'] = $params['contact_number'];
				$_SESSION['customer_information'] =  $params;
		
				
	        	$this->response = array(
                    'success' => true,
                    'message' => $response['respmsg'],
                     'redirect' => WEBROOT."/customer/my_profile",
                );
	        }
		}
		
		public function edit_profile()
		{
			$actions = array("update_me");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                return $this->$action();
            }
			$this->view->assign('customer', $_SESSION['customer_information']);
		}
		
		public function my_profile()
		{
			
			if(!($_SESSION['session_id']))
			{
				header("Location: ". WEBROOT."/marketplace/in");
			}
			
			// print_r($_SESSION['customer_information']);
			
			$params = array(
				'session_id' => $_SESSION['session_id'],
			);
			$response = lib::getWsResponse(API_URL, 'get_my_customer_reservation', $params);
			$reservations_count = count($response['respmsg']);

			$this->view->assign('reservations',$response['respmsg']);
			$this->view->assign('reservations_count',$reservations_count);
			
			$response = lib::getWsResponse(API_URL, 'get_customer_check_ins', $params);
			$checkins_count = count($response['respmsg']);

			$this->view->assign('check_ins',$response['respmsg']);
			$this->view->assign('checkins_count',$checkins_count);

			
			$response = lib::getWsResponse(API_URL, 'list_my_favorite', $params);
			$favorites_count = count($response['respmsg']);

			$this->view->assign('favorites',$response['respmsg']);
			$this->view->assign('favorites_count',$favorites_count);
			
			// print_r($response['respmsg']);
			// die();
			
			// print_r($_SESSION);
			// die();
			// print_r($_SESSION['customer_information']);
			// die();

			$mobile = $_SESSION['customer_information']['mobile_number'];
			$mobile = substr($mobile, 0,3).'-'.substr($mobile, 3,3).'-'.substr($mobile, 6,4);
		
			$this->view->assign('customer_info',$_SESSION['customer_information']);
			$this->view->assign('mobile',$mobile);
			
		}
		
		
		
		public function logout()
		{	
			session_destroy();
			header("Location: ". WEBROOT."/marketplace/in");
		}
		
		public function login()
		{
			$actions = array("logmein");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
			
			// print_r($_SESSION);
			// die();
		}
		
		private function logmein()
		{
			$params = array(
				'P01' => $_POST['txtUsername'],
				'P02' => $_POST['txtPassword'],
			);
			$response = lib::getWsResponse(API_URL, 'login_customer', $params);
			
			// print_r($response);
			// die();
			if (!(isset($response['respcode'], $response['respmsg']))) {
				// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	$response = $response['respmsg'];
                $_SESSION['username'] = $response['username'];
				$_SESSION['first_name'] = $response['fname'];
				$_SESSION['last_name'] = $response['lname'];
				$_SESSION['session_id'] = $response['SessionID'];
				
				
				$_SESSION['customer_information'] = $response['customer_information'];
				
				$this->response = array(
                    'success' => true,
                    // 'message' => $response['respmsg'],
                    'redirect' => WEBROOT."/customer/my_profile",
                );
				
	        }
		}
		
		public function sign_up()
		{
			$actions = array("register_me", "update_contact");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
		}
		
		private function register_me()
		{
			//username, password, email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
			//birth_date, gender
			//public function online_register_customer()
			
			// print_r($_POST);
			// die();
// 			
			$params = array(
				'username' => $_POST['txtUsername'],
				'password' => $_POST['txtPassword'],
				'email' => $_POST['txtEmailAddress'],
				'contact_number' => $_POST['txtContactNumber'],
				'address' => $_POST['txtAddress'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country' => "United States",
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'birth_date' => $_POST['txtBirthDate'],
				'gender' => $_POST['txtGender'],
			);
			$response = lib::getWsResponse(API_URL, 'online_register_customer', $params);
			
			// print_r($params);
			// die();
			if (!(isset($response['respcode'], $response['respmsg']))) {
				// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	        }else{
	        	$this->response = array(
                    'success' => true,
                    'message' => $response['respmsg'],
                    'redirect' => WEBROOT."/customer/login",
                );
	        }
		}
		
		
		
		public function management()
        {

            $actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
            $this->backend->get_customers($this->merchant_id); //
            $customers = $this->backend->get_response();
            $customers = $customers['ResponseMessage'];
            
            $this->view->assign('customers', $customers);   
		}
		
		
		//merchant_id, last_name, first_name, middle_name, mobile, telephone, state, city, zip, country, email, address, udf1, udf2, udf3	
		protected function register()
		{
			$params = array(
				'merchant_id' => $this->merchant_id,
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'mobile' => $_POST['txtMobile'],
				'telephone' => $_POST['txtTelephone'],
				'state' => $_POST['txtState'],
				'city' => $_POST['txtCity'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'address' => $_POST['txtAddress'],
				'udf1' => $_POST['txtUDF1'],
				'udf2' => $_POST['txtUDF2'],
				'udf3' => $_POST['txtUDF3'],
				'password' => $_POST['txtPassword']
			);
			
		  $result = $this->backend->register_customer($params);
          $response = $this->backend->get_response();
                                                             
          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
          } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
          } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "New Customer has been registered.",
                );
          }
		}
		
		
		//id,merchant_id, last_name, first_name, middle_name, mobile, telephone, state, city, zip, country, email, address, udf1, udf2, udf3 
		protected function update()
		{
			$params = array(
				'id' => $_POST['txtCustomerId'],
				'merchant_id' => $_POST['txtMerchantId'],
				'last_name' => $_POST['txtLastName'],
				'first_name' => $_POST['txtFirstName'],
				'middle_name' => $_POST['txtMiddleName'],
				'mobile' => $_POST['txtMobile'],
				'telephone' => $_POST['txtTelephone'],
				'state' => $_POST['txtState'],
				'city' => $_POST['txtCity'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'address' => $_POST['txtAddress'],
				'udf1' => $_POST['txtUDF1'],
				'udf2' => $_POST['txtUDF2'],
				'udf3' => $_POST['txtUDF3'],
			);
			
			  $result = $this->backend->update_customer($params);
	          $response = $this->backend->get_response();
	                                                             
	          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	                $this->response['success'] = false;
	                $this->response['message'] = 'System error, unable to connect to database';
	          } elseif (!($response['ResponseCode'] == '0000')) {
	                $this->response['success'] = false;
	                $this->response['message'] = $response['ResponseMessage'];
	          } else {
	                $this->response = array(
	                    'data'=> array(),
	                    'success' => true,
	                    'message' => "Customer has been updated.",
	                );
	          }
		}
		
		protected function delete()
		{
			$id = $_GET['id'];
            $result = $this->backend->delete_customer($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
		}
		
	}
?>