<?php
class client extends template {
    protected $response;
    protected $upload_path;
    protected $download_path;
    protected $incoming_path;
    protected $salt;
    protected $allowed_extensions = array('txt');
    protected $allowed_types = array('text/plain');
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
    
    
    
    public function management() {
        $actions = array('register_client','get_client_byid','edit_client','delete_client');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $client_list = $this->backend->get_client();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $out_client_list = array(); 
        foreach ($client_list as $row) {
            $out_client_list[] = array(
                'id'=> sprintf('%06d',$row['id']),
                'name'=>$row['name'],
                'code'=>$row['client_code'],
                'street'=>$row['street'],
                'city'=> $row['city'],
                'state'=>$row['state'],
                'phone'=>$row['phone'],
                'status'=>$row['status'],
                'button'=> '<input type="image" onclick="EditClient('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditClient('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteClient('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
        $this->view->assign('client_list', $out_client_list);
        
    }
    
    protected function delete_client()
    {
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_client($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $client_list = $this->backend->get_client();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $out_client_list = array(); 
        foreach ($client_list as $row) {
            $out_client_list[] = array(
                sprintf('%06d',$row['id']),
                $row['name'],
                $row['client_code'],
                $row['street'],
                $row['city'],
                $row['state'],
                $row['phone'],
                $row['status'],
                '<input type="image" onclick="EditClient('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditClient('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteClient('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
            
            
            $this->response = array(
                'data'=>$out_client_list,
                'success' => true,
                'message' => "Client has been deleted.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Client ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'Client',$datetime,$_SESSION['username'],'');
 
        }  
    }
    
    
    protected function edit_client()
    {     
                                   
        $result = $this->backend->edit_client($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $client_list = $this->backend->get_client();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $out_client_list = array(); 
        foreach ($client_list as $row) {
            $out_client_list[] = array(
                sprintf('%06d',$row['id']),
                $row['name'],
                $row['client_code'],
                $row['street'],
                $row['city'],
                $row['state'],
                $row['phone'],
                $row['status'],
                '<input type="image" onclick="EditClient('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditClient('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteClient('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
            
            $this->response = array(
                'data'=>$out_client_list,
                'success' => true,
                'message' => "Client has been updated.",
            );
             $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Client ID# '.  sprintf('%06d',$row['id']).'was successfully updated.'  ,'Clients',$datetime,$_SESSION['username'],'');
  
        }  
    }   
     
    protected function get_client_byid()
    {
    $id= $_GET['id'];
               
        $params = array(
        'id' => $id,
        );
        $result = $this->backend->get_client_byid($params);
        $response = $this->backend->get_response();
       
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else { 
            $search_result = array();
           
            foreach ($response['ResponseMessage'] as $key => $val) {
             
                $search_result[] = array(
                        'formated_id'                 =>sprintf('%06d',$val['id']),
                        'name'                        =>$val['name'], 
                        'client_code'                 =>$val['client_code'],
                        'last_po_no'                  =>$val['last_po_no'], 
                        'street'                      =>$val['street'], 
                        'city'                        =>$val['city'], 
                        'state'                       =>$val['state'], 
                        'zip'                         =>$val['zip'], 
                        'phone'                       =>$val['phone'], 
                        'fax'                         =>$val['fax'], 
                        'notes'                       =>$val['notes'], 
                        'default_report_status'       =>$val['default_report_status'], 
                        'databack_status'             =>$val['databack_status'], 
                        'basket_notification_status'  =>$val['basket_notification_status'], 
                        'databack_fields'             =>unserialize($val['databack_fields']),
                        'allow_co_fields'             =>unserialize($val['allow_co_fields']),
                        'co_notification_status'      =>$val['co_notification_status'],
                        'schedule_notification_status'=>$val['schedule_notification_status'], 
                        'schedule_access'             =>$val['schedule_access'], 
                        'show_discount_code'          =>$val['show_discount_code'],

            );
            }
          
            $this->response = array(
                'success'   => true,
                'message'      => $search_result,
            );
        }          
   }
     protected function register_client()
    {
       
        $result = $this->backend->register_client($_POST);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $client_list = $this->backend->get_client();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $out_client_list = array(); 
        foreach ($client_list as $row) {
            $out_client_list[] = array(
                sprintf('%06d',$row['id']),
                $row['name'],
                $row['client_code'],
                $row['street'],
                $row['city'],
                $row['state'],
                $row['phone'],
                $row['status'],
                '<input type="image" onclick="EditClient('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                <input type="image" onclick="EditClient('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                <input type="image" onclick="DeleteClient('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
            );
        }
            
            $this->response = array(
                'data'=>$out_client_list,
                'success' => true,
                'message' => "New Client has been added.",
            );
             $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('Registered new client '. $_POST['txtClientName']  ,'Clients',$datetime,$_SESSION['username'],'');
  
        }  
    }                  
    
    protected function alertMessage($message){
        echo '<script type="text/javascript">'
                , 'messageBox("'. $message .'");'
                , '</script>';
    }
    
    function phpAlertWithRedirect($msg,$url) {
        echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. $url .'"</script>';
        
        /*echo '<script type="text/javascript">alert("' . $msg . '")
        window.location.href="'. WEBROOT .'/utilities/generate" 
        </script>';*/
    }
    

    protected function array_map_recursive($fn, $arr) {
        $rarr = array();
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }
        return $rarr;
    }
    
    
    
}

?>
