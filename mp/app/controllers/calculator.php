<?php
class calculator extends template {
	protected $response;
    protected $is_agent; //tagging if the user login is an agent
	protected $parent_id; //this id can be a iso_id or agent_id
	
	
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
    }
	

	public function compute()
	{
		// print_r("hello lem");
		// die();
		$actions = array("get_city", "get_price");
		if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
				
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			// die();
        }
	}
	public function get_price()
	{
		$price['New South Wales'] = array(
			'Sydney' => array(
				'Unleaded Petrol' => 103.85,
				'Premium Unleaded Petrol' => 111.03,
				'Unleaded Petrol E10' => 102.58,
				'Unleaded Petrol 98' => 126.34,
				'Diesel' => 101.94,
			),
			'Newcastle' => array(
				'Unleaded Petrol' => 104.85,
				'Premium Unleaded Petrol' => 113.50,
				'Unleaded Petrol E10' => 104.24,
				'Unleaded Petrol 98' => "--",
				'Diesel' => 102.45,
			)
		);
		
		$price['Northern Territory'] = array(
			'Darwin' => array(
				'Unleaded Petrol' => 108.27,
				'Premium Unleaded Petrol' => 116.90,
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' => 105.88,
			),
		);
		
		$price['Queensland'] = array(
			'Brisbane' => array(
				'Unleaded Petrol' =>  	103.31,
				'Premium Unleaded Petrol' =>  	111.13,
				'Unleaded Petrol E10' => 102.29,
				'Unleaded Petrol 98' => 127.02,
				'Diesel' =>  	101.41,
			),
			'Gladstone' => array(
				'Unleaded Petrol' =>  	107.18,
				'Premium Unleaded Petrol' =>  	 	115.48,
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' => 104.30,
			),
			'Mackay' => array(
				'Unleaded Petrol' => 108.10,
				'Premium Unleaded Petrol' =>  	116.28,
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' =>  	104.57,
			),
			'Townsville' => array(
				'Unleaded Petrol' => 108.05,
				'Premium Unleaded Petrol' =>  	116.60,
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' => 104.23,
			),
			'Cairns' => array(
				'Unleaded Petrol' =>  	106.50,
				'Premium Unleaded Petrol' => 121.63,
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' =>  	104.16,
			),
			'Weipa' => array(
				'Unleaded Petrol' =>  	123.46,
				'Premium Unleaded Petrol' => "--",
				'Unleaded Petrol E10' => "--",
				'Unleaded Petrol 98' => "--",
				'Diesel' =>  	108.97,
			)
		);
		
	}
	
	public function get_city()
	{
		$city['New South Wales'] = "<option value='Sydney'>Sydney</option><option value='Newcastle'>Newcastle</option>";
		$city['Northern Territory'] = "<option value='Darwin'>Darwin</option>";
		$city['Queensland'] = "<option value='Brisbane'>Brisbane</option><option value='Gladstone'>Gladstone</option>
		<option value='Mackay'>Mackay</option><option value='Townsville'>Townsville</option>
		<option value='Cairns'>Cairns</option><option value='Weipa'>Weipa</option>";
		
		$city['South Australia'] = "<option value='Adelaide'>Adelaide</option><option value='Port Lincoln'>Port Lincoln</option>
		<option value='*Port Bonython'>*Port Bonython</option>";
		$city['Tasmania'] = "<option value='Hobart'>Hobart</option><option value='Devonport'>Devonport</option>";
		$city['Victoria'] = "<option value='Melbourne'>Melbourne</option><option value='Geelong'>Geelong</option>";	
		$city['Western Australia'] = "<option value='Perth'>Perth</option><option value='Broome'>Broome</option>
		<option value='Port Hedland'>Port Hedland</option><option value='Esperance'>Esperance</option>
		<option value='Dampier'>Dampier</option>";	
		
		$this->response = array(
            'success' => true,
            'message' => $city[$_GET['city']],
        );
		
	}
}
