<?php
  class agent extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }
	  
	  private function update_agent()
	  {
			//email, last_name, first_name, agent_partner, agent_id, mobile_number, other_number,
			//address1, address2, city, state, zip, country
	  		$params = array(
	  			'id' => $_POST['txtProfileId'],
				'update_by' => $_SESSION['username'],
				'status' => 'A',
				'partner_type_id' => $_POST['txtPartnerTypeId'],
				'parent_id' => -1,
				'first_name' => $_POST['txtFirstName'],
				'last_name' => $_POST['txtLastName'],
				'agent_partner' => $_POST['txtPartner'],
				'agent_id' => $_POST['txtAgentId'],
				'address1' => $_POST['txtAddress1'],
				'address2' => $_POST['txtAddress2'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'email2' => $_POST['txtEmail2'],
				'mobile_number' => $_POST['txtMobileNumber'],
				'other_number' => $_POST['txtPhone1'],
				'other_number_2' => $_POST['txtPhone2'],
				'fax' => $_POST['txtFax'],
				'logo' => '',
				'business_country' => $_POST['txtCountryBusiness'],
				'business_address' => $_POST['txtAddress'],
				'business_city' =>  $_POST['txtCityBusiness'],
				'business_state' =>  $_POST['txtStateBusiness'],
				'business_zip' =>  $_POST['txtZipBusiness'],
				'website' => $_POST['txtWebsite'],
				'bank_name' => $_POST['txtBankName'],
				'account_name' => $_POST['txtAccountName'],
				'account_number' => $_POST['txtAccountNumber'],
				'routing_number' => $_POST['txtRoutingNumber'],
				'ssn' => $_POST['txtSSN'],
				'dob' => $_POST['txtDOB'],
			);
			
		  $result = $this->backend->update_agent($params);
	      $response = $this->backend->get_response();
	                         			                                 
	      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['ResponseCode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['ResponseMessage'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['ResponseMessage'],
	                'redirect' => WEBROOT."/agent/management",
	            );
	      }
	  }
	  
	  
	  
	  public function edit($get)
	  {
		
		$id = $get[0];
		
  		$actions = array("update_agent");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
        }
		
		$result = $this->backend->get_agent_information($id);
		$agent = $this->backend->get_response();
		$agent = $agent['ResponseMessage'];
		
		$this->view->assign('agent', $agent);	
		$this->view->assign('agent_id', $id);
			
		
			
			
		$result = $this->backend->get_partner_types();
		$partner_type = $this->backend->get_response();
		$partner_type = $partner_type['ResponseMessage'];
		
		$this->view->assign('partner_type', $partner_type);	
			
	  	
		$result = $this->backend->get_partner_types();
		$partner_type = $this->backend->get_response();
		$partner_type = $partner_type['ResponseMessage'];
		
		$this->view->assign('partner_type', $partner_type);
		
			
	  	$result = $this->backend->api_get_countries();
		$country = $this->backend->get_response();
		$country = $country['ResponseMessage'];
		$this->view->assign('country', $country);
	  }
	  
	  
	  private function create_agent(){
	  	
			//email, last_name, first_name, agent_partner, agent_id, mobile_number, other_number,
			//address1, address2, city, state, zip, country
	  		$params = array(
				'create_by' => $_SESSION['username'],
				'status' => 'A',
				'partner_type_id' => $_POST['txtPartnerTypeId'],
				'parent_id' => -1,
				'first_name' => $_POST['txtFirstName'],
				'last_name' => $_POST['txtLastName'],
				'agent_partner' => $_POST['txtPartner'],
				'agent_id' => $_POST['txtAgentId'],
				'address1' => $_POST['txtAddress1'],
				'address2' => $_POST['txtAddress2'],
				'city' => $_POST['txtCity'],
				'state' => $_POST['txtState'],
				'zip' => $_POST['txtZip'],
				'country' => $_POST['txtCountry'],
				'email' => $_POST['txtEmail'],
				'email2' => $_POST['txtEmail2'],
				'mobile_number' => $_POST['txtMobileNumber'],
				'other_number' => $_POST['txtPhone1'],
				'other_number_2' => $_POST['txtPhone2'],
				'fax' => $_POST['txtFax'],
				'logo' => '',
				'business_country' => $_POST['txtCountryBusiness'],
				'business_address' => $_POST['txtAddress'],
				'business_city' =>  $_POST['txtCityBusiness'],
				'business_state' =>  $_POST['txtStateBusiness'],
				'business_zip' =>  $_POST['txtZipBusiness'],
				'website' => $_POST['txtWebsite'],
				'bank_name' => $_POST['txtBankName'],
				'account_name' => $_POST['txtAccountName'],
				'account_number' => $_POST['txtAccountNumber'],
				'routing_number' => $_POST['txtRoutingNumber'],
				'ssn' => $_POST['txtSSN'],
				'dob' => $_POST['txtDOB'],
			);
			
		  $result = $this->backend->create_agent($params);
	      $response = $this->backend->get_response();
	                         			                                 
	      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['ResponseCode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['ResponseMessage'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['ResponseMessage'],
	                'redirect' => WEBROOT."/agent/management",
	            );
	      }
	  }
	  
	  
      
	  public function add()
	  {
	  	
		$actions = array("create_agent");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
		
		
	  	$result = $this->backend->get_partner_types();
		$partner_type = $this->backend->get_response();
		$partner_type = $partner_type['ResponseMessage'];
		
		$this->view->assign('partner_type', $partner_type);
		
		$result = $this->backend->api_get_countries();
		$country = $this->backend->get_response();
		$country = $country['ResponseMessage'];
		$this->view->assign('country', $country);
		
		$result = $this->backend->get_states("");
		$states = $this->backend->get_response();
		$states = $states['ResponseMessage'];
		$this->view->assign('states', $states);
	  }
	  
	  
      public function management()
      {		
            $actions = array("delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
            $this->backend->get_all_agents();
            $data = $this->backend->get_response();
            $data = $data['ResponseMessage'];

            // $countries = $this->backend->get_countries();
            // $countries[''] ='Select Country';
            
            // $this->view->assign('countries', $countries);  
            
            $this->view->assign('agents', $data);         
      }
      
      //id,agent_code, agent_name, legal_name, dba_name, tax_id, phone_no, reseller_name, reseller_type, fax, dba_address1,dba_address2, dba_city, dba_state, dba_zip, dba_country, mailing_address1, mailing_address2, mailing_city, mailing_state,mailing_zip, mailing_country, account_no, routing_no, email, username, password
      private function register()
      {
          $params = array(
            'agent_code' => $_POST['txtAgentCode'],
            'agent_name' => $_POST['txtAgentName'],
            'legal_name' => $_POST['txtLegalName'],
            'dba_name' => $_POST['txtDBAName'],
            'tax_id' => $_POST['txtTaxId'],
            'phone_no' => $_POST['txtPhoneNo'],
            'reseller_name' => $_POST['txtResellerName'],
            'reseller_type' => $_POST['txtResellerType'],
            'fax' => $_POST['txtFax'],
            'username' => $_POST['txtUsername'],
            'password' => $_POST['txtPassword'],
            'dba_address1' => $_POST['txtDBAAddress1'],
            'dba_address2' => $_POST['txtDBAAddress2'],
            'dba_city' => $_POST['txtDBACity'],
            'dba_state' => $_POST['txtDBAState'],
            'dba_zip' => $_POST['txtDBAZip'],
            'dba_country' => $_POST['txtDBACountry'],
            'mailing_address1' => $_POST['txtMailingAddress1'],
            'mailing_address2' => $_POST['txtMailingAddress2'],
            'mailing_city' => $_POST['txtMailingCity'],
            'mailing_state' => $_POST['txtMailingState'],
            'mailing_zip' => $_POST['txtMailingZip'],
            'mailing_country' => $_POST['txtMailingCountry'],
            'account_no' => $_POST['txtAccountNo'],
            'routing_no' => $_POST['txtRoutingNo'],
            'email' => $_POST['txtEmail'],
            'iso_id' => $this->iso_id,
          );
          
		  
          
          $result = $this->backend->register_agent($params);
          $response = $this->backend->get_response();
                                                             
          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
          } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
          } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "New Agent has been registered.",
                );
          }
      }
      private function update()
      {
        $params = array(
            'id' => $_POST['txtAgentId'],
            'agent_code' => $_POST['txtAgentCode'],
            'agent_name' => $_POST['txtAgentName'],
            'legal_name' => $_POST['txtLegalName'],
            'dba_name' => $_POST['txtDBAName'],
            'tax_id' => $_POST['txtTaxId'],
            'phone_no' => $_POST['txtPhoneNo'],
            'reseller_name' => $_POST['txtResellerName'],
            'reseller_type' => $_POST['txtResellerType'],
            'fax' => $_POST['txtFax'],
            'dba_address1' => $_POST['txtDBAAddress1'],
            'dba_address2' => $_POST['txtDBAAddress2'],
            'dba_city' => $_POST['txtDBACity'],
            'dba_state' => $_POST['txtDBAState'],
            'dba_zip' => $_POST['txtDBAZip'],
            'dba_country' => $_POST['txtDBACountry'],
            'mailing_address1' => $_POST['txtMailingAddress1'],
            'mailing_address2' => $_POST['txtMailingAddress2'],
            'mailing_city' => $_POST['txtMailingCity'],
            'mailing_state' => $_POST['txtMailingState'],
            'mailing_zip' => $_POST['txtMailingZip'],
            'mailing_country' => $_POST['txtMailingCountry'],
            'account_no' => $_POST['txtAccountNo'],
            'routing_no' => $_POST['txtRoutingNo'],
            'email' => $_POST['txtEmail'],
            'iso_id' => $this->iso_id,
          );
          
          $result = $this->backend->update_agent($params);
          $response = $this->backend->get_response();
                                                             
          if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
          } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
          } else {
                $this->response = array(
                    'data'=> array(),
                    'success' => true,
                    'message' => "Agent has been updated.",
                );
          }
          
      }
      private function delete()
      {
            $id = $_GET['id'];
            
            $result = $this->backend->delete_agent($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
      }
  }  
?>
