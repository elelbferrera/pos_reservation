<?php
class reports extends template {
    
    public function __construct($meta) {
        parent::__construct($meta);
    }
    
    public function tr() {
        $actions = array('generate_payment','generate_payment_daily','generate_payment_monthly','generate_payment_weekly','export_payment','export_payment_weekly','export_payment_daily','export_payment_monthly','export_payment_custom');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            if ($action != 'export_payment_weekly'){
                $this->layout = 'json';    
            }
            $this->$action();
        }
    }
    
    public function sr() {
        $actions = array();
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
    }
    
    protected function export_payment_weekly(){
        $params = array(
            'startdate' => $_POST['weekly_startdate_export'],
            'enddate'   => $_POST['weekly_enddate_export']
        );
        
        $this->export_payment($params);
        
    }
    
    protected function export_payment_daily(){
        $params = array(
            'startdate' => $_POST['daily_startdate_export'],
            'enddate'   => $_POST['daily_enddate_export']
        );
        
        $this->export_payment($params);
        
    }
    
    protected function export_payment_custom(){
        $params = array(
            'startdate' => $_POST['custom_startdate_export'],
            'enddate'   => $_POST['custom_enddate_export']
        );
        
        $this->export_payment($params);
        
    }
    
    protected function export_payment_monthly(){
        $date = $_POST['monthly_export'];
        $start_date = date('Y',strtotime($date)).'-'.date('m',strtotime($date)) .'-'. '01'; 
        $tmp_end_date = date("Y-m-d", strtotime("+1 month", strtotime($start_date))); //start date + 1 month 
        $end_date = date("Y-m-d", strtotime("-1 day", strtotime($tmp_end_date))); // tmp_enddate - 1 day to get the last day of the
        
        $params = array(
            'startdate' => $start_date,
            'enddate'   => $end_date
        );
        
        //print_r($params);
        //die();
        $this->export_payment($params);
        
    }
    
    protected function export_payment($params)
    {
                 
        $startDate= date("Y-m-d", strtotime($params['startdate']));
        //$endDate= date("Y-m-d", strtotime('+1 day',strtotime($params['enddate'])));
        $endDate= date("Y-m-d", strtotime($params['enddate']));
        
        $search_params = array(
            'startDate' => $startDate,
            'endDate' => $endDate,
        );
        
        //print_r($search_params);
        //die();
                                                                        
        $result = $this->backend->generate_payment_report($search_params);
        $response = $this->backend->get_response();
        
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {   
            //$search_result = array();
            $rowPosition=1;
            $countRecord=0;
            $loanAmount=0;
            
            require_once "PHPExcel.php"; 
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            $excel->getActiveSheet()->setTitle('Transaction Report');
            //set cell A1 content with some text
            $excel->getActiveSheet()->setCellValue('A1', 'Date Time');
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
             
            $excel->getActiveSheet()->setCellValue('B1', 'Transaction Number');
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            
            $excel->getActiveSheet()->setCellValue('C1', 'Account No');
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('D1', 'Account Name');
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('E1', 'Amount');
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(10);
            $excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            
//            $excel->getActiveSheet()->setCellValue('F1', 'Loan Start Date');
//            $excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(10);
//            $excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

//            $excel->getActiveSheet()->setCellValue('G1', 'Loan Termination Date');
//            $excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(10);
//            $excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
//            
  //$val['transaction_date'],
//                        $val['reference_no'],
//                        $val['account_no'],
//                        $val['customer_name'],
//                        $val['total_amount'],

            
            foreach ($response['ResponseMessage'] as $key => $val) {
                $rowPosition++;

                //$excel->getActiveSheet()->setCellValue('A'.$rowPosition, "'".sprintf('%06d',$val['loan_number']));
                $excel->getActiveSheet()->setCellValue('A'.$rowPosition, $val['transaction_date']);
                $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                
                 
                $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $val['reference_no']);
                $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                
                
                $excel->getActiveSheet()->setCellValue('C'.$rowPosition, "'". $val['account_no']);
                $excel->getActiveSheet()->getStyle('C'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('D'.$rowPosition, $val['customer_name']);
                $excel->getActiveSheet()->getStyle('D'.$rowPosition)->getFont()->setSize(10);
                

                $excel->getActiveSheet()->setCellValue('E'.$rowPosition, $val['total_amount']);
                $excel->getActiveSheet()->getStyle('E'.$rowPosition)->getFont()->setSize(10);
                
                
              //  $excel->getActiveSheet()->setCellValue('F'.$rowPosition, $val['approved_on']);
//                $excel->getActiveSheet()->getStyle('F'.$rowPosition)->getFont()->setSize(10);
//                
//                $excel->getActiveSheet()->setCellValue('G'.$rowPosition, $val['TerminationDate']);
//                $excel->getActiveSheet()->getStyle('G'.$rowPosition)->getFont()->setSize(10);

                
                $countRecord++;
                //$loanAmount = $loanAmount+$val['amount'];
            }
             //JR-2014/03/31 For Summary (Count, Sum, Average)
             
              //JR-2014/03/31 For Summary (Count, Sum, Average)
              /*
             if ($countRecord>0){
                 $rowPosition+=4;
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Count:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $countRecord);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                 
                 $rowPosition++;
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Total Loan Amount:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $loanAmount);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
                 
                 $rowPosition++;
                 //$y = $x->format('%m month, %d days ,%H hours');
                 $excel->getActiveSheet()->setCellValue('A'.$rowPosition, 'Average Loan Amount:');
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setSize(10);
                 $excel->getActiveSheet()->getStyle('A'.$rowPosition)->getFont()->setBold(true);
                 $excel->getActiveSheet()->setCellValue('B'.$rowPosition, $loanAmount / $countRecord);
                 $excel->getActiveSheet()->getStyle('B'.$rowPosition)->getFont()->setSize(10);
             }
             */
             
        }
        
        
         
        $filename='Transaction Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
     }
    
    protected function generate_payment_weekly() {
        $startdate = $_GET['startdate'];
        $enddate = $_GET['enddate'];
        //echo $startdate;
        //die();
        if (!isset($startdate, $enddate)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($startdate) !='' || trim($enddate) != '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $startdate,
                'endDate'   => $enddate,
            );
            
            //print_r($search_params);
            //die();
            
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['partner'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                        //$button,
                    );
                }
                 
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
            }
        }        
    }
    
    protected function generate_payment() {
     
        if (!isset($_POST['tr_startdate'], $_POST['tr_startdate'])) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($_POST['tr_startdate']) !='' || trim($_POST['tr_startdate']) != '')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $_POST['tr_startdate'],
                'endDate' => $_POST['tr_enddate'],
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['partner'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                        //$button,
                    );
                }
                 
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
            }
        }        
    }
    
    protected function generate_payment_daily() {
        
        $date = $_GET['date'];
        //print_r($date);
        //die();
     
        if (!isset($date)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($date) !='')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $date,
                'endDate' => $date,
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['partner'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                    );
                }
                
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
                 
                
            }
        }        
    } 
    
    protected function generate_payment_monthly() {
        
        $date = $_POST['month'];
        $start_date = date('Y',strtotime($date)).'-'.date('m',strtotime($date)) .'-'. '01'; 
        $tmp_end_date = date("Y-m-d", strtotime("+1 month", strtotime($start_date))); //start date + 1 month 
        $end_date = date("Y-m-d", strtotime("-1 day", strtotime($tmp_end_date))); // tmp_enddate - 1 day to get the last day of the current month
        //print_r($end_date);
        //die();
     
        if (!isset($date)) {
            $this->response['message'] = 'Incomplete search parameters';
        } elseif (!(trim($date) !='')) {
            $this->response['message'] = 'Incomplete search parameters';
        } else {           
            $search_params = array(
                'startDate' => $start_date,
                'endDate' => $end_date,
            );
           
            $result = $this->backend->generate_payment_report($search_params);
            $response = $this->backend->get_response();
           
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                
                $search_result = array();
                foreach ($response['ResponseMessage'] as     $key => $val) {
                    $search_result[] = array(
                        $val['transaction_date'],
                        $val['reference_no'],
                        $val['partner'],
                        $val['account_no'],
                        $val['customer_name'],
                        $val['total_amount'],
                    );
                }
                
                if(empty($search_result)){
                    $this->response = array(
                        'success'   => false,
                        'data'      => $search_result,
                    );
                    
                } else {
                    $this->response = array(
                        'success'   => true,
                        'data'      => $search_result,
                    );
                }
                 
                
            }
        }        
    }
    
    
   }
