<?php
class admin extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        $this->check_session();
    }
      public function users() {
        $actions = array('register_user','delete_user','edit_user','get_user_by_id');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }else{
            
            $user_list = $this->backend->get_users();
            $response = $this->backend->get_response();
            $user_list = $response['ResponseMessage'];
            
            
            $this->view->assign('users', $user_list);
            
            
            if ($_SESSION['user_type_desc'] == "SUPER ADMIN")
            {
                $user_types = array(
                    '1' => 'SUPER ADMIN',
                    '4' => 'ISO',
                    '9' => 'ADMIN',
                );
            }
            else{
                $user_types=  $this->backend->get_user_types();
            }
            
            $this->view->assign('user_types', $user_types);
            
        }
    }
       protected function get_user_by_id()
    {
        $id= $_GET['id'];
               
        $params = array('id' => $id,);
        $result = $this->backend->get_user_by_id($params);
        $response = $this->backend->get_response();

        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) 
        { $this->response['message'] = 'System error, unable to connect to database'; } 
        elseif (!($response['ResponseCode'] == '0000')) 
        { $this->response['message'] = $response['ResponseMessage']; } 
        else 
        { 
            $search_result = array();            
            foreach ($response['ResponseMessage'] as $key => $row) 
            {             
                $search_result[] = array(
                        'formated_id'=> sprintf('%06d',$row['id']),
                        'fname'=>$row['fname'],
                        'lname'=>$row['lname'],
                        'user_type'=> $row['user_type'],
                        'username'=>$row['username'],
                        'mobile_number' =>  $row['mobile_number'],
                        'email_address'=>$row['email_address'],
                        'status'=>$row['status_text'],
                        'is_center'=>$row['is_center'],
                        'user_center'=>$row['user_center'],
                        );
            }
                    
            $this->response = array(
                'success'      => true,
                'message'      => $search_result,);
        }          
    }

    
    public function edit_user() 
    {             
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) 
        {
            $this->layout = 'json';            
            if (!(isset($_POST['txtFirstName'], $_POST['txtLastName'], $_POST['txtEmail'], $_POST['txtUsername'], $_POST['user_type']))) 
            { $this->response['message'] = 'Please enter information on the required fields marked by *.'; } 
            elseif (!(trim($_POST['txtFirstName']) != '' && trim($_POST['txtLastName']) != '' && trim($_POST['txtEmail']) != '' && trim($_POST['txtUsername']) != '' && trim($_POST['user_type']) != '')) 
            { $this->response['message'] = 'Please enter information on the required fields marked by *.'; } 
            else 
            {
                $reg_params = array(
                    'id' => $_POST['txtID'],
                    'fname' => $_POST['txtFirstName'],
                    'lname' => $_POST['txtLastName'],
                    'email_address' => $_POST['txtEmail'],
                    'mobile_number' => $_POST['txtMobile'],                    
                    'username' => $_POST['txtUsername'],
                   // 'password' => $_POST['txtPassword'],
                    'user_type' => $_POST['user_type'],
                    'is_center' =>  isset($_POST['is_center'])? 1: 0,
                    'user_center' =>  isset($_POST['user_center'])? $_POST['user_center']: 0,
                    );
               
                $result = $this->backend->edit_user($reg_params);
                $response = $this->backend->get_response();

                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) 
                { $this->response['message'] = 'System error, unable to connect to database'; } 
                elseif (!($response['ResponseCode'] == '0000')) 
                { $this->response['message'] = $response['ResponseMessage']; } 
                else 
                {                                        
                    $user_list = $this->backend->get_users_detailed();
                    $response1 = $this->backend->get_response();
                    $user_list = $response1['ResponseMessage'];
                    $out_user_list = array(); 
                    foreach ($user_list as $row) 
                    {
                        $out_user_list[] = array(
                            sprintf('%06d',$row['id']),
                            $row['fname'],
                            $row['lname'],
                            $row['user_type_desc'],
                            $row['username'],
                            $row['email_address'],
                            $row['status_text'],
                            '<input type="image" onclick="EditUser('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                             <input type="image" onclick="EditUser('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                             <input type="image" onclick="DeleteUser('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',);
                    }                     
                    
                    $this->response = array(
                        'success' => true,
                        'message' => $response['ResponseMessage']['message'],
                        'data'=>$out_user_list,
                    );
                 
                    $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Updated user '. $_POST['txtFirstName']. ' '. $_POST['txtLastName']. ' with user type id#'. $_POST['user_type'],'Update User',$datetime,$_SESSION['username'],'');

                }
            }
        }
    }
    
    
      protected function delete_user()
    {
        $id= $_GET['id'];
        $params = array(
                'id' => $id,
            );
        $result = $this->backend->delete_user($params);
        $response = $this->backend->get_response();
                                                         
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
        $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
        $this->response['message'] = $response['ResponseMessage'];
        } else {

        $client_list = $this->backend->get_users_detailed();
        $response = $this->backend->get_response();
        $client_list = $response['ResponseMessage'];
        $out_client_list = array(); 
        foreach ($client_list as $row) {
            $out_client_list[] = array(
                 sprintf('%06d',$row['id']),
                            $row['fname'],
                            $row['lname'],
                            $row['user_type_desc'],
                            $row['username'],
                            $row['email_address'],
                            $row['status_text'],
                            '<input type="image" onclick="EditUser('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                             <input type="image" onclick="EditUser('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                             <input type="image" onclick="DeleteUser('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',);
            
        }
            
            
            $this->response = array(
                'data'=>$out_client_list,
                'success' => true,
                'message' => "User has been deleted.",
            ); 
            $datetime=$this->backend->get_server_date();
            $this->backend->save_to_action_logs('User ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'User',$datetime,$_SESSION['username'],'');
 
        }  
    }
    
    
 
      public function register_user() {
       
      
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->layout = 'json';            
            if (!(isset($_POST['txtFirstName'], $_POST['txtLastName'], $_POST['txtEmail'], $_POST['txtUsername'], $_POST['user_type']))) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } elseif (!(trim($_POST['txtFirstName']) != '' && trim($_POST['txtLastName']) != '' && trim($_POST['txtEmail']) != '' && trim($_POST['txtUsername']) != '' && trim($_POST['user_type']) != '')) {
                $this->response['message'] = 'Please enter information on the required fields marked by *.';
            } else {
                $reg_params = array(
                    'fname' => $_POST['txtFirstName'],
                    'lname' => $_POST['txtLastName'],
                    'email_address' => $_POST['txtEmail'],
                    // 'mobile_number' => $_POST['txtMobile'],
                    'email_address' => $_POST['txtEmail'],
                    'username' => $_POST['txtUsername'],
                    'password' => $_POST['txtPassword'],
                    'user_type' => $_POST['user_type'],
                    'is_center' => isset($params['is_center'])? $params['is_center']: 0,
                    'user_center' =>  isset($_POST['user_center'])? $_POST['user_center']: 0,  
                    'created_by' => $_SESSION['username'],
                 );
                $result = $this->backend->register_user($reg_params);
                $response = $this->backend->get_response();
                if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                    $this->response['message'] = 'System error, unable to connect to database';
                } elseif (!($response['ResponseCode'] == '0000')) {
                    $this->response['message'] = $response['ResponseMessage'];
                } else {
                    
                    
                    // $user_list = $this->backend->get_users_detailed();
                    // $response1 = $this->backend->get_response();
                    // $user_list = $response1['ResponseMessage'];
                    // $out_user_list = array(); 
                    // foreach ($user_list as $row) {
                        // $out_user_list[] = array(
                             // sprintf('%06d',$row['id']),
                            // $row['fname'],
                            // $row['lname'],
                             // $row['user_type_desc'],
                            // $row['username'],
                            // $row['email_address'],
                            // $row['status_text'],
                            // '<input type="image" onclick="EditUser('.$row['id'].',true)" src="'.WEBROOT.'/public/images/view.png" title="view" />
                            // <input type="image" onclick="EditUser('.$row['id'].')" src="'.WEBROOT.'/public/images/edit.png" title="edit" />
                            // <input type="image" onclick="DeleteUser('.$row['id'].')" src="'.WEBROOT.'/public/images/deleted.png" title="delete" />',
                        // );
                    // }
                     
                    
                    $this->response = array(
                        'success' => true,
                        // 'message' => $response['ResponseMessage']['message'],
                        // 'data'=>$out_user_list,
                    );
					$datetime = date('Y-m-d H:i:s');
                 
                    // $datetime=$this->backend->get_server_date();
                    $this->backend->save_to_action_logs('Created new user '. $_POST['txtFirstName']. ' '. $_POST['txtLastName']. ' with user type id#'. $_POST['user_type'],'User Registration',$datetime,$_SESSION['username'],'');

                }
            }
        }
    }
    
    
    
    // PARTNER LISTING
    public function acl()
    {
        $actions = array('delete_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        
        if ($usertype == "SUPER ADMIN"){
            $profile_list = $this->backend->get_all_profile();    
        
        } else {
            $profile_list = $this->backend->get_all_profile_selected($row[0]['id']);    
        }
        $this->view->assign('profile_list', $profile_list);

    }
    
    //NEW MODULE
    public function new_module()
    {
        $actions = array('update_module','add_module','delete_module');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $result = $this->backend->get_all_resources();
        $response = $this->backend->get_response();
        $search_result = $response['ResponseMessage'];
        $this->view->assign('result', $search_result);
    }
    
    protected function add_module(){
        $params = array(
            'resource' => $_POST['adminModuleValue'],
            'description' => $_POST['adminModuleName'],
        );    
        $payment = $this->backend->add_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
     protected function update_module()
    {
         $params = array(
            'id' => $_POST['admin_module_id'],
            'value' => $_POST['admin_module_value'],
            'description' =>  $_POST['admin_module_name'],
        );    
        $this->backend->update_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }
    
    protected function delete_module()
    {
         $params = array(
            'id' => $_GET['id'],
        );    
        $this->backend->delete_module($params);
        $response = $this->backend->get_response();
        $this->response = array(
                'success' => true,
                'message' => $response['ResponseMessage']['message']
                );
    }

    // NEW PROFILE
    public function new_profile()
    {
        $actions = array('add_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        
        if ($usertype == SYSTEM_ADMIN){
            $module_list = $this->backend->get_all_modules_list();
        } else {
            $module_list = $this->backend->get_all_modules_list_selected($row[0]['id']);
        }
        $this->view->assign('module_list', $module_list);
    }
    
    public function add_profile()
    {
        $profile_name = trim($_POST['profile_name']);
        
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $user_type_id = $row[0]['id'];

        $params = array(
            'description'   => $profile_name,
            'module_access' => $_POST['module_access'],
            'created_by'    => $user_type_id,
        );
        
        //print_r($params);
        //die();
        
        $result = $this->backend->add_profile($params);
        $response = $this->backend->get_response();
                                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'success' => true,
                'message' => "Successfully added a profile.",
            ); 
        }  
    }

    // EDIT PROFILE
    public function edit_profile($profile_id = null)
    {
        $actions = array('save_edit_profile');
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            $this->$action();
        }
//        
        if(is_null($profile_id))
            return FALSE;
            
        if(!is_numeric($profile_id[0]))
            return FALSE;    
            
        //print_r($profile_id);
        $result = $this->backend->get_user_type_by_username($_SESSION['username']);
        $response = $this->backend->get_response();
        $row=$response['ResponseMessage'];
        $usertype = $row[0]['description'];
        
        if ($usertype == "SUPER ADMIN"){
            $module_list = $this->backend->get_all_modules_list();
        } else {
            $module_list = $this->backend->get_all_modules_list_selected($row[0]['id']);
        }
        
        
        //$module_list = $this->backend->get_all_modules_list();
        $this->view->assign('module_list', $module_list);
        

        $profile_info = $this->backend->get_all_profile('id = '.$this->db->db_escape($profile_id[0]));
        $profile_info['id'] = $profile_info[0]['id'];
        $profile_info['profile_name'] = $profile_info[0]['description'];
        
        $access = $this->backend->get_all_profile_access($profile_id[0]);
        
        $resource_id="";
        foreach($access as $data) {
            $resource_id .= $data['resource_id'] .",";
        }

       
        $profile_info['module_access'] = explode(',', $resource_id);
        $this->view->assign('profile_info', $profile_info);
    }
    
    protected function save_edit_profile()
    {
        $profile_name = trim($_POST['profile_name']);

        $params = array(
            'description'  => $profile_name,
            'module_access' => $_POST['module_access'],
            'id'            => $_POST['id']
        );
        
        //print_r($params);
        //die();

        $result = $this->backend->edit_profile($params);
        $response = $this->backend->get_response();
        
        
                                                                 
        if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['ResponseCode'] == '0000')) {
            $this->response['message'] = $response['ResponseMessage'];
        } else {
            $this->response = array(
                'success' => true,
                //'message' => "Profile has been updated. This action automatically log out your account.",
                'message' => "Profile has been updated.",
                //'redirect'=>  WEBROOT .'/log/out', 
            ); 
            //die();
        }  
       
    }

    protected function delete_profile()
    {
         $params = array(
            'id' => $_GET['id'],
        );    
        $this->backend->delete_profile($params['id']);
        $response = $this->backend->get_response();
        $this->response = array(
            'success' => true,
            'message' => $response['ResponseMessage']['message']
        );
    }
    
}
