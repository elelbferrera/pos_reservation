<?php
    class product extends template{
      protected $response;
	  protected $iso_id;
      public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
			
			// $this->iso_id = $_SESSION['user_info']['reference_id'];
      }
      
      public function management()
      {
      	
		    $result = $this->backend->product_list();
            $response = $this->backend->get_response();
			
			$products = $response['ResponseMessage'];	
			
            $this->view->assign('products', $products);
			
      }  
	  
	  private function update_product()
	  {
	  	
	  }
	  
	  private function create_product()
	  {
	  		$params = array(
				'create_by' => $_SESSION['username'],
				'status' => 'A',
				'name' => $_POST['txtProductName'],
				'parent_id' => -1,
				'buy_rate' => $_POST['txtBuyRate'],
				'description' => $_POST['txtProductDescription'],
				'custom_fields' => $_POST['txtCustomFields'],
			);
			
			if(isset($_POST['txtProductID']))
			{
				$params['id'] = $_POST['txtProductID'];
				$result = $this->backend->update_product($params);
			      $response = $this->backend->get_response();
			                         			                                 
			      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			            $this->response['success'] = false;
			            $this->response['message'] = 'System error, unable to connect to database';
			      } elseif (!($response['ResponseCode'] == '0000')) {
			            $this->response['success'] = false;
			            $this->response['message'] = $response['ResponseMessage'];
			      } else {
			            $this->response = array(
			                'success' => true,
			                'message' => $response['ResponseMessage'],
			                'redirect' => WEBROOT."/product/management/",
			            );
			      }
			}else{
				  $result = $this->backend->create_product($params);
			      $response = $this->backend->get_response();
			                         			                                 
			      if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
			            $this->response['success'] = false;
			            $this->response['message'] = 'System error, unable to connect to database';
			      } elseif (!($response['ResponseCode'] == '0000')) {
			            $this->response['success'] = false;
			            $this->response['message'] = $response['ResponseMessage'];
			      } else {
			            $this->response = array(
			                'success' => true,
			                'message' => $response['ResponseMessage'],
			                'redirect' => WEBROOT."/product/management/",
			            );
			      }
			}
	  }
	  
	  public function add()
	  {
	  		$actions = array("create_product");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
	  }
	  
	  public function edit($get){
	  		$product_id = $get[0];
			
			
			if($product_id =="action=create_product")
	  		{
		  		//$_GET['action'] = "create_product";
				// if(isset($_POST['action']) && in_array($_GET['action'], $actions))
				// {
					// print_r($_GET);
					// die();
					$action = "create_product";
	                $this->layout = 'json';
	                return $this->$action();
				// }
			}
			
			$result = $this->backend->get_product_information($product_id);
	      	$response = $this->backend->get_response();
			
			$product = $response['ResponseMessage'];
			$product = $response['ResponseMessage'];
			
			//get_product_custom_fields
			$result = $this->backend->get_product_custom_fields($product_id);
	      	$response = $this->backend->get_response();
			
			$custom_fields = $response['ResponseMessage'];
			
			$this->view->assign("product", $product);
			$this->view->assign("custom_fields", $custom_fields);			
	  }
	  
	  public function delete(){
	  	
	  }
	  
	}
?>