<?php
class marketplace extends template {
    protected $response;
    
    public function __construct($meta) {
        parent::__construct($meta);
        // $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        // $this->check_session();
    }
    
	public function in($var)
	{
        
		$this->view->assign('marketplace_url', WEBROOT."/marketplace/in");
			
			
			// print_r($_SESSION);
				// die();
		if(isset($var[0]))
		{
			//added -- Alshey
			if ($var[0] == 'allstores') {
					$response = lib::getWsResponse(API_URL, 'get_all_active_merchant');
					
					// print_r($response);
					// die();
					
					if (!(isset($response['respcode'], $response['respmsg']))) {
						// $this->layout = "json";
			            $this->response['success'] = false;
			            $this->response['message'] = 'System error, unable to connect to database';
			        } elseif (!($response['respcode'] == '0000')) {
			        	// $this->layout = "json";
			            $this->response['success'] = false;
			            $this->response['message'] = $response['respmsg'];
			        }

			        $merchants = $response['respmsg']; 
			       	$this->view->assign("show_all_store", 1);
			        $this->view->assign("merchants", $merchants);
			        $this->view->assign("search_value", "");
			} else {

				$params = array(
		     		'search_value' => $var[0]   	    
		        );
		        $response = lib::getWsResponse(API_URL, 'online_get_merchants', $params);
				
				// print_r($response);
				// die();
				
				if (!(isset($response['respcode'], $response['respmsg']))) {
					// $this->layout = "json";
		            $this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		        } elseif (!($response['respcode'] == '0000')) {
		        	// $this->layout = "json";
		            $this->response['success'] = false;
		            $this->response['message'] = $response['respmsg'];
		        }
				
				$merchants = $response['respmsg']; 
				// // print_r();
				// // die();
	// 			
				// foreach($merchants as $m)
				// {
					// print_r($m);
					// die();
				// }
				$this->view->assign("search_value", $var[0]);
				
				$this->view->assign("merchants", $merchants);
			}
		}else{
			$this->view->assign("search_value", "");
			$this->view->assign("merchants", array());
		}		
		
	}
    
}
