<?php
    class reservation extends template {
        protected $response;
		protected $merchant_id;
        public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            // $this->check_session();
        }
		
      	public function management()
        {
	
            $actions = array("add", "update", "delete", "search");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
			
			$today = date("Y-m-d");
			$prev_month = date("Y-m-d", strtotime("$today -1 month"));
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	'date' => $prev_month,
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservations_per_merchant', $params);
			$reservations = $response['respmsg'];
			$this->view->assign('reservations', $reservations);
			
			$response = lib::getWsResponse(API_URL, 'get_branch_per_merchant', $params);
			$branches = $response['respmsg'];
			
			
			$this->view->assign('branches', $branches);
			// print_r($branches);
			// die();
			
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            	'date' => date('Y-m-d'),
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_salon_specialist_per_merchant_per_day', $params);
		 	$specialist = $response['respmsg'];
			$this->view->assign('specialist', $specialist);
			
			
			
			$response = lib::getWsResponse(API_URL, 'get_products_per_merchant', $params);
		 	$services = $response['respmsg'];
			
			$this->view->assign('services', $services);
			
			
			// print_r($specialist);
			// die();
			
			$results = "resources: [";
			foreach($specialist as $sp)
			{
				$id = $sp['id'];
				$name = $sp['first_name'] . " " .$sp['last_name'];
				$results.="{ id: '{$id}', title: '{$name}' },";
			}
			$results.="{ id: '-1', title: 'N/A' },";
			$results .="],";
			
			// $results ="resources: [{ id: 'a', title: 'Room A' },
        	// { id: 'b', title: 'Room B' },
        	// { id: 'c', title: 'Room C'},
        	// { id: 'd', title: 'Room D'}";
			
			
			
			$events = "events: [";
			
			// print_r($reservations);
			// die();
// 			
			$hidden_fields = "";
			foreach($reservations as $r)
			{
				$id = $r['id'];	
				$date = substr($r['reservation_date'], 0, 10);
				
				// print_r($date);
				// die();
				$date .= "T".$r['reservation_time'];
				
				$title = $r['first_name']." ".$r['last_name']. "-". $r['mobile'];
				$specialist = $r['sale_person_ids'];
				
				$color = $r['color'];
				if($color == "")
				{
					$color ="#000000";
				}
				if($specialist !="")
				{
					
					
					$specialist = explode(",", $specialist);
					foreach($specialist as $s)
					{
						$events.="{ id: '{$id}', resourceId: '{$s[0]}', start: '{$date}', title: '{$title}',backgroundColor: '{$color}' },";
						goto nextRec;
					}
					nextRec:
				}else{
					$events.="{ id: '{$id}', resourceId: '-1', start: '{$date}', title: '{$title}',backgroundColor: '{$color}' },";
				}
			}
			
			
			$events .="],";;
			  // events: [
		        // // { id: '1', resourceId: 'a', start: '2016-12-06', end: '2016-12-08', title: 'event 1',backgroundColor: "#0073b7",borderColor: "#0073b7"  },
		        // { id: '2', resourceId: '4', start: '2016-12-19T22:00', title: 'event 2' },
		        // { id: '3', resourceId: '5', start: '2016-12-07T12:00:00', end: '2016-12-08T06:00:00', title: 'event 3' },
		        // { id: '4', resourceId: '6', start: '2016-12-07T07:30:00', end: '2016-12-07T09:30:00', title: 'event 4' },
		        // { id: '5', resourceId: '5', start: '2016-12-07T10:00:00', end: '2016-12-07T15:00:00', title: 'event 5' }
		      // ],
			 
			// print_r($reservations);
			// die(); 
			
			$this->view->assign('events', $events);
			$this->view->assign('resources_info', $results);
			
			$params = array(
            	'session_id' => $_SESSION['sessionid'],
            );

            //LC-09/11/2013
            $response = lib::getWsResponse(API_URL, 'get_reservation_status_per_merchant', $params);
		 	$status = $response['respmsg'];
			$this->view->assign('status', $status);

			
        }
        
	  private function search()
	  {
			$params = array(
				'session_id' => $_SESSION['sessionid'],
				'mobile' => $_POST['txtMobileSearch']
			);
			
			//LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'search_customer_by_mobile', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		$resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'last_name' => $resp['last_name'],
			            'first_name' => $resp['first_name'],
			            'mobile' => $resp['mobile'],
			        );
			  }
	  }
	  
	  private function add()
	  {
	  	// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
        	'email_address' => $_POST['txtEmail']
         );

		print_r($params);
		die();

        //LC-09/11/2013
        $response = lib::getWsResponse(API_URL, 'create_reservation_via_merchant', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	  private function update()
	  {
	  	
		// session_id, id, name, description, category_id, price, cost, minutes_of_product, quantity, product_type
		$params = array(
        	'session_id' => $_SESSION['sessionid'],
        	'id' => $_POST['txtReservationId'],
        	'reservation_status_id' => $_POST['txtStatus'],
        	'reservation_date' => $_POST['txtDate'],
        	'reservation_time' => $_POST['txtTime'],
        	'branch_id' => $_POST['txtBranch'],
        	'last_name' => $_POST['txtLastName'],
        	'first_name' => $_POST['txtFirstName'],
        	'notes' => $_POST['txtNotes'],
        	'mobile' => $_POST['txtMobile'],
        	'product_id' => $_POST['txtProducts'],
        	'sale_person_id' => $_POST['txtSpecialist'],
         );
		
		// print_r($params);
		// die();
        //LC-09/11/2013
        
        $response = lib::getWsResponse(API_URL, 'update_reservation', $params);
			                   
	      if (!(isset($response['respcode'], $response['respcode']))) {
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
				
	      } elseif (!($response['respcode'] == '0000')) {
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	      } else {
	            $this->response = array(
	                'success' => true,
	                'message' => $response['respmsg'],
	            );
	      }
	  }
	  
	
		
        
        protected function delete()
        {
            $id = $_GET['id'];
            $result = $this->backend->delete_branch($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
        }
        
        
    }
?>
