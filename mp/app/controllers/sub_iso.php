<?php
	class sub_iso extends template{
		protected $response;
		public function __construct($meta) {
            parent::__construct($meta);
            $this->response = array('success' => FALSE, 'message' => 'Unknown error');
            $this->check_session();
      	}
		
		public function management()
		{
			$parent_iso_id =$_SESSION['user_info']['reference_id'];
			
			$actions = array("register", "update", "delete");
            if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
                $action = $_GET['action'];
                $this->layout = 'json';
                $this->$action();
            }
            
            $this->backend->get_iso($parent_iso_id);
            $isos = $this->backend->get_response();
            $isos = $isos['ResponseMessage'];
                        
            $countries = $this->backend->get_countries();
            $this->view->assign('countries', $countries);  
            
            $this->view->assign('isos', $isos);  
		}
		
	  protected function register()
      {
      		$parent_iso_id =$_SESSION['user_info']['reference_id'];
            $insert_data = array(
                'iso_code' => $_POST['txtISOCode'],
                'company_name' => $_POST['txtCompanyName'],
                'dba_name' => $_POST['txtDBAName'],
                'address1' => $_POST['txtAddress1'],
                'address2' => $_POST['txtAddress2'],
                'city' => $_POST['txtCity'],
                'state' => $_POST['txtState'],
                'zip' => $_POST['txtZip'],
                'country' => $_POST['txtCountry'],
                'contact_number_1' => $_POST['txtContactNumber1'],
                'contact_number_2' => $_POST['txtContactNumber2'],
                'internal_id' => $_POST['txtInternalID'],
                'account_number' => $_POST['txtAccountNumber'],
                'routing_number' => $_POST['txtRoutingNumber'],
                'udf1' => $_POST['txtUDF1'],
                'udf2' => $_POST['txtUDF2'],
                'udf3' => $_POST['txtUDF3'],
                
                'first_name' => $_POST['txtFirstName'],
                'last_name' => $_POST['txtLastName'],
                'email' => $_POST['txtEmail'],
                'contact_number_1_admin' => $_POST['txtContactNumber1Admin'],
                'contact_number_2_admin' => $_POST['txtContactNumber2Admin'],
                
                'udf1_admin' => $_POST['txtUDFAdmin1'],
                'udf2_admin' => $_POST['txtUDFAdmin2'],
                'udf3_admin' => $_POST['txtUDFAdmin3'],
                
                'username' => $_POST['txtUsername'],
                'password' => $_POST['txtPassword'],

            );
            
            

            $insert_data['parent_iso_id'] = $parent_iso_id;
        
        
            $result = $this->backend->register_iso($insert_data);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => "New ISO has been registered.",
                    );
            }
      }
      protected function update()
      {
          $insert_data = array(
                'id' => $_POST['txtISOId'],
                'iso_code' => $_POST['txtISOCode'],
                'company_name' => $_POST['txtCompanyName'],
                'dba_name' => $_POST['txtDBAName'],
                'address1' => $_POST['txtAddress1'],
                'address2' => $_POST['txtAddress2'],
                'city' => $_POST['txtCity'],
                'state' => $_POST['txtState'],
                'zip' => $_POST['txtZip'],
                'country' => $_POST['txtCountry'],
                'contact_number_1' => $_POST['txtContactNumber1'],
                'contact_number_2' => $_POST['txtContactNumber2'],
                'internal_id' => $_POST['txtInternalID'],
                'account_number' => $_POST['txtAccountNumber'],
                'routing_number' => $_POST['txtRoutingNumber'],
                'udf1' => $_POST['txtUDF1'],
                'udf2' => $_POST['txtUDF2'],
                'udf3' => $_POST['txtUDF3'],
                'first_name' => $_POST['txtFirstName'],
                'last_name' => $_POST['txtLastName'],
                'email' => $_POST['txtEmail'],
                'contact_number_1_admin' => $_POST['txtContactNumber1Admin'],
                'contact_number_2_admin' => $_POST['txtContactNumber2Admin'],
                'udf1_admin' => $_POST['txtUDFAdmin1'],
                'udf2_admin' => $_POST['txtUDFAdmin2'],
                'udf3_admin' => $_POST['txtUDFAdmin3'],
                'parent_iso_id' => $_POST['txtParentISOId']
            );
        
            $result = $this->backend->update_iso($insert_data);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => "ISO has been updated.",
                    );
            }
      }
      
      protected function delete()
      {
            $id = $_GET['id'];
            $result = $this->backend->delete_iso($id);
            $response = $this->backend->get_response();
                                                             
            if (!(isset($response['ResponseCode'], $response['ResponseMessage']))) {
                $this->response['success'] = false;
                $this->response['message'] = 'System error, unable to connect to database';
            } elseif (!($response['ResponseCode'] == '0000')) {
                $this->response['success'] = false;
                $this->response['message'] = $response['ResponseMessage'];
            } else {
                    $this->response = array(
                        'data'=> array(),
                        'success' => true,
                        'message' => $response['ResponseMessage'],
                    );
            }  
      }
      
		
	}
?>