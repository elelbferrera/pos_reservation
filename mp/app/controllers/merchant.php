<?php
  class merchant extends template {
    protected $response;
	protected $merchant_id;
    public function __construct($meta) {
        parent::__construct($meta);
        $this->response = array('success' => FALSE, 'message' => 'Unknown error');
        // $this->check_session();
        
        // print_r($meta['params'][0]);
        // die();


    }

  //   private function check_for()
  //   {
  //   	$resp_html = "";

		// $params = array(
		// 	'merchant_id' => $_POST['merchant_id'],
		// 	'specialist_id' => $_POST['specialist_id'],
		// );


  //   }

    private function get_services()
    {

		// {$item.id}
		// {$item.name}
		// {$item.price|string_format:"%.2f"}
    	$resp_html = "";

		$params = array(
			'merchant_id' => $_POST['merchant_id'],
			'specialist_id' => $_POST['specialist_id'],
		);


		if($_POST['specialist_id'] > 0)
		{
	     //LC-09/11/2013
			$response = lib::getWsResponse(API_URL, 'online_get_products_per_merchant_per_specialist', $params);
			                   
			  if (!(isset($response['respcode'], $response['respcode']))) {
			        $this->response['success'] = false;
			        $this->response['message'] = 'System error, unable to connect to database';
			  } elseif (!($response['respcode'] == '0000')) {
			        $this->response['success'] = false;
			        $this->response['message'] = $response['respmsg'];
			  } else {
			  		/*$message ="";


			  		foreach($response['respmsg'] as $r)
			  		{
			  			$id = $r['id'];
			  			$name = $r['name'];
			  			$price = "$".number_format($r['price'], 2);
 
			  			$resp_html .="<li class='col-lg-6 col-sm-6 col-xs-12'>
					       <div class='agreement'>
					        <label style='display:inline-flex;'>
					        	<input style='padding-top: 10px' type='checkbox' name='chkServices[]' id='chkServices[]' value='{$id}' onchange='check_service_change(this)' />
					        	<div style='padding: 5px;'>{$name} <span>{$price}</span></div></label>
					       </div>
					     </li>";
			  		}*/

			  		$message ="No services define";

			  	if(count($response['respmsg']> 0))
					 {
					 	$message ="";
					 }
			  		$responseN = lib::getWsResponse(API_URL, 'online_get_product_category_per_merchant_per_specialist', $params);

			  		  if (!(isset($responseN['respcode'], $responseN['respcode']))) {
					        $this->responseN['success'] = false;
					        $this->responseN['message'] = 'System error, unable to connect to database';
					  } elseif (!($responseN['respcode'] == '0000')) {
					        $this->responseN['success'] = false;
					        $this->responseN['message'] = $responseN['respmsg'];
					  } else {
					  	$message ='<ul class="nav nav-tabs" role="tablist">';
    
					  	foreach ($responseN['respmsg'] as $key => $value) {
					  		if ($key == 0) {
					  			$message .= '<li role="presentation" class="active"><a href="#cat-'.$key.'" aria-controls="cat-'.$key.'" role="tab" data-toggle="tab">'.$value['category_name'].'</a></li>';
					  		} else {
					  			$message .= '<li role="presentation"><a href="#cat-'.$key.'" aria-controls="cat-'.$key.'" role="tab" data-toggle="tab">'.$value['category_name'].'</a></li>';
					  		}

					  	}

					  	$message .='</ul><div class="tab-content">';

					  	foreach ($responseN['respmsg'] as $key => $value) {
					  		if ($key == 0) {
					  			$message .= '<div role="tabpanel" class="tab-pane active" id="cat-'.$key.'">
                                                <ul class="services-choice" name="ulServices" id="ulServices">';

					  			foreach($response['respmsg'] as $r) {
					  				if ($r['category_id'] == $value['category_id']) {
					  					$message .= '<li class="col-lg-6 col-sm-6 col-xs-12">
                                                       <div class="agreement">
                                                        <label style="display:inline-flex;">
                                                          <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="'.$r['id'].'" onchange="check_service_change(this)" />
                                                          <div style="padding: 5px;">'.$r['name'].' <span>$'.number_format($r['price'], 2).'</span></div></label>
                                                       </div>
                                                    </li>';
					  				}
					  			}
					  		} else {
					  			$message .= '<div role="tabpanel" class="tab-pane" id="cat-'.$key.'">
                                                <ul class="services-choice" name="ulServices" id="ulServices">';
                                                
					  			foreach($response['respmsg'] as $r) {
					  				if ($r['category_id'] == $value['category_id']) {
					  					$message .= '<li class="col-lg-6 col-sm-6 col-xs-12">
                                                       <div class="agreement">
                                                        <label style="display:inline-flex;">
                                                          <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="'.$r['id'].'" onchange="check_service_change(this)" />
                                                          <div style="padding: 5px;">'.$r['name'].' <span>$'.number_format($r['price'], 2).'</span></div></label>
                                                       </div>
                                                    </li>';
					  				}
					  			}
					  		}

					  		$message .= '</ul></div>';
					  	}


					  }

			  		// $resp = $response['respmsg'];
			        $this->response = array(
			            'success' => true,
			            'message' => $message/*$resp_html*/,
			        );
			  }
			}else{
				$params = array(
					'merchant_id' => $_POST['merchant_id'],
				);
				$response = lib::getWsResponse(API_URL, 'get_all_merchant_details', $params);

				// print_r($response);
				// die();
					
				if (!(isset($response['respcode'], $response['respmsg']))) {
					// $this->layout = "json";
		            $this->response['success'] = false;
		            $this->response['message'] = 'System error, unable to connect to database';
		        } elseif (!($response['respcode'] == '0000')) {
		        	// $this->layout = "json";
		            $this->response['success'] = false;
		            $this->response['message'] = $response['respmsg'];
		        }
				
				/*foreach($response['respmsg']['services'] as $r)
		  		{
		  			$id = $r['id'];
		  			$name = $r['name'];
		  			$price = "$".number_format($r['price'], 2);

		  			$resp_html .="<li class='col-lg-6 col-sm-6 col-xs-12'>
				       <div class='agreement'>
				        <label style='display:inline-flex;'>
				        	<input style='padding-top: 10px' type='checkbox' name='chkServices[]' id='chkServices[]' value='{$id}' onchange='check_service_change(this)' />
				        	<div style='padding: 5px;'>{$name} <span>{$price}</span></div></label>
				       </div>
				     </li>";
		  		}*/

		  		$message ='<ul class="nav nav-tabs" role="tablist">';

		        foreach ($response['respmsg']['service_category'] as $key => $value) {
		        	if ($key ==0) {
		        		$message .= '<li role="presentation" class="active"><a href="#cat-'.$key.'" aria-controls="cat-'.$key.'" role="tab" data-toggle="tab">'.$value['name'].'</a></li>';
		        	} else {
		        		$message .= '<li role="presentation"><a href="#cat-'.$key.'" aria-controls="cat-'.$key.'" role="tab" data-toggle="tab">'.$value['name'].'</a></li>';
		        	}
		        }
		       
		       	$message .= '</ul><div class="tab-content">';

		       	foreach ($response['respmsg']['service_category'] as $key => $value2) {
		       		if ($key == 0) { 
		       			$message .= '<div role="tabpanel" class="tab-pane active" id="cat-'.$key.'"><ul class="services-choice" name="ulServices" id="ulServices">';

		       			foreach ($response['respmsg']['services'] as $item) {
		       				if ($item['category_id'] == $value2['id']) {
		       					$message .= '<li class="col-lg-6 col-sm-6 col-xs-12">
                                                       <div class="agreement">
                                                      
                                                        <label style="display:inline-flex;">
                                                          <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="'.$item['id'].'" onchange="check_service_change(this)" />
                                                          <div style="padding: 5px;">'.$item['name'].' <span>$'.number_format($item['price'], 2).'</span></div></label>
                                                       </div>
                                                    </li>';
		       				}
		       			}

		       			$message .= '</ul>';
		       		} else {
		       			$message .= '<div role="tabpanel" class="tab-pane" id="cat-'.$key.'"><ul class="services-choice" name="ulServices" id="ulServices">';

		       			foreach ($response['respmsg']['services'] as $item) {
		       				if ($item['category_id'] == $value2['id']) {
		       					$message .= '<li class="col-lg-6 col-sm-6 col-xs-12">
                                                       <div class="agreement">
                                                      
                                                        <label style="display:inline-flex;">
                                                          <input style="padding-top: 10px" type="checkbox" name="chkServices[]" id="chkServices[]" value="'.$item['id'].'" onchange="check_service_change(this)" />
                                                          <div style="padding: 5px;">'.$item['name'].' <span>$'.number_format($item['price'], 2).'</span></div></label>
                                                       </div>
                                                    </li>';
		       				}
		       			}

		       			$message .= '</ul>';
		       		}

		       		$message .= '</div>';
		       	}
		  		$this->response = array(
		            'success' => true,
		            'message' => $message/*$resp_html*/,
		        );
			}

    }

    private function get_suggestions()
	{

		// $post_data = array(
		// 	'method' => 'get_reservation_suggestions',
		// 	'merchant_id' => 52,
		// 	'product_ids' => '3',
		// 	'specialist_ids' => '7',
		// 	'reservation_date' => '2017-02-28',
		// );

		// $post_data = array(
		// 	'method' => 'check_for_conflict',
		// 	'merchant_id' => 52,
		// 	'product_id' => '3',
		// 	'specialist_id' => '7',
		// 	'reservation_date' => '2017-02-28',
		// 	'reservation_time' => '12:00',
		// );

		$params = array(
			'merchant_id' => $_POST['merchant_id'],
			'product_ids' => $_POST['product_ids'],
			'specialist_ids' => $_POST['product_ids'],
			'reservation_date' => $_POST['date'],
		);

		// print_r(API_URL);
		// die();
		
		//LC-09/11/2013
		$response = lib::getWsResponse(API_URL, 'get_reservation_suggestions', $params);
		                   
		  if (!(isset($response['respcode'], $response['respcode']))) {
		        $this->response['success'] = false;
		        $this->response['message'] = 'System error, unable to connect to database';
		  } elseif (!($response['respcode'] == '0000')) {
		        $this->response['success'] = false;
		        $this->response['message'] = $response['respmsg'];
		  } else {
		  		$message ="";
		  		foreach($response['respmsg'] as $r)
		  		{
		  			
		  		}
		  		$resp = $response['respmsg'];
		        $this->response = array(
		            'success' => true,
		            'message' => $message,
		        );
		  }

	}

	
	
	private function save_as_favorite()
	{
		$params = array(
			'merchant_id' => $_POST['txtMerchantIdFavorite'],
			'session_id' =>  $_SESSION['session_id'],
			'favorite_id' => $_POST['txtFavoriteId'], 
		);
		if($params['favorite_id']>0)
		{	
			$response = lib::getWsResponse(API_URL, 'remove_as_favorite', $params);
		}else{
			$response = lib::getWsResponse(API_URL, 'save_as_favorite', $params);
		}
		
		// print_r($params);
		// die();
		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }else{
        	$response = $response['respmsg'];
            
            $this->response = array(
                'success' => true,
                'message' => $response,
                // 'redirect' => WEBROOT."/marketplace/in",
            );
        }
	}
	
	
	private function create_review()
	{
		
		 $params = array(
			'merchant_id' => $_POST['txtMerchantId'],
			'session_id' =>  $_SESSION['session_id'],
			'comment' => $_POST['txtReviewMessage'],
		);
		$response = lib::getWsResponse(API_URL, 'create_my_review', $params);
		
		
		// print_r($params);
		// die();
		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }else{
        	$response = $response['respmsg'];
            
            $this->response = array(
                'success' => true,
                'message' => $response,
                // 'redirect' => WEBROOT."/marketplace/in",
            );
        }
	}
	
	private function check_me_in()
	{
		 //online_check_in_for_waiting_list 
		 $params = array(
			'merchant_id' => $_POST['txtMerchantId'],
			'check_in_time' => $_POST['txtTime'],
			'last_name' => $_POST['txtLastName'],
			'first_name' => $_POST['txtFirstName'],
			'contact_number' => $_POST['txtContactNumber'],
			'create_date' => $_POST['txtDate'],
			'product_id' => $_POST['txtServices'],
			'email_address' => $_POST['txtEmailAddress']
		);

		 // print_r($params);
		 // die();
		
		$merchant_id = $_POST['txtMerchantId'];
		$param_time = array(
			'merchant_id' => $merchant_id,
		);
		$response = lib::getWsResponse(API_URL, 'get_time_zone', $param_time);
		
		date_default_timezone_set($response['respmsg']['timezone']);
		
		$time = date("H:i");
		$params['check_in_time'] = $time;
		
		$response = lib::getWsResponse(API_URL, 'online_check_in_for_waiting_list', $params);
		
		// print_r($params);
		// die();

		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }else{
        	$response = $response['respmsg'];
            
            $this->response = array(
                'success' => true,
                'message' => $response,
                'redirect' => WEBROOT."/marketplace/in",
            );
        }
	}


	
	private function create_reservation()
	{
		$params = array(
			'merchant_id' => $_POST['txtMerchantId'],
			'reservation_date' => $_POST['txtDateNew'],
			'reservation_time' => $_POST['txtTime'],
			'product_id' => $_POST['txtServices'],
			'sale_person_id' => $_POST['txtSpecialist'],
			'branch_id' => -1,
			'last_name' => $_POST['txtLastName'],
			'first_name' => $_POST['txtFirstName'],
			'notes' => $_POST['txtNotes'],
			'mobile' => $_POST['txtContactNumber'],
			'email_address' => $_POST['txtEmail'],
			'is_market' => 1
		);

		
		
		if($_POST['txtSpecialist'] !=="")
		{
			$post_data = array(
				// 'method' => 'check_for_conflict',
				'merchant_id' => $_POST['txtMerchantId'],
				'product_id' => $_POST['txtServices'],
				'reservation_date' => $_POST['txtDateNew'],
				'reservation_time' => $_POST['txtTime'],
				'specialist_id' => $_POST['txtSpecialist']
			);		

			$response = lib::getWsResponse(API_URL, 'check_for_conflict', $post_data);

			// 	print_r($post_data);
			// die();
			if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = 'System error, unable to connect to database';
	            return;
	        } elseif (!($response['respcode'] == '0000')) {
	        	// $this->layout = "json";
	            $this->response['success'] = false;
	            $this->response['message'] = $response['respmsg'];
	            return;
	        }else{
	        	

	            $response = $response['respmsg'];		
            	if($response =="Conflict")
            	{

            		$this->response['success'] = false;
			        $this->response['message'] = $response;

		            // get_reservation_suggestions
		            $post_data = array(
						'method' => 'get_reservation_suggestions',
						'merchant_id' => $_POST['txtMerchantId'],
						'product_ids' => $_POST['txtServices'],
						'specialist_ids' => $_POST['txtSpecialist'],
						'reservation_date' => $_POST['txtDateNew'],
		            );
		            $response = lib::getWsResponse(API_URL, 'get_reservation_suggestions', $post_data);
		            

		            

				    if (!(isset($response['respcode'], $response['respmsg']))) {
			            $this->response['success'] = false;
			            $this->response['message'] = 'System error, unable to connect to database';
			            return;
			        } elseif ($response['respcode'] == '0000') {
			        	// $this->layout = "json";


			            $this->response['success'] = false;
			            $this->response['message'] = "Conflict";
			            // return;
			            

			        	$suggestions = $response['respmsg'];
	        			$s_count = 0;
			        	
			        	$innerHTML="";
			        	foreach($suggestions as $s)
			        	{
			        		$s_count++;

			        		$time = $s['time'];
			        		$m_time = $s['m_time'];
			        		$service = $s['service'];
			        		$service_id = $s['service_id'];
			        		$date = $s['date'];
			        		$specialist_name = $s['first_name']." ". $s['last_name'];
			        		$specialist_id = $s['specialist_id'];

			        		$opacity ="";
			        		// $opacity = "style='display: list-item; opacity: 1'";

			        		// if($s_count >6)
			        		// {
			        		// 	$opacity ="style='display: none; opacity: 1;'";
			        		// }

			        		// onclick=ReserveMe('{$time}','{$date}','{$service_id}','{$specialist_id}'

			    			//<input type='hidden' name='txtSuggestion{$s_count}' id='txtSuggestion{$s_count}' value='{$s_count}'>
							// <input type='hidden' name='txtTime{$s_count}' id='txtTime{$s_count}' value='{$m_time}'>
							// <input type='hidden' name='txtServiceId{$s_count}' id='txtServiceId{$s_count}' value='{$service_id}'>
							// <input type='hidden' name='txtDate{$s_count}' id='txtDate{$s_count}' value='{$date}'>
							// <input type='hidden' name='txtSpecialistId{$s_count}' id='txtSpecialistId{$s_count}' value='{$specialist_id}'>

							$innerHTML .="
									<input type='hidden' name='txtData{$s_count}' id='txtData{$s_count}' value='{$time}|{$date}|{$specialist_id}|{$service_id}'>
									<li class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
								  <div class='coupon_content suggestion-li'>
								        <div class='row'>
								            <div class='col-lg-6 col-sm-6 col-xs-12'>
								                 <div class='suggestion-title'><strong>Time</strong>: {$time}</div>
								                 <div class='suggestion-title'><strong>Specialist</strong>: {$specialist_name}</div>
								                 <div class='suggestion-title'><strong>Services:</strong><br /> </div>
								                 <p>{$service}</p>
								            </div>
								            <div class='col-lg-6 col-sm-6 col-xs-8'>
								                 <button type='button' class='tabbtn btn btn-primary' id='btnReserveMySuggestion' name='btnReserveMySuggestion' style='width:100%' onClick='ReserveMe({$s_count})'><i class='fa fa-arrow-circle-right'></i> Reserve Now</button>
								            </div>
								        </div>
								  </div>
								</li>";
						}

						$this->response['suggestions'] = $innerHTML;

						// print_r($this->response);
						// die();

						return;
					}
	        	}
	        }
			
		}
		

		
		$response = lib::getWsResponse(API_URL, 'online_create_reservation', $params);
		// print_r($response);
		// die();
		
		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }else{
//         	
			// print_r($response);
		// die();
        	$response = $response['respmsg'];
            
            $this->response = array(
                'success' => true,
                'message' => $response,
                'redirect' => WEBROOT."/marketplace/in",
            );
			
        }
	}
		
	private function get_salon_specialist()
	{
		$params = array(
			'merchant_id' => $_POST['merchant_id'],
			'services' => $_POST['services'],
			'date' => $_POST['date'],
		);

		// print_r(API_URL);
		// die();
		
		//LC-09/11/2013
		$response = lib::getWsResponse(API_URL, 'online_get_salon_specialist_per_merchant_per_day', $params);
		                   
		  if (!(isset($response['respcode'], $response['respcode']))) {
		        $this->response['success'] = false;
		        $this->response['message'] = 'System error, unable to connect to database';
		  } elseif (!($response['respcode'] == '0000')) {
		        $this->response['success'] = false;
		        $this->response['message'] = $response['respmsg'];
		  } else {
		  		$message ="";
		  		foreach($response['respmsg'] as $r)
		  		{
		  			$id =$r['id'];
		  			$first_name =$r['first_name'];
		  			$last_name =$r['last_name'];
					$message .= "<li class='col-lg-6 col-sm-6 col-xs-12'>
                                                   <div class='agreement'>
                                                    <label style='display:inline-flex;'>
                                                    	<input style='padding-top: 10px' type='checkbox' name='chkSpecialist[]' id='chkSpecialist[]' value='{$id}' />
                                                    	<div style='padding: 5px;'>{$first_name}  {$last_name}</div> </label>
                                                   </div>
                                             </li>";

		  		}
		  		$resp = $response['respmsg'];
		        $this->response = array(
		            'success' => true,
		            'message' => $message,
		        );
		  }

	}



	public function reservation($var)
	{
		$actions = array("create_reservation","get_salon_specialist", "get_suggestions", "get_services", "check_contact_number", "check_if_appointment_exist");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			
        }
		
		
		if(isset($_GET)){
        	$url = $_SERVER['REQUEST_URI'];
        	$arr = explode('?', $url, 2);

        	$id = explode('/', $arr[0], 6);
        	$merchant_id = $id[5];
        	//print_r($merchant_id); die();
        }else{
				$merchant_id = $var[0];
		}
		
		$params = array(
			'merchant_id' => $merchant_id,
		);
		$response = lib::getWsResponse(API_URL, 'get_all_merchant_details', $params);
		// print_r($merchant_id); die();
			
		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }
		
		$merchant_info = $response['respmsg'];
		
		// print_r($merchant_info);
		// die();
		
		$this->view->assign("m", $merchant_info);
	}
	
	
	public function profile($var)
	{
		// print_r($var);
		// die();
		
		$actions = array("check_me_in", "create_review", "save_as_favorite");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			
        }

		$merchant_id = $var[0];
// /check_if_favorite
		if(isset($_SESSION['session_id']))
		{
				
			$params = array(
				'merchant_id' => $merchant_id,
				'session_id' => $_SESSION['session_id']
			);
			$response = lib::getWsResponse(API_URL, 'check_if_favorite', $params);
			
			if($response['respcode'] =="0000")
			{
				$this->view->assign("favorite_id", $response['respmsg']['favorite_id']);
			}else{
				$this->view->assign("favorite_id", -1);
			}
		}
		
		
		$params = array(
			'merchant_id' => $merchant_id,
		);
		$response = lib::getWsResponse(API_URL, 'get_time_zone', $params);
		
		
		
		// print_r($response);
		// die();
		date_default_timezone_set($response['respmsg']['timezone']);
		
		// print_r(date("h:i"));
		// print_r($response['respmsg']['timezone']);
		// die();
		
		$actions = array("check_me_in");
        if (isset($_GET['action']) && in_array($_GET['action'], $actions)) {
            $action = $_GET['action'];
            $this->layout = 'json';
            return $this->$action();
			
        }
		
		
		// $merchant_id = $var[0];
		// $params = array(
			// 'merchant_id' => $merchant_id,
		// );
		$response = lib::getWsResponse(API_URL, 'get_all_merchant_details', $params);
			
		if (!(isset($response['respcode'], $response['respmsg']))) {
			// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = 'System error, unable to connect to database';
        } elseif (!($response['respcode'] == '0000')) {
        	// $this->layout = "json";
            $this->response['success'] = false;
            $this->response['message'] = $response['respmsg'];
        }
		
		$merchant_info = $response['respmsg'];
		
		// print_r($merchant_info['service_category']);
		// die();
		
		// print_r($merchant_info);
		// die();
		
		$this->view->assign("m", $merchant_info);
	}
	
	private function check_contact_number(){

		
		$params = array(
			'contact_number' =>  $_POST['txtContactNumber'],
		);
		
		$response = lib::getWsResponse(API_URL, 'get_contact_number', $params);

		$this->response['success'] = $response['respmsg'];
		$this->response['message'] = $response['respmsg'];

	
	}

		private function check_if_appointment_exist() {
			// if any specialist, check only business hours & day
			$params = array(
				'merchant_id' 		=> $_POST['txtMerchantId'],
				'product_id' 		=> $_POST['txtServices'],
				'reservation_date' 	=> $_POST['txtDateNew'],
				'reservation_time' 	=> $_POST['txtTime'],
				'specialist_id' 	=> $_POST['txtSpecialist']
			);	

			if ($_POST['txtSpecialist'] == "") {
				$response = lib::getWsResponse(API_URL, 'check_if_store_open', $params);

				if (!(isset($response['respcode'], $response['respcode']))) {
				    $this->response['success'] = false;
				    $this->response['message'] = 'System error, unable to connect to database';
				} elseif (!($response['respcode'] == '0000')) {
				    $this->response['success'] = false;
				    $this->response['message'] = $response['respmsg'];
				} else {
					$response = $response['respmsg'];
		        	if ($response == "Closedate") {
		        		$this->response['success'] = false;
				        $this->response['message'] = "Sorry, we are closed at this date and time.";
				        $this->response['action']  = $response;
				        return;
		        	} else {
		        		$this->response['success'] = true;
		        		$this->response['message'] = "Date & Time available.";
		        	}
				}
			} 
			// if specific specialist is selected
			else {
				$response = lib::getWsResponse(API_URL, 'check_for_conflict_two', $params);
				
				if (!(isset($response['respcode'], $response['respcode']))) {
				    $this->response['success'] = false;
				    $this->response['message'] = 'System error, unable to connect to database';
				} elseif (!($response['respcode'] == '0000')) {
				    $this->response['success'] = false;
				    $this->response['message'] = $response['respmsg'];
				} else {
					$response = $response['respmsg'];
					
					// check business hours
		        	if ($response == "Closedate") {
		        		$this->response['success'] = false;
				        $this->response['message'] = "Sorry, we are closed at this date and time.";
				        $this->response['action']  = $response;
				        return;
		        	} 

		        	// check day off of specialist, show suggestions
		        	else if ($response == 'SpecialistUnavailable') {
		        		$this->response['success'] = false;
				        $this->response['message'] = "Specialist is unavailable.";
				        $this->response['action']  = $response;
		        	} 
					// check time of specialist selected
		        	else if ($response == 'SpecialistUnavailableTime') {
		        		$this->response['success'] = false;
				        $this->response['message'] = "Specialist is unavailable on your selected time.";
				        $this->response['action']  = $response;
		        	} 

		        	// check availability of specialist based on reservations
		        	else if ($response == 'ReservationConflict') {
		        		$this->response['success'] = false;
				        $this->response['message'] = "Time is unavailable. Please check suggested specialists.";
				        $this->response['action']  = $response;
		        	} 

		        	else if ($response == 'NoConflict') {
		        		$this->response['success'] = true;
		        		$this->response['message'] = "No Conflict";
		        	}

					// check covered service of specialist selected

					$post_data = array(
						'method' 			=> 'get_specialist_suggestions',
						'merchant_id' 		=> $_POST['txtMerchantId'],
						'product_ids' 		=> $_POST['txtServices'],
						'specialist_ids' 	=> $_POST['txtSpecialist'],
						'reservation_date' 	=> $_POST['txtDateNew'],
						'reservation_time' 	=> $_POST['txtTime']
		            );

					// show suggestions
					$response = lib::getWsResponse(API_URL, 'get_specialist_suggestions', $post_data);
					
				    if (!(isset($response['respcode'], $response['respmsg']))) {
			            $this->response['success'] = false;
			            $this->response['message'] = 'System error, unable to connect to database';
			            return;
			        } else if ($response['respcode'] == '0000') {
			        	$suggestions = $response['respmsg'];
			        	$innerHTML="";


			        	if (count($suggestions) == 0) {
			        		$innerHTML .="
								<li class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
							  <div class='coupon_content suggestion-li'>
							        <div class='row'>
							            <div class='col-lg-6 col-sm-6 col-xs-12'>
							                 <div class='suggestion-title'><strong>Specialist</strong>: Any specialist</div>
							            </div>
							            <div class='col-lg-6 col-sm-6 col-xs-8'>
							                 <button type='button' class='tabbtn btn btn-primary' name='' style='width:100%' onclick='select_specialist(-1)'><i class='fa fa-arrow-circle-right'></i> Select specialist</button>
							            </div>
							        </div>
							  </div>
							</li>";
			        	} else {
				        	foreach($suggestions as $s) {
				        		$specialist_name = $s['first_name'] . ' ' . $s['last_name'];
								$specialist_id = $s['id'];

								$innerHTML .="
										<li class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
									  <div class='coupon_content suggestion-li'>
									        <div class='row'>
									            <div class='col-lg-6 col-sm-6 col-xs-12'>
									                 <div class='suggestion-title'><strong>Specialist</strong>: {$specialist_name}</div>
									            </div>
									            <div class='col-lg-6 col-sm-6 col-xs-8'>
									                 <button type='button' class='tabbtn btn btn-primary' name='' style='width:100%' onclick='select_specialist({$s['id']})'><i class='fa fa-arrow-circle-right'></i> Select specialist</button>
									            </div>
									        </div>
									  </div>
									</li>";
				        	}
				        }

				        $this->response['suggestions'] = $innerHTML;
				        return;
			        }
				}
			}
		}
    
  }
?>
