<?php
class backend {
    protected $response;
    protected $db;
    public $salt;

    public function __construct(&$db) {
        $this->salt = PROGRAM;  
        $this->salt2 = '2016'.PROGRAM.'2016';
        $this->db = $db;
        require_once 'xorry.php';
        require_once 'class.phpmailer.php';
        require_once dirname(__DIR__) . '/libraries/HideIt.php';
        $this->hideit = new HideIt('aes-256-cbc');
		error_reporting(E_ALL);
    }
	
	public function get_available_products()
	{
		$cmd ="SELECT * FROM products WHERE status='A'";
		
		return $this->db->fetchAll($cmd);
	}
	
	
	public function get_product_order_info($order_id)
	{
		$cmd =" SELECT * FROM product_order WHERE id={$order_id}";
		
		return $this->db->fetchRow($cmd);
	}
	
	//order_id, status, create_by, signature
	public function update_signature($params)
	{
		//product_order - update
		
		$cmd ="SELECT * FROM product_order WHERE id={$params['order_id']}";
		
		$res = $this->db->fetchRow($cmd);
		
		$this->db->begin_transaction();	
		$update_data = array(
			'status' => $params['status'],
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $params['create_by'],
			'signature' => $params['signature'],
		);
		if (!$this->db->update('product_order', $update_data, 'id='. $params['order_id'])) {
			$this->db->rollback_transaction();	
            return $this->failed('Unable to update status');
        }
		
		//'update_date' => date('Y-m-d H:i:s'),
		
		//product_order_comment - insert
			$insert = array(
				'product_order_id' => $params['order_id'],
				'product_id' => $res['product_id'],
				'partner_id' => $res['partner_id'],
				'comment' => 'Order has been updated to ' .$params['status'],
				'create_by' => $params['create_by'],
				'is_public' => 1,
				'parent_id' =>-1,
				'user_id' => 1,
			);
		if(!$this->db->insert('product_order_comment', $insert)) {
			$this->db->rollback_transaction();	
            return $this->failed("Unable to update status");
        }		
		//product_order_history - insert
		$insert = array(
			'product_order_id' => $params['order_id'],
			'status' => $params['status'],
			'create_by' => $params['create_by'],
		);
		
		$this->db->commit_transaction();
		
		return $this->success('Product has been updated.');
	}
	
	//params: fname, lname, email_address, mobile_number, address, username, user_type
    public function register_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['fname'], $params['lname'], $params['email_address'], $params['username'], $params['user_type'], $params['created_by']))) {
            return $this->failed('Missing required fields');
            die();
        } elseif (($params['fname'] == '' && $params['lname'] == '' && $params['email_address'] == '' && $params['username'] == '' && $params['user_type'] == '' && $params['created_by'] == '')) {
            return $this->failed('Missing required fields');
            die();
        }
         elseif (!strstr($params['email_address'], '@')) {
             return $this->failed('Invalid email address');
             die();
        }
       /* elseif (!($this->get_userid_by_email($params['email_address'])==-1)) {
            return $this->failed('Email has already been used.');
            die();
       
        }*/
        else {
            if ($this->get_user_details($params)) {

                return $this->failed('Username already exist');
                die();
            } else {
                
     
                $insert_data = array(
                	'status' => 'A',
                    'first_name'         => $params['fname'],
                    'last_name'         => $params['lname'],
                    'email_address' => $params['email_address'],
                    'username'      => $params['username'],
                    'user_type_id'     => $params['user_type'],
                    'create_by'    => $params['created_by'],
                    'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))
                    //'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))),  
                );
               
               
                if (!$this->db->insert('users', $insert_data)) {
                    return $this->failed('Unable to register new user');
                } else {
                    $response = array(
                        'message' => 'Successfully registered new user record',
                    );
                    
                    return $this->success($response);
                }
            }
        }
    }
    
    public function get_user_details($params) {
        $params = array_map('trim', $params);
        
        if (!(isset($params['username']) && trim($params['username']) != '')) {
         
            return $this->failed('Missing required parameter');
        } else {
            $row = $this->db->fetchRow("select * from users where status = 'A'  and  username=" . $this->db->db_escape($params['username']));
            if (!(isset($row['id']))) {
                return $this->failed('User account not found');
            } else {
                 
                return $this->success($row);
            }
        }
    }
	
	public function get_users() {
        $query = "select CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,first_name as fname,last_name as lname,username,u.email_address,user_type_id,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type_id where u.status='A'";
    
		$query .=" AND ut.description ='SUPER ADMIN'";
			   
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
	
	
	
	//order_id, status, update_by
	public function update_product_order($params)
	{
		//product_order - update
		
		$cmd ="SELECT * FROM product_order WHERE id={$params['order_id']}";
		
		$res = $this->db->fetchRow($cmd);
		
		$this->db->begin_transaction();	
		$update_data = array(
			'status' => $params['status'],
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $params['create_by'],
		);
		if (!$this->db->update('product_order', $update_data, 'id='. $params['order_id'])) {
			$this->db->rollback_transaction();	
            return $this->failed('Unable to update status');
        }
		
		//'update_date' => date('Y-m-d H:i:s'),
		
		//product_order_comment - insert
			$insert = array(
				'product_order_id' => $params['order_id'],
				'product_id' => $res['product_id'],
				'partner_id' => $res['partner_id'],
				'comment' => 'Order has been updated to ' .$params['status'],
				'create_by' => $params['create_by'],
				'is_public' => 1,
				'parent_id' =>-1,
				'user_id' => $params['user_id']
			);
		if(!$this->db->insert('product_order_comment', $insert)) {
			$this->db->rollback_transaction();	
            return $this->failed("Unable to update status");
        }		
		//product_order_history - insert
		$insert = array(
			'product_order_id' => $params['order_id'],
			'status' => $params['status'],
			'create_by' => $params['create_by'],
		);
		
		$this->db->commit_transaction();
		
		return $this->success('Product has been updated.');
	}
	
	public function get_statuses(){
		$cmd = "SELECT id,status FROM order_status";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//order_id, product_id, partner_id, comment, parent_id, create_by, user_id, is_public, attachment
	public function create_comment($params)
	{
		$insert_data = array(
			'product_order_id' => $params['order_id'],
			'product_id' => $params['product_id'],
			'partner_id' => $params['partner_id'],
			'comment' => $params['comment'],
			'parent_id' => $params['parent_id'],
			'create_by' => $params['create_by'],
			'user_id' => $params['user_id'],
			'attachment' => $params['attachment'],
			'is_internal' => $params['is_internal'],
		);
		if(!$this->db->insert('product_order_comment', $insert_data)) {
            return $this->failed("Unable to add comment");
        } else {
            //get user type id
            // $id=$this->db->lastInsertId(); 
            return $this->success('Comment has been posted.');
        }
	}
	
	//order_id, product_id, partner_id, comment, parent_id, create_by, user_id, is_public, attachment
	public function create_sub_comment($params)
	{
		$insert_data = array(
			'product_order_id' => $params['order_id'],
			'product_id' => $params['product_id'],
			'partner_id' => $params['partner_id'],
			'comment' => $params['comment'],
			'parent_id' => $params['parent_id'],
			'create_by' => $params['create_by'],
			'user_id' => $params['user_id'],
			'attachment' => $params['attachment']
		);
		if(!$this->db->insert('product_order_comment', $insert_data)) {
            return $this->failed("Unable to add comment");
        } else {
            //get user type id
            // $id=$this->db->lastInsertId(); 
            return $this->success('Comment has been posted.');
        }
	}
	
	public function get_comments($parent_id, $include_public= 0)
	{
		$cmd="SELECT product_order_id, pc.comment,pc.create_date, u.last_name, u.first_name,pc.parent_id FROM product_order_comment pc
			LEFT JOIN users u ON pc.user_id=u.id
			
			WHERE product_order_id IN(
			SELECT po.id FROM product_order po 
			LEFT JOIN partners p on po.partner_id=p.id  
			WHERE parent_id={$parent_id}
			)
			AND is_public = {$include_public}
			ORDER BY product_order_id, parent_id";
		
			$rs =$this->db->fetchAll($cmd);
			return $this->success($rs);
	}
	
	
	
	
	public function get_comment($order_id, $include_internal = 0)
	{
		$cmd="SELECT pc.id as comment_id, product_order_id, pc.comment,pc.create_date, u.last_name, u.first_name,pc.parent_id FROM product_order_comment pc
			LEFT JOIN users u ON pc.user_id=u.id
			WHERE product_order_id ={$order_id}
			AND parent_id=-1";
			
			
		
		$rs =$this->db->fetchAll($cmd);
		
		if($include_internal == 0)
		{
			$cmd .=" AND is_internal = {$include_internal}
			ORDER BY create_date DESC, parent_id";
		}else{
			$cmd .="
			ORDER BY create_date DESC, parent_id";
		}
		
		$rs =$this->db->fetchAll($cmd);
		
		
		$results = array();
		foreach($rs as $r)
		{
			$parent_id = $r['comment_id'];
			$r['sub_comments'] = $this->get_sub_comment($parent_id, $order_id);
			
			$results[] = $r;
		}
		
		
		return $results;
	}
	
	public function get_sub_comment($parent_id, $order_id)
	{
		$cmd="SELECT product_order_id, pc.comment,pc.create_date, u.last_name, u.first_name,pc.parent_id FROM product_order_comment pc
			LEFT JOIN users u ON pc.user_id=u.id
			WHERE product_order_id ={$order_id}
			 AND parent_id={$parent_id}
			ORDER BY create_date DESC";
		$rs =$this->db->fetchAll($cmd);
		return $rs;
	}
	
	
	
	
	public function get_applications($parent_id, $include_internal =0)
	{
		$cmd ="SELECT po.create_date, p.id as product_id, p.name, po.status, po.id as order_id,
		pc.company_name, po.partner_id, parent.last_name, parent.first_name, ps.parent_id
 		from product_order po 
		INNER JOIN products p on p.id=po.product_id 
		INNER JOIN partner_company pc ON po.partner_id=pc.partner_id
		INNER JOIN partners ps ON  pc.partner_id=ps.id
		INNER JOIN partner_contact parent ON ps.parent_id = parent.partner_id
		 
		";
		if($include_internal == 0)
		{
			$cmd .="WHERE ps.parent_id={$parent_id}";
		}
		$rs =$this->db->fetchAll($cmd);
		
		
		$results = array();
		foreach($rs as $r)
		{
			$order_id = $r['order_id'];
			$r['comments'] = $this->get_comment($order_id, $include_internal);
			$results[] = $r;
		}
		
		// print_r($results);
		// die();
		
		return $this->success($results);
	}
	
	
	
	
	public function get_application_information($partner_id)
	{
		$cmd ="SELECT po.create_date, p.id as product_id, p.name, po.status, po.id as order_id 
		,pc.company_name
		from product_order po 
		LEFT JOIN products p on p.id=po.product_id
		LEFT JOIN partner_company pc ON po.partner_id=pc.partner_id
		WHERE po.partner_id={$partner_id} 
		";
		
		
		// print_r($cmd);
		// die();
		$rs =$this->db->fetchAll($cmd);
		
		return $this->success($rs);
	}
	
	public function get_product_fee($order_id)
	{
		
		//$row = $this->db->fetchAll("SELECT * FROM merchant_reward_info WHERE order_id={$order_id} AND product_name IN('MONTHLY FEE', 'OTHER FEE')");
		$row = $this->db->fetchAll("SELECT * FROM merchant_reward_info WHERE order_id={$order_id} AND product_name NOT IN('PUNCHCARD', 'PROMOTION','LOYALTY')");
		return $row;
		
	}
	
	public function get_product_order_status($order_id)
	{
		$row = $this->db->fetchRow("SELECT * FROM product_order WHERE id={$order_id}");
		return $row;
	}
	
	
	//create_by, status, order_id
	public function update_order_status($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'update_by' => $params['create_by'],
			'update_date' => date('Y-m-d H:i:s'),
			'status' => $params['status']
		);
		if (!$this->db->update('product_order', $insert_data, 'id='. $params['order_id'])) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		$product_order_id = $params['order_id'];
		//product_order_history
		$insert_data = array(
			'product_order_id' => $product_order_id,
			'status' => $params['status'],
			'create_by' => $params['create_by'],
		);
		
		if (!$this->db->insert('product_order_history', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		$this->db->commit_transaction();
		
		return $this->success("Product has been updated");
	}

	
	public function get_partner_attachment($partner_id)
	{
		$cmd ="SELECT name, document_image FROM partner_attachment  WHERE partner_id={$partner_id}";
		
		return $this->db->fetchAll($cmd);
	}
	
	public function get_application_history($partner_id)
	{
		$cmd ="SELECT ph.create_date, p.id as product_id, p.name, ph.status, po.id as order_id FROM product_order_history ph
LEFT JOIN product_order po ON po.id=ph.product_order_id
LEFT JOIN products p on p.id=po.product_id
		WHERE po.partner_id={$partner_id}
		";
		
		$rs =$this->db->fetchAll($cmd);
		
		return $this->success($rs);
	}
	
	
	public function get_all_merchants($parent_id=-1)
	{
		$cmd ="SELECT pc.*,partner_id_reference as merchant_id, p.merchant_processor  
		as processor
		FROM partners p
		INNER JOIN partner_company pc ON p.id=pc.partner_id
		
		WHERE partner_type_id=3 "; //partner_type_id for merchant
		if($parent_id > 0)
		{
			$cmd .="  AND p.parent_id=" . $parent_id;
		}
		
		$cmd .=" ORDER BY p.create_date DESC";
		$records  = $this->db->fetchAll($cmd);
		
		return $this->success($records);
	}
	
	
	//partner_type_id, email, merchant_id, logo, merchant_processor, 
	//company_name, address1, address2, city state, zip, country, phone1, phone2,
	//create_by, document_image,
	//reward_info = array()
	public function create_merchant($params)
	{
		//$params = array_map('trim', $params);
		$this->db->begin_transaction();	
		
		
		$phone_prefix  = PHONEPREFIX;		
		
		if($this->check_if_profile_exist("partner_company", "email", $params['email'],-1, "", "", false, "partner_"))
		{
			return $this->failed("Email company has already been use.");
		}
// 		
		// print_r($params['email']);
// die();
		
		if($this->check_if_profile_exist("partners", "partner_id_reference", $params['merchant_id'],-1, "partner_type_id", $params['partner_type_id'], false)>0)
		{
			return $this->failed("Merchant Id already been registered.");
		}
		
		if($this->check_if_profile_exist("partner_contact", "email", $params['contact_email_address'],-1, "", "", false, "partner_"))
		{
			return $this->failed("Email Contact has already been use.");
		}
		
		if($this->check_if_profile_exist("partner_contact", "mobile_number", $phone_prefix.$params['mobile_number'],-1, "", "", false, "partner_"))
		{
			return $this->failed("Mobile Number has already been use.");
		}
		
		
		
		
		// print_r($this->check_if_profile_exist("partners", "partner_id_reference", $params['merchant_id'],-1, "partner_type_id", $params['partner_type_id'], false));
		// die();
		
		
		//partners
		$insert_data = array(
			'create_by' => $params['create_by'],
			'status' => 'A',
			'partner_type_id' => $params['partner_type_id'],
			'parent_id' => isset($params['partner_id']) ? $params['partner_id'] : -1 ,
			'logo' => $params['logo'] == "" ? "" : $params['logo'],
			'partner_id_reference' => $params['merchant_id'],
			'merchant_processor' => $params['merchant_processor'],
		);
		

		
		
		if (!$this->db->insert('partners', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		//merchant company information
		$partner_id = $this->db->lastInsertId();
		$insert_data = array(
			'partner_id' => $partner_id,
			'company_name' => $params['company_name'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country' => $params['country'],
			'email' => $params['email'],
			'phone1' => $phone_prefix.$params['phone1'],
			'phone2' => $phone_prefix.$params['phone2'],
			'dba' => $params['dba'],
			'fax' => $params['fax'],
		);
		if (!$this->db->insert('partner_company', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		//last_name, first_name, position, mobile_number, office_number, extension, contact_email_address
		$insert_data = array(
			'partner_id' => $partner_id,
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email' => $params['contact_email_address'],
			'mobile_number' => $phone_prefix.$params['mobile_number'],
			'other_number' => $phone_prefix.$params['office_number'],
			'other_number_2' => $phone_prefix.$params['office_number2'],
			'position' => $params['position'],
			'extension' => $params['extension'],
			'fax' => $params['contact_fax'],
			'website' => $params['website'],
		);
		
		
		if (!$this->db->insert('partner_contact', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		
		
		// $insert_data = array(
			// 'partner_id' => $partner_id,
			// 'status' =>'NEW',
			// 'create_by' => $params['create_by'],
		// );
		// //merchant_application
		// if (!$this->db->insert('merchant_application', $insert_data)) {
			// $this->db->rollback_transaction();
            // return $this->failed('Unable to create merchant');
        // }
		// $merchant_application_id = $this->db->lastInsertId();
		//merchant_application_status
		
		//product_order
		$insert_data = array(
			'product_id' => $params['product_id'], //GIFT AND REWARDS
			'partner_id' => $partner_id,
			'quantity' => 1,
			'amount' => 0,
			'create_by' => $params['create_by'],
			'status' => 'Pending'
		);
		if (!$this->db->insert('product_order', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		$product_order_id = $this->db->lastInsertId();
		//product_order_history
		$insert_data = array(
			'product_order_id' => $product_order_id,
			'status' => 'Pending',
			'create_by' => $params['create_by'],
		);
		
		if (!$this->db->insert('product_order_history', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		

		//partner_attachment for  Void Check
		$insert_data = array(
			'partner_id' => $partner_id,
			'name' => 'VOID CHECK',
			'document_image' => $params['document_image'],
			'create_by' => $params['create_by'],
			'status' => 'A'
		);
		if (!$this->db->insert('partner_attachment', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		
		//partner_attachment for logo
		$insert_data = array(
			'partner_id' => $partner_id,
			'name' => 'LOGO',
			'document_image' => $params['logo'],
			'create_by' => $params['create_by'],
			'status' => 'A'
		);
		if (!$this->db->insert('partner_attachment', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create merchant');
        }
		
		//merchant_reward_info
		foreach($params['reward_info'] as $r)
		{
			$insert_data = array(
				'product_name' => $r['type'],
				'partner_id' =>$partner_id,
				'order_id' => $product_order_id,
				'product_information' => $r['information'],
				'create_by' => $params['create_by'],
			);
			if (!$this->db->insert('merchant_reward_info', $insert_data)) {
				$this->db->rollback_transaction();
            	return $this->failed('Unable to create merchant');
            }
		}
		
		
		//partner_payment_method
		$insert_data = array(
			'partner_id' => $partner_id,
			'payment_type' => 'BANK',
			'account_number' => $params['account_number'],
			'routing_number' => $params['routing_number'],
			'bank_name' => $params['bank_name'],
			'account_name'=> $params['account_name'],
			'status' => 'A',
			'create_by' => $params['create_by'],
		);
		if (!$this->db->insert('partner_payment_method', $insert_data)) {
				$this->db->rollback_transaction();
            	return $this->failed('Unable to create merchant');
        }
		
		//partner_mailing_address
		$insert_data = array(
			'partner_id' => $partner_id,
			'country' => $params['business_country'],
			'address' => $params['business_address'],
			'city' => $params['business_city'],
			'state' => $params['business_state'],
			'zip' => $params['business_zip'],
			'create_by' => $params['create_by'],
		);
		if (!$this->db->insert('partner_mailing_address', $insert_data)) {
				$this->db->rollback_transaction();
            	return $this->failed('Unable to create merchant');
        }
		
		
		$this->db->commit_transaction();
		return $this->success("Merchant has been created.");
	}
	
	public function update_agent($params)
	{
		if($this->check_if_profile_exist("partner_contact", "email", $params['email'], $params['id'], "", "", false, "partner_"))
		{
			return $this->failed("Email has already been used.");
		}
		
		if($this->check_if_profile_exist("partners", "partner_id_reference", $params['agent_id'], $params['id'], "partner_type_id", $params['partner_type_id'], false))
		{
			return $this->failed("Agent Id already been used.");
		}
		
		$this->db->begin_transaction();	
		//partners
		$date_time = date('Y-m-d H:i:s');
		
		$update_data = array(
			'update_date' => $date_time,
			'update_by' => $params['update_by'],
			'status' => 'A',
			'partner_type_id' => $params['partner_type_id'],
			'parent_id' => -1,
			'logo' => $params['logo'] == "" ? "" : $params['logo'],
			'partner_id_reference' => $params['agent_id'],
			'agent_partner' => $params['agent_partner'],
		);
		
		
		if (!$this->db->update('partners', $update_data, 'id=' . $params['id'])) {
	        $this->db->rollback_transaction();  
            //$this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to update agent.");
	    }
		
		// 'partner_id' => $partner_id,
		//partner_company
		$update_data = array(
			'update_date' => $date_time,
			'update_by' => $params['update_by'],
			'first_name' => $params['first_name'],
			'last_name' => $params['last_name'],
			'email' => $params['email'],
			'mobile_number' => $params['mobile_number'],
			'other_number' => $params['other_number'],
			'position' => "",
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country' => $params['country'],
		);
		if (!$this->db->update('partner_contact', $update_data, 'partner_id=' . $params['id'])) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to update agent 1');
        }
		$this->db->commit_transaction();
		
		$user_info = array(
			'update_date' => $date_time,
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email'],
			'username' => $params['agent_id']
		);
		if (!$this->db->update('users', $user_info, 'user_type_id=6 AND reference_id=' .$params['id'])) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to update agent 2');
        }
		
		// $data = array(
			// 'message' => "Partner has been updated.",
		// );
		return $this->success("Agent has been updated.");
     }
	
	
	
	
	public function get_agent_information($id)
	{
		$cmd ="SELECT p.*, pc.*
		 FROM partners p INNER JOIN partner_contact pc ON p.id=pc.partner_id WHERE p.status='A' AND p.partner_type_id=1 AND p.id={$id}";
		 
		$record  = $this->db->fetchRow($cmd);
		return $this->success($record);
	}
	
	public function get_all_agents()
	{
		$cmd ="SELECT p.id,agent_partner, partner_id_reference, 
		pc.first_name, pc.last_name, pc.email, pc.mobile_number 
		 FROM partners p INNER JOIN partner_contact pc ON p.id=pc.partner_id WHERE p.status='A' AND p.partner_type_id=1";
		 
		$records  = $this->db->fetchAll($cmd);
		
		return $this->success($records);
	}
	
	public function send_mail_notification($body,$email_address,$subject='', $attachment ="")
    {
    	// mail("lemmuelcabuhat@gmail.com", "Hello", "TESTING");
		// die();		
			
    	//date_default_timezone_set('America/Toronto');
    	
    	
    	//$file = $path.$filename;
// $content = file_get_contents( $attachment);
// $content = chunk_split(base64_encode($content));
// $uid = md5(uniqid(time()));
// $name = basename($attachment);
// 
// // header
// $header = "From: <".EMAIL_USERNAME.">\r\n";
// $header .= "Reply-To: ".EMAIL_USERNAME."\r\n";
// $header .= "MIME-Version: 1.0\r\n";
// $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
// 
// // message & attachment
// $nmessage = "--".$uid."\r\n";
// $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
// $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
// $nmessage .= $body."\r\n\r\n";
// $nmessage .= "--".$uid."\r\n"; 
		// if($attachment !="")
	   // {	 
			// // $mail->addAttachment($attachment);
// 				   
			// $nmessage .= "Content-Type: application/octet-stream; name=\"".$attachment."\"\r\n";
			// $nmessage .= "Content-Transfer-Encoding: base64\r\n";
// 			       
			// $nmessage .= "Content-Disposition: attachment; filename=\"".$attachment."\"\r\n\r\n";
	   // }
// $nmessage .= $content."\r\n\r\n";
// $nmessage .= "--".$uid."--";
// 
// 
// 
// if (mail($email_address, $subject, $nmessage, $header)) {
    	// print_r("true...");
// die();
    // die();	
    // return true; // Or do something here
// } else {
	// print_r("false...");
	// die();
  // return false;
// }
    	
        
        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
        // $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        // if(!IS_PRODUCTION)
		// {
		 	$mail->SMTPAuth   = true;                  // enable SMTP authentication
        	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        // }
        
        // print_r(HOST.PORT.EMAIL_USERNAME.EMAIL_PASSWORD);
        // die();
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');       
        $mail->From = 'admin@goetu.com';
        $mail->FromName = 'Go3 Admin';
       if($attachment !="")
	   {	 
	   		$mail->addAttachment($attachment);
	   }
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             //$mail->addAddress($ad);
             $mail->AddAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
         
        if(!$mail->Send()) {
        	// print_r($mail->ErrorInfo());
        	// die();
            return false;//
        } else {
        	// die();
            return true;// $this->success("Message sent!");
        } 
    }
	
	
	private function check_if_profile_exist($table, $field, $value, $id = -1, $other_field ="", $other_field_value =-1, $include_status = true, $id_prefix ="")
    {
    	
		
        $value =strtoupper($value);
        $cmd ="SELECT id FROM {$table} WHERE ucase({$field})= {$this->db->db_escape($value)} AND {$id_prefix}id<>{$id} ";
		
		if($other_field !="")
		{
			$cmd .=" AND {$other_field}={$other_field_value}";
		}
		
		if($include_status)
		{
			$cmd .=" AND status='A'";
		} 
		
		// print_r($cmd);die();
		
		  
        $row = $this->db->fetchRow($cmd);
        if (!(isset($row['id']))) {
            return false;
        } else { 
            return true;
        }
    }
	
	//username , password, last_name, first_name, email_address, user_type_id, reference_id
    public function create_user($insert_data)
    {
        //$insert_data = array_map('trim', $insert_data);
        //customer 3
        if(in_array($insert_data['user_type_id'], unserialize(CUSTOMER_USER_TYPE_ID)))
        {
            $insert_data['is_customer'] =1;
        }
        //agent 6
        if(in_array($insert_data['user_type_id'], unserialize(AGENT_USER_TYPE_ID)))
        {
            $insert_data['is_agent'] =1;
        }
        //merchant admin 2, 8 merchant, 7 branch merchant admin
        if(in_array($insert_data['user_type_id'], unserialize(MERCHANT_USER_TYPE_ID)))
        {
            $insert_data['is_merchant'] =1;
        }
        //iso_user_type 4, 5
        if(in_array($insert_data['user_type_id'], unserialize(ISO_USER_TYPE_ID)))
        {
            $insert_data['is_iso'] = 1;
        }   
        //super admin 1, 9 admin
        if(in_array($insert_data['user_type_id'], unserialize(ADMIN_USER_TYPE_ID)))
        {
            $insert_data['is_admin'] = 1;
        }
        
        $insert_data['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$insert_data['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        
		
		$insert = array(
			'username' => $insert_data['username'],
			'password' => $insert_data['password'],
			'last_name' => $insert_data['last_name'],
			'first_name' => $insert_data['first_name'],
			'email_address' => $insert_data['email_address'],
			'user_type_id' => $insert_data['user_type_id'],
			'reference_id' => $insert_data['reference_id'],
			'is_agent' => $insert_data['is_agent'],
			'status' => $insert_data['status'],
		);
		
		
        if (!$this->db->insert('users', $insert)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
            die();
        }                                                                        
    }

	
	
	
	
	//email, last_name, first_name, agent_partner, agent_id, mobile_number, other_number,
	//address1, address2, city, state, zip, country, partner_type_id
	public function create_agent($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();	
		
		$phone_prefix  = PHONEPREFIX;	
		if($this->check_if_profile_exist("partner_contact", "email", $params['email'],-1, "", "", false, "partner_"))
		{
			return $this->failed("Email has already been registered.");
		}
		
		if($this->check_if_profile_exist("partners", "partner_id_reference", $params['agent_id'],-1, "partner_type_id", $params['partner_type_id'], false))
		{
			return $this->failed("Agent Id already been registered.");
		}
		
		//partners
		$insert_data = array(
			'create_by' => $params['create_by'],
			'status' => 'A',
			'partner_type_id' => $params['partner_type_id'],
			'parent_id' => -1,
			'logo' => $params['logo'] == "" ? "" : $params['logo'],
			'partner_id_reference' => $params['agent_id'],
			'agent_partner' => $params['agent_partner'],
		);
		if (!$this->db->insert('partners', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create agent');
        }
		//agent contact information
		$partner_id = $this->db->lastInsertId();
		$insert_data = array(
			'partner_id' => $partner_id,
			'first_name' => $params['first_name'],
			'last_name' => $params['last_name'],
			'email' => $params['email'],
			'position' => "",
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country' => $params['country'],
			
			'ssn' => $params['ssn'],
			'dob' => $params['dob'],
			'mobile_number' => $phone_prefix.$params['mobile_number'],
			'other_number' => $phone_prefix.$params['other_number'],
			'other_number_2' => $phone_prefix.$params['other_number_2'],
			'email2' => $params['email2'],
			'fax' => $params['fax'],
			'website' => $params['website'],
			
		);
		if (!$this->db->insert('partner_contact', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create agent');
        }
		
		//partner_payment_method
		$insert_data = array(
			'partner_id' => $partner_id,
			'payment_type' => 'BANK',
			'account_number' => $params['account_number'],
			'routing_number' => $params['routing_number'],
			'bank_name' => $params['bank_name'],
			'account_name'=> $params['account_name'],
			'status' => 'A',
			'create_by' => $params['create_by'],
		);
		if (!$this->db->insert('partner_payment_method', $insert_data)) {
				$this->db->rollback_transaction();
            	return $this->failed('Unable to create merchant');
        }
		
		//partner_mailing_address
		$insert_data = array(
			'partner_id' => $partner_id,
			'country' => $params['business_country'],
			'address' => $params['business_address'],
			'city' => $params['business_city'],
			'state' => $params['business_state'],
			'zip' => $params['business_zip'],
			'create_by' => $params['create_by'],
		);
		if (!$this->db->insert('partner_mailing_address', $insert_data)) {
				$this->db->rollback_transaction();
            	return $this->failed('Unable to create merchant');
        }
		
		
		//insert to users
		//user_type for agent is 6
		
		//username , password, last_name, first_name, email_address, user_type_id, reference_id
		$default_password = rand(1111111, 99999999);
		$insert_data = array(
			'username' => $params['agent_id'],
			'password' => $default_password,
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email'],
			'user_type_id' => 6,
			'reference_id' => $partner_id,
		);
		
		//this method already contains a rollback transaction if it failed.
		$this->create_user($insert_data);
		
		//email the username and password to the agent.
		
		$email_address = $params['email'];
		$this->send_mail_notification("Your agent account has been created Username: {$params['agent_id']} Password: {$default_password}.", $email_address, "Agent Account Creation");
		
		$this->db->commit_transaction();
		return $this->success("Agent has been created.");
	}
	
	// public function get_all_merchants($parent_id, $is_agent)
    // {
        // $cmd = "SELECT m.*, b.name as business_type,u.username, u.password FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
        // LEFT JOIN users u on u.reference_id=m.id AND u.is_merchant=1  
        // WHERE m.status='A'";
//         
		// if($is_agent)
		// {
			// $cmd.=" AND m.agent_id= {$parent_id}";
		// }else{
			// $cmd.=" AND m.iso_id= {$parent_id}";
		// }
// 		        
        // $result = $this->db->fetchAll($cmd);
        // return $this->success($result);
    // }
	
	private function get_profile($table_name, $id)
	{
		$cmd = "SELECT * FROM {$table_name} WHERE id={$id}";
		return $this->db->fetchRow($cmd);
	}
	
	public function get_product_custom_fields($product_id)
	{
		$cmd ="SELECT * FROM product_custom_fields WHERE status='A' AND product_id ={$product_id}";
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	public function get_product_information($product_id)
	{
		$cmd ="SELECT * FROM products WHERE id={$product_id}";
		$results = $this->db->fetchRow($cmd);
		return $this->success($results);
	}
	
	public function update_partner($params)
	{
		$this->db->begin_transaction();	
		//partners
		$date_time = date('Y-m-d H:i:s');
		$update_data = array(
			'update_by' => $params['create_by'],
			'update_date' => $date_time,
			'status' => 'A',
			'partner_type_id' => $params['partner_type_id'], //AGENT, ISO, MERCHANT, SUB ISO, SUB AGENT
			'parent_id' => $params['parent_id'] =="" ? -1 : $params['parent_id'],
			'logo' => $params['logo'] == "" ? "" : $params['logo'],
		);
		
		if (!$this->db->update('partners', $update_data, 'id=' . $params['partner_id'])) {
	        $this->db->rollback_transaction();  
            //$this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to update partner.");
	    }
		
		// 'partner_id' => $partner_id,
		//partner_company
		$update_data = array(
			'company_name' => $params['company_name'],
			'dba' => $params['dba'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country_id' => $params['country_id'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'update_by' => $params['create_by'],
			'update_date' => $date_time,
		);
		if (!$this->db->update('partner_company', $update_data, 'partner_id=' . $params['partner_id'])) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to update partner');
        }
		$this->db->commit_transaction();
		
		// $data = array(
			// 'message' => "Partner has been updated.",
		// );
		return $this->success("Partner has been updated.");
     }
	
	
	
	
	
	
	public function get_partner_info($id)
	{
		$cmd="SELECT pc.dba,p.id as partner_id, pc.company_name,pc.email, pc.phone1, pc.phone2,pc.state,pc.country as country_name 
		,pc.zip, pc.city, p.partner_type_id,pc.address1, pc.address2
		,pcon.last_name, pcon.first_name, p.merchant_processor, p.partner_id_reference as merchant_id,
		pcon.position, pcon.mobile_number, pcon.other_number as office_number,
		pcon.extension, pcon.email as contact_email, pc.fax, pcon.fax as contact_fax,p.parent_id,
		ppm.bank_name, ppm.account_name, ppm.account_number, ppm.routing_number
		FROM partners p
LEFT JOIN partner_company pc ON p.id=pc.partner_id

LEFT JOIN partner_type pt on p.partner_type_id = pt.id
LEFT JOIN partner_contact pcon on p.id=pcon.partner_id
LEFT JOIN partner_payment_method ppm on p.id=ppm.partner_id
WHERE p.status='A' AND p.id={$id}";


		
		$rs = $this->db->fetchRow($cmd);
		return $this->success($rs);
	}
	
	
	
	public function get_partner_types()
	{
		$cmd ="SELECT * FROM partner_type WHERE status='A'";
		$rs = $this->db->fetchAll($cmd);
		return $this->success($rs);
	}
	
	
	public function get_partners()
	{
		$cmd="SELECT pc.dba,p.id as partner_id, pc.company_name,pc.email, pc.phone1, pc.phone2,pc.state,country as country_name FROM partners p
LEFT JOIN partner_company pc ON p.id=pc.partner_id
WHERE p.status='A'";
		$rs = $this->db->fetchAll($cmd);
		return $this->success($rs);
	}
	
	public function api_get_countries()
	{
		$cmd = "SELECT id,name, iso_code_2, iso_code_3 FROM countries";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//country_id
	public function get_states($params)
	{
		$cmd = "SELECT abbr as code, name FROM states ";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	//name, description, parent_id, create_by, buy_rate
	//product_custom_fields
	public function create_product($params)
	{
		$this->db->begin_transaction();	
		$insert_data = array(
			'name' => $params['name'],
			'description' => $params['description'],
			'parent_id' => $params['parent_id'],
			'create_by' => $params['create_by'],
			'buy_rate' => $params['buy_rate'],
			'status' => 'A',
		);
		if (!$this->db->insert('products', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to save products');
        } else {
        	
			$product_id = $this->db->lastInsertId();
			//insert into prouct_custom_fields
			if($params['custom_fields'] != "")
			{
				$custom_fields = substr($params['custom_fields'], 0, strlen($params['custom_fields']) - 1);
				
				
				$data = explode("|", $custom_fields);
				
				$custom_field ="";
				foreach($data as $d)
				{
					$r = explode("~", $d);
					//$custom_field[] = $r;

					
					$insert_data = array(
						'product_id' => $product_id,
						'field_name' =>  $r[0],
						'field_type' => $r[1],
						'create_by' => $_SESSION['username'],
						'options' => trim($r[2]),
						'status' => 'A'
					);
					
					if (!$this->db->insert('product_custom_fields', $insert_data)) {
				        $this->db->rollback_transaction();
				        $datetime = date('Y-m-d H:i:s');
						//$this->save_to_action_logs('Error creating product detail merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
				    	return $this->failed("Unable to create product. Please try again.");
				    }
				}
			 }
			
			
        		
            //return $this->success('Successfully saved new action_logs');
        }
        $this->db->commit_transaction();
        return $this->success('Product has been created');
	}
	
	public function update_product($params)
	{
		$this->db->begin_transaction();
		$update_data = array(
			'name' => $params['name'],
			'description' => $params['description'],
			'parent_id' => $params['parent_id'],
			'update_by' => $params['create_by'],
			'update_date' => date('Y-m-d H:i:s'),
			'buy_rate' => $params['buy_rate'],
			'status' => 'A',
		);
		if (!$this->db->update('products', $update_data, 'id=' . $params['id'])) {
	        $this->db->rollback_transaction();
            
            //$this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to update products1.");
	    }
		
		$product_id = $params['id'];
		//update product_custom_fields
		if($params['custom_fields'] != "")
		{
			$custom_fields = substr($params['custom_fields'], 0, strlen($params['custom_fields']) - 1);
			
			
			$data = explode("|", $custom_fields);
			
			$ids ="";
			$custom_field ="";
			foreach($data as $d)
			{
				
				
				$r = explode("~", $d);
				// print_r($r);
				// die();
				//$custom_field[] = $r;
				// $insert_data = array(
					// 'product_id' => $product_id,
					// 'field_name' =>  $r[0],
					// 'field_type' => $r[1],
					// 'create_by' => $_SESSION['username'],
					// 'options' => trim($r[2]),
					// 'status' => 'A'
				// );
				//check if fields already exist on the server
				$cmd ="SELECT * FROM product_custom_fields WHERE status='A' AND product_id=".$product_id;
				$cmd .=" AND ucase(field_name)=". $this->db->db_escape(strtoupper($r[0]));
				
				$data = $this->db->fetchRow($cmd);
				
				$insert_data = array(
					'product_id' => $product_id,
					'field_name' =>  $r[0],
					'field_type' => $r[1],
					'create_by' => $_SESSION['username'],
					'update_by' => $_SESSION['username'],
					'update_date' => date('Y-m-d H:i:s'),
					'options' => trim($r[2]),
					'status' => 'A'
				);
				
				if(isset($data['id']))
				{
					if (!$this->db->update('product_custom_fields', $insert_data, 'id='.$data['id'])) {
				        $this->db->rollback_transaction();
				        $datetime = date('Y-m-d H:i:s');
						//$this->save_to_action_logs('Error creating product detail merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
				    	return $this->failed("Unable to update product. Please try again.");
				    }
					$ids.="{$data['id']},";
				}else{
					if (!$this->db->insert('product_custom_fields', $insert_data)) {
				        $this->db->rollback_transaction();
				        $datetime = date('Y-m-d H:i:s');
						//$this->save_to_action_logs('Error creating product detail merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
				    	return $this->failed("Unable to update product. Please try again.");
				    }
					
					$ids.= $this->db->lastInsertId().",";
				}
			}
			$ids = substr($ids, 0, strlen($ids)-1);
			$this->db->query("UPDATE product_custom_fields SET status='D', update_by='{$_SESSION['username']}',update_date='".date('Y-m-d H:i:s')."' WHERE product_id=".$product_id ." AND id NOT IN({$ids})");
		}else{
			$this->db->query("UPDATE product_custom_fields SET status='D', update_by='{$_SESSION['username']}',update_date='".date('Y-m-d H:i:s')."' WHERE product_id=".$product_id);
		}
		
		$this->db->commit_transaction();
		
        return $this->success('Product has been updated.');
	}
	
	public function product_list()
	{
		$cmd = "SELECT * FROM products WHERE status='A'";
		$records = $this->db->fetchAll($cmd);
		$results = array();
		foreach($records as $r)
		{
			$cmd = "SELECT * FROM product_custom_fields WHERE product_id={$r['id']}";
			$customs = $this->db->fetchAll($cmd);
			$r['custom_fiels'] = $customs;
			$results[] = $r;
		}
		return $this->success($results);
	}
	
	public function create_partner($params)
	{
		$this->db->begin_transaction();	
		//partners
		$insert_data = array(
			'create_by' => $params['create_by'],
			'status' => 'A',
			'partner_type_id' => $params['partner_type_id'], //AGENT, ISO, MERCHANT, SUB ISO, SUB AGENT
			'parent_id' => $params['parent_id'] =="" ? -1 : $params['parent_id'],
			'logo' => $params['logo'] == "" ? "" : $params['logo'],
		);
		if (!$this->db->insert('partners', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create partner');
        }
		//partner_attachment
		$partner_id = $this->db->lastInsertId();

		//partner_company
		$insert_data = array(
			'partner_id' => $partner_id,
			'company_name' => $params['company_name'],
			'dba' => $params['dba'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country_id' => $params['country_id'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
		);
		if (!$this->db->insert('partner_company', $insert_data)) {
			$this->db->rollback_transaction();
            return $this->failed('Unable to create partner');
        }

		
		// $insert_data = array(
			// 'partner_id' => $partner_id,
			// 'name' => $params['document_name'],
			// 'document_image' => $params['document_image'],
			// 'create_by' => $params['create_by'],
			// 'status' => 'A',
		// );
		// if (!$this->db->insert('partner_attachment', $insert_data)) {
			// $this->db->rollback_transaction();
            // return $this->failed('Unable to create partner');
        // }
// 		
// 
// 		
		// //partner_contact
		// $insert_data = array(
			// 'partner_id' => $partner_id,
			// 'first_name' => $params['first_name'],
			// 'last_name' => $params['last_name'],
			// 'email' => $params['contact_email'],
			// 'mobile_number' => $params['contact_mobile_number'],
			// 'other_number' => $params['contact_other_number'],
			// 'position' => $params['contact_position'],
			// 'address1' => $params['contact_address1'],
			// 'address2' => $params['contact_address2'],
			// 'city' => $params['contact_city'],
			// 'state' => $params['contact_state'],
			// 'zip' => $params['contact_zip'],
			// 'country' => $params['contact_country'],
		// );
		// if (!$this->db->insert('partner_contact', $insert_data)) {
			// $this->db->rollback_transaction();
            // return $this->failed('Unable to create partner');
        // }
// 		
		// //partner_payment_method
		// $insert_data = array(
			// 'partner_id' => $partner_id,
			// 'payment_type' => $params['payment_type'],
			// 'account_number' => $params['account_number'],
			// 'routing_number' => $params['routing_number'],
			// 'status' => 'A',
			// 'create_by' => $params['create_by'],
			// 'card_number' => $params['card_number'],
			// 'cvv_code' => $params['cvv_code'],
			// 'expiry_month' => $params['expiry_month'],
			// 'expiry_year' => $params['expiry_year'],
		// );
		// if (!$this->db->insert('partner_payment_method', $insert_data)) {
			// $this->db->rollback_transaction();
            // return $this->failed('Unable to create partner');
        // }
		
		//partner_product
		
		
		//partner_product_custom_field
		$this->db->commit_transaction();
		$data = array(
			'id' => $partner_id,
			'message' => "New Partner has been created.",
		);
		return $this->success($data);
	}
	
	
	

    
    //params: action_taken, action_module, action_date, action_by, remark
    public function save_to_action_logs($action_taken, $action_module, $action_date, $action_by, $remark) {
        if (!($action_taken != '' && $action_module != '' && $action_date != '' && $action_by != '')) {
            return $this->failed('Invalid parameters');
        } else {
            if($action_module =="ERROR")
            {
                $action_taken .= " ". $this->db->get_error_string();
            }
            
            $insert_data = array(
                'action_taken'  => $action_taken,
                'action_module'            => $action_module,
                'action_date'              => $action_date,
                'action_by'         => $action_by,
                'remark'      => $remark,            
            );
            if (!$this->db->insert('action_logs', $insert_data)) {
                return $this->failed('Unable to save action_logs');
            } else {
                return $this->success('Successfully saved new action_logs');

            }
        }
    }
    
    
    
    
    // ACL PROFILE
    public function delete_module($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array('deleted'=>1);
        $this->db->update('resources', $values, 'id='.$id);
        $response = array(
            'message' => "Successfully deleted module",
        );
        return $this->success($response);
    }   
    
    
    public function add_module($params)
    {
         $params = array_map('trim', $params);
         //print_r($params);
         //die();
         
         $insert_data = array(
              'resource' => $params['resource'],
              'description' => $params['description'],
              'create_date' => date('Y-m-d H:i:s'),
              
         );           
         
         
         if (!$this->db->insert('resources', $insert_data)) 
         {
            return $this->failed("Error creating module.");
         }else {
            $response = array(
                'message' => "Successfully created module.",
            );
            return $this->success($response);
         }   
    }

    
    public function get_all_profile($where = null)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A'";

        if(!is_null($where))
            $query .= ' AND '.$where;

        $profile_list = $this->db->fetchAll($query);

        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }
    
    public function get_all_profile_selected($user_type_id)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A' AND create_by =". $user_type_id;
        $profile_list = $this->db->fetchAll($query);
        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }    
    
    public function get_all_profile_access($id)
    {
        $query = 'SELECT resource_id FROM user_templates WHERE user_type_id =' . $id;
        $access_list = $this->db->fetchAll($query);

        if(!$access_list)
            return array();
        else
            return $access_list;
    }
    

    public function get_all_modules_list($where = null)
    {
        $query = 'SELECT * FROM resources where deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $query .= ' ORDER BY description';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    public function get_all_modules_list_selected($user_type_id)
    {
        //$query = 'SELECT * FROM resources where deleted = 0';
        $query = 'SELECT r.* FROM resources r inner join user_templates ut on ut.resource_id = r.id';
        $query .= ' WHERE deleted = 0 and user_type_id = ' . $this->db->db_escape($user_type_id);         
        $query .= ' ORDER BY resource';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    protected function is_profile_existing($profile_name)
    {
        $query = 'select * from user_types where description ='.$this->db->db_escape($profile_name);
        $check_profile = $this->db->fetchAll($query);
        if (count($check_profile)>0) {
           return true;
        } else {
           return false;
        }
           
    }

    public function add_profile($params)
    {
        if(count($params['module_access']) == 0 || !isset($params['module_access']))
            return $this->failed('Select a module');
            
        //print_r($params['module_access']);
        //die();

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }
        
        if($this->is_profile_existing($params['description'])){
            return $this->failed('Profile already exist!');
        }

        $data = array(
            'description'   => $params['description'],
            'create_by'    => $params['created_by']
        );

       if(!$this->db->insert('user_types', $data)) {
            return $this->failed("Unable to add profile");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
            //print_r($id);
            // delete all access from user_templates
            $this->db->delete('user_templates', 'user_type_id=' . $id);
            // insert new access rights
            foreach($params['module_access'] as $data) {
                $insert_data = array(
                    'resource_id'   => $data,
                    'user_type_id'  => $id,
                );
                $this->db->insert('user_templates', $insert_data);
            }
            return $this->success('Profile Saved');
        }
    }

    public function edit_profile($params)
    {
        if(!isset($params['module_access']) || count($params['module_access']) == 0)
            return $this->failed('You cant add a profile with no module');

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }

        if(!is_numeric($params['id']))
            return FALSE;
        
        $id = $params['id'];
        // delete all access from user_templates
        $this->db->delete('user_templates', 'user_type_id=' . $id);
        // insert new access rights
        foreach($params['module_access'] as $data) {
            $insert_data = array(
                'resource_id'   => $data,
                'user_type_id'  => $id,
            );
            $this->db->insert('user_templates', $insert_data);
        }
        return $this->success('Profile updated');
    }

    public function delete_profile($profile_id = null)
    {
        if(is_null($profile_id) || !is_numeric($profile_id))
            return $this->failed('Invalid ID');

        if($this->db->update('user_types', array('status' => 'D'), 'id='.$this->db->db_escape($profile_id))){
            $response = array(
                'message' => "Successfully deleted module",
            );
            return $this->success($response);
        }
        else{
            $response = array(
                'message' => "Unable to delete profile",
            );
            return $this->failed($response);
        }
    }
    
    public function get_all_resources()
    {
        $query = 'SELECT * FROM resources where deleted = 0';
        $query .= ' ORDER BY id';
        //print_r($query);
        //die();
        $module_list = $this->db->fetchAll($query);
        return $this->success($module_list);
    }
    
    public function update_module($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
           'resource'      => $params['value'], 
           'description'    => $params['description'],
        );
        if (!($this->db->update('resources', $update_data, 'id=' . $params['id']))) {
            $response = array(
                'success' => false,
                'message' => 'Unable to update module.',
            );
            return $this->failed($response);    
        } else {
            $response = array(
                'success' => true,
                'message' => 'Successfully updated module',
            );
            return $this->success($response);
        }
    }
    
    // End of New code for go3 rewards
    
    public function replace_schar($val){
        
          $replace = array(
                    '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                    '&quot;' => '',
                    'À' => 'A', '??' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                    '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                    'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', '??' => 'D',
                    '??' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                    'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                    'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', '??' => 'I',
                    'Î' => 'I', '??' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                    'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                    'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                    'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                    'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                    'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                    'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                    '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                    '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                    'W' => 'W', '??' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                    'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                    'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                    'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                    'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                    'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                    'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                    'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                    'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                    'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                    '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                    '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                    '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                    'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                    'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                    'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                    'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                    '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                    '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                    '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                    '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                    '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                    '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                    '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                    '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                    '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                    '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                    '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                    '?' => 'yu', '?' => 'ya', "'"=>"`"
                    );
                    
                    $final = str_replace(array_keys($replace), $replace, $val);
        return $final;
        
    }
        
    //=============LOOKUP FUNCTIONS=====================    
    
    public function validate_session($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']) && strlen($params['session_id']) == 32)) {
            return $this->failed('Invalid or expired session id');
        } else {
            $chk = $this->db->fetchRow('select * from sessions where session_id='. $this->db->db_escape($params['session_id']));
            if (!(isset($chk['session_id']))) {
                return $this->failed('Invalid or expired session id');
            } elseif (isset($chk['logout_date']) && $chk['logout_date'] != '') {
                return $this->failed('Invalid or expired session id');
            //} elseif (strtotime($chk['last_seen']) + (3600) < time()) {
            } elseif (strtotime($chk['last_seen']) + (600) < time()) {
            //} elseif (strtotime($chk['last_seen']) + (30) < time()) {
                return $this->failed('Invalid or expired session id');
            } else {
                $values = array("last_seen" => date('Y-m-d H:i:s'));
                $this->db->update('sessions', $values, 'session_id=' . $this->db->db_escape($params['session_id']));
                //return $this->success('session id valid');
				$user_info = unserialize($chk['session_data']);
				return $this->success($user_info);
            }
        }
    }
 
    public function get_user_by_id($params) 
    {
        $query = "select u.user_center, u.is_center, CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type WHERE u.id = ".$params['id'];

        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    
    public function get_users_detailed_selected($user_type_id) {
        $query = 'select u.status, u.id,fname,lname,username,u.email_address,mobile_number,user_type_id,ut.description ' ;                $query .= 'user_type_desc from users u ';
        $query .= 'inner join user_types ut on ut.id=u.user_type_id ';
        $query .= 'where ut.created_by=' . $user_type_id;
/*        print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_list()
    {
        $query="SELECT id,username,password,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'I' THEN 'INACTIVE' ELSE 'PENDING' END as status";
        $query.=",date_created,created_by,fname";
        $query.=",lname,email_address,mobile_number,address,user_type,customers_id,agent_id FROM users";
        $users= $this->db->fetchAll($query);
        $this->success($users); 
        $response = $this->get_response(); 
        return    $response;
    }
	
	    public function get_user_type_by_username($username){
        $sql = "SELECT ut.* FROm users u LEFT JOIN user_types ut ON u.user_type_id=ut.id WHERE u.username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchAll($sql);
        return $this->success($result);  
    }
    
    public function generate_password() {
        return substr(md5(uniqid(time(), true)), 5,8);
    }


    protected function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } else {
                    if (file_exists($dst . '/' . $file)) @unlink($dst . '/' . $file);
                    copy($src . '/' . $file, $dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }
         
    
                                     
    //params: username, password
    public function login($params) {
        $params = array_map('trim', $params);

        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
			// print_r($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))));
			// die();
			
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
            
			// print_r($user_check);
			// die();
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				$profile = '';
				if($user_check['is_iso'])
				{
					$profile = 'iso';
				}elseif($user_check['is_merchant']){
					$profile = 'merchants';
				}elseif ($user_check['is_agent']){
					$profile = 'agents';
				}else{
					$profile = 'customers';
				}				
				
				$id = $user_check['reference_id'];
				// print_r($id);
				// die();
				// $user_info = $this->get_profile($profile, $id);
				$user_info = array();
				
				//$user_info['profile'] = $profile;
				
				$user_info['user_id'] = $user_check['id'];
				$user_info['partner_id'] = $id;
				$user_info['is_agent'] = $user_check['is_agent'];
				// $user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                );
                
                return $this->success($response);
            }
        }
    }
    

    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = $this->db->fetchRow('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
	
	
    
    protected function create_session($user_id, $session_data = "") {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
            //'session_data' => $session_data,
        );
		if(is_array($session_data))
		{
			$insert_data['session_data'] = serialize($session_data);
		}else
		{
			$insert_data['session_data'] =  $session_data;
		}
		
        $this->db->insert('sessions', $insert_data);
        return $session_id;        
    }
    

    
    //user_id
    public function get_user_permissions($user_id, $user_type_id) {        
        $permissions = $this->db->fetchPairs('select p.id,r.resource from permissions p inner join resources r on p.resource_id=r.id where p.user_id=' . $this->db->db_escape($user_id));

        
        $permissions2 = $this->db->fetchPairs('select ut.id,r.resource from user_templates ut inner join resources r on ut.resource_id=r.id where ut.user_type_id=' . $this->db->db_escape($user_type_id));
        $permissions = array_flip(array_merge(array_flip($permissions), array_flip($permissions2)));
        

        if (is_array($permissions) && count($permissions) > 0) {
            return $permissions;
        }
    }

    

    
    

    //params: session_id
    public function logout($params) {//note: session maintenance is not yet implemented in backend.php, client must implement its own
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']))) {
            return $this->failed('Invalid session ID');
        } else {
            $check = $this->db->fetchRow('select * from sessions where logout_date is null and session_id=' . $this->db->db_escape($params['session_id']));
            if (!(isset($check['id']))) {
                return $this->failed('Invalid session ID');
            } else {
                $update_data = array(
                    'logout_date' => date('Y-m-d H:i:s'),
                );
                if (!$this->db->update('sessions', $update_data, 'id='. $check['id'])) {
                    return $this->failed('Unable to logout');
                } else {
                    return $this->success('Successfully logged out user.');
                }
            }
        }
    }

    
    protected function failed($message) {
        $this->response = array(
            'ResponseCode' => '0001',
            'ResponseMessage' => $message,
        );
        return false;
    }
    
    protected function success($message, $record_count=-1) {
        $this->response = array(
            'ResponseCode' => '0000',
            'ResponseMessage' => $message,
        );
        return true;
    }
    
    public function get_response() {
        return $this->response;
    }
    

    
     // ACL
    public function get_module_name($module_list = array(), $include_action = false)
    {
        if(count($module_list) > 0) {
            $return_arr = array();
            $query = "SELECT * FROM acl_modules WHERE id IN(".implode(',', $module_list).") AND main = 1";
            $module_list = $this->db->fetchAll($query);

            foreach($module_list as $module) {
                $return_arr[] = $module['module_class'];
                $return_arr[] = $module['main_module_index'];
                if($include_action)
                    $return_arr[] = $module['module_action'];
            }

            $result = array_unique($return_arr);
            return $result;
        } else {
            return FALSE;
        }
    }

    public function remove_module_buttons($class)
    {
        $module_btn_return = array();

        $all_module_btn = $this->db->fetchAll('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND with_button = 1');
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];
        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_access = $this->db->fetchRow($query);
        $module_access = explode(',', $module_access['module_access']);

        if($all_module_btn && $module_access) {
            foreach($all_module_btn as $btn) {
                if(in_array($btn['id'], $module_access)) {
                    $module_btn_return[$btn['module_action']] = '';
                } else {
                    $module_btn_return[$btn['module_action']] = 'display: none;';
                }
            }
        }

        return $module_btn_return;
    }

    public function get_all_modules()
    {
        $return_arr = array();
        $query = "SELECT * FROM acl_modules"; // check if it will be a 1d array if i just get the id instead of all
        $all_modules = $this->db->fetchAll($query);

        foreach($all_modules as $module)
            $return_arr[] = $module['id'];

        return $return_arr;
    }

    public function get_user_modules()
    {
        $user_info = $this->get_user_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['user_type'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            $module_list = explode(',', $module_list['module_access']);
            $all_modules = $this->get_all_modules();

            $unset_modules = array_diff($all_modules, $module_list);

            if(count($unset_modules) > 0)
                return $unset_modules;
            else
                return FALSE;
        }
    }

    public function get_user_access_list()
    {
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            return explode(',', $module_list['module_access']);
        } else {
            return array();
        }
    }

    public function get_module_id($class, $action)
    {
        $module_info = $this->db->fetchRow('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND module_action = '.$this->db->db_escape($action));

        if(!$module_info)
            return FALSE;

        $module_id = $module_info['id'];
        $user_list = $this->get_user_access_list();

        if(in_array($module_id, $user_list)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    
     public function get_pan_key() {                                                     
        //$query = $this->db->select('key')->from('keys')->order_by('timestamp', 'desc')->get();
        $query = $this->db->fetchRow("SELECT `key` FROM `keys` ORDER by `timestamp` desc");
        if (!$query) {
            //$key = $query->result_array();
            $dek_enc = $query['key'];
            $iv = sha1('aes-256-cbc');
            //$kek = file_get_contents('http://192.168.2.16/web/i.php?token=cfbe176207b80774e8911c10893f5a0f');
            $kek="cfbe176207b80774e8911c10893f5a0f";
            $dek_plain = $this->hideit->decrypt($kek, $dek_enc, $iv);
            return $dek_plain;
        } else {
            return '';
        }
    }  
                    
    
    
}
