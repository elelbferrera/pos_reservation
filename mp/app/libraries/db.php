<?php
class db {
    private static $instance;
    private static $params;
    private $mysql;
    private $status = 0;

    public function getStatus() {
        return $this->status;
    }
    
    public function get_error_string()
    {
        return mysqli_error($this->mysql);
    }
    
    
    public function rollback_transaction()
    {
        mysqli_rollback($this->mysql);
        mysqli_autocommit($this->mysql, true);
    }
    
    
    public function begin_transaction()
    {
        mysqli_autocommit($this->mysql, false);
    }
    
    public function commit_transaction()
    {
        mysqli_commit($this->mysql);
        mysqli_autocommit($this->mysql, true);
    }
    
    public static function getInstance($params) {
        if (!isset(self::$instance) || self::$params != serialize($params)) {
            self::$params = serialize($params);
            $c = __CLASS__;
            self::$instance = new $c($params);
        }
        return self::$instance;
    }

    public function __construct($params) {
		ini_set('display_errors', true);
        $this->status = 0;
        //$this->mysql = mysql_connect($params['host'], $params['user'], $params['password']);
        $this->mysql = mysqli_connect($params['host'], $params['user'], $params['password']);
        if ($this->mysql) {
            //if (mysql_select_db($params['db'], $this->mysql)) {
            if (mysqli_select_db($this->mysql, $params['db'])) {
                $this->status = 1;
            }
        }
    }

    public function fetchRow($query) {
        if (strpos(strtoupper($query), ' LIMIT ') === FALSE) {
            $query .= ' LIMIT 0,1';
        }
        //$res = mysql_query($query, $this->mysql);
        $res = mysqli_query($this->mysql, $query);
		if ( $res === false ){
			print_r($query);
			die();
			//handle the error here
		}
		
        //$arr = mysql_fetch_assoc($res);
        $arr = mysqli_fetch_assoc($res);
        return $arr;
    }

    public function query($query) {
        //return mysql_query($query, $this->mysql);
        return mysqli_query($this->mysql, $query);
    }
    
	public function fetchReassignToFile($query, $file_name, $phone_field, $po_id, $insert_query, $batch_no,$from,$to)
    {
          
        //$res = mysqli_query($this->mysql,$query);
                           
        $file = time();
//        while ($arr = mysql_fetch_assoc($res)) {
//            fputcsv($file_name, array_values($arr));    
//        }
//        fclose($file_name);
                                                            
       $insert_query = str_replace("{SELECT}", "SELECT da.phone_no, {$po_id}, '{$file}'", $insert_query);
       
       mysqli_query($this->mysql,"INSERT INTO data_phone_delete (phone_no, po_id, file) {$insert_query} ");
     
        //mysql_query("INSERT INTO data_phone_delete (phone_no, po_id, file) VALUES('{$arr[$phone_field]}',{$po_id},'{$file}')", $this->mysql);
     
        $delete_cmd ="DELETE FROM po_transaction_data_2_{$po_id} WHERE phone_no IN(SELECT DISTINCT phone_no FROM data_phone_delete WHERE po_id={$po_id} AND file='{$file}' )
AND po_id={$po_id} AND batch_no={$batch_no} AND id>={$from} AND id<={$to}";
        mysqli_query($this->mysql,$delete_cmd);
        
        $cmd="DELETE FROM po_transaction_data2_nos_{$po_id} WHERE phone_no IN(SELECT DISTINCT phone_no FROM data_phone_delete WHERE po_id={$po_id} AND file='{$file}' )
AND po_id={$po_id} AND batch_no={$batch_no} ";
       //$delete_cmd = str_replace("po_transaction_data_2_{$po_id}", "po_transaction_data2_nos_{$po_id}", $delete_cmd);
        mysqli_query($this->mysql,$cmd);
       // $delete_cmd = str_replace("po_transaction_data2_nos_{$po_id}", "po_transaction_data_2_calling_list_{$po_id}", $delete_cmd);
       $cmd="DELETE FROM po_transaction_data_2_calling_list_{$po_id} WHERE phone_no IN(SELECT DISTINCT phone_no FROM data_phone_delete WHERE po_id={$po_id} AND file='{$file}' )
AND po_id={$po_id} AND batch_no={$batch_no} ";
        mysqli_query( $this->mysql,$cmd);
    /* $delete_cmd = "DELETE FROM po_transaction_data_ans_2_{$po_id} WHERE col2 IN(SELECT DISTINCT phone_no FROM data_phone_delete WHERE                        po_id={$po_id} AND file='{$file}') AND po_id={$po_id}";*/
      $delete_cmd = "DELETE FROM po_transaction_data_ans_2_{$po_id} WHERE col2 IN(SELECT DISTINCT phone_no FROM data_phone_delete WHERE                        po_id={$po_id} AND file='{$file}') AND po_id={$po_id} AND crid_permanent>={$from} AND crid_permanent<={$to}";
      
        mysqli_query($this->mysql,$delete_cmd);          
        //$delete_cmd = "DELETE FROM data_phone_delete WHERE po_id ={$po_id} AND file='{$file}'";
        $delete_cmd = " TRUNCATE TABLE data_phone_delete";
        mysqli_query($this->mysql,$delete_cmd);     
        
      //  ob_start();
        //header("Refresh: 0; url=".WEBROOT."/pomaintenance/downloadcall_list/{$po_id}");
        //ob_end_flush();    
        
    }

   
   public function fetchAllTOFile($query, $file_name,$status_code_position=0,$custom_status_code="") {      
        $res = mysqli_query( $this->mysql,$query);
        $rec_cnt=0;
         
            
        //$all = array();
/*        $fp = fopen('php://output', 'w');
        
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        header('Pragma: no-cache');
        header('Expires: 0');
*/        
//For Custom databack
            $set_custom_status_code = explode(",",$custom_status_code); 
            while ($arr = mysqli_fetch_assoc($res)) {
                
            //fputcsv($fp, array_values($arr));
            if($status_code_position==0 && $custom_status_code==""){
                
                fputcsv($file_name, array_values($arr));
               //$this->fputcsv_eol($file_name,$arr);
            }else{
                foreach($set_custom_status_code as $r)
                {
                      $custom_status_code = explode("=",$r);
                      if(trim($custom_status_code[0])==trim($arr[$status_code_position])){
                         $arr[$status_code_position]= trim($custom_status_code[1]);
                      }
                }
                 //fputs($file_name,"\r\n");
                 fputcsv($file_name, array_values($arr));
                //$this->fputcsv_eol($file_name,$arr);
                
            }
           
           $rec_cnt++; 
        }
                fclose($file_name);
               return $rec_cnt; 
              //  die();
        
        
        // while ($arr = mysql_fetch_assoc($res)) {
            // //$all[] = $arr;
            // //print_r($arr);
            // file_put_contents($file, $arr, FILE_APPEND);
        // }
        // return $all;
    }
    function fputcsv_eol($handle, $array, $delimiter = ',', $enclosure = '"', $eol = "\r\n") {
        $return = fputcsv($handle, $array, $delimiter, $enclosure);
        if($return !== FALSE && "\r\n" != $eol && 0 === fseek($handle, -1, SEEK_CUR)) {
            fwrite($handle, $eol);
        }
        return $return;
    }
   
    
    
    public function fetchAll($query) {      
        //$res = mysql_query($query, $this->mysql);
		$res = mysqli_query($this->mysql, $query);
		
        $all = array();
        
        if ( $res === false ){
            print_r($query);
            die();
            //handle the error here
        }
		
        while ($arr = mysqli_fetch_assoc($res)) {
            
            
            $all[] = $arr;
			//print_r($arr);
        }
        return $all;
    }

    public function fetchPairs($query) {
        //$res = mysql_query($query, $this->mysql);
        $res = mysqli_query($this->mysql, $query);
        if ( $res === false ){
            print_r($query);
            die();
            //handle the error here
        }
        $all = array();
        while ($arr = mysqli_fetch_assoc($res)) {

            
            $key = array_shift($arr);
            $val = array_shift($arr);
            $all[$key] = $val;
        }
        return $all;
    }

    public function lastInsertId() {
        //return mysql_insert_id($this->mysql);
        return mysqli_insert_id($this->mysql);
    }

    public function db_escape($values, $quotes = true) {
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $values[$key] = $this->db_escape($value, $quotes);
            }
        } else if ($values === null) {
            $values = 'NULL';
        } else if (is_bool($values)) {
            $values = $values ? 1 : 0;
        } else if ((is_numeric($values) && 'a'.$values != 'a'.(double)$values) || !is_numeric($values)) {
            //$values = mysql_real_escape_string($values, $this->mysql);
            $values = mysqli_real_escape_string($this->mysql, $values);
            
            if ($quotes) {
                $values = "'$values'";
            }
        }
        return $values;
    }
	
	//JR-2014/03/24
    public function db_escape_wildcard($values, $quotes = true) {
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $values[$key] = $this->db_escape($value, $quotes);
            }
        } else if ($values === null) {
            $values = 'NULL';
        } else if (is_bool($values)) {
            $values = $values ? 1 : 0;
        } else if ((is_numeric($values) && 'a'.$values != 'a'.(double)$values) || !is_numeric($values)) {
            //$values = mysql_real_escape_string($values, $this->mysql);
            $values = mysqli_real_escape_string($this->mysql, $values);
            if ($quotes) {
                $values = "'%$values%'";
            }
        } else {
           //$values = mysqli_real_escape_string($values, $this->mysql);
           $values = mysqli_real_escape_string($this->mysql, $values);
            if ($quotes) {
                $values = "'%$values%'";
            } 
        }
        return $values;
    }

    public function insert($table, $values) {
        if (trim($table) != '' && is_array($values) && count($values) > 0) {
            $values = $this->db_escape($values);
			
			  // if($table=="merchant_rewards")
			  // {
            	  // $insert_query = 'insert into ' . $table . '(' . implode(',', array_keys($values)) .') VALUES (' . implode(',', $values) . ')';
			  // }else{
				$insert_query = 'insert into ' . $table . '(`' . implode('`,`', array_keys($values)) .'`) VALUES (' . implode(',', $values) . ')';
			  // }
            //if (mysql_query($insert_query, $this->mysql) === TRUE) {
            if (mysqli_query($this->mysql, $insert_query) === TRUE) {
                //return mysql_affected_rows($this->mysql);
                return mysqli_affected_rows($this->mysql);
            } else {
                print_r($insert_query);
                die();
                return FALSE;
            }
        } else {
            var_dump('invalid insert');
        }
    }

    public function batchInsert($table, $values, $group = 100) {
        if (trim($table) != '' && is_array($values) && count($values) > 0) {
            $val_query = array();
            foreach ($values as $key => $val) {
                if (is_array($val)) {
                    $val = $this->db_escape($val);
                    if ($key == 0) {
                        $insert_query = 'insert into ' . $table . '(`' . implode('`,`', array_keys($val)) .'`) VALUES ';
                    }
                    if ($key % $group == $group - 1) {
                        $query[] = $insert_query . implode(',', $val_query);
                        $val_query = array();
                    }
                    $val_query[] = '(' . implode(',', $val) . ')';
                }
            }
            if (count($val_query) > 0) {
                $query[] = $insert_query . implode(',', $val_query);
            }
            $affected = 0;
            foreach ($query as $q) {
                
                //if (mysql_query($q, $this->mysql) === TRUE) {
                if (mysqli_query($this->mysql,$q) === TRUE) {
                    //$affected += mysql_affected_rows($this->mysql);
                    $affected += mysqli_affected_rows($this->mysql);
                }
            }
            return $affected;
        } else {
            var_dump('invalid insert');
        }
    }

    public function update($table, $values, $where) {
        if (trim($table) != '' && is_array($values) && count($values) > 0 && trim($where) != '') {
            $values = $this->db_escape($values);
            foreach ($values as $key => $val) {
            	//$key = trim($key);
            	// if($key =="status")
				// {
				    // $set[] = "`$key`=$val";
				// }else{
					$set[] = "$key=$val";
				// }
            }
			
			

            $insert_query = 'update ' . $table . ' SET ' . implode(',', $set) . ' WHERE ' . $where;
               
			// if($table =="customers")
			// {
				// var_dump($insert_query);
			// die();
			// }
			
            //if (mysql_query($insert_query, $this->mysql) === TRUE) {
            if (mysqli_query($this->mysql,$insert_query) === TRUE) {
                //return mysql_affected_rows($this->mysql);
                return mysqli_affected_rows($this->mysql);
            } else {
            	print_r($insert_query);
                return FALSE;
            }
        } else {
            var_dump('invalid insert');
        }
    }

    public function delete($table, $where) {
        if (trim($table) != '' && trim($where) != '') {
            $delete_query = 'delete from `' . $table . '` where ' . $where;
            //if (mysql_query($delete_query, $this->mysql) === TRUE) {
            if (mysqli_query($this->mysql, $delete_query) === TRUE) {
                //return mysql_affected_rows($this->mysql);
                return mysqli_affected_rows($this->mysql);
            } else {
                return FALSE;
            }
        }
    }
}