<?php
class backend {
    protected $response;
    protected $db;
    public $salt;

    public function __construct(&$db) {
        $this->salt = PROGRAM;  
        $this->salt2 = '2016'.PROGRAM.'2016';
        $this->db = $db;
        require_once 'xorry.php';
        require_once 'class.phpmailer.php';
        require_once dirname(__DIR__) . '/libraries/HideIt.php';
        $this->hideit = new HideIt('aes-256-cbc');
		error_reporting(E_ALL);
    }
	

	//username
	public function forgot_password($params)
	{
		$params = array_map('trim', $params);
		$user = $this->db->fetchRow("SELECT * FROM users WHERE username=".$this->db->db_escape($params['username']));
		
		if(!isset($user['id']))

		{
			return $this->failed("Unable to find username.");
		}
		
		$decrypt_password = $this->hideit->decrypt(sha1(md5($this->salt)),$user['password'], sha1(md5($this->salt)));
		
		
		$body="<br><br>Greetings!<br><br>
				Your password is: <b> {$decrypt_password} </b>
           . <br> <br> Thanks,<br> Go3Rewards Admin";    
           
        if($this->send_mail_notification($body,$user['email_address'],"Go3 Notification"))
		{
			return $this->success("Password has been sent on your email.");
		}else{
			return $this->failed("Unable to forgot your password. Please try again.");
		}
	}


	//merchant_id, page_number ,limit last 15 as default
	//optional start_date, end_date, transaction_type
	public function get_transactions_per_merchant($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$limit = $params['limit'];
		$page_number = $params['page_number'];
		if($limit =="")
		{
			$limit = 15;
		}
		
		$cmd = "SELECT c.last_name, c.first_name, ct.id as transaction_id, amount, ct.points_earned, ct.create_date as transaction_date, 
 		CASE ct.status WHEN 'A' THEN 'Completed' WHEN 'V' THEN 'VOID' ELSE 'UNKNOWN' END as status,
 		type as transaction_type, ct.description
		FROM customer_transactions ct 
		LEFT JOIN customers c ON c.id=ct.customer_id 
		 WHERE ct.merchant_id={$merchant_id}  ";
		
		if($params['start_date']!="")
		{
			$cmd .=" AND ct.create_date>='{$params['start_date']}'";
			$cmd .=" AND ct.create_date<=DATE_ADD('{$params['end_date']}', INTERVAL 1 DAY)";
		} 
		
		if($params['transaction_type']!="")
		{
			$cmd.=" AND ct.transaction_type='{$params['transaction_type']}'";
		}
		
		$cmd .=" ORDER BY ct.create_date DESC limit {$page_number},{$limit}";
		
		$trans = $this->db->fetchAll($cmd);
		return $this->success($trans);
	}

	public function reset_points()
	{
		$cmd = "SELECT * FROM customer_points_merchant";
		$res = $this->db->fetchAll($cmd);
		
		foreach($res as $r)
		{
			//check customer_id 
			//customer_reward_level_merchant get current level
			
			//get required points from merchant_rewards
			
			//if customer_accu_points_merchant points_earned_current 
			//greater than required points, check if qualified for next level
			//then if qualified upgrade customer on merchant selected
			// check if points is still valid on date of current level 
			// if expire reset points 
			// reset points_earned_current
			//else 
			// do nothing
		}
	}
    

	
	//email, password
	public function validate_customer_password($params)
	{
		$params = array_map('trim', $params);
		$user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['email']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
        
        if (!(isset($user_check['id']))) {
            return $this->failed('Invalid password');
        } else {
            
            $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
        
            $user_check['fname'] = $user_check['first_name'];
            $user_check['lname'] = $user_check['last_name'];
			
			
			$profile = '';
			if($user_check['is_iso'])
			{
				$profile = 'iso';
			}elseif($user_check['is_merchant']){
				$profile = 'merchants';
			}elseif ($user_check['is_agent']){
				$profile = 'agents';
			}else{
				$profile = 'customers';
			}				
			
			if($profile <> 'customers')
			{
				return $this->failed("Invalid user type.");
			}
			
			$id = $user_check['reference_id'];
			$user_info = $this->get_profile($profile, $id);
			
			return $this->success($user_info);
		}
	}
	
	public function api_get_countries()
	{
		$cmd = "SELECT id,name, iso_code_2, iso_code_3 FROM countries";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//country_id
	public function get_states($params)
	{
		$cmd = "SELECT code, name FROM states WHERE country_id={$params['country_id']}";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	
	//merchant_id, mobile, password
	public function merchant_activate_customer($params)
	{
		$params = array_map('trim', $params);
		$password = $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))));
		$mobile = $params['mobile'];
		
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		
		$customer_info = $this->get_customer_info($mobile);
		$customer_id = $customer_info['id'];
		
		if($this->check_if_customer_a_member($customer_id, $merchant_id))
		{
			return $this->failed("Customer has already been activated.");
		}
		
		$user_check = $this->db->fetchRow("select  u.* from users u where u.username=". $this->db->db_escape($customer_info['email']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
            
        if (!(isset($user_check['id']))) {
            return $this->failed('Invalid username or password for customer.');
        }
		
		$merchant_username = $this->get_username_by_reference($merchant_id, "merchant");
		
		$merchant_username= $merchant_username['username'];
		
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'create_by'	=> 	$merchant_username,
		);
		
		if (!$this->db->insert('merchant_customers', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error activating customer'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Error activating customer. Please try again.");
	    }
	    
    	$insert_data = array(
			'merchant_id' =>  $merchant_id,
			'customer_id' => $customer_id,
			'level' => 1,
			'create_by' => $merchant_username,
		);
			
		if (!$this->db->insert('customer_reward_level_merchant', $insert_data)) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new customer.'. $customer_info['last_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to create new transaction.");
        } 
    
		return $this->success("Customer has been added to your list. You may now proceed with your transaction.");	
	}
	//merchant_id, mobile
	public function merchant_search_customer($params)
	{
		$params = array_map('trim', $params);
		$mobile =$params['mobile'];
		
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		$merchant_id = $params['merchant_id'];
		$customer_info = $this->get_customer_info($mobile);
		
		
		
	
		if(!isset($customer_info['id']))
		{
			return $this->failed("3");
		}
		
		$customer_id = $customer_info['id'];
		
		if($this->check_if_customer_a_member($customer_id, $merchant_id))
		{
			return $this->failed("2");
		}
		return $this->success($customer_info);		
	}
	
	
	private function check_if_customer_a_member($customer_id, $merchant_id)
	{
		$cmd ="SELECT id  FROM merchant_customers WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
		
		$row = $this->db->fetchRow($cmd);
		if(isset($row['id']))
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	
	//merchant_id, customer_id
	public function get_merchant_customer_balance($params)
	{
		$cmd =" SELECT m.*, cp.amount_earned, points_earned, cp.update_date,IFNULL(crlm.level,1) as reward_level 
		FROM merchants m 
		INNER JOIN merchant_customers mc ON m.id=mc.merchant_id AND mc.customer_id='{$params['customer_id']}'
		INNER JOIN customer_points_merchant cp ON m.id=cp.merchant_id AND cp.customer_id='{$params['customer_id']}'
		LEFT JOIN customer_reward_level_merchant crlm ON mc.customer_id=crlm.customer_id AND mc.merchant_id=crlm.merchant_id AND crlm.status='A'";
		$cmd .=" WHERE m.id={$params['merchant_id']}";
		
		$result = $this->db->fetchRow($cmd);
		
	
		$rewards = $this->get_reward_info($result['reward_level'], $params['merchant_id']);

		$result['required_amount'] = $rewards['amount'];
		$result['point_conversion'] = $rewards['amount_point_conversion'];
		$result['reward_code'] = $rewards['reward_code'];
		$result['reward_name'] = $rewards['reward_name'];
		
		return $this->success($result);
	}
	

	
	//username, password, last_name, first_name, email, merchant_id
	public function register_merchant_user($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$cmd ="SELECT id FROM users WHERE (username= ". $this->db->db_escape($params['username']);
		$cmd .= " OR email_address=" .$this->db->db_escape($params['email']) ;
		$cmd .= ")  AND status='A'";
		
		$user = $this->db->fetchRow($cmd);
		
		if(isset($user['id']))
		{
			return $this->failed("User information already exist. Please enter a new info.");
		}
		
		$user_insert = array(
		    'username' => $params['username'],
		    'password' => $params['password'], //encryption happens on creation of user method
		    'last_name' => $params['last_name'],
		    'first_name' => $params['first_name'],
	        'email_address' => $params['email'],
		    'reference_id' => $merchant_id, //newly created Id
		    'user_type_id' => 7, //user
	    );    
		
		$this->db->begin_transaction();	
		$this->create_user($user_insert);    // this function will proceed to rollback in case of error.
		
		$this->db->commit_transaction();
		
		return $this->success("User successfully registered.");
	}
	
	//merchant_id, transaction_id
	public function void_redeem_points($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction has been voided.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		
		$customer_id = $customer_info['id'];
	
		
		// if(isset($params['password']))
		// {
			// if($params['password'] !="")
			// {
				// $password = $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))));
// 				
				// $user_check = $this->db->fetchRow("select  u.id from users u where u.username=". $this->db->db_escape($customer_info['email']) ." and password=" . $this->db->db_escape($password) . " and u.status='A'");
// 		            
		        // if (!(isset($user_check['id']))) {
		            // return $this->failed('Invalid password for customer.');
		        // }
			// }
		// }else{
			// return $this->failed("Enter customer password.");
		// }
		
		
		//$amount = $trans_info['amount'];
		$points_prev = $trans_info['points_earned'];

		
		$datetime = date('Y-m-d H:i:s');
        
		$customer_id  = $customer_info['id'];
		
		$cmd ="SELECT id,points_earned, amount_earned FROM customer_points_merchant WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
		$points =$this->db->fetchRow($cmd);
		if(!isset($points['points_earned']))
		{
			return $this->failed("There are no available points to redeem.");
		}
		
		$avail_points = $points['points_earned'];
		//$amount_earned = $points['amount_earned'];
		
		$this->db->begin_transaction();
		
		$datetime = date('Y-m-d H:i:s');
		
		 
		
		$update_data = array(
			//'amount_earned' => $amount_earned -  $amount,
			'points_earned' => $avail_points + $points_prev,
			'update_date' => $datetime,
			'update_by' => $merchant_username,
		);
		
		$this->db->begin_transaction();	
		if (!$this->db->update('customer_points_merchant', $update_data, 'id=' . $points['id'])) {
	        $this->db->rollback_transaction();
            
            $this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to convert points.");
	    }
		
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		if (!$this->db->update('merchant_redeem_transactions', $update_data, 'transaction_id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("Your transaction has been voided by {$merchant_name}.", $email_address, "Void Transaction");
		
		
		$this->db->commit_transaction();
		return $this->success("Succefully void transaction.");
	}
	
	//merchant_id, points, customer_ref email or mobile, description
	public function redeem_points($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}	
		$merchant_id =$params['merchant_id'];
		
		
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		

		$customer_id = $customer_info['id'];
		
		$merchant_username = $this->get_username_by_reference($merchant_id, "merchant");
		$merchant_username= $merchant_username['username'];
		
		$level = $this->get_customer_level($merchant_id, $customer_info['id'], $merchant_username);
		$rewards = $this->get_reward_info($level, $merchant_id);	
		
		$cmd ="SELECT id,points_earned, amount_earned FROM customer_points_merchant WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
		$points =$this->db->fetchRow($cmd);
		if(!isset($points['points_earned']))
		{
			return $this->failed("There are no available points to redeem.");
		}
		
		$avail_points = $points['points_earned'];
		if($params['points'] > $avail_points)
		{
			return $this->failed("The points you are trying to redeem is not sufficient. Customer current points is {$avail_points} you are trying to redeem {$params['points']} point(s).");
		}
		
		// $amount_earned = $points['amount_earned'];
// 		
// 		
		// $new_earned =  ($params['points']/$rewards['point']) *  $rewards['point_amount_conversion'];
		
		$datetime = date('Y-m-d H:i:s');
		$update_data = array(
			//'amount_earned' => $amount_earned +  $new_earned,
			'points_earned' => $avail_points - $params['points'],
			'update_date' => $datetime,
			'update_by' => $merchant_username,
		);
		
		
		$this->db->begin_transaction();	
		if (!$this->db->update('customer_points_merchant', $update_data, 'id=' . $points['id'])) {
	        $this->db->rollback_transaction();
            
            $this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to convert points.");
	    }
		
		
		//update customer points customer_points_merchant
		//create a transaction convert/redeem
		
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => 0,
			'points_earned' => $params['points'],
			//'description' => "Redeem Points.",
			'description' => $params['description'],
			'create_by' => $merchant_username,
			'type' => 'REDEEM',
		);
	
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error redeeming points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to redeem points. Please try again.");
	    }
		
		$transaction_id = $this->db->lastInsertId();
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'points' => $params['points'],
			'transaction_id' => $transaction_id,
			'create_by' => $merchant_username,
		);

		if (!$this->db->insert('merchant_redeem_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error redeeming points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to redeem points. Please try again.");
	    }

		$this->db->commit_transaction();
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("You have redeemed {$params['points']} pt(s) from {$merchant_name}", $email_address, "Redeem Transaction");
		
		return $this->success("Successfully redeem points.");
	}
	
	
	//merchant_id, transaction_id
	public function void_convert_customer_points($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction has been voided.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		
		$customer_id = $customer_info['id'];
		
		$amount = $trans_info['amount'];
		$points = $trans_info['points_earned'];

		
		$datetime = date('Y-m-d H:i:s');
        
		$customer_id  = $customer_info['id'];
		
		$cmd ="SELECT id,points_earned, amount_earned FROM customer_points_merchant WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
		$points =$this->db->fetchRow($cmd);
		if(!isset($points['points_earned']))
		{
			return $this->failed("There are no available points to redeem.");
		}
		
		$avail_points = $points['points_earned'];
		$amount_earned = $points['amount_earned'];
		
		$this->db->begin_transaction();
		
		$datetime = date('Y-m-d H:i:s');
		$update_data = array(
			'amount_earned' => $amount_earned -  $amount,
			'points_earned' => $avail_points + $points,
			'update_date' => $datetime,
			'update_by' => $merchant_username,
		);
		
		$this->db->begin_transaction();	
		if (!$this->db->update('customer_points_merchant', $update_data, 'id=' . $points['id'])) {
	        $this->db->rollback_transaction();
            
            $this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to convert points.");
	    }
		
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		if (!$this->db->update('merchant_convert_transactions', $update_data, 'transaction_id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		
		$this->db->commit_transaction();
		return $this->success("Succefully void transaction.");
	}
	
	
	
	
	//merchant_id, transaction_id
	public function void_sale_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction has been voided.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		
		$customer_id = $customer_info['id'];
		
		$amount = $trans_info['amount'];

		
		$datetime = date('Y-m-d H:i:s');
        
		$customer_id  = $customer_info['id'];
		
		$this->db->begin_transaction();
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		//void merchant_reload_transactions
		if (!$this->db->update('merchant_sale_transactions', $update_data, 'transaction_id=' . $transaction_id)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding sale transaction merchant_sale_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void sale transaction.");
        }		
		
		//debit customer amount on merchant
		if(!$this->update_customer_amount_earned($merchant_id, $customer_id, $merchant_username, $amount, "add"))
		{
			$this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error voiding sale transaction'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	
			return $this->failed("Unable to process void transaction. Please try again.");
		}
		
		$this->db->commit_transaction();
		return $this->success("Transaction has been voided successfully.");
	}
	
	//merchant_id, amount, customer_ref can be mobile or email of customer, user_info
	public function sale_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$amount =$params['amount'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$merchant_username= $merchant_username['username'];
			
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		

		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		$merchant_id = $params['merchant_id'];
		
		//add customer_transactions 
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => $amount,
			'points_earned' => 0,
			'description' => "Sale Transaction",
			'create_by' => $merchant_username,
			'type' => 'DEDUCT AMOUNT',
		);
	
		$c_amount = $this->get_customer_existing_points($merchant_id, $customer_id);
		
		

		if(isset($c_amount['id']))
		{
			if($c_amount['amount_earned']< $amount)
			{
				return $this->failed("Insufficient balance.");
			}
		}else{
			return $this->failed("Insufficient balance.");
		}
		
	
		$this->db->begin_transaction();
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating sale transaction '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process sale transaction. Please try again.");
	    }
		$transaction_id = $this->db->lastInsertId();
		
		//merchant_reload_transactions
		$insert_data = array(
			'amount' => $amount,
			'customer_id' => $customer_id,
			'transaction_id' => $transaction_id,
			'status' => 'A',
			'create_by' => $merchant_username
		);
		if (!$this->db->insert('merchant_sale_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating sale transaction merchant_sale_transactions'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to deduct amount. Please try again.");
	    }
		
		if(!$this->update_customer_amount_earned($merchant_id, $customer_id, $merchant_username, $amount, "deduct"))
		{
			$this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating sale transaction update earned amount'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	
			return $this->failed("Unable to deduct amount. Please try again.");
		}
		
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("You have deduct an amount of $ {$amount} from {$merchant_name}", $email_address, "Deduct Amount Transaction");
		
		
		
		$this->db->commit_transaction();
		return $this->success("Deduct Amount has been processed.");
	}
	
	
	
	public function get_transaction_type($transaction_id, $merchant_id)
	{
		$cmd ="SELECT type FROM customer_transactions WHERE id={$transaction_id} AND merchant_id={$merchant_id}";
		$type = $this->db->fetchRow($cmd);
		
		if(isset($type['type']))
		{
			return $this->success($type['type']);
		}else{
			return $this->failed("Invalid transaction id.");
		}
	}
	
	
	private function update_customer_amount_earned($merchant_id, $customer_id, $merchant_username, $amount, $type ="add")
	{
		$datetime = date('Y-m-d H:i:s'); 
		$c_points = $this->get_customer_existing_points($merchant_id, $customer_id);
		

		if(isset($c_points['id']))
		{
			//update data on tables
			if($type=="add")
			{
				$update_data = array(
					'amount_earned' => $c_points['amount_earned'] + $amount,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);
			}else{
				$update_data = array(
					'amount_earned' => $c_points['amount_earned'] - $amount,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);
			}
			
			if (!$this->db->update('customer_points_merchant', $update_data, 'id=' . $c_points['id'])) {
	            return false;
	        }
		}else{
			//insert record
			$insert_data = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $customer_id,
				'amount_earned' => $amount,
			
			);
			if (!$this->db->insert('customer_points_merchant', $insert_data)) {
	            return false;
        	}
		}
		return true;
	}

	//merchant_id, transaction_id
	public function void_reload_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction has been voided.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		
		$customer_id = $customer_info['id'];
		
		$points_earned = -$trans_info['points_earned'];
		$amount = $trans_info['amount'];
		//update customers table points_earned and total_amount accumulated not by merchant
		$update_customer = array(
			'points_earned' => $customer_info['points_earned'] + $points_earned, // update total points earned
			'total_amount' =>  0, 
		);
		
		$datetime = date('Y-m-d H:i:s');
        
		$customer_id  = $customer_info['id'];
		
		$this->db->begin_transaction();
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		//void merchant_reload_transactions
		if (!$this->db->update('merchant_reload_transactions', $update_data, 'transaction_id=' . $transaction_id)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }		
		
		//debit customer amount on merchant
		if(!$this->update_customer_amount_earned($merchant_id, $customer_id, $merchant_username, $amount, "subtract"))
		{
			$this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error voiding reload transaction update earned amount'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	
			return $this->failed("Unable to process void transaction. Please try again.");
		}
		

		$this->db->commit_transaction();
		return $this->success("Transaction has been voided successfully.");
	}
	
	//merchant_id, amount, customer_ref can be mobile or email of customer, user_info
	public function reload_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$amount =$params['amount'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$merchant_username= $merchant_username['username'];
			
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		

		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		$merchant_id = $params['merchant_id'];
		
		//add customer_transactions 
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => $amount,
			'points_earned' => 0,
			'description' => "Reload Transaction",
			'create_by' => $merchant_username,
			//'type' => 'RELOAD',
			'type' => 'ADD AMOUNT'
		);
	
		$this->db->begin_transaction();
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating reload transaction '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process reload transaction. Please try again.");
	    }
		$transaction_id = $this->db->lastInsertId();
		
		//merchant_reload_transactions
		$insert_data = array(
			'amount' => $amount,
			'customer_id' => $customer_id,
			'transaction_id' => $transaction_id,
			'status' => 'A',
			'create_by' => $merchant_username
		);
		if (!$this->db->insert('merchant_reload_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating reload transaction merchant_reload_transactions'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process reload transaction. Please try again.");
	    }
		
		if(!$this->update_customer_amount_earned($merchant_id, $customer_id, $merchant_username, $amount, "add"))
		{
			$this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating reload transaction update earned amount'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	
			return $this->failed("Unable to process reload transaction. Please try again.");
		}
		
		$this->db->commit_transaction();
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("You have reloaded an amount of $ {$amount} from {$merchant_name}", $email_address, "Reload Amount Transaction");
		
		return $this->success("Reload transaction has been processed.");
	}
	
	//merchant_id, merchant_logo
	public function update_merchant_logo($params)
	{
		$date_time = date('Y-m-d H:i:s');
		$update_data = array(
			'merchant_logo' => DOMAIN."/public/".$params['merchant_logo'],
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $_SESSION['username'],
		);
		
		$this->db->begin_transaction();	
		if (!$this->db->update('merchants', $update_data, 'id=' . $params['merchant_id'])) {
	        $this->db->rollback_transaction();
            
            $this->save_to_action_logs('Error updating merchant logo merchant_id'. $params['merchant_id'],'ERROR',$datetime,"",'');
            return $this->failed("Unable to update merchant logo.");
	    }
		
		$this->db->commit_transaction();
		return $this->success("Merchant logo has been updated.");	
	}
	
	
	
	//customer_id, page_number, limit, merchant_id
	public function my_transactions_per_merchant($params)
	{
		//results should also include merchant name
		$cmd ="SELECT m.merchant_name,ct.id as transaction_id, amount, points_earned, type as transaction_type, ct.create_date  as transaction_date,
			description,CASE ct.status WHEN 'A' THEN 'Completed' WHEN 'V' THEN 'VOID' ELSE 'UNKNOWN' END as status
			FROM customer_transactions ct
			LEFT JOIN merchants m on ct.merchant_id=m.id
			WHERE ct.customer_id ={$params['customer_id']} AND ct.merchant_id = {$params['merchant_id']}
			ORDER BY ct.create_date DESC
			LIMIT {$params['page_number']},{$params['limit']}
		";
		$data = $this->db->fetchAll($cmd);
		

		
		$cmd_count="SELECT count(ct.id) as row_count
			FROM customer_transactions ct
			LEFT JOIN merchants m on ct.merchant_id=m.id
			WHERE ct.customer_id ={$params['customer_id']} AND ct.merchant_id = {$params['merchant_id']}";
		
		$count = $this->db->fetchRow($cmd_count);
		$data['record_count'] =$count['row_count'];
		
		return $this->success($data);
	}
	
	public function get_my_profile($profile_name, $profile_id)
	{
		$cmd ="SELECT * FROM {$profile_name} WHERE id = {$profile_id} AND status='A'";
		$data = $this->db->fetchRow($cmd);
		
		if(!isset($data['id']))
		{
			return $this->failed("Unable to retrieve profile");
		}else{
			return $this->success($data);
		}
	}
	
	
	//merchant_id, points, customer_ref email or mobile
	public function convert_customer_points($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}	
		$merchant_id =$params['merchant_id'];
		
		
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		
		$merchant_username = $this->get_username_by_reference($merchant_id, "merchant");
		$merchant_username= $merchant_username['username'];
		
		$level = $this->get_customer_level($merchant_id, $customer_info['id'], $merchant_username);
		$rewards = $this->get_reward_info($level, $merchant_id);	
		
		$cmd ="SELECT id,points_earned, amount_earned FROM customer_points_merchant WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
		$points =$this->db->fetchRow($cmd);
		if(!isset($points['points_earned']))
		{
			return $this->failed("There are no available points to convert.");
		}
		
		$avail_points = $points['points_earned'];
		if($params['points'] > $avail_points)
		{
			return $this->failed("The points you are trying to convert is not sufficient. Customer current points is {$avail_points} you are trying to convert {$params['points']} point(s).");
		}
		
		$amount_earned = $points['amount_earned'];
		
		
		$new_earned =  ($params['points']/$rewards['point']) *  $rewards['point_amount_conversion'];
		
		$datetime = date('Y-m-d H:i:s');
		$update_data = array(
			'amount_earned' => $amount_earned +  $new_earned,
			'points_earned' => $avail_points - $params['points'],
			'update_date' => $datetime,
			'update_by' => $merchant_username,
		);
		
		
		$this->db->begin_transaction();	
		if (!$this->db->update('customer_points_merchant', $update_data, 'id=' . $points['id'])) {
	        $this->db->rollback_transaction();
            
            $this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to convert points.");
	    }
		
		
		//update customer points customer_points_merchant
		//create a transaction convert/redeem
		
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => $new_earned,
			'points_earned' => $params['points'],
			'description' => "Convert points transaction.",
			'create_by' => $merchant_username,
			'type' => 'CONVERT',
		);
	
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to convert points. Please try again.");
	    }

		$transaction_id = $this->db->lastInsertId();
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'points' => $params['points'],
			'transaction_id' => $transaction_id,
			'create_by' => $merchant_username,
		);

		if (!$this->db->insert('merchant_convert_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error converting points '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to convert points. Please try again.");
	    }


		$this->db->commit_transaction();
		
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("Your {$params['points']} pt(s) has been converted to $ {$new_earned} from {$merchant_name}", $email_address, "Convert Points Transaction");
		
		return $this->success("Successfully converted points.");
	}
	
	
	//merchant_id, punch_code, hole_no, pin, customer_ref email or mobile no.
	public function claim_punch_card_freebie($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}	
		$merchant_id =$params['merchant_id'];
		$punch_code =$params['punch_code'];
		$hole_no = $params['hole_no'];
		$pin = $params['pin'];
		
		$merchant_username = $this->get_username_by_reference($merchant_id, "merchant");
		$merchant_username= $merchant_username['username'];
		
		
		$cmd ="SELECT * FROM merchant_punch_card WHERE punch_code={$this->db->db_escape($punch_code)} AND merchant_id={$merchant_id}";
		
		$data = $this->db->fetchRow($cmd);
		
		$startDate = new DateTime($data['start_date']);
		$endDate = new DateTime($data['end_date']);
		
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);	
		if(!($current_date >= $startDate && $current_date<= $endDate))
		{
			return $this->failed("Punch Card already expired. Please check the validity.");
		}
		
		if($pin != $data['pin'])
		{
			return $this->failed("Please enter a valid PIN.");
		}
		
		//check if hole no has a freebie
		$cmd = "SELECT * FROM merchant_punch_card_detail WHERE punch_card_id ={$data['id']} AND hole_no={$hole_no}";
		
		$det =  $this->db->fetchRow($cmd);
		
		
		if(!isset($det['id']))
		{
			return $this->failed("The hole you specify does not have a valid freebie.");
		}
		
		
		$detail_id =$det['id'];
		
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		
		$cmd = "SELECT * FROM merchant_punch_card_claim WHERE punch_card_id ={$data['id']} AND punch_card_detail_id={$detail_id} AND customer_id={$customer_id} AND merchant_id={$merchant_id}";
		
		
		$claim = $this->db->fetchRow($cmd);
		if(!isset($claim['id'])){
			return $this->failed("The information you supply. Does not have an active claim of freebie.");
		}
		
		if($claim['status'] =="C")
		{
			return $this->failed("Freebie has already been claimed.");
		}
		
		if($claim['status'] =="V")
		{
			return $this->failed("Freebie has already neen voided.");
		}
		
		$this->db->begin_transaction();
		
		$datetime = date('Y-m-d H:i:s');
		$cmd =" UPDATE merchant_punch_card_claim SET status='C', update_by='{$merchant_username}', update_date='{$datetime}' WHERE id ={$claim['id']}";
		
		if (!$this->db->query($cmd)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error claiming freebies '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to claim freebies.");
        }
        
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => 0,
			'points_earned' => 0,
			'description' => "Transaction Punch Card Claim Freebie Hole No. {$hole_no}",
			'create_by' => $merchant_username,
			//'type' => 'PUNCHCARD',
			'type' => 'CLAIMED PUNCHCARD',
		);
	
		
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating punch code transaction '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to claim punch code transaction. Please try again.");
	    }
		
		$transaction_id = $this->db->lastInsertId();
		$this->db->commit_transaction();
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("Your punchcard freebie {$det['description']}from {$merchant_name} has been claimed.", $email_address, "Claim Freebie");
		
		return $this->success("You have succefully claim the freebie.");
	}
	
	//merchant_id, punch_code, customer_ref mobile or email of customer
	public function my_merchant_get_punchcard_information($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		

		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		

		
		$cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, trans.`total_hole` as accumulated_hole, start_date, end_date
FROM merchant_punch_card_customer_total trans
LEFT JOIN `merchant_punch_card` p ON trans.punch_card_id = p.id WHERE trans.customer_id={$customer_id} AND p.merchant_id={$params['merchant_id']}";
		$cmd .=" AND p.punch_code={$this->db->db_escape($params['punch_code'])}";
		$data = $this->db->fetchRow($cmd);
		
		if(count($data) >0)
		{
			$startDate = new DateTime($data['start_date']);
			$endDate = new DateTime($data['end_date']);
			
			$current_date = date('Y-m-d');
			$current_date = new DateTime($current_date);	
			if(!($current_date >= $startDate && $current_date<= $endDate))
			{
				return $this->failed("Punch Card already expired. Please check the validity.");
			}
		}else{
			$cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, 0 as accumulated_hole, start_date, end_date FROM merchant_punch_card p WHERE p.merchant_id={$params['merchant_id']}";
			$cmd .=" AND p.punch_code={$this->db->db_escape($params['punch_code'])}";
			$data = $this->db->fetchRow($cmd);
			
			$startDate = new DateTime($data['start_date']);
			$endDate = new DateTime($data['end_date']);
			
			$current_date = date('Y-m-d');
			$current_date = new DateTime($current_date);	
			if(!($current_date >= $startDate && $current_date<= $endDate))
			{
				return $this->failed("Punch Card already expired. Please check the validity.");
			}
		}
		$punch_card_id =$data['punch_card_id'];
		//$cmd ="SELECT hole_no, description FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}";
		$cmd = "SELECT hole_no, description, CASE c.status WHEN 'C' THEN 'Claimed' ELSE 'NA' END AS claim_status FROM merchant_punch_card_detail d
LEFT JOIN merchant_punch_card_claim c ON d.id=c.punch_card_detail_id
	WHERE d.punch_card_id={$punch_card_id}
		";
		
		
		
		$details = $this->db->fetchAll($cmd);
		$info = $data;
		$info['freebies'] =  $details;
		
		$results[] = $info;
		
		return $this->success($results);
	}
	
	//merchant_id
	public function get_merchant_rewards($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		$cmd ="SELECT reward_code, reward_name, point as required_points, point_amount_conversion,
amount as equivalent_amount, amount as required_amount,amount_point_conversion, goal_point, description FROM merchant_rewards WHERE status='A' AND merchant_id={$params['merchant_id']} ";
		$data = $this->db->fetchAll($cmd);
		return $this->success($data);
	}
	
	//merchant_id
	public function get_merchant_punch_card($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		$cmd ="SELECT * FROM merchant_punch_card WHERE status='A' AND merchant_id={$params['merchant_id']} AND CURDATE()>=start_date AND end_date>=CURDATE()";
		$data = $this->db->fetchAll($cmd);
		
		$results ="";
		foreach($data as $d)
		{
			$punch_card_id =$d['id'];
			$cmd ="SELECT hole_no, description FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}";
			$details = $this->db->fetchAll($cmd);
			$info = $d;
			$info['freebies'] =  $details;
			
			$results[] = $info;
		}
		return $this->success($results);
	
	}
	
	//merchant_id
	public function get_merchant_deals($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		$cmd ="SELECT * FROM merchant_deals WHERE status='A' AND merchant_id={$params['merchant_id']} AND CURDATE()>=start_date AND end_date>=CURDATE()";
		
		$deals = $this->db->fetchAll($cmd);
		
		return $this->success($deals);
	}
	
	private function get_punch_info($punch_code, $merchant_id)
	{
		$cmd = "SELECT * FROM merchant_punch_card WHERE status='A'";
		$cmd .= " AND punch_code=" . $this->db->db_escape($punch_code);
		$cmd .=" AND merchant_id={$merchant_id}";
		
		return $this->db->fetchRow($cmd);
	}
	
	//customer_id, punch_code
	public function get_punch_card_status($params)
	{
		// $cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, CASE WHEN trans.`total_hole`=NULL  THEN trans.`total_hole` ELSE 0 END as accumulated_hole, start_date, end_date
// FROM `merchant_punch_card` p 
// LEFT JOIN merchant_punch_card_customer_total trans ON p.id=trans.punch_card_id  AND trans.customer_id={$params['customer_id']}
		// WHERE p.punch_code='{$params['punch_code']}'";
// 	
		// $data = $this->db->fetchAll($cmd);
// 		
		// $results ="";
		// foreach($data as $d)
		// {
			// $punch_card_id =$d['punch_card_id'];
			// $cmd ="SELECT hole_no, description FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}";
			// $details = $this->db->fetchAll($cmd);
			// $info = $d;
			// $info['freebies'] =  $details;
// 			
			// $results[] = $info;
		// }
		$cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, trans.`total_hole` as accumulated_hole, start_date, end_date
FROM merchant_punch_card_customer_total trans
LEFT JOIN `merchant_punch_card` p ON trans.punch_card_id = p.id WHERE trans.customer_id={$params['customer_id']}";
		$cmd .=" AND p.punch_code={$this->db->db_escape($params['punch_code'])}";
		$data = $this->db->fetchRow($cmd);
		
		if(count($data) >0)
		{
			$startDate = new DateTime($data['start_date']);
			$endDate = new DateTime($data['end_date']);
			
			$current_date = date('Y-m-d');
			$current_date = new DateTime($current_date);	
			if(!($current_date >= $startDate && $current_date<= $endDate))
			{
				return $this->failed("Punch Card already expired. Please check the validity.");
			}
		}else{
			//$cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, 0 as accumulated_hole, start_date, end_date FROM merchant_punch_card p 
			//WHERE p.merchant_id={$params['merchant_id']}";
			$cmd ="SELECT p.id as punch_card_id, p.punch_code, p.description,number_of_punch as goal_no_of_hole, 0 as accumulated_hole, start_date, end_date FROM merchant_punch_card p 
			WHERE ";
			//$cmd .=" AND p.punch_code={$this->db->db_escape($params['punch_code'])}";
			$cmd .=" p.punch_code={$this->db->db_escape($params['punch_code'])}";
			$data = $this->db->fetchRow($cmd);
			
			$startDate = new DateTime($data['start_date']);
			$endDate = new DateTime($data['end_date']);
			
			$current_date = date('Y-m-d');
			$current_date = new DateTime($current_date);	
			if(!($current_date >= $startDate && $current_date<= $endDate))
			{
				return $this->failed("Punch Card already expired. Please check the validity.");
			}
		}
		
		$punch_card_id =$data['punch_card_id'];
		//$cmd ="SELECT hole_no, description FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}";
		$cmd = "SELECT hole_no, description, CASE c.status WHEN 'C' THEN 'Claimed' ELSE 'NA' END AS claim_status 
		FROM merchant_punch_card_detail d
LEFT JOIN merchant_punch_card_claim c ON d.id=c.punch_card_detail_id
	WHERE d.punch_card_id={$punch_card_id}
		";


		$details = $this->db->fetchAll($cmd);
		$info = $data;
		$info['freebies'] =  $details;
		
		$results[] = $info;
		
		return $this->success($results);
		
	}
	
	
	//transaction_id, merchant_id
	public function void_punch_card_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction had already been voided.");
		}
		
		if($trans_info['type'] != 'PUNCHCARD')
		{
			return $this->failed("Invalid transaction type to void. Please void a Punch Card transaction type.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		$customer_id = $customer_info['id'];
		
		//customer_transactions
		$this->db->begin_transaction();
		
		$datetime = date('Y-m-d H:i:s');
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding punch card transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
        
        $update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('merchant_punch_card_transactions', $update_data, 'transaction_id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction merchant_punch_card_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		$cmd ="SELECT merchant_punch_card_id as id FROM merchant_punch_card_transactions WHERE transaction_id={$transaction_id} AND customer_id={$customer_id} AND merchant_id={$merchant_id}";
		$punch_card_info = $this->db->fetchRow($cmd);
		
        
		$datetime = date('Y-m-d H:i:s');
		$cmd =" UPDATE merchant_punch_card_customer_total SET total_hole=total_hole-1"; 
		$cmd .=" 	,update_by = ".$this->db->db_escape($merchant_username);
		$cmd .=" 	,update_date = ".$this->db->db_escape($datetime);
		$cmd .=" WHERE merchant_id={$merchant_id} AND punch_card_id={$punch_card_info['id']} AND customer_id={$customer_id}" ;
		
		if(!$this->db->query($cmd))
		{
			$this->db->rollback_transaction();
        	$datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error voiding punch card transaction merchant_punch_card_customer_total'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
    		return $this->failed("Unable to process punch card total transaction. Please try again.");
		}
		
        
		$cmd ="SELECT total_hole FROM merchant_punch_card_customer_total WHERE merchant_id={$merchant_id} AND punch_card_id={$punch_card_info['id']}";
		$data = $this->db->fetchRow($cmd);
		
		//check if no_of_hole has a freebie, if has freebie then insert to merchant_punch_card_detail
		$cmd ="SELECT id FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_info['id']} AND hole_no={$data['total_hole']}+1";
		$data = $this->db->fetchRow($cmd);
		if(isset($data['id']))
		{
			// $cmd ="SELECT id, status FROM merchant_punch_card_claim WHERE punch_card_id={$punch_card_info['id']}";
			// $cmd .=" AND customer_id={$customer_id} AND punch_card_detail_id={$data['id']}";
			// $data = $this->db->fetchRow($cmd);
			
			$cmd ="UPDATE merchant_punch_card_claim SET status='V', update_by='{$merchant_username}',update_date='{$datetime}'
			 WHERE punch_card_id={$punch_card_info['id']}";
			$cmd .=" AND customer_id={$customer_id}";
			$cmd .=" AND punch_card_detail_id = {$data['id']}";
			$cmd .=" AND merchant_id = {$merchant_id}";

			if (!$this->db->query($cmd)) {
		        $this->db->rollback_transaction();
		        $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error voiding punch card transaction merchant_punch_card_claim'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		    	return $this->failed("Unable to void punch card transaction. Please try again.");
		    }
		}
		
		$this->db->commit_transaction();
		return $this->success("Punch Card transaction has been successfully voided.");
        
	}
	

	
	
	//merchant_id, amount, customer_ref can be mobile or email of customer, punch_card_code
	public function create_punch_card_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$amount =$params['amount'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$merchant_username= $merchant_username['username'];
			
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		

		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		$merchant_id = $params['merchant_id'];
		$punch_code = $params['punch_card_code'];
		
		$punch_card_info = $this->get_punch_info($punch_code, $merchant_id);
		
		if(!isset($punch_card_info['id']))
		{
			return $this->failed("Invalid Punch Card Code information.");
		}
		
		
		$startDate = new DateTime($punch_card_info['start_date']);
		$endDate = new DateTime($punch_card_info['end_date']);
		
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);	
		if(!($current_date >= $startDate && $current_date<= $endDate))
		{
			return $this->failed("Punch Card is expired.");
		}
		
		// print_r($deal_info['start_date']);
		// die();

		//customer_transactions
		// include type = DEAL

		//add customer_transactions 
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => $amount,
			'points_earned' => 0,
			'description' => "Transaction Punch Card {$punch_code}",
			'create_by' => $merchant_username,
			'type' => 'PUNCHCARD',
		);
	
		$this->db->begin_transaction();
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating punch code transaction '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process punch code transaction. Please try again.");
	    }
		$transaction_id = $this->db->lastInsertId();
		
		
		//merchant_punch_card_transactions
		$insert_data = array(
			'merchant_punch_card_id' => $punch_card_info['id'],
			'merchant_id' => $merchant_id,
			'amount' => $amount,
			'customer_id' => $customer_id,
			'transaction_id' => $transaction_id,
			'status' => 'A',
			'create_by' => $merchant_username,
			'number_of_hole' => 1,
		);
		if (!$this->db->insert('merchant_punch_card_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating punch card transaction merchant_deal_transactions'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process punch card transaction. Please try again.");
	    }
		
		//check if merchant_punch_card_customer_total already exist, update or create
		$cmd ="SELECT * FROM merchant_punch_card_customer_total WHERE merchant_id={$merchant_id} AND punch_card_id={$punch_card_info['id']} AND customer_id={$customer_id}";
		$data = $this->db->fetchRow($cmd);
		if(isset($data['id']))
		{
			$datetime = date('Y-m-d H:i:s');
			$cmd =" UPDATE merchant_punch_card_customer_total SET total_hole=total_hole+1"; 
			$cmd .=" 	,update_by = ".$this->db->db_escape($merchant_username);
			$cmd .=" 	,update_date = ".$this->db->db_escape($datetime);
			$cmd .=" WHERE merchant_id={$merchant_id} AND punch_card_id={$punch_card_info['id']} AND customer_id={$customer_id}" ;
			
			if(!$this->db->query($cmd))
			{
				$this->db->rollback_transaction();
	        	$datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating punch card transaction merchant_deal_transactions'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    		return $this->failed("Unable to process punch card total transaction. Please try again.");
			}
		}else{
			$insert_data = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $customer_id,
				'punch_card_id' => $punch_card_info['id'],
				'total_hole' => 1,
				'create_by' => $merchant_username,
				'status' => 'A',
			);
			if (!$this->db->insert('merchant_punch_card_customer_total', $insert_data)) {
		        $this->db->rollback_transaction();
		        $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating punch card transaction merchant_punch_card_customer_total'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		    	return $this->failed("Unable to process punch card transaction. Please try again.");
		    }

		}
		
		$cmd ="SELECT total_hole FROM merchant_punch_card_customer_total WHERE merchant_id={$merchant_id} AND punch_card_id={$punch_card_info['id']}";
		$data = $this->db->fetchRow($cmd);
		
		//check if no_of_hole has a freebie, if has freebie then insert to merchant_punch_card_detail
		$cmd ="SELECT id FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_info['id']} AND hole_no={$data['total_hole']}";
		$data = $this->db->fetchRow($cmd);
		if(isset($data['id']))
		{
			// $cmd ="SELECT id, status FROM merchant_punch_card_claim WHERE punch_card_id={$punch_card_info['id']}";
			// $cmd .=" AND customer_id={$customer_id} AND punch_card_detail_id={$data['id']}";
			// $data = $this->db->fetchRow($cmd);
			$insert_data = array(
				'punch_card_id' => $punch_card_info['id'],
				'customer_id' => $customer_id,
				'merchant_id' => $merchant_id,
				'punch_card_detail_id' => $data['id'],
				'create_by' => $merchant_username,
				'status' => 'A',
			);
			if (!$this->db->insert('merchant_punch_card_claim', $insert_data)) {
		        $this->db->rollback_transaction();
		        $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating punch card transaction merchant_punch_card_claim'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		    	return $this->failed("Unable to process punch card transaction. Please try again.");
		    }
		}
		
		$this->db->commit_transaction();
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		$this->send_mail_notification("Your punchcard transaction for {$punch_code} from {$merchant_name} has been processed.", $email_address, "Punch Card Transaction");
		
		
		return $this->success("Punch Card transaction has been procesed.");
		
	}
	
	public function delete_punch_card($id)
	{
		
		
		$cmd = "SELECT id FROM merchant_punch_card_transactions WHERE merchant_punch_card_id ={$id}";
		$trans = $this->db->fetchRow($cmd);
		if(isset($trans['id']))
		{
			return $this->failed("You cannot delete this punch card. Transaction has already been made.");
		}
			
		

		$delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('merchant_punch_card', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete punch card '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete rewards");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete Punch Card with id'. $id  ,'PUNCHCARD',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted Punch Card");
        }
	}
	
	public function get_list_punch_card($merchant_id)
	{
		// $cmd="SELECT m.id as punch_id,merchant_id, punch_code, m.description, number_of_punch, cost, pin, start_date, end_date,status,m.create_date,
// md.id as punch_id_detail,hole_no,md.description as detail_description 
// FROM merchant_punch_card m LEFT JOIN merchant_punch_card_detail md ON m.id=md.punch_card_id
// WHERE m.merchant_id={$merchant_id}";
		$cmd="SELECT m.id as punch_id,merchant_id, punch_code, m.description, number_of_punch, cost, pin, start_date, end_date,status,m.create_date
		FROM merchant_punch_card m 
		WHERE m.merchant_id={$merchant_id} AND m.status='A'";
				
		$data = $this->db->fetchAll($cmd);
		
		$pcs = array();
		foreach($data  as $r)
		{
			$cmd = "SELECT md.id as punch_id_detail,hole_no,md.description as detail_description FROM merchant_punch_card_detail md WHERE punch_card_id={$r['punch_id']}";
			$details = $this->db->fetchAll($cmd);

			$r['details'] = $details;
			$pcs[] = $r;
		}
		
		return $this->success($pcs);
	}
	
	public function update_punch_card($params)
	{
		$startDate = new DateTime($params['start_date']);
		$endDate = new DateTime($params['end_date']);
		
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);	
		if(!($current_date >= $startDate && $current_date<= $endDate))
		{
			return $this->failed("Please check your validity date.");
		}
		if($startDate > $endDate)
		{
			return $this->failed("Please check your validity date.");
		}
		
		if($params['number_of_holes'] <=0)
		{
			return $this->failed("Please check your total no. of holes.");
		}
		
		if($params['description'] =="")
		{
			return $this->failed("Please check your description");
		}
		
		$this->db->begin_transaction();
		
		//insert merchant_punch_card
		$punch_card_id = $params['punch_card_id'];
		$update_data = array(
			'merchant_id' => $params['merchant_id'],
			//'punch_code' => $this->generate_profile_code("merchant_punch_card", "PC", "punch_code"),
			'description' => $params['description'],
			'number_of_punch' => $params['number_of_holes'],
			'cost' => $params['cost'],
			'pin' => $params['pin'],
			'start_date' => $params['start_date'],
			'end_date' => $params['end_date'],
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $_SESSION['username'],
			'status' =>  'A'
		);
		
		$this->db->begin_transaction();
		if (!$this->db->update('merchant_punch_card', $update_data, 'id=' . $punch_card_id)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error update punch card merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
	    	return $this->failed("Unable to update punch card. Please try again1.");
	    }
		//$punch_card_id = $this->db->lastInsertId();
		
		//insert merchant_punch_card_detail
		

			$punch_card_detail = unserialize($params['punch_card_details']);
			
		if($punch_card_detail!="")
		{
			$this->db->query("DELETE FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}");
			
			foreach ($punch_card_detail as  $p)
			{
					$insert_data = array(
						'punch_card_id' => $punch_card_id,
						'hole_no' =>  $p[0],
						'description' => $p[1],
						'create_by' => $_SESSION['username'],
					);
					
					if (!$this->db->insert('merchant_punch_card_detail', $insert_data)) {
				        $this->db->rollback_transaction();
				        $datetime = date('Y-m-d H:i:s');
						$this->save_to_action_logs('Error creating punch card detail merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
				    	return $this->failed("Unable to punch card. Please try again.");
				    }
			}
		}else{
			$this->db->query("DELETE FROM merchant_punch_card_detail WHERE punch_card_id={$punch_card_id}");
		}
		$this->db->commit_transaction();
		
		return $this->success("Punch card has been updated.");
	}
	
	public function create_punch_card($params)
	{
		$startDate = new DateTime($params['start_date']);
		$endDate = new DateTime($params['end_date']);
		
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);	
		if(!($current_date >= $startDate && $current_date<= $endDate))
		{
			return $this->failed("Please check your validity date.");
		}
		if($startDate > $endDate)
		{
			return $this->failed("Please check your validity date.");
		}
		
		if($params['number_of_holes'] <=0)
		{
			return $this->failed("Please check your total no. of holes.");
		}
		
		if($params['description'] =="")
		{
			return $this->failed("Please check your description");
		}
		
		$this->db->begin_transaction();
		
		//insert merchant_punch_card
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'punch_code' => $this->generate_profile_code("merchant_punch_card", "PC", "punch_code"),
			'description' => $params['description'],
			'number_of_punch' => $params['number_of_holes'],
			'cost' => $params['cost'],
			'pin' => $params['pin'],
			'start_date' => $params['start_date'],
			'end_date' => $params['end_date'],
			'status' =>  'A'
		);
		
		$this->db->begin_transaction();
		if (!$this->db->insert('merchant_punch_card', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating punch card merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
	    	return $this->failed("Unable to punch card. Please try again.");
	    }
		$punch_card_id = $this->db->lastInsertId();
		
		//insert merchant_punch_card_detail
		$punch_card_detail = unserialize($params['punch_card_details']);
		if($punch_card_detail!="")
		{
			
			foreach ($punch_card_detail as  $p)
			{
				$insert_data = array(
					'punch_card_id' => $punch_card_id,
					'hole_no' =>  $p[0],
					'description' => $p[1],
					'create_by' => $_SESSION['username'],
				);
				
				if (!$this->db->insert('merchant_punch_card_detail', $insert_data)) {
			        $this->db->rollback_transaction();
			        $datetime = date('Y-m-d H:i:s');
					$this->save_to_action_logs('Error creating punch card detail merchant id '. $params['merchant_id']  ,'ERROR',$datetime,$_SESSION['username'],'');
			    	return $this->failed("Unable to punch card. Please try again.");
			    }
			}
		}
		$this->db->commit_transaction();
		
		return $this->success("Punch card has been created.");
		
	}
	
	//get_transactions of customer todo
	//customer_id, page_number, limit
	public function get_my_transactions($params)
	{
		//results should also include merchant name
		$cmd ="SELECT m.merchant_name,ct.id as transaction_id, amount, points_earned, type as transaction_type, ct.create_date  as transaction_date,
			description,CASE ct.status WHEN 'A' THEN 'Completed' WHEN 'V' THEN 'VOID' ELSE 'UNKNOWN' END as status
			FROM customer_transactions ct
			LEFT JOIN merchants m on ct.merchant_id=m.id
			WHERE ct.customer_id ={$params['customer_id']}
			ORDER BY ct.create_date DESC
			LIMIT {$params['page_number']},{$params['limit']}
		";
		
		$data = $this->db->fetchAll($cmd);
		
		$cmd_count = "SELECT count(ct.id) as row_count
			FROM customer_transactions ct
			LEFT JOIN merchants m on ct.merchant_id=m.id
			WHERE ct.customer_id ={$params['customer_id']}";
		
		$count = $this->db->fetchRow($cmd_count);
		$data['record_count'] =$count['row_count'];
		
		return $this->success($data);
	}
	
	//merchant_id, transaction_id
	public function void_deal_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction had already been voided.");
		}
		
		if($trans_info['type'] != 'PROMOTION')
		{
			return $this->failed("Invalid transaction type to void. Please void a PROMOTION transaction type.");
		}
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		$customer_id = $customer_info['id'];
		
		//customer_transactions
		$this->db->begin_transaction();
		
		$datetime = date('Y-m-d H:i:s');
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		//merchant_deal_transactions
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('merchant_deal_transactions', $update_data, 'transaction_id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction merchant_deal_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		
		$cmd ="SELECT * FROM merchant_deal_transactions WHERE transaction_id={$transaction_id}";
		$row =$this->db->fetchRow($cmd);
		
		$amount = $row['amount'];
		$points = $row['points'];
        
		$redeem_type =$row['redeem_type'];
        
		if($redeem_type != "other")
		{
			if(!$this->update_customer_points($merchant_id, $customer_id, $merchant_username, $amount, $points))
			{
				$this->db->rollback_transaction();
				$datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating promo '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return $this->failed("Unable to create promo transaction.");
			}
		}
	
		$cmd = "UPDATE merchant_deal_number_of_use SET  total_use=total_use-1,update_date='{$datetime}',update_by='{$merchant_username}' WHERE merchant_deal_id = {$row['merchant_deal_id']}";
		if (!$this->db->query($cmd))
		{
			$this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error voiding deal transaction merchant_deal_number_of_use'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return false;   
		}
		
		$this->db->commit_transaction();
		return $this->success("Promotion transaction has been voided.");
		
		//merchant_deal_number_of_use
	}
		

		
	//customer_id, page_number, limit
	public function api_get_my_merchants($params)
    {
        //$cmd = "SELECT m.*, cp.amount_earned, points_earned, cp.update_date
// FROM  
// customer_points_merchant cp LEFT JOIN merchants m on cp.merchant_id=m.id AND cp.customer_id={$params['customer_id']}";
        
       $cmd =" SELECT m.*, cp.amount_earned, points_earned, cp.update_date FROM merchants m 
		INNER JOIN merchant_customers mc ON m.id=mc.merchant_id AND mc.customer_id='{$params['customer_id']}'
		INNER JOIN customer_points_merchant cp ON m.id=cp.merchant_id AND cp.customer_id='{$params['customer_id']}'
		";
		
		if($params['value'] !="")
		{
			$cmd.=" WHERE (m.merchant_name LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd.=" OR m.zip LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd.=" )";
		}
		
        
    	$cmd .=" ORDER BY m.merchant_name ASC LIMIT {$params['page_number']},{$params['limit']}";

        $result = $this->db->fetchAll($cmd);

		$cmd_count =" SELECT count(m.id) as row_count FROM merchants m 
		INNER JOIN merchant_customers mc ON m.id=mc.merchant_id AND mc.customer_id='{$params['customer_id']}'
		INNER JOIN customer_points_merchant cp ON m.id=cp.merchant_id AND cp.customer_id='{$params['customer_id']}'
		";
		
		if($params['value'] !="")
		{
			$cmd_count.=" WHERE (m.merchant_name LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd_count.=" OR m.zip LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd_count.=" )";
		}
		
		$count = $this->db->fetchRow($cmd_count);
		$result['record_count'] =$count['row_count'];
		
        return $this->success($result);
    }
	
	
	//merchant_id, amount, customer_ref can be mobile or email of customer, deal_code, user_info, redeem_type
	public function create_deal_transaction($params)
	{
		
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		//$amount =$params['amount'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$merchant_username= $merchant_username['username'];
			
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		

		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		$customer_id = $customer_info['id'];
		$merchant_id = $params['merchant_id'];
		$deal_code = $params['deal_code'];
		$redeem_type = $params['redeem_type'];
		
		$deal_info = $this->get_deal_info($deal_code, $merchant_id);
		

		if(!isset($deal_info['id']))
		{
			return $this->failed("Invalid deal information.");
		}
		
		$startDate = new DateTime($deal_info['start_date']);
		$endDate = new DateTime($deal_info['end_date']);
		
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);	
		if(!($current_date >= $startDate && $current_date<= $endDate))
		{
			return $this->failed("Deal has been expired.");
		}
		
		if($redeem_type=="amount")
		{
			if($deal_info['required_amount']<=0)
			{
				return $this->failed("Promotion cannot be redeem by amount.");
			}
		}
		
		if($redeem_type=="points")
		{
			if($deal_info['required_points']<=0)
			{
				return $this->failed("Promotion cannot be redeem by points.");
			}
		}
		
		$points = 0;
		$amount = 0;
		if($redeem_type =="points")
		{
			$points = $deal_info['required_points'];
		}else{
			$amount = $deal_info['required_amount'];
		}	
		
		if($redeem_type != "other")
		{
			
			if($deal_info['allow_gift'] != 1)
			{
				return $this->failed("This promotion cannot be claimed by gift points/amount.");
			}
			
			$cmd ="SELECT id,points_earned, amount_earned FROM customer_points_merchant WHERE customer_id={$customer_id} AND merchant_id={$merchant_id}";
			$points_sys =$this->db->fetchRow($cmd);
			if(!isset($points_sys['points_earned']))
			{
				return $this->failed("There are no available points/amount to redeem.");
			}
			if($redeem_type =="points")
			{
				$avail_points = $points_sys['points_earned'];
				if($points > $avail_points)
				{
					$points = number_format($points,2);
					$avail_points = number_format($avail_points,2);
					return $this->failed("The points you are trying to redeem is not sufficient. Customer current points is {$avail_points} you are trying to redeem {$points} point(s).");
				}
			}
			if($redeem_type =="amount")
			{
				$avail_amount = $points_sys['amount_earned'];
				if($amount > $avail_amount)
				{
					$amount = number_format($amount,2);
					$avail_amount = number_format($avail_amount,2);
					return $this->failed("The amount you are trying to redeem is not sufficient. Customer current amount earned is $ {$avail_amount} you are trying to redeem {$amount}.");
				}
			}
		}else{
			if($deal_info['allow_other_payment']==0)
			{
				return $this->failed("This promotion cannot be claimed by other payment.");
			}
		}
		
		
		
	
		
		// print_r($deal_info['start_date']);
		// die();

		//customer_transactions
		// include type = DEAL

		
		
		//add customer_transactions 
		$insert_data = array(
			'merchant_id' => $merchant_id,
			'customer_id' => $customer_id,
			'level' => -1,
			'amount' => $amount,
			//'points_earned' => 0,
			'points_earned' => $points,
			'description' => "Claim Promotion {$deal_code}",
			'create_by' => $merchant_username,
			'type' => 'PROMOTION',
		);
	
		$this->db->begin_transaction();
		if (!$this->db->insert('customer_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating deal transaction '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process deal transaction. Please try again.");
	    }
		$transaction_id = $this->db->lastInsertId();
		
		//merchant_deal_transactions
		$insert_data = array(
			'merchant_deal_id' => $deal_info['id'],
			'amount' => $amount,
			'customer_id' => $customer_id,
			'transaction_id' => $transaction_id,
			'status' => 'A',
			'create_by' => $merchant_username,
			'redeem_type' => $redeem_type,
			'points' => $points,
		);
		if (!$this->db->insert('merchant_deal_transactions', $insert_data)) {
	        $this->db->rollback_transaction();
	        $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error creating deal transaction merchant_deal_transactions'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
	    	return $this->failed("Unable to process deal transaction. Please try again.");
	    }
			
		
		//update if exist customer_points_merchant total_amount and points_earned and amount_earned accumulated per merchant
		if($redeem_type != "other")
		{
			if(!$this->update_customer_points($merchant_id, $customer_id, $merchant_username, $amount, $points, "DEDUCT"))
			{
				$this->db->rollback_transaction();
				$datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating promo '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return $this->failed("Unable to create promo transaction.");
			}
		}
		//merchant_deal_number_of_use
		if(!$this->update_deal_number_of_use($deal_info['id'], $merchant_username, $customer_info))
		{
			return $this->failed("Unable to process deal transaction. Please try again.");
		}
		
		
		$profile =$this->get_profile("merchants",$merchant_id);
		$merchant_name = $profile['merchant_name']; 
		
		$email_address = $customer_info['email'];
		
		$this->send_mail_notification("You promo transaction {$deal_code} has been processed from {$merchant_name}", $email_address, "Promo Transaction");
		
		
		$this->db->commit_transaction();
		return $this->success("Promo transaction has been processed.");
	}



	private function update_deal_number_of_use($deal_id, $merchant_username, $customer_info)
	{
		$cmd ="SELECT * FROM merchant_deal_number_of_use WHERE merchant_deal_id={$deal_id}";
		$row =$this->db->fetchRow($cmd);
		if(isset($row['id']))
		{
			//update number_of_use
			$update_data = array(
				'total_use' => $row['total_use'] + 1,
				'update_date' => date('Y-m-d H:i:s'),
				'update_by' => $merchant_username
			);
			if (!($this->db->update('merchant_deal_number_of_use', $update_data, 'id=' . $row['id']))) {
	            $this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating deal transaction merchant_deal_number_of_use'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		    	return false;   
	        }
		}else
		{
			//insert_record
			$insert_data =  array(
				'merchant_deal_id' => $deal_id,
				'total_use' => 1,
				'create_by' => $merchant_username,
			); 
			if (!$this->db->insert('merchant_deal_number_of_use', $insert_data)) {
		        $this->db->rollback_transaction();
		        $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error updating deal transaction merchant_deal_number_of_use'. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		    	return false;
	    	}
		}	
		return true;
	}
	

	

	
	
	private function get_deal_info($deal_code, $merchant_id)
	{
		$cmd = "SELECT * FROM merchant_deals WHERE status='A'";
		$cmd .= " AND deal_code=" . $this->db->db_escape($deal_code);
		$cmd .=" AND merchant_id={$merchant_id}";
		
		return $this->db->fetchRow($cmd);
	}
	

	
	protected function generate_profile_code($table, $prefix ="DEAL", $field_name = "deal_code") {
        do {
        	//$customer_code = uniqid(time(), true);
            //$code = $prefix.uniqid(time());
            $code = $prefix.uniqid();
            $check = $this->db->fetchRow("select id from {$table} where {$field_name}='{$code}'");
        } while (isset($check['id']));
        return $code;
    }
	
	public function delete_deal($id)
	{
		
		
		$deal_id = $id;
		
		$cmd = "SELECT id FROM merchant_deal_transactions WHERE merchant_deal_id ={$deal_id}";
		$trans = $this->db->fetchRow($cmd);
		if(isset($trans['id']))
		{
			return $this->failed("You cannot delete this promotion. Transaction has already been made.");
		}
		
		
		$delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('merchant_deals', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete deals '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete rewards");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete Deals with id'. $id  ,'REWARDS',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted Deals");
        }
	}
	
	//id,merchant_id, deal_code, is_unlimited, number_of_use, start_date, end_date, description 
	public function update_deal($params)
	{
		$params = array_map('trim', $params);
		if(!is_numeric($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		$update_data = $params;
		if($this->check_if_profile_exist_by_merchant("merchant_deals", "deal_code", $params['deal_code'],$params['id'], $params['merchant_id']))
        {
	        return $this->failed('Deal Code has already been used.');
	        die();
        }
		
		if($update_data['start_date'] == "" && $update_data['end_date'] == "")
		{
			return $this->failed("Please enter a valid. Validity dates.");
		}
			
		$start_date = strtotime($params['start_date']);
		$end_date = strtotime($params['end_date']);
		
		if($start_date >= $end_date)
		{
			return $this->failed("Unable to save deal. End Date should always be greater than start date.");
		}
		
		$update_data = array_map('trim', $params);
		unset($update_data['id']);
		unset($update_data['deal_code']);

		$datetime = date('Y-m-d H:i:s');
        $update_data['update_date'] = $datetime;
		$update_data['update_by'] = $_SESSION['username'];
		
		$this->db->begin_transaction();
        if (!$this->db->update('merchant_deals', $update_data, 'id=' . $params['id'])) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating Deals '.$params['deal_code']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to update deals.");
        } else {

            $this->save_to_action_logs('Deal has been updated '. $params['deal_code']  ,'REWARDS',$datetime,$_SESSION['username'],'');
            
            $this->db->commit_transaction();
            return $this->success("Deal has been updated.");
        }
	}
	
	//merchant_id, deal_code, is_unlimited, number_of_use, start_date, end_date, description
	public function create_deals($params)
	{
		$params = array_map('trim', $params);
		if(!is_numeric($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id.");
		}
		
		$insert_data = $params;
		// if($this->check_if_profile_exist_by_merchant("merchant_deals", "deal_code", $params['deal_code'],-1, $params['merchant_id']))
        // {
	        // return $this->failed('Deal Code has already been used.');
	        // die();
        // }
		
		if($insert_data['start_date'] == "" && $insert_data['end_date'] == "")
		{
			return $this->failed("Please enter a valid. Validity dates.");
		}
			
		$start_date = strtotime($params['start_date']);
		$end_date = strtotime($params['end_date']);
		
		if($start_date >= $end_date)
		{
			return $this->failed("Unable to save promotion. End Date should always be greater than start date.");
		}
		
		$insert_data['deal_code'] = $this->generate_profile_code("merchant_deals", "");
		$insert_data['create_by'] = $_SESSION['username'];
		$insert_data['status'] ="A";
		
		$this->db->begin_transaction();
        if (!$this->db->insert('merchant_deals', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new promotion '. $params['deal_code']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new customer.");
        } else {
        	$reference_id = $this->db->lastInsertId();			
			$datetime = date('Y-m-d H:i:s');
			   
			$this->save_to_action_logs('Registered new promotion '. $params['deal_code']  ,'DEALS',$datetime,$_SESSION['username'],'');
			
			$this->db->commit_transaction();
			return $this->success("New promotion has been registered.");
		}
	}

    private function check_if_profile_exist_by_merchant($table, $field, $value, $id = -1, $merchant_id)
    {
        $value =strtoupper($value);
        $cmd ="SELECT id FROM {$table} WHERE ucase({$field})= {$this->db->db_escape($value)} AND id<>{$id} AND status='A' AND merchant_id={$merchant_id}";   
        $row = $this->db->fetchRow($cmd);
        if (!(isset($row['id']))) {
            return false;
        } else { 
            return true;
        }
    }
	
	//merchant_id
	public function get_deals($merchant_id)
	{
		$cmd ="SELECT * FROM merchant_deals WHERE status='A' AND merchant_id={$merchant_id}";
		$data = $this->db->fetchAll($cmd);
		return $this->success($data);
	}
	
	
	//merchant_id, limit, search_value
	public function get_all_customers_per_merchant($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id =$params['merchant_id'];
		$limit = $params['limit'];
		$search_value = $params['search_value'];
		
		
		
		$cmd= "SELECT c.id as customer_id, IFNULL(crlm.level,1) as reward_level, c.customer_code, c.last_name, c.first_name, c.middle_name, c.mobile,c.telephone, c.state, c.city, c.zip, c.country 
		,c.email,c.address, c.udf1,c.udf2,c.udf3, c.create_date, cpm.points_earned, cpm.amount_earned, c.date_of_birth
		FROM customers c 
		LEFT JOIN merchant_customers mc on mc.customer_id = c.id 
		LEFT JOIN customer_points_merchant cpm on cpm.customer_id=mc.customer_id AND cpm.merchant_id=mc.merchant_id
		LEFT JOIN customer_reward_level_merchant crlm ON c.id=crlm.customer_id AND mc.merchant_id=crlm.merchant_id AND crlm.status='A'
		WHERE mc.merchant_id={$merchant_id}";

		if($search_value !="")
		{
			$cmd .=" AND (c.last_name LIKE '%{$search_value}%' ";
			$cmd .=" OR c.first_name LIKE '%{$search_value}%' ";
			$cmd .=" OR c.middle_name LIKE '%{$search_value}%' ";
			$cmd .=" OR c.mobile LIKE '%{$search_value}%' ";
			$cmd .=" OR c.email LIKE '%{$search_value}%' ";
			$cmd .=")";			
		}
	

	
		$customers = $this->db->fetchAll($cmd);
		
		$level = 0;
		$results = array();
		foreach($customers as $c)
		{
		
			if($level != $c['reward_level'])
			{
				$level = $c['reward_level'];

				$rewards = $this->get_reward_info($level, $merchant_id);
			}
			$level = $c['reward_level'];
			$res = $c;
			$res['required_amount'] = $rewards['amount'];
			$res['point_conversion'] = $rewards['amount_point_conversion'];
			$res['reward_code'] = $rewards['reward_code'];
			$res['reward_name'] = $rewards['reward_name'];
			$results[] =$res;
		}
				
		
		
		return $this->success($results);
	}
	
	
	//merchant_id, transaction_id
	public function void_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$transaction_id = $params['transaction_id'];
		
		$merchant_username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		$merchant_username= $merchant_username['username'];
		
		//get customer from customer_transactions
		$trans_info = $this->get_customer_trans_info($merchant_id, $transaction_id);
		
		if(!isset($trans_info['id']))
		{
			return $this->failed("Unable to find transaction. Please pass a valid transaction.");
		}
		
		if($trans_info['status'] =="V")
		{
			return $this->failed("Transaction has been voided.");
		}
		
		
		
		$customer_info = $this->get_customer_info_by_id($trans_info['customer_id']);
		
		$customer_id = $customer_info['id'];
		
		

		
		$points_earned = -$trans_info['points_earned'];
		$amount = -$trans_info['amount'];
		//update customers table points_earned and total_amount accumulated not by merchant
		$update_customer = array(
			'points_earned' => $customer_info['points_earned'] + $points_earned, // update total points earned
			'total_amount' =>  0, 
		);
		
		$datetime = date('Y-m-d H:i:s');
        $update_customer['update_date'] = $datetime;
		$update_customer['update_by'] = $merchant_username;
		
		$customer_id  = $customer_info['id'];
		
		
		$this->db->begin_transaction();
		
		$update_data = array(
			'update_date' => $datetime,
			'update_by' => $merchant_username,
			'status' => 'V'
		);
		if (!$this->db->update('customer_transactions', $update_data, 'id=' . $transaction_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction customer_transactions '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        }
		
		
        if (!$this->db->update('customers', $update_customer, 'id=' . $customer_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error voiding transaction Customer '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return $this->failed("Unable to void transaction.");
        } else {

            $this->save_to_action_logs('Voiding customers table transaction ok '. $customer_info['last_name']." ".$customer_info['first_name']  ,'CUSTOMER',$datetime,$merchant_username,'');
        				
			//update if exist customer_points_merchant total_amount and points_earned and amount_earned accumulated per merchant
			if(!$this->update_customer_points($merchant_id, $customer_id, $merchant_username, $amount, $points_earned))
			{
				$this->db->rollback_transaction();
				$datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error voiding transaction customer_points_merchant '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return $this->failed("Unable to void transaction.");
			}
			
			if(!$this->update_customer_accumulated_points($merchant_id, $customer_id, $merchant_username, $amount, $points_earned))
			{
				$this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
	            $this->save_to_action_logs('Error updating transaction customer_accu_points_merchant '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return $this->failed("Unable to void transaction.");
			}
		}
		
		


		$this->db->commit_transaction();
		return $this->success("Transaction has been voided successfully.");
	}
	
	private function get_customer_trans_info($merchant_id, $transaction_id)
	{
		$cmd ="SELECT * FROM customer_transactions WHERE merchant_id ={$merchant_id} AND id={$transaction_id}";
		return $this->db->fetchRow($cmd);
			
	}
	
	//merchant_id, customer_id, page_number ,limit last 15 as default
	//optional start_date, end_date, transaction_type
	public function get_customer_transactions_per_merchant($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		$merchant_id = $params['merchant_id'];
		$customer_id = $params['customer_id'];
		$limit = $params['limit'];
		$page_number = $params['page_number'];
		if($limit =="")
		{
			$limit = 15;
		}
		
		$cmd = "SELECT id as transaction_id, amount, points_earned, create_date as transaction_date, CASE ct.status WHEN 'A' THEN 'Completed' WHEN 'V' THEN 'VOID' ELSE 'UNKNOWN' END as status, type as transaction_type 
		,ct.description
		FROM customer_transactions ct 
		 WHERE merchant_id={$merchant_id} AND customer_id={$customer_id} ";
		
		if($params['start_date']!="")
		{
			$cmd .=" AND ct.create_date>='{$params['start_date']}'";
			$cmd .=" AND ct.create_date<=DATE_ADD('{$params['end_date']}', INTERVAL 1 DAY)";
		} 
		
		if($params['transaction_type']!="")
		{
			$cmd.=" AND ct.transaction_type='{$params['transaction_type']}'";
		}
		
		$cmd .=" ORDER BY create_date DESC limit {$page_number},{$limit}";
		
		
		
		$trans = $this->db->fetchAll($cmd);
		return $this->success($trans);
	}
	
	//merchant_id, amount, customer_ref can be mobile or email of customer, description 
	public function create_transaction($params)
	{
		$params = array_map('trim', $params);
		if(!isset($params['merchant_id']))
		{
			return $this->failed("Invalid merchant.");
		}
		
		
		$username = $this->get_username_by_reference($params['merchant_id'], "merchant");
		
		$merchant_id = $params['merchant_id'];
		$username= $username['username'];
			
		//check if email or mobile of customer is valid.
		$customer_info = $this->get_customer_info($params['customer_ref']);
		
		$customer_id = $customer_info['id'];
		// print_r($customer_info);
		// die();
		
		if(!isset($customer_info['id']))
		{
			return $this->failed("Customer cannot be found. Please try again.");
		}
		//transaction_by amount or actual points
		
		if($this->process_transaction($params['amount'], $params['merchant_id'], $username, $params['description'], $customer_info))
		{
			$profile =$this->get_profile("merchants",$merchant_id);
			$merchant_name = $profile['merchant_name']; 
		
			$email_address = $customer_info['email'];
			$this->send_mail_notification("Your reward transaction $ {$params['amount']} from {$merchant_name} has been processed.", $email_address, "Add Points Transaction");	
				
			return $this->success("Transaction has been recorded");
		}else{
			return $this->failed("Unable to transact. Please try again.");
		}
		
		
	}

	private function get_customer_level($merchant_id, $customer_id, $username)
	{
		$cmd = "SELECT level FROM customer_reward_level_merchant WHERE merchant_id={$merchant_id} AND customer_id = {$customer_id} AND status='A'";
		$level = $this->db->fetchRow($cmd);
		if(!isset($level['level']))
		{
			//insert record level =1
			$insert_data = array(
				'merchant_id' =>  $merchant_id,
				'customer_id' => $customer_id,
				'level' => 1,
				'create_by' => $username,
			);
			
			if (!$this->db->insert('customer_reward_level_merchant', $insert_data)) {
	            $datetime = date('Y-m-d H:i:s');
	            $this->save_to_action_logs('Error creating new transaction for reward '. $params['mobile']  ,'ERROR',$datetime,$_SESSION['username'],'');
	            return $this->failed("Unable to create new transaction.");
	        } 
			return 1;
		}else{
			return $level['level'];
		}
	}
	
	private function get_next_reward_level_info($merchant_id, $current_level)
	{
		$datetime = date('Y-m-d');
		//$cmd = "SELECT * FROM merchant_rewards WHERE merchant_id ={$merchant_id} AND level>{$current_level} AND '{$datetime}'>=start_date AND '{$datetime}'<=end_date ORDER BY merchant_rewards.level DESC  LIMIT 1 ";
		
		//$cmd = "SELECT * FROM merchant_rewards WHERE merchant_id ={$merchant_id} AND level>{$current_level} AND '{$datetime}'<=end_date ORDER BY merchant_rewards.level DESC  LIMIT 1 ";
		$cmd = "SELECT * FROM merchant_rewards WHERE merchant_id ={$merchant_id} AND level>{$current_level} AND '{$datetime}'<=end_date ORDER BY merchant_rewards.level ASC  LIMIT 1 ";
		
		return $this->db->fetchRow($cmd);
	}
	
	
	private function get_reward_info($level, $merchant_id)
	{
		$cmd ="SELECT * FROM merchant_rewards WHERE level={$level} AND merchant_id={$merchant_id} AND status='A'";
		return $this->db->fetchRow($cmd);
		
	}
	
	private function get_customer_existing_points($merchant_id, $customer_id){
		$cmd ="SELECT * FROM customer_points_merchant WHERE merchant_id={$merchant_id} AND customer_id={$customer_id}";
		return $this->db->fetchRow($cmd);
	}
	
	private function get_customer_existing_accumulated_points($merchant_id, $customer_id)
	{
		$cmd ="SELECT * FROM customer_accu_points_merchant WHERE merchant_id={$merchant_id} AND customer_id={$customer_id}";
		return $this->db->fetchRow($cmd);
	}
	
	private function process_transaction($amount, $merchant_id, $merchant_username, $description, $customer_info)
	{
		//amount_type // can be points or actual_amount
		// get level from customer_reward_level_merchant
		//if not yet defined, put the customer to level 1
		$level = $this->get_customer_level($merchant_id, $customer_info['id'], $merchant_username);
		
		
		$rewards = $this->get_reward_info($level, $merchant_id);
		
		
		
		
		//get points
		$points_earned = $this->get_points_earned_from_reward($rewards, $customer_info, $amount);
		


		//update customers table points_earned and total_amount accumulated not by merchant
		$update_customer = array(
			'points_earned' => $customer_info['points_earned'] + $points_earned, // update total points earned
			'total_amount' =>  0, 
		);
		
		$datetime = date('Y-m-d H:i:s');
        $update_customer['update_date'] = $datetime;
		$update_customer['update_by'] = $merchant_username;
		
		$customer_id  = $customer_info['id'];
		
		
		$this->db->begin_transaction();
        if (!$this->db->update('customers', $update_customer, 'id=' . $customer_id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating transaction Customer '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            return false;
        } else {

            $this->save_to_action_logs('Updating customers table transaction ok '. $customer_info['last_name']." ".$customer_info['first_name']  ,'CUSTOMER',$datetime,$merchant_username,'');
        	
			//add customer_transactions 
			$insert_data = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $customer_id,
				'level' => $level,
				'amount' => $amount,
				'points_earned' => $points_earned,
				'description' => $description,
				'create_by' => $merchant_username,
				'type' => 'ADD POINTS',
 			);
		
			if (!$this->db->insert('customer_transactions', $insert_data)) {
	            $this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating transaction Customer '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
            	return false;
	        }
			
			//update if exist customer_points_merchant total_amount and points_earned and amount_earned accumulated per merchant
			if(!$this->update_customer_points($merchant_id, $customer_id, $merchant_username, 0, $points_earned))
			{
				$this->db->rollback_transaction();
				$datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error updating transaction customer_points_merchant '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return false;
			}
			
			if(!$this->update_customer_accumulated_points($merchant_id, $customer_id, $merchant_username, $amount, $points_earned))
			{
				$this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
	            $this->save_to_action_logs('Error updating transaction customer_accu_points_merchant '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return false;
			}
			

			//add merchant_customers - add merchant_id on the customer 
			if(!$this->add_customer_to_merchant($merchant_id, $customer_id, $merchant_username))
			{
				$this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
				$this->save_to_action_logs('Error creating transaction merchant_customers '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
				return false;
			}
			
			//check if customer is for promotion to new level rewards
			//promote customer to new level if goal_points is on the range of next level
			//
			$c_points = $this->get_customer_existing_points($merchant_id, $customer_id);
			
			
			
			
			$next_level = $this->get_next_reward_level_info($merchant_id, $level);
		
			if(isset($next_level['goal_point']))
			{

				$goal_points = $next_level['goal_point'];
				$goal_points = 0;
				//if($c_points['points_earned'] >= $goal_points)
				
				
				if($c_points['points_earned'] >= $goal_points)
				{
					//deactivate customer_reward_level_merchant
					$next_level_no = $next_level['level'];
					$update_data = array(
						'status' =>'D',
						'update_date' => $datetime,
						'update_by' => 'AUTO',
					);
					
					$cmd = "SELECT id FROM customer_reward_level_merchant WHERE merchant_id={$merchant_id} AND customer_id = {$customer_id} AND status='A' AND level={$level}";
					$id = $this->db->fetchRow($cmd);
					if(isset($id['id']))
					{
						$id = $id['id'];
					}
					
					if (!$this->db->update('customer_reward_level_merchant', $update_data, 'id=' . $id)) {
			
			            $this->db->rollback_transaction();
			            $datetime = date('Y-m-d H:i:s');
			            $this->save_to_action_logs('Error upgrading reward to deactivate '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
			            return false;
			        }
					
					
					$insert_data = array(
						'merchant_id' => $merchant_id,
						'customer_id' => $customer_id,
						'level' => $next_level_no,
						'create_by' => 'AUTO',
					);
					
					//insert customer_reward_level_merchant
					if (!$this->db->insert('customer_reward_level_merchant', $insert_data)) {
			            $this->db->rollback_transaction();
			            $datetime = date('Y-m-d H:i:s');
						$this->save_to_action_logs('Error upgrading rewards '. $customer_info['last_name']." ".$customer_info['first_name']  ,'ERROR',$datetime,$merchant_username,'');
		            	return false;
			        }
				
				}
			}

			
			$this->db->commit_transaction();
			return true;
        }
		//$this->db->commit_transaction();
	}

	private function get_points_earned_from_reward($rewards, $customer_info, $amount)
	{
		$current_date = date('Y-m-d');
		$current_date = new DateTime($current_date);
		
		$points_earned = 0;
	
		// if($rewards['is_expire_on_day'])
		// {
			// if($rewards['is_expire_on_create_date'])
			// {
			   // $dtCreateDate = new DateTime($customer_info['create_date']);
			   // $dDiff = $current_date->diff($dtCreateDate);
			   // $day_diff = $dDiff->days; 
// 			 	
				// $no_of_days = $rewards['number_of_days'];
// 				
// 				
				// if($no_of_days >= $day_diff)
				// {
					// $amount_requirement = $rewards['amount'];
					// $point_conversion = $rewards['amount_point_conversion'];
					// $points_earned = ($amount/$amount_requirement) * $point_conversion;
				// }
			// }else{
				// //compare date on start_date_on_day with the no of days define 
			    // $dtCreateDate = new DateTime($customer_info['start_date_on_day']);
			    // $dDiff = $current_date->diff($dtCreateDate);
			    // $day_diff = $dDiff->days; 
// 			 	
				// $no_of_days = $rewards['number_of_days'];
// 				
				// if($no_of_days >= $day_diff)
				// {
					// $amount_requirement = $rewards['amount'];
					// $point_conversion = $rewards['amount_point_conversion'];
					// $points_earned = ($amount/$amount_requirement) * $point_conversion;
				// }
			// }
// 			
		// }else{ //base on start_date and end_date
// 			
			// $startDate = new DateTime($rewards['start_date']);
			$endDate = new DateTime($rewards['end_date']);
// 			
			// if($current_date >= $startDate && $current_date<= $endDate)
			// {
				if($current_date<= $endDate)
				 {
					$amount_requirement = $rewards['amount'];
					$point_conversion = $rewards['amount_point_conversion'];
					$points_earned = ($amount/$amount_requirement) * $point_conversion;
				 }
			// }
		// }
		return $points_earned;
	}



	private function update_customer_points($merchant_id, $customer_id, $merchant_username , $amount, $points_earned, $type ="add")
	{
		$datetime = date('Y-m-d H:i:s');
		$c_points = $this->get_customer_existing_points($merchant_id, $customer_id);
		
		if(isset($c_points['id']))
		{
			//update data on tables
			if($type=="add")
			{
				$update_data = array(
					'total_amount' => $c_points['total_amount'] + $amount,
					'amount_earned' => $c_points['amount_earned'] + $amount,
					'points_earned' => $c_points['points_earned'] + $points_earned,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);
			}else{
				$update_data = array(
					'total_amount' => $c_points['total_amount'] - $amount,
					'amount_earned' => $c_points['amount_earned'] - $amount,
					'points_earned' => $c_points['points_earned'] - $points_earned,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);
				
			}
			
			if(!$this->db->update('customer_points_merchant', $update_data, 'id=' . $c_points['id'])) {
	            return false;
	        }
		}else{
			//insert record
			$insert_data = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $customer_id,
				'total_amount' => $amount,
				'points_earned' => $points_earned,
				
			);
			if (!$this->db->insert('customer_points_merchant', $insert_data)) {
	            return false;
        	}
			
		}
		return true;
	}

	private function update_customer_accumulated_points($merchant_id, $customer_id, $merchant_username , $amount, $points_earned, $type ="add")
	{
		//update if exist customer_accu_points_merchant table update points_earned and total_amount per merchant since the beginning
		$datetime = date('Y-m-d H:i:s');
		$ac_points = $this->get_customer_existing_accumulated_points($merchant_id, $customer_id);
		if(isset($ac_points['id']))
		{
			//update data on tables
			if($type=="add")
			{
				$update_data = array(
					'total_amount' => $ac_points['total_amount'] + $amount,
					'points_earned' => $ac_points['points_earned'] + $points_earned,
					'points_earned_current' => $ac_points['points_earned_current'] + $points_earned,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);
			}else{
				$update_data = array(
					'total_amount' => $ac_points['total_amount'] - $amount,
					'points_earned' => $ac_points['points_earned'] - $points_earned,
					'points_earned_current' => $ac_points['points_earned_current'] - $points_earned,
					'update_by' => $merchant_username,
					'update_date' => $datetime,
				);				
			}
			
			if (!$this->db->update('customer_accu_points_merchant', $update_data, 'id=' . $ac_points['id'])) {
				return false;
	        }
		}else{
			//insert record
			$insert_data = array(
				'merchant_id' => $merchant_id,
				'customer_id' => $customer_id,
				'total_amount' => $amount,
				'points_earned' => $points_earned,
				'points_earned_current' => $points_earned,
				
			);
			if (!$this->db->insert('customer_accu_points_merchant', $insert_data)) {
	            return false;
        	}
		}
		return true;
	}

	private function add_customer_to_merchant($merchant_id, $customer_id, $merchant_username)
	{
		$m_customer = $this->check_if_customer_transaction_on_merchant($merchant_id, $customer_id);
		if(!isset($m_customer['id']))
		{
			$insert_data = array(
 				'merchant_id' => $merchant_id,
 				'customer_id' => $customer_id,
 				'create_by' => $merchant_username
			);				
			if (!$this->db->insert('merchant_customers', $insert_data)) {
	            return false;
        	}else
        	{
        		return true;
        	}
		}
		return true;
	}
	private function check_if_customer_transaction_on_merchant($merchant_id, $customer_id)
	{
		$cmd =" SELECT id FROM merchant_customers WHERE customer_id={$customer_id} AND merchant_id={$merchant_id} ";
		return $this->db->fetchRow($cmd);
	}
	
	
	private function  get_username_by_reference($id, $user_type)
	{
		$cmd = "SELECT username FROM users WHERE reference_id={$id}";
		switch ($user_type) {
			case 'merchant':
				$cmd .=" AND is_merchant=1";
				break;
			case 'iso':
				$cmd .=" AND is_iso=1";
				break;
			default:
				$cmd=" AND is_customer=1";
				break;
		}		
		
		return $this->db->fetchRow($cmd);
	}
	
	
	
	public function get_customer_info_by_id($value)
	{
		$cmd ="SELECT * FROM customers WHERE id =" . $value;
		return $this->db->fetchRow($cmd);
	}
	
	//value can be email or mobile
	public function get_customer_info($value)
	{
		$cmd ="SELECT * FROM customers WHERE email =" . $this->db->db_escape($value) ." OR mobile=" . $this->db->db_escape($value) ;
		return $this->db->fetchRow($cmd);
	}
	
	//business_type_id, customer_id, page_number, limit, value
	public function api_get_all_merchants($params)
    {
    	if($params['customer_id'] != "")
    	{
    		$cmd = "SELECT m.*, b.name as business_type, cp.points_earned, cp.amount_earned FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
    		LEFT JOIN customer_points_merchant cp ON m.id=cp.merchant_id AND cp.customer_id='{$params['customer_id']}' 
	        WHERE m.status='A' ";
    	
    	}else{
	        $cmd = "SELECT m.*, b.name as business_type FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
	        WHERE m.status='A' ";
			
        }
		if($params['business_type_id'] > 0)
		{
			$cmd.=" AND b.id= {$params['business_type_id']}";
		}
		
		if($params['value'] !="")
		{
			$cmd.=" AND (m.merchant_name LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd.=" OR m.zip LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd.=" )";
		}
		
		
		$cmd .=" ORDER BY m.merchant_name ASC LIMIT {$params['page_number']},{$params['limit']}";
		
		
        $result = $this->db->fetchAll($cmd);
		

		
		if($params['customer_id'] != "")
    	{
    		$cmd_count = "SELECT count(m.id) row_count FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
    		LEFT JOIN customer_points_merchant cp ON m.id=cp.merchant_id AND cp.customer_id='{$params['customer_id']}' 
	        WHERE m.status='A' ";
    	
    	}else{
	        $cmd_count = "SELECT count(m.id) row_count FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
	        WHERE m.status='A' ";
			
        }
		if($params['business_type_id'] > 0)
		{
			$cmd_count.=" AND b.id= {$params['business_type_id']}";
		}
		
		if($params['value'] !="")
		{
			$cmd_count.=" AND (m.merchant_name LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd_count.=" OR m.zip LIKE ".$this->db->db_escape('%'.$params['value'].'%');
			$cmd_count.=" )";
		}
		
		
		$count = $this->db->fetchRow($cmd_count);
		
		$result['record_count'] = $count['row_count'];
		
		
        return $this->success($result);
    }
	
	
	private function get_profile($table_name, $id)
	{
		$cmd = "SELECT * FROM {$table_name} WHERE id={$id}";
		return $this->db->fetchRow($cmd);
	}
	

	
	
	private function get_current_level($merchant_id)
	{
		$cmd ="SELECT level FROM merchant_rewards WHERE merchant_id={$merchant_id} AND status='A' ORDER BY level DESC LIMIT 1 ";
		return $this->db->fetchRow($cmd);
	}
	
	//merchant_id, reward_code, reward_name, start_date,
	//end_date, is_expire_on_day, number_of_days, is_expire_on_create_date, start_date_on_day
	//point, point_amount_conversion, amount, amount_point_conversion, goal_point, automatically_claim_reward, day_of_month
	public function create_rewards($params)
	{
		
		$params = array_map('trim', $params);
        if($this->check_if_profile_exist("merchant_rewards", "reward_code", $params['reward_code'], "-1"))
        {
	        return $this->failed('Reward Code has already been used.');
	        die();
        }
		$insert_data = array_map('trim', $params);
		
		$insert_data =$params;
		//to do check level of rewards.
		//if level of rewards is more than 1, goal should be greater than zero
		
		$level = 0;
		$reward_level = $this->get_current_level($insert_data['merchant_id']);
		if(isset($reward_level['level']))
		{
			$level = $reward_level['level'] + 1;
		}else{
			$level = 1;
		}		
		

		
		// if($insert_data['is_expire_on_day'])
		// {
			// if(!is_numeric($insert_data['number_of_days']))
			// {
				// return $this->failed("Number of days should be numeric.");
			// }
			// if($insert_data['is_expire_on_create_date'])
			// {
				// if(is_null($insert_data['start_date_on_day']))
				// {
					// return $this->failed("Please enter the date when the reward will take effect.");
				// }
// 				
			// }else{
				// $insert_data['start_date_on_day'] = null;
			// }
// 			
			// $insert_data['start_date'] = null;
			// $insert_data['end_date'] = null;
		// }else{
			// if($insert_data['start_date'] == "" && $insert_data['end_date'] == "")
			// {
				// return $this->failed("Please enter a valid. Validity dates.");
			// }
// 			
			// $start_date = strtotime($insert_data['start_date']);
			// $end_date = strtotime($insert_data['end_date']);
// 			
			// if($start_date >= $end_date)
			// {
				// return $this->failed("Unable to save rewards. End Date should always be greater than start date.");
			// }
		// }
		
		
		if($level > 1)
		{
			if($insert_data['goal_point']<=0)
			{
				return $this->failed("Please enter a goal points.");
			}
		}
		
	
		$insert_data['level'] = $level;
		$insert_data['create_by'] = $_SESSION['username'];
		$insert_data['status'] ="A";
		
		$this->db->begin_transaction();
        if (!$this->db->insert('merchant_rewards', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new rewards '. $params['reward_code']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new customer.");
        } else {
        	$reference_id = $this->db->lastInsertId();			
			$datetime = date('Y-m-d H:i:s');
			   
			$this->save_to_action_logs('Registered new rewards '. $params['reward_code']  ,'REWARDS',$datetime,$_SESSION['username'],'');
			
			$this->db->commit_transaction();
			return $this->success("New Rewards has been registered.");
		}
		
	}
	
	
	//id,merchant_id, reward_code, reward_name, start_date,
	//end_date, is_expire_on_day, number_of_days, is_expire_on_create_date, start_date_on_day
	//point, point_amount_conversion, amount, amount_point_conversion, goal_point, automatically_claim_reward, day_of_month 
	public function update_rewards($params)
	{
		$update_data = array_map('trim', $params);
		$params = array_map('trim', $params);
        if($this->check_if_profile_exist("merchant_rewards", "reward_code", $params['reward_code'], $update_data['id']))
        {
	        return $this->failed('Reward Code has already been used.');
	        die();
        }
		
		unset($update_data['id']);

		$datetime = date('Y-m-d H:i:s');
        $update_data['update_date'] = $datetime;
		$update_data['update_by'] = $_SESSION['username'];
		
		
		// if($update_data['start_date_on_day'] =="")
		// {
			// $update_data['start_date_on_day'] = null;
		// }
		// if($update_data['start_date'] =="")
		// {
			// $update_data['start_date'] = null;
		// }
// 
		// if($update_data['end_date'] =="")
		// {
			// $update_data['end_date'] = null;
		// }
		
		$this->db->begin_transaction();
        if (!$this->db->update('merchant_rewards', $update_data, 'id=' . $params['id'])) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating Reward '. $params['reward_name']." ".$params['reward_code']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to update rewards.".serialize($update_data));
        } else {

            $this->save_to_action_logs('Reward has been updated '. $params['reward_name']." ".$params['reward_code']  ,'REWARDS',$datetime,$_SESSION['username'],'');
            
            $this->db->commit_transaction();
            return $this->success("Reward has been updated.");
        }
	}
	
	
	public function delete_rewards($id)
	{
		
		$cmd ="SELECT level, merchant_id FROM merchant_rewards WHERE id={$id} AND status='A'";
		$rewards =$this->db->fetchRow($cmd);
		
		$merchant_id = $rewards['merchant_id'];
		
		$level = $rewards['level'];
		
		$cmd = "SELECT id FROM customer_transactions WHERE merchant_id={$merchant_id} AND level={$level}";
		$trans = $this->db->fetchRow($cmd);
		if(isset($trans['id']))
		{
			return $this->failed("You cannot delete this rewards. Transaction has already been made.");
		}
		
		$delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('merchant_rewards', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete rewards '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete rewards");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete Rewards with reward id'. $id  ,'REWARDS',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted Rewards");
        }
	}
	
	public function get_rewards($merchant_id)
	{
		$query ="SELECT * FROM merchant_rewards WHERE merchant_id={$merchant_id} AND status='A'";
		$data = $this->db->fetchAll($query);
		
		return $this->success($data);
	}
	
	
	protected function generate_customer_code() {
        do {
        	//$customer_code = uniqid(time(), true);
            $customer_code = uniqid(time(), TRUE);
            $check = $this->db->fetchRow("select id from customers where customer_code='{$customer_code}'");
        } while (isset($check['id']));
        return $customer_code;
    }
	
	
	public function delete_customer($id)
    {
        $delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('customers', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete customer '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete customer");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete Customer with Customer id'. $id  ,'AGENT',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted Customer");
        }
    }
	

	
	public function get_customers($merchant_id)
	{
		//$query ="SELECT c.*,m.merchant_code, m.merchant_name FROM customers c LEFT JOIN merchants m on c.merchant_id=m.id";
		$query ="SELECT c.*,m.merchant_code, m.merchant_name FROM customers c 
		LEFT JOIN merchant_customers mc on c.id = mc.customer_id AND mc.merchant_id={$merchant_id}
		LEFT JOIN merchants m on m.id=mc.merchant_id";
		//$query.=" WHERE c.merchant_id={$merchant_id} AND c.status='A'";
		$query.=" WHERE mc.merchant_id={$merchant_id} AND c.status='A'";

		
		$data = $this->db->fetchAll($query);
		
		return $this->success($data);
	}
	
	//customer information update and registration
	//merchant_reward_id ? how are we going to register a new customer
	//id //for customer_id
	//merchant_id, last_name, first_name, middle_name, mobile, telephone,
	//state, city, zip, country, email, address, udf1, udf2, udf3, date_of_birth  
	public function update_customer($params)
	{
		$params = array_map('trim', $params);
        if($this->check_if_profile_exist("customers", "mobile", $params['mobile'], $params['id']))
        {
	        return $this->failed('Mobile number has already been used.');
	        die();
        }
		
		if($this->check_if_profile_exist("customers", "email", $params['email'], $params['id']))
        {
	        return $this->failed('Email has already been used.');
	        die();
        }

        $datetime = date('Y-m-d H:i:s');
        $params['update_date'] = $datetime;
        if(!isset($params['from_api']))
		{
			$params['update_by'] = $_SESSION['username'];
		}else{
			$params['update_by'] = 'API';
		}	
		
		$id = $params['id']; 
		//fields no need for updating
		unset($params['id']);
		unset($params['from_api']);
		
		$params['date_of_birth'] = isset($params['date_of_birth']) ? $params['date_of_birth'] : '';
	
		$this->db->begin_transaction();
        if (!$this->db->update('customers', $params, 'id=' . $id)) {

            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating Customer '. $params['last_name']." ".$params['first_name']  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update Customer.");
        } else {

			$cmd ="UPDATE users SET update_date = ".$this->db->db_escape(date('Y-m-d H:i:s'));
			$cmd .= ", email_address=".$this->db->db_escape($params['email']);
			$cmd .=" WHERE reference_id=".$id;
			$cmd .=" AND user_type_id=3"; //user_type for customer
			
			$this->db->query($cmd);

            $this->save_to_action_logs('Customer has been updated '. $params['last_name']." ".$params['first_name']  ,'CUSTOMER',$datetime,$params['update_by'],'');
            
            $this->db->commit_transaction();
            return $this->success("Customer has been updated.");
        }
	}
	
	//customer information update and registration
	//merchant_reward_id ? how are we going to register a new customer
	//merchant_id, last_name, first_name, middle_name, mobile, total_amount, telephone,
	//state, city, zip, country, email, address, udf1, udf2, udf3, 
	
	//from_api
	
	public function register_customer($params)
	{
		$params = array_map('trim', $params);
		
		if(!is_numeric($params['merchant_id']))
		{
			return $this->failed("Invalid merchant id. Please use -1 as a default.");
		}
		
        if($this->check_if_profile_exist("customers", "mobile", $params['mobile']))
        {
	        return $this->failed('Mobile number has already been used.');
	        die();
        }
		
		if($this->check_if_profile_exist("customers", "email", $params['email']))
        {
	        return $this->failed('Email has already been used.');
	        die();
        }
		//to do validate if username already exist.
        $user = array(
            'username' => $params['email'],
            'user_id' => -1,
        );
        if($this->check_if_username_exist($user, 1))
        {
            return $this->failed("Email has already been used.");
            die();
        }
		
		//username should be the mobile no. of the customer
		//password should be default password that will be email or text to the customer
		$insert_data = array(
			'customer_code' => $this->generate_customer_code(),
			'merchant_id' => $params['merchant_id'],
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'middle_name' => $params['middle_name'],
			'mobile' => $params['mobile'],
			'telephone' => $params['telephone'],
			'state' => $params['state'],
			'city' => $params['city'],
			'zip' => $params['zip'],
			'country' => $params['country'],
			'email' => $params['email'],
			'address' => $params['address'],
			'udf1' => $params['udf1'],
			'udf2' => $params['udf2'],
			'udf3' => $params['udf3'],
			'date_of_birth' => isset($params['date_of_birth']) ? $params['date_of_birth'] : '',
			//'create_by' => $_SESSION['username'],
			'status' => "A"
		);
		
		if(!isset($params['from_api']))
		{
			$insert_data['create_by'] = $_SESSION['username'];
		}else{
			$insert_data['create_by'] = "API";
		}
			
		$activation_code = rand(1000, 999999);
		$activation_code = $this->hideit->encrypt(sha1(md5($this->salt)),$activation_code, sha1(md5($this->salt)));	
		$insert_data['activation_code'] = $activation_code;
		
		$mobile =$this->db->db_escape($params['mobile']);
		$delete_query ="DELETE FROM customers WHERE status='I'";
		$delete_query .=" AND mobile={$mobile}";
		
		//delete old registration that has not been activated
		$this->db->query($delete_query);
		$delete_query ="DELETE FROM users WHERE username={$mobile} AND status<>'A'";
		$this->db->query($delete_query);
		
		
		$this->db->begin_transaction();
        if (!$this->db->insert('customers', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new customer '. $params['mobile']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new customer.");
        } else {
        	$reference_id = $this->db->lastInsertId();
	        $user_insert = array(
	                'username' => $params['email'],
	                'password' => $params['password'], //get activation code as the default password
	                'last_name' => $params['last_name'],
	                'first_name' => $params['first_name'],
	                'email_address' => $params['email'],
	                'reference_id' => $reference_id, //newly created Id
	                'user_type_id' => 3, //usertype for customer
	                'status' => 'A', //inactive status until it has been activated
	                'create_by' => $insert_data['create_by']
	        );
			$this->create_user($user_insert);
			
			$datetime = date('Y-m-d H:i:s');
			
			if($params['merchant_id'] > 0)
			{
				$insert_merchant_customer = array(
					'customer_id' => $reference_id,
					'merchant_id' => $params['merchant_id'],
					'status' => 'A',
					'create_by' => $insert_data['create_by'],
				);
				
				if (!$this->db->insert('merchant_customers', $insert_merchant_customer)) {
		            $this->db->rollback_transaction();
		            $datetime = date('Y-m-d H:i:s');
		            $this->save_to_action_logs('Error creating new customer merchant_customers '. $params['mobile']  ,'ERROR',$datetime,$_SESSION['username'],'');
		            return $this->failed("Unable to create new customer.");
		        }
				
				$insert_data = array(
					'merchant_id' =>  $params['merchant_id'],
					'customer_id' => $reference_id,
					'level' => 1,
					'create_by' => $insert_data['create_by'],
				);
				
				if (!$this->db->insert('customer_reward_level_merchant', $insert_data)) {
		            $datetime = date('Y-m-d H:i:s');
		            $this->save_to_action_logs('Error creating new customer.'. $params['mobile']  ,'ERROR',$datetime,$_SESSION['username'],'');
		            return $this->failed("Unable to create new transaction.");
		        } 
				
			}
			   
			$this->save_to_action_logs('Registered new customer '. $params['email']  ,'CUSTOMER',$datetime,$insert_data['create_by'],'');
			
			$this->db->commit_transaction();
			return $this->success("New Customer has been registered.");
		}
	}
    
    
    //start new code for Go3 Rewards
    public function get_agents($parent_id =-1)
    {
        $query="SELECT a.*,u.username,u.password FROM agents a LEFT JOIN users u ON a.id=u.reference_id AND is_agent=1
WHERE a.status='A' AND a.iso_id={$parent_id}";
        $data = $this->db->fetchAll($query);
        return $this->success($data);
    }
    //iso_id,agent_code, agent_name, legal_name, dba_name, tax_id, phone_no, reseller_name, reseller_type, fax, dba_address1,
    //dba_address2, dba_city, dba_state, dba_zip, dba_country, mailing_address1, mailing_address2, mailing_city, mailing_state,
    //mailing_zip, mailing_country, account_no, routing_no, email, 
    public function register_agent($params)
    {
	    //to do validate if username already exist.
        $user = array(
            'username' => $params['username'],
            'user_id' => -1,
        );
        if($this->check_if_username_exist($user))
        {
            return $this->failed("Username has already been used.");
            die();
        }
        $insert_data = array_map('trim', $params);
        $insert_data = array(
            'agent_code' => $params['agent_code'],
            'agent_name' => $params['agent_name'],
            'legal_name' => $params['legal_name'],
            'dba_name' => $params['dba_name'],
            'tax_id' => $params['tax_id'],
            'phone_no' => $params['phone_no'],
            'reseller_name' => $params['reseller_name'],
            'reseller_type' => $params['reseller_type'],
            'fax' => $params['fax'],
            'dba_address1' => $params['dba_address1'],
            'dba_address2' => $params['dba_address2'],
            'dba_city' => $params['dba_city'],
            'dba_state' => $params['dba_state'],
            'dba_zip' => $params['dba_zip'],
            'dba_country' => $params['dba_country'],
            'mailing_address1' => $params['mailing_address1'],
            'mailing_address2' => $params['mailing_address2'],
            'mailing_city' => $params['mailing_city'],
            'mailing_state' => $params['mailing_state'],
            'mailing_zip' => $params['mailing_zip'],
            'mailing_country' => $params['mailing_country'],
            'account_no' => $params['account_no'],
            'routing_no' => $params['routing_no'],
            'email' => $params['email'],
            'iso_id' => $params['iso_id'],
          );
        
        $insert_data['status'] ="A";
        $insert_data['create_by'] = $_SESSION['username'];
        
        if($this->check_if_profile_exist("agents", "agent_code", $params['agent_code']))
        {
            return $this->failed('Agent Code has already been used.');
            die();
        }

        if($this->check_if_profile_exist("agents", "agent_name", $params['agent_name']))
        {
            return $this->failed('Agent name has already been registered.');
            die();
        }
        
        $this->db->begin_transaction();
        if (!$this->db->insert('agents', $insert_data)) {
            
        die();
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new Agent '. $params['agent_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new Agent.");
        } else {
            //create an Agent user
            $reference_id = $this->db->lastInsertId();
            $user_insert = array(
                'username' => $params['username'],
                'password' => $params['password'], //encryption happens on creation of user method
                'last_name' => $params['agent_name'],
                'first_name' => $params['agent_code'],
                'email_address' => $params['email'],
                'reference_id' => $reference_id, //newly created Id
                'user_type_id' => 6, //user
            );
            
            $this->create_user($user_insert);    // this function will proceed to rollback in case of error.
            
            $datetime = date('Y-m-d H:i:s');
           
            $this->save_to_action_logs('Registered new agent '. $params['agent_name']  ,'AGENT',$datetime,$_SESSION['username'],'');
            
            $this->db->commit_transaction();
            return $this->success("New Agent has been registered.");
        }
    }

    //id,iso_id,agent_code, agent_name, legal_name, dba_name, tax_id, phone_no, reseller_name, reseller_type, fax, dba_address1,
    //dba_address2, dba_city, dba_state, dba_zip, dba_country, mailing_address1, mailing_address2, mailing_city, mailing_state,
    //mailing_zip, mailing_country, account_no, routing_no, email,  
    public function update_agent($params)
    {
        $insert_data = array_map('trim', $params);
        
        $insert_data['update_by'] = $_SESSION['username'];
		$datetime = date('Y-m-d H:i:s');
		$insert_data['update_date'] = $datetime;
        
        if($this->check_if_profile_exist("agents", "agent_code", $params['agent_code'], $insert_data['id']))
        {
            return $this->failed('Agent Code has already been used.'.$params['id']);
            die();
        }

        if($this->check_if_profile_exist("agents", "agent_name", $params['agent_name'], $insert_data['id']))
        {
            return $this->failed('Agent name has been used.');
            die();
        }
        $this->db->begin_transaction();
        if (!$this->db->update('agents', $insert_data, 'id=' . $params['id'])) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating Agent '. $params['agent_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new Agent.");
        } else {

            $this->save_to_action_logs('Agent has been updated '. $params['agent_name']  ,'AGENT',$datetime,$_SESSION['username'],'');
            
            $this->db->commit_transaction();
            return $this->success("New Agent has been registered.");
        }
    }
    public function delete_agent($id)
    {
        $delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('agents', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete agent '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete agent");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete Agent with Agent id'. $id  ,'AGENT',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted Agent");
        }
    }
    
    public function delete_iso($id)
    {
        $delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('iso', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete iso '. $id ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete iso");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete ISO with ISO id'. $id  ,'ISO',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted ISO");
        }
    }
    
    //additional parameter which is id 
    public function update_iso($params)
    {
            $params = array_map('trim', $params);
            if($this->check_if_profile_exist("iso", "iso_code", $params['iso_code'], $params['id']))
            {
                return $this->failed('ISO Code has already been used.');
                die();
            }

            if($this->check_if_profile_exist("iso", "company_name", $params['company_name'], $params['id']))
            {
                return $this->failed('Company name has already been registered.');
                die();
            }
            
            $update_data = array(                
                'iso_code' => $params['iso_code'],
                'company_name' => $params['company_name'],
                'dba_name' =>  $params['dba_name'],
                'address1' => $params['address1'],
                'address2' => $params['address2'],
                'city' => $params['city'],
                'state' => $params['state'],
                'zip' => $params['zip'],
                'country' => $params['country'],
                'contact_number_1' => $params['contact_number_1'],
                'contact_number_2' => $params['contact_number_2'],
                'internal_id' => $params['internal_id'], 
                'account_number' => $params['account_number'], 
                'routing_number' => $params['routing_number'], 
                'udf1' => $params['udf1'], 
                'udf2' => $params['udf2'], 
                'udf3' => $params['udf3'], 
                'status' => 'A', 
                'update_by' => $_SESSION['username'], 
                'update_date' => date('Y-m-d H:i:s'),
                'parent_iso_id' => $params['parent_iso_id']
            );

            
            if (!($this->db->update('iso', $update_data, 'id=' . $params['id']))) {
                $datetime = date('Y-m-d H:i:s');
                $this->save_to_action_logs('Error unable to update iso first update '. $params['id']  ,'ERROR',$datetime,$_SESSION['username'],'');
                return $this->failed("Unable to update ISO.");    
            } else {
                
                $update_data_admin = array(
                    'first_name' => $params['first_name'],
                    'last_name' => $params['last_name'],
                    'email' => $params['email'],
                    'contact_number_1' => $params['contact_number_1_admin'],
                    'contact_number_2' => $params['contact_number_2_admin'],
                    'udf1' => $params['udf1_admin'],
                    'udf2' => $params['udf2_admin'],
                    'udf3' => $params['udf3_admin'],
                    'update_date' => date('Y-m-d H:i:s'),
                );
                if (!($this->db->update('iso_admin', $update_data_admin, 'iso_id=' . $params['id']))) {
                    $datetime = date('Y-m-d H:i:s');          
                    $this->save_to_action_logs('Error unable to update iso 2nd update '. $params['id']  ,'ERROR',$datetime,$_SESSION['username'],'');
                    return $this->failed("Unable to update ISO.");    
                }
                
                $datetime = date('Y-m-d H:i:s');
                $this->save_to_action_logs('Update ISO with ISO id'. $params['id']  ,'ISO',$datetime,$_SESSION['username'],'');
                return $this->success("Successfully updated ISO.");
            }
    }
                        
    public function register_iso($params)
    {
            $params = array_map('trim', $params);
            if($this->check_if_profile_exist("iso", "iso_code", $params['iso_code']))
            {
                return $this->failed('ISO Code has already been used.');
                die();
            }

            if($this->check_if_profile_exist("iso", "company_name", $params['company_name']))
            {
                return $this->failed('Company name has already been registered.');
                die();
            }
            
            //to do validate if username already exist.
            $user = array(
                'username' => $params['username'],
                'user_id' => -1,
            );
            if($this->check_if_username_exist($user))
            {
                return $this->failed("Username has already been used.");
                die();
            }
            
            $insert_data = array(
                'iso_code' => $params['iso_code'],
                'company_name' => $params['company_name'],
                'dba_name' =>  $params['dba_name'],
                'address1' => $params['address1'],
                'address2' => $params['address2'],
                'city' => $params['city'],
                'state' => $params['state'],
                'zip' => $params['zip'],
                'country' => $params['country'],
                'contact_number_1' => $params['contact_number_1'],
                'contact_number_2' => $params['contact_number_2'],
                'internal_id' => $params['internal_id'], 
                'account_number' => $params['account_number'], 
                'routing_number' => $params['routing_number'], 
                'udf1' => $params['udf1'], 
                'udf2' => $params['udf2'], 
                'udf3' => $params['udf3'], 
                'status' => 'A', 
                'create_by' => $_SESSION['username'], 
                'parent_iso_id' => $params['parent_iso_id']
            );
            
            $this->db->begin_transaction();
            if (!$this->db->insert('iso', $insert_data)) {
                $this->db->rollback_transaction();
                $datetime = date('Y-m-d H:i:s');
                $this->save_to_action_logs('Error creating new iso '. $params['company_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
                return $this->failed("Unable to create new iso.");
            } else {
                $iso_id = $this->db->lastInsertId();
                //register iso_admin
                $insert_data = array(
                    'iso_id' => $iso_id,
                    'first_name' => $params['first_name'],
                    'last_name' => $params['last_name'],
                    'email' => $params['email'],
                    'contact_number_1' => $params['contact_number_1_admin'],
                    'contact_number_2' => $params['contact_number_2_admin'],
                    'udf1' => $params['udf1_admin'],
                    'udf2' => $params['udf2_admin'],
                    'udf3' => $params['udf3_admin'],
                );
                if (!$this->db->insert('iso_admin', $insert_data)) {
                    $this->db->rollback_transaction();
                    $datetime = date('Y-m-d H:i:s');
                    $this->save_to_action_logs('Error creating new iso. ISO Admin '. $params['company_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
                    return $this->failed("Unable to create new iso. ISO Admin");
                }
                
                //create an ISO user
                $user_insert = array(
                    'username' => $params['username'],
                    'password' => $params['password'], //encryption happens on creation of user method
                    'last_name' => $params['last_name'],
                    'first_name' => $params['first_name'],
                    'email_address' => $params['email'],
                    'reference_id' => $iso_id, //newly created ISO Id
                );
                
                if($params['parent_iso_id'] == -1)
                {
                    $user_insert['user_type_id'] = 4; //For ISO
                }else
                {
                    $user_insert['user_type_id'] = 5; //for SUB ISO
                }
                
                
                $this->create_user($user_insert);    // this function will proceed to rollback in case of error.
                
                $datetime = date('Y-m-d H:i:s');
               
                $this->save_to_action_logs('Registered new iso '. $params['company_name']  ,'ISO',$datetime,$_SESSION['username'],'');
                
                $this->db->commit_transaction();
                return $this->success("New ISO has been registered.");
            }
            
            
    }
    
    public function get_iso($parent_iso_id=-1)
    {
        $query ="SELECT i.*, ia.id as admin_id, ia.first_name, ia.last_name, ia.email, 
            ia.contact_number_1 as admin_contact_number1, ia.contact_number_2 as admin_contact_number2,ia.udf1 as admin_udf1,
            ia.udf2 as admin_udf2, ia.udf3 as admin_udf3,
            u.username, u.password
            FROM iso i LEFT JOIN iso_admin ia on i.id=ia.iso_id 
            LEFT JOIN users u ON u.reference_id=i.id AND is_iso =1
        WHERE i.status='A' AND i.parent_iso_id={$parent_iso_id}";
        $isos = $this->db->fetchAll($query);
        return $this->success($isos);
    }
    
    //username , password, last_name, first_name, email_address, user_type_id, reference_id
    public function create_user($insert_data)
    {
        $insert_data = array_map('trim', $insert_data);
        //customer 3
        if(in_array($insert_data['user_type_id'], unserialize(CUSTOMER_USER_TYPE_ID)))
        {
            $insert_data['is_customer'] =1;
        }
        //agent 6
        if(in_array($insert_data['user_type_id'], unserialize(AGENT_USER_TYPE_ID)))
        {
            $insert_data['is_agent'] =1;
        }
        //merchant admin 2, 8 merchant, 7 branch merchant admin
        if(in_array($insert_data['user_type_id'], unserialize(MERCHANT_USER_TYPE_ID)))
        {
            $insert_data['is_merchant'] =1;
        }
        //iso_user_type 4, 5
        if(in_array($insert_data['user_type_id'], unserialize(ISO_USER_TYPE_ID)))
        {
            $insert_data['is_iso'] = 1;
        }   
        //super admin 1, 9 admin
        if(in_array($insert_data['user_type_id'], unserialize(ADMIN_USER_TYPE_ID)))
        {
            $insert_data['is_admin'] = 1;
        }
        
        $insert_data['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$insert_data['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        
        if (!$this->db->insert('users', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
            die();
        }                                                                        
    }
    
    public function register_branch($params)
    {
        $params = array_map('trim', $params);
            if($this->check_if_profile_exist("merchant_branches", "branch_code", $params['branch_code']))
            {
                return $this->failed('Branch code has already been used.');
                die();
            }

            if($this->check_if_profile_exist("merchant_branches", "branch_name", $params['branch_name']))
            {
                return $this->failed('Branch name has already been used.');
                die();
            }

            //
            $this->db->begin_transaction();
            $insert_data = array(
                'branch_code' => $params['branch_code'],
                'branch_name' => $params['branch_name'],
                'contact_person' => $params['contact_person'],
                'business_address' => $params['business_address'],
                'email' => $params['email'],
                'telephone' => $params['telephone'],
                'mobile' => isset($params['mobile']) ? $params['mobile'] : "",
                'fax' => isset($params['fax']) ? $params['fax'] : "",
                'status' => 'A', 
                'create_by' => $_SESSION['username'], 
                'merchant_id' => $params['merchant_id'],
            );                     
           
            if (!$this->db->insert('merchant_branches', $insert_data)) {
                $this->db->rollback_transaction();
                $datetime = date('Y-m-d H:i:s');
                $this->save_to_action_logs('Unable to create new branch'. $params['branch_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
                return $this->failed("Unable to create new branch.");
            } else {
                $merchant_id = $this->db->lastInsertId();
               
                $datetime = date('Y-m-d H:i:s');
               
                $this->save_to_action_logs('Registered new branch '. $params['branch_name']  ,'Branch',$datetime,$_SESSION['username'],'');
               
                $this->db->commit_transaction();
                return $this->success("New Branch has been added.");
            } 
    }
    
    public function delete_branch($id)
    {
        
        $delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('merchant_branches', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to delete branch '. $id   ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to delete branch");    
        } else {
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete branch with branch id'. $id  ,'Branch',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted branch");
        }
    }
    
    public function update_branch($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
            'branch_code' => $params['branch_code'],
            'branch_name' => $params['branch_name'],
            'contact_person' => $params['contact_person'],
            'business_address' => $params['business_address'],
            'email' => $params['email'],
            'telephone' => $params['telephone'],
            'mobile' => isset($params['mobile']) ? $params['mobile'] : "",
            'fax' => isset($params['fax']) ? $params['fax'] : "",
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        if (!($this->db->update('merchant_branches', $update_data, 'id=' . $params['id']))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to update branch'. $params['branch_name']  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to update branch.");    
        } else {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Update branch with branch id'. $params['id']  ,'Branch',$datetime,$_SESSION['username'],'');
            return $this->success("Successfully updated branch.");
        }
    }
    
    public function get_branch($merchant_id=-1)
    {
        $query="SELECT m.merchant_name, mb.* FROM merchant_branches mb
LEFT JOIN merchants m ON mb.merchant_id=m.id
WHERE mb.status='A' AND mb.merchant_id={$merchant_id}";
        
        
        
        $branches = $this->db->fetchAll($query);
        return $this->success($branches);
    }
    
    
    public function get_user_types() {
        $user_types = $this->db->fetchPairs("select id,description from user_types WHERE status='A'");
        return $user_types;
    }
    
    public function get_users() {
        $query = "select CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,first_name as fname,last_name as lname,username,u.email_address,user_type_id,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type_id where u.status='A'";
       
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_countries()
    {
        $cmd = "SELECT name as id, name FROM countries ";
        $result = $this->db->fetchPairs($cmd);
        return $result;           
    }
    
    public function get_all_merchants($parent_id, $is_agent)
    {
        $cmd = "SELECT m.*, b.name as business_type,u.username, u.password FROM merchants m LEFT JOIN business_types b ON m.business_type_id=b.id 
        LEFT JOIN users u on u.reference_id=m.id AND u.is_merchant=1  
        WHERE m.status='A'";
        
		if($is_agent)
		{
			$cmd.=" AND m.agent_id= {$parent_id}";
		}else{
			$cmd.=" AND m.iso_id= {$parent_id}";
		}
		        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    
    
    public function check_user_if_exist($params)
    {
        $params = array_map('trim', $params);
        $row = $this->db->fetchRow("select * from users where status = 'A' and username=" . $this->db->db_escape($params['username']) ." AND ".$params['id']);   
        if (!(isset($row['id']))) {
            return false;
        } else { 
            return true;
        }
    }
    
    private function check_if_profile_exist($table, $field, $value, $id = -1)
    {
        $value =strtoupper($value);
        $cmd ="SELECT id FROM {$table} WHERE ucase({$field})= {$this->db->db_escape($value)} AND id<>{$id} AND status='A'";   
        $row = $this->db->fetchRow($cmd);
        if (!(isset($row['id']))) {
            return false;
        } else { 
            return true;
        }
    }
    
    //function for deleting merchant
    public function delete_merchant($id)
    {
        $delete_data = array(
            'status' => 'D', 
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
        
        if (!($this->db->update('merchants', $delete_data, 'id=' . $id))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to update merchant'. $id  ,'ERROR',$datetime, $_SESSION['username'],'');
            return $this->failed("Unable to delete merchant");    
        } else {
//            $response = array(
//                'success' => true,
//                'message' => 'Successfully deleted merchant'
//            );
            $datetime = date('Y-m-d H:i:s');
               
            $this->save_to_action_logs('Delete merchant with merchant id ='. $id ,'Merchant',$datetime,$_SESSION['username'],'');
            
            return $this->success("Successfully deleted merchant");
        }
    }
    
    
    
    //function for updating merchant
    public function update_merchant($params)
    {
    	
		$params = array_map('trim', $params);
        if($this->check_if_profile_exist("merchants", "merchant_code", $params['merchant_code'], $params['id']))
        {
	        return $this->failed('Merchant code has already been used.');
	        die();
        }
		
		if($this->check_if_profile_exist("merchants", "merchant_name", $params['merchant_name'], $params['id']))
        {
	        return $this->failed('Merchant name has already been used.');
	        die();
        }

        $update_data = array(
            'merchant_code' => $params['merchant_code'],
            'merchant_name' => $params['merchant_name'],
            //'merchant_logo' => $params['merchant_logo'],
            //'contact_person' => $params['contact_person'],
           	'last_name' => $params['last_name'],
	        'first_name' => $params['first_name'],
            'business_type_id' => $params['business_type_id'],
            'address' => $params['address'],
            'city' => isset($params['city']) ? $params['city'] : "",
            'state' => isset($params['state']) ? $params['state'] : "",
            'zip' => isset($params['zip']) ? $params['zip'] : "",
            'country' => $params['country'],
            'email' => $params['email'],
            'telephone' => $params['telephone'],
            'mobile' => isset($params['mobile']) ? $params['mobile'] : "",
            'fax' => isset($params['fax']) ? $params['fax'] : "",
            'status' => 'A', 
            //'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        );
		
		if(!isset($params['from_api']))
		{
			$update_data['update_by'] = $_SESSION['username'];
		}else{
			$update_data['update_by'] = 'API';
		}	
		
        if (!($this->db->update('merchants', $update_data, 'id=' . $params['id']))) {
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error unable to update merchant'. $params['id']  ,'ERROR',$datetime, $update_data['update_by'],'');
            return $this->failed("Unable to update merchant.");    
        } else {
        	//$this->db->update('users', )
			$cmd ="UPDATE users SET update_date = ".$this->db->db_escape(date('Y-m-d H:i:s'));
			$cmd .= ", email_address=".$this->db->db_escape($params['email']);
			$cmd .=" WHERE reference_id=".$params['id'];
			$cmd .=" AND user_type_id=7"; //user_type for merchant
			
			$this->db->query($cmd);
			
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Update merchant with merchant id ='. $params['id'] ,'Merchant',$datetime,$update_data['update_by'],'');
            return $this->success("Successfully updated merchant.");
        }
    }
    
    
    //function for registering new merchant
    public function register_merchant($params)
    {

            if($this->check_if_profile_exist("merchants", "merchant_code", $params['merchant_code']))
            {
                return $this->failed('Merchant code has already been used.');
                die();
            }

            if($this->check_if_profile_exist("merchants", "merchant_name", $params['merchant_name']))
            {
                return $this->failed('Merchant name has already been used.');
                die();
            }

            //to do validate if username already exist.
            $user = array(
                'username' => $params['username'],
                'user_id' => -1,
            );
            if($this->check_if_username_exist($user))
            {
                return $this->failed("Username has already been used.");
                die();
            }

            //
            $this->db->begin_transaction();
            $insert_data = array(
                'merchant_code' => $params['merchant_code'],
                'merchant_name' => $params['merchant_name'],
                'merchant_logo' => '',
                //'contact_person' => $params['contact_person'],
                'last_name' => $params['last_name'],
	        	'first_name' => $params['first_name'],
                'business_type_id' => -1, //txtBusinessType
                'address' => '',
                
                'city' => isset($params['city']) ? $params['city'] : "",
                'state' => isset($params['state']) ? $params['state'] : "",
                'zip' => isset($params['zip']) ? $params['zip'] : "",
                'country' => $params['country'],
                'email' => $params['email'],
                'telephone' => $params['telephone'],
                'mobile' => isset($params['mobile']) ? $params['mobile'] : "",
                'fax' => isset($params['fax']) ? $params['fax'] : "",
                'status' => 'A', 
                'create_by' => $_SESSION['username'], 
            );                     
           
		   if(isset($params['agent_id']))
		   {
		   		$insert_data['agent_id'] = $params['agent_id'];
		   }
		   if(isset($params['iso_id']))
		   {
				$insert_data['iso_id'] = $params['iso_id'];	
		   }
		
		   
		   
            if (!$this->db->insert('merchants', $insert_data)) {
                $this->db->rollback_transaction();
                $datetime = date('Y-m-d H:i:s');
                $this->save_to_action_logs('Error unable to create merchant '. $params['merchant_name']  ,'ERROR',$datetime, $_SESSION['username'],'');
                
                return $this->failed("Unable to create new merchant.");
            } else {
                    
				$reference_id = $this->db->lastInsertId();
	            $user_insert = array(
	                'username' => $params['username'],
	                'password' => $params['password'], //encryption happens on creation of user method
	                'last_name' => $params['last_name'],
	                'first_name' => $params['first_name'],
	                'email_address' => $params['email'],
	                'reference_id' => $reference_id, //newly created Id
	                'user_type_id' => 7, //user
	            );
            
            	$this->create_user($user_insert);    // this function will proceed to rollback in case of error.

               $datetime = date('Y-m-d H:i:s');
               
               $this->save_to_action_logs('Registered new merchant '. $params['merchant_name']   ,'Merchant',$datetime,$_SESSION['username'],'');
               
               
               $this->db->commit_transaction();                                                          
                return $this->success("New Merchant has been added.");
            } 
//       }
    }
    
    
    
    
    public function check_if_username_exist($params, $is_customer = 0)
    {         
        $params = array_map('trim', $params);
		$query = "select * from users where status = 'A' and username=" . $this->db->db_escape($params['username'])." AND id<> ". $params['user_id'];
		$query .= " AND is_customer=".$is_customer;
		
        $row = $this->db->fetchRow($query);
       
        if (!(isset($row['id']))) {
            return false;
        } else {
              
            return true;
        }
    }
    

    
    
    //params: action_taken, action_module, action_date, action_by, remark
    public function save_to_action_logs($action_taken, $action_module, $action_date, $action_by, $remark) {
        if (!($action_taken != '' && $action_module != '' && $action_date != '' && $action_by != '')) {
            return $this->failed('Invalid parameters');
        } else {
            if($action_module =="ERROR")
            {
                $action_taken .= " ". $this->db->get_error_string();
            }
            
            $insert_data = array(
                'action_taken'  => $action_taken,
                'action_module'            => $action_module,
                'action_date'              => $action_date,
                'action_by'         => $action_by,
                'remark'      => $remark,            
            );
            if (!$this->db->insert('action_logs', $insert_data)) {
                return $this->failed('Unable to save action_logs');
            } else {
                return $this->success('Successfully saved new action_logs');

            }
        }
    }
    
    
    
    
    // ACL PROFILE
    public function delete_module($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array('deleted'=>1);
        $this->db->update('resources', $values, 'id='.$id);
        $response = array(
            'message' => "Successfully deleted module",
        );
        return $this->success($response);
    }   
    
    
    public function add_module($params)
    {
         $params = array_map('trim', $params);
         //print_r($params);
         //die();
         
         $insert_data = array(
              'resource' => $params['resource'],
              'description' => $params['description'],
              'create_date' => date('Y-m-d H:i:s'),
              
         );           
         
         
         if (!$this->db->insert('resources', $insert_data)) 
         {
            return $this->failed("Error creating module.");
         }else {
            $response = array(
                'message' => "Successfully created module.",
            );
            return $this->success($response);
         }   
    }

    
    public function get_all_profile($where = null)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A'";

        if(!is_null($where))
            $query .= ' AND '.$where;

        $profile_list = $this->db->fetchAll($query);

        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }
    
    public function get_all_profile_selected($user_type_id)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A' AND create_by =". $user_type_id;
        $profile_list = $this->db->fetchAll($query);
        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }    
    
    public function get_all_profile_access($id)
    {
        $query = 'SELECT resource_id FROM user_templates WHERE user_type_id =' . $id;
        $access_list = $this->db->fetchAll($query);

        if(!$access_list)
            return array();
        else
            return $access_list;
    }
    

    public function get_all_modules_list($where = null)
    {
        $query = 'SELECT * FROM resources where deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $query .= ' ORDER BY description';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    public function get_all_modules_list_selected($user_type_id)
    {
        //$query = 'SELECT * FROM resources where deleted = 0';
        $query = 'SELECT r.* FROM resources r inner join user_templates ut on ut.resource_id = r.id';
        $query .= ' WHERE deleted = 0 and user_type_id = ' . $this->db->db_escape($user_type_id);         
        $query .= ' ORDER BY resource';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    protected function is_profile_existing($profile_name)
    {
        $query = 'select * from user_types where description ='.$this->db->db_escape($profile_name);
        $check_profile = $this->db->fetchAll($query);
        if (count($check_profile)>0) {
           return true;
        } else {
           return false;
        }
           
    }

    public function add_profile($params)
    {
        if(count($params['module_access']) == 0 || !isset($params['module_access']))
            return $this->failed('Select a module');
            
        //print_r($params['module_access']);
        //die();

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }
        
        if($this->is_profile_existing($params['description'])){
            return $this->failed('Profile already exist!');
        }

        $data = array(
            'description'   => $params['description'],
            'create_by'    => $params['created_by']
        );

       if(!$this->db->insert('user_types', $data)) {
            return $this->failed("Unable to add profile");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
            //print_r($id);
            // delete all access from user_templates
            $this->db->delete('user_templates', 'user_type_id=' . $id);
            // insert new access rights
            foreach($params['module_access'] as $data) {
                $insert_data = array(
                    'resource_id'   => $data,
                    'user_type_id'  => $id,
                );
                $this->db->insert('user_templates', $insert_data);
            }
            return $this->success('Profile Saved');
        }
    }

    public function edit_profile($params)
    {
        if(!isset($params['module_access']) || count($params['module_access']) == 0)
            return $this->failed('You cant add a profile with no module');

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }

        if(!is_numeric($params['id']))
            return FALSE;
        
        $id = $params['id'];
        // delete all access from user_templates
        $this->db->delete('user_templates', 'user_type_id=' . $id);
        // insert new access rights
        foreach($params['module_access'] as $data) {
            $insert_data = array(
                'resource_id'   => $data,
                'user_type_id'  => $id,
            );
            $this->db->insert('user_templates', $insert_data);
        }
        return $this->success('Profile updated');
    }

    public function delete_profile($profile_id = null)
    {
        if(is_null($profile_id) || !is_numeric($profile_id))
            return $this->failed('Invalid ID');

        if($this->db->update('acl_profile', array('deleted' => 1), 'id='.$this->db->db_escape($profile_id))){
            $response = array(
                'message' => "Successfully deleted module",
            );
            return $this->success($response);
        }
        else{
            $response = array(
                'message' => "Unable to delete profile",
            );
            return $this->failed($response);
        }
    }
    
    public function get_all_resources()
    {
        $query = 'SELECT * FROM resources where deleted = 0';
        $query .= ' ORDER BY id';
        //print_r($query);
        //die();
        $module_list = $this->db->fetchAll($query);
        return $this->success($module_list);
    }
    
    public function update_module($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
           'resource'      => $params['value'], 
           'description'    => $params['description'],
        );
        if (!($this->db->update('resources', $update_data, 'id=' . $params['id']))) {
            $response = array(
                'success' => false,
                'message' => 'Unable to update module.',
            );
            return $this->failed($response);    
        } else {
            $response = array(
                'success' => true,
                'message' => 'Successfully updated module',
            );
            return $this->success($response);
        }
    }
    
    // End of New code for go3 rewards
    
    public function get_all_managers()
    {
        $cmd="select id, concat(fname,' ', lname) as name from users where user_type = 5 and status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    

    
    public function replace_schar($val){
        
          $replace = array(
                    '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                    '&quot;' => '',
                    'À' => 'A', '??' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                    '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                    'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', '??' => 'D',
                    '??' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                    'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                    'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', '??' => 'I',
                    'Î' => 'I', '??' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                    'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                    'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                    'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                    'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                    'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                    'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                    '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                    '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                    'W' => 'W', '??' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                    'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                    'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                    'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                    'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                    'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                    'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                    'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                    'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                    'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                    '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                    '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                    '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                    'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                    'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                    'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                    'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                    '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                    '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                    '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                    '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                    '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                    '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                    '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                    '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                    '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                    '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                    '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                    '?' => 'yu', '?' => 'ya', "'"=>"`"
                    );
                    
                    $final = str_replace(array_keys($replace), $replace, $val);
        return $final;
        
    }
    
    public function get_email_list()
    {
        
    }
    public function  check_if_has_callables($po_id)
    {
        $cmd = "select ptc.status_code,sct.name from project_template_code ptc
        INNER JOIN status_code_type sct on  ptc.type = sct.id 
        INNER JOIN project_template pt on pt.id = ptc.project_template_id
        INNER JOIN po p on p.project_template_id=pt.id
        INNER JOIN po_transaction_data_ans_2_{$po_id}  ptds on p.id = ptds.po_id 
        where sct.name = 'Callable' and  p.id = {$po_id}";
        $rs = $this->db->fetchAll($cmd);
        
        if(count($rs)>0){
            return 1;    
        }else{
            return 2;
        }
        
        
    }
    
    public function  get_number_complete($po_id)
    {
        $cmd = "select ptc.status_code,sct.name from project_template_code ptc
        INNER JOIN status_code_type sct on  ptc.type = sct.id 
        INNER JOIN project_template pt on pt.id = ptc.project_template_id
        INNER JOIN po p on p.project_template_id=pt.id
        INNER JOIN po_transaction_data_ans_2_{$po_id}  ptds on p.id = ptds.po_id  AND UPPER(ptds.status_code)=UPPER(ptc.status_code)
        where sct.name = 'Complete' and  p.id = {$po_id}";
      
        $rs = $this->db->fetchAll($cmd);
        
        if(count($rs)>0){
            return count($rs);    
        }else{
            return 0;
        }
        
        
    }
    
    
    public function get_po_code_name($id)
    {
        $cmd = "select name,po_no from po where id ={$id}";
        $rs = $this->db->fetchRow($cmd);
        return $rs;
    }
       public function get_goal_history($params)
    {
        $cmd = "";
        
        $rs = $this->db->fetchAll($cmd);
        return $this->success($rs);
    }
    
    public function get_email_by_username($username){
        //System Admin, Admin, Manager, PO creator
        $emails="";
         $cmd = "select email_address from users u 
         where u.user_type in(1,2,5) or u.username='{$username}' and u.status = 'A'";
        $rs = $this->db->fetchAll($cmd);

        return $rs;
    }
     //Abby 3-14-16 Added goal_days_of_week and goal_value
     public function get_po_schedule_all()
     { 
        $cmd="select p.po_no, st.code as state, p.name ,goal.goal_days_of_week, goal.goal_value, CASE goal.goal_type WHEN '1' THEN 'Saturation' WHEN '2' THEN 'Targets' WHEN '3' THEN 'Stop Number' WHEN '4' THEN 'Hours' END as goal_type, prm.end_date, prm.caller_id, prm.goal_note , 'TEST' as center FROM po p INNER JOIN po_parameter prm ON p.id = prm.po_id INNER JOIN po_goal goal ON p.id = goal.po_id INNER JOIN states st ON prm.state=st.id";
         
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    
  }
    
    public function get_co_response($params)
    {
        $cmd = "SELECT cr.id,cr.create_date,cr.comment,CONCAT(us.fname,' ',us.lname) as name FROM co_response cr
        INNER JOIN users us on cr.create_by = us.username
        WHERE cr.co_id = {$params['co_id']}";
        $rs = $this->db->fetchAll($cmd);
        return $rs;
    }
    
    public function add_co_response($params)
    {
         $insert_co = array(
            'po_id'=>$params['txtID'], 
            'co_id'=>$params['id'], 
            'comment'=>$params['co_notes'], 
            'create_by'=>$_SESSION['username'],
        ); 
        if (!$this->db->insert('co_response', $insert_co)) {
            return $this->failed("Unable to add response.");
        } else {
            $update_data = array(
                    'status'=>$params['co_status'],
                    'update_by'=>$_SESSION['username'],
                    'update_date'=>date('Y-m-d H:i:s'), 
            );
            $this->db->update('change_order', $update_data,'id=' . $params['id']);  
            
            $response = array('message' => "Response has been submitted.",); 
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Change Order has been updated','PO',$datetime,$_SESSION['username'],$params['txtID']);
            $cmd = "select po_no from po where id ={$params['txtID']}";
            $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            Change Order has been modified for PO number {$rs['po_no']} by  {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
           foreach($email as $s){
            	$this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");  
            }
            return $this->success($response);
        }
    }
    public function get_change_order_list($params)
    {
        $cmd = "SELECT CONCAT(us.fname, ' ', us.lname) as assigned, co.id, co.po_id, co.center_id, co.user_id, CASE change_type 
        WHEN 1 THEN 'Date / Time Change' 
        WHEN 2 THEN 'Script Change'
        WHEN 3 THEN 'Project Hold'
        WHEN 4 THEN 'Data Change'
        WHEN 5 THEN 'Complaint Investigation'
        WHEN 6 THEN 'Monitor Request'
        WHEN 7 THEN 'Goals Change'
        WHEN 7 THEN 'Other' END as change_type,
        CASE co.status 
        WHEN 'N' THEN 'NEW'
        WHEN 'A' THEN 'ASSIGNED'
        WHEN 'P' THEN 'PROCESSING'
        WHEN 'H' THEN 'HOLD'
        WHEN 'C' THEN 'COMPLETED' END as status,
        co.description, co.notes, co.create_date, 
        co.create_by, co.update_date, co.update_by FROM change_order co
        INNER JOIN users us on co.user_id = us.id WHERE co.po_id={$params['po_id']} AND co.center_id={$_SESSION['user_center']}";
     
        $rs = $this->db->fetchAll($cmd);
        return $this->success($rs);
    }
    
    public function create_change_order($params)
    {
         $insert_co = array(
            'po_id'=>$params['txtID'], 
            'status'=>'N',  
            'center_id'=>$params['change_type'],  
            'user_id'=>$params['assigned_user'],  
            'change_type'=>$params['change_type'], 
            'description'=>$params['txtDesc'], 
            'notes'=>$params['co_notes'], 
            'create_by'=>$_SESSION['username'],
        ); 
        if (!$this->db->insert('change_order', $insert_co)) {
            return $this->failed("Unable to add new change order.");
        } else {
            $response = array('message' => "New change order has been submitted.",); 
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Submitted new Change Order','CO',$datetime,$_SESSION['username'],$params['txtID']);
            
            $cmd = "select po_no from po where id ={$params['txtID']}";
            $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            Change Order has been created for PO number {$rs['po_no']} by  {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
           foreach($email as $s){
                $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");      
           }
            return $this->success($response);
        }
    }
    
    public function get_users_per_center($center_id)
    {
        $cmd = "SELECT id, CONCAT(fname, ' ', lname) name FROM users WHERE status ='A' and user_center = {$center_id}"  ;
        return $this->db->fetchPairs($cmd);    
    } 
    public function get_for_coby_id($params)
    {
    $cmd = "SELECT CONCAT(us.fname, ' ', us.lname) as creator, co.id, co.po_id, co.center_id, co.user_id, CASE change_type 
        WHEN 1 THEN 'Date / Time Change' 
        WHEN 2 THEN 'Script Change'
        WHEN 3 THEN 'Project Hold'
        WHEN 4 THEN 'Data Change'
        WHEN 5 THEN 'Complaint Investigation'
        WHEN 6 THEN 'Monitor Request'
        WHEN 7 THEN 'Goals Change'
        WHEN 7 THEN 'Other' END as change_type,
        CASE co.status 
        WHEN 'N' THEN 'NEW'
        WHEN 'A' THEN 'ASSIGNED'
        WHEN 'P' THEN 'PROCESSING'
        WHEN 'H' THEN 'HOLD'
        WHEN 'C' THEN 'COMPLETED' END as status,
        co.description, co.notes, co.create_date, 
        co.create_by, co.update_date, co.update_by FROM change_order co
        INNER JOIN users us on co.create_by = us.username WHERE co.id={$params['co_id']}";
        return $this->db->fetchAll($cmd);    
    } 
    
    
     public function get_center_for_co($po_id)
    {
        $cmd = "SELECT cc.id,cc.name FROM call_center cc 
        INNER JOIN center_calling_list ccl on cc.id=ccl.center_id 
        WHERE cc.status='A' AND ccl.po_id={$po_id}"  ;
        return $this->db->fetchPairs($cmd);    
    } 
    
    public function get_usertype($username){
        $cmd = "select ut.description as user_type from user_types ut 
        inner join users u on u.user_type = ut.id
        where u.username = '{$username}'";
        $rs = $this->db->fetchRow($cmd);
        return $rs['user_type'];
    }    
    
     
  public function reassign_specific_center($center_id, $po_id, $batch_no, $from, $to)
    {
        $get_column_cmd="select col_fields, process_option_id, phone_field from po_transaction_data_2_column dc where is_column =1 and  po_id ={$po_id} AND batch_no={$batch_no} limit 1";
        
        $row = $this->db->fetchRow($get_column_cmd);
        
        $phone_field = $row['phone_field'];
        $col_fields = explode("|",unserialize($row['col_fields']));
        $col_fields_db ="";
        foreach($col_fields as $r)
        {
            $col_fields_db.="da.{$r}," ;   
        }
        
        $status_codes = $this->get_status_codes($po_id);
        
        $status_codes_callable = "";
        

        foreach($status_codes as $s)
        {
               if( strtolower($s['status_type']) =="complete")//  if( strtolower($s['status_type']) =="callable")
            {
                $status = $s['status_code'];
                $status_codes_callable .= "'{$status}',";
            }
        }
        
        if($status_codes_callable != "") {
            $status_codes_callable = substr($status_codes_callable, 0, strlen($status_codes_callable)-1); 
        }
 
        
        $process_option_id = $row['process_option_id'];
        //print_r($col_fields_db);
        $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);
        
        $first_name_field = $this->get_first_name_field($po_id);
      
        $cmd ="SELECT {$col_fields_db} FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";
        $res = $this->db->fetchRow($cmd);
        
        
        $col_cmd ="SELECT * FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";
        $columns = $this->db->fetchRow($col_cmd);
        $col_ctr =1;
        while($columns["col{$col_ctr}"] <> "")
        {
            if( trim($columns["col{$col_ctr}"]) == trim($phone_field))
            {
                $phone_field = "col{$col_ctr}";
                break;
            }
            $col_ctr ++;
        }
        $formated_po_id=$this->get_po_number($po_id);
        $file_name =$formated_po_id .'_'. time() .".csv";
        
        
        
        $cmd ="SELECT {$col_fields_db} FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";    
        
        

        $res = $this->db->fetchRow($cmd);
         $data_result =$this->check_data_if_result_exist($po_id);
         $join = "";
         if($data_result>0){
            $join = " INNER ";    
         }else{
            $join = " LEFT "; 
         }
      
      $fp = fopen('php://output', 'w');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
       fputcsv($fp, array_values($res)); 
        if($process_option_id <> 1)
        {
            $cmd ="SELECT  {$col_fields_db} FROM po_transaction_data_2_{$po_id} da
                 WHERE da.phone_no not in (
                SELECT cl.phone_no FROM po_transaction_data_2_calling_list_{$po_id} cl
              
                INNER JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2
                WHERE  cl.id>={$from} AND  cl.id<={$to}";
              //  LEFT JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2  
            if($status_codes_callable != "")
            {
                /*$cmd .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable})) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no}";*/
                $cmd .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable}))";
            }else{
                /*$cmd .="
                AND cl.batch_no={$batch_no}) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no} ";      */ 
                $cmd .="
                AND cl.batch_no={$batch_no})";
                          
                
            }
      
           // $cmd .=" WHERE da.id>={$from} AND da.id<={$to}";
            $cmd .=" AND da.id>={$from} AND da.id<={$to}";
            
           $res_out = $this->db->fetchAllTOFile($cmd, $fp);          
            
            $for_insert  ="";
            $for_insert .= "{SELECT}";
            $for_insert .= " FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in  (
            SELECT cl.phone_no FROM po_transaction_data_2_calling_list_{$po_id} cl
            INNER JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2
            WHERE  cl.id>={$from} AND  cl.id<={$to}";
            
            if($status_codes_callable != "")
            {
                /*$for_insert .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable})) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no}";*/
                
                $for_insert .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable}))";
              
                
            }else{
               /* $for_insert .="
                AND cl.batch_no={$batch_no}) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no} ";          */      
                
                 $for_insert .="
                AND da.batch_no={$batch_no} ";          
                
            }
            $for_insert .=" AND da.id>={$from} AND da.id<={$to}";
//            $for_insert .=" WHERE da.id>={$from} AND da.id<={$to}";
          
            $res = $this->db->fetchReassignToFile("", "", $phone_field, $po_id, $for_insert, $batch_no,$from,$to);
             $this->db->query("UPDATE center_calling_list set assign_record_cnt = assign_record_cnt-{$res_out} , is_reassign = 1 WHERE record_cnt_from = {$from} AND record_cnt_to={$to} and po_id ={$po_id} AND  batch_no = {$batch_no}");
             
             $this->db->query("UPDATE po_parameter set universe = universe - {$res_out} WHERE po_id={$po_id}");
             
              $this->db->update('po_parameter', $values, 'po_id='.$id);
        }else
        {
            $cmd ="SELECT  {$col_fields_db} 
            FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in (
            SELECT col2 FROM po_transaction_data_ans_2_{$po_id}
            WHERE  crid_permanent>={$from} AND  crid_permanent<={$to}
            ";
            if ($status_codes_callable !== "")
            {
                   $cmd .=" AND status_code IN({$status_codes_callable})) AND da.batch_no={$batch_no}";
             /*   $cmd .=" AND status_code IN({$status_codes_callable})) AS T
                ON T.col2=da.phone_no AND da.batch_no={$batch_no}";*/
            }else{
                $cmd .="  AND da.batch_no={$batch_no}";
            }
            $cmd .=" AND da.id>={$from} AND da.id<={$to}";
       
            $res_out = $this->db->fetchAllTOFile($cmd, $fp);  
            $for_insert  ="";
            $for_insert .= "{SELECT}";
            $for_insert .= " FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in (
            SELECT  col2 FROM po_transaction_data_ans_2_{$po_id}
            WHERE  crid_permanent>={$from} AND  crid_permanent<={$to}";
            if ($status_codes_callable !== "")
            {
               /* $for_insert .=" AND status_code IN({$status_codes_callable})) AS T
                ON T.col2=da.phone_no AND da.batch_no={$batch_no}";*/
                 $for_insert .=" AND status_code IN({$status_codes_callable})) AND da.batch_no={$batch_no}";
            }else{
                $for_insert .=" AND da.batch_no={$batch_no}";
            }
            $for_insert .=" AND da.id>={$from} AND da.id<={$to}";
            
            $this->db->query("UPDATE center_calling_list set assign_record_cnt = assign_record_cnt-{$res_out} , is_reassign = 1 WHERE record_cnt_from = {$from} AND record_cnt_to={$to} and po_id ={$po_id} AND  batch_no = {$batch_no}");
            $this->db->query("UPDATE po_parameter set universe = universe - {$res_out} WHERE po_id={$po_id}");
            $res = $this->db->fetchReassignToFile("", "", $phone_field, $po_id, $for_insert, $batch_no, $from, $to);
              
        }
    

}


        
    private function get_first_name_field($po_id)
    {       
        $cmd ="SELECT fname_field FROM po_transaction_data_2_column WHERE po_id={$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['fname_field']))
        {
            return '';
        }                                     
        
        return  $result['fname_field'];
    }

    
    public function get_action_logs($action_module,$id)
    {
         if($id>0){
            
             $cmd="select * from action_logs  where action_module ='{$action_module}' and  remark='{$id}' order by id desc limit 5";
        }else{
            $cmd="select * from action_logs order by id desc" ;
        }
        
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    }
    
      public function check_script_if_exist($id)
    {
        //$cmd="select po_id from po_transaction_data_ans_2 where po_id={$id} limit 1";
        $cmd="select po_id from po_script_question where po_id={$id}";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    
    
     public function send_mail_notification($body,$email_address,$subject='')
    {   date_default_timezone_set('America/Toronto');
        
        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
        //$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        if(!IS_PRODUCTION)
		{
		 	$mail->SMTPAuth   = true;                  // enable SMTP authentication
        	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        }
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');      
        $mail->From = 'admin@go3rewards.com';
        $mail->FromName = 'Go3 Admin';
       
	   
        
		
		
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             //$mail->addAddress($ad);
             $mail->AddAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
            
        
        if(!$mail->Send()) {
        	// print_r($mail->ErrorInfo());
			// die();
            return false;//
        } else {
            return true;// $this->success("Message sent!");
        } 
    }
    
     public function send_mail($body,$email_address,$path,$filename='',$subject='')
    {   date_default_timezone_set('America/Toronto');

        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
           $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
       // $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');      
        $mail->From = 'noreply@pbs-server.com';
        $mail->FromName = 'PBS Admin';
        if($filename!=''){
            $mail->addAttachment($path.$filename,$filename);    
        }
        
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             $mail->addAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
            
        
        if(!$mail->Send()) {
            return false;// $this->failed("Mailer Error: " . $mail->ErrorInfo);
        } else {
            unlink($path.$filename);
            return true;// $this->success("Message sent!");
        } 
        
        
    }
    
    
    public function get_poid($po_number)
    {
        $cmd = "SELECT id FROM po WHERE po_no = '{$po_number}'";
        
        $rs = $this->db->fetchRow($cmd);
        
        if(isset($rs['id']))
        {
            return $rs['id'];
        }else{
            return -1;
        }
    }
    
    
    public function get_question_results_amount($po_id, $status_codes, $questions, $start_date, $end_date, $filter_status="")
    {
        
        $status_code_column = $this->get_result_column_position($po_id);   
        
        $question_column = $this->get_result_column_position($po_id, "Q1");
        
        
        if($question_column  != "")
        {
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
        }else
        {
            $column_q ="";
        }
        $complete_status ="";
        $complete_status_for_calldate = "";
        $case_query ="";
        
        
        foreach($status_codes as $s)
        {
            $status_type = $s['status_type'];
//            if(strtolower($status_type) =="complete")
//            {
                $complete_status.="'{$s['status_code']}',"; 
//            }

           if(strtolower($status_type) =="complete")
           {
                $complete_status_for_calldate.="'{$s['status_code']}',";
           }
        }
        
     
        $complete_status = substr($complete_status,0, strlen($complete_status)-1);
        $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
         
        $select_case ="";
        $select_case_total ="";
        
        $responses ="";
        
         $is_gotv = true;
        foreach($questions  as $q)
        {
            $q_no = $q['question_no'];
//            $select_case .= "SUM(CASE col{$column_q} ";
//            $responses .= "CASE col{$column_q} ";
            
//            $select_case_total .= "SUM(CASE col{$column_q} ";
//            foreach($q['responses'] as $r)
//            {
//                $resp_no =  $r['row_no'];
//                $response =  $r['response'];
//                $rate =  $r['rate'];
//                $select_case .= " WHEN {$resp_no} THEN {$rate} ";
//                $select_case_total .= " WHEN {$resp_no} THEN {$rate} ";
//                $responses .= " WHEN {$resp_no} THEN '{$response}' ";
//                $responses .="ELSE '' END  AS Q{$q_no}_{$resp_no},";
//            }
//            $select_case .="ELSE 0 END ) AS Q{$q_no},";
//            $select_case_total .="ELSE 0 END ) +";
                
                
                $q_no = $q['question_no'];
                foreach($q['responses'] as $r)
                {
                    $resp_no =  $r['row_no'];
                    $rate =  $r['rate'];
                    $select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN {$rate} ELSE 0 END) AS Q{$q_no}{$resp_no},";                   
                    
                    $select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN 1 ELSE 0 END) AS QQty{$q_no}{$resp_no},";                   
                    
                    
                    
                    $select_case_total .= "SUM(CASE col{$column_q} ";
                    $select_case_total .= " WHEN {$resp_no} THEN {$rate} ELSE 0 END ) +";
                }
                $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
                $select_case_total .=" AS Q{$q_no},";
                //$select_case .= " SUM(CASE col{$column_q} WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
                $column_q++;
                $is_gotv = false;
        }
        
/*        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $responses = substr($responses, 0, strlen($responses)-1);*/
        
        
		
        if($is_gotv)
        {
            foreach($status_codes as $s)
            {
                $select_case .= " SUM(CASE status_code WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ELSE 0 END) AS Q{$s['status_code']},";                   

                $select_case .= " SUM(CASE status_code WHEN '{$s['status_code']}' THEN 1 ELSE 0 END) AS QQty{$s['status_code']},";   
                
                $select_case_total .= "SUM(CASE status_code ";
                $select_case_total .= " WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ELSE 0 END ) +";
                           
            }
			$select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
			$select_case_total .=" AS TotalAmount ";
        }
        
/*        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $responses = substr($responses, 0, strlen($responses)-1);*/
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        
        //$cmd =" SELECT {$select_case}, {$select_case_total} as TotalAmount, {$responses} FROM po_transaction_data_ans_2 WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
        
        //$cmd =" SELECT {$select_case}, {$select_case_total} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
        $cmd =" SELECT {$select_case}, {$select_case_total} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND status_code IN({$complete_status})";

        $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}'";
        
        
//        if($filter_status <> '')
//        {
//            $cmd .= " AND {$status_code_column} = '{$filter_status}'";
//        }

        // print_r($cmd);
        // die();
        
		/*print_r($this->db->fetchRow($cmd));
		die();*/
        return $this->db->fetchRow($cmd);
    }
    
    
    
    //billing report
    public function get_billing_report_summary($start_date, $end_date, $client_id)
    {
        
        //for testing
        if($client_id =="")
        {
            //load for testing
            $client_id =9 ;
            $star_date = "2016-02-09";
            $end_date = "2016-02-09";
        }
        
        //end for testing
        //get_script_question_response gather all responses and rate for case condition
        $cmd ="SELECT pp.*,p.po_no FROM po p
        LEFT JOIN po_parameter pp ON pp.po_id=p.id
        WHERE p.status<>'D' and p.client_id={$client_id}";
       
        //$cmd .=" AND p.id IN(2,3,4,5,6,7,9,10,11)";

     
        //WHERE  p.client_id={$client_id} AND p.status IN('A', 'C')"; //active and completed only
        //start_date>='{$start_date}' AND end_date<='{$end_date}'";
    
        $rs = $this->db->fetchAll($cmd);
  
             
          
        $total_amount = 0;
        $biller_summary_records = array();
         
        $cmd ="";

       foreach($rs as $r)
        {   
            
            $po_start_date = $r['start_date'];
            $po_end_date = $r['end_date'];
            
            
            $po_no = $r['po_no'];
            $po_id = $r['po_id'];
            $question_column = $this->get_result_column_position($po_id, "Q1");
           
             
            if($question_column =="")
            {  
                //goto nextPO;
                goto GOTV; //without question
            }
             
             
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
   
               
             
            $status_codes = $this->get_status_codes($po_id);
            
            $status_code_column = $this->get_result_column_position($po_id);
             
                  
            $complete_status ="";
            $case_query ="";
            foreach($status_codes as $s)
            {
                $status_type = $s['status_type'];                    
                if(strtolower($status_type) =="complete")
                {
                    $complete_status.="'{$s['status_code']}',";
                }
            }   
            if($complete_status !="")
            {
                $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
            }
            
            $questions = $this->get_script_question_response($po_id);
             
            $q_no =1;
            $select_case = "";
            foreach($questions as $q)
            {
                $q_no = $q['question_no'];
                $select_case .= "(CASE col{$column_q} ";
                foreach($q['responses'] as $r)
                {
                    $resp_no =  $r['row_no'];
                    if($r['rate']==""){
                        $rate = 0;
                    }else{
                        $rate =  $r['rate'];    
                    }
                    
                    $select_case .= " WHEN {$resp_no} THEN {$rate} ";
                }
                $select_case .="ELSE 0 END ) +";
                $column_q++;            
            }
            
            $select_case = substr($select_case, 0, strlen($select_case)-1);

            
             
            $cmd ="SELECT p.po_no, p.name,c.name as client_name, '{$po_start_date}' as po_start_date, '{$po_end_date}' as po_end_date,  SUM({$select_case}) AS total_amount";
            $cmd .=" FROM po p
                LEFT JOIN po_transaction_data_ans_2_{$po_id} a on p.id=a.po_id
                LEFT JOIN client c ON c.id=p.client_id
                WHERE str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}' AND p.client_id={$client_id}";
                

             
            
             
            $record = $this->db->fetchRow($cmd); 
            $total_amount = $record['total_amount'];
            $biller_summary_records['records'][] = $record;
            goto nextPO;
            
            GOTV:
            $status_codes = $this->get_status_codes($po_id);
            $complete_status ="";
            $case_query ="";
            $select_case ="";
            foreach($status_codes as $s)
            {
                 
                $status_type = $s['status_type'];                    
                if(strtolower($status_type) =="complete")
                {
                    $complete_status.="'{$s['status_code']}',";
                }
                
                $select_case .= "(CASE status_code ";
                $select_case .= " WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ";
                $select_case .="ELSE 0 END ) +";
            }   
            if($complete_status !="")
            {
                $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
            }
            
            $select_case = substr($select_case, 0, strlen($select_case)-1);
            
            $cmd ="SELECT p.po_no, p.name,c.name as client_name, '{$po_start_date}' as po_start_date, '{$po_end_date}' as po_end_date,  SUM({$select_case}) AS total_amount";
            $cmd .=" FROM po p
                LEFT JOIN po_transaction_data_ans_2_{$po_id} a on p.id=a.po_id
                LEFT JOIN client c ON c.id=p.client_id
                WHERE str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}' AND p.client_id={$client_id}";
                

          
          
           
            $record = $this->db->fetchRow($cmd); 
            $total_amount = $record['total_amount'];
            $biller_summary_records['records'][] = $record;
            
              
            nextPO:
        }
        
        $biller_summary_records['summary_amount'] = $total_amount;
            
        return $biller_summary_records;
    }
    //end billing report
    
    //--Start of Report
      public function get_summary_report_data($po_id, $call_date, $status, $center_id, $filter_status="")
    {
        $status_codes = $this->get_status_codes($po_id);
        $status_code_column = $this->get_result_column_position($po_id);
        
        $complete_status ="";
        $case_query ="";
        $callable_status ="";
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['status_code']))
        {
        
                foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
                    
                    if(strtolower($status_type) =="callable")
                    {
                        $callable_status.="'{$s['status_code']}',";
                    }
                    
                    
                    if(strtolower($status_type) =="complete")
                    {
                        $complete_status.="'{$s['status_code']}',";
                    }else{
                        $status_code = $s['status_code'];
                        //$case_query .=" SUM(CASE WHEN {$status_code_column} IN('{$status_code}') THEN 1 ELSE 0 END) AS {$status_code},";
						$case_query .=" SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN('{$status_code}') THEN 1 ELSE 0 END) AS {$status_code},";
                    }
                }   
                if($complete_status !="")
                {
                    $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
                    //$case_query .= " SUM(CASE WHEN {$status_code_column} IN({$complete_status}) THEN 1 ELSE 0 END) AS complete,";
					$case_query .= " SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN({$complete_status}) THEN 1 ELSE 0 END) AS complete,";
                }
                if($callable_status !=="")
                {
                    $callable_status = substr($callable_status, 0, strlen($callable_status)-1);
                    //$case_query .= " SUM(CASE WHEN trim({$status_code_column}) IN({$callable_status}) THEN 0 ELSE 1 END) AS penetration,";
					$case_query .= " SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN({$callable_status}) THEN 0 ELSE 1 END) AS penetration,";
                }else{
                    $case_query .= " SUM(1) AS penetration,";
                }
                
                $case_query = substr($case_query, 0, strlen($case_query)-1);
                $case_query = " SELECT {$case_query}";
                
                $cmd = "{$case_query} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id}";
                
                if($call_date =="default")
                {
                    $call_date = $this->get_min_call_date($po_id,$complete_status,$status_code_column);
                    $call_date = $call_date['call_date'];
                    
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    //$date_to = date_add($date, $diff1Day);
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$call_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$call_date}'";
                }
                
                if($filter_status <> '')
                {
                    $cmd .= " AND {$status_code_column} = '{$filter_status}'";
                }
                
                
                
                if($center_id <> '')
                {
                    $q ="SELECT min(record_cnt_from) as start_id, max(record_cnt_to) as end_id  
        FROM center_calling_list WHERE po_id={$po_id} AND center_id={$center_id}";
                    $ids = $this->db->fetchRow($q);
                    
                    $cmd .= " AND crid_permanent >= {$ids['start_id']} AND crid_permanent<={$ids['end_id']}";
                }
                
                
                
        }else{
                
                 foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
                    
                    if(strtolower($status_type) =="callable")
                    {
                        $callable_status.="'{$s['status_code']}',";
                    }
                    
                    
                    if(strtolower($status_type) =="complete")
                    {
                        $complete_status.="'{$s['status_code']}',";
                    }else{
                        $status_code = $s['status_code'];
                        $case_query .=" SUM(CASE WHEN status_code IN('{$status_code}') THEN no_of_count ELSE 0 END) AS {$status_code},";
                    }
                }   
                if($complete_status !="")
                {
                    $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
                    $case_query .= " SUM(CASE WHEN status_code IN({$complete_status}) THEN no_of_count ELSE 0 END) AS complete,";
                }
                if($callable_status !=="")
                {
                    $callable_status = substr($callable_status, 0, strlen($callable_status)-1);
                    $case_query .= " SUM(CASE WHEN status_code IN({$callable_status}) THEN 0 ELSE no_of_count END) AS penetration,";
                }else{
                    $case_query .= " SUM(1) AS penetration,";
                }
                
                $case_query = substr($case_query, 0, strlen($case_query)-1);
                $case_query = " SELECT {$case_query}";
                
                $cmd = "{$case_query} FROM po_manual_load WHERE po_id={$po_id}";
                
                if($call_date =="default")
                {
                    $call_date = $this->get_min_call_date_manual($po_id,$complete_status);
                    $call_date = $call_date['call_date'];
                    
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    //$date_to = date_add($date, $diff1Day);
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND call_date>='{$date}' AND call_date<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND call_date>='{$call_date}' AND call_date<='{$call_date}'";
                }
                
                if($center_id <> '')
                {
                    $cmd .= " AND center_id={$center_id}";
                }
                
                if($filter_status <> '')
                {
                    $cmd .= " AND status_code = '{$filter_status}'";
                }
                
                $cmd .=" GROUP BY question";
            
            
        }
//        print_r($cmd);
//        die();

        return $this->db->fetchRow($cmd);
    }

    
public function get_question_results($po_id, $status_codes, $questions, $call_date, $center_id, $filter_status="")
    {
       
        
        $complete_status ="";
        $complete_status_for_calldate = "";
        $case_query ="";
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        

        if(!isset($result['status_code']))
        {
            $status_code_column = $this->get_result_column_position($po_id);   
            $question_column = $this->get_result_column_position($po_id, "Q1");
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
                
                foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
        //            if(strtolower($status_type) =="complete")
        //            {
                        $complete_status.="'{$s['status_code']}',"; 
        //            }

                   if(strtolower($status_type) =="complete")
                   {
                        $complete_status_for_calldate.="'{$s['status_code']}',";
                   }
                }
                $complete_status = substr($complete_status,0, strlen($complete_status)-1);
                $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
                
                $select_case ="";
                
                
                foreach($questions  as $q)
                {
                    $q_no = $q['question_no'];
                    foreach($q['responses'] as $r)
                    {
                        $resp_no =  $r['row_no'];
                        //$select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN 1 ELSE 0 END) AS Q{$q_no}{$resp_no},";
						
						$select_case .= " SUM(CASE ".'replace(col'."{$column_q}".',"\r","")'." WHEN {$resp_no} THEN 1 ELSE 0 END) AS Q{$q_no}{$resp_no},";
                    }
                    //$select_case .= " SUM(CASE col{$column_q} WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
					$select_case .= " SUM(CASE ".'replace(col'."{$column_q}".',"\r","")'." WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
                    $column_q++;
                }
                
                $select_case  = substr($select_case, 0, strlen($select_case)-1);
                
                $cmd =" SELECT {$select_case} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
                
                if($call_date =="default" )
                {
                    $call_date = $this->get_min_call_date($po_id,$complete_status_for_calldate,$status_code_column);
                    
                    $call_date = $call_date['call_date'];
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$call_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$call_date}'";
                }
                
                if($center_id <> '')
                {
                    $q ="SELECT min(record_cnt_from) as start_id, max(record_cnt_to) as end_id  
        FROM center_calling_list WHERE po_id={$po_id} AND center_id={$center_id}";
                    $ids = $this->db->fetchRow($q);
                    
                    $cmd .= " AND crid_permanent >= {$ids['start_id']} AND crid_permanent<={$ids['end_id']}";
                }
                if($filter_status <> '')
                {
                    $cmd .= " AND {$status_code_column} = '{$filter_status}'";
                }
    }else{
        
             foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
        //            if(strtolower($status_type) =="complete")
        //            {
                        $complete_status.="'{$s['status_code']}',"; 
        //            }

                   if(strtolower($status_type) =="complete")
                   {
                        $complete_status_for_calldate.="'{$s['status_code']}',";
                   }
                }
                $complete_status = substr($complete_status,0, strlen($complete_status)-1);
                $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
                
                $select_case ="";
                
                
                foreach($questions  as $q)
                {
                    $q_no = $q['question_no'];
                    foreach($q['responses'] as $r)
                    {
                        $resp_no =  $r['row_no'];
                        $select_case .= " SUM(
							CASE question WHEN {$q_no} THEN
								CASE response WHEN {$resp_no} THEN no_of_count ELSE 0 END
							ELSE 
								0 
							END
						) AS Q{$q_no}{$resp_no},";
                    }
                    $select_case .= " SUM(
						CASE question WHEN {$q_no} THEN
							CASE response WHEN 0 THEN 0 WHEN '' THEN 0 ELSE no_of_count END
						ELSE 
							0 
						END
						) AS QTotal{$q_no},";
                    //$column_q++;
                }
                
                $select_case  = substr($select_case, 0, strlen($select_case)-1);
                
                $cmd =" SELECT {$select_case} FROM po_manual_load WHERE po_id={$po_id} AND status_code IN({$complete_status})";
                
                if($call_date =="default" )
                {
                    $call_date = $this->get_min_call_date_manual($po_id,$complete_status_for_calldate);
                    
                    $call_date = $call_date['call_date'];
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND call_date>='{$date}' AND call_date<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND call_date>='{$call_date}' AND call_date<='{$call_date}'";
                }
                
               
                if($filter_status <> '')
                {
                    $cmd .= " AND status_code = '{$filter_status}'";
                }
                if($center_id <> '')
                {
                    $cmd .= " AND center_id={$center_id}";
                }
				
				// if($call_date !='')
				// {
					// print_r($cmd);
					// die();
				// }
                
                // $cmd .=" GROUP BY question";
                

              
        }
				 // if($call_date !='')
				 // {
					 // print_r($cmd);
					 // //print_r($this->db->fetchRow($cmd));
					 // die();
				 // }
	  
        return $this->db->fetchRow($cmd);
    }


  
        
    public function get_script_question_response($po_id)
    {
        $cmd = "SELECT * FROM po_script_question WHERE po_id={$po_id} AND sequence>0 
AND sequence <= (SELECT number_of_question FROM po_parameter WHERE po_id={$po_id})   ORDER BY sequence";
        $questions = $this->db->fetchAll($cmd);
        $q_response = array();
        foreach($questions as $q)
        {
            $cmd = "SELECT row_no, response, rate  FROM po_script_q_response
WHERE po_script_question_id={$q['id']} ORDER BY row_no";
            $responses = $this->db->fetchAll($cmd);
            
            $q_response[] = array(
                'question_no' => $q['sequence'],
                'question' => $q['question'],
                'responses' => $responses,
                'rate' => $q['rate']
            );
        }
        return $q_response;
    }

    public function get_universe_start_date($po_id)
    {
        $cmd ="SELECT start_date, universe FROM po_parameter WHERE po_id={$po_id}";
        return $this->db->fetchRow($cmd);
    }
    
    public function get_result_dates($po_id)
    {
        
        $complete_status="";
        //use the actual status
		//$status_code_column = $this->get_result_column_position($po_id);     
		
        $status_codes = $this->get_status_codes($po_id);
         foreach($status_codes as $s)
         {
            $status_type = $s['status_type'];
            if(strtolower($status_type) =="complete")
            {
                $complete_status.="'{$s['status_code']}',";
            }
         } 
        
        $complete_status  = substr($complete_status, 0, strlen($complete_status)-1);
        $cmd ="SELECT po_id FROM po_manual_load WHERE po_id= {$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        if(isset($result['po_id']))
        {
            $cmd = "SELECT DISTINCT call_date FROM po_manual_load WHERE status_code in({$complete_status}) and  po_id={$po_id} ORDER BY call_date";
            return $this->db->fetchAll($cmd);
        }
         
        //$cmd = "SELECT DISTINCT call_date FROM po_transaction_data_ans_2_{$po_id} WHERE {$status_code_column} in({$complete_status}) AND po_id={$po_id} ORDER BY str_to_date(call_date,'%m/%d/%Y')";
		$cmd = "SELECT DISTINCT call_date FROM po_transaction_data_ans_2_{$po_id} WHERE " . 'replace(status_code,"\r","")'." in({$complete_status}) AND po_id={$po_id} ORDER BY str_to_date(call_date,'%m/%d/%Y')";
		

        return $this->db->fetchAll($cmd);     
        
           
    }
    
    public function get_result_centers($po_id)
    {
        $cmd ="SELECT c.center_id, cc.name as center_name FROM center_calling_list c LEFT JOIN call_center cc ON cc.id=c.center_id  WHERE po_id={$po_id}";
        return $this->db->fetchAll($cmd);
    }
    
    private function get_result_column_position($po_id, $column_to_search ="status_code")
    {
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
          
        $result = $this->db->fetchRow($cmd);
        
        if(isset($result['status_code']))
        {
           
            return '';
            //$cmd = "SELECT DISTINCT status_code FROM po_manual_load WHERE po_id={$po_id}";
            //return $this->db->fetchAll($cmd);
        }
            
        $cmd ="SELECT remarks FROM po_transaction_data_ans_2_column WHERE po_id={$po_id} LIMIT 1";
        
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['remarks']))
        {
            return '';
        }

          
        
        $columns = explode(";", $result['remarks']);
        foreach($columns as $c)
        {
            $column_name = explode("|", $c);

            $col_name = $column_name[1];
            if($col_name == $column_to_search)
            {
                $col_name = $column_name[0];
                return $col_name;
                break;
            } 
        }
    }
    
    public function get_status_codes($po_id, $remove_complete = 'no')
    {
        $cmd =" SELECT pt.status_code, sc.name as status_type,pt.pay_rate FROM po p 
        LEFT JOIN project_template_code pt on pt.project_template_id=p.project_template_id
        LEFT JOIN status_code_type sc on sc.id=pt.type
        WHERE p.id={$po_id} ";
        if($remove_complete =="yes")
        {
            $cmd .= " AND sc.name <> 'Complete'";
        }
        $cmd .=" ORDER BY status_type";
        
        return $this->db->fetchAll($cmd);
    }
    
    private function get_min_call_date($po_id,$complete_status_for_calldate,$status_code_column)
    {
        $cmd = "SELECT min(str_to_date(call_date,'%m/%d/%Y')) as call_date FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} in({$complete_status_for_calldate})";
        return $this->db->fetchRow($cmd);
    }
    private function get_min_call_date_manual($po_id,$complete_status_for_calldate)
    {
        $cmd = "SELECT min(str_to_date(call_date,'%m/%d/%Y')) as call_date FROM po_manual_load WHERE po_id={$po_id} AND status_code in({$complete_status_for_calldate})";
        return $this->db->fetchRow($cmd);
    }
    
    //--End of Report Script
    
    public function get_answer_call_date($po_id,$center_id){
            $call_date_list=$this->db->fetchAll("SELECT DISTINCT pda.call_date FROM po_transaction_data_ans_2_{$po_id} pda
            INNER JOIN center_calling_list ccl on(pda.po_id=ccl.po_id)
            WHERE ccl.po_id={$po_id} AND ccl.center_id ={$center_id} ");
            return  $call_date_list;
    }
    public function reassign_apply_data_center_specific($params, $batch_no, $po_id, $calling_list_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
            
            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];
           
           $cmd ="SELECT record_cnt_from, record_cnt_to FROM center_calling_list WHERE po_id =".$po_id." and batch_no = ".$batch_no." AND id={$calling_list_id}";
          
           $between_id = $this->db->fetchRow($cmd);
           
           $min_id = $between_id['record_cnt_from'];
           $max_rec_id = $between_id['record_cnt_to'];
           

           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;
           
           
           
           foreach($params as $row)
           {
               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load

                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 ORDER BY po_id,id LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 ORDER BY po_id,id  LIMIT {$limit} ) AS t";
               }
               
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               $row['assign_record_cnt'] = $limit;
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }
           
           $this->db->query("DELETE FROM center_calling_list WHERE po_id={$po_id} AND batch_no={$batch_no} AND id={$calling_list_id}");
           
           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    
    
    
    
    public function drop_result_per_center_calldate($id)
   {    
        $center_calling= $this->db->fetchRow("SELECT  MIN(record_cnt_from) ,MAX(record_cnt_to)  FROM center_calling_list  ccl
                                             LEFT JOIN call_center cc on ccl.center_id = cc.id
                                             where po_id ={$param['po_id']} AND ccl.center_id={$param['center_id']}");
        
    
       if (! $this->db->delete('po_transaction_data_ans_2'."_{$param['po_id']}", 'po_id=' .  $param['po_id'] . ' AND crid_permanent>= '. $param['po_id'].' AND crid_permanent<='.$param['po_id'])) {
                                return $this->failed("Unable to drop result.");
             }
             $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$param['po_id']}");
             return $this->success("Result has been dropped"); 
   }
    
    public function get_dedup_from_previous_file($batch_no, $po_id)
    {
        $cmd="select status from  po_transaction_data_2_column
        where po_id={$po_id} and batch_no={$batch_no}";
        $duplicate_nos_from_prev = $this->db->fetchRow($cmd);
        return $duplicate_nos_from_prev['status'];
    }
    
    public function reassign_apply_data_center($params, $batch_no, $po_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";

            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];

           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;
           
           
           
           foreach($params as $row)
           {
               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load

                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ORDER BY po_id,id LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ORDER BY po_id,id  LIMIT {$limit} ) AS t";
               }
               
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               $row['assign_record_cnt'] = $limit;
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }
           
           $this->db->query("DELETE FROM center_calling_list WHERE po_id={$po_id} AND batch_no={$batch_no}");
           
           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    public function get_center_for_upload_result($po_id)
    {
        $cmd = "SELECT DISTINCT cc.id,cc.name FROM center_calling_list  ccl
        LEFT JOIN call_center cc on ccl.center_id = cc.id
        where po_id ={$po_id}"  ;
        return $this->db->fetchPairs($cmd);    
    }
    
    public function get_upload_batch_list($po_id)
    {
        // $cmd = "SELECT DISTINCT ccl.po_id,ccl.batch_no, SUM((record_cnt_to+1)-record_cnt_from) as records, pdt.create_date  
        // FROM center_calling_list ccl
        // INNER JOIN po_transaction_data_2_column pdt
        // ON pdt.po_id = ccl.po_id AND ccl.batch_no=pdt.batch_no 
        // WHERE ccl.po_id = {$po_id}
        // GROUP BY ccl.po_id,ccl.batch_no";  
        
        $cmd = "SELECT DISTINCT pdt.po_id,pdt.batch_no, 0, pdt.create_date 
        FROM  po_transaction_data_2_column pdt
        WHERE pdt.po_id = {$po_id}
        GROUP BY pdt.po_id,pdt.batch_no";  
        
        return $this->db->fetchAll($cmd);
        
    }
    
    
    public function get_uploaded_summary_count($batch_no, $po_id)
    {
        $cmd="SELECT * FROM po_transaction_data_2_summary WHERE batch_no={$batch_no} AND po_id={$po_id}";

        return $this->db->fetchRow($cmd);
    }
    
    
    //process_option_id, po_id, batch_no, householded_count,
    //badphone_count,total_count,final_count,dedup_count
    public function insert_summary($params)
    {
       $this->db->insert('po_transaction_data_2_summary', $params);
    }
    
    public function apply_data_center_2($params, $batch_no, $po_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";

            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];
           

           
           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;   
           
           foreach($params as $row)
           {

               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
//                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
//                    $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                    //$cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";

//                    $cmd = "  SELECT DISTINCT phone_no, min(id) min_id, max(id) as max_id FROM  (select id,phone_no from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ) AS t  LIMIT {$limit}";
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";


               }
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               //new field
                if($process_id==1){
                    $row['assign_record_cnt'] = $limit-1;
                }else{
                    $row['assign_record_cnt'] = $limit;
                }
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }

           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    
    
    
        public function get_client_code($id)
    {
        $cmd="select client_code from client where id={$id}";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['client_code']))
        {
            return $result['client_code'];    
        }else{
            return "";   
        }
    }
       public function get_po_counter($client_id)
    {
        $cmd="SELECT value FROM po_counter WHERE client_id =".$client_id;
        $result = $this->db->fetchRow($cmd); 
         if(is_array($result)){
             return $result['value'];
         }else{
            
                $insert_data=array(
                    'value' => 2,
                    'client_id'  =>$client_id, 
                );
                $this->db->insert('po_counter', $insert_data);
                return 1;
         }
        
    }
    public function update_po_counter($params)
     {
          $val = ($params['value'] + 1);
          $update_data = array(
            'value' => $val,
          );
         if(!$this->db->update('po_counter', $update_data,'client_id=' . $params['client_id']))   
         {return false;} 
          else 
         {return true;}              
    }
    
        public function get_po_number($id){
          $sql="select po_no from po p where id = ".$id;
          $result = $this->db->fetchRow($sql);
          return $result['po_no']; 
  
    }
 
     //this function will verify if previous batch has not been assigned of a center
     // if this function has result it means that previous batch had assign a center
    public function has_existing_batch_without_applied_center($batch_no, $po_id)
    {
        
        
        $cmd= "SELECT id from center_calling_list where batch_no={$batch_no}
AND po_id={$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
                                            
        if(!($result['id']==""))
        {
            return 0;    
        }else{
            return 1;
//            $cmd="SELECT id from po_transaction_data_2 where  batch_no={$batch_no}
//AND po_id={$po_id} LIMIT 1";

//            $result = $this->db->fetchRow($cmd);
//              if(isset($result['id']))
//              {
//                return 1;    
//              }else{
//                return 0;
//            }
        }
    }
    //batch_no, po_id
    public function get_existing_process_option_id_remarks($batch_no, $po_id)
    {
        $cmd = "SELECT process_option_id, remarks, misc_fields, lname_field, fname_field, phone_field from po_transaction_data_2_column where batch_no={$batch_no}
AND po_id={$po_id} AND is_column=1 LIMIT 1";

         $result = $this->db->fetchRow($cmd);
         return $result;
    }
    
    public function get_total_count($batch_no, $po_id)
    {
        $cmd = "SELECT count(id) as cnt from po_transaction_data_2_{$po_id} where batch_no={$batch_no}
AND po_id={$po_id} AND is_column=0";
         $result = $this->db->fetchRow($cmd);
         return $result['cnt'];
    }
    
 


   public function drop_result($params)
   {    
       
       if($params['center_id']==0){ 
         
            /*   if (! $this->db->delete('po_transaction_data_ans_2'."_{$params['id']}", 'po_id=' .  $params['id'])) {
                                        return $this->failed("Unable to drop result.");
                     } */
            $cmd_delete= "TRUNCATE TABLE po_transaction_data_ans_2_{$params['id']}";
            $this->db->query($cmd_delete);       
            $this->db->delete('po_transaction_data_ans_2_column', 'po_id=' .  $params['id']);
            $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$params['id']}");   
        /*            $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id);
                    $this->db->delete('po_transaction_data_2_summary','po_id=' .  $id);  */
               return $this->success("Result has been dropped"); 
       }else{
         
            $call_date = new DateTime($params['call_date']);
            $call_date = $call_date->format('d/m/Y');
            $center_calling= $this->db->fetchRow("SELECT  MIN(record_cnt_from) as min ,MAX(record_cnt_to) as max  FROM center_calling_list  ccl
                                             LEFT JOIN call_center cc on ccl.center_id = cc.id
                                             where po_id ={$params['id']} AND ccl.center_id={$params['center_id']}");
             
            if (! $this->db->delete('po_transaction_data_ans_2'."_{$params['id']}", 'po_id=' .  $params['id'] .' AND call_date='. $this->db->db_escape($call_date). ' AND crid_permanent>= '. $center_calling['min'].' AND crid_permanent<='.$center_calling['max'])) {
                                return $this->failed("Unable to drop result.");
             } 
             
             $cmd= "SELECT id from po_transaction_data_ans_2_{$params['id']} where  po_id={$params['id']} LIMIT 1"; 
             $result = $this->db->fetchRow($cmd);
             
            if($result['id']=="")
            {
                $this->db->delete('po_transaction_data_ans_2_column', 'po_id=' . $params['id']);
            }
            $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$params['id']}");
            return $this->success("Result has been dropped"); 
           
           
       }
   }
    
 

    private function validate_response($q_columns, $data, $responses)
    {
             $index = 0;
             while ($index < count($q_columns))
             {
                         
                      
                  $question_reponse = $data[$q_columns[$index]]; //responsse from file
              
                  if(!array_key_exists($question_reponse,$responses[$index]))
                  {
                     
                      return false;
                  }
                          
                  $goto_next_q =  $responses[$index][$question_reponse];   
                  $goto_next_q--;
                    
               
                  if($goto_next_q == $index)   //check if some question is skip because of goto
                  {
                     
                  }else{
                 
                      //loop on previous columns   
                      $loop_cnt_previous = $index+1;
                      while ($loop_cnt_previous < $goto_next_q)
                      {
                          $question_reponse = $data[$q_columns[$loop_cnt_previous]]; //responsse from file
                      
                          if($question_reponse>0){
                             if(!array_key_exists($question_reponse,$responses[$index]))
                             {
                               return false;
                             }
                           }
                          if($question_reponse>0 || $question_reponse==""){ //Check if the previous Question had a value
                               return false;
                          }
                        
                          $loop_cnt_previous++;
                      }
                  }
                  
                 
                  $index = $goto_next_q;
                 
                  
                  //fo
                 
             }
            
             return true;
    }
   
    public function drop_manual_load($id)
    {
             if (! $this->db->delete('po_manual_load', 'po_id=' .  $id)) {
                                return $this->failed("Unable to drop manual load.");
             }    
            return $this->success("Manual load has been dropped");
    }
    private function get_center_id($center_code)
    {
        $cmd="select id from call_center where center_code='{$center_code}'";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['id']))
        {
            return $result['id'];    
        }else{
            return -1;   
        }
    }
    
    private function is_center_assign_on_po($center_id, $po_id)
    {        
        $cmd="select id from center_calling_list where po_id={$po_id} AND center_id={$center_id}";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['id']))
        {
            return $result['id'];    
        }else{
            return -1;   
        }
        
    }
    
    public function process_manual_file($file_name, $po_id)
    {
        ini_set('max_execution_time', 0);
        $file = $file_name;
        $handle = fopen($file, "r");
        $line_number = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                
                // process the line read.
                    if(trim($line) !="")
                    {
                        $line_number++;
                        $row = explode(",",$line);
                        if(!(isset($row[0],$row[1],$row[2], $row[3], $row[4], $row[5])))
                        {
                          
                          return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}.");  
                          if($line_number>1)
                            {
                                unlink(md5($po_id.'_'.$center_id).'.txt');        
                            }
                          die();
                        }
                        elseif(is_numeric($row[3] && is_numeric($row[4] && is_numeric($row[5]))))
                        {
                          return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Question or Reponse or No. of Count");  
                          if($line_number>1)
                            {
                                unlink(md5($po_id.'_'.$center_id).'.txt');        
                            }
                          die();  
                        }
                        else{
                            $center_code = $row[0];
                            $call_date = $row[1];
                            $call_date = new DateTime($call_date);
                            
                            $call_date = $call_date->format('Y-m-d');
                            $status_code = $row[2];    
                            $question_no = $row[3];    
                            $response = $row[4];    
                            $no_of_counts = $row[5];    
                            
                            //validate if center code is valid 
                            $center_id = $this->get_center_id($center_code);
                            if($center_id==-1)
                            {
                                return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Invalid center code.".$center_id);  
                                if($line_number>1)
                                {
                                    unlink(md5($po_id.'_'.$center_id).'.txt');        
                                }
                                die();  
                            }
                            //check if center is on the center_calling_list table
                            if($this->is_center_assign_on_po($center_id, $po_id)==-1)
                            {
                                return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Center Code is not assign on this PO.");  
                                if($line_number>1)
                                {
                                    unlink(md5($po_id.'_'.$center_id).'.txt');        
                                }
                                die();  
                            }                   
                            $script = "({$po_id}, {$center_id}, '{$call_date}', '{$status_code}', {$question_no}, {$response}, {$no_of_counts}, '{$_SESSION['username']}'),";
                        }
                        
                        //file_put_contents(md5($po_id.'_'.$center_id).'.txt', $script , FILE_APPEND);                       
                        file_put_contents(md5($po_id).'.txt', $script , FILE_APPEND);                       
                   }
                
                
            }
           
            //$this->db->query("INSERT INTO po_manual_load (po_id, center_id, call_date, status_code, question, response, no_of_count, create_by) VALUES".substr(file_get_contents(md5($po_id).'.txt'),0, strlen(file_get_contents(md5($po_id.'_'.$center_id).'.txt'))-1));
            
            $this->db->query("INSERT INTO po_manual_load (po_id, center_id, call_date, status_code, question, response, no_of_count, create_by) VALUES".substr(file_get_contents(md5($po_id).'.txt'),0, strlen(file_get_contents(md5($po_id).'.txt'))-1));
              
            unlink(md5($po_id).'.txt');
            fclose($handle);
            return array("success" => true, "message" => "File has been processed.", "no_of_records" => $line_number);
        } else {
            // error opening the file.
            print_r('Error Reading');
        } 
        unlink($file);
    }

    
    
    public function update_final_universe($params)
    {
        $params = array_map('trim', $params);
        $id = $params['po_id'];     
        
        $po_universe = $this->db->fetchRow("select universe from po_parameter where po_id={$id}");
        $values = array(
                            'universe'=>$po_universe['universe']+$params['universe'],
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po_parameter', $values, 'po_id='.$id);
        $response = array(
            'message' => "PO final universe has been updated.",
        );
        return $this->success($response); 
    }
    
    
     public function check_data_if_manual_data_exist($id)
    {
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$id}  LIMIT 1";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    public function check_data_if_result_exist($id)
    {
        //$cmd="select po_id from po_transaction_data_ans_2 where po_id={$id} limit 1";
        $cmd="select po_id from po_transaction_data_ans_2_column where po_id={$id} limit 1";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    public function check_data_if_exist($params)
    {
        //$cmd="select po_id from po_transaction_data_2 where po_id={$params['id']} and is_column=0 limit 1";
        $cmd="select po_id from po_transaction_data_2_column where po_id={$params['id']} limit 1";
        $result = $this->db->fetchAll($cmd);
        return $result;
    }
    public function check_po_status($params)
    {
        $cmd="select status from po where id={$params['id']}";

        $result = $this->db->fetchRow($cmd);
        return $result;
    }
    public function get_project_status()
    {
        $cmd="select code,name from project_status ";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    
    }
    public function change_po_status($params)
    {
       $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>$params['status'],
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po', $values, 'id='.$id);
        $response = array(
            'message' => "PO status has been updated.",
        );
        return $this->success($response); 
    }
    
    public function get_po_listby_id($id)
    {
        if($id>0){
            //$cmd="select id,name from po  where status = 'A' AND client_id=".$id;
             $cmd="select id,name from po  where client_id=".$id;
        }else{
            $cmd="select id,name from po  where status = 'A'";
        }
        
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    }
    
    public function get_po_listby_all_id($id)
    {
       $cmd="select id,name from po  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    } 
    
     public function get_po_template_listby_all_id($id)
    {
        $cmd="select id,name from project_template  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    
    public function get_po_template_listby_id($id)
    {
        $cmd="select id,name from project_template  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
 

  public function get_total_dedup($param){
       // $cmd="select count(id) cnt from po_transaction_data_2 where phone_no in(
            // select phone_no from  po_transaction_data_2
            // where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1
            // order by phone_no 
          // )";
           $cmd1="select phone_no from  po_transaction_data_2_{$param['po_id']}
            where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
            //order by phone_no";
   //print_r($cmd1);
          
    //$rec2 = $this->db->fetchAll($cmd1);
    
          $dedup_phone = $this->db->fetchAll($cmd1); 
    $dedup_cnt = count($dedup_phone);
          
//    $phone_nos = "";
//    foreach($dedup_phone as $r)
//    {
//   $phone_nos.="'{$r['phone_no']}',";
//    }
//    
// 
//    $phone_nos = substr($phone_nos, 0, strlen($phone_nos)-1); 
//    $phone_nos= str_replace(",", " OR phone_no=",$phone_nos);

    
    //$cmd = "select count(id) cnt from po_transaction_data_2 where phone_no in({$phone_nos}) AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0";
//    $cmd = "select count(id) cnt from po_transaction_data_2 where (phone_no = {$phone_nos}) AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0";
//    print_r($cmd);
//    die();

        // $cmd ="SELECT count(b.phone_no) cnt" ;
        // $cmd .= " FROM ";
        // $cmd .= " po_transaction_data_2 b, "; 
        // $cmd .= " (select phone_no, count(1) as rec from  po_transaction_data_2 where po_id={$param['po_id']} and ";
        // $cmd .= " batch_no={$param['batch_no']} AND is_column=0  GROUP BY phone_no) AS a ";
        // $cmd .= " WHERE b.phone_no=a.phone_no ";
        // $cmd .= " AND a.rec>1 AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0  ";
        $cmd= "SELECT SUM(nos) AS cnt FROM ( SELECT count(phone_no) as nos FROM po_transaction_data_2_{$param['po_id']} where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no) > 1 ) AS t";

    $duplicate_nos = $this->db->fetchRow($cmd);
    
    
    $duplicat_nos_cnt = $duplicate_nos['cnt'];
    
          //$rec2 = $this->db->fetchAll($cmd1);                
          //$val1 = $rec1['cnt'];
          // $val2 = count($rec2);
          //return ($val1-$val2);
          $res = ($duplicat_nos_cnt - $dedup_cnt);
          
    return $res;
                                                                        
   }
 

    
   public function get_poname($id)
   {
       $cmd="select name from po where  id =".$id;
        $result = $this->db->fetchRow($cmd);                
        return $result['name'];
       
   } 
   public function get_item_for_billing($params)
  {
      
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE =". $params['po_id']);
        $no_of_q = $row_no_of_question['number_of_question'];

        $q_fields="";
        $q_fields_empty="";
        
        for($x =1; $x<=$no_of_q; $x++)
        {
            $q_fields.="'Q{$x}'," ; 
            $q_fields_empty.="''," ; 
        }
       
        $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
            print_r($q_fields);
            exit;
        $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
        
       $cmd = "select * from po_transaction_data_ans_2 where po_id = 1";
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
  } 
    
    public function paid_billing($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'P',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('billing_header', $values, 'id='.$id);
        $response = array(
            'message' => "Billing has been paid.",
        );
        return $this->success($response);
    }
    
    public function get_billing_details($param)
    {
        $cmd="select c.street,c.city,c.state, c.id as client_id,bh.remarks,bh.total_amount, CASE bh.status WHEN 'S' THEN 'SAVED' WHEN 'P' THEN 'PAID'  END as status,bh.create_date,bh.create_by,bh.invoice_no,
        bh.billing_date,bh.due_date,c.name as client_name,bd.*
        FROM  billing_header bh
        LEFT JOIN billing_detail bd on bh.id = bd.billing_header_id  
        LEFT JOIN client c on c.id=bh.client_id 
        where bh.id=".$this->db->db_escape( $param['id']);
      
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    } 
    public function get_billing_list()
    {
        $cmd="select bh.id,bh.total_amount as amount_due, CASE bh.status WHEN 'S' THEN 'SAVED' WHEN 'P' THEN 'PAID'  END as status,bh.create_date,bh.create_by,bh.invoice_no,
        bh.billing_date,bh.due_date,c.name as client_name
        FROM  billing_header bh  
        LEFT JOIN client c on c.id=bh.client_id 
        where c.status = 'A'";
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    } 
   public function save_billing($params)
   {
     
        if($params['txtID']==""){
            $invoice =$this->get_counter('billing');
            $insert_data = array(
                'client_id'     =>$params['client_name'],
                'invoice_no'     =>'INV# '.sprintf('%06d',$invoice),
                'billing_date'  =>$params['txtDueDate'], 
                'due_date'      =>$params['txtDueDate'], 
                'total_amount'  =>str_replace(',','',$params['txtTotal_amount']), 
                'status'        =>'S', 
                'remarks'       =>$params['txtRemarks'], 
                'create_by'     =>$_SESSION['username'], 
            );                     
             
            if (!$this->db->insert('billing_header', $insert_data)) {
                return $this->failed("Unable to save billing transaction.");
            } else {
                    $bill_id = $this->db->lastInsertId();
                    $row = explode(";",$params['txtRowValues']);
                    foreach($row as $res_row) 
                    { $temp[] = explode("|",$res_row); }
                    foreach($temp as $val) 
                    {                 
                             $insert_billing_details[] = array(     
                                'billing_header_id'=>$this->db->lastInsertId(), 
                                'line_number'=>$val[0],  
                                'description'=>$val[1],  
                                'quantity'=>$val[2],  
                                'rate'=>$val[3],  
                                'amount'=>str_replace(',','',$val[4]), 
                                 );
                            
                    }    
                
                        if (!$this->db->batchInsert('billing_detail', $insert_billing_details)) {
                            $this->db->delete('billing_header', 'id=' .  $this->db->lastInsertId());
                            return $this->failed("Unable to save billing transaction.");
                        } 
                            $params= array(
                                'name'=>'billing',
                                'value'=> $invoice,
                            );
                         $this->update_counter($params);   
                         return $this->response = array(
                                'doc_id'  =>  $bill_id,
                                'invoice' =>'INV# '.sprintf('%06d',$invoice),
                                'ResponseCode' => '0000',
                                'ResponseMessage' =>  'New billing transaction has been saved.',
                                ); 
            } 
        } else{

            $update_data = array(
                'client_id'     =>$params['client_name'],
                'billing_date'  =>$params['txtDueDate'], 
                'due_date'      =>$params['txtDueDate'], 
                'status'        =>'S', 
                'total_amount'  =>str_replace(',','',$params['txtTotal_amount']), 
                'remarks'       =>$params['txtRemarks'], 
                'update_date'=>date('Y-m-d H:i:s'), 
                'update_by'     =>$_SESSION['username'], 
            );                     
             
            if (!$this->db->update('billing_header', $update_data,'id=' . $params['txtID'])) {
                return $this->failed("Unable to update billing transaction.");
            } else {
                    $row = explode(";",$params['txtRowValues']);
                    foreach($row as $res_row) 
                    { $temp[] = explode("|",$res_row); }
                    foreach($temp as $val) 
                    {                 
                             $insert_billing_details[] = array(     
                                'billing_header_id'=>$params['txtID'], 
                                'line_number'=>$val[0],  
                                'description'=>$val[1],  
                                'quantity'=>$val[2],  
                                'rate'=>$val[3],  
                                'amount'=>$val[4], 
                                 );
                            
                    }    
                        $this->db->delete('billing_detail', 'billing_header_id=' .  $params['txtID']);
                        if (!$this->db->batchInsert('billing_detail', $insert_billing_details)) {
                            return $this->failed("Unable to save billing transaction.");
                        } 
                         return $this->response = array(
                                'doc_id'  =>  $params['txtID'],
                                'invoice' =>0,
                                'ResponseCode' => '0000',
                                'ResponseMessage' =>  'Billing Transaction has been updated.',
                                );  
            } 
            
            
            
            
           
        }
    }      
        
    public function get_counter($name)
    {
        $cmd="SELECT value FROM counter WHERE name =".$this->db->db_escape($name); 
        $result = $this->db->fetchRow($cmd); 
         if(is_array($result)){
             return $result['value'];
         }else{
                $insert_data=array(
                    'value' => 2,
                    'name'  =>$name, 
                );
                $this->db->insert('counter', $insert_data);
                return 1;
         }
        
    }
    public function update_counter($params)
     {
          $val = ($params['value'] + 1);
          $update_data = array(
            'value' => $val,
          );
         if(!$this->db->update('counter', $update_data,'name=' . $this->db->db_escape( $params['name'])))   
         {return false;} 
          else 
         {return true;}              
    }
   
 
  public function apply_data_center($params){

        $divisor = 10000;
        foreach($params as $row)
        {
           $center_calling_list_insert[] =  $row;
           
           //LC-01/04/2016 - will set no of records variable no_of_record;
          /* if(isset($row['no_of_record']))
           {*/
             $limit =  $row['record_cnt_to'];
          /* }else{
             $limit =  ($row['record_cnt_to'] - $row['record_cnt_from'])+1;
           }*/
           //PP 01/11/2016 - identify the # of list
           $list_cnt = $row['list_cnt'];
           $po_id =$row['po_id']; 
           $batch_no =$row['batch_no'];
           $center_id = $row['center_id'];
          
          /* //print_r($limit%3000); //get remainder
          //die();
           // $page = (int) ($limit/3000);
           
           
            // for($x=1; $x<=$page; $x++)
            // {
                // $cmd = "UPDATE po_transaction_data_2 SET center_id={$center_id}
                    // WHERE id IN (
                    // SELECT id FROM (
                    // SELECT id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
                    // AND center_id is null LIMIT {$limit}
                // ) AS c)";
                // $this->db->query($cmd);
            // }
           
           // $remainder = limit%3000;           
           // $cmd = "UPDATE po_transaction_data_2 SET center_id={$center_id}
                    // WHERE id IN (
                    // SELECT id FROM (
                    // SELECT id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
                    // AND center_id is null LIMIT {$remainder}
            // ) AS c)";
            // $this->db->query($cmd);
           
           
           //die();
           

            //$this->db->query("SET SESSION group_concat_max_len = 100000");
            //$res = $this->db->fetchRow("SELECT GROUP_CONCAT(id) AS id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
            //AND center_id is null LIMIT {$limit}");
            
            

            
            //print_r("SELECT GROUP_CONCAT(id) AS id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
            //AND center_id is null LIMIT {$limit}");

            
            //$cmd= "UPDATE po_transaction_data_2 SET center_id={$center_id}
            //WHERE id IN ({$res['id']}) AND po_id={$po_id}";
            

            
            //$this->db->query($cmd);     */
			
			//LEMS for testing
		   $cmd ="SELECT process_option_id FROM po_transaction_data_2_{$row['po_id']} WHERE po_id =".$row['po_id']." and center_id is null and batch_no = ".$row['batch_no']." AND is_column=1";

		   $process_id = $this->db->fetchRow($cmd);
      
           
		   $process_id = $process_id['process_option_id'];
		   
           $looper = floor($limit/$divisor);
           
           $remainder =$limit%$divisor;
           
           
           for($cnt=1; $cnt<=$looper; $cnt++)
           {
                 $limit = $divisor;
		         if($process_id==1){ //Non-Household List AS-IS
                         //AND is_column=0 AND  LENGTH(TRIM(phone_no))<>10";
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                        
                 }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
                 
//                        $cmd = " select id from po_transaction_data_2 where   po_id =". $row['po_id'];
//                        $cmd .=" and batch_no=".$row['batch_no'];
//                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
//                        $cmd .=" UNION ALL";
//                        $cmd .=" select id from po_transaction_data_2 where   po_id =". $row['po_id'];
//                        $cmd .=" and batch_no=".$row['batch_no'];
//                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
//                        $cmd .=" LIMIT {$limit}";


                       
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                             
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";  
                        
                 }elseif($process_id==3 ){ //Load then Household list  
                        /*$params=array(
                            'po_id'=> $row['po_id'],
                            'batch_no'=>$row['batch_no'],
                        );
                        $dedup_cnt=$this->get_total_dedup($params);
                        $limit = ($limit+$dedup_cnt);                */
                        $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                        $cmd .=" LIMIT {$limit}";
                           //caught these variables
                
                        
                            
                        /*$cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";*/
                       /* $cmd .=" UNION ALL";
                        $cmd .=" select id from po_transaction_data_2 where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
                        $cmd .=" LIMIT {$limit}"; */
                         
                           
                       /* $cmd = " select id from po_transaction_data_2 where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}"; */  
                     
                       //" from po_transaction_data_2 where   po_id =". $params['po_id'];
                        //$cmd .=" and batch_no=".$params['batch_no'];
                        //$cmd .=" and center_id=".$params['center_id']. " AND  LENGTH(TRIM(phone_no))<>10 AND is_column=0 GROUP BY phone_no ";
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";
		           }
		           else{ //4 Already-Householded List Load
		           
		           }
		       

               $ids = "";
             //  $cmd = "select  id from po_transaction_data_2 where   po_id =".$row['po_id']." and center_id is null and batch_no = ".$row['batch_no']." AND is_column=0 LIMIT ".$row['record_cnt_to'];             
               $result = $this->db->fetchAll($cmd);
               
           /*    foreach ($result as $r) {
                     $ids= $r['id'].",";
                     file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
               }  */
              
               $myfile = fopen(md5($row['po_id'].'_'.$row['batch_no']).'.txt', "a");
               foreach ($result as $r) {
                     $ids= $r['id'].",";
                     //file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
                     fwrite($myfile, $ids);
                     /* //$update_data = array(
                        //'center_id' => $row['center_id'],
                        //);
                     //$this->db->update('po_transaction_data_2', $update_data,'id=' . $r['id']);        */
               }
               fclose($myfile);
               
               $this->db->query("UPDATE po_transaction_data_2_{$row['po_id']} SET list_cnt=".$list_cnt.", center_id=".$row['center_id']." WHERE  po_id = {$row['po_id']} and  id 
                            IN(".
                                substr(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'), 0, strlen(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'))-1)
                            .") AND batch_no={$row['batch_no']}");
               //unlink(md5($row['po_id'].'_'.$row['batch_no']).'.txt');     
           }      
           
           
           if($remainder>0)
           {
                 $limit = $remainder;
                 if($process_id==1){ //Non-Household List AS-IS
                         //AND is_column=0 AND  LENGTH(TRIM(phone_no))<>10";
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                        
                 }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
                 
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
                        $cmd .=" UNION ALL";
                        $cmd .=" select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
                        $cmd .=" LIMIT {$limit}";
                             
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";  
                        
                 }elseif($process_id==3 ){ //Load then Household list  
                        /*$params=array(
                            'po_id'=> $row['po_id'],
                            'batch_no'=>$row['batch_no'],
                        );
                        $dedup_cnt=$this->get_total_dedup($params);
                        $limit = ($limit+$dedup_cnt);                */
                        $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                        $cmd .=" LIMIT {$limit}";
                   }
               

               $ids = "";
                   
               $result = $this->db->fetchAll($cmd);
               
           /*    foreach ($result as $r) {
                     $ids= $r['id'].",";
                     file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
               }  */
              
               $myfile = fopen(md5($row['po_id'].'_'.$row['batch_no']).'.txt', "a");
               foreach ($result as $r) {
                     $ids= $r['id'].",";
                     //file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
                     fwrite($myfile, $ids);
                     /* //$update_data = array(
                        //'center_id' => $row['center_id'],
                        //);
                     //$this->db->update('po_transaction_data_2', $update_data,'id=' . $r['id']);        */
               }
               fclose($myfile);
               
               $this->db->query("UPDATE po_transaction_data_2_{$row['po_id']} SET list_cnt=".$list_cnt.", center_id=".$row['center_id']." WHERE  po_id = {$row['po_id']} and  id 
                            IN(".
                                substr(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'), 0, strlen(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'))-1)
                            .") AND batch_no={$row['batch_no']}");
               unlink(md5($row['po_id'].'_'.$row['batch_no']).'.txt');   
           } 
                   
            
        }
         
        
        if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
            return $this->failed("Unable to insert center calling list data");
        } 
       else {
              $value=array(
                            'id'    =>$po_id,
                            'status'=> 'A',
                            );
                            $this->change_po_status($value);
           
            $response = array(
                'message' => "Center data has been populated.",
            );                                                              
            return $this->success($response);
        }         
    }
   
  
    public function get_databack_2($params)
         {
            $dir = (dirname(dirname(__DIR__)));
            $dir = str_replace('\\', "/", $dir);
            $cmd = "select po_no from po where id ={$params['po_id']}";
            $rs = $this->db->fetchRow($cmd);
            $file_name =$rs["po_no"]."_DataBack_".time().".csv";
            $dir_file_name = $dir."/public/{$file_name}";
          

            
            $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);
            $no_of_q = $row_no_of_question['number_of_question'];
            $q_fields="";
            $q_fields_empty="";
            
            for($x =1; $x<=$no_of_q; $x++)
            {
                $q_fields.="'Q{$x}'," ; 
                $q_fields_empty.="''," ; 
            }
            
            $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
            $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
            
            
            
            //databack column
            $get_data_ans_col = "select remarks from po_transaction_data_ans_2_column where is_column =1 and  po_id =". $params['po_id'] ." limit 1";
            

            $row_data_ans_col = $this->db->fetchRow($get_data_ans_col);   
            $data_ans_col = explode(";",($row_data_ans_col['remarks']),-1);
            $ans_col_db ="";
            $temp_fields="";
            $phone_col="";
            $customer_response_map = array();
            $arr_response_map = array(); 
            //Get First name column
             $first_name_col = $this->db->fetchRow("SELECT fname_field FROM po_transaction_data_2_column  WHERE po_id = ".$params['po_id'] ." AND is_column=1 LIMIT 1");
             //Get First name column
            $proc_opt = $this->db->fetchRow("SELECT process_option_id FROM po_transaction_data_2_column  WHERE po_id = ".$params['po_id'] ." AND is_column=1 LIMIT 1");
            //Get Custom Status Code
            $custom_status_code = $this->db->fetchRow("select custom_status from po_management  WHERE po_id = ".$params['po_id']);
            //Get Customer Response Map
            $response_map = $this->db->fetchRow("select customer_response_map from po_management  WHERE po_id = ".$params['po_id']);
            if($response_map['customer_response_map']!=""){
               $customer_response_map = explode(",",$response_map['customer_response_map']); 
            } 
       
            $status_code_position=0;   
            foreach($data_ans_col as $r)
            {
                $column_name = explode("|",$r);  
                
                if(trim($column_name[1])=='spoke_to'||$column_name[1]=='call_date'||$column_name[1]=='call_time'||$column_name[1]=='duration'||
                    $column_name[1]=='TSR'||$column_name[1]=='status_code'||$column_name[1]=='remarks'
                    ||$column_name[1]=='cap1'||$column_name[1]=='cap2'||$column_name[1]=='cap3'||$column_name[1]=='cap4'){
                    $col1 = $column_name[0];
                    $col = $column_name[1];
                    if(trim($column_name[1])=='spoke_to'){
                        $fname = "tpda.{$col1}";
                        $ans_col_db.="'{$col}' AS {$col}Ans,";
                        if($proc_opt['process_option_id']==4){
                            $temp_fields .=  " tpda.{$col1} AS spoke_to, ";         
                        }else{
                            $temp_fields .=  "CASE WHEN tpda.{$col1}>=1  THEN ptd.{$first_name_col['fname_field']} ELSE '' END AS spoke_to,";         
                        }
                        
                        
                    }else{ 
                        //For custom Status code
                        if(trim($column_name[1])=='status_code'){
                            $status_code_position = "{$col1}tpdaa";
                          }  
                        $ans_col_db.="'{$col}' AS {$col}Ans,";      
                        $temp_fields .= "tpda.{$col1} as {$col1}tpdaa ,"; 
                    }
                }
             
                 if(trim($column_name[1])=='phone'){
                        $phone_col = $column_name[0];    
                    }
                 
                for($x =1; $x<=$no_of_q; $x++){
                    $is_has_custom_response = 0;
                    if(trim($column_name[1])=="Q".$x){
                        $col1 = $column_name[0];
                        $col = $column_name[1];
                        $ans_col_db.="'{$col}',";
                        //Response Map
                        $str_case = "";
                        
                         foreach($customer_response_map as $res){
                                 $res1= explode(':',$res);
                                 if($res1[0]==$x){
                                    $str_case="CASE tpda.{$col1} "; 
                                 }                     
                         }
                        
                        
                        if(count($customer_response_map)>0){
                            
                           $temp_fields.=  $str_case; 
                             foreach($customer_response_map as $res){
                                 
                                 $res1= explode(':',$res);
                                 if($res1[0]==$x){
                                    $is_has_custom_response = 1;
                                    $res2= explode('=',$res1[1]);
                                    $temp_fields.="WHEN {$res2[0]} THEN '{$res2[1]}' ";   
                                 }
                                 
                             }
                                if($is_has_custom_response==1){
                                    $temp_fields.=  "END AS {$col1}Ans, "; 
                                }else{
                                   $temp_fields .=  "tpda.{$col1} AS {$col1}Ans," ; 
                                }
                        }else{
                             $temp_fields .=  "tpda.{$col1} AS {$col1}Ans," ;            
                        }   
                     }
                    
                }           
            }
            
           
             $temp_fields =substr($temp_fields, 0, strlen($temp_fields)-1); 
             $ans_col_db = substr($ans_col_db, 0, strlen($ans_col_db)-1);
          
             if(!(isset($row_data_ans_col['remarks'])))
             {
                    $ans_col_db ="'spoke_to','call_date','call_time','duration','TSR','status_code',";
                    $temp_fields = "'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'' status_code,";
                    for($x =1; $x<=$no_of_q; $x++){
                        $ans_col_db.="'Q{$x}',";       
                        $temp_fields.="'' Q{$x},";       
                    }
                    $ans_col_db = substr($ans_col_db, 0, strlen($ans_col_db)-1); 
             }
              
             
             $temp_fields  = substr($temp_fields, 0, strlen($temp_fields)-1);
              
           /*   print_r($temp_fields);
              die(); */
             
            $get_column_cmd="select  col_fields,misc_fields, remarks from po_transaction_data_2_column where is_column =1 and  po_id =". $params['po_id'] ." limit 1";
            $row = $this->db->fetchRow($get_column_cmd);
                     
            
            //Col
            $col_fields = explode("|",unserialize($row['col_fields']));
           
            $col_fields_db ="";     
            foreach($col_fields as $r)
            {
                $col_fields_db.="ptd.{$r}," ;   

            }
            $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);

             //Misc
             $misc_field="";
            if($row['misc_fields']!="")
            {
                $misc_field = unserialize($row['misc_fields']);
            }
            $other_fields ="";           
            $other_fields_rows="";
    //        foreach($misc_field as $r)
    //        {
    //            $column_name = explode("|",$r);
    //            $col = $column_name[1];
    //            $other_fields.="'ptd.{$col}'," ;    
    //            $other_fields_rows.="ptd.{$col} AS {$col}tpda," ;    
    //        }
    //        
    //        $other_fields = substr($other_fields, 0, strlen($other_fields)-1);
    //        $other_fields_rows = substr($other_fields_rows, 0, strlen($other_fields_rows)-1);
            
             
           /*  //Remarks
            $remarks = unserialize($row['remarks']);
            $remarks = explode("|",$remarks);
            $remarks_field ="";
            $remarks_data_field = "";
            foreach($remarks as $r)
            {
                $col = $r;
                
                foreach($data_ans_col as $r1){
                    
                    $column_name = explode("|",$r1);  
                
                     if(trim($column_name[1])==trim($col))  {
                          $remarks_data_field.="tpda.{$column_name[0]}," ;
                      
                     }
                   
                 }
                 
                 if(!(isset($row_data_ans_col['remarks'])))
                 {
                    $remarks_data_field .="'',";
                 }
     
                
                $remarks_field.="'{$col}'," ;
                //$remarks_data_field.="''," ;
            } */
    //        Array ( [0] => col1|crID [1] => col2|phone [2] => col3|last_name [3] => col4|first_name [4] => col5|Firstname [5] => col6|Midname [6] => col7|Lastname [7] => col8|State [8] => col9|Email [9] => col10|misc [10] => col11|misc2 [11] => col12|spoke_to [12] => col13|call_date [13] => col14|call_time [14] => col15|duration [15] => col16|TSR [16] => col17|status_code [17] => col18|Q1 [18] => col19|Q2 [19] => col20|Q3 ) 
            
            
    //        print_r($remarks);
    //        die();
            
    //        print_r(count($remarks));
    //        die();
       /*     $remarks_field = substr($remarks_field, 0, strlen($remarks_field)-1);   
            $remarks_data_field = substr($remarks_data_field, 0, strlen($remarks_data_field)-1);  */
            
            $other_fields = str_replace("'", "", $other_fields);
           /* if(trim($remarks_field!=""))
            {
                $cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec',{$remarks_field},'phone_clean',{$ans_col_db} from po_transaction_data_2 ptd";     
            }else{
                $cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2 ptd"; 
            } */
             
              
            //$cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2_column ptd"; 
            $cmd="select {$col_fields_db},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2_column ptd"; 
            $cmd .=" where po_id={$params['po_id']} and is_column=1 limit 1";     
              
               
            $res = $this->db->fetchRow($cmd);
            
           $fp = fopen('php://output', 'w');
            
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
            header('Pragma: no-cache');
            header('Expires: 0');
            
            fputcsv($fp, array_values($res)); 
                      
             //$cmd UNION ALL"; 
           /* if(trim($remarks_field!=""))
            {
              $cmd .=" select {$col_fields_db},{$other_fields},ptd.center_id,'Y',{$remarks_data_field},ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            }else{
                $cmd .=" select {$col_fields_db},{$other_fields},ptd.center_id,'Y',ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            }  */    
    //        $cmd =" select {$col_fields_db},{$other_fields_rows},ptd.center_id, CASE WHEN LENGTH(TRIM(ptd.phone_no))=10 THEN 'Y' ELSE 'N'  END as master_rec,ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            $cmd =" select {$col_fields_db},ptd.center_id, CASE WHEN LENGTH(TRIM(ptd.phone_no))=10 THEN 'Y' ELSE 'N'  END as master_rec,ptd.phone_no,{$temp_fields}  from  po_transaction_data_2_{$params['po_id']} ptd";
            
                                                                                   
            //$cmd .=" INNER JOIN   po_transaction_data_ans_2 tpda on tpda.crid_permanent = ptd.id where tpda.po_id={$params['po_id']}";    
            /*$cmd .=" LEFT JOIN   po_transaction_data_ans_2 tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id where  ptd.po_id={$params['po_id']} AND ptd.is_column=0 AND ptd.center_id>=1"; */   
            if($phone_col!=""){
                $cmd .=" LEFT JOIN   po_transaction_data_ans_2_{$params['po_id']} tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id AND ptd.phone_no  = tpda.{$phone_col} where  ptd.po_id={$params['po_id']} AND ptd.is_column=0";    
            }else{
                $cmd .=" LEFT JOIN   po_transaction_data_ans_2_{$params['po_id']} tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id  where  ptd.po_id={$params['po_id']} AND ptd.is_column=0";    
            }
         
            
            $status_codes = $this->get_status_codes($params['po_id']);
            $status_codes_callable="";
            foreach($status_codes as $s)
            {
                $status = $s['status_code'];
                foreach($_GET['coded_types'] as $c)
                {
                    if($s['status_type'] == $c)
                    {
                        $status_codes_callable .= "'{$status}',";        
                    }
                }
            }
            if($status_codes_callable != "") {
                $status_codes_callable = substr($status_codes_callable, 0, strlen($status_codes_callable)-1); 
            }
            
            if($_GET['record_type'] == 0) // this means that the user is downloading a databack depending on the param selected
            {
                //add filter parameters
                $cmd .=" AND  " . 'replace(tpda.status_code,"\r","")'." IN({$status_codes_callable})";
                if(!($_GET['txtCalled_From'] =="" AND $_GET['txtCalled_To'] == ""))
                {
                    $cmd .= " AND str_to_date(tpda.call_date,'%m/%d/%Y')>='{$_GET['txtCalled_From']}' AND str_to_date(tpda.call_date,'%m/%d/%Y')<='{$_GET['txtCalled_To']}'";
                } 
                
            }
            
    //        print_r($cmd);
    //        die();
          /*   print_r($cmd);
                die();*/
           
            //$res = $this->db->fetchAllTOFile($cmd, $file_name);      
                $res = $this->db->fetchAllTOFile($cmd, $fp,$status_code_position,$custom_status_code['custom_status']);
            /*$cmd .=" INTO OUTFILE '{$dir_file_name}'";
            $cmd .=" FIELDS TERMINATED BY ','";
            $cmd .=" LINES TERMINATED BY '\n';"; 
            */
            /*    $this->db->query($cmd);
                return $this->success($file_name);  */ 
            
    //        print_r($cmd);
    //        die();
    //        $res = $this->db->fetchAllTOFile($cmd, $file_name);
            //print_r($cmd);
          die();      
         }


 
     public function get_databack($params)
     {
        $cmd="select ptd.misc_fields, ptd.phone_no,tpda.* from po_transaction_data_ans tpda
        LEFT JOIN  po_transaction_data ptd on tpda.po_transaction_data_id = ptd.id where tpda.po_id =". $params['id']; 
      
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
 
      public function get_calling_list($params)
     {  
        $sql="select ccl.is_reassign, ccl.list_cnt,ccl.id,  cc.center_code,cc.name as center_name,ptd.batch_no,ptd.po_id,ptd.create_date,  ccl.center_id,ccl.record_cnt_from,ccl.record_cnt_to, ptd.total_count as record_count, ptd.process_option_id, ccl.assign_record_cnt ";
        //$sql .="     from po_transaction_data_2 ptd";
        //$sql .="     INNER JOIN center_calling_list ccl on ccl.po_id = ptd.po_id and ccl.batch_no = ptd.batch_no";
         $sql .="    ,householded_count, badphone_count, total_count, final_count, dedup_count";
        $sql .="     FROM center_calling_list ccl";
        //$sql .="     INNER JOIN call_center cc on cc.id = ccl.center_id ";
        $sql .="     LEFT JOIN call_center cc on cc.id = ccl.center_id ";
        $sql .="     INNER JOIN po_transaction_data_2_summary ptd on ccl.po_id = ptd.po_id and ccl.batch_no = ptd.batch_no";
        $sql .=" LEFT JOIN users u on u.user_center=ccl.center_id  ";
        //$sql .=" WHERE ptd.po_id =". $params['id'];
        $sql .=" WHERE ccl.po_id ={$params['id']}";
        $user_type = $this->get_user_type_by_username($_SESSION['username']);
        if(!in_array($user_type,array('Admin','System Admin','Data'))){
          $sql .= " and is_center = 1 and  u.username = '{$_SESSION['username']}'";  
        }
        $sql .=" GROUP BY ccl.id, center_id,cc.id ORDER BY ccl.center_id, ccl.id,ccl.is_reassign";
        
//        print_r($sql);
//        die();
        $result = $this->db->fetchAll($sql);
    
//          $out_calling_list = array(); 
//          foreach ($result as $row) {
            //   $sql="select center_id, count(id) as record_count   from po_transaction_data where batch_no = ".$row['batch_no']." and po_id =".$row['po_id'];
              // $record_count = $this->db->fetchRow($sql);
              // $record_count = $this->db->fetchRow($sql);
//                $out_calling_list[] = array(
//                    'id'  =>$row['id'], 
//                    'batch_no' =>$row['batch_no'],
//                    'center_name'=>$row['center_name'],
//                    'center_code'=> $row['center_code'],
//                    'po_id' => sprintf('%06d',$row['po_id']),
//                    'record_cnt' =>$row['record_cnt_from'] ." - ". $row['record_cnt_to'],
//                    'record_cnt_from' =>$row['record_cnt_from'],
//                    'record_cnt_to' =>$row['record_cnt_to'],
//                    'Date_uploaded' =>$row['create_date'],
//                    'button' =>'',
//                );
//        }
        
        
        //return $this->success( $out_calling_list);   
        return $this->success( $result);   
    }
 
 
    public function get_uploadbatchno($id)
    {
        if($id>0){
            $sql="select  max(batch_no)+1 as batch_no_cnt   from po_transaction_data_2_column where  po_id =".$id;
            //$sql="select  max(batch_no)+1 as batch_no_cnt   from po_transaction_data_2 where  po_id =".$id;
           
            $po_data_trans=$this->db->fetchRow($sql);  
            if($po_data_trans['batch_no_cnt']>0){
                 return $po_data_trans['batch_no_cnt'];
            }else{
                 return 1;
            }
        }
    
    }
    
 public function check_if_has_po_transaction_data($params)
  {
            
        $sql="select * from po_transaction_data_ans_2_column where  po_id =". $params['id']." LIMIT 1";
         
        $po_data_trans=$this->db->fetchAll($sql);  
        
        if(count($po_data_trans)>0){
            return TRUE;
            
        }else{
            return FALSE;
        }
  }
    
     public function isexist_po_transaction_data($params)
     {
        $cmd="select * from po_transaction_data_{$params['id']} where  po_id =".$this->hideit->decrypt(sha1(md5($this->salt)), $params['id'], sha1(md5($this->salt)))." LIMIT 1";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
    
     public function get_poid_by_encrypted_id($params)
     {
        $cmd="select id from po where  id =".$this->hideit->decrypt(sha1(md5($this->salt)), $params['id'], sha1(md5($this->salt)));
        
        $result = $this->db->fetchRow($cmd);                
        return $this->success( $result['id']);
     } 
    public function get_po_transaction_data($params)
     {
         $process_list_cmd="select ptd.process_option_id FROM po_transaction_data_2_{$params['id']} ptd
         inner join process_list_option plo on(plo.id=ptd.process_option_id) where  po_id =". $params['id'] ." limit 1";
         
         $result = $this->db->fetchRow($process_list_cmd);
         if($result['process_option_id']==1){ //Non-Household List AS-IS
                $cmd="select * from po_transaction_data_2_{$params['id']} where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['id'];
                $cmd .=" UNION ALL";
                $cmd .=" (select * from po_transaction_data_2_{$params['id']} where batch_no =". $params['batch_no'] ." and po_id =". $params['id'];
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
              
         }elseif($result['process_option_id']==2){ //Non-Household List DE-DUP
                $cmd="SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']}";
                $cmd .=" where is_column =1 and batch_no= " .$params['batch_no']. "  and po_id =". $params['id']; 
                $cmd .=" UNION ALL";
                $cmd .=" (SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']} where batch_no =". $params['batch_no'] ." and  po_id =". $params['id'] ." GROUP BY phone_no  order by id";
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
         }elseif($result['process_option_id']==3){//Already-Householded List Load
                 
         }else{ //4 - Already-Householded List Load
             
         }
         
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
     
      public function export_calling_list_2($params)
     {             
     
        $center_calling_list_id = $params['id'];
        
        $cmd ="SELECT record_cnt_from as min_id, record_cnt_to as max_id  FROM center_calling_list WHERE list_cnt ={$center_calling_list_id} and po_id ={$params['po_id']} and batch_no ={$params['batch_no']} AND center_id={$params['center_id']}";

           $r_id = $this->db->fetchRow($cmd);
           $min_id = $r_id['min_id'];
           $max_id = $r_id['max_id'];
         
        $dir = (dirname(dirname(__DIR__)));
        $dir = str_replace('\\', "/", $dir);
        $cmd = "select po_no from po where id ={$params['po_id']}";
        $rs = $this->db->fetchRow($cmd);
        $file_name =$rs["po_no"]."_Calling_List_".$params['center_code'].'_'.$params['batch_no'].'_'.time().".csv";
        $dir_file_name = $dir."/public/{$file_name}";
        
        
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);
        $no_of_q = $row_no_of_question['number_of_question'];
      
        $get_column_cmd="select lname_field, fname_field, misc_fields, remarks from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
  
        $row = $this->db->fetchRow($get_column_cmd);
        
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);

        $no_of_q = $row_no_of_question['number_of_question'];

        $q_fields="";
        $q_fields_empty="";
        
        for($x =1; $x<=$no_of_q; $x++)
        {
            $q_fields.="'Q{$x}'," ; 
            $q_fields_empty.="'' Q{$x}," ; 
        }
        
        $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
        
        $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
        
        $last_name = $row['lname_field'];
        $first_name = $row['fname_field'];
        
        $misc_field="";
        if($row['misc_fields']!="")
        {
            $misc_field = unserialize($row['misc_fields']);
        }
        $other_fields ="";
        if($misc_field!="")
        {
            foreach($misc_field as $r)
            {
                $column_name = explode("|",$r);
                $col = $column_name[1];
                $other_fields.="'{$col}'," ;
            }
        }
        $other_fields = substr($other_fields, 0, strlen($other_fields)-1);
    
        $remarks = unserialize($row['remarks']);
        /*$remarks = unserialize($row['remarks']);
        $remarks = explode("|",$remarks);
        $remarks_field ="";
        $remarks_empty_field = "";
        foreach($remarks as $r)
        {
            $col = $r;
            $remarks_field.="'{$col}'," ;
            $remarks_empty_field.="''," ;
        }
//        print_r(count($remarks));
//        die();
        $remarks_field = substr($remarks_field, 0, strlen($remarks_field)-1);
        
        $remarks_empty_field = substr($remarks_empty_field, 0, strlen($remarks_empty_field)-1);*/
        $other_fields = str_replace("'", "", $other_fields);
            
         if($params['process_option_id']==1){ //Non-Household List AS-IS
         
                if(!(trim($other_fields)==""))
                {
                    $other_fields = ",{$other_fields}";
                }
                
        if($q_fields==""){
            $cmd="select 'crID','phone','last_name','first_name' {$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time', 'duration','TSR','status_code' from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
   
        }else{
            $cmd="select 'crID','phone','last_name','first_name' {$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time', 'duration','TSR','status_code',
{$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
     
        } 
        
        
                $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');

                
                fputcsv($fp, array_values($res));


                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name}, {$other_fields},'','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
                if($q_fields_empty==""){
                    $cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code from po_transaction_data_2_{$params['po_id']} ptd  where   ptd.po_id =". $params['po_id'];    
                }else{
                    $cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code,{$q_fields_empty} from po_transaction_data_2_{$params['po_id']} ptd  where   ptd.po_id =". $params['po_id'];
                }
                
                //$cmd .=" and ccl.list_cnt = ".$params['id']."  and  ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.list_cnt = ".$params['id']."  and  ptd.batch_no=".$params['batch_no'] ;
                $cmd .=" and  ptd.batch_no=".$params['batch_no'] ;
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND ptd.is_column=0 AND  LENGTH(TRIM(ptd.phone_no))=10 order by  CAST(crID AS UNSIGNED)";
                $cmd .=" and id>={$min_id} AND id<={$max_id} AND ptd.is_column=0 AND  LENGTH(TRIM(ptd.phone_no))=10 ";
                //order by  CAST(crID AS UNSIGNED)";
           
//                print_r($cmd);
//                die();
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";                
         }
         
         elseif($params['process_option_id']==2 || $params['process_option_id']==4){ //Non-Household List DE-DUP    //Already-Householded List Load
                
             if($q_fields==""){
                 $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code' from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
             }else{
                 $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code',
    {$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
             }
                
                //$cmd .=" UNION ALL";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name}, {$other_fields}, '','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
                
                $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, array_values($res));

                
                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'' status_code,{$q_fields_empty} from po_transaction_data_2 ptd where   ptd.po_id =". $params['po_id'];
                if($q_fields_empty==""){
                    $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name}, ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
                }else{
                    $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name}, ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code,{$q_fields_empty} from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
                }
                
                
                
                
                //$cmd .=" and  ccl.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and  ptd.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'];
                $cmd .="  and ptd.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 order by  CAST(crID AS UNSIGNED)";
                $cmd .=" and id>={$min_id} AND id<={$max_id} AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 "; //"order by  CAST(crID AS UNSIGNED)";
                
                
                
                //"GROUP BY phone_no HAVING COUNT(phone_no)=1";
//                $cmd .=" UNION ALL";
//                $cmd .=" select id,phone_no,{$last_name},{$first_name}, {$other_fields}, {$remarks_empty_field},'','','','','','',{$q_fields_empty} from po_transaction_data_2 where   po_id =". $params['po_id'];
//                $cmd .=" and batch_no=".$params['batch_no'];
//                $cmd .=" and center_id=".$params['center_id']. "  AND  is_column=0";//" GROUP BY phone_no HAVING COUNT(phone_no)>1";
//               print_r($cmd);
//               die();
                     
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";  
                
         }elseif($params['process_option_id']==3){      //Load then Household list
              if($q_fields==""){
                    $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code'
              from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];  
              }else{
                $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code',
              {$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];          
              } 
                
              
              $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');

                
                fputcsv($fp, array_values($res));
                                                   

                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select GROUP_CONCAT(ptd.id SEPARATOR '|'),ptd.phone_no,{$last_name},GROUP_CONCAT({$first_name} SEPARATOR '|'), {$other_fields}, '','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
//                $cmd .=" select GROUP_CONCAT(ptd.id SEPARATOR '|'),ptd.phone_no,{$last_name},GROUP_CONCAT({$first_name} SEPARATOR '|'),{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'' status_code,
//              {$q_fields_empty}  from po_transaction_data_2 ptd where   ptd.po_id =". $params['po_id'];
        
            if($q_fields_empty==""){
                $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name},ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'NC' status_code
              from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
            }else{
                $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name},ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'NC' status_code,
              {$q_fields_empty}  from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
            }
            
              
                //$cmd .=" and  ccl.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and  ptd.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'];
                $cmd .=" and ptd.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 GROUP BY ptd.phone_no order by  CAST(crID AS UNSIGNED)";
                //$cmd .="  and id>={$min_id} AND id<={$max_id} AND LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 GROUP BY ptd.phone_no "; //" order by  CAST(crID AS UNSIGNED)";
                
                $cmd .="  and id>={$min_id} AND id<={$max_id} AND LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0";
                  
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";
         }
         
         //print_r(phpinfo());
         //die();
       
        /* print_r($cmd);
           die();  */   
         
         //$res = $this->db->query($cmd);
        // $res = $this->db->fetchAll($cmd);
//        print_r($cmd);
//        die();
        
          //$res = $this->db->fetchAllTOFile($cmd, $file_name);
          
          $cmd = str_replace(",,",",", $cmd); // to handle empty other fields

          
          
          $datetime=$this->get_server_date();
            $this->save_to_action_logs('Download calling list.' ,'PO',$datetime,$_SESSION['username'],$params['po_id']);                                                            
          $res = $this->db->fetchAllTOFile($cmd, $fp);
           
            die();
        /*$fp = fopen('php://output', 'w');
        
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
        foreach($res as $row)
        {
            fputcsv($fp, array_values($row));
        }
        die();
         */   
         
         
         //$cmd = escapeshellcmd('mysql -u lemmuelc_webpol -p %3uLw(g06T}J -h localhost -e "'.$cmd.'"');

        
         //$test = shell_exec('/usr/bin/mysql -ulemmuelc_webpol -p %3uLw(g06T}J -h localhost -e' .'"'.$cmd);
         //var_dump($test);
         //die(mysql_error());
         //return $this->success($file_name);
         
     }

      
  
     public function export_calling_list($params)
     {
         $process_list_cmd="select ptd.process_option_id FROM po_transaction_data_2 ptd
         inner join process_list_option plo on(plo.id=ptd.process_option_id) where  po_id =". $params['id'] ." limit 1";
         
         $result = $this->db->fetchRow($process_list_cmd);
         if($result['process_option_id']==1){ //Non-Household List AS-IS
                $cmd="select col1,col2,col3 from po_transaction_data_2_{$params['id']} where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['id'];
                $cmd .=" UNION ALL";
                $cmd .=" select  col1,col2,col3 from po_transaction_data_2_{$params['id']} where   po_id =". $params['id'];
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] ;
                $cmd .=" INTO OUTFILE 'C:/file.csv'";
                $cmd .=" FIELDS TERMINATED BY ','";
                $cmd .=" LINES TERMINATED BY '\n';";   
         }elseif($result['process_option_id']==2){ //Non-Household List DE-DUP
                $cmd="SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']}";
                $cmd .=" where is_column =1 and batch_no= " .$params['batch_no']. "  and po_id =". $params['id']; 
                $cmd .=" UNION ALL";
                $cmd .=" (SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']} where  po_id =". $params['id'] ." GROUP BY phone_no  order by id";
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
         }elseif($result['process_option_id']==3){//Already-Householded List Load
              //"select phone_no from po_transaction_data_2  where center_id=1 and  po_id =1 group by phone_no having count(phone_no)>1";
         } 
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
     
     public function get_po_transaction_data_col($params)
     {
        $cmd="select * from po_transaction_data_2_{$params['id']} where  po_id =". $params['id']." LIMIT 1";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
    
     public function get_column_data($params)
     {

               $cmd="select row_data
                from po_transaction_data_{$params['id']} 
                where is_column = 1 and  po_id =". $params['id'];
                $result = $this->db->fetchRow($cmd);
             
                
            return $this->success( $result['row_data']);
     } 
     public function get_customer_databack_fields($params)
     {
        $cmd="select customer_databack_fields from po_management  where po_id =". $params['id'];
       
        $result = $this->db->fetchRow($cmd);
        return $this->success( $result['customer_databack_fields']);
     } 
     public function get_upload_summary($params)
     {
         
          if($params['type']=='AS-IS'){ //Non-Household List AS-IS
             
                $cmd =" select id,phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0";
                $result = $this->db->fetchAll($cmd);
                return count($result);
         }elseif($params['type']=='NON-H' || $params['type']=='AL-H'){ //Non-Household List DE-DUP    //Already-Householded List Load
             
                $cmd =" select phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
                $result = $this->db->fetchAll($cmd);
                return count($result);
         }elseif($params['type']=='LD-H'){//Load then Household list
                $cmd =" select id,phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0 GROUP BY phone_no ";
                 $result = $this->db->fetchAll($cmd);
                 return count($result);
         }elseif ($params['type']=='bad_phone'){
             $cmd="select count(ptd.id) return_value from po_transaction_data_2_{$params['po_id']} ptd 
                 where ptd.is_column = 0 and ptd.batch_no = ". $params['batch_no'] ." and  ptd.po_id =". $params['po_id']. 
             " AND LENGTH(TRIM(phone_no))<>10";
           
             $result = $this->db->fetchRow($cmd);
             return $result['return_value'];
         }
  
        /* if($params['type']=='householded_records'){
        
           $cmd="select phone_no from po_transaction_data_2  where  is_column is null and batch_no = ". $params['batch_no'] ." and  po_id =". $params['po_id'] . 
          " group by phone_no having count(phone_no)>1";
          $result = $this->db->fetchRow($cmd);
          return $result;
         }elseif ($params['type']=='bad_phone'){
             $cmd="select count(ptd.id) return_value from po_transaction_data_2 ptd 
                 where ptd.batch_no = ". $params['batch_no'] ." and  ptd.po_id =". $params['po_id']. 
             " AND LENGTH(phone_no)<>10";
             $result = $this->db->fetchRow($cmd);
        return $result['return_value'];
         }else{}   */
         
        
     } 
     public function get_process_list_option()
    {
        $cmd="select id,name from process_list_option  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
     
  /*  public function upload_call_list($params)
    {  
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 1000, ",");
            $check_col =$data;
            $num = count($data); 
           fclose($handle); 
        } 
       
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\n");  
       //$file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
                        $phone_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                            if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .="@col".$c.","."'|',";

                          } 
                          
       $file_query .= substr($col, 0, -1);   
       $file_query .=" )";
       $file_query .=" set row_data=CONCAT(".substr($row_data, 0, -5)."),\n";
       $file_query .="  phone_no=@col".$phone_cnt.",po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       $file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
        
       $res= $this->db->query($file_query);

       if($res==1)
       {                     
           $update_po=array(
              'is_column'=>1,
           );
           if(!$this->db->update('po_transaction_data', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
              return false; 
           }else{
              return true; 
           }
       }else{
           return false;
       }
    }   */
    
    public function validate_column_format($po_id)
    {
       
        
    }
    
         
         
     public function upload_call_list_2_temp($params)
    {  
        
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $file_col="";
        $wrong_format = "";
        $wrong_format_str="";
        if (($handle = fopen($file, "r")) !== FALSE) {
          
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data); 
            
            
           fclose($handle); 
        } 
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            $file_col.="{$check_col[$x]}|" ;
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
       $file_col = explode("|",substr($file_col, 0, strlen($file_col)-1));
       $cmd="SELECT col_fields FROM po_transaction_data_2_column WHERE po_id={$po_id} and is_column = 1 limit 1";
        
       $row = $this->db->fetchAll($cmd);
        if(count($row)>0){
            
        
          foreach($row as $r_col){
           $col_fields = unserialize($r_col['col_fields']);
           $col_fields = explode("|",$col_fields);
           $format_col_fields="";
            foreach($col_fields as $r){
                $format_col_fields.="{$r}," ;
            }
               
            $format_col_fields = substr($format_col_fields, 0, strlen($format_col_fields)-1);
            
             $cmd="SELECT {$format_col_fields} FROM po_transaction_data_2_column WHERE po_id={$po_id} and is_column = 1 limit 1";
             $row_actual_col = $this->db->fetchRow($cmd);
           
              $x=1;
             
              foreach($file_col as $r){
                    if(trim($row_actual_col['col'.($x)]) != trim($r)){
                        $wrong_format=$wrong_format.trim($r)."|";
                    }
                     
                    $x++;
              }
           }
         $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
      
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>$row_actual_col,
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
         
    }else{
        
          foreach($file_col as $r){
                  
                  if( trim($r)=='spoke_to'|| trim($r)=='call_date'|| trim($r)=='call_time'|| trim($r)=='duration'||
                      trim($r)=='TSR'|| trim($r)=='status_code'){
                       $wrong_format=$wrong_format.trim($r)."|";
                }
          }
          
          $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
     
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>'',
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
          
    }
    
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $file_query  ="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_2_temp_{$po_id}";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
      // $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");  
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape(NEWLINE);  
       $file_query .=" IGNORE 1 LINES";
        
     
       $file_query .=" (";
                        $phone_cnt=-1;
                        $columns1 = "";
                        for ($c=1; $c <=$num; $c++) {
                             if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .= "col".$c."="."@col".$c.",";
                           $row_data_col .="col".$c."|";       
                           $columns1 .= ",col{$c}";
                          } 
                         
       $file_query .= substr($col, 0, -1);   
       //$file_query .=" ) set col_fields= '".serialize(substr($row_data_col, 0, -1))."',".$row_data;
       $file_query .=" ) set col_fields= '',".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .="  ,phone_no=trim(@col".$phone_cnt."),po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       //$file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .= "misc_fields='',remarks='',";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       

  
       $res= $this->db->query($file_query);
       if($res!=1)
       {     
          $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "Error in temp",
                );
           return $response;  
       }
     
       //insert to permanent table
       $insert_query="";
       $fixed_columns ="pdt.po_id,pdt.batch_no,is_column,center_id,pdt.phone_no,phone_field,lname_field,fname_field,process_option_id,remarks,col_fields,misc_fields,status,list_cnt,pdt.create_by,pdt.create_date";
       
       
       $fixed_columns_insert = str_replace("pdt.", "", $fixed_columns);
       $columns1_insert = str_replace("pdt.", "", $columns1);
       
       //$insert_query.=" INSERT INTO po_transaction_data_2 ({$fixed_columns}{$columns1}) SELECT {$fixed_columns}{$columns1}"; 
       $insert_query.=" INSERT INTO po_transaction_data_2_{$po_id} ({$fixed_columns_insert}{$columns1_insert}) SELECT {$fixed_columns}{$columns1}"; 
       //$insert_query.=" FROM po_transaction_data_2_temp WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id} AND batch_no={$params['batch_no']}";
       $insert_query.=" FROM po_transaction_data_2_temp_{$po_id} pdt 
LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null";
       
       $res= $this->db->query($insert_query);
       
       if($params['process_option_id']==3) //Load Then Household List Dedup
       {
           //$insert_query =" INSERT INTO po_transaction_data_2_calling_list (cr_ids,first_names,{$fixed_columns}{$columns1}) SELECT GROUP_CONCAT(id SEPARATOR '|') crids,GROUP_CONCAT({$params['fname_field']} SEPARATOR '|') first_names,{$fixed_columns}{$columns1}";
           $insert_query =" INSERT INTO po_transaction_data_2_calling_list_{$po_id} (cr_ids,first_names,{$fixed_columns_insert}{$columns1_insert}) SELECT GROUP_CONCAT(pdt.id SEPARATOR '|') crids,GROUP_CONCAT({$params['fname_field']} SEPARATOR '|') first_names,{$fixed_columns}{$columns1}";
           //$insert_query.=" FROM po_transaction_data_2 WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id}  AND batch_no={$params['batch_no']} GROUP BY phone_no ORDER BY po_id,id";
           $insert_query.=" FROM po_transaction_data_2_{$po_id} pdt 
            LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no 
            WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null GROUP BY pdt.phone_no  ORDER BY pdt.po_id,pdt.id";
           $res= $this->db->query($insert_query);
       }
       
       if($params['process_option_id']==2 || $params['process_option_id']==4) //Load Then Household List Dedup
       {
            
            //$insert_query =" INSERT INTO po_transaction_data_2_calling_list (cr_ids,first_names,{$fixed_columns}{$columns1}) SELECT id,{$params['fname_field']} first_names,{$fixed_columns}{$columns1}";
            $insert_query =" INSERT INTO po_transaction_data_2_calling_list_{$po_id} (cr_ids,first_names,{$fixed_columns_insert}{$columns1_insert}) SELECT pdt.id,{$params['fname_field']} first_names,{$fixed_columns}{$columns1}";
           //$insert_query.=" FROM po_transaction_data_2 WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id} AND batch_no={$params['batch_no']} GROUP BY phone_no  ORDER BY po_id,id";
           $insert_query.=" FROM po_transaction_data_2_{$po_id} pdt 
LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no 
WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null GROUP BY pdt.phone_no  ORDER BY pdt.po_id,pdt.id";
           $res= $this->db->query($insert_query);
       }
       
       if($res==1)
       {    
            $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $ctr++; 
            }
            
            //$dedup_prev = $this->db->fetchRow("SELECT count(id) as total_dedup FROM po_transaction_data_2_temp WHERE phone_no IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND batch_no={$params['batch_no']}");
            $dedup_prev = $this->db->fetchRow("SELECT count(id) as total_dedup FROM po_transaction_data_2_temp_{$po_id} WHERE phone_no IN(SELECT phone_no FROM po_transaction_data2_nos_{$po_id} WHERE po_id={$po_id}) AND batch_no={$params['batch_no']} AND po_id={$po_id}");
            

            
            $cmd ="INSERT INTO po_transaction_data_2_column (". substr($column_name, 0, strlen($column_name)-1) .",col_fields,po_id,is_column,process_option_id,batch_no,phone_field,fname_field,lname_field,misc_fields,remarks,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",'".serialize(substr($row_data_col, 0, -1))."'";   
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['process_option_id'];   
            $cmd .= ",".$params['batch_no'];   
            $cmd .= ",".$this->db->db_escape($params['phone_field']); 
            $cmd .= ",".$this->db->db_escape($params['fname_field']); 
            $cmd .= ",".$this->db->db_escape($params['lname_field']); 
            $cmd .= ",".$this->db->db_escape($params['misc_fields']); 
            $cmd .= ",".$this->db->db_escape($params['remarks']); 
            
            if(isset($dedup_prev['total_dedup']))
            {
                $cmd .= ",'{$dedup_prev['total_dedup']}'";    
            }else
            {
                $cmd .= ",'0'";    
            }
            //$cmd .= ",'N'";
            
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
            

            $this->db->query($cmd);

            
            $cmd ="INSERT INTO po_transaction_data2_nos_{$po_id}" ;
            $cmd .="(phone_no, po_id, batch_no) SELECT phone_no, po_id, batch_no FROM po_transaction_data_2_temp_{$po_id} WHERE po_id={$po_id} AND batch_no={$params['batch_no']}";
            
            $this->db->query($cmd);
            
            
   //         $this->db->query("DELETE FROM po_transaction_data_2_temp_{$po_id} WHERE po_id={$po_id} AND batch_no={$params['batch_no']}");
            
            
//           $update_po=array(
//              'is_column'=>1,
//           );
//           if(!$this->db->update('po_transaction_data_2', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
               $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                ); 
//           }else{
                $value=array(
                            'id'    =>$po_id,
                            'status'=> 'DR',
                            );
                            $this->change_po_status($value);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              $response = array(
                     'data'   =>"",
                    'err_num'=>"0000",
                    'message' => "",
                   
                );
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Upload data row','PO',$datetime,$_SESSION['username'],$po_id); 
              return $response; 
//           }
       }else{
           $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "{$insert_query}",
                );
           return $response;  
       }
    }
    
     
   
    public function drop_client_data($id, $batch_no =0)
    {
         
        $data_result =$this->check_data_if_result_exist($id);
       
        if($data_result>0){
            return $this->failed("Unable to drop data if the result already uploaded.");
        }else{
            
           
            
            if($batch_no == 0)
            {
               
//                 if (! $this->db->delete('po_transaction_data_2', 'po_id=' .  $id)) {
//                                    return $this->failed("Unable to drop client data.1");
//                 }    
                $values = array(
                                    'status'=>'PD', //Once data dropped need to change the status to Pending Data
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                $this->db->delete('center_calling_list', 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_summary', 'po_id=' .  $id);
                /*$this->db->delete('po_transaction_data2_nos_'.$id, 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_temp_'.$id, 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_calling_list_'.$id, 'po_id=' .  $id);*/
                $cmd_delete= " TRUNCATE TABLE po_transaction_data_2_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data2_nos_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data_2_temp_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data_2_calling_list_{$id};";
                $this->db->query($cmd_delete);
                
                $this->db->query($cmd_delete);
                $this->db->update('po', $values, 'id='.$id);
                $values_u = array(
                                    'universe'=>0,
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                                $this->db->update('po_parameter', $values_u, 'po_id='.$id);
                
                
            }else
            {
//                if (! $this->db->delete('po_transaction_data_2', 'po_id=' .  $id . " AND batch_no={$batch_no}")) {
//                    return $this->failed("Unable to drop client data.");
//                }   
                $this->db->delete("po_transaction_data_2_".$id, 'po_id=' .  $id . " AND batch_no={$batch_no}");
                $universe_drop_cnt = $this->db->fetchRow("select (final_count-dedup_count) as final_universe from po_transaction_data_2_summary where po_id= {$id} AND batch_no={$batch_no}");
                $po_universe = $this->db->fetchRow("select universe from po_parameter where po_id={$id}");
                 
                $this->db->delete('center_calling_list', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_summary', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data2_nos_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_temp_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_calling_list_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                
                $cmd="SELECT id FROM po_transaction_data_2_column WHERE po_id={$id}";
               
                  $values_u = array(
                                    'universe'=>$po_universe['universe']-$universe_drop_cnt['final_universe'],
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                                $this->db->update('po_parameter', $values_u, 'po_id='.$id);
                
                $row = $this->db->fetchRow($cmd);
                if(!isset($row['id']))
                {
                    $values = array(
                        'status'=>'PD', //Once data dropped need to change the status to Pending Data
                        'update_by'=>$_SESSION['username'],
                        'update_date'=>date('Y-m-d H:i:s'), 
                    );
                    $this->db->update('po', $values, 'id='.$id);
                }
                    
            }
            return $this->success("Client data has been dropped");
        }
        
    }
    
    public function upload_call_list($params)
    {  
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $file_col="";
        $wrong_format = "";
        $wrong_format_str="";
        if (($handle = fopen($file, "r")) !== FALSE) {
          
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data); 
            
            
           fclose($handle); 
        } 
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            $file_col.="{$check_col[$x]}|" ;
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
       $file_col = explode("|",substr($file_col, 0, strlen($file_col)-1));
       $cmd="SELECT col_fields FROM po_transaction_data_2 WHERE po_id={$po_id} and is_column = 1 limit 1";
        
       $row = $this->db->fetchAll($cmd);
        if(count($row)>0){
            
        
          foreach($row as $r_col){
           $col_fields = unserialize($r_col['col_fields']);
           $col_fields = explode("|",$col_fields);
           $format_col_fields="";
            foreach($col_fields as $r){
                $format_col_fields.="{$r}," ;
            }
               
            $format_col_fields = substr($format_col_fields, 0, strlen($format_col_fields)-1);
            
             $cmd="SELECT {$format_col_fields} FROM po_transaction_data_2 WHERE po_id={$po_id} and is_column = 1 limit 1";
             $row_actual_col = $this->db->fetchRow($cmd);
              $x=0;
            
              foreach($file_col as $r){
                    if(trim($row_actual_col['col'.($x+1)]) != trim($r)){
                        
                        $wrong_format=$wrong_format.trim($r)."|";
                         
                    }
                    $x++;
              }
           }
         $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
           
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>$row_actual_col,
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
         
    }
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_2";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");   
       $file_query .=" IGNORE 1 LINES";
     
       $file_query .=" (";
                        $phone_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                             if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .= "col".$c."="."@col".$c.",";
                           $row_data_col .="col".$c."|";       
                          } 
                         
       $file_query .= substr($col, 0, -1);   
       //$file_query .=" ) set col_fields= '".serialize(substr($row_data_col, 0, -1))."',".$row_data;
       $file_query .=" ) set col_fields= '',".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .="  ,phone_no=@col".$phone_cnt.",po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       //$file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .= "misc_fields='',remarks='',";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       

       $res= $this->db->query($file_query);
     
       if($res==1)
       {                  
            
            
            $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $ctr++; 
            }
            
            $cmd ="INSERT INTO po_transaction_data_2_column (". substr($column_name, 0, strlen($column_name)-1) .",col_fields,po_id,is_column,process_option_id,batch_no,phone_field,fname_field,lname_field,misc_fields,remarks,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",'".serialize(substr($row_data_col, 0, -1))."'";   
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['process_option_id'];   
            $cmd .= ",".$params['batch_no'];   
            $cmd .= ",".$this->db->db_escape($params['phone_field']); 
            $cmd .= ",".$this->db->db_escape($params['fname_field']); 
            $cmd .= ",".$this->db->db_escape($params['lname_field']); 
            $cmd .= ",".$this->db->db_escape($params['misc_fields']); 
            $cmd .= ",".$this->db->db_escape($params['remarks']); 
            $cmd .= ",'N'";
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
            

            $this->db->query($cmd);
//           $update_po=array(
//              'is_column'=>1,
//           );
//           if(!$this->db->update('po_transaction_data_2', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
               $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                ); 
//           }else{
                $value=array(
                            'id'    =>$po_id,
                            'status'=> 'DR',
                            );
                            $this->change_po_status($value);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              $response = array(
                     'data'   =>"",
                    'err_num'=>"0000",
                    'message' => "",
                   
                ); 
              return $response; 
//           }
       }else{
           $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                );
           return $response;  
       }
    }
   public function get_responses($po_id)
   {
        $cmd ="select psq.sequence as question,psr.row_no as response_no,psr.response,psr.go_to From  po_script_q_response psr
            INNER JOIN po_script_question psq ON(psr.po_script_question_id=psq.id) WHERE psq.po_id={$po_id} order by psq.sequence" ; 
         
        $result=$this->db->fetchAll($cmd); 
        return $result;  
   } 
    public function check_skip_logic($params)
   {
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        
        //Check Skip Logic
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id={$po_id}");
        $max_res = $this->db->fetchRow("select max(psr.row_no) as max_res From  po_script_q_response psr
            INNER JOIN po_script_question psq ON(psr.po_script_question_id=psq.id) WHERE psq.po_id={$po_id}");
      /*
         print_r($max_res['max_res']);
         exit;
               */

          $status_codes = $this->db->fetchAll("select ptc.status_code,sct.name from project_template_code ptc
                                                INNER JOIN status_code_type sct on  ptc.type = sct.id 
                                                INNER JOIN project_template pt on pt.id = ptc.project_template_id
                                                INNER JOIN po p on p.project_template_id=pt.id
                                                where p.id ={$po_id}");
   
        
           $status_type = array();
           foreach($status_codes as $res_q){
                   $status_type[strtoupper($res_q['status_code'])] = strtoupper($res_q['name']);
                    
           }
         /*  print_r($status_type['T']);
           exit; 
                 */
        $no_of_q = $row_no_of_question['number_of_question'];
        $closing_no = ($no_of_q+1);
        $qestion_response=$this->get_responses($po_id);
       /* print_r($qestion_response);
        exit;     */
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 20000, ","); 
            $check_col =$data;
            $num = count($data);
            $col_num="";
            $status_code_col="";
            //Get column position
             for ($c=0; $c < $num; $c++) {
                for ($x=1; $x <=$no_of_q; $x++) {
                    if($data[$c]=='Q'.$x){
                               $col_num=$col_num. ($c).',';
                    } 
                }  
                if(trim($data[$c])=='status_code'){
                               $status_code_col=$c;
                    } 
             }
             
             $row = 0;
             $col_num = substr($col_num, 0, strlen($col_num)-1);
             $col_num = explode(",",$col_num);
              
            
              $script_res = array();
               $skip_logic=array();
               $str = "";
              $setof_res = array();
              $skip_num = 2;
              //$x=$max_res['max_res']; 
               // $x=1;
                 
//               print_r($qestion_response);
//               exit;  
                 for ($y=1; $y <=$no_of_q; $y++) {
                     
                     foreach($qestion_response as $res_q){ 
                         
                           if($res_q['question']==$y){
//                            $skip_logic=$skip_logic+array(
//                             /*$res_q['go_to']=>$res_q['response_no'],*/
//                             
//                             $res_q['response_no']=>$res_q['go_to'],
//                            );
                             
                             $resp[$res_q['response_no']] = $res_q['go_to'];
//                             $resp[] = array($res_q['go_to']);
                            $skip_logic[$res_q['response_no']] = $res_q['go_to'];
                           }
                           $resp ="";
                     } 
                        $setof_res[]=$skip_logic;
                        $skip_logic=array();
                        $resp ="";
                 }
                 /*print_r($setof_res);
                 exit;  */
            $wrong_crID ="";   
             while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                $num = count($data);
                /*print_r($col_num[0]);
                exit;  */
                  /*  // print_r($data[$status_code_col]);
                  */     
                      
                      //Check if the status codes are valid based on the project template
                        if(!array_key_exists(strtoupper($data[$status_code_col]),$status_type))
                        {
                             $wrong_crID=$data[0]; //Retrun the crID with incorrect skip logic.
                              $response = array(
                                     'success'=>false,
                                     'message' => "Unable to upload result. Invalid status code ".$data[$status_code_col]. ".  (CRID# ".$wrong_crID.")" ,
                                   );  
                              return $response;
                            
                        }else{
                            $type =  $status_type[strtoupper($data[$status_code_col])];
                         
                        }
                     
                         //Check all Complete answers
                    if($type=='COMPLETE' && $no_of_q>0){
                         if(trim($data[$col_num[0]])=="0" || trim($data[$col_num[0]])==""){
                              $wrong_crID=$data[0]; //Retrun the crID with incorrect skip logic.
                              $response = array(
                                     'success'=>false,
                                     'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                                   ); 
                              return $response;
                        }  
                       
                          
                      
                            if(!$this->validate_response($col_num,$data,$setof_res)){
                               $wrong_crID=$data[0]; 
                               $response = array(
                                  'success'=>false,
                                  'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                               ); 
                            return $response;
                            }   
                    }else{
                             if($type!="LEAVE MESSAGE(COMPLETE)" && $no_of_q>0)
                               {
                                //Check if non Complete status had a value.
                                    foreach($col_num as $r){
                                        if(trim($data[$r])!="" && trim($data[$r])!="0"){
                                           $wrong_crID=$data[0]; 
                                           $response = array(
                                              'success'=>false,
                                              'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                                           );
                                           return $response;  
                                        }
                                    }
                               }

                    }
                }
                  
           fclose($handle);  
           $response = array(
                 'success'=>true,
                 'message' => "CSV file has been uploaded." ,
           ); 
           
           return $response;
    }
         
      
       
}
    public function alertMessage($message){
      echo '<script type="text/javascript">alert("' . $message . '")"</script>';
    } 
   public function upload_finished_call_list($params)
    {  

        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $check_crid="";
        $cmd="select allow_disable_logic from po_management where po_id={$po_id}";
        $result = $this->db->fetchRow($cmd);
        if($result['allow_disable_logic']==2)
        {
           $skip_logic=$this->check_skip_logic($params);
           if(!$skip_logic['success']){
             return $this->failed($skip_logic);
            }   
       } 
       $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$params['id']." and center_id is null AND is_column=1 LIMIT 1";
       $proc_opt = $this->db->fetchRow($cmd);
       
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data);
            $col_num="";      
            $call_date_position=""; 
            $crID_position=0;
            //Validation for Call date
            $call_date_position="";
            $call_date_temp="";
            $status_code_position="";
            $status_code_temp="";
             for ($c=0; $c <$num; $c++) {
                    if ($check_col[$c]=="crID") {
                              $crID_position = $c;   
                     }
                     if ($check_col[$c]=="call_date") {
                              $call_date_position = $c;   
                     }
                     if ($check_col[$c]=="status_code") {
                              $status_code_position = $c;   
                     }
                  }
                  
                    while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                        $temp_crID=$data[$crID_position];
                        $call_date_temp=$data[$call_date_position];
                        $status_code_temp=$data[$status_code_position];
                            if ($call_date_temp=="" ){
                                 return $this->failed("Unable to upload result. Please check your call_date value. CRID# ({$temp_crID})"); 
                                 die(); 
                            }  
                             if ($call_date_temp!="" && $status_code_temp=='NC'){
                                 return $this->failed("Unable to upload result. Please check your call_date value. CRID# ({$temp_crID})"); 
                                 die(); 
                            } 
                        }
           
             
             
            
          //Validation of spoke_to   for Option 3
           
           $spoke_to_position=0;
          if($proc_opt['process_option_id']==3) {
                  for ($c=0; $c <$num; $c++) {
                     if ($check_col[$c]=="crID") {
                              $crID_position = $c;   
                     }
                     if($check_col[$c]=="spoke_to"){
                                $spoke_to_position = $c;  
                     }
                  }  
              
                while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                       $temp_crID=$data[$crID_position];
                       $temp_spoke=$data[$spoke_to_position];
                        if (strpos($temp_crID, '|') !== FALSE){
                             $ids = explode("|",$temp_crID);  
                             if($temp_spoke>count($ids)){
                                 return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                                 die();
                             }
                        }else{
                             if($temp_spoke>1){
                                 return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                                 die();
                             }    
                        }
                   
                }
       
                
          }
          
          //Validation of spoke_to   for Option 4 First name
           if($proc_opt['process_option_id']==4) {
               
           $first_name_col = $this->db->fetchRow("SELECT fname_field FROM po_transaction_data_2_column  WHERE po_id = ".$params['id'] ." AND is_column=1 LIMIT 1");
           $first_name = $this->db->fetchRow("SELECT  {$first_name_col['fname_field']} as col  FROM po_transaction_data_2_column  WHERE po_id = ".$params['id'] ." AND is_column=1 LIMIT 1");
           $first_name  =$first_name['col'];
           $first_name = substr($first_name, 0, 5);
        
                $get_column_cmd="select  misc_fields from po_transaction_data_2_column where is_column =1 and  po_id =". $params['id'] ." limit 1";
                $row = $this->db->fetchRow($get_column_cmd);
                //Misc fields and get the firstname prefix
                $misc_field="";
                $count_firstname=1; //Set default value for firstname #1
                if($row['misc_fields']!=""){
                        $misc_field = unserialize($row['misc_fields']);
                   
                    foreach($misc_field as $r){
                        $column_name = explode("|",$r);
                        if(strtolower($first_name)==strtolower(substr($column_name[0], 0, 5))){
                              $count_firstname++;
                        }
                    }
                     for ($c=0; $c <$num; $c++) {
                         if($check_col[$c]=="spoke_to"){
                                    $spoke_to_position = $c;  
                         }
                      }  
                  
                     while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                           $temp_crID=$data[$crID_position];
                           $temp_spoke=$data[$spoke_to_position];
                           
                     } 
                 if($temp_spoke>$count_firstname){
                     return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                     die();
                 }
             } 
                 
           }
          
            fclose($handle);  
          }
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
      
       
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $row_data_col_2="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_ans_2_{$po_id}";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
       //$file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");  
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape(NEWLINE);  
       //$file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
                        $crID_cnt=-1;
                        $spoke_to_cnt=-1;
                        $call_date_cnt = -1;
                        $call_time_cnt=-1;
                        $question_cnt=-1;
                        $status_code_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                            if ($check_col[$c-1]=="crID") {
                              $crID_cnt = $c;  
                              
                              
                            }
                            if($check_col[$c-1]=="spoke_to"){
                                $spoke_to_cnt = $c;  
                            }
                             if($check_col[$c-1]=="call_date"){
                                $call_date_cnt = $c;  
                            }
                            if($check_col[$c-1]=="call_time"){
                                $call_time_cnt = $c;  
                            }
                            if($check_col[$c-1]=="status_code"){
                                $status_code_cnt = $c;  
                            }
                            
                        
                          /*  //Get the actual column of all the question
                                for($x =1; $x<=$no_of_q; $x++){
                                     if($check_col[$c-1]=="Q{$x}"){
                                        $q_fields.="@col".$c.","; 
                                        $q_fields_data .="concat('col{$c}','|',@col{$c},';'),";
                                        $question_cnt=$c;
                                     }    
                                }   */ 
                        
                            
                            $col .="@col".$c.","; 
                            $row_data .= "col".$c."="."@col".$c.","; 
                            $row_data_col .="concat('col{$c}','|',@col{$c},';'),";         
                          }                                
       $file_query .= substr($col, 0, -1);      
       $file_query .=" ) set remarks=concat(".substr($row_data_col, 0, -1)."),".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .=", crid_permanent=@col".$crID_cnt.",po_transaction_data_2_id=@col".$crID_cnt.",po_id=".$po_id.",\n";
       $file_query .=" batch_no=-1,record_no=".$params['record_no'].",status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       $file_query .=",status_code =TRIM(@col".$status_code_cnt."), call_date =@col".$call_date_cnt.",call_time=@col".$call_time_cnt."\n";
       
      /* //For Billing
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE billing_record";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\n");  
      // $file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
       $file_query .= substr($q_fields, 0, -1);   
       $file_query .=" )";
       $file_query .=" set row_data=CONCAT(".substr($q_fields_data, 0, -1).")\n";
       $file_query = substr($file_query, 0, -1);   */
     
       $res= $this->db->query($file_query);
        
       if($res==1)
       {
          
           $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $row_data_col_2 .="concat('col{$ctr}','|','{$col['2']}',';'),";   
                  $ctr++; 
            }
              
            $cmd ="INSERT INTO po_transaction_data_ans_2_column (". substr($column_name, 0, strlen($column_name)-1) .",po_id,is_column,batch_no,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['batch_no'];               
            $cmd .= ",'N'";
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
           if(!$this->db->query($cmd)){
              return false; 
           }else{
               $this->db->query("UPDATE po_transaction_data_ans_2_column set remarks=concat(".substr($row_data_col_2, 0, -1).") WHERE id=". $this->db->lastInsertId());
               $cmd="select id,po_transaction_data_2_id,col{$spoke_to_cnt} as spoke_to from po_transaction_data_ans_2_{$po_id} where col{$spoke_to_cnt} >0 and  LOCATE('|',po_transaction_data_2_id)>0";
               $result=$this->db->fetchAll($cmd); 
               foreach($result as $res_row) {
                    $crids = explode("|",$res_row['po_transaction_data_2_id']); 
                    $spoke_to_permanent_id = $crids[$res_row['spoke_to']-1];
                    $this->db->query("update po_transaction_data_ans_2_{$po_id} set crid_permanent = {$spoke_to_permanent_id} where id = {$res_row['id']}");
               }
               $this->db->delete('po_transaction_data_ans_2'."_{$po_id}", ' crid_permanent= 0 and po_id=' .  $params['id']);
                 $datetime=$this->get_server_date();
                 $this->save_to_action_logs('Upload data row','PO',$datetime,$_SESSION['username'],$params['id']);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              
              //Update number of Completes
              $number_complete = $this->get_number_complete($params['id']);
              $this->db->query("UPDATE po_parameter set complete=complete+{$number_complete} WHERE po_id=". $params['id']);
               
               return $this->success("CSV file has been uploaded.");
           }  
           return $this->success("CSV file has been uploaded.");
           
       }else{
          return $this->failed("Unable to upload result.");
       }
     
       
    }
    
  public function check_if_has_script($po_id)
  {
       
        $sql="select * from po_script_question where po_id=".$po_id;
         
        $po_que=$this->db->fetchAll($sql);  
        
        if(count($po_que)>0){
            return TRUE;
            
        }else{
            return FALSE;
        }
  }
       public function edit_user($params)
    {
        $params = array_map('trim', $params);
        if (!(isset($params['mobile_number'],$params['fname'], $params['lname'], $params['email_address'], $params['username'], $params['user_type']))) {
            return $this->failed('Missing required fields');
            die();
        } elseif (($params['mobile_number'] == '' && $params['fname'] == '' && $params['lname'] == '' && $params['email_address'] == '' && $params['username'] == '' && $params['user_type'] == '')) {
            return $this->failed('Missing required fields');
            die();
        }
         elseif (!strstr($params['email_address'], '@')) {
             return $this->failed('Invalid email address');
             die();
        }
        else 
        {        
            $id = $params['id'];
            $update_data = array(
                'username'      => $params['username'],
               // 'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))),
                'fname'         => $params['fname'],
                'lname'         => $params['lname'],
                'email_address' => $params['email_address'],
                'mobile_number' => $params['mobile_number'],                
                'user_type'     => $params['user_type'],                      
                'is_center'     =>  $params['is_center'],
                'user_center'     =>  $params['user_center'],
                'update_by'=>$_SESSION['username'],
                'update_date'=>date('Y-m-d H:i:s'));          
               
                
             if (!$this->db->update('users', $update_data,'id=' . $id)) 
             { return $this->failed("Unable to update user."); } 
             else 
             {
                $response = array('message' => "User record has been updated.",);
                return $this->success($response);
            }              
        }                   
    }
  public function add_po_script($params){
                     
      $num_of_q = $params['txtQuestion_index'];
      
     
         // $num_of_q= $num_of_q-2; //minus 2 to eliminate na start and end
          $po_id =  $params['txtID'];
          //For Editing
          $sql="select * from po_script_question where po_id=".$po_id;
          $po_que=$this->db->fetchAll($sql);
          foreach ($po_que as $row) {
                  $this->db->delete('po_script_q_response', 'po_script_question_id=' . $row['id']);
           }
           $this->db->delete('po_script_question', 'po_id=' . $po_id);
          //End
       for ($i = 0; $i < $num_of_q; $i++) {
           if($i==0)
           {
               //For Start   
                $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$i, 
                    'question'=>$this->replace_schar($params['txtOpening']), 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );  
           }elseif($i==($num_of_q-1))
           {
               //For End
              $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$num_of_q-1, 
                    'question'=>$this->replace_schar($params['txtEnd']), 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );  
           }else{
               //For Questions
             $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$i, 
                    'question'=>$this->replace_schar($params['txtScript'.$i]), 
                //    'rate'=> $params['txtRate_'.$i], 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );
           }
       
      
      
     

        if (!$this->db->insert('po_script_question', $insert_po_script)) {
            return $this->failed("Unable to add new PO Script");
        } else {
            
            if($i==0)
           {
             
           }elseif($i==($num_of_q-1))
           {
             
           }else{
               
               $temp = array();  
               $temp1 = array();
                       $insert_po_response = array();
                        if($params['txtResponse_'.$i]!="")
                        {                     
                              $row=explode("|",substr($params['txtResponse_'.$i],0,-1));
                                foreach($row as $res_row) {
                                    $temp[] = explode(",",$res_row);;
                                }
                                foreach($temp as $val) {
                                    if($val[2]=="Closing"){
                                        $goto=$num_of_q-1; 
                                    }else{
                                        $goto=$val[2]; 
                                    }

                                    
                                   
                                $insert_po_response[] = array(     
                                        'po_script_question_id'=>$this->db->lastInsertId(), 
                                        'row_no'=>$val[0],  
                                        'response'=>$val[1],  
                                        'rate'=>$val[3],  
                                        'go_to'=>$goto,  
                                        'create_by'=>$_SESSION['username'],
                                         );
                             }
                          
                        if (!$this->db->batchInsert('po_script_q_response', $insert_po_response)) {
                            return $this->failed("Unable to add new PO Script");
                        } else {
                                $value=array(
                                        'id'    =>$po_id,
                                        'status'=> 'PD',
                                );
                            
                            $this->change_po_status($value);
                        }
                            
                            
                        }  
               
               
           }
                    
       
     }
           
    }
    $datetime=$this->get_server_date();
    $this->save_to_action_logs('Added new PO Script.','PO Script',$datetime,$_SESSION['username'],$po_id);
    if(count($po_que)>0){
        //Email notification   
        //Send mail if the script has been modified
        $cmd = "select po_no from po where id ={$po_id}";
        $rs = $this->db->fetchRow($cmd);
        $email = $this->get_email_by_username($_SESSION['username']);
        $body="<br><br>Greetings!<br><br>
        PO Script for PO number {$rs['po_no']} has been modified by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
        foreach($email as $s){
            $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");      
        }
        }
    $response = array(
                    'message' => "PO Script has beed added.",
                );                                                              
            return $this->success($response);    
  }
  
   public function edit_project($params){
       
           $id =$params['txtID'];
            $update_po = array( 
            'name'=>$params['txtPOName'],  
            'project_id_old'=>isset($params['po_list'])? $params['po_list']: -1,  
            'project_template_id'=>$params['po_template_list'],  
            //'client_id'=>$params['client'], 
            'status'   => $params['status'],
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        ); 
        
       /* $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=".$id);
        $no_of_q = $row_no_of_question['number_of_question'];
        if($no_of_q!=$params['number_of_question']){
            return $this->failed("Warning, PBS detected that you've modified the # of questions."); 
        }
              */
        if (!$this->db->update('po', $update_po, 'id='.$id)) {
            return $this->failed("Unable to update project.");
        } else {
            
            $update_po_param = array( 
           //  'name'=>$params['name'], 
             //'client_id'=>$params['client'], 
           //  'contact_id'=>$params['contact_id'],  
             'start_date'=>$params['start_date'],  
             'start_time'=>$params['start_time'],  
             'end_date'=>$params['end_date'], 
             'end_time'=>$params['end_time'], 
             'state'=>$params['state'], 
             'universe'=>$params['universe'],  
             'total_contact'=>$params['total_contact'],  
             'caller_id'=>$params['caller_id'],  
             'goal_type'=>$params['goal_type'],  
             'goal_note'=>$params['goal_note'], 
             'number_of_question'=>$params['number_of_question'], 
             'update_by'=>$_SESSION['username'],
             'update_date'=>date('Y-m-d H:i:s'), 
        ); 
        
            if (!$this->db->update('po_parameter', $update_po_param, 'po_id='.$id)) {         
                return $this->failed("Unable to update project.");
            }else {
                  $update_po_manage = array( 
                        'campaign_note'=>$params['campaign_note'], 
                        'center_note'=>$params['center_note'], 
                        'manager_note'=>$params['manager_note'], 
                        'email_list'=>serialize($params['email_list']), 
                        'allow_download'=>$params['allow_download'], 
                        'allow_report'=>$params['allow_report'], 
                        'allow_grab_report'=>$params['allow_grab_report'], 
                        'allow_blank_question_zero'=>$params['allow_blank_question_zero'], 
                        'allow_disable_logic'=>$params['allow_disable_logic'], 
                        'custom_status'=>$params['custom_status'], 
                        'custom_databack'=>$params['custom_databack'], 
                        'customer_response_map'=>$params['customer_response_map'], 
                        'customer_databack_fields'=> serialize($params['customer_databack_fields']),
                        //'misc_field_databack'=>$params['misc_field_databack'],
                        'status'=>$params['status'], 
                      //  'report_template'=>$params['report_template'], 
                        'manager_id'=>$params['manager_id'], 
                        'update_by'=>$_SESSION['username'],
                        'update_date'=>date('Y-m-d H:i:s'), 
                    ); 
                    
                if (!$this->db->update('po_management', $update_po_manage, 'po_id='.$id)) {
                return $this->failed("Unable to update project.");
                }else {
                
                         //Abby 03-20 Inserting Goal information on po_goal
                    /*$arr_day=array(isset($params['day_checked_1']),isset($params['day_checked_2']),isset($params['day_checked_3']),isset($params['day_checked_4']),isset($params['day_checked_5']),isset($params['day_checked_6']),isset($params['day_checked_7'])) ; 
                    */ 
                    $arr_day = array();       
                    for($x =1; $x<=7; $x++){
                        isset($params['day_checked_'.$x]) ? $arr_day=array_merge($arr_day,array($x)) :  ''; 
                    }
                            
                                             $insert_po_goal = array(
                                                 'po_id'=>$id,
                                                 'goal_type'=>$params['goal_type'],
                                                 'goal_days_of_week'=>implode("-",$arr_day),
                                                 'goal_value'=>$params['goal_value'],
                                                 'update_date'=>date('Y-m-d H:i:s'),
                                                  );    
                                            //Abby 03-20 Inserting Goal information on po_goal
                                        
                        if (!$this->db->update('po_goal', $insert_po_goal,'po_id='.$id)) {
                            return $this->failed("Unable to update project.");
                            //Abby 03-20 Inserting Goal information on po_goal
                   }else{
                       
                   }
                
                  
                    
                }      
            } 
           
            
            $response = array(
                'message' => "PO has been updated.",
            );   
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Edit project order ' .$_POST['txtPOName'] ,'PO',$datetime,$_SESSION['username'],$id);    
            $email = $this->get_email_by_username($_SESSION['username']);
            $allow_report = "";
            if($params['allow_report']==1){
                $allow_report="-View report has been changed to Y";
            }
            $body="<br><br>Greetings!<br><br>
                                PO {$params['txtPOName']} has been modified by {$_SESSION['username']}.<br> {$allow_report} <br> <br> Thanks,<br> PBS Admin";    
                                $cmd = "select po_no from po where id ={$id}";
                                $rs = $this->db->fetchRow($cmd);
                                
                                 foreach($email as $s){
                                     $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]"); 
                                 }
                                
                                                      
            return $this->success($response);
        }         
    }
  
  
   public function get_po_number_of_question($params)
   {
     
      $cmd="select number_of_question
            from po_parameter 
            where  po_id =". $params['id'];
            $result = $this->db->fetchRow($cmd);
        return $this->success( $result['number_of_question']);
   } 
   
    public function get_client_databack_fields($params)
   {
        $cmd="select databack_fields from client c
        inner join po p on c.id = p.client_id
        where p.id = ".$params['id'];
        $result = $this->db->fetchRow($cmd);
        return $this->success( $result['databack_fields']);
   } 
   
    public function get_po_byid($params)
  {
      //Abby 3-31 added goal_value and goal_days_of_week
      $cmd="select p.id, p.name po_name, p.client_id, p.project_id_old, p.project_template_id, 
            p.status as po_status,pp.po_id, pp.name, pp.client_id, pp.contact_id, pp.start_date, pp.start_time, 
            pp.end_date, pp.end_time, pp.state,pp.universe, pp.total_contact, pp.caller_id, pp.goal_type, 
            pp.goal_note, pp.number_of_question,pm.po_id, pm.campaign_note, pm.center_note, pm.manager_note, pm.email_list, pm.allow_download, 
            pm.allow_report, pm.allow_grab_report, pm.allow_blank_question_zero, pm.allow_disable_logic, pm.custom_status,
            pm.custom_databack, pm.customer_response_map, pm.customer_databack_fields,pm.misc_field_databack, 
            pm.status, pm.report_template, pm.manager_id,pg.goal_value,pg.goal_days_of_week
            from po p 
            inner join po_parameter pp on(pp.po_id = p.id)
            inner join po_management  pm on(pm.po_id=p.id)
            left join po_goal pg on(pg.po_id=p.id)
            where  p.status<>'D' and p.id =".$params['id'];
            $record = $this->db->fetchAll($cmd); 
            return $this->success($record);
  }  
    public function delete_po($params)
  {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po', $values, 'id='.$id);
        $response = array(
            'message' => "PO has been deleted.",
        );
        $datetime=$this->get_server_date();
        $this->save_to_action_logs('PO ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'PO',$datetime,$_SESSION['username'],$id);
 
        return $this->success($response);
    }   
   public function get_po($status=false)
   {
       
       $cmd="select pp.complete, pp.universe, pm.allow_report,  p.po_no,DATE_FORMAT(pp.start_date,'%Y/%m/%d') as start_date,DATE_FORMAT(pp.end_date,'%Y/%m/%d') as end_date,   p.id, p.name as po_name,c.client_code,c.name as client_name,pt.name as template_name , p.project_id_old, p.project_template_id,
        p.create_date, p.create_by, p.update_date, p.update_by, 
        ps.name as status
        from po p
        left join po_parameter as pp on(p.id=pp.po_id)
        left join client c on (c.id=p.client_id)
        left join project_status ps on (ps.code = p.status)
        left join project_template pt on (pt.id=p.project_template_id)
        left join po_management pm on(p.id=pm.po_id)
        where p.status <> 'D' and c.status='A'";
        if($_SESSION['client_id']>0){
           $cmd .=" AND p.client_id = {$_SESSION['client_id']}"; 
        }
        if($status=='true'){
          $cmd .=" AND pm.status<>'C'";  
        }
       
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
  
 public function add_project($params){
        $client_code = $this->get_client_code($params['client']);
        $po_no= $this->get_po_counter($params['client']);     
        $po_name =  $params['txtPOName'];
      if($params['po_list']>0){
          $cmd=" INSERT INTO po (SELECT NULL,'".$client_code.sprintf('%06d',$po_no)."', '".$params['txtPOName']."', p.project_id_old, p.project_id, p.project_template_id, p.client_id, 'PD', NULL, '{$_SESSION['username']}', p.update_date, ";
          $cmd .=" '{$_SESSION['username']}' FROM po AS p WHERE p.id={$params['po_list']});";
          if(!$this->db->query($cmd)){
             return $this->failed("Unable to add new project.");   
          }else{
              $new_po_id = $this->db->lastInsertId();
        $cmd  =" INSERT INTO po_parameter"; 
        $cmd .=" (SELECT NULL,{$new_po_id}, name, client_id, contact_id, start_date, start_time, end_date, end_time, ";
        $cmd .=" state, universe, total_contact, caller_id, goal_type, goal_note, number_of_question, status, create_date, ";
        $cmd .=" '{$_SESSION['username']}', update_date, '{$_SESSION['username']}' from po_parameter WHERE po_id={$params['po_list']});";
        if(!$this->db->query($cmd)){
             $this->db->delete('po', 'id=' . $new_po_id);
             return $this->failed("Unable to add new project.");   
          }else{ 
                $cmd =" INSERT INTO po_management"; 
                $cmd .=" (SELECT NULL, {$new_po_id}, campaign_note, center_note,manager_note, email_list, allow_download, allow_report, allow_grab_report, ";
                $cmd .=" allow_blank_question_zero, allow_disable_logic,";
                $cmd .=" custom_status, custom_databack, customer_response_map,"; 
                $cmd .=" customer_databack_fields, misc_field_databack, status, report_template, manager_id, create_date, create_by, update_date, update_by";
                $cmd .=" from po_management where  po_id={$params['po_list']});";
                
             if(!$this->db->query($cmd)){
                $this->db->delete('po', 'id=' . $new_po_id);
                $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                return $this->failed("Unable to add new project.");   
             }else{
                     $old_response_id = $this->db->fetchRow("SELECT id FROM po_script_question WHERE sequence>0  and  po_id={$params['po_list']}");
                  
                      if($old_response_id['id']!=""){
                            //For Scipt
                            $cmd =" INSERT INTO po_script_question";  
                            $cmd .=" (SELECT NULL, {$new_po_id}, sequence, question, rate, status, NULL, create_by, update_date,";  
                            $cmd .=" update_by from po_script_question where po_id={$params['po_list']})"; 
                             if(!$this->db->query($cmd)){
                                 $this->db->delete('po', 'id=' . $new_po_id);
                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                return $this->failed("Unable to add new project.");   
                             }else{
                                                              
                                    $list_of_questions =  $this->db->fetchAll("SELECT * FROM po_script_question WHERE po_id={$new_po_id}");
                                      
                                     foreach( $list_of_questions as $r){
                                     //
                                  
                                       $new_po_script_question_id = $r['id'];   
                                       $cmd =" INSERT INTO po_script_q_response"; 
                                       $cmd .=" (SELECT NULL, {$new_po_script_question_id}, row_no, response, go_to, NULL, create_by, update_date, update_by,rate from po_script_q_response where po_script_question_id={$old_response_id['id']})";
                                     
                                        if(!$this->db->query($cmd)){
                                             $this->db->delete('po', 'id=' . $new_po_id);
                                             $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                             $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                             $this->db->delete('po_script_question', 'po_id=' . $new_po_id);
                                            return $this->failed("Unable to add new project.");   
                                        }else{
                                            $cmd="insert into po_goal"; 
                                            $cmd .=" ( id,po_id, goal_type, goal_value,";
                                            $cmd .="  goal_days_of_week, status, create_by)";
                                            $cmd .=" (select NULL, {$new_po_id}, goal_type, goal_value,"; 
                                            $cmd .=" goal_days_of_week, status, '{$_SESSION['username']}'";
                                            $cmd .=" from po_goal where po_id={$params['po_list']})";
                                            if(!$this->db->query($cmd)){
                                                 $this->db->delete('po', 'id=' . $new_po_id);
                                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_question', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id);
                                               
                                            return $this->failed("Unable to add new project."); 
                                            }
                                        }
                                        
                                        
                                     } 
                                   
                                    //Copy Data
                                       if(isset($params['copy_data'])){
                                           //Get columns white values
                                            $get_column_cmd="select  col_fields from po_transaction_data_2_column where is_column =1 and  po_id =". $params['po_list'] ." limit 1";
                                            $row = $this->db->fetchRow($get_column_cmd);
                                            //Col
                                            $col_fields = explode("|",unserialize($row['col_fields']));
                                            $col_fields_db ="";     
                                            foreach($col_fields as $r){
                                                $col_fields_db.="{$r}," ;   
                                            }
                                            $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);
                                           //
                                           
                                            $cmd =" INSERT INTO po_transaction_data_2(id,po_id,batch_no, is_column, center_id, phone_no, phone_field
, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields, status, list_cnt, create_by
, create_date,{$col_fields_db})";  
                                            $cmd .=" (SELECT NULL, {$new_po_id}, batch_no,"; 
                                            $cmd .=" is_column, center_id, phone_no, phone_field, lname_field,"; 
                                            $cmd .=" fname_field, process_option_id, remarks, col_fields,"; 
                                            $cmd .=" misc_fields, status, list_cnt, '{$_SESSION['username']}', NULL,{$col_fields_db}  ";
                                            $cmd .=" from po_transaction_data_2 where po_id={$params['po_list']})";
                                           
                                            if(!$this->db->query($cmd)){
                                                 $this->db->delete('po', 'id=' . $new_po_id);
                                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                 $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                return $this->failed("Unable to add new project.");   
                                            }else{
                                                    $cmd =" INSERT INTO po_transaction_data_2_column(id,po_id,batch_no, is_column, center_id, phone_no, phone_field
, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields, status, list_cnt, create_by
, create_date,{$col_fields_db})";  
                                                    $cmd .=" (SELECT NULL, {$new_po_id}, batch_no, is_column, center_id,"; 
                                                    $cmd .=" phone_no, phone_field, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields,"; 
                                                    $cmd .=" status, list_cnt, '{$_SESSION['username']}', NULL,{$col_fields_db}  ";
                                                    $cmd .=" from po_transaction_data_2_column where po_id={$new_po_id})";
                                                    if(!$this->db->query($cmd)){
                                                         $this->db->delete('po', 'id=' . $new_po_id);
                                                         $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                         $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                         $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                         $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                         $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                        return $this->failed("Unable to add new project.");   
                                                    }else{
                                                        $cmd =" insert into po_transaction_data_2_summary(id,process_option_id, po_id, batch_no, householded_count, badphone_count, total_count, final_count, dedup_count, create_date) ";  
                                                        $cmd .=" (SELECT NULL, process_option_id, {$new_po_id}, batch_no, householded_count, "; 
                                                        $cmd .=" badphone_count, total_count, final_count, dedup_count,NULL"; 
                                                        $cmd .=" from po_transaction_data_2_summary where po_id={$new_po_id})";
                                                        if(!$this->db->query($cmd)){
                                                             $this->db->delete('po', 'id=' . $new_po_id);
                                                             $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                             $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                             $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_transaction_data_2_column', 'po_id=' . $new_po_id);
                                                            return $this->failed("Unable to add new project.");   
                                                        }else{
                                                             
                                                                $cmd ="  insert into po_transaction_data2_nos";  
                                                                $cmd .=" (SELECT NULL, phone_no, NULL, {$new_po_id}, batch_no "; 
                                                                $cmd .=" from po_transaction_data2_nos where po_id={$params['po_list']})";
                                                                if(!$this->db->query($cmd)){
                                                                     $this->db->delete('po', 'id=' . $new_po_id);
                                                                     $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                                     $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                                     $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_transaction_data_2_summary', 'po_id=' . $new_po_id);
                                                                    return $this->failed("Unable to add new project.");   
                                                                }else{
                                                                    $value=array(
                                                                    'id'    =>$po_id,
                                                                    'status'=> 'A',
                                                                    );

                                                                    $this->change_po_status($value);
                                                                }
                                                            
                                                        }
                                                          
                                                    }
                                                    
                                                
                                            }
                                           
                                       }
                                     //End Copy data
                                     
                                      
                                   
                                   
                                     $params= array(
                                            'client_id'=>$params['client'],
                                            'value'=> $po_no,
                                        );
                                     $this->update_po_counter($params);  
                                     
                                $response = array(
                                    'message' => "New project has been added.",
                                ); 
                                
                            $datetime=$this->get_server_date();
                            $this->save_to_action_logs('Registered new project order ' . $client_code.sprintf('%06d',$po_no) ,'PO',$datetime,$_SESSION['username'],$new_po_id);  
                                           
                        //Email notification           
                        $email = $this->get_email_by_username($_SESSION['username']);
                        $body="<br><br>Greetings!<br><br>
                        PO {$po_name} has been created by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
                        foreach($email as $s){
                            $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#".$client_code.sprintf('%06d',$po_no)."]");
                        }
                        $this->create_po_transaction_tables($new_po_id);
                                return $this->success($response);
                                     
                                    // 
                                    }
                      }else{
                           $params= array(
                                            'client_id'=>$params['client'],
                                            'value'=> $po_no,
                                        );
                                     $this->update_po_counter($params);  
                                $response = array(
                                    'message' => "New project has been added, but an error occured while copying the script.",
                                );                                                              
                                return $this->success($response); 
                        }
                        
             }
                
          }
        
       
       }  
      }else{
        $insert_po = array(
            'po_no'=>$client_code.sprintf('%06d',$po_no), 
            'name'=>$params['txtPOName'],  
            'project_id_old'=>isset($params['po_list'])? $params['po_list']: -1,  
            'project_template_id'=>$params['po_template_list'],  
            'client_id'=>$params['client'], 
            'create_by'=>$_SESSION['username'],
        ); 
         $po_id = -1; 
        if (!$this->db->insert('po', $insert_po)) {
            return $this->failed("Unable to add new project.");
        } else {
              $po_id = $this->db->lastInsertId(); 
            $insert_po_param = array( 
             'po_id'=>$po_id,  
           //  'name'=>$params['name'], 
             'client_id'=>$params['client'], 
            // 'contact_id'=>$params['contact_id'],
             'start_date'=>date('Y-m-d',strtotime($params['start_date'])),  
             'start_time'=>$params['start_time'],  
             'end_date'=>  date('Y-m-d',strtotime($params['end_date'])),
             'end_time'=>$params['end_time'], 
             'state'=>$params['state'], 
             'universe'=>$params['universe'],  
             'total_contact'=>$params['total_contact'],  
             'caller_id'=>$params['caller_id'],  
             'goal_type'=>$params['goal_type'],  
             'goal_note'=>$params['goal_note'], 
             'number_of_question'=>$params['number_of_question'], 
             'create_by'=>$_SESSION['username'],
        ); 
            if (!$this->db->insert('po_parameter', $insert_po_param)) {
                $this->db->delete('po', 'id=' . $po_id);
                return $this->failed("Unable to add new project.");
            }else {
                  $insert_po_manage = array( 
                        'po_id'=>$po_id,
                        'campaign_note'=>$params['campaign_note'], 
                        'center_note'=>$params['center_note'], 
                        'manager_note'=>$params['manager_note'], 
                        'email_list'=>serialize($params['email_list']), 
                        'allow_download'=>isset($params['allow_download'])?$params['allow_download']:1, 
                        'allow_report'=>isset($params['allow_report'])?$params['allow_report']:2, 
                        'allow_grab_report'=>isset($params['allow_grab_report'])?$params['allow_grab_report']:1, 
                        'allow_blank_question_zero'=>isset($params['allow_blank_question_zero'])?$params['allow_blank_question_zero']:1,
                        'allow_disable_logic'=>isset($params['allow_disable_logic'])?$params['allow_disable_logic']:1, 
                        'custom_status'=>isset($params['custom_status'])?$params['custom_status']:"", 
                        'custom_databack'=>isset($params['custom_databack'])?$params['custom_databack']:"", 
                        'customer_response_map'=>isset($params['customer_response_map'])?$params['customer_response_map']:"", 
                        'customer_databack_fields'=>isset($params['customer_databack_fields'])? serialize($params['customer_databack_fields']):serialize(""),
                        //'misc_field_databack'=>$params['misc_field_databack'],
                        'status'=>isset($params['status'])?$params['status']:'N', 
                      //  'report_template'=>$params['report_template'], 
                        'manager_id'=>isset($params['manager_id'])?$params['manager_id']:1, 
                        'create_by'=>$_SESSION['username'],
                    ); 
                    
                if (!$this->db->insert('po_management', $insert_po_manage)) {
                $this->db->delete('po_parameter', 'po_id=' .$po_id);
                return $this->failed("Unable to add new project.");
                }else {
                    //Abby 03-20 Inserting Goal information on po_goal
                   /* $arr_day=array($params['day_checked_1'],$params['day_checked_2'],$params['day_checked_3'],$params['day_checked_4'],$params['day_checked_5'],$params['day_checked_6'],$params['day_checked_7']) ; */
                    
                      $arr_day = array();       
                    for($x =1; $x<=7; $x++){
                        isset($params['day_checked_'.$x]) ? $arr_day=array_merge($arr_day,array($x)) :  ''; 
                    }
                   
                    /*print_r($arr_day);
                    die();*/
                     $insert_po_goal = array(
                         'po_id'=>$po_id,
                         'goal_type'=>$params['goal_type'],
                         'goal_days_of_week'=>implode("-",$arr_day),
                         'goal_value'=>$params['goal_value'],
                          );    
                    //Abby 03-20 Inserting Goal information on po_goal
                        if (!$this->db->insert('po_goal', $insert_po_goal)) {
                            $this->db->delete('po_management', 'po_id=' . $po_id);
                            $this->db->delete('po_parameter', 'po_id=' .$po_id);
                            return $this->failed("Unable to add new project.");
                            //Abby 03-20 Inserting Goal information on po_goal
                   }else{ 
                     $params= array(
                                    'client_id'=>$params['client'],
                                    'value'=> $po_no,
                     );
                     $this->update_po_counter($params);   
                     $datetime=$this->get_server_date();
                    // $this->save_to_action_logs('Registered new project order ' . $client_code.sprintf('%06d',$po_no) ,'PO',$datetime,$_SESSION['username'],$po_id);
               
                    $this->create_po_transaction_tables($po_id);
                }      
            } 
        }
        
             //Email notification           
             $cmd = "select po_no from po where id ={$po_id}";
             $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            PO {$po_name} has been created by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin"; 
            foreach($email as $s){   
                $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");                                                           }
            $response = array(
                'message' => "New project has been added.",
            );                                                              
            return $this->success($response);
        }         
      }
    }
 public function delete_projecttemplate($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('project_template', $values, 'id='.$id);
        $response = array(
            'message' => "Project Template has been deleted.",
        );
        return $this->success($response);
    }   
 public function get_project_template_byid($params)
  {
        
        $record = $this->db->fetchAll("select id, name, client_id, report_type_id, is_auto_call, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status , create_date, create_by, update_date, update_by from project_template  WHERE  id=".$params['id']);
        return $this->success($record);
  }  
  public function get_project_template_code_byid($params)
  {
      
        $record = $this->db->fetchAll("select     id, project_template_id, type, status_code, pay_rate, status, create_date, create_by, update_date, update_by from project_template_code   WHERE project_template_id=".$params['id']);
        return $this->success($record);
  }     
    
public function edit_projecttemplate($params)
{
    $id =$params['txtID'];
    $update_data = array(
        'name'=>$params['txtTemplateName'], 
        'client_id'=>$params['client_name_temp'],
        'report_type_id'=>$params['report_type'],
        'is_auto_call' =>  isset($params['is_auto_call'])=='on'? 1: 0,
        'update_by'                   =>$_SESSION['username'],
        'update_date'                 =>date('Y-m-d H:i:s'), 
    );                      

     if (!$this->db->update('project_template', $update_data,'id=' . $id)) {
        return $this->failed("Unable to update project template.");
    } else {
       $this->db->delete('project_template_code', 'project_template_id=' . $id);
        $cnt=count($_POST['entity_type']);
        $project_template_codes = array(); 
        for ($i = 0; $i < $cnt; $i++) {
        $project_template_codes[] = array(
                    'project_template_id'=>$id,
                    'status_code'=>$_POST['txtStatus'][$i],
                    'type'=>$_POST['entity_type'][$i],
                    'pay_rate'=>$_POST['txtPayRate'][$i],
                    'create_by'=>$_SESSION['username'],
                );
        }
       
               if (!$this->db->batchInsert('project_template_code', $project_template_codes)) {
                    return $this->failed('Unable to save project template code.');
               }
        
        
        $response = array(
            'message' => "Project Template has been updated.",
        );

        return $this->success($response);
    }           
} 

 public function add_projecttemplate($params){
        
      
        $insert_data = array(
            'name'=>$params['txtTemplateName'], 
            'client_id'=>$params['client_name_temp'],
            'report_type_id'=>$params['report_type'],
            'is_auto_call' =>  isset($params['is_auto_call'])=='on'? 1: 0,

             
        );                     
      
        if (!$this->db->insert('project_template', $insert_data)) {
            return $this->failed("Unable to add new project template.");
        } else {
           
            
            
            
        $template_id=$this->db->lastInsertId();
        $cnt=count($_POST['entity_type']);
        $project_template_codes = array(); 
        for ($i = 0; $i < $cnt; $i++) {
        $project_template_codes[] = array(
                    'project_template_id'=>$template_id,
                    'status_code'=>$_POST['txtStatus'][$i],
                    'type'=>$_POST['entity_type'][$i],
                    'pay_rate'=>$_POST['txtPayRate'][$i],
                    'create_by'=>$_SESSION['username'],
                );
        }
       
               if (!$this->db->batchInsert('project_template_code', $project_template_codes)) {
                    return $this->failed('Unable to save project template code.');
               }
            $response = array(
                'message' => "New project template has been added.",
            );                                                              
            return $this->success($response);
        }         
    }
    
  public function get_project_template()
    {
        $cmd="select pt.id, pt.name as template_name, pt.client_id, pt.report_type_id,rt.name as report_type,CASE pt.is_auto_call WHEN 1 THEN 'YES' WHEN '0' THEN 'NO'  END as is_auto_call, 
            CASE pt.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,pt.create_date, pt.create_by, pt.update_date, pt.update_by,
            c.name as client_name
            from project_template pt
            inner join client  c on pt.client_id = c.id
            inner join report_type rt on pt.report_type_id = rt.id 
            where pt.status='A' AND pt.client_id={$_SESSION['client_id']}";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
   
        
   public function delete_client($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('client', $values, 'id='.$id);
        $response = array(
            'message' => "Client has been deleted.",
        );
        return $this->success($response);
    }
    
    public function delete_user($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
                        
                      
                        
        $this->db->update('users', $values, 'id='.$id);
        $response = array(
            'message' => "User has been deleted.",
        );
        return $this->success($response);
    }
    
    
   public function get_client()
    {
        $cmd="select id,client_code,name,last_po_no,street,city,state,zip,phone,fax,notes,default_report_status,
        databack_status,basket_notification_status,databack_fields, 
        allow_co_fields,co_notification_status,schedule_notification_status,schedule_access,show_discount_code,
        client_logo,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,create_date,create_by,update_date,update_by
        from client  where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    public function get_client_list($id=0)
    {
        if($id==0){
            $cmd="select id,name from client where status = 'A'";
        }else{
                $cmd="select id,name from client where status = 'A' and id = {$id}";       
           
        }
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_center_list()
    {
        $cmd="select id,name from call_center  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);    
        return $this->success($result);
    }
    public function get_center_list_user()
    {
        $cmd="select id,name from call_center  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);    
        return $result;
    }
     public function get_contact_list()
    {
        $cmd="select id, concat(first_name,last_name) as name from contact   where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_goal_type()
    {
        $cmd="select  id,name, create_date, create_by,update_date,update_by from goal_type ";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
     public function get_states_list()
    {
        $cmd="select id, name from states";
       
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_po_list()
    {
        $cmd="select id,name from po  where status = 'A'";
       
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
    public function get_po_template_list()
    {
        $cmd="select id,name from project_template  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
     public function get_report_type_list()
    {
        $cmd="select id,name from report_type  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
  public function get_client_byid($params)
  {
        $record = $this->db->fetchAll("select id,client_code,name,last_po_no,street,city,state,zip,phone,fax,notes,default_report_status,
                databack_status,basket_notification_status,databack_fields, 
                allow_co_fields,co_notification_status,schedule_notification_status,schedule_access,show_discount_code,
                client_logo,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,create_date,create_by,update_date,update_by
                from client  WHERE id=".$params['id']);
        return $this->success($record);
  }  
   public function edit_client($params)
   {
        $id =$params['txtID'];
        $update_data = array(
            'name'                        =>$params['txtClientName'], 
            'client_code'                 =>$params['txtClientCode'],
            'last_po_no'                  =>$params['txtLastPO'], 
            'street'                      =>$params['txtAddress'], 
            'city'                        =>$params['txtCity'], 
            'state'                       =>$params['txtState'], 
            'zip'                         =>$params['txtZipCode'], 
            'phone'                       =>$params['txtPhone'], 
            'fax'                         =>$params['txtFax'], 
            'notes'                       =>$params['txtNote'], 
            'default_report_status'       =>$params['report_ready'], 
            'databack_status'             =>$params['data_back'], 
            'basket_notification_status'  =>$params['data_basket_notif'], 
            'databack_fields'             =>serialize($params['data_fields']), 
            'allow_co_fields'             =>serialize($params['allow_fields']),
            'co_notification_status'      =>$params['co_notif'],
            'schedule_notification_status'=>$params['sched_notif'], 
            'schedule_access'             =>$params['sched_access'], 
            'show_discount_code'          =>$params['discount_code'],
            'client_logo'                 =>'C:', 
            'status'                      =>'A', 
            'update_by'                   =>$_SESSION['username'],
            'update_date'                 =>date('Y-m-d H:i:s'), 
    );                      
       
         if (!$this->db->update('client', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update client.");
        } else {
            $response = array(
                'message' => "Client has been updated.",
            );

            return $this->success($response);
        }           
    } 
   public function register_client($params){
       $u_param = array(
            'username'=>$params['txtClientUsername'],
       );
        if ($this->check_user_if_exist($u_param)) {
            return $this->failed('Username already exist.');
            die();
        } else{
                
            
     
       
        $insert_data = array(
            'name'                        =>$params['txtClientName'], 
            'client_code'                 =>$params['txtClientCode'],
            'last_po_no'                  =>$params['txtLastPO'], 
            'street'                      =>$params['txtAddress'], 
            'city'                        =>$params['txtCity'], 
            'state'                       =>$params['txtState'], 
            'zip'                         =>$params['txtZipCode'], 
            'phone'                       =>$params['txtPhone'], 
            'fax'                         =>$params['txtFax'], 
            'notes'                        =>$params['txtNote'], 
            'default_report_status'       =>$params['report_ready'], 
            'databack_status'             =>$params['data_back'], 
            'basket_notification_status'  =>$params['data_basket_notif'], 
            'databack_fields'             =>serialize($params['data_fields']), 
            'allow_co_fields'             =>serialize($params['allow_fields']),
            'co_notification_status'      =>$params['co_notif'],
            'schedule_notification_status'=>$params['sched_notif'], 
            'schedule_access'             =>$params['sched_access'], 
            'show_discount_code'          =>$params['discount_code'],
            'client_logo'                 =>'C:', 
            'status'                      =>'A', 
            'create_by'                   =>$_SESSION['username'], 
        );                     
       
        if (!$this->db->insert('client', $insert_data)) {
            return $this->failed("Unable to add new client.");
        } else {
                $client_id = $this->db->lastInsertId();
                 $insert_data = array(
                    'fname'         => $params['txtClientName'],
                    'lname'         => $params['txtClientName'],
                    'username'      => $params['txtClientUsername'],
                    'user_type'     => 3,
                    'is_client'     => 1,
                    'client_id'     => $client_id,
                    'created_by'    => $_SESSION['username'],
                    'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['txtClientPassword'], sha1(md5($this->salt))),  
                );
           $this->db->insert('users', $insert_data);
           $response = array(
                'message' => "New client has been added.",
            );                                                              
            return $this->success($response);
        } 
       }        
    }
    
   public function delete_callcenter($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('call_center', $values, 'id='.$id);
        $response = array(
            'message' => "Contact has been deleted.",
        );
        return $this->success($response);
    }   
    
 
  public function delete_contact($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('contact', $values, 'id='.$id);
        $response = array(
            'message' => "Contact has been deleted.",
        );
        return $this->success($response);
    }   
    
  
    public function edit_contact($params){
        $params = array_map('trim', $params);
        $id =$params['txtID'];
        $params = array_map('trim', $params);
             $update_data = array(
                    'first_name'=>$params['txtFirstName'], 
                    'last_name' =>$params['txtLastName'],
                    'city'=>$params['txtCity'],
                    'state'=>$params['txtState'],
                    'zip'=>$params['txtZipCode'],  
                    'phone'=>$params['txtPhone'],
                    'ext'=>$params['txtExt'],
                    'fax'=>$params['txtFax'],
                    'email'=>$params['txtEmail'],
                    'login_id'=>$params['txtUsername'],
                    'password' =>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtPassword'], sha1(md5($this->salt))),
                    'notes'=>$params['txtNote'],
                    'status'=>'A',
                    'update_by'=>$_SESSION['username'],
                    'update_date'=>date('Y-m-d H:i:s'), 
        );                      
       
         if (!$this->db->update('contact', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update contact.");
        } else {
            $response = array(
                'message' => "Contact has been updated.",
            );

            return $this->success($response);
        }           
    }
    
 
     public function get_contact()
    {
        $cmd="select id, client_id, login_id, password, first_name, last_name, street, city, state, zip, phone, ext, fax, email, 
              notes,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status, create_date, create_by, update_date, update_by from contact where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }

  public function get_contact_byid($params)
  {
        
        $record = $this->db->fetchAll("select     id, client_id, login_id, password, first_name, last_name, street, city, state, zip, phone, ext, fax, email, 
                                        notes,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status, create_date, create_by, update_date, update_by from contact  WHERE id=".$params['id']);
        
        return $this->success($record);
  } 
 
   public function register_contact($params){
        
        $params = array_map('trim', $params);
        $insert_data = array(
            'first_name'=>$params['txtFirstName'], 
            'last_name' =>$params['txtLastName'],
            'city'=>$params['txtCity'],
            'state'=>$params['txtState'],
            'zip'=>$params['txtZipCode'],  
            'phone'=>$params['txtPhone'],
            'ext'=>$params['txtExt'],
            'fax'=>$params['txtFax'],
            'email'=>$params['txtEmail'],
            'login_id'=>$params['txtUsername'],
            'password' =>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtPassword'], sha1(md5($this->salt))),
            'notes'=>$params['txtNote'],
            'status'=>'A',
            'create_by'=>$_SESSION['username'],
        );                     
       
        if (!$this->db->insert('contact', $insert_data)) {
            return $this->failed("Unable to add new contact.");
        } else {
           
            $response = array(
                'message' => "New contact has been added.",
            );                                                              
           
            
            return $this->success($response);
        }         
    }
    
  
    
   public function register_callcanter($params){
        
        $params = array_map('trim', $params);
        $insert_data = array(
            'name'=>$params['txtCallCenterName'], 
            'center_code'=>$params['txtCallCenterCode'],  
            'ftp_site'=>$params['txtFTPSite'],   
            'ftp_login'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPLogin'], sha1(md5($this->salt))),  
            'ftp_password'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPPassword'], sha1(md5($this->salt))),   
            'status'=>'A', 
            'create_by'=>$_SESSION['username'], 

        );                     
       
        if (!$this->db->insert('call_center', $insert_data)) {
            return $this->failed("Unable to add new call center.");
        } else {
           
            $response = array(
                'message' => "New call center has been added.",
            );                                                              
            return $this->success($response);
        }         
    }
   public function edit_callcanter($params){
        $params = array_map('trim', $params);
        $id =$params['txtID'];
        $params = array_map('trim', $params);
            $update_data = array(
                'name'=>$params['txtCallCenterName'], 
                'center_code'=>$params['txtCallCenterCode'],  
                'ftp_site'=>$params['txtFTPSite'],   
                'ftp_login'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPLogin'], sha1(md5($this->salt))),  
                'ftp_password'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPPassword'], sha1(md5($this->salt))),   
                'status'=>'A', 
                'update_by'=>$_SESSION['username'],
                'update_date'=>date('Y-m-d H:i:s'), 

        
            );                     
       
         if (!$this->db->update('call_center', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update call center.");
        } else {
            $response = array(
                'message' => "Call Center has been updated.",
            );
/*         Add Action Logs*/
            return $this->success($response);
        }           
    }
    //PP 2015/07/27
    public function get_callcenter()
    {
        $cmd="select id, name, center_code, ftp_site,ftp_login,ftp_password, create_date, create_by, update_date, update_by, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status from call_center  where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    public function get_callcenter_byid($params)
  {
        
        $record = $this->db->fetchAll("select id, name, center_code, ftp_site,ftp_login,ftp_password, create_date, create_by, update_date, update_by, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status from call_center WHERE id=".$params['id']);
        return $this->success($record);
  }  

    public function get_user_types_exclude_client() {
        $user_types = $this->db->fetchPairs('select id,description from user_types where id<>3');
        return $user_types;
    }
    
    public function get_user_types_selected($user_type_id) {
        $user_types = $this->db->fetchPairs('select id,description from user_types where created_by='.$user_type_id);
        return $user_types;
    }
    
    public function get_user_type_by_username($username){
        $sql = "SELECT ut.* FROm users u LEFT JOIN user_types ut ON u.user_type_id=ut.id WHERE u.username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchAll($sql);
        return $this->success($result);  
    }
    
    public function get_user_type_by_username_ex($username){
        
        $result = $this->db->fetchRow("SELECT ut.description FROm users u LEFT JOIN user_types ut ON u.user_type=ut.id WHERE u.username='{$username}'");
        return $result['description'];  
    }
    
    public function get_user_info($params){
        $username = $params['username'];
        $return_value = $params['return_value'];
        
        $sql = "select * from users  WHERE username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchRow($sql);
        return $result[$return_value];  
    }


    
    
    
    //=============LOOKUP FUNCTIONS=====================    
    
    public function validate_session($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']) && strlen($params['session_id']) == 32)) {
            return $this->failed('Invalid or expired session id');
        } else {
            $chk = $this->db->fetchRow('select * from sessions where session_id='. $this->db->db_escape($params['session_id']));
            if (!(isset($chk['session_id']))) {
                return $this->failed('Invalid or expired session id');
            } elseif (isset($chk['logout_date']) && $chk['logout_date'] != '') {
                return $this->failed('Invalid or expired session id');
            //} elseif (strtotime($chk['last_seen']) + (3600) < time()) {
            } elseif (strtotime($chk['last_seen']) + (600) < time()) {
            //} elseif (strtotime($chk['last_seen']) + (30) < time()) {
                return $this->failed('Invalid or expired session id');
            } else {
                $values = array("last_seen" => date('Y-m-d H:i:s'));
                $this->db->update('sessions', $values, 'session_id=' . $this->db->db_escape($params['session_id']));
                //return $this->success('session id valid');
				$user_info = unserialize($chk['session_data']);
				return $this->success($user_info);
            }
        }
    }
    //params: username, old_pw, new_pw
    public function change_password($params) {
        $params = array_map('trim', $params);
        if (!isset($params['username'], $params['old_pw'], $params['new_pw'])) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['username'] != '' && $params['old_pw'] !='' && $params['new_pw'] != '')) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['old_pw'] != $params['new_pw'])) {
            return $this->failed('Old and new password should not be the same');
        } else {
            $user_check = $this->db->fetchRow('select * from users where username='. $this->db->db_escape($params['username']) .' and password=' . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['old_pw'], sha1(md5($this->salt)))));
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid old password');
            } else {
            	
				//$this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))));
				
                $update_data = array(
                    'password' => $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_pw'], sha1(md5($this->salt))),
                );
                if (!($this->db->update('users', $update_data, 'id=' . $user_check['id']))) {
                    return $this->failed('Unable to change user password');
                } else {
                    //return $this->failed('Successfully changed user password');
                    return $this->success('Successfully changed user password');
                }
            }
        }
    }
    //params: user_id
    public function update_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['user_id'],$params['fname'],$params['lname'], $params['email_address'], $params['mobile_number'], $params['address'], $params['user_type']))) {
            return $this->failed('Missing required fields');
        }else if($params['mobile_number']==""){
            return $this->failed('Missing required mobile number');
        } else {
            $user_check = $this->db->fetchRow('select * from users where id='. $this->db->db_escape($params['user_id']));
            if (!(isset($user_check['id'])))  {
                return $this->failed('User does not exist');
            } else {
                //echo $params['user_id'];
                $update_data = array(
                    'fname' => $params['fname'],
                    'lname' => $params['lname'],
                    'email_address' => $params['email_address'],
                    'mobile_number' => $params['mobile_number'],
                    'address' => $params['address'],
                    'user_type' => $params['user_type'],
                    'status' => $params['status'],
                );
                
                if (!($this->db->update('users', $update_data, 'id=' . $user_check['id']))) {
                    return $this->failed('Unable to change user information');
                } else {
                    //return $this->failed('Successfully changed user password');
                    return $this->success('Successfully changed user information');
                }
                
            }
        }
    
    }
    public function get_user_by_id($params) 
    {
        $query = "select u.user_center, u.is_center, CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type WHERE u.id = ".$params['id'];

        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
 /*   //params: user_id
    public function delete_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['user_id']) && is_numeric($params['user_id']) && $params['user_id'] > 0)) {
            return $this->failed('Invalid user ID');
        } else {
            $user_check = $this->db->fetchRow('select * from users where id='. $this->db->db_escape($params['user_id']));
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
               // if ($this->get_users_customers($user_check['username'])) {
                 //   return $this->failed('There are existing customers registered under this agent/user.');
               // } else {
                    $this->db->delete('users', 'id=' . $params['user_id']);
                    return $this->success('Successfully deleted user');
                //}
            }
        }
    }  */
    
    public function get_users_detailed_selected($user_type_id) {
        $query = 'select u.status, u.id,fname,lname,username,u.email_address,mobile_number,user_type_id,ut.description ' ;                $query .= 'user_type_desc from users u ';
        $query .= 'inner join user_types ut on ut.id=u.user_type_id ';
        $query .= 'where ut.created_by=' . $user_type_id;
/*        print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_list()
    {
        $query="SELECT id,username,password,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'I' THEN 'INACTIVE' ELSE 'PENDING' END as status";
        $query.=",date_created,created_by,fname";
        $query.=",lname,email_address,mobile_number,address,user_type,customers_id,agent_id FROM users";
        $users= $this->db->fetchAll($query);
        $this->success($users); 
        $response = $this->get_response(); 
        return    $response;
    }
    
    

   
    public function generate_password() {
        return substr(md5(uniqid(time(), true)), 5,8);
    }

    
    
    public function reset_password_notification($params)
    {
        require_once 'class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->isSendmail();//Set who the message is to be sent from
        $mail->From = 'flashcash@omniflashcash.com';
        $mail->FromName = 'FlashCash Reset Password';
        $mail->addAddress($params['email_address']);
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject = 'FlashCash: Your auto-generated password';
        $mail->Body    = 'Username: <b>' . $params['username'] . '</b>, New Password: <b>' . $params['generated_password'].'</b>';
        if (!$mail->send()) {
            return $this->failed('Unable to send email notification');
        } else {
            return $this->success('Message sent');
        } 
    }

    public function reset_user_password($params)
    {   
        $params = array_map('trim', $params);
        if (!(isset($params['user_id'],$params['username'],$params['email_address']))) {
            return $this->failed('Missing required fields');
        }elseif (!($params['user_id'] != ''))
        {
            return $this->failed('Missing required fields');
        }else{
            $generated_password = $this->generate_password();
            $update_data = array( 'password' => md5($this->salt . $generated_password . $this->salt));
            if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
                return $this->failed('Unable to reset your password');
            }else
            {
               $notify_param =array('email_address' => $params['email_address'],'username' => $params['username'],'generated_password' => $generated_password);
               $this->reset_password_notification($notify_param);
               return $this->success('Successfully reset password');
            }
        }
    }
    
                                 
    
     
    protected function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } else {
                    if (file_exists($dst . '/' . $file)) @unlink($dst . '/' . $file);
                    copy($src . '/' . $file, $dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }
         
    
                                         

    //params: username, password
    public function login($params) {
        $params = array_map('trim', $params);
        

        
        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
            
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				$profile = '';
				if($user_check['is_iso'])
				{
					$profile = 'iso';
				}elseif($user_check['is_merchant']){
					$profile = 'merchants';
				}elseif ($user_check['is_agent']){
					$profile = 'agents';
				}else{
					$profile = 'customers';
				}				
				
				$id = $user_check['reference_id'];
				$user_info = $this->get_profile($profile, $id);
				
				$user_info['profile'] = $profile;
				$user_info['is_agent'] = $user_check['is_agent'];
				//$user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                );
                
                return $this->success($response);
            }
        }
    }
    

    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = $this->db->fetchRow('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
	
	
    
    protected function create_session($user_id, $session_data = "") {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
            //'session_data' => $session_data,
        );
		if(is_array($session_data))
		{
			$insert_data['session_data'] = serialize($session_data);
		}else
		{
			$insert_data['session_data'] =  $session_data;
		}
		
        $this->db->insert('sessions', $insert_data);
        return $session_id;        
    }
    

    
    //user_id
    public function get_user_permissions($user_id, $user_type_id) {        
        $permissions = $this->db->fetchPairs('select p.id,r.resource from permissions p inner join resources r on p.resource_id=r.id where p.user_id=' . $this->db->db_escape($user_id));

        
        $permissions2 = $this->db->fetchPairs('select ut.id,r.resource from user_templates ut inner join resources r on ut.resource_id=r.id where ut.user_type_id=' . $this->db->db_escape($user_type_id));
        $permissions = array_flip(array_merge(array_flip($permissions), array_flip($permissions2)));
        

        if (is_array($permissions) && count($permissions) > 0) {
            return $permissions;
        }
    }

    

    
    

    //params: session_id
    public function logout($params) {//note: session maintenance is not yet implemented in backend.php, client must implement its own
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']))) {
            return $this->failed('Invalid session ID');
        } else {
            $check = $this->db->fetchRow('select * from sessions where logout_date is null and session_id=' . $this->db->db_escape($params['session_id']));
            if (!(isset($check['id']))) {
                return $this->failed('Invalid session ID');
            } else {
                $update_data = array(
                    'logout_date' => date('Y-m-d H:i:s'),
                );
                if (!$this->db->update('sessions', $update_data, 'id='. $check['id'])) {
                    return $this->failed('Unable to logout');
                } else {
                    return $this->success('Successfully logged out user.');
                }
            }
        }
    }

    
    protected function failed($message) {
        $this->response = array(
            'ResponseCode' => '0001',
            'ResponseMessage' => $message,
        );
        return false;
    }
    
    protected function success($message, $record_count=-1) {
        $this->response = array(
            'ResponseCode' => '0000',
            'ResponseMessage' => $message,
        );
        return true;
    }
    
    public function get_response() {
        return $this->response;
    }
    

    
    
    //params: username
     public function get_user_id($username) {
            $record = $this->db->fetchRow('select agent_id from users where username=' . $this->db->db_escape($username));
            if (!isset($record['agent_id'])) {
                return $this->failed('Unable to retrieve userId');
            } else {
                return $record['agent_id'];
            }
        
    }
    
   
     //params: description(user type)
       public function get_user_types_ByDesc($desc) {
            $record = $this->db->fetchRow('select id from user_types where description=' . $this->db->db_escape($desc));
            if (!isset($record['id'])) {
                return $this->failed('Unable to retrieve id');
            } else {
                return $record['id'];
            }
        
    }


      

    //params: payroll_cycle, loan_terms
    public function get_loan_term_calendar_value($payroll_cycle, $loan_term) {
            $sql = 'select value from loan_term_calendar where payroll_cycle=' . $this->db->db_escape($payroll_cycle);
            $sql .= " and loan_terms=" . $loan_term;
            $record = $this->db->fetchRow($sql);
            if (!isset($record['value'])) {
                return $this->failed('Unable to retrieve id');
            } else {
                return $record['value'];
            }
        
    }

      

    

    
    public function get_server_date() {
        $result = $this->db->fetchRow('SELECT NOW() AS server_date');
        return $result['server_date'];
    } 
    
  

    
    
    
    //params: fname, lname, email_address, mobile_number, address, username, user_type
    public function register_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['mobile_number'],$params['fname'], $params['lname'], $params['email_address'], $params['username'], $params['user_type'], $params['created_by']))) {
            return $this->failed('Missing required fields');
            die();
        } elseif (($params['mobile_number'] == '' && $params['fname'] == '' && $params['lname'] == '' && $params['email_address'] == '' && $params['username'] == '' && $params['user_type'] == '' && $params['created_by'] == '')) {
            return $this->failed('Missing required fields');
            die();
        }
         elseif (!strstr($params['email_address'], '@')) {
             return $this->failed('Invalid email address');
             die();
        }
       /* elseif (!($this->get_userid_by_email($params['email_address'])==-1)) {
            return $this->failed('Email has already been used.');
            die();
       
        }*/
        else {
            if ($this->get_user_details($params)) {

                return $this->failed('Username already exist');
                die();
            } else {
                
     
                $insert_data = array(
                    'fname'         => $params['fname'],
                    'lname'         => $params['lname'],
                    'email_address' => $params['email_address'],
                    'mobile_number' => $params['mobile_number'],
                    'username'      => $params['username'],
                    'user_type'     => $params['user_type'],
                    'created_by'    => $params['created_by'],
                    'is_center'     =>  $params['is_center'],
                    'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))),  
                );
               
               
                if (!$this->db->insert('users', $insert_data)) {
                    return $this->failed('Unable to register new user');
                } else {
                    $response = array(
                        'message' => 'Successfully registered new user record',
                    );
                    
                    return $this->success($response);
                }
            }
        }
    }
    
    public function get_user_details($params) {
        $params = array_map('trim', $params);
        
        if (!(isset($params['username']) && trim($params['username']) != '')) {
         
            return $this->failed('Missing required parameter');
        } else {
            $row = $this->db->fetchRow("select * from users where status = 'A'  and  username=" . $this->db->db_escape($params['username']));
            if (!(isset($row['id']))) {
                return $this->failed('User account not found');
            } else {
                 
                return $this->success($row);
            }
        }
    }
    
    public function get_user_information($sessionid) 
    {
        $query = "SELECT u.* FROM users u
        LEFT JOIN sessions s ON u.id = s.user_id
        WHERE s.session_id = ".$this->db->db_escape($sessionid);
        $merchant = $this->db->fetchAll($query);
        return $this->success($merchant);
    }
    
     // ACL
    public function get_module_name($module_list = array(), $include_action = false)
    {
        if(count($module_list) > 0) {
            $return_arr = array();
            $query = "SELECT * FROM acl_modules WHERE id IN(".implode(',', $module_list).") AND main = 1";
            $module_list = $this->db->fetchAll($query);

            foreach($module_list as $module) {
                $return_arr[] = $module['module_class'];
                $return_arr[] = $module['main_module_index'];
                if($include_action)
                    $return_arr[] = $module['module_action'];
            }

            $result = array_unique($return_arr);
            return $result;
        } else {
            return FALSE;
        }
    }

    public function remove_module_buttons($class)
    {
        $module_btn_return = array();

        $all_module_btn = $this->db->fetchAll('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND with_button = 1');
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];
        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_access = $this->db->fetchRow($query);
        $module_access = explode(',', $module_access['module_access']);

        if($all_module_btn && $module_access) {
            foreach($all_module_btn as $btn) {
                if(in_array($btn['id'], $module_access)) {
                    $module_btn_return[$btn['module_action']] = '';
                } else {
                    $module_btn_return[$btn['module_action']] = 'display: none;';
                }
            }
        }

        return $module_btn_return;
    }

    public function get_all_modules()
    {
        $return_arr = array();
        $query = "SELECT * FROM acl_modules"; // check if it will be a 1d array if i just get the id instead of all
        $all_modules = $this->db->fetchAll($query);

        foreach($all_modules as $module)
            $return_arr[] = $module['id'];

        return $return_arr;
    }

    public function get_user_modules()
    {
        $user_info = $this->get_user_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['user_type'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            $module_list = explode(',', $module_list['module_access']);
            $all_modules = $this->get_all_modules();

            $unset_modules = array_diff($all_modules, $module_list);

            if(count($unset_modules) > 0)
                return $unset_modules;
            else
                return FALSE;
        }
    }

    public function get_user_access_list()
    {
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            return explode(',', $module_list['module_access']);
        } else {
            return array();
        }
    }

    public function get_module_id($class, $action)
    {
        $module_info = $this->db->fetchRow('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND module_action = '.$this->db->db_escape($action));

        if(!$module_info)
            return FALSE;

        $module_id = $module_info['id'];
        $user_list = $this->get_user_access_list();

        if(in_array($module_id, $user_list)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    
     public function get_pan_key() {                                                     
        //$query = $this->db->select('key')->from('keys')->order_by('timestamp', 'desc')->get();
        $query = $this->db->fetchRow("SELECT `key` FROM `keys` ORDER by `timestamp` desc");
        if (!$query) {
            //$key = $query->result_array();
            $dek_enc = $query['key'];
            $iv = sha1('aes-256-cbc');
            //$kek = file_get_contents('http://192.168.2.16/web/i.php?token=cfbe176207b80774e8911c10893f5a0f');
            $kek="cfbe176207b80774e8911c10893f5a0f";
            $dek_plain = $this->hideit->decrypt($kek, $dek_enc, $iv);
            return $dek_plain;
        } else {
            return '';
        }
    }  
                    
    
    
}
