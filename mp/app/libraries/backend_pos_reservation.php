<?php
class backend {
    protected $response;
    protected $db;
    public $salt;

    public function __construct(&$db) {
        $this->salt = PROGRAM;  
        $this->salt2 = '2016'.PROGRAM.'2016';
        $this->db = $db;
        require_once 'xorry.php';
        require_once 'class.phpmailer.php';
        require_once dirname(__DIR__) . '/libraries/HideIt.php';
        $this->hideit = new HideIt('aes-256-cbc');
		error_reporting(E_ALL);
    }

	private function send_sms($message, $mobile_number)
	{
		$params = array(
            'user'      => 'GO3INFOTECH',
            'password'  => 'TA0828g3i',
            'sender'    => 'Go3Solutions',
            'SMSText'   => $message,
            'GSM'       => $mobile_number,
        );
        $send_url = 'https://api2.infobip.com/api/v3/sendsms/plain?' . http_build_query($params);

        $send_response = file_get_contents($send_url);
        if (!($send_response != '')) {
            return  array(
                'success' => FALSE,
                'message' => 'No Response',
            );
        } else {
            if (strstr($send_response, '<status>0</status>') === false) {
                return array(
                    'success' => FALSE,
                    'message' => 'Failed:' . $send_response,
                );
            } else {
                return array(
                    'success' => true,
                    'message' => 'Success:' . $send_response,
                );
            }
        }
	}

	// } elseif (strtotime($chk['last_seen']) + (3600) < time()) {
	//contact_number, verfication_code, new_password
	public function reset_password($params)
	{
		$cmd="SELECT id FROM customer WHERE status='A' AND mobile_number='{$params['contact_number']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Contact number cannot be found.");
		}
		
		$id =  $res['id'];
		
		$cmd =" SELECT create_date FROM customer_verification  WHERE customer_id={$id} AND status='A' AND passcode='{$params['verification_code']}'";
		
		$chk= $this->db->fetchRow($cmd);
		
		if (strtotime($chk['create_date']) + (3600) < time()) {
			return $this->failed("Your verification code already expires.");
		}
		
		$new_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_password'], sha1(md5($this->salt)));  
		
		$update_user = array(
			'password' => $new_password
		);
		
		
		if(!$this->db->update('users', $update_user, 'reference_id='. $id. " AND status='A' AND user_type_id=3"))
		{
            // $this->db->rollback_transaction();	
            $datetime = date('Y-m-d H:i:s');
			$this->save_to_action_logs('Error resetting password '  ,'ERROR',$datetime,"API",'');
            return $this->failed("Unable to reset password.");
		}
		return $this->success("Your password has been reset.");
	}
		

	//contact_number
	public function forgot_password($params)
	{
		$cmd="SELECT id FROM customer WHERE status='A' AND mobile_number='{$params['contact_number']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Contact number cannot be found.");
		}else{
			$id = $res['id'];
			$passcode = $this->generate_passcode();
			//add verification code to table
			$insert_data = array(
				'customer_id' => $id,
				'passcode' => $passcode,
				'status' => 'A',
				'create_date' => date('Y-m-d H:i:s'),
			);
			
			// var_dump(IS_PRODUCTION);
			// die();
			
			if(IS_PRODUCTION)
			{
				$insert_data['is_sms'] = 1;
			}
			
			if(!$this->db->insert("customer_verification", $insert_data))
			{
				return $this->failed("Unable to generate verification code.");
			}
			
			//send verification code to sms
			if(IS_PRODUCTION)
			{
				$this->send_sms("Your verification code is {$passcode}", $params['contact_number']);
				return $this->success("Forgot password verification code has been sent on your mobile.");
			}else{
				return $this->success("Forgot password verification code is {$passcode}.");
			}
			
		} 	
	}
	
	protected function generate_passcode() {
        do {
            $passcode = mt_rand(9999, 99999999);
            $check = $this->db->fetchRow('select id from customer_verification where passcode="' . $passcode .'"');
        } while (isset($check['id']));
        return $passcode;
    }
	

	//customer_id, merchant_id
	public function check_if_favorite($params)
	{
		$cmd =" SELECT id FROM customer_favorite WHERE customer_id={$params['customer_id']} AND merchant_id={$params['merchant_id']} AND status='A'";
		
		$res = $this->db->fetchRow($cmd);
		
		if(count($res) == 0)
		{
			return $this->success(array("favorite_id" => $res['id']));
		}else
		{
			return $this->success(array("favorite_id" => $res['id']));
		}
	}	

	//customer_id
	public function list_my_favorite($params)
	{
		$cmd =" SELECT cf.id as favorite_id, m.* FROM customer_favorite cf INNER JOIN merchant m ON cf.merchant_id=m.id WHERE cf.customer_id={$params['customer_id']} AND cf.status='A'";
		
		$results = $this->db->fetchAll($cmd);
		return $this->success($results);
	}
	
	//id, customer_id, update_by, update_date
	public function remove_as_favorite($params)
	{
		$cmd =" SELECT id FROM  customer_favorite WHERE id={$params['favorite_id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)==0)
		{
			return $this->failed("Unable to remove the selected merchant favorite.");
		}
		$update_data = array(
			'status' => 'D',
			'update_by' => $params['update_by'],
			'update_date' => $params['update_date'],	
		);
		
		if(!$this->db->update('customer_favorite', $update_data, 'id='. $params['favorite_id']. " AND customer_id={$params['customer_id']}"))
		{
			$datetime = date('Y-m-d H:i:s');
            $this->db->rollback_transaction();	
			$this->save_to_action_logs('Error removing from favorites '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to remove customer favorite.");
		}
		
		return $this->success("Favorite has been removed.");
	}
	
	//customer_id, merchant_id, create_date, create_by
	public function save_as_favorite($params)
	{
		$cmd =" SELECT id FROM merchant WHERE id={$params['merchant_id']}";
		$res = $this->db->fetchRow($cmd);
		if(count($res)==0)
		{
			return $this->failed("Unable to find merchant.");
		}
		
		$cmd =" SELECT id FROM  customer_favorite WHERE customer_id={$params['customer_id']} AND merchant_id={$params['merchant_id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)>0)
		{
			return $this->failed("Merchant has already been added on your favorites.");
		}
		
		$insert_data = array(
			'customer_id' => $params['customer_id'],
			'merchant_id' => $params['merchant_id'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => 'A'
		);
		
		$this->db->begin_transaction();
		
		if(!$this->db->insert('customer_favorite', $insert_data))
		{
			return $this->failed("Unable to save as favorite.");
		}
		
		$this->db->commit_transaction();
		return $this->success("Merchant has been added on your list of favorite.");
	}
	
	
	//customer_id email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender, update_date, update_by
	public function update_my_customer_information($params)
	{
		
		$cmd="SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' AND status='A' AND id<> {$params['customer_id']}";
		
		// print_r($cmd);
		// die();
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Mobile number has already been registered.");
		}
		
		$this->db->begin_transaction();
		
		
		$update_data = array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'middle_name' => isset($params['middle_name']) ? $params['middle_name'] : '',
			'email' => isset($params['email']) ? $params['email'] : '',
			'mobile_number' => $params['contact_number'],
			'city' => isset($params['city']) ? $params['city'] : '',
			'state' => isset($params['state']) ? $params['state'] : '',
			'zip' => isset($params['zip']) ? $params['zip'] : '',
			'country' => isset($params['country']) ? $params['country'] : '',
			'birth_date' => isset($params['birth_date']) ? $params['birth_date'] : '',
			'gender' => isset($params['gender']) ? $params['gender'] : '',
			'address' => isset($params['address']) ? $params['address'] : '',
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			
		);
		
		if(!$this->db->update('customer', $update_data, 'id='. $params['customer_id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating customer information '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update cusotmer information.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Customer information has been updated.");
	}
	
	
	
	//customer_id
	public function get_customer_check_ins($params)
	{
		$cmd =" SELECT mobile_number FROM customer WHERE status='A' AND id={$params['customer_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(isset($res['mobile_number']))
		{
			$mobile_number = $res['mobile_number'];
		}
		
		$cmd = "SELECT m.business_name,m.logo, m.id as merchant_id,w.create_date, TIME_FORMAT(check_in_time, '%h:%i%p') as check_in_time, w.status FROM waiting_list w 
		LEFT JOIN merchant m ON w.merchant_id=m.id
		WHERE contact_number='{$mobile_number}' AND customer_id=-1
		UNION ALL SELECT  m.business_name,m.logo, m.id as merchant_id,w.create_date, TIME_FORMAT(check_in_time, '%h:%i%p') as check_in_time, w.status FROM waiting_list w 
		LEFT JOIN merchant m ON w.merchant_id=m.id
		WHERE customer_id={$params['customer_id']}
		";

		
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	
	//customer_id
	public function get_my_customer_reservation($params)
	{
		
		$cmd =" SELECT mobile_number FROM customer WHERE status='A' AND id={$params['customer_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(isset($res['mobile_number']))
		{
			$mobile_number = $res['mobile_number'];
		}
		
		
		
		
		$cmd =" SELECT rh.*,TIME_FORMAT(rh.reservation_time, '%h:%i%p') asis_time,m.business_name, m.id as merchant_id, m.logo, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN merchant m ON rh.merchant_id=m.id
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id=m.id 
		WHERE rh.customer_id={$params['customer_id']} 
		
		UNION ALL
		
		SELECT rh.*,TIME_FORMAT(rh.reservation_time, '%h:%i%p') asis_time,m.business_name, m.id as merchant_id, m.logo, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN merchant m ON rh.merchant_id=m.id
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id=m.id 
		WHERE  rh.mobile='{$mobile_number}' AND rh.customer_id<0
		
		ORDER BY create_date DESC";
		
		// print_r($cmd);
		// die();
		
		$records = $this->db->fetchAll($cmd);
		
		return $this->success($records);	
	}
	
	//customer_id, merchant_id, comment, create_date, create_by
	public function create_my_review($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		
		$insert_data = array(
			'customer_id' => $params['customer_id'],
			'merchant_id' => $params['merchant_id'],
			'comment' => $params['comment'],
			'status' => 'A',
			'create_by' => $params['create_by'],
			'create_date' => $params['create_date'],
		);
		
		if(!$this->db->insert('merchant_review', $insert_data)) {
            return $this->failed("Unable to add merchant review.");
        }
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant Review has been created.");
	}
	
	
	//id, status 'A' for Active 'D' for deleted, comment
	public function update_my_review($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		$update_data = array(
			'status' => $params['status'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'comment' => $params['comment'],	
		);
		
		if(!$this->db->update('merchant_review', $update_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating merchant review '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to updating merchant review.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Merchant review has been updated.");
	}
	
	
	//merchant_id
	public function get_time_zone($params)
	{
		$cmd =" SELECT timezone FROM merchant WHERE id={$params['merchant_id']} ";
		$timezone = $this->db->fetchRow($cmd);
		
		return $this->success($timezone);
	}

	
	//merchant_id
	public function get_all_merchant_details($params)
	{
		$cmd ="SELECT * FROM merchant WHERE id={$params['merchant_id']}";
		$merch = $this->db->fetchRow($cmd);
		
		if(count($merch)==0)
		{
			return $this->failed("Unable to find merchant.");
		}
		
		//get services
		$cmd ="SELECT * FROM product_category WHERE status='A' AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['service_category'] = $res;
		
		$cmd ="SELECT * FROM product WHERE status='A' AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['services'] = $res;
		
		$cmd = "SELECT sp.id, sp.last_name, sp.first_name, GROUP_CONCAT(CASE WHEN p.name IS NULL THEN 'NA' ELSE p.name END) as services FROM sale_person sp 
LEFT JOIN product_sale_person psp ON sp.id=psp.sale_person_id
LEFT JOIN product p ON psp.product_id=p.id
 WHERE sp.status='A' AND sp.merchant_id=52
 GROUP BY sp.id, sp.last_name, sp.first_name";
		$res = $this->db->fetchAll($cmd);
		$merch['specialist'] = $res;
		
		
		//get business hours
		$cmd ="SELECT TIME_FORMAT(start_time, '%h:%i%p') as start_time, TIME_FORMAT(end_time, '%h:%i%p') as end_time,
		day, is_close, merchant_id, id 
		FROM merchant_business_hours WHERE merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['business_hours'] = $res;
		
		$cmd =" SELECT comment, r.create_date, c.first_name, c.last_name FROM merchant_review r INNER JOIN customer c ON r.customer_id=c.id  WHERE r.status='A' AND r.merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchAll($cmd);
		$merch['reviews'] = $res;
		
		
		//get gallery NA
		//get reviews NA
		//get promo NA
		return $this->success($merch);
	}
	
	//last_name, first_name, contact_number, merchant_id, check_in_time, create_date, create_by
	public function check_in_for_waiting_list($params)
	{
		$params = array_map('trim', $params);
		
		$cmd =" SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' ";
		
		$rs = $this->db->fetchRow($cmd);
		if(isset($rs['id']))
		{
			$params['customer_id'] = $rs['id'];
		}
		
		$day = date('l', strtotime($params['create_date']));
		
		
		$this->db->begin_transaction();
		
		$insert_data = array(
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'contact_number' => $params['contact_number'],
			'status' => 'WAITING', //SEATED for 
			'check_in_time' => $params['check_in_time'],
			'merchant_id' => $params['merchant_id'],
		);
		
		if(!$this->db->insert('waiting_list', $insert_data)) {
            return $this->failed("Unable to add waiting list.");
        }
		
		$this->db->commit_transaction();
		
		return $this->success("Record has been added to the waiting list.");
	}
	
	//merchant_id, create_date, is_display_all_status
	public function get_waiting_list_per_merchant($params)
	{
		$cmd ="SELECT * FROM waiting_list WHERE  create_date='{$params['create_date']}' AND merchant_id={$params['merchant_id']}";
		
		if(!$params['is_display_all_status']) //if false
		{
			$cmd .= " AND status='WAITING'";
		}
// 	
		// print_r($cmd);
		// die();
	
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);	
	}
	
	//id, merchant_id ,status = SEATED or CANCELLED, update_date, update_by
	public function update_waiting_status($params)
	{
		$params = array_map('trim', $params);
		$this->db->begin_transaction();
		$update_data = array(
			'status' => $params['status'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'update_time' => $params['update_time'],
		);
		
		if(!$this->db->update('waiting_list', $update_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error updating waiting list '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update waiting list.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Waiting list has been updated.");
	}
	
	
	//params: username, password
    public function login_customer($params) {
        $params = array_map('trim', $params);

        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
			 // print_r("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
			 // die();
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
        	
			// print_r($user_check);
			// die();
			
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				// $profile = '';
				// if($user_check['is_iso'])
				// {
					// $profile = 'iso';
				// }elseif($user_check['is_merchant']){
					// $profile = 'merchants';
				// }elseif ($user_check['is_agent']){
					// $profile = 'agents';
				// }else{
					// $profile = 'customers';
				// }				
				
				$id = $user_check['reference_id'];
				// print_r($id);
				// die();
				// $user_info = $this->get_profile($profile, $id);
				$user_info = array();
				
				//$user_info['profile'] = $profile;
				
				$user_info['user_id'] = $user_check['id'];
				$user_info['partner_id'] = $id;
				$user_info['is_agent'] = $user_check['is_agent'];
				$user_info['user_type'] = $user_check['user_type_id'];
                $user_info['user_type_desc'] = $user_check['user_type_desc'];
				$user_info['username'] = $params['username'];
				// $user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
				$customer_info = $this->db->fetchRow("SELECT * FROM customer WHERE id={$id} AND status='A'");
				
				if(count($customer_info) ==0)
				{
					return $this->failed("Unable to find customer");
				}
				
				
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                    'customer_information' => $customer_info,
                );
                
                return $this->success($response);
            }
        }
    }
	
	//username, password, email, contact_number, address, city, state, zip, country, last_name, first_name, middle_name
	//birth_date, gender
	public function register_customer($params)
	{
		$this->db->begin_transaction();
		
		$cmd="SELECT id FROM customer WHERE mobile_number='{$params['contact_number']}' AND status='A'";
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Mobile number has already been registered.");
		}
		
		
		$insert_data = array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'middle_name' => isset($params['middle_name']) ? $params['middle_name'] : '',
			'email' => isset($params['email']) ? $params['email'] : '',
			'mobile_number' => $params['contact_number'],
			'city' => isset($params['city']) ? $params['city'] : '',
			'state' => isset($params['state']) ? $params['state'] : '',
			'zip' => isset($params['zip']) ? $params['zip'] : '',
			'country' => isset($params['country']) ? $params['country'] : '',
			'birth_date' => isset($params['birth_date']) ? $params['birth_date'] : '',
			'gender' => isset($params['gender']) ? $params['gender'] : '',
			'address' => isset($params['address']) ? $params['address'] : '',
		);
		
		if(!$this->db->insert('customer', $insert_data)) {
            return $this->failed("Unable to add customer.");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
			
			//user_type_id =3 for customers
			$cmd ="SELECT id FROM users WHERE username='{$params['username']}' AND status='A' LIMIT 1";
			$results = $this->db->fetchRow($cmd);
			if(count($results)>0)
			{
				$this->db->rollback_transaction();
				return $this->failed("Username has already been use");
			}
			
			$params['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));  
        
			$insert = array(
				'username' => $params['username'],
				'password' => $params['password'],
				'last_name' => $params['last_name'],
				'first_name' => $params['first_name'],
				'email_address' => isset($params['email']) ? $params['email'] : '',
				'user_type_id' => 3,
				'reference_id' => $id,
				'status' => 'A',
			);
			
			
	        if (!$this->db->insert('users', $insert)) {
	            $this->db->rollback_transaction();
	            $datetime = date('Y-m-d H:i:s');
	            $this->save_to_action_logs('Error creating new customer'  ,'ERROR',$datetime,$_SESSION['username'],'');
	            return $this->failed("Unable to create new user.");
	            die();
	        }  
			
		}
		
		$this->db->commit_transaction();
		return $this->success("Customer has been registered.");
	}
	
	//merchant_id, service_id, date, time
	public function get_specialist_for_reservation($params)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd ="SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status FROM sale_person sp 
		LEFT JOIN 
		(SELECT DISTINCT s.* FROM 
		reservation_header h
		INNER JOIN reservation_sale_person s ON h.id=reservation_header_id
		WHERE h.reservation_status_id<>5
		AND reservation_time>='{$params['reservation_time']}'
		AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) )
		AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d
		ON sp.id=d.id ";
		
		$cmd .=" LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
		
		//add 1 hour as a buffer

		$day = date('l', strtotime($params['reservation_date']));
		$cmd .=" WHERE sp.schedule LIKE '%{$day}%'";
		$cmd .=" AND sp.merchant_id={$params['merchant_id']}";
		
		if($params['service_id'] > 0)
		{
			$cmd .=" AND ps.product_id={$params['service_id']}";
		}
		
		// print_r($cmd);
		// die();
// 		
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	public function get_product_categories($params)
	{
		$cmd ="SELECT * FROM product_category WHERE status='A' AND merchant_id ={$params['merchant_id']}";
		
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	//merchant_id, name, description, create_date, create_by
	public function create_product_category($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'name' => $params['name'],
			'description' => $params['description'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
		);
		
		if (!$this->db->insert('product_category', $insert_data)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new product category '  ,'ERROR',$datetime,$params['create_by'],'');
            return $this->failed("Unable to create product category.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Product category has been created.");
	}
	
	
	//id, merchant_id, name, description, update_date, update_by
	public function update_product_category($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'name' => $params['name'],
			'description' => $params['description'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
		);
		
		if(!$this->db->update('product_category', $insert_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error update product category '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to update product category.");
        }  
		
		$this->db->commit_transaction();
		return $this->success("Product category has been updated.");
	}
	
	//id, merchant_id
	public function delete_product_category($params)
	{
		$this->db->begin_transaction();
		$cmd ="SELECT id FROM product WHERE category_id={$params['id']} AND merchant_id={$params['merchant_id']}";
		$res = $this->db->fetchRow($cmd);
		if(count($res) > 0)
		{
			return $this->failed("Unable to delete category. Category has already been used.");
		}
		
		$cmd ="SELECT id FROM product_category WHERE id={$params['id']} AND status='A'";
		$res = $this->db->fetchRow($cmd);
		if(count($res) == 0)
		{
			return $this->failed("Unable to delete category. Id cannot be found.");
		}
		
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'status' => "D",
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
		);
		
		if(!$this->db->update('product_category', $insert_data, 'id='. $params['id']))
		{
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error deleting product category '  ,'ERROR',$datetime,$params['update_by'],'');
            return $this->failed("Unable to delete product category.");
        }  
		
		
		$this->db->commit_transaction();
		return $this->success("Product category has been deleted.");
		
	}
	
	
	public function get_merchant_profile($params)
	{
		$cmd ="SELECT m.*,mc.*  FROM  merchant m 
		LEFT JOIN merchant_contact mc on m.id=mc.merchant_id 
		LEFT JOIN merchant_mailing_address mm on m.id=mm.merchant_id  WHERE m.id={$params['merchant_id']}";
		
		$results =$this->db->fetchRow($cmd);
		return $this->success($results);
	}
	
	//merchant
	//id
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status
	//logo, description
	
	//merchant_contact
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//merchant_mailing_address
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	
	public function update_merchant_profile($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'business_name' => $params['business_name'],
			'dba' => $params['dba'],
			'processor' => $params['processor'],
			'mid' => $params['mid'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'fax' => $params['fax'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			
			'description' => $params['description'],
			'longitude' => $params['longitude'],
			'latitude' => $params['latitude'],
		);
		
		if($params['logo'] !="")
		{
			$insert_data['logo'] = $params['logo'];
		}
		
		$merchant_id = $params['id'];
		if(!$this->db->update('merchant', $insert_data, 'id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant.");
		}
		
		$insert_data = array(
			'first_name' => $params['first_name'], 
			'last_name' => $params['last_name'],
			'position' => $params['position'],
			'mobile_number' => $params['mobile_number'],
			'business_phone1' => $params['business_phone1'],
			'business_phone2' => $params['business_phone2'],
			'extension' => $params['extension'],
			'fax' => $params['contact_fax'],
			'email' => $params['contact_email'],
			'website' => $params['website'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			// 'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->update('merchant_contact', $insert_data, 'merchant_id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant.");
		}
		
		
		// mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
		$insert_data = array(
			'country' => $params['mailing_country'],
			'address' => $params['mailing_address'],
			'city' => $params['mailing_city'],
			'state' => $params['mailing_state'],
			'zip' => $params['mailing_zip'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'status' => $params['status'],
			// 'merchant_id' => $merchant_id,
		);
		
		// if(!$this->db->insert('merchant_mailing_address', $insert_data))
		if(!$this->db->update('merchant_mailing_address', $insert_data, 'merchant_id='. $merchant_id))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		
		$cmd ="SELECT id FROM merchant_business_hours WHERE merchant_id={$merchant_id} AND day='MONDAY'";
		$res = $this->db->fetchRow($cmd);
		
		if(count($res)>0)
		{
			//update
			// $update_query = "UPDATE merchant_business_hours"
			$update_data = array(
				'is_close' => $params['monday_is_close'],
				'start_time' => $params['monday_start_time'],
				'end_time' => $params['monday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'id='. $res['id']))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			
			$update_data = array(
				'is_close' => $params['tuesday_is_close'],
				'start_time' => $params['tuesday_start_time'],
				'end_time' => $params['tuesday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='TUESDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			
			$update_data = array(
				'is_close' => $params['wednesday_is_close'],
				'start_time' => $params['wednesday_start_time'],
				'end_time' => $params['wednesday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='WEDNESDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			
			$update_data = array(
				'is_close' => $params['thursday_is_close'],
				'start_time' => $params['thursday_start_time'],
				'end_time' => $params['thursday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='THURSDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			
			$update_data = array(
				'is_close' => $params['friday_is_close'],
				'start_time' => $params['friday_start_time'],
				'end_time' => $params['friday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='FRIDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			
			$update_data = array(
				'is_close' => $params['saturday_is_close'],
				'start_time' => $params['saturday_start_time'],
				'end_time' => $params['saturday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='SATURDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
			$update_data = array(
				'is_close' => $params['sunday_is_close'],
				'start_time' => $params['sunday_start_time'],
				'end_time' => $params['sunday_end_time'],
				'update_date' => $params['update_date']
			);
			if(!$this->db->update('merchant_business_hours', $update_data, 'merchant_id='. $merchant_id ." AND day='SUNDAY'"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update merchant.");
			}
		}
		else{
			//insert
			$insert_data = array(
				'day' => 'MONDAY',
				'is_close' => $params['monday_is_close'],
				'start_time' => $params['monday_start_time'],
				'end_time' => $params['monday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}
			
			$insert_data = array(
				'day' => 'TUESDAY',
				'is_close' => $params['tuesday_is_close'],
				'start_time' => $params['tuesday_start_time'],
				'end_time' => $params['tuesday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}    
			$insert_data = array(
				'day' => 'WEDNESDAY',
				'is_close' => $params['wednesday_is_close'],
				'start_time' => $params['wednesday_start_time'],
				'end_time' => $params['wednesday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}  
			$insert_data = array(
				'day' => 'THURSDAY',
				'is_close' => $params['thursday_is_close'],
				'start_time' => $params['thursday_start_time'],
				'end_time' => $params['thursday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}  
			$insert_data = array(
				'day' => 'FRIDAY',
				'is_close' => $params['friday_is_close'],
				'start_time' => $params['friday_start_time'],
				'end_time' => $params['friday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	}
			$insert_data = array(
				'day' => 'SATURDAY',
				'is_close' => $params['saturday_is_close'],
				'start_time' => $params['saturday_start_time'],
				'end_time' => $params['saturday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	} 
			$insert_data = array(
				'day' => 'SUNDAY',
				'is_close' => $params['sunday_is_close'],
				'start_time' => $params['sunday_start_time'],
				'end_time' => $params['sunday_end_time'],
				'merchant_id' => $merchant_id,
			);
			if (!$this->db->insert('merchant_business_hours', $insert_data)) {
           	 	$this->db->rollback_transaction();
            	return $this->failed("Unable to update merchant.");
        	} 
		}
		
		
		
		
		$user_name = $this->generate_username();
		$password = rand(100000, 999999);
		
		//username , password, last_name, first_name, email_address, user_type_id, reference_id
		$user_insert = array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
		);
		
		// if(!$this->db->insert('merchant_mailing_address', $insert_data))
		if(!$this->db->update('users', $user_insert, 'reference_id='. $merchant_id. " AND email_address='{$params['email']}'"))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant.");
		}
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant has been updated.");
	}
	
	
	//merchant_id
	public function get_total_analytics($params)
	{
		$cmd=" SELECT CASE WHEN rs.name  IS NULL THEN 'New' ELSE rs.name END as name, count(rh.id) AS total FROM reservation_header rh 
LEFT JOIN reservation_status rs on rh.reservation_status_id=rs.id
WHERE rh.merchant_id={$params['merchant_id']}
GROUP BY rs.name
		";	
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);	
	}
	
	//merchant_id, mobile
	public function search_customer_by_mobile($params)
	{
		$cmd ="SELECT last_name, first_name, mobile FROM reservation_header WHERE merchant_id={$params['merchant_id']}";
		$cmd .=" AND mobile =". $this->db->db_escape($params['mobile']) ." LIMIT 1";
		
		$result = $this->db->fetchRow($cmd);	
		
		if(isset($result['last_name']))
		{
			return $this->success($result);
		}else{
			return $this->failed("Unable to find mobile.");
		}	
	}
	
	//merchant_id, date, branch_id
	public function get_salon_specialist_per_merchant_per_day($params)
	{
		// $merchant_id = $params['merchant_id'];
		
		$cmd = "SELECT sp.*, mb.branch_name FROM sale_person sp LEFT JOIN 
		merchant_branch mb ON sp.branch_id=mb.id WHERE sp.status='A' AND sp.merchant_id={$params['merchant_id']}";
		
		if($params['branch_id'] !== -1)
		{
			$cmd .=" AND mb.id={$params['branch_id']}";	
		}
		
		$day = date('l', strtotime($params['date']));
		$cmd .=" AND sp.schedule LIKE '%{$day}%'";
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	public function get_countries()
	{
		$cmd = "SELECT id,name, iso_code_2, iso_code_3 FROM country";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//merchant_id
	public function get_merchant_users($params)
	{
		$cmd ="SELECT * FROM users WHERE user_type_id=8 AND status='A' AND reference_id={$params['merchant_id']}";
		$results = $this->db->fetchAll($cmd);
		
		return $this->success($results);
	}
	
	//user_id, last_name, first_name, email_address, update_date, update_by
	public function update_user_information($params)
	{
		$cmd = " SELECT id FROM users WHERE id<>{$params['user_id']} AND email_address='{$params['email_address']}'";
		$res = $this->db->fetchRow($cmd);
		if(count($res)>0)
		{
			return $this->failed("Email has already been use.");
		}
		
		$update_data =array(
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email_address'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
 		);
		
		if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
            return $this->failed("Unable to update user information.");    
        } else {
            return $this->success("Account information has been updated");
        }
	}
	
	//user_id, password, new_password, update_date, update_by
	public function update_my_password($params)
	{
		//check if current_password, via user_id
		if($params['password']== $params['new_password'])
		{
			return $this->failed("Current password and new password cannot be the same.");
		}
		
		$current_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));
		$new_password = $this->hideit->encrypt(sha1(md5($this->salt)),$params['new_password'], sha1(md5($this->salt)));
		
		$cmd ="SELECT id FROM users WHERE id={$params['user_id']}";
		$cmd .= " AND password=".$this->db->db_escape($current_password);
		
		$rs = $this->db->fetchRow($cmd);
		if(count($rs)==0)
		{
			return $this->failed("Unable to update password. Invalid old password.");
		}
		
		$update_data = array(
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			'password' => $new_password,
		);
		
		if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
            return $this->failed("Unable to update password.");    
        } else {
            return $this->success("Your account password has been updated.");
        }
	}
	
	//merchant_id, username, password, last_name, first_name, email_address, create_by, create_date, status
	public function merchant_create_user($params)
	{
		
		$cmd ="SELECT id FROM  users WHERE username=".$this->db->db_escape($params['username']) ." OR email_address=".$this->db->db_escape($params['email_address']);
		$cmd .=" LIMIT 1";
		
		
		$res = $this->db->fetchRow($cmd);
		
		if(count($res))
		{
			return $this->failed("Username/email address has already been used.");
		}
		
		//user_type_id = 8;
		$params['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        	
		$insert = array(
			'username' => $params['username'],
			'password' => $params['password'],
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email_address'],
			'user_type_id' => 8,
			'is_merchant_user' => 1,
			'reference_id' => $params['merchant_id'],
			'is_agent' => 0,
			'status' => "A",
		);
		
        if (!$this->db->insert('users', $insert)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
        }    
		
		return $this->success("New merchant user has been created."); 
	}
	
	//id from reservation list,reservation_status_id = just put -1 if the information will just be the one that needs to be update
	////merchant_id, reservation_date, reservation_time, create_date, create_by, status
	//branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited
	public function update_reservation($params)
	{
		$this->db->begin_transaction();	
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'customer_id' => isset($params['customer_id']) ? $params['customer_id'] : -1,
			'first_name' => $params['first_name'],
			'last_name' => $params['last_name'],
			'mobile' => $params['mobile'],
			'notes' => $params['notes'],
			'reservation_date' => $params['reservation_date'],
			'reservation_time' => $params['reservation_time'],
			'update_date' => $params['update_date'],
			'update_by' => $params['update_by'],
			//'status' => "NEW",
			'reservation_status_id' => $params['reservation_status_id'],
			'sale_person_ids' => $params['sale_person_id'],
			'branch_id' => $params['branch_id'],
		);
		
		// if($params['reservation_status_id'] > 0)
		// {
			// $insert_data['reservation_status_id'] = $params['reservation_status_id'];
		// }
		
		if(!$this->db->update('reservation_header', $insert_data, "id=". $params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update reservation.");
		}
		
		$this->db->query("DELETE FROM reservation_detail WHERE reservation_header_id={$params['id']}");
		
		//$reservation_id = $this->db->lastInsertId();
		$reservation_id = $params['id'];
		$products = explode(",",$params['product_id']);
		
		foreach ($products as $p)
		{
			$prod = $this->get_product_info($p);
			
			$price = $prod['price'];
			$minutes = $prod['minutes_of_product'];
			
			$insert_data = array(
				'reservation_header_id' => $reservation_id,
				'product_id' => $p,
				'price' => $price,
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'sales_person_id' => -1,
				'minutes_of_product' => $minutes,
				'status' => 'A',
			);
			
			if(!$this->db->insert('reservation_detail', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation.");
			}
		}
		$this->db->commit_transaction();
		
		return $this->success("Reservation has been updated.");
	}
	
	//merchant_id, date
	public function get_reservations_per_merchant($params)
	{

		// $cmd =" SELECT rh.*, rs.color FROM reservation_header rh
		// LEFT JOIN reservation_status rs ON rh.reservation_status_id=rs.id 
		// WHERE rh.create_date>='{$params['date']}' AND rh.merchant_id={$params['merchant_id']} ORDER BY create_date DESC";
		
		$cmd =" SELECT rh.*, CASE WHEN rs.color IS NULL THEN sd.color ELSE rs.color END as color FROM reservation_header rh
		LEFT JOIN status_default sd ON rh.reservation_status_id=sd.id  
		LEFT JOIN reservation_status rs ON sd.id=rs.status_default_id AND rs.merchant_id={$params['merchant_id']} 
		WHERE rh.create_date>='{$params['date']}' AND rh.merchant_id={$params['merchant_id']} ORDER BY create_date DESC";
				
		// print_r($cmd);
		// die();

		$results = $this->db->fetchAll($cmd);
		
		$reservations = array();
		foreach ($results as $r)
		{
			$cmd ="SELECT rd.*, p.id as product_id,p.name as product_name FROM reservation_detail rd LEFT JOIN product p ON rd.product_id=p.id WHERE p.status='A' ";
			$cmd .=" AND rd.reservation_header_id={$r['id']}";
			$details = $this->db->fetchAll($cmd);
			
			$product_id = "";
			foreach($details as $d)
			{
				$product_id .= $d['product_id'] .",";
			}
			
			$r['product_ids'] = $product_id;
			$r['details'] = $details;
			
			$reservations[] = $r;
		}
		return $this->success($reservations);
	}
	
	
	//merchant_id
	public function get_reservation_status_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		//$cmd = "SELECT * FROM reservation_status WHERE status='A' AND merchant_id={$merchant_id}";
		
		$cmd ="SELECT s.id, CASE  WHEN rs.color IS NULL THEN s.color ELSE rs.color END as color, s.name, s.description
		FROM status_default s LEFT JOIN reservation_status rs ON s.id=rs.status_default_id AND rs.merchant_id={$merchant_id}";	
		
		
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	
	//merchant_id, name, description, color, create_date, create_by, status
	public function create_reservation_status($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('reservation_status', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create reservation status.");
		}
		
		$this->db->commit_transaction();
		return $this->success("Reservation status has been created.");
	}
	
	//merchant_id, name, description, color, create_date, create_by, status
	public function update_reservation_status($params)
	{
		$this->db->begin_transaction();
		$cmd ="SELECT id FROM reservation_status WHERE status_default_id={$params['id']} AND merchant_id={$params['merchant_id']}";
		
		$res = $this->db->fetchRow($cmd);
		if(count($res) >0)
		{
			$id = $params['id'];
			unset($params['id']);
			//if(!$this->db->update('reservation_status', $params, 'id='. $params['id']))
			if(!$this->db->update('reservation_status', $params, 'status_default_id='. $id. " AND merchant_id={$params['merchant_id']}"))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation status.");
			}
		}else{
			$insert_data = array(
				'color' => $params['color'],
				'name' => $params['name'],
				'description' => $params['description'],
				'status_default_id' => $params['id'],
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'merchant_id' => $params['merchant_id']
			);
			if(!$this->db->insert('reservation_status', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to update reservation status.");
			}
		}
		$this->db->commit_transaction();
		return $this->success("Reservation status has been updated.");
	}
	
	
	//id, merchant_id, branch_id, last_name, first_name, middle_name, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time, services, contact_number, email_address
	public function update_salon_specialist($params)
	{
		$this->db->begin_transaction();
		$services = $params['services'];
		unset($params['services']);
		if(!$this->db->update('sale_person', $params, 'id='.$params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update salon specialist.");
		}
		
		$sale_person_id = $params['id'];
		$products = explode(",",$services);
		
		
		$this->db->query("DELETE FROM product_sale_person WHERE sale_person_id={$sale_person_id}");
		
		foreach($products as $p)
		{
			
			$prod = $this->get_product_info($p);
			
			if(count($prod) == 0)
			{
				return $this->failed("Unable to find product.");
			}
			
			
			$product = array(
				'product_id' => $prod['id'],
				'sale_person_id' => $sale_person_id,
				'create_date' => $params['update_date'],
				'create_by' => $params['update_by'],
				'merchant_id' => $params['merchant_id'],
			);
			if(!$this->db->insert('product_sale_person', $product))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create salon specialist.");
			}
			
		}
		
		
		
		$this->db->commit_transaction();
		return $this->success("Salon Specialist has been updated.");
	}
	
	
	
	//merchant_id, id, longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time,contact_number,
	//store_day, update_date, update_by
	public function update_merchant_branch($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->update('merchant_branch', $params, 'id='.$params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update merchant branch.");
		}
		$this->db->commit_transaction();
		return $this->success("Merchant Branch has been updated.");
		
	}
	
	//id, merchant_id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//is_inventory, update_date, update_by, status
	public function update_product($params)
	{
		$update_data =  $params;
		$this->db->begin_transaction();
		if(!$this->db->update('product', $params, 'id='. $params['id']))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to update product.");
		}
		$this->db->commit_transaction();
		return $this->success("Product has been updated.");
	}
	
	
	
	//merchant_id
	public function get_branch_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd = "SELECT * FROM merchant_branch WHERE status='A' AND merchant_id={$merchant_id}";
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	//merchant_id, category_id
	public function get_products_per_merchant($params)
	{
		// $merchant_id = $params['merchant_id'];
		$cmd = "SELECT p.*, c.name as category_name, c.description as category_description FROM product p LEFT JOIN  product_category c ON p.category_id=c.id WHERE p.status='A' AND p.merchant_id={$params['merchant_id']}";
		
		if($params['category_id']>0)
		{
			$cmd .=" AND category_id={$params['category_id']}";
		}
		
		$result = $this->db->fetchAll($cmd);
		return $this->success($result);
	}
	
	
	//merchant_id
	public function get_salon_specialist_per_merchant($merchant_id)
	{
		// $merchant_id = $params['merchant_id'];
		//$cmd = "SELECT sp.*, mb.branch_name FROM sale_person sp LEFT JOIN merchant_branch mb ON sp.branch_id=mb.id WHERE sp.status='A' AND sp.merchant_id={$merchant_id}";
		
		$cmd = "SELECT sp.* FROM sale_person sp WHERE sp.status='A' AND sp.merchant_id={$merchant_id}";
		
		$results = $this->db->fetchAll($cmd);
		
		$specialist = array();
		foreach ($results as $r)
		{
			$cmd ="SELECT p.* FROM product p LEFT JOIN product_sale_person  ps ON p.id=ps.product_id";
			$cmd .=" WHERE sale_person_id={$r['id']}";
			$details = $this->db->fetchAll($cmd);
			
			$r['products'] = $details;
			$specialist[] = $r;
		}
		
		return $this->success($specialist);	
	}
	
	
	
	//merchant_id
	public function list_reservations_per_merchant()
	{
		$cmd =" SELECT * FROM reservation_heaer h LEFT JOIN reservation_detail d ON h.id=d.reservation_header_id";
		$cmd .= " WHERE h.status=''";
		
		return $this->db->fetchAll($cmd);
	}
	
	
	
	//merchant_id, reservation_date, reservation_time, create_date, create_by, status
	//branch_id
	//last_name, first_name, notes, mobile,
	//product_id = comma delimited, 
	//sale_person_id  comma delimited
	public function create_reservation_via_merchant($params)
	{
		//search for customer for later
		
		//if not exist add to customer
		$cmd =" SELECT id FROM customer WHERE mobile_number='{$params['mobile']}' ";
		
		$rs = $this->db->fetchRow($cmd);
		if(isset($rs['id']))
		{
			$params['customer_id'] = $rs['id'];
		}
		
		$day = date('l', strtotime($params['reservation_date']));
		
		
		//check if there is available specialist on the said service
		$cmd ="SELECT * FROM (SELECT DISTINCT sp.id, sp.last_name, sp.first_name, CASE WHEN d.id IS NULL THEN 'AVAILABLE' ELSE 'UNAVAILABLE' END as current_status ";  
		$cmd .= "FROM sale_person sp LEFT JOIN "; 
		$cmd .= "(SELECT DISTINCT s.* FROM reservation_header h INNER JOIN reservation_sale_person s ON h.id=reservation_header_id "; 
		$cmd .= "WHERE h.reservation_status_id<>5 AND reservation_time>='{$params['reservation_time']}' 
		AND reservation_time<=ADDTIME('{$params['reservation_time']}', SEC_TO_TIME(duration*h.duration) ) "; 
		
		$cmd .= "AND reservation_date ='{$params['reservation_date']}' AND h.merchant_id={$params['merchant_id']}) AS d ON sp.id=d.sale_person_id  ";
		$cmd .= "LEFT JOIN product_sale_person ps ON sp.id=ps.sale_person_id ";
		$cmd .= "WHERE sp.schedule LIKE '%{$day}%' AND sp.merchant_id={$params['merchant_id']} AND ps.product_id IN({$params['product_id']})) as dt 
		WHERE current_status='AVAILABLE'";
		
		$rs = $this->db->fetchAll($cmd);
		if(count($rs)==0)
		{
			return $this->failed("Unable to reserve. No more available technicians.");
		}
		
		//check if select specialist is available
		$sale_persons = trim($params['sale_person_id']);
		if($sale_persons != "")
		{
			$cmd .= " AND id IN({$sale_persons}) ";
			$rs = $this->db->fetchAll($cmd);
			if(count($rs)==0)
			{
				return $this->failed("Unable to reserve. You choice of technician is not available.");
			}		
		}
		
		$this->db->begin_transaction();	
		$insert_data = array(
			'merchant_id' => $params['merchant_id'],
			'customer_id' => isset($params['customer_id']) ? $params['customer_id'] : -1,
			'first_name' => $params['first_name'],
			'last_name' => $params['last_name'],
			'mobile' => $params['mobile'],
			'notes' => $params['notes'],
			'reservation_date' => $params['reservation_date'],
			'reservation_time' => $params['reservation_time'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => "NEW",
			'sale_person_ids' => $params['sale_person_id'],
			'branch_id' => $params['branch_id'],
			'reservation_status_id' => -1
		);
		
		
		if(!$this->db->insert('reservation_header', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create reservation.");
		}
		
		$reservation_id = $this->db->lastInsertId();
		
		
		if($sale_persons != "")
		{
			$sale_persons = explode(",",$sale_persons);
			foreach ($sale_persons as $s)
			{
				$sale_id = $s;
				
				$insert_data = array(
					'reservation_header_id' => $reservation_id,
					'sale_person_id' => $sale_id,
					'create_date' => $params['create_date'],
					'create_by' => $params['create_by'],
				);
				
				if(!$this->db->insert('reservation_sale_person', $insert_data))
				{
					$this->db->rollback_transaction();
					return $this->failed("Unable to create reservation.");
				}
				
			}
		}
		
		
		$products = explode(",",$params['product_id']);
		
		$total_minutes = 0;
		
		
		foreach ($products as $p)
		{
			$prod = $this->get_product_info($p);
			
			$price = $prod['price'];
			$minutes = $prod['minutes_of_product'];
			
			$total_minutes += $minutes;
			$insert_data = array(
				'reservation_header_id' => $reservation_id,
				'product_id' => $p,
				'price' => $price,
				'create_date' => $params['create_date'],
				'create_by' => $params['create_by'],
				'sales_person_id' => -1,
				'minutes_of_product' => $minutes,
				'status' => 'A',
			);
			
			if(!$this->db->insert('reservation_detail', $insert_data))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create reservation.");
			}
		}
		
		if($total_minutes > 0)
		{
			if(!$this->db->update('reservation_header', $data = array('duration'=> $total_minutes), 'id='.$reservation_id))
			{
				return $this->failed("Unable to create reservation.");
			}
		}
		
		$this->db->commit_transaction();
		
		return $this->success("Reservation has been created.");
	}
	
	private function get_product_info($product_id)
	{
		$cmd ="SELECT * FROM product WHERE id={$product_id} AND status='A'";
		$result = $this->db->fetchRow($cmd);
		
		return $result;
		
	}
	
	
	//merchant_id, name, description, category_id, price, cost, minutes_of_product, quantity, 
	//is_inventory, create_date, create_by, status
	public function register_product($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('product', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create product.");
		}
		$this->db->commit_transaction();
		return $this->success("Product has been created.");
		
	}
	
	//merchant_id, branch_id, last_name, first_name, middle_name, create_date, create_by, update_date, update_by
	//status, birth_date, profile_picture, gender, city, address, state, country, start_date, end_date, commission_id, 
	//schedule, start_time, end_time, services, contact_number, email_address
	public function register_salon_specialist($params)
	{
		$this->db->begin_transaction();
		$services = $params['services'];
		unset($params['services']);
		if(!$this->db->insert('sale_person', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create salon specialist.");
		}
		
		$sale_person_id = $this->db->lastInsertId();
		$products = explode(",",$services);
		foreach($products as $p)
		{
			
			$prod = $this->get_product_info($p);
			
			if(count($prod) == 0)
			{
				return $this->failed("Unable to find product.");
			}
			
			$product = array(
				'product_id' => $prod['id'],
				'sale_person_id' => $sale_person_id,
				'create_date' => $params['create_date'],
				'create_by' => $params['create_by'],
				'merchant_id' => $params['merchant_id'],
			);
			if(!$this->db->insert('product_sale_person', $product))
			{
				$this->db->rollback_transaction();
				return $this->failed("Unable to create salon specialist.");
			}
		}
		
		$this->db->commit_transaction();
		return $this->success("Salon Specialist has been created.");
	}
	
	
	
	

	
	
	
	//merchant_id,longitude, latitude, country, address, state, zip, city, status, store_start_time, store_end_time,contact_number,
	//store_day, create_date, create_by
	public function register_merchant_branch($params)
	{
		$this->db->begin_transaction();
		if(!$this->db->insert('merchant_branch', $params))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create branch.");
		}
		$this->db->commit_transaction();
		return $this->success("Branch has been created.");
		
	}
	
	
	//search_value can be city/state/zip/business_name
	public function get_merchant($params)
	{
		
		
		if($params['search_value'] !="")
		{
			$cmd =" SELECT * FROM merchant WHERE status='A' ";
			$cmd .=" AND (city LIKE '%{$params['search_value']}%' OR state LIKE '%{$params['search_value']}%'
			 OR zip LIKE '%{$params['search_value']}%'
			 OR business_name LIKE '%{$params['search_value']}%')
			";
			
			$merchants = $this->db->fetchAll($cmd);	
		}else{
			$merchants = array();
		}
		
		//get if is close for the said day
		//get if close base on the time
		
		//get waiting time
		
		$list_merchants = array();
		
		foreach ($merchants as $m)
		{
			$merchant_id = $m['id'];
			$params = array(
				'merchant_id' => $merchant_id,
			);
			$this->get_time_zone($params);
			$timezone = $this->response['ResponseMessage']['timezone'];
			date_default_timezone_set($timezone);
			$current_date = date("Y-m-d");
			
			$day = date('l', strtotime(date("Y-m-d")));
			$current_time = date("G:i");
			
			
			
			// print_r($current_time);
				// die();
			
			$current_time = strtotime($current_time);
			
			$day = strtoupper($day);
			
			$cmd = "SELECT * FROM merchant_business_hours WHERE merchant_id={$merchant_id} AND day='{$day}'";
			
			$rs = $this->db->fetchRow($cmd);
			
			
			if($rs['is_close'])
			{
				$m['is_close'] = $rs['is_close']; 
			}else{
				$start_time = strtotime($rs['start_time']);
				$end_time = strtotime($rs['end_time']);	
				
				if( ($end_time>$current_time)  AND ($start_time<$current_time))
				{
					$m['is_close'] = 0;
				}else{
					//close
					$m['is_close'] = 1;
				}
			}
			
			$m['closing_time'] = date("h:i a", strtotime($rs['end_time']));
			$m['opening_time'] = date("h:i a", strtotime($rs['start_time']));
			$m['day'] = $day;
			
			
			$cmd =" SELECT  SUM(CASE WHEN TIMESTAMPDIFF(MINUTE, check_in_time, NOW()) IS NULL THEN 0 ELSE TIMESTAMPDIFF(MINUTE, check_in_time, NOW()) END)  as waiting_time  
			FROM waiting_list WHERE create_date='{$current_date}' AND merchant_id={$merchant_id}";
			
			$rs = $this->db->fetchRow($cmd);

			$hours = floor($rs['waiting_time'] / 60);
			$minutes = $rs['waiting_time'] % 60;

			$m['waiting_time'] = "{$hours} hour(s) and {$minutes} minute(s)";
			
			$list_merchants[] = $m;
			
		}
		
		// $merchant_list = array();
		// foreach($merchants as $m)
		// {
			// $merchant_id = $m['id'];
			// //$m['branches'] = $this->db->fetchAll("SELECT * FROM merchant_branch WHERE merchant_id={$merchant_id} AND status='A'");
			// // $m['contacts'] = $this->db->fetchAll("SELECT * FROM merchant_contact WHERE merchant_id={$merchant_id} AND status='A'");
// 			
			// //sale_person
			// // $m['salon_specialists'] = $this->db->fetchAll("SELECT * FROM sale_person WHERE merchant_id={$merchant_id} AND status='A'");
			// // $m['products'] = $this->db->fetchAll("SELECT * FROM product WHERE merchant_id={$merchant_id} AND status='A'");
// 			
			// $merchant_list[] = $m;
		// }
		
		// return $this->success($merchant_list);
		// return $this->success($merchants);
		return $this->success($list_merchants);
	}
	
	//merchant
	//business_name, dba, processor, mid, address1, address2, city, state, zip, country, email, phone1, phone2,
	//fax, create_date, create_by, status
	
	//merchant_contact
	//first_name, last_name, position, mobile_number, business_phone1, business_phone2, extension, contact_fax, contact_email,
	//website, create_date, create_by, status
	
	//merchant_mailing_address
	//mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
	
	public function register_merchant($params)
	{
		$this->db->begin_transaction();
		$insert_data = array(
			'business_name' => $params['business_name'],
			'dba' => $params['dba'],
			'processor' => $params['processor'],
			'mid' => $params['mid'],
			'address1' => $params['address1'],
			'address2' => $params['address2'],
			'city' => $params['city'],
			'state' => $params['state'],
			'zip' => $params['zip'],
			'country' => $params['country'],
			'email' => $params['email'],
			'phone1' => $params['phone1'],
			'phone2' => $params['phone2'],
			'fax' => $params['fax'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
		);
		
		
		if(!$this->db->insert('merchant', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		$merchant_id = $this->db->lastInsertId();
		
		
		$insert_data = array(
			'first_name' => $params['first_name'], 
			'last_name' => $params['last_name'],
			'position' => $params['position'],
			'mobile_number' => $params['mobile_number'],
			'business_phone1' => $params['business_phone1'],
			'business_phone2' => $params['business_phone2'],
			'extension' => $params['extension'],
			'fax' => $params['contact_fax'],
			'email' => $params['contact_email'],
			'website' => $params['website'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
			'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->insert('merchant_contact', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		
		// mailing_country, mailing_address, mailing_city, mailing_state, mailing_zip, create_date, create_by, status 
		$insert_data = array(
			'country' => $params['mailing_country'],
			'address' => $params['mailing_address'],
			'city' => $params['mailing_city'],
			'state' => $params['mailing_state'],
			'zip' => $params['mailing_zip'],
			'create_date' => $params['create_date'],
			'create_by' => $params['create_by'],
			'status' => $params['status'],
			'merchant_id' => $merchant_id,
		);
		
		if(!$this->db->insert('merchant_mailing_address', $insert_data))
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant.");
		}
		
		
		
		$user_name = $this->generate_username();
		$password = rand(100000, 999999);
		
		//username , password, last_name, first_name, email_address, user_type_id, reference_id
		$user_insert = array(
			'username' => $user_name,
			'password' => $password,
			'last_name' => $params['last_name'],
			'first_name' => $params['first_name'],
			'email_address' => $params['email'],
			'user_type_id' =>  7, //merchant admin 8, 7 merchant super admin
			'reference_id' => $merchant_id,
		);
		
		$this->create_user($user_insert, true);
		if($this->response['ResponseCode'] !="0000")
		{
			$this->db->rollback_transaction();
			return $this->failed("Unable to create merchant");
		}
		
		$this->db->commit_transaction();
		
		return $this->success("Merchant has been created.");
	}
	
	public function generate_username()
	{
		do {
            $user_name = time();
            $check = $this->db->fetchRow('select id from users where username="' . $user_name .'"');
        } while (isset($check['id']));
        return $user_name;
	}

	

	public function send_mail_notification($body,$email_address,$subject='', $attachment ="")
    {
    	// mail("lemmuelcabuhat@gmail.com", "Hello", "TESTING");
		// die();		
			
    	//date_default_timezone_set('America/Toronto');
    	
    	
    	//$file = $path.$filename;
// $content = file_get_contents( $attachment);
// $content = chunk_split(base64_encode($content));
// $uid = md5(uniqid(time()));
// $name = basename($attachment);
// 
// // header
// $header = "From: <".EMAIL_USERNAME.">\r\n";
// $header .= "Reply-To: ".EMAIL_USERNAME."\r\n";
// $header .= "MIME-Version: 1.0\r\n";
// $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
// 
// // message & attachment
// $nmessage = "--".$uid."\r\n";
// $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
// $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
// $nmessage .= $body."\r\n\r\n";
// $nmessage .= "--".$uid."\r\n"; 
		// if($attachment !="")
	   // {	 
			// // $mail->addAttachment($attachment);
// 				   
			// $nmessage .= "Content-Type: application/octet-stream; name=\"".$attachment."\"\r\n";
			// $nmessage .= "Content-Transfer-Encoding: base64\r\n";
// 			       
			// $nmessage .= "Content-Disposition: attachment; filename=\"".$attachment."\"\r\n\r\n";
	   // }
// $nmessage .= $content."\r\n\r\n";
// $nmessage .= "--".$uid."--";
// 
// 
// 
// if (mail($email_address, $subject, $nmessage, $header)) {
    	// print_r("true...");
// die();
    // die();	
    // return true; // Or do something here
// } else {
	// print_r("false...");
	// die();
  // return false;
// }
    	
        
        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
        // $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        // if(!IS_PRODUCTION)
		// {
		 	$mail->SMTPAuth   = true;                  // enable SMTP authentication
        	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        // }
        
        // print_r(HOST.PORT.EMAIL_USERNAME.EMAIL_PASSWORD);
        // die();
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');       
        $mail->From = 'admin@goetu.com';
        $mail->FromName = 'Go3 Admin';
       if($attachment !="")
	   {	 
	   		$mail->addAttachment($attachment);
	   }
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             //$mail->addAddress($ad);
             $mail->AddAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
         
        if(!$mail->Send()) {
        	// print_r($mail->ErrorInfo());
        	// die();
            return false;//
        } else {
        	// die();
            return true;// $this->success("Message sent!");
        } 
    }
	

	
	//username , password, last_name, first_name, email_address, user_type_id, reference_id
    public function create_user($insert_data, $is_send_access = false)
    {
        //$insert_data = array_map('trim', $insert_data);
        //customer 3
        $password =$insert_data['password'];
        if(in_array($insert_data['user_type_id'], unserialize(CUSTOMER_USER_TYPE_ID)))
        {
            $insert_data['is_customer'] =1;
        }
        //agent 6
        if(in_array($insert_data['user_type_id'], unserialize(AGENT_USER_TYPE_ID)))
        {
            $insert_data['is_agent'] =1;
        }
        //merchant admin 2, 8 merchant, 7 branch merchant admin
        if(in_array($insert_data['user_type_id'], unserialize(MERCHANT_USER_TYPE_ID)))
        {
            $insert_data['is_merchant'] =1;
        }
        //iso_user_type 4, 5
        if(in_array($insert_data['user_type_id'], unserialize(ISO_USER_TYPE_ID)))
        {
            $insert_data['is_iso'] = 1;
        }   
        //super admin 1, 9 admin
        if(in_array($insert_data['user_type_id'], unserialize(ADMIN_USER_TYPE_ID)))
        {
            $insert_data['is_admin'] = 1;
        }
        
        $insert_data['password'] = $this->hideit->encrypt(sha1(md5($this->salt)),$insert_data['password'], sha1(md5($this->salt)));  
        //$insert_data['create_by'] = $_SESSION['username'];
        $insert_data['status'] = 'A';
        
		
		$insert = array(
			'username' => $insert_data['username'],
			'password' => $insert_data['password'],
			'last_name' => $insert_data['last_name'],
			'first_name' => $insert_data['first_name'],
			'email_address' => $insert_data['email_address'],
			'user_type_id' => $insert_data['user_type_id'],
			'reference_id' => $insert_data['reference_id'],
			'is_agent' => isset($insert_data['is_agent']) ? 1 : 0,
			'status' => $insert_data['status'],
		);
		
		
        if (!$this->db->insert('users', $insert)) {
            $this->db->rollback_transaction();
            $datetime = date('Y-m-d H:i:s');
            $this->save_to_action_logs('Error creating new user '  ,'ERROR',$datetime,$_SESSION['username'],'');
            return $this->failed("Unable to create new user.");
            die();
        }        
		
		
		if($is_send_access)
		{
			$first_name = $insert['first_name'];
			$last_name = $insert['last_name'];
			$username = $insert['username'];
			
			$body = "Hi {$first_name} {$last_name},";
			$body .= " Your merchant has been created.";
			$body .=" Your username: {$username}";
			$body .=" Your password: {$password}";
			
			// if(IS_PRODUCTION)
			// {
				$this->send_mail_notification($body, $insert_data['email_address'], "Merchant Account Creation(Reservation)");
			// }
		}
		
		return $this->success("User has been created.");
		
		
		                                                                
    }


    //params: action_taken, action_module, action_date, action_by, remark
    public function save_to_action_logs($action_taken, $action_module, $action_date, $action_by, $remark) {
        if (!($action_taken != '' && $action_module != '' && $action_date != '' && $action_by != '')) {
            return $this->failed('Invalid parameters');
        } else {
            if($action_module =="ERROR")
            {
                $action_taken .= " ". $this->db->get_error_string();
            }
            
            $insert_data = array(
                'action_taken'  => $action_taken,
                'action_module'            => $action_module,
                'action_date'              => $action_date,
                'action_by'         => $action_by,
                'remark'      => $remark,            
            );
            if (!$this->db->insert('action_logs', $insert_data)) {
                return $this->failed('Unable to save action_logs');
            } else {
                return $this->success('Successfully saved new action_logs');

            }
        }
    }
    
    
    // ACL PROFILE
    public function delete_module($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array('deleted'=>1);
        $this->db->update('resources', $values, 'id='.$id);
        $response = array(
            'message' => "Successfully deleted module",
        );
        return $this->success($response);
    }   
    
    
    public function add_module($params)
    {
         $params = array_map('trim', $params);
         //print_r($params);
         //die();
         
         $insert_data = array(
              'resource' => $params['resource'],
              'description' => $params['description'],
              'create_date' => date('Y-m-d H:i:s'),
              
         );           
         
         
         if (!$this->db->insert('resources', $insert_data)) 
         {
            return $this->failed("Error creating module.");
         }else {
            $response = array(
                'message' => "Successfully created module.",
            );
            return $this->success($response);
         }   
    }

    
    public function get_all_profile($where = null)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A'";

        if(!is_null($where))
            $query .= ' AND '.$where;

        $profile_list = $this->db->fetchAll($query);

        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }
    
    public function get_all_profile_selected($user_type_id)
    {
        $query = "SELECT * FROM user_types WHERE status = 'A' AND create_by =". $user_type_id;
        $profile_list = $this->db->fetchAll($query);
        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }    
    
    public function get_all_profile_access($id)
    {
        $query = 'SELECT resource_id FROM user_templates WHERE user_type_id =' . $id;
        $access_list = $this->db->fetchAll($query);

        if(!$access_list)
            return array();
        else
            return $access_list;
    }
    
    public function get_all_modules_list($where = null)
    {
        $query = 'SELECT * FROM resources where deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $query .= ' ORDER BY description';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    public function get_all_modules_list_selected($user_type_id)
    {
        //$query = 'SELECT * FROM resources where deleted = 0';
        $query = 'SELECT r.* FROM resources r inner join user_templates ut on ut.resource_id = r.id';
        $query .= ' WHERE deleted = 0 and user_type_id = ' . $this->db->db_escape($user_type_id);         
        $query .= ' ORDER BY resource';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    protected function is_profile_existing($profile_name)
    {
        $query = 'select * from user_types where description ='.$this->db->db_escape($profile_name);
        $check_profile = $this->db->fetchAll($query);
        if (count($check_profile)>0) {
           return true;
        } else {
           return false;
        }
           
    }

    public function add_profile($params)
    {
        if(count($params['module_access']) == 0 || !isset($params['module_access']))
            return $this->failed('Select a module');
            
        //print_r($params['module_access']);
        //die();

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }
        
        if($this->is_profile_existing($params['description'])){
            return $this->failed('Profile already exist!');
        }

        $data = array(
            'description'   => $params['description'],
            'create_by'    => $params['created_by']
        );

       if(!$this->db->insert('user_types', $data)) {
            return $this->failed("Unable to add profile");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
            //print_r($id);
            // delete all access from user_templates
            $this->db->delete('user_templates', 'user_type_id=' . $id);
            // insert new access rights
            foreach($params['module_access'] as $data) {
                $insert_data = array(
                    'resource_id'   => $data,
                    'user_type_id'  => $id,
                );
                $this->db->insert('user_templates', $insert_data);
            }
            return $this->success('Profile Saved');
        }
    }

    public function edit_profile($params)
    {
        if(!isset($params['module_access']) || count($params['module_access']) == 0)
            return $this->failed('You cant add a profile with no module');

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }

        if(!is_numeric($params['id']))
            return FALSE;
        
        $id = $params['id'];
        // delete all access from user_templates
        $this->db->delete('user_templates', 'user_type_id=' . $id);
        // insert new access rights
        foreach($params['module_access'] as $data) {
            $insert_data = array(
                'resource_id'   => $data,
                'user_type_id'  => $id,
            );
            $this->db->insert('user_templates', $insert_data);
        }
        return $this->success('Profile updated');
    }

    public function delete_profile($profile_id = null)
    {
        if(is_null($profile_id) || !is_numeric($profile_id))
            return $this->failed('Invalid ID');

        if($this->db->update('user_types', array('status' => 'D'), 'id='.$this->db->db_escape($profile_id))){
            $response = array(
                'message' => "Successfully deleted module",
            );
            return $this->success($response);
        }
        else{
            $response = array(
                'message' => "Unable to delete profile",
            );
            return $this->failed($response);
        }
    }
    
    public function get_all_resources()
    {
        $query = 'SELECT * FROM resources where deleted = 0';
        $query .= ' ORDER BY id';
        //print_r($query);
        //die();
        $module_list = $this->db->fetchAll($query);
        return $this->success($module_list);
    }
    
    public function update_module($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
           'resource'      => $params['value'], 
           'description'    => $params['description'],
        );
        if (!($this->db->update('resources', $update_data, 'id=' . $params['id']))) {
            $response = array(
                'success' => false,
                'message' => 'Unable to update module.',
            );
            return $this->failed($response);    
        } else {
            $response = array(
                'success' => true,
                'message' => 'Successfully updated module',
            );
            return $this->success($response);
        }
    }
    
    // End of New code for go3 rewards
    
    public function replace_schar($val){
        
          $replace = array(
                    '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                    '&quot;' => '',
                    'À' => 'A', '??' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                    '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                    'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', '??' => 'D',
                    '??' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                    'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                    'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', '??' => 'I',
                    'Î' => 'I', '??' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                    'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                    'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                    'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                    'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                    'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                    'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                    '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                    '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                    'W' => 'W', '??' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                    'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                    'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                    'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                    'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                    'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                    'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                    'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                    'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                    'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                    '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                    '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                    '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                    'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                    'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                    'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                    'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                    '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                    '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                    '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                    '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                    '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                    '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                    '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                    '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                    '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                    '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                    '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                    '?' => 'yu', '?' => 'ya', "'"=>"`"
                    );
                    
                    $final = str_replace(array_keys($replace), $replace, $val);
        return $final;
        
    }
        
    //=============LOOKUP FUNCTIONS=====================    
    
    public function validate_session($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']) && strlen($params['session_id']) == 32)) {
            return $this->failed('Invalid or expired session id');
        } else {
            $chk = $this->db->fetchRow('select * from sessions where session_id='. $this->db->db_escape($params['session_id']));
            if (!(isset($chk['session_id']))) {
                return $this->failed('Invalid or expired session id');
            } elseif (isset($chk['logout_date']) && $chk['logout_date'] != '') {
                return $this->failed('Invalid or expired session id');
            //} elseif (strtotime($chk['last_seen']) + (3600) < time()) {
            // } elseif (strtotime($chk['last_seen']) + (600) < time()) {
            // //} elseif (strtotime($chk['last_seen']) + (30) < time()) {
                // return $this->failed('Invalid or expired session id');
            } 
            else {
                $values = array("last_seen" => date('Y-m-d H:i:s'));
                $this->db->update('sessions', $values, 'session_id=' . $this->db->db_escape($params['session_id']));
                //return $this->success('session id valid');
				$user_info = unserialize($chk['session_data']);
				return $this->success($user_info);
            }
        }
    }
 
    public function get_user_by_id($params) 
    {
        $query = "select u.user_center, u.is_center, CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type WHERE u.id = ".$params['id'];

        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    
    public function get_users_detailed_selected($user_type_id) {
        $query = 'select u.status, u.id,fname,lname,username,u.email_address,mobile_number,user_type_id,ut.description ' ;                $query .= 'user_type_desc from users u ';
        $query .= 'inner join user_types ut on ut.id=u.user_type_id ';
        $query .= 'where ut.created_by=' . $user_type_id;
/*        print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_list()
    {
        $query="SELECT id,username,password,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'I' THEN 'INACTIVE' ELSE 'PENDING' END as status";
        $query.=",date_created,created_by,fname";
        $query.=",lname,email_address,mobile_number,address,user_type,customers_id,agent_id FROM users";
        $users= $this->db->fetchAll($query);
        $this->success($users); 
        $response = $this->get_response(); 
        return    $response;
    }
	
	public function get_user_type_by_username($username){
        $sql = "SELECT ut.* FROm users u LEFT JOIN user_types ut ON u.user_type_id=ut.id WHERE u.username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchAll($sql);
        return $this->success($result);  
    }
    
    public function generate_password() {
        return substr(md5(uniqid(time(), true)), 5,8);
    }

    protected function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } else {
                    if (file_exists($dst . '/' . $file)) @unlink($dst . '/' . $file);
                    copy($src . '/' . $file, $dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }
                                   
    //params: username, password
    public function login($params) {
        $params = array_map('trim', $params);

        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password 3');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            
			 // print_r("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
			 // die();
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type_id=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and u.status='A'");
        
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type_id']);
            
                $user_check['fname'] = $user_check['first_name'];
                $user_check['lname'] = $user_check['last_name'];
				
				
				if($user_check['user_type_desc'] =="CUSTOMER")
				{
					return $this->failed("Invalid access rights. Customer is not allowed.");
				}
				
				$profile = '';
				if($user_check['is_iso'])
				{
					$profile = 'iso';
				}elseif($user_check['is_merchant']){
					$profile = 'merchants';
				}elseif ($user_check['is_agent']){
					$profile = 'agents';
				}else{
					$profile = 'customers';
				}				
				
				$id = $user_check['reference_id'];
				// print_r($id);
				// die();
				// $user_info = $this->get_profile($profile, $id);
				$user_info = array();
				
				//$user_info['profile'] = $profile;
				
				$user_info['user_id'] = $user_check['id'];
				$user_info['partner_id'] = $id;
				$user_info['is_agent'] = $user_check['is_agent'];
				$user_info['user_type'] = $user_check['user_type_id'];
                $user_info['user_type_desc'] = $user_check['user_type_desc'];
				$user_info['username'] = $params['username'];
				// $user_info = serialize($user_info);
				
                $session_id = $this->create_session($user_check['id'], $user_info);
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => count($permissions) > 0 ? implode(';', $permissions) : "",
                    'user_type' => $user_check['user_type_id'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'username' => $user_check['username'],
                    'user_info' => $user_info,
                );
                
                return $this->success($response);
            }
        }
    }
    
    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = $this->db->fetchRow('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
	
    protected function create_session($user_id, $session_data = "") {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
            //'session_data' => $session_data,
        );
		if(is_array($session_data))
		{
			$insert_data['session_data'] = serialize($session_data);
		}else
		{
			$insert_data['session_data'] =  $session_data;
		}
		
        $this->db->insert('sessions', $insert_data);
        return $session_id;        
    }

    //user_id
    public function get_user_permissions($user_id, $user_type_id) {        
        $permissions = $this->db->fetchPairs('select p.id,r.resource from permissions p inner join resources r on p.resource_id=r.id where p.user_id=' . $this->db->db_escape($user_id));

        
        $permissions2 = $this->db->fetchPairs('select ut.id,r.resource from user_templates ut inner join resources r on ut.resource_id=r.id where ut.user_type_id=' . $this->db->db_escape($user_type_id));
        $permissions = array_flip(array_merge(array_flip($permissions), array_flip($permissions2)));
        

        if (is_array($permissions) && count($permissions) > 0) {
            return $permissions;
        }
    }

    //params: session_id
    public function logout($params) {//note: session maintenance is not yet implemented in backend.php, client must implement its own
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']))) {
            return $this->failed('Invalid session ID');
        } else {
            $check = $this->db->fetchRow('select * from sessions where logout_date is null and session_id=' . $this->db->db_escape($params['session_id']));
            if (!(isset($check['id']))) {
                return $this->failed('Invalid session ID');
            } else {
                $update_data = array(
                    'logout_date' => date('Y-m-d H:i:s'),
                );
                if (!$this->db->update('sessions', $update_data, 'id='. $check['id'])) {
                    return $this->failed('Unable to logout');
                } else {
                    return $this->success('Successfully logged out user.');
                }
            }
        }
    }

    
    protected function failed($message) {
        $this->response = array(
            'ResponseCode' => '0001',
            'ResponseMessage' => $message,
        );
        return false;
    }
    
    protected function success($message, $record_count=-1) {
        $this->response = array(
            'ResponseCode' => '0000',
            'ResponseMessage' => $message,
        );
        return true;
    }
    
    public function get_response() {
        return $this->response;
    }

     // ACL
    public function get_module_name($module_list = array(), $include_action = false)
    {
        if(count($module_list) > 0) {
            $return_arr = array();
            $query = "SELECT * FROM acl_modules WHERE id IN(".implode(',', $module_list).") AND main = 1";
            $module_list = $this->db->fetchAll($query);

            foreach($module_list as $module) {
                $return_arr[] = $module['module_class'];
                $return_arr[] = $module['main_module_index'];
                if($include_action)
                    $return_arr[] = $module['module_action'];
            }

            $result = array_unique($return_arr);
            return $result;
        } else {
            return FALSE;
        }
    }

    public function remove_module_buttons($class)
    {
        $module_btn_return = array();

        $all_module_btn = $this->db->fetchAll('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND with_button = 1');
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];
        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_access = $this->db->fetchRow($query);
        $module_access = explode(',', $module_access['module_access']);

        if($all_module_btn && $module_access) {
            foreach($all_module_btn as $btn) {
                if(in_array($btn['id'], $module_access)) {
                    $module_btn_return[$btn['module_action']] = '';
                } else {
                    $module_btn_return[$btn['module_action']] = 'display: none;';
                }
            }
        }

        return $module_btn_return;
    }

    public function get_all_modules()
    {
        $return_arr = array();
        $query = "SELECT * FROM acl_modules"; // check if it will be a 1d array if i just get the id instead of all
        $all_modules = $this->db->fetchAll($query);

        foreach($all_modules as $module)
            $return_arr[] = $module['id'];

        return $return_arr;
    }

    public function get_user_modules()
    {
        $user_info = $this->get_user_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['user_type'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            $module_list = explode(',', $module_list['module_access']);
            $all_modules = $this->get_all_modules();

            $unset_modules = array_diff($all_modules, $module_list);

            if(count($unset_modules) > 0)
                return $unset_modules;
            else
                return FALSE;
        }
    }

    public function get_user_access_list()
    {
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            return explode(',', $module_list['module_access']);
        } else {
            return array();
        }
    }

    public function get_module_id($class, $action)
    {
        $module_info = $this->db->fetchRow('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND module_action = '.$this->db->db_escape($action));

        if(!$module_info)
            return FALSE;

        $module_id = $module_info['id'];
        $user_list = $this->get_user_access_list();

        if(in_array($module_id, $user_list)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

     public function get_pan_key() {                                                     
        //$query = $this->db->select('key')->from('keys')->order_by('timestamp', 'desc')->get();
        $query = $this->db->fetchRow("SELECT `key` FROM `keys` ORDER by `timestamp` desc");
        if (!$query) {
            //$key = $query->result_array();
            $dek_enc = $query['key'];
            $iv = sha1('aes-256-cbc');
            //$kek = file_get_contents('http://192.168.2.16/web/i.php?token=cfbe176207b80774e8911c10893f5a0f');
            $kek="cfbe176207b80774e8911c10893f5a0f";
            $dek_plain = $this->hideit->decrypt($kek, $dek_enc, $iv);
            return $dek_plain;
        } else {
            return '';
        }
    }  
    
    
}
