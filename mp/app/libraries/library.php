<?php
class lib {
    public static $ws_flag = false;

    public static function setTimeZone($time_zone, $db = NULL) {
		date_default_timezone_set($time_zone);
        if ($db !== NULL) {
            $dateTimeZoneGmt = new DateTimeZone("GMT");
            $dateTimeZoneCustom = new DateTimeZone($time_zone);
            $dateTimeGmt = new DateTime(date('Y-m-d'), $dateTimeZoneGmt);
            $timeOffset = $dateTimeZoneCustom->getOffset($dateTimeGmt) / 3600;
            $plus = ((int) $timeOffset > 0) ? '+' : '';
            $tz = $plus . $timeOffset;
            $db->query("SET time_zone = '{$tz}:00'");
        }
    }
    
    public static function getWsResponse($endpoint, $method, $params = NULL) {
        if ($params !== NULL && count($params) > 0) {
            $urlParams = '&' . http_build_query($params);
        } else {
            $urlParams = NULL;
        }
        $url = $endpoint . '?method=' . $method . $urlParams;
        $result = file_get_contents($url);
        self::$ws_flag = true;
        return json_decode($result, true);
//        $result = simplexml_load_string($result);
//        return self::simplexml2ISOarray($result);
    }


    public static function simplexml2ISOarray($xml, $attribsAsElements = 0) {
        if (@get_class($xml) == 'SimpleXMLElement') {
            $attributes = $xml->attributes();
            foreach($attributes as $k=>$v) {
                if ($v) $a[$k] = (string) $v;
            }
            $x = $xml;
            $xml = get_object_vars($xml);
        }
        if (is_array($xml)) {
            if (count($xml) == 0) return (string) $x; // for CDATA
            foreach($xml as $key=>$value) {
                $r[$key] = self::simplexml2ISOarray($value,$attribsAsElements);
                if (!is_array($r[$key])) $r[$key] = utf8_decode($r[$key]);
            }
            if (isset($a)) {
                if($attribsAsElements) {
                    $r = array_merge($a,$r);
                } else {
                    $r['@'] = $a; // Attributes
                }
            }
            return $r;
        }
        return $xml;
    }
}