<?php

class Crypt {
    private $td;
    private $iv;
    private $ks;

    function open() {
        $this->td = mcrypt_module_open('tripledes', '', 'ecb', '');
        $this->iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($this->td), MCRYPT_DEV_RANDOM);
        $this->ks = mcrypt_enc_get_key_size($this->td);
    }
    
    function close() {
        mcrypt_generic_deinit($this->td);
        mcrypt_module_close($this->td);
    }

    function encrypt($key, $data) {
        $this->open();
        $key = substr(md5($key), 0, $this->ks);
        mcrypt_generic_init($this->td, $key, $this->iv);
        $encrypted = base64_encode(mcrypt_generic($this->td, $data));
        $this->close();
        return $encrypted;
    }

    function decrypt($key, $data) {
        $this->open();
        $key = substr(md5($key), 0, $this->ks);
        mcrypt_generic_init($this->td, $key, $this->iv);
        $decrypted = mdecrypt_generic($this->td, base64_decode($data));
        $this->close();
        return $decrypted;
    }
}