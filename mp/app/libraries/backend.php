<?php
class backend {
    protected $response;
    protected $db;
    public $salt;

    
    public function __construct(&$db) {
        $this->salt = 'webpol';  
        $this->salt2 = '2015webpol2015';
        $this->db = $db;
        require_once 'xorry.php';
        require_once 'class.phpmailer.php';
        require_once dirname(__DIR__) . '/libraries/HideIt.php';
        $this->hideit = new HideIt('aes-256-cbc');
    }
	

	
	
    public function get_all_managers()
    {
        $cmd="select id, concat(fname,' ', lname) as name from users where user_type = 5 and status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
    public function check_user_if_exist($params)
    {
         
        $params = array_map('trim', $params);
        $row = $this->db->fetchRow("select * from users where status = 'A' and username=" . $this->db->db_escape($params['username']));   
        if (!(isset($row['id']))) {
              
            return false;
        } else {
              
            return true;
        }
    }
    
      public function replace_schar($val){
        
          $replace = array(
                    '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
                    '&quot;' => '',
                    'À' => 'A', '�?' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
                    '&Auml;' => 'A', 'Å' => 'A', 'A' => 'A', 'A' => 'A', 'A' => 'A', 'Æ' => 'Ae',
                    'Ç' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'C' => 'C', 'D' => 'D', '�?' => 'D',
                    '�?' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'E' => 'E',
                    'E' => 'E', 'E' => 'E', 'E' => 'E', 'E' => 'E', 'G' => 'G', 'G' => 'G',
                    'G' => 'G', 'G' => 'G', 'H' => 'H', 'H' => 'H', 'Ì' => 'I', '�?' => 'I',
                    'Î' => 'I', '�?' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I', 'I' => 'I',
                    'I' => 'I', '?' => 'IJ', 'J' => 'J', 'K' => 'K', 'L' => 'K', 'L' => 'K',
                    'L' => 'K', 'L' => 'K', '?' => 'K', 'Ñ' => 'N', 'N' => 'N', 'N' => 'N',
                    'N' => 'N', '?' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
                    'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'O' => 'O', 'O' => 'O', 'O' => 'O',
                    'Œ' => 'OE', 'R' => 'R', 'R' => 'R', 'R' => 'R', 'S' => 'S', 'Š' => 'S',
                    'S' => 'S', 'S' => 'S', '?' => 'S', 'T' => 'T', 'T' => 'T', 'T' => 'T',
                    '?' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'U' => 'U',
                    '&Uuml;' => 'Ue', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U', 'U' => 'U',
                    'W' => 'W', '�?' => 'Y', 'Y' => 'Y', 'Ÿ' => 'Y', 'Z' => 'Z', 'Ž' => 'Z',
                    'Z' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
                    'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'a' => 'a', 'a' => 'a', 'a' => 'a',
                    'æ' => 'ae', 'ç' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c', 'c' => 'c',
                    'd' => 'd', 'd' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
                    'ë' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e', 'e' => 'e',
                    'ƒ' => 'f', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'g' => 'g', 'h' => 'h',
                    'h' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'i' => 'i',
                    'i' => 'i', 'i' => 'i', 'i' => 'i', 'i' => 'i', '?' => 'ij', 'j' => 'j',
                    'k' => 'k', '?' => 'k', 'l' => 'l', 'l' => 'l', 'l' => 'l', 'l' => 'l',
                    '?' => 'l', 'ñ' => 'n', 'n' => 'n', 'n' => 'n', 'n' => 'n', '?' => 'n',
                    '?' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
                    '&ouml;' => 'oe', 'ø' => 'o', 'o' => 'o', 'o' => 'o', 'o' => 'o', 'œ' => 'oe',
                    'r' => 'r', 'r' => 'r', 'r' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
                    'û' => 'u', 'ü' => 'ue', 'u' => 'u', '&uuml;' => 'ue', 'u' => 'u', 'u' => 'u',
                    'u' => 'u', 'u' => 'u', 'u' => 'u', 'w' => 'w', 'ý' => 'y', 'ÿ' => 'y',
                    'y' => 'y', 'ž' => 'z', 'z' => 'z', 'z' => 'z', 'þ' => 't', 'ß' => 'ss',
                    '?' => 'ss', '??' => 'iy', '?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G',
                    '?' => 'D', '?' => 'E', '?' => 'YO', '?' => 'ZH', '?' => 'Z', '?' => 'I',
                    '?' => 'Y', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
                    '?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F',
                    '?' => 'H', '?' => 'C', '?' => 'CH', '?' => 'SH', '?' => 'SCH', '?' => '',
                    '?' => 'Y', '?' => '', '?' => 'E', '?' => 'YU', '?' => 'YA', '?' => 'a',
                    '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo',
                    '?' => 'zh', '?' => 'z', '?' => 'i', '?' => 'y', '?' => 'k', '?' => 'l',
                    '?' => 'm', '?' => 'n', '?' => 'o', '?' => 'p', '?' => 'r', '?' => 's',
                    '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c', '?' => 'ch',
                    '?' => 'sh', '?' => 'sch', '?' => '', '?' => 'y', '?' => '', '?' => 'e',
                    '?' => 'yu', '?' => 'ya', "'"=>"`"
                    );
                    
                    $final = str_replace(array_keys($replace), $replace, $val);
        return $final;
        
    }
    public function get_email_list()
    {
        
    }
    public function  check_if_has_callables($po_id)
    {
        $cmd = "select ptc.status_code,sct.name from project_template_code ptc
        INNER JOIN status_code_type sct on  ptc.type = sct.id 
        INNER JOIN project_template pt on pt.id = ptc.project_template_id
        INNER JOIN po p on p.project_template_id=pt.id
        INNER JOIN po_transaction_data_ans_2_{$po_id}  ptds on p.id = ptds.po_id 
        where sct.name = 'Callable' and  p.id = {$po_id}";
        $rs = $this->db->fetchAll($cmd);
        
        if(count($rs)>0){
            return 1;    
        }else{
            return 2;
        }
        
        
    }
    
    public function  get_number_complete($po_id)
    {
        $cmd = "select ptc.status_code,sct.name from project_template_code ptc
        INNER JOIN status_code_type sct on  ptc.type = sct.id 
        INNER JOIN project_template pt on pt.id = ptc.project_template_id
        INNER JOIN po p on p.project_template_id=pt.id
        INNER JOIN po_transaction_data_ans_2_{$po_id}  ptds on p.id = ptds.po_id  AND UPPER(ptds.status_code)=UPPER(ptc.status_code)
        where sct.name = 'Complete' and  p.id = {$po_id}";
      
        $rs = $this->db->fetchAll($cmd);
        
        if(count($rs)>0){
            return count($rs);    
        }else{
            return 0;
        }
        
        
    }
    
    
    public function get_po_code_name($id)
    {
        $cmd = "select name,po_no from po where id ={$id}";
        $rs = $this->db->fetchRow($cmd);
        return $rs;
    }
       public function get_goal_history($params)
    {
        $cmd = "";
        
        $rs = $this->db->fetchAll($cmd);
        return $this->success($rs);
    }
    
    public function get_email_by_username($username){
        //System Admin, Admin, Manager, PO creator
        $emails="";
         $cmd = "select email_address from users u 
         where u.user_type in(1,2,5) or u.username='{$username}' and u.status = 'A'";
        $rs = $this->db->fetchAll($cmd);

        return $rs;
    }
     //Abby 3-14-16 Added goal_days_of_week and goal_value
     public function get_po_schedule_all()
     { 
        $cmd="select p.po_no, st.code as state, p.name ,goal.goal_days_of_week, goal.goal_value, CASE goal.goal_type WHEN '1' THEN 'Saturation' WHEN '2' THEN 'Targets' WHEN '3' THEN 'Stop Number' WHEN '4' THEN 'Hours' END as goal_type, prm.end_date, prm.caller_id, prm.goal_note , 'TEST' as center FROM po p INNER JOIN po_parameter prm ON p.id = prm.po_id INNER JOIN po_goal goal ON p.id = goal.po_id INNER JOIN states st ON prm.state=st.id";
         
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    
  }
    
    
    public function create_po_transaction_tables($po_id)
    {
        //po_transaction_data2_nos_
        $cmd ="CREATE TABLE `po_transaction_data2_nos_{$po_id}` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `phone_no` varchar(20) DEFAULT NULL,
            `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `po_id` int(11) DEFAULT NULL,
            `batch_no` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `indexforphoneandpo_id` (`phone_no`,`po_id`),
            KEY `phonenopoid` (`phone_no`,`po_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=4096 DEFAULT CHARSET=latin1;";
        $this->db->query($cmd);
        
        //po_transaction_data_2_
        $cmd = "CREATE TABLE `po_transaction_data_2_{$po_id}` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `po_id` int(11) NOT NULL,
      `batch_no` int(11) DEFAULT NULL,
      `is_column` int(11) DEFAULT NULL,
      `center_id` int(11) DEFAULT NULL,
      `phone_no` varchar(32) DEFAULT NULL,
      `phone_field` varchar(32) DEFAULT NULL,
      `lname_field` varchar(32) DEFAULT NULL,
      `fname_field` varchar(32) DEFAULT NULL,
      `process_option_id` int(11) DEFAULT NULL,
      `remarks` text,
      `col_fields` text,
      `misc_fields` text,
      `status` char(1) DEFAULT NULL,
      `list_cnt` int(11) DEFAULT NULL,
      `create_by` varchar(32) DEFAULT NULL,
      `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `col1` varchar(100) DEFAULT NULL,
      `col2` varchar(100) DEFAULT NULL,
      `col3` varchar(100) DEFAULT NULL,
      `col4` varchar(100) DEFAULT NULL,
      `col5` varchar(100) DEFAULT NULL,
      `col6` varchar(100) DEFAULT NULL,
      `col7` varchar(100) DEFAULT NULL,
      `col8` varchar(100) DEFAULT NULL,
      `col9` varchar(100) DEFAULT NULL,
      `col10` varchar(100) DEFAULT NULL,
      `col11` varchar(100) DEFAULT NULL,
      `col12` varchar(100) DEFAULT NULL,
      `col13` varchar(100) DEFAULT NULL,
      `col14` varchar(100) DEFAULT NULL,
      `col15` varchar(100) DEFAULT NULL,
      `col16` varchar(100) DEFAULT NULL,
      `col17` varchar(100) DEFAULT NULL,
      `col18` varchar(100) DEFAULT NULL,
      `col19` varchar(100) DEFAULT NULL,
      `col20` varchar(100) DEFAULT NULL,
      `col21` varchar(100) DEFAULT NULL,
      `col22` varchar(100) DEFAULT NULL,
      `col23` varchar(100) DEFAULT NULL,
      `col24` varchar(100) DEFAULT NULL,
      `col25` varchar(100) DEFAULT NULL,
      `col26` varchar(100) DEFAULT NULL,
      `col27` varchar(100) DEFAULT NULL,
      `col28` varchar(100) DEFAULT NULL,
      `col29` varchar(100) DEFAULT NULL,
      `col30` varchar(100) DEFAULT NULL,
      `col31` varchar(100) DEFAULT NULL,
      `col32` varchar(100) DEFAULT NULL,
      `col33` varchar(100) DEFAULT NULL,
      `col34` varchar(100) DEFAULT NULL,
      `col35` varchar(100) DEFAULT NULL,
      `col36` varchar(100) DEFAULT NULL,
      `col37` varchar(100) DEFAULT NULL,
      `col38` varchar(100) DEFAULT NULL,
      `col39` varchar(100) DEFAULT NULL,
      `col40` varchar(100) DEFAULT NULL,
      `col41` varchar(100) DEFAULT NULL,
      `col42` varchar(100) DEFAULT NULL,
      `col43` varchar(100) DEFAULT NULL,
      `col44` varchar(100) DEFAULT NULL,
      `col45` varchar(100) DEFAULT NULL,
      `col46` varchar(100) DEFAULT NULL,
      `col47` varchar(100) DEFAULT NULL,
      `col48` varchar(100) DEFAULT NULL,
      `col49` varchar(100) DEFAULT NULL,
      `col50` varchar(100) DEFAULT NULL,
      `col51` varchar(100) DEFAULT NULL,
      `col52` varchar(100) DEFAULT NULL,
      `col53` varchar(100) DEFAULT NULL,
      `col54` varchar(100) DEFAULT NULL,
      `col55` varchar(100) DEFAULT NULL,
      `col56` varchar(100) DEFAULT NULL,
      `col57` varchar(100) DEFAULT NULL,
      `col58` varchar(100) DEFAULT NULL,
      `col59` varchar(100) DEFAULT NULL,
      `col60` varchar(100) DEFAULT NULL,
      `col61` varchar(100) DEFAULT NULL,
      `col62` varchar(100) DEFAULT NULL,
      `col63` varchar(100) DEFAULT NULL,
      `col64` varchar(100) DEFAULT NULL,
      `col65` varchar(100) DEFAULT NULL,
      `col66` varchar(100) DEFAULT NULL,
      `col67` varchar(100) DEFAULT NULL,
      `col68` varchar(100) DEFAULT NULL,
      `col69` varchar(100) DEFAULT NULL,
      `col70` varchar(100) DEFAULT NULL,
      `col71` varchar(100) DEFAULT NULL,
      `col72` varchar(100) DEFAULT NULL,
      `col73` varchar(100) DEFAULT NULL,
      `col74` varchar(100) DEFAULT NULL,
      `col75` varchar(100) DEFAULT NULL,
      `col76` varchar(100) DEFAULT NULL,
      `col77` varchar(100) DEFAULT NULL,
      `col78` varchar(100) DEFAULT NULL,
      `col79` varchar(100) DEFAULT NULL,
      `col80` varchar(100) DEFAULT NULL,
      `col81` varchar(100) DEFAULT NULL,
      `col82` varchar(100) DEFAULT NULL,
      `col83` varchar(100) DEFAULT NULL,
      `col84` varchar(100) DEFAULT NULL,
      `col85` varchar(100) DEFAULT NULL,
      `col86` varchar(100) DEFAULT NULL,
      `col87` varchar(100) DEFAULT NULL,
      `col88` varchar(100) DEFAULT NULL,
      `col89` varchar(100) DEFAULT NULL,
      `col90` varchar(100) DEFAULT NULL,
      `col91` varchar(100) DEFAULT NULL,
      `col92` varchar(100) DEFAULT NULL,
      `col93` varchar(100) DEFAULT NULL,
      `col94` varchar(100) DEFAULT NULL,
      `col95` varchar(100) DEFAULT NULL,
      `col96` varchar(100) DEFAULT NULL,
      `col97` varchar(100) DEFAULT NULL,
      `col98` varchar(100) DEFAULT NULL,
      `col99` varchar(100) DEFAULT NULL,
      `col100` varchar(100) DEFAULT NULL,
      `col500` varchar(100) DEFAULT NULL,
      `col101` varchar(100) DEFAULT NULL,
      `col102` varchar(100) DEFAULT NULL,
      `col103` varchar(100) DEFAULT NULL,
      `col104` varchar(100) DEFAULT NULL,
      `col105` varchar(100) DEFAULT NULL,
      `col106` varchar(100) DEFAULT NULL,
      `col107` varchar(100) DEFAULT NULL,
      `col108` varchar(100) DEFAULT NULL,
      `col109` varchar(100) DEFAULT NULL,
      `col110` varchar(100) DEFAULT NULL,
      `col111` varchar(100) DEFAULT NULL,
      `col112` varchar(100) DEFAULT NULL,
      `col113` varchar(100) DEFAULT NULL,
      `col114` varchar(100) DEFAULT NULL,
      `col115` varchar(100) DEFAULT NULL,
      `col116` varchar(100) DEFAULT NULL,
      `col117` varchar(100) DEFAULT NULL,
      `col118` varchar(100) DEFAULT NULL,
      `col119` varchar(100) DEFAULT NULL,
      `col120` varchar(100) DEFAULT NULL,
      `col121` varchar(100) DEFAULT NULL,
      `col122` varchar(100) DEFAULT NULL,
      `col123` varchar(100) DEFAULT NULL,
      `col124` varchar(100) DEFAULT NULL,
      `col125` varchar(100) DEFAULT NULL,
      `col126` varchar(100) DEFAULT NULL,
      `col127` varchar(100) DEFAULT NULL,
      `col128` varchar(100) DEFAULT NULL,
      `col129` varchar(100) DEFAULT NULL,
      `col130` varchar(100) DEFAULT NULL,
      `col131` varchar(100) DEFAULT NULL,
      `col132` varchar(100) DEFAULT NULL,
      `col133` varchar(100) DEFAULT NULL,
      `col134` varchar(100) DEFAULT NULL,
      `col135` varchar(100) DEFAULT NULL,
      `col136` varchar(100) DEFAULT NULL,
      `col137` varchar(100) DEFAULT NULL,
      `col138` varchar(100) DEFAULT NULL,
      `col139` varchar(100) DEFAULT NULL,
      `col140` varchar(100) DEFAULT NULL,
      `col141` varchar(100) DEFAULT NULL,
      `col142` varchar(100) DEFAULT NULL,
      `col143` varchar(100) DEFAULT NULL,
      `col144` varchar(100) DEFAULT NULL,
      `col145` varchar(100) DEFAULT NULL,
      `col146` varchar(100) DEFAULT NULL,
      `col147` varchar(100) DEFAULT NULL,
      `col148` varchar(100) DEFAULT NULL,
      `col149` varchar(100) DEFAULT NULL,
      `col150` varchar(100) DEFAULT NULL,
      `col151` varchar(100) DEFAULT NULL,
      `col152` varchar(100) DEFAULT NULL,
      `col153` varchar(100) DEFAULT NULL,
      `col154` varchar(100) DEFAULT NULL,
      `col155` varchar(100) DEFAULT NULL,
      `col156` varchar(100) DEFAULT NULL,
      `col157` varchar(100) DEFAULT NULL,
      `col158` varchar(100) DEFAULT NULL,
      `col159` varchar(100) DEFAULT NULL,
      `col160` varchar(100) DEFAULT NULL,
      `col161` varchar(100) DEFAULT NULL,
      `col162` varchar(100) DEFAULT NULL,
      `col163` varchar(100) DEFAULT NULL,
      `col164` varchar(100) DEFAULT NULL,
      `col165` varchar(100) DEFAULT NULL,
      `col166` varchar(100) DEFAULT NULL,
      `col167` varchar(100) DEFAULT NULL,
      `col168` varchar(100) DEFAULT NULL,
      `col169` varchar(100) DEFAULT NULL,
      `col170` varchar(100) DEFAULT NULL,
      `col171` varchar(100) DEFAULT NULL,
      `col172` varchar(100) DEFAULT NULL,
      `col173` varchar(100) DEFAULT NULL,
      `col174` varchar(100) DEFAULT NULL,
      `col175` varchar(100) DEFAULT NULL,
      `col176` varchar(100) DEFAULT NULL,
      `col177` varchar(100) DEFAULT NULL,
      `col178` varchar(100) DEFAULT NULL,
      `col179` varchar(100) DEFAULT NULL,
      `col180` varchar(100) DEFAULT NULL,
      `col181` varchar(100) DEFAULT NULL,
      `col182` varchar(100) DEFAULT NULL,
      `col183` varchar(100) DEFAULT NULL,
      `col184` varchar(100) DEFAULT NULL,
      `col185` varchar(100) DEFAULT NULL,
      `col186` varchar(100) DEFAULT NULL,
      `col187` varchar(100) DEFAULT NULL,
      `col188` varchar(100) DEFAULT NULL,
      `col189` varchar(100) DEFAULT NULL,
      `col190` varchar(100) DEFAULT NULL,
      `col191` varchar(100) DEFAULT NULL,
      `col192` varchar(100) DEFAULT NULL,
      `col193` varchar(100) DEFAULT NULL,
      `col194` varchar(100) DEFAULT NULL,
      `col195` varchar(100) DEFAULT NULL,
      `col196` varchar(100) DEFAULT NULL,
      `col197` varchar(100) DEFAULT NULL,
      `col198` varchar(100) DEFAULT NULL,
      `col199` varchar(100) DEFAULT NULL,
      `col200` varchar(100) DEFAULT NULL,
      `col201` varchar(100) DEFAULT NULL,
      `col202` varchar(100) DEFAULT NULL,
      `col203` varchar(100) DEFAULT NULL,
      `col204` varchar(100) DEFAULT NULL,
      `col205` varchar(100) DEFAULT NULL,
      `col206` varchar(100) DEFAULT NULL,
      `col207` varchar(100) DEFAULT NULL,
      `col208` varchar(100) DEFAULT NULL,
      `col209` varchar(100) DEFAULT NULL,
      `col210` varchar(100) DEFAULT NULL,
      `col211` varchar(100) DEFAULT NULL,
      `col212` varchar(100) DEFAULT NULL,
      `col213` varchar(100) DEFAULT NULL,
      `col214` varchar(100) DEFAULT NULL,
      `col215` varchar(100) DEFAULT NULL,
      `col216` varchar(100) DEFAULT NULL,
      `col217` varchar(100) DEFAULT NULL,
      `col218` varchar(100) DEFAULT NULL,
      `col219` varchar(100) DEFAULT NULL,
      `col220` varchar(100) DEFAULT NULL,
      `col221` varchar(100) DEFAULT NULL,
      `col222` varchar(100) DEFAULT NULL,
      `col223` varchar(100) DEFAULT NULL,
      `col224` varchar(100) DEFAULT NULL,
      `col225` varchar(100) DEFAULT NULL,
      `col226` varchar(100) DEFAULT NULL,
      `col227` varchar(100) DEFAULT NULL,
      `col228` varchar(100) DEFAULT NULL,
      `col229` varchar(100) DEFAULT NULL,
      `col230` varchar(100) DEFAULT NULL,
      `col231` varchar(100) DEFAULT NULL,
      `col232` varchar(100) DEFAULT NULL,
      `col233` varchar(100) DEFAULT NULL,
      `col234` varchar(100) DEFAULT NULL,
      `col235` varchar(100) DEFAULT NULL,
      `col236` varchar(100) DEFAULT NULL,
      `col237` varchar(100) DEFAULT NULL,
      `col238` varchar(100) DEFAULT NULL,
      `col239` varchar(100) DEFAULT NULL,
      `col240` varchar(100) DEFAULT NULL,
      `col241` varchar(100) DEFAULT NULL,
      `col242` varchar(100) DEFAULT NULL,
      `col243` varchar(100) DEFAULT NULL,
      `col244` varchar(100) DEFAULT NULL,
      `col245` varchar(100) DEFAULT NULL,
      `col246` varchar(100) DEFAULT NULL,
      `col247` varchar(100) DEFAULT NULL,
      `col248` varchar(100) DEFAULT NULL,
      `col249` varchar(100) DEFAULT NULL,
      `col250` varchar(100) DEFAULT NULL,
      `col251` varchar(100) DEFAULT NULL,
      `col252` varchar(100) DEFAULT NULL,
      `col253` varchar(100) DEFAULT NULL,
      `col254` varchar(100) DEFAULT NULL,
      `col255` varchar(100) DEFAULT NULL,
      `col256` varchar(100) DEFAULT NULL,
      `col257` varchar(100) DEFAULT NULL,
      `col258` varchar(100) DEFAULT NULL,
      `col259` varchar(100) DEFAULT NULL,
      `col260` varchar(100) DEFAULT NULL,
      `col261` varchar(100) DEFAULT NULL,
      `col262` varchar(100) DEFAULT NULL,
      `col263` varchar(100) DEFAULT NULL,
      `col264` varchar(100) DEFAULT NULL,
      `col265` varchar(100) DEFAULT NULL,
      `col266` varchar(100) DEFAULT NULL,
      `col267` varchar(100) DEFAULT NULL,
      `col268` varchar(100) DEFAULT NULL,
      `col269` varchar(100) DEFAULT NULL,
      `col270` varchar(100) DEFAULT NULL,
      `col271` varchar(100) DEFAULT NULL,
      `col272` varchar(100) DEFAULT NULL,
      `col273` varchar(100) DEFAULT NULL,
      `col274` varchar(100) DEFAULT NULL,
      `col275` varchar(100) DEFAULT NULL,
      `col276` varchar(100) DEFAULT NULL,
      `col277` varchar(100) DEFAULT NULL,
      `col278` varchar(100) DEFAULT NULL,
      `col279` varchar(100) DEFAULT NULL,
      `col280` varchar(100) DEFAULT NULL,
      `col281` varchar(100) DEFAULT NULL,
      `col282` varchar(100) DEFAULT NULL,
      `col283` varchar(100) DEFAULT NULL,
      `col284` varchar(100) DEFAULT NULL,
      `col285` varchar(100) DEFAULT NULL,
      `col286` varchar(100) DEFAULT NULL,
      `col287` varchar(100) DEFAULT NULL,
      `col288` varchar(100) DEFAULT NULL,
      `col289` varchar(100) DEFAULT NULL,
      `col290` varchar(100) DEFAULT NULL,
      `col291` varchar(100) DEFAULT NULL,
      `col292` varchar(100) DEFAULT NULL,
      `col293` varchar(100) DEFAULT NULL,
      `col294` varchar(100) DEFAULT NULL,
      `col295` varchar(100) DEFAULT NULL,
      `col296` varchar(100) DEFAULT NULL,
      `col297` varchar(100) DEFAULT NULL,
      `col298` varchar(100) DEFAULT NULL,
      `col299` varchar(100) DEFAULT NULL,
      `col300` varchar(100) DEFAULT NULL,
      `col301` varchar(100) DEFAULT NULL,
      `col302` varchar(100) DEFAULT NULL,
      `col303` varchar(100) DEFAULT NULL,
      `col304` varchar(100) DEFAULT NULL,
      `col305` varchar(100) DEFAULT NULL,
      `col306` varchar(100) DEFAULT NULL,
      `col307` varchar(100) DEFAULT NULL,
      `col308` varchar(100) DEFAULT NULL,
      `col309` varchar(100) DEFAULT NULL,
      `col310` varchar(100) DEFAULT NULL,
      `col311` varchar(100) DEFAULT NULL,
      `col312` varchar(100) DEFAULT NULL,
      `col313` varchar(100) DEFAULT NULL,
      `col314` varchar(100) DEFAULT NULL,
      `col315` varchar(100) DEFAULT NULL,
      `col316` varchar(100) DEFAULT NULL,
      `col317` varchar(100) DEFAULT NULL,
      `col318` varchar(100) DEFAULT NULL,
      `col319` varchar(100) DEFAULT NULL,
      `col320` varchar(100) DEFAULT NULL,
      `col321` varchar(100) DEFAULT NULL,
      `col322` varchar(100) DEFAULT NULL,
      `col323` varchar(100) DEFAULT NULL,
      `col324` varchar(100) DEFAULT NULL,
      `col325` varchar(100) DEFAULT NULL,
      `col326` varchar(100) DEFAULT NULL,
      `col327` varchar(100) DEFAULT NULL,
      `col328` varchar(100) DEFAULT NULL,
      `col329` varchar(100) DEFAULT NULL,
      `col330` varchar(100) DEFAULT NULL,
      `col331` varchar(100) DEFAULT NULL,
      `col332` varchar(100) DEFAULT NULL,
      `col333` varchar(100) DEFAULT NULL,
      `col334` varchar(100) DEFAULT NULL,
      `col335` varchar(100) DEFAULT NULL,
      `col336` varchar(100) DEFAULT NULL,
      `col337` varchar(100) DEFAULT NULL,
      `col338` varchar(100) DEFAULT NULL,
      `col339` varchar(100) DEFAULT NULL,
      `col340` varchar(100) DEFAULT NULL,
      `col341` varchar(100) DEFAULT NULL,
      `col342` varchar(100) DEFAULT NULL,
      `col343` varchar(100) DEFAULT NULL,
      `col344` varchar(100) DEFAULT NULL,
      `col345` varchar(100) DEFAULT NULL,
      `col346` varchar(100) DEFAULT NULL,
      `col347` varchar(100) DEFAULT NULL,
      `col348` varchar(100) DEFAULT NULL,
      `col349` varchar(100) DEFAULT NULL,
      `col350` varchar(100) DEFAULT NULL,
      `col351` varchar(100) DEFAULT NULL,
      `col352` varchar(100) DEFAULT NULL,
      `col353` varchar(100) DEFAULT NULL,
      `col354` varchar(100) DEFAULT NULL,
      `col355` varchar(100) DEFAULT NULL,
      `col356` varchar(100) DEFAULT NULL,
      `col357` varchar(100) DEFAULT NULL,
      `col358` varchar(100) DEFAULT NULL,
      `col359` varchar(100) DEFAULT NULL,
      `col360` varchar(100) DEFAULT NULL,
      `col361` varchar(100) DEFAULT NULL,
      `col362` varchar(100) DEFAULT NULL,
      `col363` varchar(100) DEFAULT NULL,
      `col364` varchar(100) DEFAULT NULL,
      `col365` varchar(100) DEFAULT NULL,
      `col366` varchar(100) DEFAULT NULL,
      `col367` varchar(100) DEFAULT NULL,
      `col368` varchar(100) DEFAULT NULL,
      `col369` varchar(100) DEFAULT NULL,
      `col370` varchar(100) DEFAULT NULL,
      `col371` varchar(100) DEFAULT NULL,
      `col372` varchar(100) DEFAULT NULL,
      `col373` varchar(100) DEFAULT NULL,
      `col374` varchar(100) DEFAULT NULL,
      `col375` varchar(100) DEFAULT NULL,
      `col376` varchar(100) DEFAULT NULL,
      `col377` varchar(100) DEFAULT NULL,
      `col378` varchar(100) DEFAULT NULL,
      `col379` varchar(100) DEFAULT NULL,
      `col380` varchar(100) DEFAULT NULL,
      `col381` varchar(100) DEFAULT NULL,
      `col382` varchar(100) DEFAULT NULL,
      `col383` varchar(100) DEFAULT NULL,
      `col384` varchar(100) DEFAULT NULL,
      `col385` varchar(100) DEFAULT NULL,
      `col386` varchar(100) DEFAULT NULL,
      `col387` varchar(100) DEFAULT NULL,
      `col388` varchar(100) DEFAULT NULL,
      `col389` varchar(100) DEFAULT NULL,
      `col390` varchar(100) DEFAULT NULL,
      `col391` varchar(100) DEFAULT NULL,
      `col392` varchar(100) DEFAULT NULL,
      `col393` varchar(100) DEFAULT NULL,
      `col394` varchar(100) DEFAULT NULL,
      `col395` varchar(100) DEFAULT NULL,
      `col396` varchar(100) DEFAULT NULL,
      `col397` varchar(100) DEFAULT NULL,
      `col398` varchar(100) DEFAULT NULL,
      `col399` varchar(100) DEFAULT NULL,
      `col400` varchar(100) DEFAULT NULL,
      `col401` varchar(100) DEFAULT NULL,
      `col402` varchar(100) DEFAULT NULL,
      `col403` varchar(100) DEFAULT NULL,
      `col404` varchar(100) DEFAULT NULL,
      `col405` varchar(100) DEFAULT NULL,
      `col406` varchar(100) DEFAULT NULL,
      `col407` varchar(100) DEFAULT NULL,
      `col408` varchar(100) DEFAULT NULL,
      `col409` varchar(100) DEFAULT NULL,
      `col410` varchar(100) DEFAULT NULL,
      `col411` varchar(100) DEFAULT NULL,
      `col412` varchar(100) DEFAULT NULL,
      `col413` varchar(100) DEFAULT NULL,
      `col414` varchar(100) DEFAULT NULL,
      `col415` varchar(100) DEFAULT NULL,
      `col416` varchar(100) DEFAULT NULL,
      `col417` varchar(100) DEFAULT NULL,
      `col418` varchar(100) DEFAULT NULL,
      `col419` varchar(100) DEFAULT NULL,
      `col420` varchar(100) DEFAULT NULL,
      `col421` varchar(100) DEFAULT NULL,
      `col422` varchar(100) DEFAULT NULL,
      `col423` varchar(100) DEFAULT NULL,
      `col424` varchar(100) DEFAULT NULL,
      `col425` varchar(100) DEFAULT NULL,
      `col426` varchar(100) DEFAULT NULL,
      `col427` varchar(100) DEFAULT NULL,
      `col428` varchar(100) DEFAULT NULL,
      `col429` varchar(100) DEFAULT NULL,
      `col430` varchar(100) DEFAULT NULL,
      `col431` varchar(100) DEFAULT NULL,
      `col432` varchar(100) DEFAULT NULL,
      `col433` varchar(100) DEFAULT NULL,
      `col434` varchar(100) DEFAULT NULL,
      `col435` varchar(100) DEFAULT NULL,
      `col436` varchar(100) DEFAULT NULL,
      `col437` varchar(100) DEFAULT NULL,
      `col438` varchar(100) DEFAULT NULL,
      `col439` varchar(100) DEFAULT NULL,
      `col440` varchar(100) DEFAULT NULL,
      `col441` varchar(100) DEFAULT NULL,
      `col442` varchar(100) DEFAULT NULL,
      `col443` varchar(100) DEFAULT NULL,
      `col444` varchar(100) DEFAULT NULL,
      `col445` varchar(100) DEFAULT NULL,
      `col446` varchar(100) DEFAULT NULL,
      `col447` varchar(100) DEFAULT NULL,
      `col448` varchar(100) DEFAULT NULL,
      `col449` varchar(100) DEFAULT NULL,
      `col450` varchar(100) DEFAULT NULL,
      `col451` varchar(100) DEFAULT NULL,
      `col452` varchar(100) DEFAULT NULL,
      `col453` varchar(100) DEFAULT NULL,
      `col454` varchar(100) DEFAULT NULL,
      `col455` varchar(100) DEFAULT NULL,
      `col456` varchar(100) DEFAULT NULL,
      `col457` varchar(100) DEFAULT NULL,
      `col458` varchar(100) DEFAULT NULL,
      `col459` varchar(100) DEFAULT NULL,
      `col460` varchar(100) DEFAULT NULL,
      `col461` varchar(100) DEFAULT NULL,
      `col462` varchar(100) DEFAULT NULL,
      `col463` varchar(100) DEFAULT NULL,
      `col464` varchar(100) DEFAULT NULL,
      `col465` varchar(100) DEFAULT NULL,
      `col466` varchar(100) DEFAULT NULL,
      `col467` varchar(100) DEFAULT NULL,
      `col468` varchar(100) DEFAULT NULL,
      `col469` varchar(100) DEFAULT NULL,
      `col470` varchar(100) DEFAULT NULL,
      `col471` varchar(100) DEFAULT NULL,
      `col472` varchar(100) DEFAULT NULL,
      `col473` varchar(100) DEFAULT NULL,
      `col474` varchar(100) DEFAULT NULL,
      `col475` varchar(100) DEFAULT NULL,
      `col476` varchar(100) DEFAULT NULL,
      `col477` varchar(100) DEFAULT NULL,
      `col478` varchar(100) DEFAULT NULL,
      `col479` varchar(100) DEFAULT NULL,
      `col480` varchar(100) DEFAULT NULL,
      `col481` varchar(100) DEFAULT NULL,
      `col482` varchar(100) DEFAULT NULL,
      `col483` varchar(100) DEFAULT NULL,
      `col484` varchar(100) DEFAULT NULL,
      `col485` varchar(100) DEFAULT NULL,
      `col486` varchar(100) DEFAULT NULL,
      `col487` varchar(100) DEFAULT NULL,
      `col488` varchar(100) DEFAULT NULL,
      `col489` varchar(100) DEFAULT NULL,
      `col490` varchar(100) DEFAULT NULL,
      `col491` varchar(100) DEFAULT NULL,
      `col492` varchar(100) DEFAULT NULL,
      `col493` varchar(100) DEFAULT NULL,
      `col494` varchar(100) DEFAULT NULL,
      `col495` varchar(100) DEFAULT NULL,
      `col496` varchar(100) DEFAULT NULL,
      `col497` varchar(100) DEFAULT NULL,
      `col498` varchar(100) DEFAULT NULL,
      `col499` varchar(100) DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `po_id` (`po_id`),
        KEY `combi` (`po_id`,`is_column`,`center_id`),
        KEY `phone_noandpo_id` (`po_id`,`phone_no`),
        KEY `batchnophoneno` (`batch_no`,`phone_no`)
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;";
        $this->db->query($cmd);
        
        //po_transaction_data_2_calling_list_
        $cmd = "CREATE TABLE `po_transaction_data_2_calling_list_{$po_id}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cr_ids` varchar(500) NOT NULL,
  `first_names` varchar(500) NOT NULL,
  `po_id` int(11) NOT NULL,
  `batch_no` int(11) DEFAULT NULL,
  `is_column` int(11) DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `phone_no` varchar(32) DEFAULT NULL,
  `phone_field` varchar(32) DEFAULT NULL,
  `lname_field` varchar(32) DEFAULT NULL,
  `fname_field` varchar(32) DEFAULT NULL,
  `process_option_id` int(11) DEFAULT NULL,
  `remarks` text,
  `col_fields` text,
  `misc_fields` text,
  `status` char(1) DEFAULT NULL,
  `list_cnt` int(11) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `col1` varchar(100) DEFAULT NULL,
  `col2` varchar(100) DEFAULT NULL,
  `col3` varchar(100) DEFAULT NULL,
  `col4` varchar(100) DEFAULT NULL,
  `col5` varchar(100) DEFAULT NULL,
  `col6` varchar(100) DEFAULT NULL,
  `col7` varchar(100) DEFAULT NULL,
  `col8` varchar(100) DEFAULT NULL,
  `col9` varchar(100) DEFAULT NULL,
  `col10` varchar(100) DEFAULT NULL,
  `col11` varchar(100) DEFAULT NULL,
  `col12` varchar(100) DEFAULT NULL,
  `col13` varchar(100) DEFAULT NULL,
  `col14` varchar(100) DEFAULT NULL,
  `col15` varchar(100) DEFAULT NULL,
  `col16` varchar(100) DEFAULT NULL,
  `col17` varchar(100) DEFAULT NULL,
  `col18` varchar(100) DEFAULT NULL,
  `col19` varchar(100) DEFAULT NULL,
  `col20` varchar(100) DEFAULT NULL,
  `col21` varchar(100) DEFAULT NULL,
  `col22` varchar(100) DEFAULT NULL,
  `col23` varchar(100) DEFAULT NULL,
  `col24` varchar(100) DEFAULT NULL,
  `col25` varchar(100) DEFAULT NULL,
  `col26` varchar(100) DEFAULT NULL,
  `col27` varchar(100) DEFAULT NULL,
  `col28` varchar(100) DEFAULT NULL,
  `col29` varchar(100) DEFAULT NULL,
  `col30` varchar(100) DEFAULT NULL,
  `col31` varchar(100) DEFAULT NULL,
  `col32` varchar(100) DEFAULT NULL,
  `col33` varchar(100) DEFAULT NULL,
  `col34` varchar(100) DEFAULT NULL,
  `col35` varchar(100) DEFAULT NULL,
  `col36` varchar(100) DEFAULT NULL,
  `col37` varchar(100) DEFAULT NULL,
  `col38` varchar(100) DEFAULT NULL,
  `col39` varchar(100) DEFAULT NULL,
  `col40` varchar(100) DEFAULT NULL,
  `col41` varchar(100) DEFAULT NULL,
  `col42` varchar(100) DEFAULT NULL,
  `col43` varchar(100) DEFAULT NULL,
  `col44` varchar(100) DEFAULT NULL,
  `col45` varchar(100) DEFAULT NULL,
  `col46` varchar(100) DEFAULT NULL,
  `col47` varchar(100) DEFAULT NULL,
  `col48` varchar(100) DEFAULT NULL,
  `col49` varchar(100) DEFAULT NULL,
  `col50` varchar(100) DEFAULT NULL,
  `col51` varchar(100) DEFAULT NULL,
  `col52` varchar(100) DEFAULT NULL,
  `col53` varchar(100) DEFAULT NULL,
  `col54` varchar(100) DEFAULT NULL,
  `col55` varchar(100) DEFAULT NULL,
  `col56` varchar(100) DEFAULT NULL,
  `col57` varchar(100) DEFAULT NULL,
  `col58` varchar(100) DEFAULT NULL,
  `col59` varchar(100) DEFAULT NULL,
  `col60` varchar(100) DEFAULT NULL,
  `col61` varchar(100) DEFAULT NULL,
  `col62` varchar(100) DEFAULT NULL,
  `col63` varchar(100) DEFAULT NULL,
  `col64` varchar(100) DEFAULT NULL,
  `col65` varchar(100) DEFAULT NULL,
  `col66` varchar(100) DEFAULT NULL,
  `col67` varchar(100) DEFAULT NULL,
  `col68` varchar(100) DEFAULT NULL,
  `col69` varchar(100) DEFAULT NULL,
  `col70` varchar(100) DEFAULT NULL,
  `col71` varchar(100) DEFAULT NULL,
  `col72` varchar(100) DEFAULT NULL,
  `col73` varchar(100) DEFAULT NULL,
  `col74` varchar(100) DEFAULT NULL,
  `col75` varchar(100) DEFAULT NULL,
  `col76` varchar(100) DEFAULT NULL,
  `col77` varchar(100) DEFAULT NULL,
  `col78` varchar(100) DEFAULT NULL,
  `col79` varchar(100) DEFAULT NULL,
  `col80` varchar(100) DEFAULT NULL,
  `col81` varchar(100) DEFAULT NULL,
  `col82` varchar(100) DEFAULT NULL,
  `col83` varchar(100) DEFAULT NULL,
  `col84` varchar(100) DEFAULT NULL,
  `col85` varchar(100) DEFAULT NULL,
  `col86` varchar(100) DEFAULT NULL,
  `col87` varchar(100) DEFAULT NULL,
  `col88` varchar(100) DEFAULT NULL,
  `col89` varchar(100) DEFAULT NULL,
  `col90` varchar(100) DEFAULT NULL,
  `col91` varchar(100) DEFAULT NULL,
  `col92` varchar(100) DEFAULT NULL,
  `col93` varchar(100) DEFAULT NULL,
  `col94` varchar(100) DEFAULT NULL,
  `col95` varchar(100) DEFAULT NULL,
  `col96` varchar(100) DEFAULT NULL,
  `col97` varchar(100) DEFAULT NULL,
  `col98` varchar(100) DEFAULT NULL,
  `col99` varchar(100) DEFAULT NULL,
  `col100` varchar(100) DEFAULT NULL,
  `col500` varchar(100) DEFAULT NULL,
  `col101` varchar(100) DEFAULT NULL,
  `col102` varchar(100) DEFAULT NULL,
  `col103` varchar(100) DEFAULT NULL,
  `col104` varchar(100) DEFAULT NULL,
  `col105` varchar(100) DEFAULT NULL,
  `col106` varchar(100) DEFAULT NULL,
  `col107` varchar(100) DEFAULT NULL,
  `col108` varchar(100) DEFAULT NULL,
  `col109` varchar(100) DEFAULT NULL,
  `col110` varchar(100) DEFAULT NULL,
  `col111` varchar(100) DEFAULT NULL,
  `col112` varchar(100) DEFAULT NULL,
  `col113` varchar(100) DEFAULT NULL,
  `col114` varchar(100) DEFAULT NULL,
  `col115` varchar(100) DEFAULT NULL,
  `col116` varchar(100) DEFAULT NULL,
  `col117` varchar(100) DEFAULT NULL,
  `col118` varchar(100) DEFAULT NULL,
  `col119` varchar(100) DEFAULT NULL,
  `col120` varchar(100) DEFAULT NULL,
  `col121` varchar(100) DEFAULT NULL,
  `col122` varchar(100) DEFAULT NULL,
  `col123` varchar(100) DEFAULT NULL,
  `col124` varchar(100) DEFAULT NULL,
  `col125` varchar(100) DEFAULT NULL,
  `col126` varchar(100) DEFAULT NULL,
  `col127` varchar(100) DEFAULT NULL,
  `col128` varchar(100) DEFAULT NULL,
  `col129` varchar(100) DEFAULT NULL,
  `col130` varchar(100) DEFAULT NULL,
  `col131` varchar(100) DEFAULT NULL,
  `col132` varchar(100) DEFAULT NULL,
  `col133` varchar(100) DEFAULT NULL,
  `col134` varchar(100) DEFAULT NULL,
  `col135` varchar(100) DEFAULT NULL,
  `col136` varchar(100) DEFAULT NULL,
  `col137` varchar(100) DEFAULT NULL,
  `col138` varchar(100) DEFAULT NULL,
  `col139` varchar(100) DEFAULT NULL,
  `col140` varchar(100) DEFAULT NULL,
  `col141` varchar(100) DEFAULT NULL,
  `col142` varchar(100) DEFAULT NULL,
  `col143` varchar(100) DEFAULT NULL,
  `col144` varchar(100) DEFAULT NULL,
  `col145` varchar(100) DEFAULT NULL,
  `col146` varchar(100) DEFAULT NULL,
  `col147` varchar(100) DEFAULT NULL,
  `col148` varchar(100) DEFAULT NULL,
  `col149` varchar(100) DEFAULT NULL,
  `col150` varchar(100) DEFAULT NULL,
  `col151` varchar(100) DEFAULT NULL,
  `col152` varchar(100) DEFAULT NULL,
  `col153` varchar(100) DEFAULT NULL,
  `col154` varchar(100) DEFAULT NULL,
  `col155` varchar(100) DEFAULT NULL,
  `col156` varchar(100) DEFAULT NULL,
  `col157` varchar(100) DEFAULT NULL,
  `col158` varchar(100) DEFAULT NULL,
  `col159` varchar(100) DEFAULT NULL,
  `col160` varchar(100) DEFAULT NULL,
  `col161` varchar(100) DEFAULT NULL,
  `col162` varchar(100) DEFAULT NULL,
  `col163` varchar(100) DEFAULT NULL,
  `col164` varchar(100) DEFAULT NULL,
  `col165` varchar(100) DEFAULT NULL,
  `col166` varchar(100) DEFAULT NULL,
  `col167` varchar(100) DEFAULT NULL,
  `col168` varchar(100) DEFAULT NULL,
  `col169` varchar(100) DEFAULT NULL,
  `col170` varchar(100) DEFAULT NULL,
  `col171` varchar(100) DEFAULT NULL,
  `col172` varchar(100) DEFAULT NULL,
  `col173` varchar(100) DEFAULT NULL,
  `col174` varchar(100) DEFAULT NULL,
  `col175` varchar(100) DEFAULT NULL,
  `col176` varchar(100) DEFAULT NULL,
  `col177` varchar(100) DEFAULT NULL,
  `col178` varchar(100) DEFAULT NULL,
  `col179` varchar(100) DEFAULT NULL,
  `col180` varchar(100) DEFAULT NULL,
  `col181` varchar(100) DEFAULT NULL,
  `col182` varchar(100) DEFAULT NULL,
  `col183` varchar(100) DEFAULT NULL,
  `col184` varchar(100) DEFAULT NULL,
  `col185` varchar(100) DEFAULT NULL,
  `col186` varchar(100) DEFAULT NULL,
  `col187` varchar(100) DEFAULT NULL,
  `col188` varchar(100) DEFAULT NULL,
  `col189` varchar(100) DEFAULT NULL,
  `col190` varchar(100) DEFAULT NULL,
  `col191` varchar(100) DEFAULT NULL,
  `col192` varchar(100) DEFAULT NULL,
  `col193` varchar(100) DEFAULT NULL,
  `col194` varchar(100) DEFAULT NULL,
  `col195` varchar(100) DEFAULT NULL,
  `col196` varchar(100) DEFAULT NULL,
  `col197` varchar(100) DEFAULT NULL,
  `col198` varchar(100) DEFAULT NULL,
  `col199` varchar(100) DEFAULT NULL,
  `col200` varchar(100) DEFAULT NULL,
  `col201` varchar(100) DEFAULT NULL,
  `col202` varchar(100) DEFAULT NULL,
  `col203` varchar(100) DEFAULT NULL,
  `col204` varchar(100) DEFAULT NULL,
  `col205` varchar(100) DEFAULT NULL,
  `col206` varchar(100) DEFAULT NULL,
  `col207` varchar(100) DEFAULT NULL,
  `col208` varchar(100) DEFAULT NULL,
  `col209` varchar(100) DEFAULT NULL,
  `col210` varchar(100) DEFAULT NULL,
  `col211` varchar(100) DEFAULT NULL,
  `col212` varchar(100) DEFAULT NULL,
  `col213` varchar(100) DEFAULT NULL,
  `col214` varchar(100) DEFAULT NULL,
  `col215` varchar(100) DEFAULT NULL,
  `col216` varchar(100) DEFAULT NULL,
  `col217` varchar(100) DEFAULT NULL,
  `col218` varchar(100) DEFAULT NULL,
  `col219` varchar(100) DEFAULT NULL,
  `col220` varchar(100) DEFAULT NULL,
  `col221` varchar(100) DEFAULT NULL,
  `col222` varchar(100) DEFAULT NULL,
  `col223` varchar(100) DEFAULT NULL,
  `col224` varchar(100) DEFAULT NULL,
  `col225` varchar(100) DEFAULT NULL,
  `col226` varchar(100) DEFAULT NULL,
  `col227` varchar(100) DEFAULT NULL,
  `col228` varchar(100) DEFAULT NULL,
  `col229` varchar(100) DEFAULT NULL,
  `col230` varchar(100) DEFAULT NULL,
  `col231` varchar(100) DEFAULT NULL,
  `col232` varchar(100) DEFAULT NULL,
  `col233` varchar(100) DEFAULT NULL,
  `col234` varchar(100) DEFAULT NULL,
  `col235` varchar(100) DEFAULT NULL,
  `col236` varchar(100) DEFAULT NULL,
  `col237` varchar(100) DEFAULT NULL,
  `col238` varchar(100) DEFAULT NULL,
  `col239` varchar(100) DEFAULT NULL,
  `col240` varchar(100) DEFAULT NULL,
  `col241` varchar(100) DEFAULT NULL,
  `col242` varchar(100) DEFAULT NULL,
  `col243` varchar(100) DEFAULT NULL,
  `col244` varchar(100) DEFAULT NULL,
  `col245` varchar(100) DEFAULT NULL,
  `col246` varchar(100) DEFAULT NULL,
  `col247` varchar(100) DEFAULT NULL,
  `col248` varchar(100) DEFAULT NULL,
  `col249` varchar(100) DEFAULT NULL,
  `col250` varchar(100) DEFAULT NULL,
  `col251` varchar(100) DEFAULT NULL,
  `col252` varchar(100) DEFAULT NULL,
  `col253` varchar(100) DEFAULT NULL,
  `col254` varchar(100) DEFAULT NULL,
  `col255` varchar(100) DEFAULT NULL,
  `col256` varchar(100) DEFAULT NULL,
  `col257` varchar(100) DEFAULT NULL,
  `col258` varchar(100) DEFAULT NULL,
  `col259` varchar(100) DEFAULT NULL,
  `col260` varchar(100) DEFAULT NULL,
  `col261` varchar(100) DEFAULT NULL,
  `col262` varchar(100) DEFAULT NULL,
  `col263` varchar(100) DEFAULT NULL,
  `col264` varchar(100) DEFAULT NULL,
  `col265` varchar(100) DEFAULT NULL,
  `col266` varchar(100) DEFAULT NULL,
  `col267` varchar(100) DEFAULT NULL,
  `col268` varchar(100) DEFAULT NULL,
  `col269` varchar(100) DEFAULT NULL,
  `col270` varchar(100) DEFAULT NULL,
  `col271` varchar(100) DEFAULT NULL,
  `col272` varchar(100) DEFAULT NULL,
  `col273` varchar(100) DEFAULT NULL,
  `col274` varchar(100) DEFAULT NULL,
  `col275` varchar(100) DEFAULT NULL,
  `col276` varchar(100) DEFAULT NULL,
  `col277` varchar(100) DEFAULT NULL,
  `col278` varchar(100) DEFAULT NULL,
  `col279` varchar(100) DEFAULT NULL,
  `col280` varchar(100) DEFAULT NULL,
  `col281` varchar(100) DEFAULT NULL,
  `col282` varchar(100) DEFAULT NULL,
  `col283` varchar(100) DEFAULT NULL,
  `col284` varchar(100) DEFAULT NULL,
  `col285` varchar(100) DEFAULT NULL,
  `col286` varchar(100) DEFAULT NULL,
  `col287` varchar(100) DEFAULT NULL,
  `col288` varchar(100) DEFAULT NULL,
  `col289` varchar(100) DEFAULT NULL,
  `col290` varchar(100) DEFAULT NULL,
  `col291` varchar(100) DEFAULT NULL,
  `col292` varchar(100) DEFAULT NULL,
  `col293` varchar(100) DEFAULT NULL,
  `col294` varchar(100) DEFAULT NULL,
  `col295` varchar(100) DEFAULT NULL,
  `col296` varchar(100) DEFAULT NULL,
  `col297` varchar(100) DEFAULT NULL,
  `col298` varchar(100) DEFAULT NULL,
  `col299` varchar(100) DEFAULT NULL,
  `col300` varchar(100) DEFAULT NULL,
  `col301` varchar(100) DEFAULT NULL,
  `col302` varchar(100) DEFAULT NULL,
  `col303` varchar(100) DEFAULT NULL,
  `col304` varchar(100) DEFAULT NULL,
  `col305` varchar(100) DEFAULT NULL,
  `col306` varchar(100) DEFAULT NULL,
  `col307` varchar(100) DEFAULT NULL,
  `col308` varchar(100) DEFAULT NULL,
  `col309` varchar(100) DEFAULT NULL,
  `col310` varchar(100) DEFAULT NULL,
  `col311` varchar(100) DEFAULT NULL,
  `col312` varchar(100) DEFAULT NULL,
  `col313` varchar(100) DEFAULT NULL,
  `col314` varchar(100) DEFAULT NULL,
  `col315` varchar(100) DEFAULT NULL,
  `col316` varchar(100) DEFAULT NULL,
  `col317` varchar(100) DEFAULT NULL,
  `col318` varchar(100) DEFAULT NULL,
  `col319` varchar(100) DEFAULT NULL,
  `col320` varchar(100) DEFAULT NULL,
  `col321` varchar(100) DEFAULT NULL,
  `col322` varchar(100) DEFAULT NULL,
  `col323` varchar(100) DEFAULT NULL,
  `col324` varchar(100) DEFAULT NULL,
  `col325` varchar(100) DEFAULT NULL,
  `col326` varchar(100) DEFAULT NULL,
  `col327` varchar(100) DEFAULT NULL,
  `col328` varchar(100) DEFAULT NULL,
  `col329` varchar(100) DEFAULT NULL,
  `col330` varchar(100) DEFAULT NULL,
  `col331` varchar(100) DEFAULT NULL,
  `col332` varchar(100) DEFAULT NULL,
  `col333` varchar(100) DEFAULT NULL,
  `col334` varchar(100) DEFAULT NULL,
  `col335` varchar(100) DEFAULT NULL,
  `col336` varchar(100) DEFAULT NULL,
  `col337` varchar(100) DEFAULT NULL,
  `col338` varchar(100) DEFAULT NULL,
  `col339` varchar(100) DEFAULT NULL,
  `col340` varchar(100) DEFAULT NULL,
  `col341` varchar(100) DEFAULT NULL,
  `col342` varchar(100) DEFAULT NULL,
  `col343` varchar(100) DEFAULT NULL,
  `col344` varchar(100) DEFAULT NULL,
  `col345` varchar(100) DEFAULT NULL,
  `col346` varchar(100) DEFAULT NULL,
  `col347` varchar(100) DEFAULT NULL,
  `col348` varchar(100) DEFAULT NULL,
  `col349` varchar(100) DEFAULT NULL,
  `col350` varchar(100) DEFAULT NULL,
  `col351` varchar(100) DEFAULT NULL,
  `col352` varchar(100) DEFAULT NULL,
  `col353` varchar(100) DEFAULT NULL,
  `col354` varchar(100) DEFAULT NULL,
  `col355` varchar(100) DEFAULT NULL,
  `col356` varchar(100) DEFAULT NULL,
  `col357` varchar(100) DEFAULT NULL,
  `col358` varchar(100) DEFAULT NULL,
  `col359` varchar(100) DEFAULT NULL,
  `col360` varchar(100) DEFAULT NULL,
  `col361` varchar(100) DEFAULT NULL,
  `col362` varchar(100) DEFAULT NULL,
  `col363` varchar(100) DEFAULT NULL,
  `col364` varchar(100) DEFAULT NULL,
  `col365` varchar(100) DEFAULT NULL,
  `col366` varchar(100) DEFAULT NULL,
  `col367` varchar(100) DEFAULT NULL,
  `col368` varchar(100) DEFAULT NULL,
  `col369` varchar(100) DEFAULT NULL,
  `col370` varchar(100) DEFAULT NULL,
  `col371` varchar(100) DEFAULT NULL,
  `col372` varchar(100) DEFAULT NULL,
  `col373` varchar(100) DEFAULT NULL,
  `col374` varchar(100) DEFAULT NULL,
  `col375` varchar(100) DEFAULT NULL,
  `col376` varchar(100) DEFAULT NULL,
  `col377` varchar(100) DEFAULT NULL,
  `col378` varchar(100) DEFAULT NULL,
  `col379` varchar(100) DEFAULT NULL,
  `col380` varchar(100) DEFAULT NULL,
  `col381` varchar(100) DEFAULT NULL,
  `col382` varchar(100) DEFAULT NULL,
  `col383` varchar(100) DEFAULT NULL,
  `col384` varchar(100) DEFAULT NULL,
  `col385` varchar(100) DEFAULT NULL,
  `col386` varchar(100) DEFAULT NULL,
  `col387` varchar(100) DEFAULT NULL,
  `col388` varchar(100) DEFAULT NULL,
  `col389` varchar(100) DEFAULT NULL,
  `col390` varchar(100) DEFAULT NULL,
  `col391` varchar(100) DEFAULT NULL,
  `col392` varchar(100) DEFAULT NULL,
  `col393` varchar(100) DEFAULT NULL,
  `col394` varchar(100) DEFAULT NULL,
  `col395` varchar(100) DEFAULT NULL,
  `col396` varchar(100) DEFAULT NULL,
  `col397` varchar(100) DEFAULT NULL,
  `col398` varchar(100) DEFAULT NULL,
  `col399` varchar(100) DEFAULT NULL,
  `col400` varchar(100) DEFAULT NULL,
  `col401` varchar(100) DEFAULT NULL,
  `col402` varchar(100) DEFAULT NULL,
  `col403` varchar(100) DEFAULT NULL,
  `col404` varchar(100) DEFAULT NULL,
  `col405` varchar(100) DEFAULT NULL,
  `col406` varchar(100) DEFAULT NULL,
  `col407` varchar(100) DEFAULT NULL,
  `col408` varchar(100) DEFAULT NULL,
  `col409` varchar(100) DEFAULT NULL,
  `col410` varchar(100) DEFAULT NULL,
  `col411` varchar(100) DEFAULT NULL,
  `col412` varchar(100) DEFAULT NULL,
  `col413` varchar(100) DEFAULT NULL,
  `col414` varchar(100) DEFAULT NULL,
  `col415` varchar(100) DEFAULT NULL,
  `col416` varchar(100) DEFAULT NULL,
  `col417` varchar(100) DEFAULT NULL,
  `col418` varchar(100) DEFAULT NULL,
  `col419` varchar(100) DEFAULT NULL,
  `col420` varchar(100) DEFAULT NULL,
  `col421` varchar(100) DEFAULT NULL,
  `col422` varchar(100) DEFAULT NULL,
  `col423` varchar(100) DEFAULT NULL,
  `col424` varchar(100) DEFAULT NULL,
  `col425` varchar(100) DEFAULT NULL,
  `col426` varchar(100) DEFAULT NULL,
  `col427` varchar(100) DEFAULT NULL,
  `col428` varchar(100) DEFAULT NULL,
  `col429` varchar(100) DEFAULT NULL,
  `col430` varchar(100) DEFAULT NULL,
  `col431` varchar(100) DEFAULT NULL,
  `col432` varchar(100) DEFAULT NULL,
  `col433` varchar(100) DEFAULT NULL,
  `col434` varchar(100) DEFAULT NULL,
  `col435` varchar(100) DEFAULT NULL,
  `col436` varchar(100) DEFAULT NULL,
  `col437` varchar(100) DEFAULT NULL,
  `col438` varchar(100) DEFAULT NULL,
  `col439` varchar(100) DEFAULT NULL,
  `col440` varchar(100) DEFAULT NULL,
  `col441` varchar(100) DEFAULT NULL,
  `col442` varchar(100) DEFAULT NULL,
  `col443` varchar(100) DEFAULT NULL,
  `col444` varchar(100) DEFAULT NULL,
  `col445` varchar(100) DEFAULT NULL,
  `col446` varchar(100) DEFAULT NULL,
  `col447` varchar(100) DEFAULT NULL,
  `col448` varchar(100) DEFAULT NULL,
  `col449` varchar(100) DEFAULT NULL,
  `col450` varchar(100) DEFAULT NULL,
  `col451` varchar(100) DEFAULT NULL,
  `col452` varchar(100) DEFAULT NULL,
  `col453` varchar(100) DEFAULT NULL,
  `col454` varchar(100) DEFAULT NULL,
  `col455` varchar(100) DEFAULT NULL,
  `col456` varchar(100) DEFAULT NULL,
  `col457` varchar(100) DEFAULT NULL,
  `col458` varchar(100) DEFAULT NULL,
  `col459` varchar(100) DEFAULT NULL,
  `col460` varchar(100) DEFAULT NULL,
  `col461` varchar(100) DEFAULT NULL,
  `col462` varchar(100) DEFAULT NULL,
  `col463` varchar(100) DEFAULT NULL,
  `col464` varchar(100) DEFAULT NULL,
  `col465` varchar(100) DEFAULT NULL,
  `col466` varchar(100) DEFAULT NULL,
  `col467` varchar(100) DEFAULT NULL,
  `col468` varchar(100) DEFAULT NULL,
  `col469` varchar(100) DEFAULT NULL,
  `col470` varchar(100) DEFAULT NULL,
  `col471` varchar(100) DEFAULT NULL,
  `col472` varchar(100) DEFAULT NULL,
  `col473` varchar(100) DEFAULT NULL,
  `col474` varchar(100) DEFAULT NULL,
  `col475` varchar(100) DEFAULT NULL,
  `col476` varchar(100) DEFAULT NULL,
  `col477` varchar(100) DEFAULT NULL,
  `col478` varchar(100) DEFAULT NULL,
  `col479` varchar(100) DEFAULT NULL,
  `col480` varchar(100) DEFAULT NULL,
  `col481` varchar(100) DEFAULT NULL,
  `col482` varchar(100) DEFAULT NULL,
  `col483` varchar(100) DEFAULT NULL,
  `col484` varchar(100) DEFAULT NULL,
  `col485` varchar(100) DEFAULT NULL,
  `col486` varchar(100) DEFAULT NULL,
  `col487` varchar(100) DEFAULT NULL,
  `col488` varchar(100) DEFAULT NULL,
  `col489` varchar(100) DEFAULT NULL,
  `col490` varchar(100) DEFAULT NULL,
  `col491` varchar(100) DEFAULT NULL,
  `col492` varchar(100) DEFAULT NULL,
  `col493` varchar(100) DEFAULT NULL,
  `col494` varchar(100) DEFAULT NULL,
  `col495` varchar(100) DEFAULT NULL,
  `col496` varchar(100) DEFAULT NULL,
  `col497` varchar(100) DEFAULT NULL,
  `col498` varchar(100) DEFAULT NULL,
  `col499` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `po_id` (`po_id`),
  KEY `combi` (`po_id`,`is_column`,`center_id`),
  KEY `index_key` (`cr_ids`,`po_id`,`batch_no`,`phone_no`,`center_id`),
  KEY `new` (`id`,`batch_no`),
  KEY `phone` (`phone_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
";
        $this->db->query($cmd);
        
        //po_transaction_data_2_temp_
        $cmd = "CREATE TABLE `po_transaction_data_2_temp_{$po_id}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `batch_no` int(11) DEFAULT NULL,
  `is_column` int(11) DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `phone_no` varchar(32) DEFAULT NULL,
  `phone_field` varchar(32) DEFAULT NULL,
  `lname_field` varchar(32) DEFAULT NULL,
  `fname_field` varchar(32) DEFAULT NULL,
  `process_option_id` int(11) DEFAULT NULL,
  `remarks` text,
  `col_fields` text,
  `misc_fields` text,
  `status` char(1) DEFAULT NULL,
  `list_cnt` int(11) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `col1` varchar(100) DEFAULT NULL,
  `col2` varchar(100) DEFAULT NULL,
  `col3` varchar(100) DEFAULT NULL,
  `col4` varchar(100) DEFAULT NULL,
  `col5` varchar(100) DEFAULT NULL,
  `col6` varchar(100) DEFAULT NULL,
  `col7` varchar(100) DEFAULT NULL,
  `col8` varchar(100) DEFAULT NULL,
  `col9` varchar(100) DEFAULT NULL,
  `col10` varchar(100) DEFAULT NULL,
  `col11` varchar(100) DEFAULT NULL,
  `col12` varchar(100) DEFAULT NULL,
  `col13` varchar(100) DEFAULT NULL,
  `col14` varchar(100) DEFAULT NULL,
  `col15` varchar(100) DEFAULT NULL,
  `col16` varchar(100) DEFAULT NULL,
  `col17` varchar(100) DEFAULT NULL,
  `col18` varchar(100) DEFAULT NULL,
  `col19` varchar(100) DEFAULT NULL,
  `col20` varchar(100) DEFAULT NULL,
  `col21` varchar(100) DEFAULT NULL,
  `col22` varchar(100) DEFAULT NULL,
  `col23` varchar(100) DEFAULT NULL,
  `col24` varchar(100) DEFAULT NULL,
  `col25` varchar(100) DEFAULT NULL,
  `col26` varchar(100) DEFAULT NULL,
  `col27` varchar(100) DEFAULT NULL,
  `col28` varchar(100) DEFAULT NULL,
  `col29` varchar(100) DEFAULT NULL,
  `col30` varchar(100) DEFAULT NULL,
  `col31` varchar(100) DEFAULT NULL,
  `col32` varchar(100) DEFAULT NULL,
  `col33` varchar(100) DEFAULT NULL,
  `col34` varchar(100) DEFAULT NULL,
  `col35` varchar(100) DEFAULT NULL,
  `col36` varchar(100) DEFAULT NULL,
  `col37` varchar(100) DEFAULT NULL,
  `col38` varchar(100) DEFAULT NULL,
  `col39` varchar(100) DEFAULT NULL,
  `col40` varchar(100) DEFAULT NULL,
  `col41` varchar(100) DEFAULT NULL,
  `col42` varchar(100) DEFAULT NULL,
  `col43` varchar(100) DEFAULT NULL,
  `col44` varchar(100) DEFAULT NULL,
  `col45` varchar(100) DEFAULT NULL,
  `col46` varchar(100) DEFAULT NULL,
  `col47` varchar(100) DEFAULT NULL,
  `col48` varchar(100) DEFAULT NULL,
  `col49` varchar(100) DEFAULT NULL,
  `col50` varchar(100) DEFAULT NULL,
  `col51` varchar(100) DEFAULT NULL,
  `col52` varchar(100) DEFAULT NULL,
  `col53` varchar(100) DEFAULT NULL,
  `col54` varchar(100) DEFAULT NULL,
  `col55` varchar(100) DEFAULT NULL,
  `col56` varchar(100) DEFAULT NULL,
  `col57` varchar(100) DEFAULT NULL,
  `col58` varchar(100) DEFAULT NULL,
  `col59` varchar(100) DEFAULT NULL,
  `col60` varchar(100) DEFAULT NULL,
  `col61` varchar(100) DEFAULT NULL,
  `col62` varchar(100) DEFAULT NULL,
  `col63` varchar(100) DEFAULT NULL,
  `col64` varchar(100) DEFAULT NULL,
  `col65` varchar(100) DEFAULT NULL,
  `col66` varchar(100) DEFAULT NULL,
  `col67` varchar(100) DEFAULT NULL,
  `col68` varchar(100) DEFAULT NULL,
  `col69` varchar(100) DEFAULT NULL,
  `col70` varchar(100) DEFAULT NULL,
  `col71` varchar(100) DEFAULT NULL,
  `col72` varchar(100) DEFAULT NULL,
  `col73` varchar(100) DEFAULT NULL,
  `col74` varchar(100) DEFAULT NULL,
  `col75` varchar(100) DEFAULT NULL,
  `col76` varchar(100) DEFAULT NULL,
  `col77` varchar(100) DEFAULT NULL,
  `col78` varchar(100) DEFAULT NULL,
  `col79` varchar(100) DEFAULT NULL,
  `col80` varchar(100) DEFAULT NULL,
  `col81` varchar(100) DEFAULT NULL,
  `col82` varchar(100) DEFAULT NULL,
  `col83` varchar(100) DEFAULT NULL,
  `col84` varchar(100) DEFAULT NULL,
  `col85` varchar(100) DEFAULT NULL,
  `col86` varchar(100) DEFAULT NULL,
  `col87` varchar(100) DEFAULT NULL,
  `col88` varchar(100) DEFAULT NULL,
  `col89` varchar(100) DEFAULT NULL,
  `col90` varchar(100) DEFAULT NULL,
  `col91` varchar(100) DEFAULT NULL,
  `col92` varchar(100) DEFAULT NULL,
  `col93` varchar(100) DEFAULT NULL,
  `col94` varchar(100) DEFAULT NULL,
  `col95` varchar(100) DEFAULT NULL,
  `col96` varchar(100) DEFAULT NULL,
  `col97` varchar(100) DEFAULT NULL,
  `col98` varchar(100) DEFAULT NULL,
  `col99` varchar(100) DEFAULT NULL,
  `col100` varchar(100) DEFAULT NULL,
  `col500` varchar(100) DEFAULT NULL,
  `is_exempted` int(1) DEFAULT '0',
  `col101` varchar(100) DEFAULT NULL,
  `col102` varchar(100) DEFAULT NULL,
  `col103` varchar(100) DEFAULT NULL,
  `col104` varchar(100) DEFAULT NULL,
  `col105` varchar(100) DEFAULT NULL,
  `col106` varchar(100) DEFAULT NULL,
  `col107` varchar(100) DEFAULT NULL,
  `col108` varchar(100) DEFAULT NULL,
  `col109` varchar(100) DEFAULT NULL,
  `col110` varchar(100) DEFAULT NULL,
  `col111` varchar(100) DEFAULT NULL,
  `col112` varchar(100) DEFAULT NULL,
  `col113` varchar(100) DEFAULT NULL,
  `col114` varchar(100) DEFAULT NULL,
  `col115` varchar(100) DEFAULT NULL,
  `col116` varchar(100) DEFAULT NULL,
  `col117` varchar(100) DEFAULT NULL,
  `col118` varchar(100) DEFAULT NULL,
  `col119` varchar(100) DEFAULT NULL,
  `col120` varchar(100) DEFAULT NULL,
  `col121` varchar(100) DEFAULT NULL,
  `col122` varchar(100) DEFAULT NULL,
  `col123` varchar(100) DEFAULT NULL,
  `col124` varchar(100) DEFAULT NULL,
  `col125` varchar(100) DEFAULT NULL,
  `col126` varchar(100) DEFAULT NULL,
  `col127` varchar(100) DEFAULT NULL,
  `col128` varchar(100) DEFAULT NULL,
  `col129` varchar(100) DEFAULT NULL,
  `col130` varchar(100) DEFAULT NULL,
  `col131` varchar(100) DEFAULT NULL,
  `col132` varchar(100) DEFAULT NULL,
  `col133` varchar(100) DEFAULT NULL,
  `col134` varchar(100) DEFAULT NULL,
  `col135` varchar(100) DEFAULT NULL,
  `col136` varchar(100) DEFAULT NULL,
  `col137` varchar(100) DEFAULT NULL,
  `col138` varchar(100) DEFAULT NULL,
  `col139` varchar(100) DEFAULT NULL,
  `col140` varchar(100) DEFAULT NULL,
  `col141` varchar(100) DEFAULT NULL,
  `col142` varchar(100) DEFAULT NULL,
  `col143` varchar(100) DEFAULT NULL,
  `col144` varchar(100) DEFAULT NULL,
  `col145` varchar(100) DEFAULT NULL,
  `col146` varchar(100) DEFAULT NULL,
  `col147` varchar(100) DEFAULT NULL,
  `col148` varchar(100) DEFAULT NULL,
  `col149` varchar(100) DEFAULT NULL,
  `col150` varchar(100) DEFAULT NULL,
  `col151` varchar(100) DEFAULT NULL,
  `col152` varchar(100) DEFAULT NULL,
  `col153` varchar(100) DEFAULT NULL,
  `col154` varchar(100) DEFAULT NULL,
  `col155` varchar(100) DEFAULT NULL,
  `col156` varchar(100) DEFAULT NULL,
  `col157` varchar(100) DEFAULT NULL,
  `col158` varchar(100) DEFAULT NULL,
  `col159` varchar(100) DEFAULT NULL,
  `col160` varchar(100) DEFAULT NULL,
  `col161` varchar(100) DEFAULT NULL,
  `col162` varchar(100) DEFAULT NULL,
  `col163` varchar(100) DEFAULT NULL,
  `col164` varchar(100) DEFAULT NULL,
  `col165` varchar(100) DEFAULT NULL,
  `col166` varchar(100) DEFAULT NULL,
  `col167` varchar(100) DEFAULT NULL,
  `col168` varchar(100) DEFAULT NULL,
  `col169` varchar(100) DEFAULT NULL,
  `col170` varchar(100) DEFAULT NULL,
  `col171` varchar(100) DEFAULT NULL,
  `col172` varchar(100) DEFAULT NULL,
  `col173` varchar(100) DEFAULT NULL,
  `col174` varchar(100) DEFAULT NULL,
  `col175` varchar(100) DEFAULT NULL,
  `col176` varchar(100) DEFAULT NULL,
  `col177` varchar(100) DEFAULT NULL,
  `col178` varchar(100) DEFAULT NULL,
  `col179` varchar(100) DEFAULT NULL,
  `col180` varchar(100) DEFAULT NULL,
  `col181` varchar(100) DEFAULT NULL,
  `col182` varchar(100) DEFAULT NULL,
  `col183` varchar(100) DEFAULT NULL,
  `col184` varchar(100) DEFAULT NULL,
  `col185` varchar(100) DEFAULT NULL,
  `col186` varchar(100) DEFAULT NULL,
  `col187` varchar(100) DEFAULT NULL,
  `col188` varchar(100) DEFAULT NULL,
  `col189` varchar(100) DEFAULT NULL,
  `col190` varchar(100) DEFAULT NULL,
  `col191` varchar(100) DEFAULT NULL,
  `col192` varchar(100) DEFAULT NULL,
  `col193` varchar(100) DEFAULT NULL,
  `col194` varchar(100) DEFAULT NULL,
  `col195` varchar(100) DEFAULT NULL,
  `col196` varchar(100) DEFAULT NULL,
  `col197` varchar(100) DEFAULT NULL,
  `col198` varchar(100) DEFAULT NULL,
  `col199` varchar(100) DEFAULT NULL,
  `col200` varchar(100) DEFAULT NULL,
  `col201` varchar(100) DEFAULT NULL,
  `col202` varchar(100) DEFAULT NULL,
  `col203` varchar(100) DEFAULT NULL,
  `col204` varchar(100) DEFAULT NULL,
  `col205` varchar(100) DEFAULT NULL,
  `col206` varchar(100) DEFAULT NULL,
  `col207` varchar(100) DEFAULT NULL,
  `col208` varchar(100) DEFAULT NULL,
  `col209` varchar(100) DEFAULT NULL,
  `col210` varchar(100) DEFAULT NULL,
  `col211` varchar(100) DEFAULT NULL,
  `col212` varchar(100) DEFAULT NULL,
  `col213` varchar(100) DEFAULT NULL,
  `col214` varchar(100) DEFAULT NULL,
  `col215` varchar(100) DEFAULT NULL,
  `col216` varchar(100) DEFAULT NULL,
  `col217` varchar(100) DEFAULT NULL,
  `col218` varchar(100) DEFAULT NULL,
  `col219` varchar(100) DEFAULT NULL,
  `col220` varchar(100) DEFAULT NULL,
  `col221` varchar(100) DEFAULT NULL,
  `col222` varchar(100) DEFAULT NULL,
  `col223` varchar(100) DEFAULT NULL,
  `col224` varchar(100) DEFAULT NULL,
  `col225` varchar(100) DEFAULT NULL,
  `col226` varchar(100) DEFAULT NULL,
  `col227` varchar(100) DEFAULT NULL,
  `col228` varchar(100) DEFAULT NULL,
  `col229` varchar(100) DEFAULT NULL,
  `col230` varchar(100) DEFAULT NULL,
  `col231` varchar(100) DEFAULT NULL,
  `col232` varchar(100) DEFAULT NULL,
  `col233` varchar(100) DEFAULT NULL,
  `col234` varchar(100) DEFAULT NULL,
  `col235` varchar(100) DEFAULT NULL,
  `col236` varchar(100) DEFAULT NULL,
  `col237` varchar(100) DEFAULT NULL,
  `col238` varchar(100) DEFAULT NULL,
  `col239` varchar(100) DEFAULT NULL,
  `col240` varchar(100) DEFAULT NULL,
  `col241` varchar(100) DEFAULT NULL,
  `col242` varchar(100) DEFAULT NULL,
  `col243` varchar(100) DEFAULT NULL,
  `col244` varchar(100) DEFAULT NULL,
  `col245` varchar(100) DEFAULT NULL,
  `col246` varchar(100) DEFAULT NULL,
  `col247` varchar(100) DEFAULT NULL,
  `col248` varchar(100) DEFAULT NULL,
  `col249` varchar(100) DEFAULT NULL,
  `col250` varchar(100) DEFAULT NULL,
  `col251` varchar(100) DEFAULT NULL,
  `col252` varchar(100) DEFAULT NULL,
  `col253` varchar(100) DEFAULT NULL,
  `col254` varchar(100) DEFAULT NULL,
  `col255` varchar(100) DEFAULT NULL,
  `col256` varchar(100) DEFAULT NULL,
  `col257` varchar(100) DEFAULT NULL,
  `col258` varchar(100) DEFAULT NULL,
  `col259` varchar(100) DEFAULT NULL,
  `col260` varchar(100) DEFAULT NULL,
  `col261` varchar(100) DEFAULT NULL,
  `col262` varchar(100) DEFAULT NULL,
  `col263` varchar(100) DEFAULT NULL,
  `col264` varchar(100) DEFAULT NULL,
  `col265` varchar(100) DEFAULT NULL,
  `col266` varchar(100) DEFAULT NULL,
  `col267` varchar(100) DEFAULT NULL,
  `col268` varchar(100) DEFAULT NULL,
  `col269` varchar(100) DEFAULT NULL,
  `col270` varchar(100) DEFAULT NULL,
  `col271` varchar(100) DEFAULT NULL,
  `col272` varchar(100) DEFAULT NULL,
  `col273` varchar(100) DEFAULT NULL,
  `col274` varchar(100) DEFAULT NULL,
  `col275` varchar(100) DEFAULT NULL,
  `col276` varchar(100) DEFAULT NULL,
  `col277` varchar(100) DEFAULT NULL,
  `col278` varchar(100) DEFAULT NULL,
  `col279` varchar(100) DEFAULT NULL,
  `col280` varchar(100) DEFAULT NULL,
  `col281` varchar(100) DEFAULT NULL,
  `col282` varchar(100) DEFAULT NULL,
  `col283` varchar(100) DEFAULT NULL,
  `col284` varchar(100) DEFAULT NULL,
  `col285` varchar(100) DEFAULT NULL,
  `col286` varchar(100) DEFAULT NULL,
  `col287` varchar(100) DEFAULT NULL,
  `col288` varchar(100) DEFAULT NULL,
  `col289` varchar(100) DEFAULT NULL,
  `col290` varchar(100) DEFAULT NULL,
  `col291` varchar(100) DEFAULT NULL,
  `col292` varchar(100) DEFAULT NULL,
  `col293` varchar(100) DEFAULT NULL,
  `col294` varchar(100) DEFAULT NULL,
  `col295` varchar(100) DEFAULT NULL,
  `col296` varchar(100) DEFAULT NULL,
  `col297` varchar(100) DEFAULT NULL,
  `col298` varchar(100) DEFAULT NULL,
  `col299` varchar(100) DEFAULT NULL,
  `col300` varchar(100) DEFAULT NULL,
  `col301` varchar(100) DEFAULT NULL,
  `col302` varchar(100) DEFAULT NULL,
  `col303` varchar(100) DEFAULT NULL,
  `col304` varchar(100) DEFAULT NULL,
  `col305` varchar(100) DEFAULT NULL,
  `col306` varchar(100) DEFAULT NULL,
  `col307` varchar(100) DEFAULT NULL,
  `col308` varchar(100) DEFAULT NULL,
  `col309` varchar(100) DEFAULT NULL,
  `col310` varchar(100) DEFAULT NULL,
  `col311` varchar(100) DEFAULT NULL,
  `col312` varchar(100) DEFAULT NULL,
  `col313` varchar(100) DEFAULT NULL,
  `col314` varchar(100) DEFAULT NULL,
  `col315` varchar(100) DEFAULT NULL,
  `col316` varchar(100) DEFAULT NULL,
  `col317` varchar(100) DEFAULT NULL,
  `col318` varchar(100) DEFAULT NULL,
  `col319` varchar(100) DEFAULT NULL,
  `col320` varchar(100) DEFAULT NULL,
  `col321` varchar(100) DEFAULT NULL,
  `col322` varchar(100) DEFAULT NULL,
  `col323` varchar(100) DEFAULT NULL,
  `col324` varchar(100) DEFAULT NULL,
  `col325` varchar(100) DEFAULT NULL,
  `col326` varchar(100) DEFAULT NULL,
  `col327` varchar(100) DEFAULT NULL,
  `col328` varchar(100) DEFAULT NULL,
  `col329` varchar(100) DEFAULT NULL,
  `col330` varchar(100) DEFAULT NULL,
  `col331` varchar(100) DEFAULT NULL,
  `col332` varchar(100) DEFAULT NULL,
  `col333` varchar(100) DEFAULT NULL,
  `col334` varchar(100) DEFAULT NULL,
  `col335` varchar(100) DEFAULT NULL,
  `col336` varchar(100) DEFAULT NULL,
  `col337` varchar(100) DEFAULT NULL,
  `col338` varchar(100) DEFAULT NULL,
  `col339` varchar(100) DEFAULT NULL,
  `col340` varchar(100) DEFAULT NULL,
  `col341` varchar(100) DEFAULT NULL,
  `col342` varchar(100) DEFAULT NULL,
  `col343` varchar(100) DEFAULT NULL,
  `col344` varchar(100) DEFAULT NULL,
  `col345` varchar(100) DEFAULT NULL,
  `col346` varchar(100) DEFAULT NULL,
  `col347` varchar(100) DEFAULT NULL,
  `col348` varchar(100) DEFAULT NULL,
  `col349` varchar(100) DEFAULT NULL,
  `col350` varchar(100) DEFAULT NULL,
  `col351` varchar(100) DEFAULT NULL,
  `col352` varchar(100) DEFAULT NULL,
  `col353` varchar(100) DEFAULT NULL,
  `col354` varchar(100) DEFAULT NULL,
  `col355` varchar(100) DEFAULT NULL,
  `col356` varchar(100) DEFAULT NULL,
  `col357` varchar(100) DEFAULT NULL,
  `col358` varchar(100) DEFAULT NULL,
  `col359` varchar(100) DEFAULT NULL,
  `col360` varchar(100) DEFAULT NULL,
  `col361` varchar(100) DEFAULT NULL,
  `col362` varchar(100) DEFAULT NULL,
  `col363` varchar(100) DEFAULT NULL,
  `col364` varchar(100) DEFAULT NULL,
  `col365` varchar(100) DEFAULT NULL,
  `col366` varchar(100) DEFAULT NULL,
  `col367` varchar(100) DEFAULT NULL,
  `col368` varchar(100) DEFAULT NULL,
  `col369` varchar(100) DEFAULT NULL,
  `col370` varchar(100) DEFAULT NULL,
  `col371` varchar(100) DEFAULT NULL,
  `col372` varchar(100) DEFAULT NULL,
  `col373` varchar(100) DEFAULT NULL,
  `col374` varchar(100) DEFAULT NULL,
  `col375` varchar(100) DEFAULT NULL,
  `col376` varchar(100) DEFAULT NULL,
  `col377` varchar(100) DEFAULT NULL,
  `col378` varchar(100) DEFAULT NULL,
  `col379` varchar(100) DEFAULT NULL,
  `col380` varchar(100) DEFAULT NULL,
  `col381` varchar(100) DEFAULT NULL,
  `col382` varchar(100) DEFAULT NULL,
  `col383` varchar(100) DEFAULT NULL,
  `col384` varchar(100) DEFAULT NULL,
  `col385` varchar(100) DEFAULT NULL,
  `col386` varchar(100) DEFAULT NULL,
  `col387` varchar(100) DEFAULT NULL,
  `col388` varchar(100) DEFAULT NULL,
  `col389` varchar(100) DEFAULT NULL,
  `col390` varchar(100) DEFAULT NULL,
  `col391` varchar(100) DEFAULT NULL,
  `col392` varchar(100) DEFAULT NULL,
  `col393` varchar(100) DEFAULT NULL,
  `col394` varchar(100) DEFAULT NULL,
  `col395` varchar(100) DEFAULT NULL,
  `col396` varchar(100) DEFAULT NULL,
  `col397` varchar(100) DEFAULT NULL,
  `col398` varchar(100) DEFAULT NULL,
  `col399` varchar(100) DEFAULT NULL,
  `col400` varchar(100) DEFAULT NULL,
  `col401` varchar(100) DEFAULT NULL,
  `col402` varchar(100) DEFAULT NULL,
  `col403` varchar(100) DEFAULT NULL,
  `col404` varchar(100) DEFAULT NULL,
  `col405` varchar(100) DEFAULT NULL,
  `col406` varchar(100) DEFAULT NULL,
  `col407` varchar(100) DEFAULT NULL,
  `col408` varchar(100) DEFAULT NULL,
  `col409` varchar(100) DEFAULT NULL,
  `col410` varchar(100) DEFAULT NULL,
  `col411` varchar(100) DEFAULT NULL,
  `col412` varchar(100) DEFAULT NULL,
  `col413` varchar(100) DEFAULT NULL,
  `col414` varchar(100) DEFAULT NULL,
  `col415` varchar(100) DEFAULT NULL,
  `col416` varchar(100) DEFAULT NULL,
  `col417` varchar(100) DEFAULT NULL,
  `col418` varchar(100) DEFAULT NULL,
  `col419` varchar(100) DEFAULT NULL,
  `col420` varchar(100) DEFAULT NULL,
  `col421` varchar(100) DEFAULT NULL,
  `col422` varchar(100) DEFAULT NULL,
  `col423` varchar(100) DEFAULT NULL,
  `col424` varchar(100) DEFAULT NULL,
  `col425` varchar(100) DEFAULT NULL,
  `col426` varchar(100) DEFAULT NULL,
  `col427` varchar(100) DEFAULT NULL,
  `col428` varchar(100) DEFAULT NULL,
  `col429` varchar(100) DEFAULT NULL,
  `col430` varchar(100) DEFAULT NULL,
  `col431` varchar(100) DEFAULT NULL,
  `col432` varchar(100) DEFAULT NULL,
  `col433` varchar(100) DEFAULT NULL,
  `col434` varchar(100) DEFAULT NULL,
  `col435` varchar(100) DEFAULT NULL,
  `col436` varchar(100) DEFAULT NULL,
  `col437` varchar(100) DEFAULT NULL,
  `col438` varchar(100) DEFAULT NULL,
  `col439` varchar(100) DEFAULT NULL,
  `col440` varchar(100) DEFAULT NULL,
  `col441` varchar(100) DEFAULT NULL,
  `col442` varchar(100) DEFAULT NULL,
  `col443` varchar(100) DEFAULT NULL,
  `col444` varchar(100) DEFAULT NULL,
  `col445` varchar(100) DEFAULT NULL,
  `col446` varchar(100) DEFAULT NULL,
  `col447` varchar(100) DEFAULT NULL,
  `col448` varchar(100) DEFAULT NULL,
  `col449` varchar(100) DEFAULT NULL,
  `col450` varchar(100) DEFAULT NULL,
  `col451` varchar(100) DEFAULT NULL,
  `col452` varchar(100) DEFAULT NULL,
  `col453` varchar(100) DEFAULT NULL,
  `col454` varchar(100) DEFAULT NULL,
  `col455` varchar(100) DEFAULT NULL,
  `col456` varchar(100) DEFAULT NULL,
  `col457` varchar(100) DEFAULT NULL,
  `col458` varchar(100) DEFAULT NULL,
  `col459` varchar(100) DEFAULT NULL,
  `col460` varchar(100) DEFAULT NULL,
  `col461` varchar(100) DEFAULT NULL,
  `col462` varchar(100) DEFAULT NULL,
  `col463` varchar(100) DEFAULT NULL,
  `col464` varchar(100) DEFAULT NULL,
  `col465` varchar(100) DEFAULT NULL,
  `col466` varchar(100) DEFAULT NULL,
  `col467` varchar(100) DEFAULT NULL,
  `col468` varchar(100) DEFAULT NULL,
  `col469` varchar(100) DEFAULT NULL,
  `col470` varchar(100) DEFAULT NULL,
  `col471` varchar(100) DEFAULT NULL,
  `col472` varchar(100) DEFAULT NULL,
  `col473` varchar(100) DEFAULT NULL,
  `col474` varchar(100) DEFAULT NULL,
  `col475` varchar(100) DEFAULT NULL,
  `col476` varchar(100) DEFAULT NULL,
  `col477` varchar(100) DEFAULT NULL,
  `col478` varchar(100) DEFAULT NULL,
  `col479` varchar(100) DEFAULT NULL,
  `col480` varchar(100) DEFAULT NULL,
  `col481` varchar(100) DEFAULT NULL,
  `col482` varchar(100) DEFAULT NULL,
  `col483` varchar(100) DEFAULT NULL,
  `col484` varchar(100) DEFAULT NULL,
  `col485` varchar(100) DEFAULT NULL,
  `col486` varchar(100) DEFAULT NULL,
  `col487` varchar(100) DEFAULT NULL,
  `col488` varchar(100) DEFAULT NULL,
  `col489` varchar(100) DEFAULT NULL,
  `col490` varchar(100) DEFAULT NULL,
  `col491` varchar(100) DEFAULT NULL,
  `col492` varchar(100) DEFAULT NULL,
  `col493` varchar(100) DEFAULT NULL,
  `col494` varchar(100) DEFAULT NULL,
  `col495` varchar(100) DEFAULT NULL,
  `col496` varchar(100) DEFAULT NULL,
  `col497` varchar(100) DEFAULT NULL,
  `col498` varchar(100) DEFAULT NULL,
  `col499` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`po_id`,`id`),
  KEY `po_id` (`po_id`),
  KEY `combi` (`po_id`,`is_column`,`center_id`),
  KEY `batch_no` (`batch_no`),
  KEY `new_id` (`id`),
  KEY `phone_no` (`phone_no`),
  KEY `possible_keys` (`id`,`po_id`,`batch_no`,`phone_no`),
  KEY `NewIndex1` (`po_id`,`batch_no`,`phone_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
";
        $this->db->query($cmd);
        
        //po_transaction_data_ans_2_
        $cmd ="CREATE TABLE `po_transaction_data_ans_2_{$po_id}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `po_transaction_data_2_id` varchar(50) DEFAULT NULL,
  `crid_permanent` int(11) DEFAULT NULL,
  `batch_no` int(11) DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `record_no` int(11) DEFAULT NULL,
  `po_id` int(11) DEFAULT NULL,
  `is_column` varchar(3) DEFAULT NULL COMMENT 'YES / NO',
  `remarks` text,
  `remarks_2` text COMMENT 'for Separated value',
  `row_data` text COMMENT 'serialize json',
  `row_responses` text COMMENT 'serialize json',
  `call_date` varchar(32) DEFAULT NULL,
  `call_time` varchar(32) DEFAULT NULL,
  `col1` varchar(100) DEFAULT NULL,
  `col2` varchar(100) DEFAULT NULL,
  `col3` varchar(100) DEFAULT NULL,
  `col4` varchar(100) DEFAULT NULL,
  `col5` varchar(100) DEFAULT NULL,
  `col6` varchar(100) DEFAULT NULL,
  `col7` varchar(100) DEFAULT NULL,
  `col8` varchar(100) DEFAULT NULL,
  `col9` varchar(100) DEFAULT NULL,
  `col10` varchar(100) DEFAULT NULL,
  `col11` varchar(100) DEFAULT NULL,
  `col12` varchar(100) DEFAULT NULL,
  `col13` varchar(100) DEFAULT NULL,
  `col14` varchar(100) DEFAULT NULL,
  `col15` varchar(100) DEFAULT NULL,
  `col16` varchar(100) DEFAULT NULL,
  `col17` varchar(100) DEFAULT NULL,
  `col18` varchar(100) DEFAULT NULL,
  `col19` varchar(100) DEFAULT NULL,
  `col20` varchar(100) DEFAULT NULL,
  `col21` varchar(100) DEFAULT NULL,
  `col22` varchar(100) DEFAULT NULL,
  `col23` varchar(100) DEFAULT NULL,
  `col24` varchar(100) DEFAULT NULL,
  `col25` varchar(100) DEFAULT NULL,
  `col26` varchar(100) DEFAULT NULL,
  `col27` varchar(100) DEFAULT NULL,
  `col28` varchar(100) DEFAULT NULL,
  `col29` varchar(100) DEFAULT NULL,
  `col30` varchar(100) DEFAULT NULL,
  `col31` varchar(100) DEFAULT NULL,
  `col32` varchar(100) DEFAULT NULL,
  `col33` varchar(100) DEFAULT NULL,
  `col34` varchar(100) DEFAULT NULL,
  `col35` varchar(100) DEFAULT NULL,
  `col36` varchar(100) DEFAULT NULL,
  `col37` varchar(100) DEFAULT NULL,
  `col38` varchar(100) DEFAULT NULL,
  `col39` varchar(100) DEFAULT NULL,
  `col40` varchar(100) DEFAULT NULL,
  `col41` varchar(100) DEFAULT NULL,
  `col42` varchar(100) DEFAULT NULL,
  `col43` varchar(100) DEFAULT NULL,
  `col44` varchar(100) DEFAULT NULL,
  `col45` varchar(100) DEFAULT NULL,
  `col46` varchar(100) DEFAULT NULL,
  `col47` varchar(100) DEFAULT NULL,
  `col48` varchar(100) DEFAULT NULL,
  `col49` varchar(100) DEFAULT NULL,
  `col50` varchar(100) DEFAULT NULL,
  `col51` varchar(100) DEFAULT NULL,
  `col52` varchar(100) DEFAULT NULL,
  `col53` varchar(100) DEFAULT NULL,
  `col54` varchar(100) DEFAULT NULL,
  `col55` varchar(100) DEFAULT NULL,
  `col56` varchar(100) DEFAULT NULL,
  `col57` varchar(100) DEFAULT NULL,
  `col58` varchar(100) DEFAULT NULL,
  `col59` varchar(100) DEFAULT NULL,
  `col60` varchar(100) DEFAULT NULL,
  `col61` varchar(100) DEFAULT NULL,
  `col62` varchar(100) DEFAULT NULL,
  `col63` varchar(100) DEFAULT NULL,
  `col64` varchar(100) DEFAULT NULL,
  `col65` varchar(100) DEFAULT NULL,
  `col66` varchar(100) DEFAULT NULL,
  `col67` varchar(100) DEFAULT NULL,
  `col68` varchar(100) DEFAULT NULL,
  `col69` varchar(100) DEFAULT NULL,
  `col70` varchar(100) DEFAULT NULL,
  `col71` varchar(100) DEFAULT NULL,
  `col72` varchar(100) DEFAULT NULL,
  `col73` varchar(100) DEFAULT NULL,
  `col74` varchar(100) DEFAULT NULL,
  `col75` varchar(100) DEFAULT NULL,
  `col76` varchar(100) DEFAULT NULL,
  `col77` varchar(100) DEFAULT NULL,
  `col78` varchar(100) DEFAULT NULL,
  `col79` varchar(100) DEFAULT NULL,
  `col80` varchar(100) DEFAULT NULL,
  `col81` varchar(100) DEFAULT NULL,
  `col82` varchar(100) DEFAULT NULL,
  `col83` varchar(100) DEFAULT NULL,
  `col84` varchar(100) DEFAULT NULL,
  `col85` varchar(100) DEFAULT NULL,
  `col86` varchar(100) DEFAULT NULL,
  `col87` varchar(100) DEFAULT NULL,
  `col88` varchar(100) DEFAULT NULL,
  `col89` varchar(100) DEFAULT NULL,
  `col90` varchar(100) DEFAULT NULL,
  `col91` varchar(100) DEFAULT NULL,
  `col92` varchar(100) DEFAULT NULL,
  `col93` varchar(100) DEFAULT NULL,
  `col94` varchar(100) DEFAULT NULL,
  `col95` varchar(100) DEFAULT NULL,
  `col96` varchar(100) DEFAULT NULL,
  `col97` varchar(100) DEFAULT NULL,
  `col98` varchar(100) DEFAULT NULL,
  `col99` varchar(100) DEFAULT NULL,
  `col100` varchar(100) DEFAULT NULL,
  `col500` varchar(100) DEFAULT NULL,
  `col101` varchar(100) DEFAULT NULL,
  `col102` varchar(100) DEFAULT NULL,
  `col103` varchar(100) DEFAULT NULL,
  `col104` varchar(100) DEFAULT NULL,
  `col105` varchar(100) DEFAULT NULL,
  `col106` varchar(100) DEFAULT NULL,
  `col107` varchar(100) DEFAULT NULL,
  `col108` varchar(100) DEFAULT NULL,
  `col109` varchar(100) DEFAULT NULL,
  `col110` varchar(100) DEFAULT NULL,
  `col111` varchar(100) DEFAULT NULL,
  `col112` varchar(100) DEFAULT NULL,
  `col113` varchar(100) DEFAULT NULL,
  `col114` varchar(100) DEFAULT NULL,
  `col115` varchar(100) DEFAULT NULL,
  `col116` varchar(100) DEFAULT NULL,
  `col117` varchar(100) DEFAULT NULL,
  `col118` varchar(100) DEFAULT NULL,
  `col119` varchar(100) DEFAULT NULL,
  `col120` varchar(100) DEFAULT NULL,
  `col121` varchar(100) DEFAULT NULL,
  `col122` varchar(100) DEFAULT NULL,
  `col123` varchar(100) DEFAULT NULL,
  `col124` varchar(100) DEFAULT NULL,
  `col125` varchar(100) DEFAULT NULL,
  `col126` varchar(100) DEFAULT NULL,
  `col127` varchar(100) DEFAULT NULL,
  `col128` varchar(100) DEFAULT NULL,
  `col129` varchar(100) DEFAULT NULL,
  `col130` varchar(100) DEFAULT NULL,
  `col131` varchar(100) DEFAULT NULL,
  `col132` varchar(100) DEFAULT NULL,
  `col133` varchar(100) DEFAULT NULL,
  `col134` varchar(100) DEFAULT NULL,
  `col135` varchar(100) DEFAULT NULL,
  `col136` varchar(100) DEFAULT NULL,
  `col137` varchar(100) DEFAULT NULL,
  `col138` varchar(100) DEFAULT NULL,
  `col139` varchar(100) DEFAULT NULL,
  `col140` varchar(100) DEFAULT NULL,
  `col141` varchar(100) DEFAULT NULL,
  `col142` varchar(100) DEFAULT NULL,
  `col143` varchar(100) DEFAULT NULL,
  `col144` varchar(100) DEFAULT NULL,
  `col145` varchar(100) DEFAULT NULL,
  `col146` varchar(100) DEFAULT NULL,
  `col147` varchar(100) DEFAULT NULL,
  `col148` varchar(100) DEFAULT NULL,
  `col149` varchar(100) DEFAULT NULL,
  `col150` varchar(100) DEFAULT NULL,
  `col151` varchar(100) DEFAULT NULL,
  `col152` varchar(100) DEFAULT NULL,
  `col153` varchar(100) DEFAULT NULL,
  `col154` varchar(100) DEFAULT NULL,
  `col155` varchar(100) DEFAULT NULL,
  `col156` varchar(100) DEFAULT NULL,
  `col157` varchar(100) DEFAULT NULL,
  `col158` varchar(100) DEFAULT NULL,
  `col159` varchar(100) DEFAULT NULL,
  `col160` varchar(100) DEFAULT NULL,
  `col161` varchar(100) DEFAULT NULL,
  `col162` varchar(100) DEFAULT NULL,
  `col163` varchar(100) DEFAULT NULL,
  `col164` varchar(100) DEFAULT NULL,
  `col165` varchar(100) DEFAULT NULL,
  `col166` varchar(100) DEFAULT NULL,
  `col167` varchar(100) DEFAULT NULL,
  `col168` varchar(100) DEFAULT NULL,
  `col169` varchar(100) DEFAULT NULL,
  `col170` varchar(100) DEFAULT NULL,
  `col171` varchar(100) DEFAULT NULL,
  `col172` varchar(100) DEFAULT NULL,
  `col173` varchar(100) DEFAULT NULL,
  `col174` varchar(100) DEFAULT NULL,
  `col175` varchar(100) DEFAULT NULL,
  `col176` varchar(100) DEFAULT NULL,
  `col177` varchar(100) DEFAULT NULL,
  `col178` varchar(100) DEFAULT NULL,
  `col179` varchar(100) DEFAULT NULL,
  `col180` varchar(100) DEFAULT NULL,
  `col181` varchar(100) DEFAULT NULL,
  `col182` varchar(100) DEFAULT NULL,
  `col183` varchar(100) DEFAULT NULL,
  `col184` varchar(100) DEFAULT NULL,
  `col185` varchar(100) DEFAULT NULL,
  `col186` varchar(100) DEFAULT NULL,
  `col187` varchar(100) DEFAULT NULL,
  `col188` varchar(100) DEFAULT NULL,
  `col189` varchar(100) DEFAULT NULL,
  `col190` varchar(100) DEFAULT NULL,
  `col191` varchar(100) DEFAULT NULL,
  `col192` varchar(100) DEFAULT NULL,
  `col193` varchar(100) DEFAULT NULL,
  `col194` varchar(100) DEFAULT NULL,
  `col195` varchar(100) DEFAULT NULL,
  `col196` varchar(100) DEFAULT NULL,
  `col197` varchar(100) DEFAULT NULL,
  `col198` varchar(100) DEFAULT NULL,
  `col199` varchar(100) DEFAULT NULL,
  `col200` varchar(100) DEFAULT NULL,
  `col201` varchar(100) DEFAULT NULL,
  `col202` varchar(100) DEFAULT NULL,
  `col203` varchar(100) DEFAULT NULL,
  `col204` varchar(100) DEFAULT NULL,
  `col205` varchar(100) DEFAULT NULL,
  `col206` varchar(100) DEFAULT NULL,
  `col207` varchar(100) DEFAULT NULL,
  `col208` varchar(100) DEFAULT NULL,
  `col209` varchar(100) DEFAULT NULL,
  `col210` varchar(100) DEFAULT NULL,
  `col211` varchar(100) DEFAULT NULL,
  `col212` varchar(100) DEFAULT NULL,
  `col213` varchar(100) DEFAULT NULL,
  `col214` varchar(100) DEFAULT NULL,
  `col215` varchar(100) DEFAULT NULL,
  `col216` varchar(100) DEFAULT NULL,
  `col217` varchar(100) DEFAULT NULL,
  `col218` varchar(100) DEFAULT NULL,
  `col219` varchar(100) DEFAULT NULL,
  `col220` varchar(100) DEFAULT NULL,
  `col221` varchar(100) DEFAULT NULL,
  `col222` varchar(100) DEFAULT NULL,
  `col223` varchar(100) DEFAULT NULL,
  `col224` varchar(100) DEFAULT NULL,
  `col225` varchar(100) DEFAULT NULL,
  `col226` varchar(100) DEFAULT NULL,
  `col227` varchar(100) DEFAULT NULL,
  `col228` varchar(100) DEFAULT NULL,
  `col229` varchar(100) DEFAULT NULL,
  `col230` varchar(100) DEFAULT NULL,
  `col231` varchar(100) DEFAULT NULL,
  `col232` varchar(100) DEFAULT NULL,
  `col233` varchar(100) DEFAULT NULL,
  `col234` varchar(100) DEFAULT NULL,
  `col235` varchar(100) DEFAULT NULL,
  `col236` varchar(100) DEFAULT NULL,
  `col237` varchar(100) DEFAULT NULL,
  `col238` varchar(100) DEFAULT NULL,
  `col239` varchar(100) DEFAULT NULL,
  `col240` varchar(100) DEFAULT NULL,
  `col241` varchar(100) DEFAULT NULL,
  `col242` varchar(100) DEFAULT NULL,
  `col243` varchar(100) DEFAULT NULL,
  `col244` varchar(100) DEFAULT NULL,
  `col245` varchar(100) DEFAULT NULL,
  `col246` varchar(100) DEFAULT NULL,
  `col247` varchar(100) DEFAULT NULL,
  `col248` varchar(100) DEFAULT NULL,
  `col249` varchar(100) DEFAULT NULL,
  `col250` varchar(100) DEFAULT NULL,
  `col251` varchar(100) DEFAULT NULL,
  `col252` varchar(100) DEFAULT NULL,
  `col253` varchar(100) DEFAULT NULL,
  `col254` varchar(100) DEFAULT NULL,
  `col255` varchar(100) DEFAULT NULL,
  `col256` varchar(100) DEFAULT NULL,
  `col257` varchar(100) DEFAULT NULL,
  `col258` varchar(100) DEFAULT NULL,
  `col259` varchar(100) DEFAULT NULL,
  `col260` varchar(100) DEFAULT NULL,
  `col261` varchar(100) DEFAULT NULL,
  `col262` varchar(100) DEFAULT NULL,
  `col263` varchar(100) DEFAULT NULL,
  `col264` varchar(100) DEFAULT NULL,
  `col265` varchar(100) DEFAULT NULL,
  `col266` varchar(100) DEFAULT NULL,
  `col267` varchar(100) DEFAULT NULL,
  `col268` varchar(100) DEFAULT NULL,
  `col269` varchar(100) DEFAULT NULL,
  `col270` varchar(100) DEFAULT NULL,
  `col271` varchar(100) DEFAULT NULL,
  `col272` varchar(100) DEFAULT NULL,
  `col273` varchar(100) DEFAULT NULL,
  `col274` varchar(100) DEFAULT NULL,
  `col275` varchar(100) DEFAULT NULL,
  `col276` varchar(100) DEFAULT NULL,
  `col277` varchar(100) DEFAULT NULL,
  `col278` varchar(100) DEFAULT NULL,
  `col279` varchar(100) DEFAULT NULL,
  `col280` varchar(100) DEFAULT NULL,
  `col281` varchar(100) DEFAULT NULL,
  `col282` varchar(100) DEFAULT NULL,
  `col283` varchar(100) DEFAULT NULL,
  `col284` varchar(100) DEFAULT NULL,
  `col285` varchar(100) DEFAULT NULL,
  `col286` varchar(100) DEFAULT NULL,
  `col287` varchar(100) DEFAULT NULL,
  `col288` varchar(100) DEFAULT NULL,
  `col289` varchar(100) DEFAULT NULL,
  `col290` varchar(100) DEFAULT NULL,
  `col291` varchar(100) DEFAULT NULL,
  `col292` varchar(100) DEFAULT NULL,
  `col293` varchar(100) DEFAULT NULL,
  `col294` varchar(100) DEFAULT NULL,
  `col295` varchar(100) DEFAULT NULL,
  `col296` varchar(100) DEFAULT NULL,
  `col297` varchar(100) DEFAULT NULL,
  `col298` varchar(100) DEFAULT NULL,
  `col299` varchar(100) DEFAULT NULL,
  `col300` varchar(100) DEFAULT NULL,
  `col301` varchar(100) DEFAULT NULL,
  `col302` varchar(100) DEFAULT NULL,
  `col303` varchar(100) DEFAULT NULL,
  `col304` varchar(100) DEFAULT NULL,
  `col305` varchar(100) DEFAULT NULL,
  `col306` varchar(100) DEFAULT NULL,
  `col307` varchar(100) DEFAULT NULL,
  `col308` varchar(100) DEFAULT NULL,
  `col309` varchar(100) DEFAULT NULL,
  `col310` varchar(100) DEFAULT NULL,
  `col311` varchar(100) DEFAULT NULL,
  `col312` varchar(100) DEFAULT NULL,
  `col313` varchar(100) DEFAULT NULL,
  `col314` varchar(100) DEFAULT NULL,
  `col315` varchar(100) DEFAULT NULL,
  `col316` varchar(100) DEFAULT NULL,
  `col317` varchar(100) DEFAULT NULL,
  `col318` varchar(100) DEFAULT NULL,
  `col319` varchar(100) DEFAULT NULL,
  `col320` varchar(100) DEFAULT NULL,
  `col321` varchar(100) DEFAULT NULL,
  `col322` varchar(100) DEFAULT NULL,
  `col323` varchar(100) DEFAULT NULL,
  `col324` varchar(100) DEFAULT NULL,
  `col325` varchar(100) DEFAULT NULL,
  `col326` varchar(100) DEFAULT NULL,
  `col327` varchar(100) DEFAULT NULL,
  `col328` varchar(100) DEFAULT NULL,
  `col329` varchar(100) DEFAULT NULL,
  `col330` varchar(100) DEFAULT NULL,
  `col331` varchar(100) DEFAULT NULL,
  `col332` varchar(100) DEFAULT NULL,
  `col333` varchar(100) DEFAULT NULL,
  `col334` varchar(100) DEFAULT NULL,
  `col335` varchar(100) DEFAULT NULL,
  `col336` varchar(100) DEFAULT NULL,
  `col337` varchar(100) DEFAULT NULL,
  `col338` varchar(100) DEFAULT NULL,
  `col339` varchar(100) DEFAULT NULL,
  `col340` varchar(100) DEFAULT NULL,
  `col341` varchar(100) DEFAULT NULL,
  `col342` varchar(100) DEFAULT NULL,
  `col343` varchar(100) DEFAULT NULL,
  `col344` varchar(100) DEFAULT NULL,
  `col345` varchar(100) DEFAULT NULL,
  `col346` varchar(100) DEFAULT NULL,
  `col347` varchar(100) DEFAULT NULL,
  `col348` varchar(100) DEFAULT NULL,
  `col349` varchar(100) DEFAULT NULL,
  `col350` varchar(100) DEFAULT NULL,
  `col351` varchar(100) DEFAULT NULL,
  `col352` varchar(100) DEFAULT NULL,
  `col353` varchar(100) DEFAULT NULL,
  `col354` varchar(100) DEFAULT NULL,
  `col355` varchar(100) DEFAULT NULL,
  `col356` varchar(100) DEFAULT NULL,
  `col357` varchar(100) DEFAULT NULL,
  `col358` varchar(100) DEFAULT NULL,
  `col359` varchar(100) DEFAULT NULL,
  `col360` varchar(100) DEFAULT NULL,
  `col361` varchar(100) DEFAULT NULL,
  `col362` varchar(100) DEFAULT NULL,
  `col363` varchar(100) DEFAULT NULL,
  `col364` varchar(100) DEFAULT NULL,
  `col365` varchar(100) DEFAULT NULL,
  `col366` varchar(100) DEFAULT NULL,
  `col367` varchar(100) DEFAULT NULL,
  `col368` varchar(100) DEFAULT NULL,
  `col369` varchar(100) DEFAULT NULL,
  `col370` varchar(100) DEFAULT NULL,
  `col371` varchar(100) DEFAULT NULL,
  `col372` varchar(100) DEFAULT NULL,
  `col373` varchar(100) DEFAULT NULL,
  `col374` varchar(100) DEFAULT NULL,
  `col375` varchar(100) DEFAULT NULL,
  `col376` varchar(100) DEFAULT NULL,
  `col377` varchar(100) DEFAULT NULL,
  `col378` varchar(100) DEFAULT NULL,
  `col379` varchar(100) DEFAULT NULL,
  `col380` varchar(100) DEFAULT NULL,
  `col381` varchar(100) DEFAULT NULL,
  `col382` varchar(100) DEFAULT NULL,
  `col383` varchar(100) DEFAULT NULL,
  `col384` varchar(100) DEFAULT NULL,
  `col385` varchar(100) DEFAULT NULL,
  `col386` varchar(100) DEFAULT NULL,
  `col387` varchar(100) DEFAULT NULL,
  `col388` varchar(100) DEFAULT NULL,
  `col389` varchar(100) DEFAULT NULL,
  `col390` varchar(100) DEFAULT NULL,
  `col391` varchar(100) DEFAULT NULL,
  `col392` varchar(100) DEFAULT NULL,
  `col393` varchar(100) DEFAULT NULL,
  `col394` varchar(100) DEFAULT NULL,
  `col395` varchar(100) DEFAULT NULL,
  `col396` varchar(100) DEFAULT NULL,
  `col397` varchar(100) DEFAULT NULL,
  `col398` varchar(100) DEFAULT NULL,
  `col399` varchar(100) DEFAULT NULL,
  `col400` varchar(100) DEFAULT NULL,
  `col401` varchar(100) DEFAULT NULL,
  `col402` varchar(100) DEFAULT NULL,
  `col403` varchar(100) DEFAULT NULL,
  `col404` varchar(100) DEFAULT NULL,
  `col405` varchar(100) DEFAULT NULL,
  `col406` varchar(100) DEFAULT NULL,
  `col407` varchar(100) DEFAULT NULL,
  `col408` varchar(100) DEFAULT NULL,
  `col409` varchar(100) DEFAULT NULL,
  `col410` varchar(100) DEFAULT NULL,
  `col411` varchar(100) DEFAULT NULL,
  `col412` varchar(100) DEFAULT NULL,
  `col413` varchar(100) DEFAULT NULL,
  `col414` varchar(100) DEFAULT NULL,
  `col415` varchar(100) DEFAULT NULL,
  `col416` varchar(100) DEFAULT NULL,
  `col417` varchar(100) DEFAULT NULL,
  `col418` varchar(100) DEFAULT NULL,
  `col419` varchar(100) DEFAULT NULL,
  `col420` varchar(100) DEFAULT NULL,
  `col421` varchar(100) DEFAULT NULL,
  `col422` varchar(100) DEFAULT NULL,
  `col423` varchar(100) DEFAULT NULL,
  `col424` varchar(100) DEFAULT NULL,
  `col425` varchar(100) DEFAULT NULL,
  `col426` varchar(100) DEFAULT NULL,
  `col427` varchar(100) DEFAULT NULL,
  `col428` varchar(100) DEFAULT NULL,
  `col429` varchar(100) DEFAULT NULL,
  `col430` varchar(100) DEFAULT NULL,
  `col431` varchar(100) DEFAULT NULL,
  `col432` varchar(100) DEFAULT NULL,
  `col433` varchar(100) DEFAULT NULL,
  `col434` varchar(100) DEFAULT NULL,
  `col435` varchar(100) DEFAULT NULL,
  `col436` varchar(100) DEFAULT NULL,
  `col437` varchar(100) DEFAULT NULL,
  `col438` varchar(100) DEFAULT NULL,
  `col439` varchar(100) DEFAULT NULL,
  `col440` varchar(100) DEFAULT NULL,
  `col441` varchar(100) DEFAULT NULL,
  `col442` varchar(100) DEFAULT NULL,
  `col443` varchar(100) DEFAULT NULL,
  `col444` varchar(100) DEFAULT NULL,
  `col445` varchar(100) DEFAULT NULL,
  `col446` varchar(100) DEFAULT NULL,
  `col447` varchar(100) DEFAULT NULL,
  `col448` varchar(100) DEFAULT NULL,
  `col449` varchar(100) DEFAULT NULL,
  `col450` varchar(100) DEFAULT NULL,
  `col451` varchar(100) DEFAULT NULL,
  `col452` varchar(100) DEFAULT NULL,
  `col453` varchar(100) DEFAULT NULL,
  `col454` varchar(100) DEFAULT NULL,
  `col455` varchar(100) DEFAULT NULL,
  `col456` varchar(100) DEFAULT NULL,
  `col457` varchar(100) DEFAULT NULL,
  `col458` varchar(100) DEFAULT NULL,
  `col459` varchar(100) DEFAULT NULL,
  `col460` varchar(100) DEFAULT NULL,
  `col461` varchar(100) DEFAULT NULL,
  `col462` varchar(100) DEFAULT NULL,
  `col463` varchar(100) DEFAULT NULL,
  `col464` varchar(100) DEFAULT NULL,
  `col465` varchar(100) DEFAULT NULL,
  `col466` varchar(100) DEFAULT NULL,
  `col467` varchar(100) DEFAULT NULL,
  `col468` varchar(100) DEFAULT NULL,
  `col469` varchar(100) DEFAULT NULL,
  `col470` varchar(100) DEFAULT NULL,
  `col471` varchar(100) DEFAULT NULL,
  `col472` varchar(100) DEFAULT NULL,
  `col473` varchar(100) DEFAULT NULL,
  `col474` varchar(100) DEFAULT NULL,
  `col475` varchar(100) DEFAULT NULL,
  `col476` varchar(100) DEFAULT NULL,
  `col477` varchar(100) DEFAULT NULL,
  `col478` varchar(100) DEFAULT NULL,
  `col479` varchar(100) DEFAULT NULL,
  `col480` varchar(100) DEFAULT NULL,
  `col481` varchar(100) DEFAULT NULL,
  `col482` varchar(100) DEFAULT NULL,
  `col483` varchar(100) DEFAULT NULL,
  `col484` varchar(100) DEFAULT NULL,
  `col485` varchar(100) DEFAULT NULL,
  `col486` varchar(100) DEFAULT NULL,
  `col487` varchar(100) DEFAULT NULL,
  `col488` varchar(100) DEFAULT NULL,
  `col489` varchar(100) DEFAULT NULL,
  `col490` varchar(100) DEFAULT NULL,
  `col491` varchar(100) DEFAULT NULL,
  `col492` varchar(100) DEFAULT NULL,
  `col493` varchar(100) DEFAULT NULL,
  `col494` varchar(100) DEFAULT NULL,
  `col495` varchar(100) DEFAULT NULL,
  `col496` varchar(100) DEFAULT NULL,
  `col497` varchar(100) DEFAULT NULL,
  `col498` varchar(100) DEFAULT NULL,
  `col499` varchar(100) DEFAULT NULL,
  `status` char(1) DEFAULT NULL COMMENT 'Y - with reponse - Default N - no response',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(50) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_by` varchar(50) DEFAULT NULL,
  `status_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`crid_permanent`),
  KEY `phone2` (`col2`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";
        $this->db->query($cmd);
    }
    public function get_co_response($params)
    {
        $cmd = "SELECT cr.id,cr.create_date,cr.comment,CONCAT(us.fname,' ',us.lname) as name FROM co_response cr
        INNER JOIN users us on cr.create_by = us.username
        WHERE cr.co_id = {$params['co_id']}";
        $rs = $this->db->fetchAll($cmd);
        return $rs;
    }
    
    public function add_co_response($params)
    {
         $insert_co = array(
            'po_id'=>$params['txtID'], 
            'co_id'=>$params['id'], 
            'comment'=>$params['co_notes'], 
            'create_by'=>$_SESSION['username'],
        ); 
        if (!$this->db->insert('co_response', $insert_co)) {
            return $this->failed("Unable to add response.");
        } else {
                $update_data = array(
                    'status'=>$params['co_status'],
                    'update_by'=>$_SESSION['username'],
                    'update_date'=>date('Y-m-d H:i:s'), 
                );
            $this->db->update('change_order', $update_data,'id=' . $params['id']);  
            $response = array('message' => "Response has been submitted.",); 
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Change Order has been updated','PO',$datetime,$_SESSION['username'],$params['txtID']);
            $cmd = "select po_no from po where id ={$params['txtID']}";
            $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            Change Order has been modified for PO number {$rs['po_no']} by  {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
           foreach($email as $s){
            $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");  
            }
            return $this->success($response);
        }
    }
    public function get_change_order_list($params)
    {
        $cmd = "SELECT CONCAT(us.fname, ' ', us.lname) as assigned, co.id, co.po_id, co.center_id, co.user_id, CASE change_type 
        WHEN 1 THEN 'Date / Time Change' 
        WHEN 2 THEN 'Script Change'
        WHEN 3 THEN 'Project Hold'
        WHEN 4 THEN 'Data Change'
        WHEN 5 THEN 'Complaint Investigation'
        WHEN 6 THEN 'Monitor Request'
        WHEN 7 THEN 'Goals Change'
        WHEN 7 THEN 'Other' END as change_type,
        CASE co.status 
        WHEN 'N' THEN 'NEW'
        WHEN 'A' THEN 'ASSIGNED'
        WHEN 'P' THEN 'PROCESSING'
        WHEN 'H' THEN 'HOLD'
        WHEN 'C' THEN 'COMPLETED' END as status,
        co.description, co.notes, co.create_date, 
        co.create_by, co.update_date, co.update_by FROM change_order co
        INNER JOIN users us on co.user_id = us.id WHERE co.po_id={$params['po_id']} AND co.center_id={$_SESSION['user_center']}";
     
        $rs = $this->db->fetchAll($cmd);
        return $this->success($rs);
    }
    
    public function create_change_order($params)
    {
         $insert_co = array(
            'po_id'=>$params['txtID'], 
            'status'=>'N',  
            'center_id'=>$params['change_type'],  
            'user_id'=>$params['assigned_user'],  
            'change_type'=>$params['change_type'], 
            'description'=>$params['txtDesc'], 
            'notes'=>$params['co_notes'], 
            'create_by'=>$_SESSION['username'],
        ); 
        if (!$this->db->insert('change_order', $insert_co)) {
            return $this->failed("Unable to add new change order.");
        } else {
            $response = array('message' => "New change order has been submitted.",); 
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Submitted new Change Order','CO',$datetime,$_SESSION['username'],$params['txtID']);
            
            $cmd = "select po_no from po where id ={$params['txtID']}";
            $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            Change Order has been created for PO number {$rs['po_no']} by  {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
           foreach($email as $s){
                $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");      
           }
            return $this->success($response);
        }
    }
    
    public function get_users_per_center($center_id)
    {
        $cmd = "SELECT id, CONCAT(fname, ' ', lname) name FROM users WHERE status ='A' and user_center = {$center_id}"  ;
        return $this->db->fetchPairs($cmd);    
    } 
    public function get_for_coby_id($params)
    {
    $cmd = "SELECT CONCAT(us.fname, ' ', us.lname) as creator, co.id, co.po_id, co.center_id, co.user_id, CASE change_type 
        WHEN 1 THEN 'Date / Time Change' 
        WHEN 2 THEN 'Script Change'
        WHEN 3 THEN 'Project Hold'
        WHEN 4 THEN 'Data Change'
        WHEN 5 THEN 'Complaint Investigation'
        WHEN 6 THEN 'Monitor Request'
        WHEN 7 THEN 'Goals Change'
        WHEN 7 THEN 'Other' END as change_type,
        CASE co.status 
        WHEN 'N' THEN 'NEW'
        WHEN 'A' THEN 'ASSIGNED'
        WHEN 'P' THEN 'PROCESSING'
        WHEN 'H' THEN 'HOLD'
        WHEN 'C' THEN 'COMPLETED' END as status,
        co.description, co.notes, co.create_date, 
        co.create_by, co.update_date, co.update_by FROM change_order co
        INNER JOIN users us on co.create_by = us.username WHERE co.id={$params['co_id']}";
        return $this->db->fetchAll($cmd);    
    } 
    
    
     public function get_center_for_co($po_id)
    {
        $cmd = "SELECT cc.id,cc.name FROM call_center cc 
        INNER JOIN center_calling_list ccl on cc.id=ccl.center_id 
        WHERE cc.status='A' AND ccl.po_id={$po_id}"  ;
        return $this->db->fetchPairs($cmd);    
    } 
    
    public function get_usertype($username){
        $cmd = "select ut.description as user_type from user_types ut 
        inner join users u on u.user_type = ut.id
        where u.username = '{$username}'";
        $rs = $this->db->fetchRow($cmd);
        return $rs['user_type'];
    }    
    
     
  public function reassign_specific_center($center_id, $po_id, $batch_no, $from, $to)
    {
        $get_column_cmd="select col_fields, process_option_id, phone_field from po_transaction_data_2_column dc where is_column =1 and  po_id ={$po_id} AND batch_no={$batch_no} limit 1";
        
        $row = $this->db->fetchRow($get_column_cmd);
        
        $phone_field = $row['phone_field'];
        $col_fields = explode("|",unserialize($row['col_fields']));
        $col_fields_db ="";
        foreach($col_fields as $r)
        {
            $col_fields_db.="da.{$r}," ;   
        }
        
        $status_codes = $this->get_status_codes($po_id);
        
        $status_codes_callable = "";
        

        foreach($status_codes as $s)
        {
               if( strtolower($s['status_type']) =="complete")//  if( strtolower($s['status_type']) =="callable")
            {
                $status = $s['status_code'];
                $status_codes_callable .= "'{$status}',";
            }
        }
        
        if($status_codes_callable != "") {
            $status_codes_callable = substr($status_codes_callable, 0, strlen($status_codes_callable)-1); 
        }
 
        
        $process_option_id = $row['process_option_id'];
        //print_r($col_fields_db);
        $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);
        
        $first_name_field = $this->get_first_name_field($po_id);
      
        $cmd ="SELECT {$col_fields_db} FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";
        $res = $this->db->fetchRow($cmd);
        
        
        $col_cmd ="SELECT * FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";
        $columns = $this->db->fetchRow($col_cmd);
        $col_ctr =1;
        while($columns["col{$col_ctr}"] <> "")
        {
            if( trim($columns["col{$col_ctr}"]) == trim($phone_field))
            {
                $phone_field = "col{$col_ctr}";
                break;
            }
            $col_ctr ++;
        }
        $formated_po_id=$this->get_po_number($po_id);
        $file_name =$formated_po_id .'_'. time() .".csv";
        
        
        
        $cmd ="SELECT {$col_fields_db} FROM po_transaction_data_2_column da WHERE po_id={$po_id} LIMIT 1";    
        
        

        $res = $this->db->fetchRow($cmd);
         $data_result =$this->check_data_if_result_exist($po_id);
         $join = "";
         if($data_result>0){
            $join = " INNER ";    
         }else{
            $join = " LEFT "; 
         }
      
      $fp = fopen('php://output', 'w');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
       fputcsv($fp, array_values($res)); 
        if($process_option_id <> 1)
        {
            $cmd ="SELECT  {$col_fields_db} FROM po_transaction_data_2_{$po_id} da
                 WHERE da.phone_no not in (
                SELECT cl.phone_no FROM po_transaction_data_2_calling_list_{$po_id} cl
              
                INNER JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2
                WHERE  cl.id>={$from} AND  cl.id<={$to}";
              //  LEFT JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2  
            if($status_codes_callable != "")
            {
                /*$cmd .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable})) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no}";*/
                $cmd .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable}))";
            }else{
                /*$cmd .="
                AND cl.batch_no={$batch_no}) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no} ";      */ 
                $cmd .="
                AND cl.batch_no={$batch_no})";
                          
                
            }
      
           // $cmd .=" WHERE da.id>={$from} AND da.id<={$to}";
            $cmd .=" AND da.id>={$from} AND da.id<={$to}";
            
           $res_out = $this->db->fetchAllTOFile($cmd, $fp);          
            
            $for_insert  ="";
            $for_insert .= "{SELECT}";
            $for_insert .= " FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in  (
            SELECT cl.phone_no FROM po_transaction_data_2_calling_list_{$po_id} cl
            INNER JOIN po_transaction_data_ans_2_{$po_id} ans ON cl.phone_no=ans.col2
            WHERE  cl.id>={$from} AND  cl.id<={$to}";
            
            if($status_codes_callable != "")
            {
                /*$for_insert .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable})) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no}";*/
                
                $for_insert .="
                AND cl.batch_no={$batch_no} AND status_code IN({$status_codes_callable}))";
              
                
            }else{
               /* $for_insert .="
                AND cl.batch_no={$batch_no}) AS T ON T.phone_no=da.phone_no
                AND da.batch_no={$batch_no} ";          */      
                
                 $for_insert .="
                AND da.batch_no={$batch_no} ";          
                
            }
            $for_insert .=" AND da.id>={$from} AND da.id<={$to}";
//            $for_insert .=" WHERE da.id>={$from} AND da.id<={$to}";
          
            $res = $this->db->fetchReassignToFile("", "", $phone_field, $po_id, $for_insert, $batch_no,$from,$to);
             $this->db->query("UPDATE center_calling_list set assign_record_cnt = assign_record_cnt-{$res_out} , is_reassign = 1 WHERE record_cnt_from = {$from} AND record_cnt_to={$to} and po_id ={$po_id} AND  batch_no = {$batch_no}");
             
             $this->db->query("UPDATE po_parameter set universe = universe - {$res_out} WHERE po_id={$po_id}");
             
              $this->db->update('po_parameter', $values, 'po_id='.$id);
        }else
        {
            $cmd ="SELECT  {$col_fields_db} 
            FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in (
            SELECT col2 FROM po_transaction_data_ans_2_{$po_id}
            WHERE  crid_permanent>={$from} AND  crid_permanent<={$to}
            ";
            if ($status_codes_callable !== "")
            {
                   $cmd .=" AND status_code IN({$status_codes_callable})) AND da.batch_no={$batch_no}";
             /*   $cmd .=" AND status_code IN({$status_codes_callable})) AS T
                ON T.col2=da.phone_no AND da.batch_no={$batch_no}";*/
            }else{
                $cmd .="  AND da.batch_no={$batch_no}";
            }
            $cmd .=" AND da.id>={$from} AND da.id<={$to}";
       
            $res_out = $this->db->fetchAllTOFile($cmd, $fp);  
            $for_insert  ="";
            $for_insert .= "{SELECT}";
            $for_insert .= " FROM po_transaction_data_2_{$po_id} da
            WHERE da.phone_no not in (
            SELECT  col2 FROM po_transaction_data_ans_2_{$po_id}
            WHERE  crid_permanent>={$from} AND  crid_permanent<={$to}";
            if ($status_codes_callable !== "")
            {
               /* $for_insert .=" AND status_code IN({$status_codes_callable})) AS T
                ON T.col2=da.phone_no AND da.batch_no={$batch_no}";*/
                 $for_insert .=" AND status_code IN({$status_codes_callable})) AND da.batch_no={$batch_no}";
            }else{
                $for_insert .=" AND da.batch_no={$batch_no}";
            }
            $for_insert .=" AND da.id>={$from} AND da.id<={$to}";
            
            $this->db->query("UPDATE center_calling_list set assign_record_cnt = assign_record_cnt-{$res_out} , is_reassign = 1 WHERE record_cnt_from = {$from} AND record_cnt_to={$to} and po_id ={$po_id} AND  batch_no = {$batch_no}");
            $this->db->query("UPDATE po_parameter set universe = universe - {$res_out} WHERE po_id={$po_id}");
            $res = $this->db->fetchReassignToFile("", "", $phone_field, $po_id, $for_insert, $batch_no, $from, $to);
              
        }
    

}


        
    private function get_first_name_field($po_id)
    {       
        $cmd ="SELECT fname_field FROM po_transaction_data_2_column WHERE po_id={$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['fname_field']))
        {
            return '';
        }                                     
        
        return  $result['fname_field'];
    }

    
    public function get_action_logs($action_module,$id)
    {
         if($id>0){
            
             $cmd="select * from action_logs  where action_module ='{$action_module}' and  remark='{$id}' order by id desc limit 5";
        }else{
            $cmd="select * from action_logs order by id desc" ;
        }
        
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    }
    
      public function check_script_if_exist($id)
    {
        //$cmd="select po_id from po_transaction_data_ans_2 where po_id={$id} limit 1";
        $cmd="select po_id from po_script_question where po_id={$id}";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    
    
     public function send_mail_notification($body,$email_address,$subject='')
    {   date_default_timezone_set('America/Toronto');
        
        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
           $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
       // $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');      
        $mail->From = 'noreply@pbs-server.com';
        $mail->FromName = 'PBS Admin';
       
        
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             $mail->addAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
            
        
        if(!$mail->Send()) {
            return false;// $this->failed("Mailer Error: " . $mail->ErrorInfo);
        } else {
            return true;// $this->success("Message sent!");
        } 
        
        
    }
    
     public function send_mail($body,$email_address,$path,$filename='',$subject='')
    {   date_default_timezone_set('America/Toronto');

        $web_root = dirname(__FILE__)."/";
        //set_time_limit(3600);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        //$mail->Timeout = 3600;                               // 1 = errors and messages
           $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                       // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
       // $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = HOST;      // sets GMAIL as the SMTP server
        $mail->Port       = PORT;                   // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_USERNAME;  // GMAIL username
        $mail->Password   = EMAIL_PASSWORD;   
        //$mail->SetFrom('pbsadmin@pbs-server.com', 'pbsadmin@pbs-server.com');      
        $mail->From = 'noreply@pbs-server.com';
        $mail->FromName = 'PBS Admin';
        if($filename!=''){
            $mail->addAttachment($path.$filename,$filename);    
        }
        
        //$mail->addAddress($email_address);
        $addr = explode(',',trim($email_address));
        foreach ($addr as $ad) {
             $mail->addAddress($ad);      
        }
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject =$subject;
        $mail->Body    =  $body;  
        
            
        
        if(!$mail->Send()) {
            return false;// $this->failed("Mailer Error: " . $mail->ErrorInfo);
        } else {
            unlink($path.$filename);
            return true;// $this->success("Message sent!");
        } 
        
        
    }
    
    
    public function get_poid($po_number)
    {
        $cmd = "SELECT id FROM po WHERE po_no = '{$po_number}'";
        
        $rs = $this->db->fetchRow($cmd);
        
        if(isset($rs['id']))
        {
            return $rs['id'];
        }else{
            return -1;
        }
    }
    
    
    public function get_question_results_amount($po_id, $status_codes, $questions, $start_date, $end_date, $filter_status="")
    {
        
        $status_code_column = $this->get_result_column_position($po_id);   
        
        $question_column = $this->get_result_column_position($po_id, "Q1");
        
        
        if($question_column  != "")
        {
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
        }else
        {
            $column_q ="";
        }
        $complete_status ="";
        $complete_status_for_calldate = "";
        $case_query ="";
        
        
        foreach($status_codes as $s)
        {
            $status_type = $s['status_type'];
//            if(strtolower($status_type) =="complete")
//            {
                $complete_status.="'{$s['status_code']}',"; 
//            }

           if(strtolower($status_type) =="complete")
           {
                $complete_status_for_calldate.="'{$s['status_code']}',";
           }
        }
        
     
        $complete_status = substr($complete_status,0, strlen($complete_status)-1);
        $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
         
        $select_case ="";
        $select_case_total ="";
        
        $responses ="";
        
         $is_gotv = true;
        foreach($questions  as $q)
        {
            $q_no = $q['question_no'];
//            $select_case .= "SUM(CASE col{$column_q} ";
//            $responses .= "CASE col{$column_q} ";
            
//            $select_case_total .= "SUM(CASE col{$column_q} ";
//            foreach($q['responses'] as $r)
//            {
//                $resp_no =  $r['row_no'];
//                $response =  $r['response'];
//                $rate =  $r['rate'];
//                $select_case .= " WHEN {$resp_no} THEN {$rate} ";
//                $select_case_total .= " WHEN {$resp_no} THEN {$rate} ";
//                $responses .= " WHEN {$resp_no} THEN '{$response}' ";
//                $responses .="ELSE '' END  AS Q{$q_no}_{$resp_no},";
//            }
//            $select_case .="ELSE 0 END ) AS Q{$q_no},";
//            $select_case_total .="ELSE 0 END ) +";
                
                
                $q_no = $q['question_no'];
                foreach($q['responses'] as $r)
                {
                    $resp_no =  $r['row_no'];
                    $rate =  $r['rate'];
                    $select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN {$rate} ELSE 0 END) AS Q{$q_no}{$resp_no},";                   
                    
                    $select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN 1 ELSE 0 END) AS QQty{$q_no}{$resp_no},";                   
                    
                    
                    
                    $select_case_total .= "SUM(CASE col{$column_q} ";
                    $select_case_total .= " WHEN {$resp_no} THEN {$rate} ELSE 0 END ) +";
                }
                $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
                $select_case_total .=" AS Q{$q_no},";
                //$select_case .= " SUM(CASE col{$column_q} WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
                $column_q++;
                $is_gotv = false;
        }
        
/*        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $responses = substr($responses, 0, strlen($responses)-1);*/
        
        
		
        if($is_gotv)
        {
            foreach($status_codes as $s)
            {
                $select_case .= " SUM(CASE status_code WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ELSE 0 END) AS Q{$s['status_code']},";                   

                $select_case .= " SUM(CASE status_code WHEN '{$s['status_code']}' THEN 1 ELSE 0 END) AS QQty{$s['status_code']},";   
                
                $select_case_total .= "SUM(CASE status_code ";
                $select_case_total .= " WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ELSE 0 END ) +";
                           
            }
			$select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
			$select_case_total .=" AS TotalAmount ";
        }
        
/*        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $responses = substr($responses, 0, strlen($responses)-1);*/
        $select_case  = substr($select_case, 0, strlen($select_case)-1);
        $select_case_total  = substr($select_case_total, 0, strlen($select_case_total)-1);
        
        //$cmd =" SELECT {$select_case}, {$select_case_total} as TotalAmount, {$responses} FROM po_transaction_data_ans_2 WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
        
        //$cmd =" SELECT {$select_case}, {$select_case_total} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
        $cmd =" SELECT {$select_case}, {$select_case_total} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND status_code IN({$complete_status})";

        $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}'";
        
        
//        if($filter_status <> '')
//        {
//            $cmd .= " AND {$status_code_column} = '{$filter_status}'";
//        }

        // print_r($cmd);
        // die();
        
		/*print_r($this->db->fetchRow($cmd));
		die();*/
        return $this->db->fetchRow($cmd);
    }
    
    
    
    //billing report
    public function get_billing_report_summary($start_date, $end_date, $client_id)
    {
        
        //for testing
        if($client_id =="")
        {
            //load for testing
            $client_id =9 ;
            $star_date = "2016-02-09";
            $end_date = "2016-02-09";
        }
        
        //end for testing
        //get_script_question_response gather all responses and rate for case condition
        $cmd ="SELECT pp.*,p.po_no FROM po p
        LEFT JOIN po_parameter pp ON pp.po_id=p.id
        WHERE p.status<>'D' and p.client_id={$client_id}";
       
        //$cmd .=" AND p.id IN(2,3,4,5,6,7,9,10,11)";

     
        //WHERE  p.client_id={$client_id} AND p.status IN('A', 'C')"; //active and completed only
        //start_date>='{$start_date}' AND end_date<='{$end_date}'";
    
        $rs = $this->db->fetchAll($cmd);
  
             
          
        $total_amount = 0;
        $biller_summary_records = array();
         
        $cmd ="";

       foreach($rs as $r)
        {   
            
            $po_start_date = $r['start_date'];
            $po_end_date = $r['end_date'];
            
            
            $po_no = $r['po_no'];
            $po_id = $r['po_id'];
            $question_column = $this->get_result_column_position($po_id, "Q1");
           
             
            if($question_column =="")
            {  
                //goto nextPO;
                goto GOTV; //without question
            }
             
             
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
   
               
             
            $status_codes = $this->get_status_codes($po_id);
            
            $status_code_column = $this->get_result_column_position($po_id);
             
                  
            $complete_status ="";
            $case_query ="";
            foreach($status_codes as $s)
            {
                $status_type = $s['status_type'];                    
                if(strtolower($status_type) =="complete")
                {
                    $complete_status.="'{$s['status_code']}',";
                }
            }   
            if($complete_status !="")
            {
                $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
            }
            
            $questions = $this->get_script_question_response($po_id);
             
            $q_no =1;
            $select_case = "";
            foreach($questions as $q)
            {
                $q_no = $q['question_no'];
                $select_case .= "(CASE col{$column_q} ";
                foreach($q['responses'] as $r)
                {
                    $resp_no =  $r['row_no'];
                    if($r['rate']==""){
                        $rate = 0;
                    }else{
                        $rate =  $r['rate'];    
                    }
                    
                    $select_case .= " WHEN {$resp_no} THEN {$rate} ";
                }
                $select_case .="ELSE 0 END ) +";
                $column_q++;            
            }
            
            $select_case = substr($select_case, 0, strlen($select_case)-1);

            
             
            $cmd ="SELECT p.po_no, p.name,c.name as client_name, '{$po_start_date}' as po_start_date, '{$po_end_date}' as po_end_date,  SUM({$select_case}) AS total_amount";
            $cmd .=" FROM po p
                LEFT JOIN po_transaction_data_ans_2_{$po_id} a on p.id=a.po_id
                LEFT JOIN client c ON c.id=p.client_id
                WHERE str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}' AND p.client_id={$client_id}";
                

             
            
             
            $record = $this->db->fetchRow($cmd); 
            $total_amount = $record['total_amount'];
            $biller_summary_records['records'][] = $record;
            goto nextPO;
            
            GOTV:
            $status_codes = $this->get_status_codes($po_id);
            $complete_status ="";
            $case_query ="";
            $select_case ="";
            foreach($status_codes as $s)
            {
                 
                $status_type = $s['status_type'];                    
                if(strtolower($status_type) =="complete")
                {
                    $complete_status.="'{$s['status_code']}',";
                }
                
                $select_case .= "(CASE status_code ";
                $select_case .= " WHEN '{$s['status_code']}' THEN {$s['pay_rate']} ";
                $select_case .="ELSE 0 END ) +";
            }   
            if($complete_status !="")
            {
                $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
            }
            
            $select_case = substr($select_case, 0, strlen($select_case)-1);
            
            $cmd ="SELECT p.po_no, p.name,c.name as client_name, '{$po_start_date}' as po_start_date, '{$po_end_date}' as po_end_date,  SUM({$select_case}) AS total_amount";
            $cmd .=" FROM po p
                LEFT JOIN po_transaction_data_ans_2_{$po_id} a on p.id=a.po_id
                LEFT JOIN client c ON c.id=p.client_id
                WHERE str_to_date(call_date,'%m/%d/%Y')>='{$start_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$end_date}' AND p.client_id={$client_id}";
                

          
          
           
            $record = $this->db->fetchRow($cmd); 
            $total_amount = $record['total_amount'];
            $biller_summary_records['records'][] = $record;
            
              
            nextPO:
        }
        
        $biller_summary_records['summary_amount'] = $total_amount;
            
        return $biller_summary_records;
    }
    //end billing report
    
    //--Start of Report
      public function get_summary_report_data($po_id, $call_date, $status, $center_id, $filter_status="")
    {
        $status_codes = $this->get_status_codes($po_id);
        $status_code_column = $this->get_result_column_position($po_id);
        
        $complete_status ="";
        $case_query ="";
        $callable_status ="";
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['status_code']))
        {
        
                foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
                    
                    if(strtolower($status_type) =="callable")
                    {
                        $callable_status.="'{$s['status_code']}',";
                    }
                    
                    
                    if(strtolower($status_type) =="complete")
                    {
                        $complete_status.="'{$s['status_code']}',";
                    }else{
                        $status_code = $s['status_code'];
                        //$case_query .=" SUM(CASE WHEN {$status_code_column} IN('{$status_code}') THEN 1 ELSE 0 END) AS {$status_code},";
						$case_query .=" SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN('{$status_code}') THEN 1 ELSE 0 END) AS {$status_code},";
                    }
                }   
                if($complete_status !="")
                {
                    $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
                    //$case_query .= " SUM(CASE WHEN {$status_code_column} IN({$complete_status}) THEN 1 ELSE 0 END) AS complete,";
					$case_query .= " SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN({$complete_status}) THEN 1 ELSE 0 END) AS complete,";
                }
                if($callable_status !=="")
                {
                    $callable_status = substr($callable_status, 0, strlen($callable_status)-1);
                    //$case_query .= " SUM(CASE WHEN trim({$status_code_column}) IN({$callable_status}) THEN 0 ELSE 1 END) AS penetration,";
					$case_query .= " SUM(CASE WHEN ".'replace(trim(status_code),"\r","")'." IN({$callable_status}) THEN 0 ELSE 1 END) AS penetration,";
                }else{
                    $case_query .= " SUM(1) AS penetration,";
                }
                
                $case_query = substr($case_query, 0, strlen($case_query)-1);
                $case_query = " SELECT {$case_query}";
                
                $cmd = "{$case_query} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id}";
                
                if($call_date =="default")
                {
                    $call_date = $this->get_min_call_date($po_id,$complete_status,$status_code_column);
                    $call_date = $call_date['call_date'];
                    
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    //$date_to = date_add($date, $diff1Day);
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$call_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$call_date}'";
                }
                
                if($filter_status <> '')
                {
                    $cmd .= " AND {$status_code_column} = '{$filter_status}'";
                }
                
                
                
                if($center_id <> '')
                {
                    $q ="SELECT min(record_cnt_from) as start_id, max(record_cnt_to) as end_id  
        FROM center_calling_list WHERE po_id={$po_id} AND center_id={$center_id}";
                    $ids = $this->db->fetchRow($q);
                    
                    $cmd .= " AND crid_permanent >= {$ids['start_id']} AND crid_permanent<={$ids['end_id']}";
                }
                
                
                
        }else{
                
                 foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
                    
                    if(strtolower($status_type) =="callable")
                    {
                        $callable_status.="'{$s['status_code']}',";
                    }
                    
                    
                    if(strtolower($status_type) =="complete")
                    {
                        $complete_status.="'{$s['status_code']}',";
                    }else{
                        $status_code = $s['status_code'];
                        $case_query .=" SUM(CASE WHEN status_code IN('{$status_code}') THEN no_of_count ELSE 0 END) AS {$status_code},";
                    }
                }   
                if($complete_status !="")
                {
                    $complete_status = substr($complete_status, 0, strlen($complete_status)-1);
                    $case_query .= " SUM(CASE WHEN status_code IN({$complete_status}) THEN no_of_count ELSE 0 END) AS complete,";
                }
                if($callable_status !=="")
                {
                    $callable_status = substr($callable_status, 0, strlen($callable_status)-1);
                    $case_query .= " SUM(CASE WHEN status_code IN({$callable_status}) THEN 0 ELSE no_of_count END) AS penetration,";
                }else{
                    $case_query .= " SUM(1) AS penetration,";
                }
                
                $case_query = substr($case_query, 0, strlen($case_query)-1);
                $case_query = " SELECT {$case_query}";
                
                $cmd = "{$case_query} FROM po_manual_load WHERE po_id={$po_id}";
                
                if($call_date =="default")
                {
                    $call_date = $this->get_min_call_date_manual($po_id,$complete_status);
                    $call_date = $call_date['call_date'];
                    
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    //$date_to = date_add($date, $diff1Day);
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND call_date>='{$date}' AND call_date<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND call_date>='{$call_date}' AND call_date<='{$call_date}'";
                }
                
                if($center_id <> '')
                {
                    $cmd .= " AND center_id={$center_id}";
                }
                
                if($filter_status <> '')
                {
                    $cmd .= " AND status_code = '{$filter_status}'";
                }
                
                $cmd .=" GROUP BY question";
            
            
        }
//        print_r($cmd);
//        die();

        return $this->db->fetchRow($cmd);
    }

    
public function get_question_results($po_id, $status_codes, $questions, $call_date, $center_id, $filter_status="")
    {
       
        
        $complete_status ="";
        $complete_status_for_calldate = "";
        $case_query ="";
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        

        if(!isset($result['status_code']))
        {
            $status_code_column = $this->get_result_column_position($po_id);   
            $question_column = $this->get_result_column_position($po_id, "Q1");
            $column_q = explode("col",$question_column);
            $column_q = $column_q[1];
                
                foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
        //            if(strtolower($status_type) =="complete")
        //            {
                        $complete_status.="'{$s['status_code']}',"; 
        //            }

                   if(strtolower($status_type) =="complete")
                   {
                        $complete_status_for_calldate.="'{$s['status_code']}',";
                   }
                }
                $complete_status = substr($complete_status,0, strlen($complete_status)-1);
                $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
                
                $select_case ="";
                
                
                foreach($questions  as $q)
                {
                    $q_no = $q['question_no'];
                    foreach($q['responses'] as $r)
                    {
                        $resp_no =  $r['row_no'];
                        //$select_case .= " SUM(CASE col{$column_q} WHEN {$resp_no} THEN 1 ELSE 0 END) AS Q{$q_no}{$resp_no},";
						
						$select_case .= " SUM(CASE ".'replace(col'."{$column_q}".',"\r","")'." WHEN {$resp_no} THEN 1 ELSE 0 END) AS Q{$q_no}{$resp_no},";
                    }
                    //$select_case .= " SUM(CASE col{$column_q} WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
					$select_case .= " SUM(CASE ".'replace(col'."{$column_q}".',"\r","")'." WHEN 0 THEN 0 WHEN '' THEN 0 ELSE 1 END) AS QTotal{$q_no},";
                    $column_q++;
                }
                
                $select_case  = substr($select_case, 0, strlen($select_case)-1);
                
                $cmd =" SELECT {$select_case} FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} IN({$complete_status})";
                
                if($call_date =="default" )
                {
                    $call_date = $this->get_min_call_date($po_id,$complete_status_for_calldate,$status_code_column);
                    
                    $call_date = $call_date['call_date'];
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND str_to_date(call_date,'%m/%d/%Y')>='{$call_date}' AND str_to_date(call_date,'%m/%d/%Y')<='{$call_date}'";
                }
                
                if($center_id <> '')
                {
                    $q ="SELECT min(record_cnt_from) as start_id, max(record_cnt_to) as end_id  
        FROM center_calling_list WHERE po_id={$po_id} AND center_id={$center_id}";
                    $ids = $this->db->fetchRow($q);
                    
                    $cmd .= " AND crid_permanent >= {$ids['start_id']} AND crid_permanent<={$ids['end_id']}";
                }
                if($filter_status <> '')
                {
                    $cmd .= " AND {$status_code_column} = '{$filter_status}'";
                }
    }else{
        
             foreach($status_codes as $s)
                {
                    $status_type = $s['status_type'];
        //            if(strtolower($status_type) =="complete")
        //            {
                        $complete_status.="'{$s['status_code']}',"; 
        //            }

                   if(strtolower($status_type) =="complete")
                   {
                        $complete_status_for_calldate.="'{$s['status_code']}',";
                   }
                }
                $complete_status = substr($complete_status,0, strlen($complete_status)-1);
                $complete_status_for_calldate = substr($complete_status_for_calldate,0, strlen($complete_status_for_calldate)-1);
                
                $select_case ="";
                
                
                foreach($questions  as $q)
                {
                    $q_no = $q['question_no'];
                    foreach($q['responses'] as $r)
                    {
                        $resp_no =  $r['row_no'];
                        $select_case .= " SUM(
							CASE question WHEN {$q_no} THEN
								CASE response WHEN {$resp_no} THEN no_of_count ELSE 0 END
							ELSE 
								0 
							END
						) AS Q{$q_no}{$resp_no},";
                    }
                    $select_case .= " SUM(
						CASE question WHEN {$q_no} THEN
							CASE response WHEN 0 THEN 0 WHEN '' THEN 0 ELSE no_of_count END
						ELSE 
							0 
						END
						) AS QTotal{$q_no},";
                    //$column_q++;
                }
                
                $select_case  = substr($select_case, 0, strlen($select_case)-1);
                
                $cmd =" SELECT {$select_case} FROM po_manual_load WHERE po_id={$po_id} AND status_code IN({$complete_status})";
                
                if($call_date =="default" )
                {
                    $call_date = $this->get_min_call_date_manual($po_id,$complete_status_for_calldate);
                    
                    $call_date = $call_date['call_date'];
                    $date = date_create($call_date);
                    $diff1Day = new DateInterval('P1D');
                    
                    $date = date_format($date, 'Y-m-d');
                    $cmd .= " AND call_date>='{$date}' AND call_date<='{$date}'";
                }elseif($call_date !='')
                {
                    $cmd .= " AND call_date>='{$call_date}' AND call_date<='{$call_date}'";
                }
                
               
                if($filter_status <> '')
                {
                    $cmd .= " AND status_code = '{$filter_status}'";
                }
                if($center_id <> '')
                {
                    $cmd .= " AND center_id={$center_id}";
                }
				
				// if($call_date !='')
				// {
					// print_r($cmd);
					// die();
				// }
                
                // $cmd .=" GROUP BY question";
                

              
        }
				 // if($call_date !='')
				 // {
					 // print_r($cmd);
					 // //print_r($this->db->fetchRow($cmd));
					 // die();
				 // }
	  
        return $this->db->fetchRow($cmd);
    }


  
        
    public function get_script_question_response($po_id)
    {
        $cmd = "SELECT * FROM po_script_question WHERE po_id={$po_id} AND sequence>0 
AND sequence <= (SELECT number_of_question FROM po_parameter WHERE po_id={$po_id})   ORDER BY sequence";
        $questions = $this->db->fetchAll($cmd);
        $q_response = array();
        foreach($questions as $q)
        {
            $cmd = "SELECT row_no, response, rate  FROM po_script_q_response
WHERE po_script_question_id={$q['id']} ORDER BY row_no";
            $responses = $this->db->fetchAll($cmd);
            
            $q_response[] = array(
                'question_no' => $q['sequence'],
                'question' => $q['question'],
                'responses' => $responses,
                'rate' => $q['rate']
            );
        }
        return $q_response;
    }

    public function get_universe_start_date($po_id)
    {
        $cmd ="SELECT start_date, universe FROM po_parameter WHERE po_id={$po_id}";
        return $this->db->fetchRow($cmd);
    }
    
    public function get_result_dates($po_id)
    {
        
        $complete_status="";
        //use the actual status
		//$status_code_column = $this->get_result_column_position($po_id);     
		
        $status_codes = $this->get_status_codes($po_id);
         foreach($status_codes as $s)
         {
            $status_type = $s['status_type'];
            if(strtolower($status_type) =="complete")
            {
                $complete_status.="'{$s['status_code']}',";
            }
         } 
        
        $complete_status  = substr($complete_status, 0, strlen($complete_status)-1);
        $cmd ="SELECT po_id FROM po_manual_load WHERE po_id= {$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
        if(isset($result['po_id']))
        {
            $cmd = "SELECT DISTINCT call_date FROM po_manual_load WHERE status_code in({$complete_status}) and  po_id={$po_id} ORDER BY call_date";
            return $this->db->fetchAll($cmd);
        }
         
        //$cmd = "SELECT DISTINCT call_date FROM po_transaction_data_ans_2_{$po_id} WHERE {$status_code_column} in({$complete_status}) AND po_id={$po_id} ORDER BY str_to_date(call_date,'%m/%d/%Y')";
		$cmd = "SELECT DISTINCT call_date FROM po_transaction_data_ans_2_{$po_id} WHERE " . 'replace(status_code,"\r","")'." in({$complete_status}) AND po_id={$po_id} ORDER BY str_to_date(call_date,'%m/%d/%Y')";
		

        return $this->db->fetchAll($cmd);     
        
           
    }
    
    public function get_result_centers($po_id)
    {
        $cmd ="SELECT c.center_id, cc.name as center_name FROM center_calling_list c LEFT JOIN call_center cc ON cc.id=c.center_id  WHERE po_id={$po_id}";
        return $this->db->fetchAll($cmd);
    }
    
    private function get_result_column_position($po_id, $column_to_search ="status_code")
    {
        
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$po_id}  LIMIT 1";
          
        $result = $this->db->fetchRow($cmd);
        
        if(isset($result['status_code']))
        {
           
            return '';
            //$cmd = "SELECT DISTINCT status_code FROM po_manual_load WHERE po_id={$po_id}";
            //return $this->db->fetchAll($cmd);
        }
            
        $cmd ="SELECT remarks FROM po_transaction_data_ans_2_column WHERE po_id={$po_id} LIMIT 1";
        
        $result = $this->db->fetchRow($cmd);
        
        if(!isset($result['remarks']))
        {
            return '';
        }

          
        
        $columns = explode(";", $result['remarks']);
        foreach($columns as $c)
        {
            $column_name = explode("|", $c);

            $col_name = $column_name[1];
            if($col_name == $column_to_search)
            {
                $col_name = $column_name[0];
                return $col_name;
                break;
            } 
        }
    }
    
    public function get_status_codes($po_id, $remove_complete = 'no')
    {
        $cmd =" SELECT pt.status_code, sc.name as status_type,pt.pay_rate FROM po p 
        LEFT JOIN project_template_code pt on pt.project_template_id=p.project_template_id
        LEFT JOIN status_code_type sc on sc.id=pt.type
        WHERE p.id={$po_id} ";
        if($remove_complete =="yes")
        {
            $cmd .= " AND sc.name <> 'Complete'";
        }
        $cmd .=" ORDER BY status_type";
        
        return $this->db->fetchAll($cmd);
    }
    
    private function get_min_call_date($po_id,$complete_status_for_calldate,$status_code_column)
    {
        $cmd = "SELECT min(str_to_date(call_date,'%m/%d/%Y')) as call_date FROM po_transaction_data_ans_2_{$po_id} WHERE po_id={$po_id} AND {$status_code_column} in({$complete_status_for_calldate})";
        return $this->db->fetchRow($cmd);
    }
    private function get_min_call_date_manual($po_id,$complete_status_for_calldate)
    {
        $cmd = "SELECT min(str_to_date(call_date,'%m/%d/%Y')) as call_date FROM po_manual_load WHERE po_id={$po_id} AND status_code in({$complete_status_for_calldate})";
        return $this->db->fetchRow($cmd);
    }
    
    //--End of Report Script
    
    public function get_answer_call_date($po_id,$center_id){
            $call_date_list=$this->db->fetchAll("SELECT DISTINCT pda.call_date FROM po_transaction_data_ans_2_{$po_id} pda
            INNER JOIN center_calling_list ccl on(pda.po_id=ccl.po_id)
            WHERE ccl.po_id={$po_id} AND ccl.center_id ={$center_id} ");
            return  $call_date_list;
    }
    public function reassign_apply_data_center_specific($params, $batch_no, $po_id, $calling_list_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
            
            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];
           
           $cmd ="SELECT record_cnt_from, record_cnt_to FROM center_calling_list WHERE po_id =".$po_id." and batch_no = ".$batch_no." AND id={$calling_list_id}";
          
           $between_id = $this->db->fetchRow($cmd);
           
           $min_id = $between_id['record_cnt_from'];
           $max_rec_id = $between_id['record_cnt_to'];
           

           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;
           
           
           
           foreach($params as $row)
           {
               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load

                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 ORDER BY po_id,id LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND id>={$min_id} AND id<={$max_rec_id} AND is_column=0 ORDER BY po_id,id  LIMIT {$limit} ) AS t";
               }
               
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               $row['assign_record_cnt'] = $limit;
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }
           
           $this->db->query("DELETE FROM center_calling_list WHERE po_id={$po_id} AND batch_no={$batch_no} AND id={$calling_list_id}");
           
           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    
    
    
    
    public function drop_result_per_center_calldate($id)
   {    
        $center_calling= $this->db->fetchRow("SELECT  MIN(record_cnt_from) ,MAX(record_cnt_to)  FROM center_calling_list  ccl
                                             LEFT JOIN call_center cc on ccl.center_id = cc.id
                                             where po_id ={$param['po_id']} AND ccl.center_id={$param['center_id']}");
        
    
       if (! $this->db->delete('po_transaction_data_ans_2'."_{$param['po_id']}", 'po_id=' .  $param['po_id'] . ' AND crid_permanent>= '. $param['po_id'].' AND crid_permanent<='.$param['po_id'])) {
                                return $this->failed("Unable to drop result.");
             }
             $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$param['po_id']}");
             return $this->success("Result has been dropped"); 
   }
    
    public function get_dedup_from_previous_file($batch_no, $po_id)
    {
        $cmd="select status from  po_transaction_data_2_column
        where po_id={$po_id} and batch_no={$batch_no}";
        $duplicate_nos_from_prev = $this->db->fetchRow($cmd);
        return $duplicate_nos_from_prev['status'];
    }
    
    public function reassign_apply_data_center($params, $batch_no, $po_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";

            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];

           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;
           
           
           
           foreach($params as $row)
           {
               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load

                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ORDER BY po_id,id LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ORDER BY po_id,id  LIMIT {$limit} ) AS t";
               }
               
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               $row['assign_record_cnt'] = $limit;
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }
           
           $this->db->query("DELETE FROM center_calling_list WHERE po_id={$po_id} AND batch_no={$batch_no}");
           
           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    public function get_center_for_upload_result($po_id)
    {
        $cmd = "SELECT DISTINCT cc.id,cc.name FROM center_calling_list  ccl
        LEFT JOIN call_center cc on ccl.center_id = cc.id
        where po_id ={$po_id}"  ;
        return $this->db->fetchPairs($cmd);    
    }
    
    public function get_upload_batch_list($po_id)
    {
        // $cmd = "SELECT DISTINCT ccl.po_id,ccl.batch_no, SUM((record_cnt_to+1)-record_cnt_from) as records, pdt.create_date  
        // FROM center_calling_list ccl
        // INNER JOIN po_transaction_data_2_column pdt
        // ON pdt.po_id = ccl.po_id AND ccl.batch_no=pdt.batch_no 
        // WHERE ccl.po_id = {$po_id}
        // GROUP BY ccl.po_id,ccl.batch_no";  
        
        $cmd = "SELECT DISTINCT pdt.po_id,pdt.batch_no, 0, pdt.create_date 
        FROM  po_transaction_data_2_column pdt
        WHERE pdt.po_id = {$po_id}
        GROUP BY pdt.po_id,pdt.batch_no";  
        
        return $this->db->fetchAll($cmd);
        
    }
    
    
    public function get_uploaded_summary_count($batch_no, $po_id)
    {
        $cmd="SELECT * FROM po_transaction_data_2_summary WHERE batch_no={$batch_no} AND po_id={$po_id}";

        return $this->db->fetchRow($cmd);
    }
    
    
    //process_option_id, po_id, batch_no, householded_count,
    //badphone_count,total_count,final_count,dedup_count
    public function insert_summary($params)
    {
       $this->db->insert('po_transaction_data_2_summary', $params);
    }
    
    public function apply_data_center_2($params, $batch_no, $po_id){
        
          //  $cmd ="SELECT process_option_id FROM po_transaction_data_2 WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";

            $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$po_id." and center_id is null and batch_no = ".$batch_no." AND is_column=1 LIMIT 1";
          
           $process_id = $this->db->fetchRow($cmd);
           $process_id = $process_id['process_option_id'];
           

           
           //$result = $this->db->fetchAll($cmd);

           $counter=1;
           $ctr_index=0;
           
           $max_id=0;   
           
           foreach($params as $row)
           {

               $list_cnt = $row['list_cnt'];
               $po_id =$row['po_id']; 
               $batch_no =$row['batch_no'];
               $center_id = $row['center_id'];
               
               $limit = $row['record_cnt_to'];
               if($process_id==1){ //Non-Household List AS-IS
                    $cmd = " SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
               }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
//                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";

               }elseif($process_id==3 ){ //Load then Household list               
//                    $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                    //$cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";

//                    $cmd = "  SELECT DISTINCT phone_no, min(id) min_id, max(id) as max_id FROM  (select id,phone_no from po_transaction_data_2 where   po_id =". $po_id;
//                    $cmd .=" and batch_no=".$batch_no;
//                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 ) AS t  LIMIT {$limit}";
                    $cmd = "  SELECT min(id) min_id, max(id) as max_id FROM (select id from po_transaction_data_2_calling_list_{$po_id} where   po_id =". $po_id;
                    $cmd .=" and batch_no=".$batch_no;
                    $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND id>{$max_id} AND is_column=0 LIMIT {$limit} ) AS t";


               }
               $data = $this->db->fetchRow($cmd);
               
               $row['record_cnt_from'] = $data['min_id'];
               $row['record_cnt_to'] = $data['max_id'];
               
               //new field
                if($process_id==1){
                    $row['assign_record_cnt'] = $limit-1;
                }else{
                    $row['assign_record_cnt'] = $limit;
                }
               $max_id = $data['max_id'];
               $center_calling_list_insert[] =  $row;
                
           }

           if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
                return $this->failed("Unable to insert center calling list data");
            } 
            else {
                $value=array(
                    'id'    =>$po_id,
                    'status'=> 'A',
                    );
                $this->change_po_status($value);
                   
                    $response = array(
                        'message' => "Center data has been populated.",
                    );                                                              
                return $this->success($response);
            }         
    }
    
    
    
        public function get_client_code($id)
    {
        $cmd="select client_code from client where id={$id}";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['client_code']))
        {
            return $result['client_code'];    
        }else{
            return "";   
        }
    }
       public function get_po_counter($client_id)
    {
        $cmd="SELECT value FROM po_counter WHERE client_id =".$client_id;
        $result = $this->db->fetchRow($cmd); 
         if(is_array($result)){
             return $result['value'];
         }else{
            
                $insert_data=array(
                    'value' => 2,
                    'client_id'  =>$client_id, 
                );
                $this->db->insert('po_counter', $insert_data);
                return 1;
         }
        
    }
    public function update_po_counter($params)
     {
          $val = ($params['value'] + 1);
          $update_data = array(
            'value' => $val,
          );
         if(!$this->db->update('po_counter', $update_data,'client_id=' . $params['client_id']))   
         {return false;} 
          else 
         {return true;}              
    }
    
        public function get_po_number($id){
          $sql="select po_no from po p where id = ".$id;
          $result = $this->db->fetchRow($sql);
          return $result['po_no']; 
  
    }
 
     //this function will verify if previous batch has not been assigned of a center
     // if this function has result it means that previous batch had assign a center
    public function has_existing_batch_without_applied_center($batch_no, $po_id)
    {
        
        
        $cmd= "SELECT id from center_calling_list where batch_no={$batch_no}
AND po_id={$po_id} LIMIT 1";
        $result = $this->db->fetchRow($cmd);
                                            
        if(!($result['id']==""))
        {
            return 0;    
        }else{
            return 1;
//            $cmd="SELECT id from po_transaction_data_2 where  batch_no={$batch_no}
//AND po_id={$po_id} LIMIT 1";

//            $result = $this->db->fetchRow($cmd);
//              if(isset($result['id']))
//              {
//                return 1;    
//              }else{
//                return 0;
//            }
        }
    }
    //batch_no, po_id
    public function get_existing_process_option_id_remarks($batch_no, $po_id)
    {
        $cmd = "SELECT process_option_id, remarks, misc_fields, lname_field, fname_field, phone_field from po_transaction_data_2_column where batch_no={$batch_no}
AND po_id={$po_id} AND is_column=1 LIMIT 1";

         $result = $this->db->fetchRow($cmd);
         return $result;
    }
    
    public function get_total_count($batch_no, $po_id)
    {
        $cmd = "SELECT count(id) as cnt from po_transaction_data_2_{$po_id} where batch_no={$batch_no}
AND po_id={$po_id} AND is_column=0";
         $result = $this->db->fetchRow($cmd);
         return $result['cnt'];
    }
    
 


   public function drop_result($params)
   {    
       
       if($params['center_id']==0){ 
         
            /*   if (! $this->db->delete('po_transaction_data_ans_2'."_{$params['id']}", 'po_id=' .  $params['id'])) {
                                        return $this->failed("Unable to drop result.");
                     } */
            $cmd_delete= "TRUNCATE TABLE po_transaction_data_ans_2_{$params['id']}";
            $this->db->query($cmd_delete);       
            $this->db->delete('po_transaction_data_ans_2_column', 'po_id=' .  $params['id']);
            $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$params['id']}");   
        /*            $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id);
                    $this->db->delete('po_transaction_data_2_summary','po_id=' .  $id);  */
               return $this->success("Result has been dropped"); 
       }else{
         
            $call_date = new DateTime($params['call_date']);
            $call_date = $call_date->format('d/m/Y');
            $center_calling= $this->db->fetchRow("SELECT  MIN(record_cnt_from) as min ,MAX(record_cnt_to) as max  FROM center_calling_list  ccl
                                             LEFT JOIN call_center cc on ccl.center_id = cc.id
                                             where po_id ={$params['id']} AND ccl.center_id={$params['center_id']}");
             
            if (! $this->db->delete('po_transaction_data_ans_2'."_{$params['id']}", 'po_id=' .  $params['id'] .' AND call_date='. $this->db->db_escape($call_date). ' AND crid_permanent>= '. $center_calling['min'].' AND crid_permanent<='.$center_calling['max'])) {
                                return $this->failed("Unable to drop result.");
             } 
             
             $cmd= "SELECT id from po_transaction_data_ans_2_{$params['id']} where  po_id={$params['id']} LIMIT 1"; 
             $result = $this->db->fetchRow($cmd);
             
            if($result['id']=="")
            {
                $this->db->delete('po_transaction_data_ans_2_column', 'po_id=' . $params['id']);
            }
            $this->db->query("UPDATE po_parameter set complete = 0 WHERE po_id={$params['id']}");
            return $this->success("Result has been dropped"); 
           
           
       }
   }
    
 

    private function validate_response($q_columns, $data, $responses)
    {
             $index = 0;
             while ($index < count($q_columns))
             {
                         
                      
                  $question_reponse = $data[$q_columns[$index]]; //responsse from file
              
                  if(!array_key_exists($question_reponse,$responses[$index]))
                  {
                     
                      return false;
                  }
                          
                  $goto_next_q =  $responses[$index][$question_reponse];   
                  $goto_next_q--;
                    
               
                  if($goto_next_q == $index)   //check if some question is skip because of goto
                  {
                     
                  }else{
                 
                      //loop on previous columns   
                      $loop_cnt_previous = $index+1;
                      while ($loop_cnt_previous < $goto_next_q)
                      {
                          $question_reponse = $data[$q_columns[$loop_cnt_previous]]; //responsse from file
                      
                          if($question_reponse>0){
                             if(!array_key_exists($question_reponse,$responses[$index]))
                             {
                               return false;
                             }
                           }
                          if($question_reponse>0 || $question_reponse==""){ //Check if the previous Question had a value
                               return false;
                          }
                        
                          $loop_cnt_previous++;
                      }
                  }
                  
                 
                  $index = $goto_next_q;
                 
                  
                  //fo
                 
             }
            
             return true;
    }
   
    public function drop_manual_load($id)
    {
             if (! $this->db->delete('po_manual_load', 'po_id=' .  $id)) {
                                return $this->failed("Unable to drop manual load.");
             }    
            return $this->success("Manual load has been dropped");
    }
    private function get_center_id($center_code)
    {
        $cmd="select id from call_center where center_code='{$center_code}'";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['id']))
        {
            return $result['id'];    
        }else{
            return -1;   
        }
    }
    
    private function is_center_assign_on_po($center_id, $po_id)
    {        
        $cmd="select id from center_calling_list where po_id={$po_id} AND center_id={$center_id}";
        
        $result = $this->db->fetchRow($cmd);
        if(isset($result['id']))
        {
            return $result['id'];    
        }else{
            return -1;   
        }
        
    }
    
    public function process_manual_file($file_name, $po_id)
    {
        ini_set('max_execution_time', 0);
        $file = $file_name;
        $handle = fopen($file, "r");
        $line_number = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                
                // process the line read.
                    if(trim($line) !="")
                    {
                        $line_number++;
                        $row = explode(",",$line);
                        if(!(isset($row[0],$row[1],$row[2], $row[3], $row[4], $row[5])))
                        {
                          
                          return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}.");  
                          if($line_number>1)
                            {
                                unlink(md5($po_id.'_'.$center_id).'.txt');        
                            }
                          die();
                        }
                        elseif(is_numeric($row[3] && is_numeric($row[4] && is_numeric($row[5]))))
                        {
                          return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Question or Reponse or No. of Count");  
                          if($line_number>1)
                            {
                                unlink(md5($po_id.'_'.$center_id).'.txt');        
                            }
                          die();  
                        }
                        else{
                            $center_code = $row[0];
                            $call_date = $row[1];
                            $call_date = new DateTime($call_date);
                            
                            $call_date = $call_date->format('Y-m-d');
                            $status_code = $row[2];    
                            $question_no = $row[3];    
                            $response = $row[4];    
                            $no_of_counts = $row[5];    
                            
                            //validate if center code is valid 
                            $center_id = $this->get_center_id($center_code);
                            if($center_id==-1)
                            {
                                return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Invalid center code.".$center_id);  
                                if($line_number>1)
                                {
                                    unlink(md5($po_id.'_'.$center_id).'.txt');        
                                }
                                die();  
                            }
                            //check if center is on the center_calling_list table
                            if($this->is_center_assign_on_po($center_id, $po_id)==-1)
                            {
                                return array("success" => false, "message" => "Unable to process file. Please check line number {$line_number}. Center Code is not assign on this PO.");  
                                if($line_number>1)
                                {
                                    unlink(md5($po_id.'_'.$center_id).'.txt');        
                                }
                                die();  
                            }                   
                            $script = "({$po_id}, {$center_id}, '{$call_date}', '{$status_code}', {$question_no}, {$response}, {$no_of_counts}, '{$_SESSION['username']}'),";
                        }
                        
                        //file_put_contents(md5($po_id.'_'.$center_id).'.txt', $script , FILE_APPEND);                       
                        file_put_contents(md5($po_id).'.txt', $script , FILE_APPEND);                       
                   }
                
                
            }
           
            //$this->db->query("INSERT INTO po_manual_load (po_id, center_id, call_date, status_code, question, response, no_of_count, create_by) VALUES".substr(file_get_contents(md5($po_id).'.txt'),0, strlen(file_get_contents(md5($po_id.'_'.$center_id).'.txt'))-1));
            
            $this->db->query("INSERT INTO po_manual_load (po_id, center_id, call_date, status_code, question, response, no_of_count, create_by) VALUES".substr(file_get_contents(md5($po_id).'.txt'),0, strlen(file_get_contents(md5($po_id).'.txt'))-1));
              
            unlink(md5($po_id).'.txt');
            fclose($handle);
            return array("success" => true, "message" => "File had been processed.", "no_of_records" => $line_number);
        } else {
            // error opening the file.
            print_r('Error Reading');
        } 
        unlink($file);
    }

    
    
    public function update_final_universe($params)
    {
        $params = array_map('trim', $params);
        $id = $params['po_id'];     
        
        $po_universe = $this->db->fetchRow("select universe from po_parameter where po_id={$id}");
        $values = array(
                            'universe'=>$po_universe['universe']+$params['universe'],
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po_parameter', $values, 'po_id='.$id);
        $response = array(
            'message' => "PO final universe has been updated.",
        );
        return $this->success($response); 
    }
    
    
     public function check_data_if_manual_data_exist($id)
    {
        $cmd ="SELECT status_code FROM po_manual_load WHERE po_id={$id}  LIMIT 1";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    public function check_data_if_result_exist($id)
    {
        //$cmd="select po_id from po_transaction_data_ans_2 where po_id={$id} limit 1";
        $cmd="select po_id from po_transaction_data_ans_2_column where po_id={$id} limit 1";
        $result = $this->db->fetchAll($cmd);
        return count($result);
    }
    public function check_data_if_exist($params)
    {
        //$cmd="select po_id from po_transaction_data_2 where po_id={$params['id']} and is_column=0 limit 1";
        $cmd="select po_id from po_transaction_data_2_column where po_id={$params['id']} limit 1";
        $result = $this->db->fetchAll($cmd);
        return $result;
    }
    public function check_po_status($params)
    {
        $cmd="select status from po where id={$params['id']}";

        $result = $this->db->fetchRow($cmd);
        return $result;
    }
    public function get_project_status()
    {
        $cmd="select code,name from project_status ";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    
    }
    public function change_po_status($params)
    {
       $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>$params['status'],
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po', $values, 'id='.$id);
        $response = array(
            'message' => "PO status has been updated.",
        );
        return $this->success($response); 
    }
    
    public function get_po_listby_id($id)
    {
        if($id>0){
            //$cmd="select id,name from po  where status = 'A' AND client_id=".$id;
             $cmd="select id,name from po  where client_id=".$id;
        }else{
            $cmd="select id,name from po  where status = 'A'";
        }
        
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    }
    
    public function get_po_listby_all_id($id)
    {
       $cmd="select id,name from po  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd); 
        return $this->success($result);
    } 
    
     public function get_po_template_listby_all_id($id)
    {
        $cmd="select id,name from project_template  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    
    public function get_po_template_listby_id($id)
    {
        $cmd="select id,name from project_template  where status = 'A' AND client_id=".$id;
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
 

  public function get_total_dedup($param){
       // $cmd="select count(id) cnt from po_transaction_data_2 where phone_no in(
            // select phone_no from  po_transaction_data_2
            // where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1
            // order by phone_no 
          // )";
           $cmd1="select phone_no from  po_transaction_data_2_{$param['po_id']}
            where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
            //order by phone_no";
   //print_r($cmd1);
          
    //$rec2 = $this->db->fetchAll($cmd1);
    
          $dedup_phone = $this->db->fetchAll($cmd1); 
    $dedup_cnt = count($dedup_phone);
          
//    $phone_nos = "";
//    foreach($dedup_phone as $r)
//    {
//   $phone_nos.="'{$r['phone_no']}',";
//    }
//    
// 
//    $phone_nos = substr($phone_nos, 0, strlen($phone_nos)-1); 
//    $phone_nos= str_replace(",", " OR phone_no=",$phone_nos);

    
    //$cmd = "select count(id) cnt from po_transaction_data_2 where phone_no in({$phone_nos}) AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0";
//    $cmd = "select count(id) cnt from po_transaction_data_2 where (phone_no = {$phone_nos}) AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0";
//    print_r($cmd);
//    die();

        // $cmd ="SELECT count(b.phone_no) cnt" ;
        // $cmd .= " FROM ";
        // $cmd .= " po_transaction_data_2 b, "; 
        // $cmd .= " (select phone_no, count(1) as rec from  po_transaction_data_2 where po_id={$param['po_id']} and ";
        // $cmd .= " batch_no={$param['batch_no']} AND is_column=0  GROUP BY phone_no) AS a ";
        // $cmd .= " WHERE b.phone_no=a.phone_no ";
        // $cmd .= " AND a.rec>1 AND po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0  ";
        $cmd= "SELECT SUM(nos) AS cnt FROM ( SELECT count(phone_no) as nos FROM po_transaction_data_2_{$param['po_id']} where po_id={$param['po_id']} and batch_no={$param['batch_no']} AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no) > 1 ) AS t";

    $duplicate_nos = $this->db->fetchRow($cmd);
    
    
    $duplicat_nos_cnt = $duplicate_nos['cnt'];
    
          //$rec2 = $this->db->fetchAll($cmd1);                
          //$val1 = $rec1['cnt'];
          // $val2 = count($rec2);
          //return ($val1-$val2);
          $res = ($duplicat_nos_cnt - $dedup_cnt);
          
    return $res;
                                                                        
   }
 

    
   public function get_poname($id)
   {
       $cmd="select name from po where  id =".$id;
        $result = $this->db->fetchRow($cmd);                
        return $result['name'];
       
   } 
   public function get_item_for_billing($params)
  {
      
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE =". $params['po_id']);
        $no_of_q = $row_no_of_question['number_of_question'];

        $q_fields="";
        $q_fields_empty="";
        
        for($x =1; $x<=$no_of_q; $x++)
        {
            $q_fields.="'Q{$x}'," ; 
            $q_fields_empty.="''," ; 
        }
       
        $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
            print_r($q_fields);
            exit;
        $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
        
       $cmd = "select * from po_transaction_data_ans_2 where po_id = 1";
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
  } 
    
    public function paid_billing($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'P',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('billing_header', $values, 'id='.$id);
        $response = array(
            'message' => "Billing has been paid.",
        );
        return $this->success($response);
    }
    
    public function get_billing_details($param)
    {
        $cmd="select c.street,c.city,c.state, c.id as client_id,bh.remarks,bh.total_amount, CASE bh.status WHEN 'S' THEN 'SAVED' WHEN 'P' THEN 'PAID'  END as status,bh.create_date,bh.create_by,bh.invoice_no,
        bh.billing_date,bh.due_date,c.name as client_name,bd.*
        FROM  billing_header bh
        LEFT JOIN billing_detail bd on bh.id = bd.billing_header_id  
        LEFT JOIN client c on c.id=bh.client_id 
        where bh.id=".$this->db->db_escape( $param['id']);
      
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    } 
    public function get_billing_list()
    {
        $cmd="select bh.id,bh.total_amount as amount_due, CASE bh.status WHEN 'S' THEN 'SAVED' WHEN 'P' THEN 'PAID'  END as status,bh.create_date,bh.create_by,bh.invoice_no,
        bh.billing_date,bh.due_date,c.name as client_name
        FROM  billing_header bh  
        LEFT JOIN client c on c.id=bh.client_id 
        where c.status = 'A'";
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    } 
   public function save_billing($params)
   {
     
        if($params['txtID']==""){
            $invoice =$this->get_counter('billing');
            $insert_data = array(
                'client_id'     =>$params['client_name'],
                'invoice_no'     =>'INV# '.sprintf('%06d',$invoice),
                'billing_date'  =>$params['txtDueDate'], 
                'due_date'      =>$params['txtDueDate'], 
                'total_amount'  =>str_replace(',','',$params['txtTotal_amount']), 
                'status'        =>'S', 
                'remarks'       =>$params['txtRemarks'], 
                'create_by'     =>$_SESSION['username'], 
            );                     
             
            if (!$this->db->insert('billing_header', $insert_data)) {
                return $this->failed("Unable to save billing transaction.");
            } else {
                    $bill_id = $this->db->lastInsertId();
                    $row = explode(";",$params['txtRowValues']);
                    foreach($row as $res_row) 
                    { $temp[] = explode("|",$res_row); }
                    foreach($temp as $val) 
                    {                 
                             $insert_billing_details[] = array(     
                                'billing_header_id'=>$this->db->lastInsertId(), 
                                'line_number'=>$val[0],  
                                'description'=>$val[1],  
                                'quantity'=>$val[2],  
                                'rate'=>$val[3],  
                                'amount'=>str_replace(',','',$val[4]), 
                                 );
                            
                    }    
                
                        if (!$this->db->batchInsert('billing_detail', $insert_billing_details)) {
                            $this->db->delete('billing_header', 'id=' .  $this->db->lastInsertId());
                            return $this->failed("Unable to save billing transaction.");
                        } 
                            $params= array(
                                'name'=>'billing',
                                'value'=> $invoice,
                            );
                         $this->update_counter($params);   
                         return $this->response = array(
                                'doc_id'  =>  $bill_id,
                                'invoice' =>'INV# '.sprintf('%06d',$invoice),
                                'ResponseCode' => '0000',
                                'ResponseMessage' =>  'New billing transaction has been saved.',
                                ); 
            } 
        } else{

            $update_data = array(
                'client_id'     =>$params['client_name'],
                'billing_date'  =>$params['txtDueDate'], 
                'due_date'      =>$params['txtDueDate'], 
                'status'        =>'S', 
                'total_amount'  =>str_replace(',','',$params['txtTotal_amount']), 
                'remarks'       =>$params['txtRemarks'], 
                'update_date'=>date('Y-m-d H:i:s'), 
                'update_by'     =>$_SESSION['username'], 
            );                     
             
            if (!$this->db->update('billing_header', $update_data,'id=' . $params['txtID'])) {
                return $this->failed("Unable to update billing transaction.");
            } else {
                    $row = explode(";",$params['txtRowValues']);
                    foreach($row as $res_row) 
                    { $temp[] = explode("|",$res_row); }
                    foreach($temp as $val) 
                    {                 
                             $insert_billing_details[] = array(     
                                'billing_header_id'=>$params['txtID'], 
                                'line_number'=>$val[0],  
                                'description'=>$val[1],  
                                'quantity'=>$val[2],  
                                'rate'=>$val[3],  
                                'amount'=>$val[4], 
                                 );
                            
                    }    
                        $this->db->delete('billing_detail', 'billing_header_id=' .  $params['txtID']);
                        if (!$this->db->batchInsert('billing_detail', $insert_billing_details)) {
                            return $this->failed("Unable to save billing transaction.");
                        } 
                         return $this->response = array(
                                'doc_id'  =>  $params['txtID'],
                                'invoice' =>0,
                                'ResponseCode' => '0000',
                                'ResponseMessage' =>  'Billing Transaction has been updated.',
                                );  
            } 
            
            
            
            
           
        }
    }      
        
    public function get_counter($name)
    {
        $cmd="SELECT value FROM counter WHERE name =".$this->db->db_escape($name); 
        $result = $this->db->fetchRow($cmd); 
         if(is_array($result)){
             return $result['value'];
         }else{
                $insert_data=array(
                    'value' => 2,
                    'name'  =>$name, 
                );
                $this->db->insert('counter', $insert_data);
                return 1;
         }
        
    }
    public function update_counter($params)
     {
          $val = ($params['value'] + 1);
          $update_data = array(
            'value' => $val,
          );
         if(!$this->db->update('counter', $update_data,'name=' . $this->db->db_escape( $params['name'])))   
         {return false;} 
          else 
         {return true;}              
    }
   
 
  public function apply_data_center($params){

        $divisor = 10000;
        foreach($params as $row)
        {
           $center_calling_list_insert[] =  $row;
           
           //LC-01/04/2016 - will set no of records variable no_of_record;
          /* if(isset($row['no_of_record']))
           {*/
             $limit =  $row['record_cnt_to'];
          /* }else{
             $limit =  ($row['record_cnt_to'] - $row['record_cnt_from'])+1;
           }*/
           //PP 01/11/2016 - identify the # of list
           $list_cnt = $row['list_cnt'];
           $po_id =$row['po_id']; 
           $batch_no =$row['batch_no'];
           $center_id = $row['center_id'];
          
          /* //print_r($limit%3000); //get remainder
          //die();
           // $page = (int) ($limit/3000);
           
           
            // for($x=1; $x<=$page; $x++)
            // {
                // $cmd = "UPDATE po_transaction_data_2 SET center_id={$center_id}
                    // WHERE id IN (
                    // SELECT id FROM (
                    // SELECT id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
                    // AND center_id is null LIMIT {$limit}
                // ) AS c)";
                // $this->db->query($cmd);
            // }
           
           // $remainder = limit%3000;           
           // $cmd = "UPDATE po_transaction_data_2 SET center_id={$center_id}
                    // WHERE id IN (
                    // SELECT id FROM (
                    // SELECT id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
                    // AND center_id is null LIMIT {$remainder}
            // ) AS c)";
            // $this->db->query($cmd);
           
           
           //die();
           

            //$this->db->query("SET SESSION group_concat_max_len = 100000");
            //$res = $this->db->fetchRow("SELECT GROUP_CONCAT(id) AS id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
            //AND center_id is null LIMIT {$limit}");
            
            

            
            //print_r("SELECT GROUP_CONCAT(id) AS id FROM  po_transaction_data_2   WHERE po_id={$po_id} AND batch_no={$batch_no}
            //AND center_id is null LIMIT {$limit}");

            
            //$cmd= "UPDATE po_transaction_data_2 SET center_id={$center_id}
            //WHERE id IN ({$res['id']}) AND po_id={$po_id}";
            

            
            //$this->db->query($cmd);     */
			
			//LEMS for testing
		   $cmd ="SELECT process_option_id FROM po_transaction_data_2_{$row['po_id']} WHERE po_id =".$row['po_id']." and center_id is null and batch_no = ".$row['batch_no']." AND is_column=1";

		   $process_id = $this->db->fetchRow($cmd);
      
           
		   $process_id = $process_id['process_option_id'];
		   
           $looper = floor($limit/$divisor);
           
           $remainder =$limit%$divisor;
           
           
           for($cnt=1; $cnt<=$looper; $cnt++)
           {
                 $limit = $divisor;
		         if($process_id==1){ //Non-Household List AS-IS
                         //AND is_column=0 AND  LENGTH(TRIM(phone_no))<>10";
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                        
                 }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
                 
//                        $cmd = " select id from po_transaction_data_2 where   po_id =". $row['po_id'];
//                        $cmd .=" and batch_no=".$row['batch_no'];
//                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
//                        $cmd .=" UNION ALL";
//                        $cmd .=" select id from po_transaction_data_2 where   po_id =". $row['po_id'];
//                        $cmd .=" and batch_no=".$row['batch_no'];
//                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
//                        $cmd .=" LIMIT {$limit}";


                       
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                             
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";  
                        
                 }elseif($process_id==3 ){ //Load then Household list  
                        /*$params=array(
                            'po_id'=> $row['po_id'],
                            'batch_no'=>$row['batch_no'],
                        );
                        $dedup_cnt=$this->get_total_dedup($params);
                        $limit = ($limit+$dedup_cnt);                */
                        $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                        $cmd .=" LIMIT {$limit}";
                           //caught these variables
                
                        
                            
                        /*$cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";*/
                       /* $cmd .=" UNION ALL";
                        $cmd .=" select id from po_transaction_data_2 where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
                        $cmd .=" LIMIT {$limit}"; */
                         
                           
                       /* $cmd = " select id from po_transaction_data_2 where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}"; */  
                     
                       //" from po_transaction_data_2 where   po_id =". $params['po_id'];
                        //$cmd .=" and batch_no=".$params['batch_no'];
                        //$cmd .=" and center_id=".$params['center_id']. " AND  LENGTH(TRIM(phone_no))<>10 AND is_column=0 GROUP BY phone_no ";
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";
		           }
		           else{ //4 Already-Householded List Load
		           
		           }
		       

               $ids = "";
             //  $cmd = "select  id from po_transaction_data_2 where   po_id =".$row['po_id']." and center_id is null and batch_no = ".$row['batch_no']." AND is_column=0 LIMIT ".$row['record_cnt_to'];             
               $result = $this->db->fetchAll($cmd);
               
           /*    foreach ($result as $r) {
                     $ids= $r['id'].",";
                     file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
               }  */
              
               $myfile = fopen(md5($row['po_id'].'_'.$row['batch_no']).'.txt', "a");
               foreach ($result as $r) {
                     $ids= $r['id'].",";
                     //file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
                     fwrite($myfile, $ids);
                     /* //$update_data = array(
                        //'center_id' => $row['center_id'],
                        //);
                     //$this->db->update('po_transaction_data_2', $update_data,'id=' . $r['id']);        */
               }
               fclose($myfile);
               
               $this->db->query("UPDATE po_transaction_data_2_{$row['po_id']} SET list_cnt=".$list_cnt.", center_id=".$row['center_id']." WHERE  po_id = {$row['po_id']} and  id 
                            IN(".
                                substr(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'), 0, strlen(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'))-1)
                            .") AND batch_no={$row['batch_no']}");
               //unlink(md5($row['po_id'].'_'.$row['batch_no']).'.txt');     
           }      
           
           
           if($remainder>0)
           {
                 $limit = $remainder;
                 if($process_id==1){ //Non-Household List AS-IS
                         //AND is_column=0 AND  LENGTH(TRIM(phone_no))<>10";
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0";
                        $cmd .=" LIMIT {$limit}";
                        
                 }elseif($process_id==2 || $process_id==4){ //Non-Household List DE-DUP   /4 Already-Householded List Load
                 
                        $cmd = " select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
                        $cmd .=" UNION ALL";
                        $cmd .=" select id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null  AND  is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)>1";
                        $cmd .=" LIMIT {$limit}";
                             
                        //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                        //$cmd .=" FIELDS TERMINATED BY ','";
                        //$cmd .=" LINES TERMINATED BY '\n';";  
                        
                 }elseif($process_id==3 ){ //Load then Household list  
                        /*$params=array(
                            'po_id'=> $row['po_id'],
                            'batch_no'=>$row['batch_no'],
                        );
                        $dedup_cnt=$this->get_total_dedup($params);
                        $limit = ($limit+$dedup_cnt);                */
                        $cmd = " select GROUP_CONCAT(id) as id from po_transaction_data_2_{$row['po_id']} where   po_id =". $row['po_id'];
                        $cmd .=" and batch_no=".$row['batch_no'];
                        $cmd .=" and center_id is null AND  LENGTH(TRIM(phone_no))=10 AND is_column=0 GROUP BY phone_no";
                        $cmd .=" LIMIT {$limit}";
                   }
               

               $ids = "";
                   
               $result = $this->db->fetchAll($cmd);
               
           /*    foreach ($result as $r) {
                     $ids= $r['id'].",";
                     file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
               }  */
              
               $myfile = fopen(md5($row['po_id'].'_'.$row['batch_no']).'.txt', "a");
               foreach ($result as $r) {
                     $ids= $r['id'].",";
                     //file_put_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt', $ids , FILE_APPEND);
                     fwrite($myfile, $ids);
                     /* //$update_data = array(
                        //'center_id' => $row['center_id'],
                        //);
                     //$this->db->update('po_transaction_data_2', $update_data,'id=' . $r['id']);        */
               }
               fclose($myfile);
               
               $this->db->query("UPDATE po_transaction_data_2_{$row['po_id']} SET list_cnt=".$list_cnt.", center_id=".$row['center_id']." WHERE  po_id = {$row['po_id']} and  id 
                            IN(".
                                substr(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'), 0, strlen(file_get_contents(md5($row['po_id'].'_'.$row['batch_no']).'.txt'))-1)
                            .") AND batch_no={$row['batch_no']}");
               unlink(md5($row['po_id'].'_'.$row['batch_no']).'.txt');   
           } 
                   
            
        }
         
        
        if (!$this->db->batchInsert('center_calling_list', $center_calling_list_insert)) {
            return $this->failed("Unable to insert center calling list data");
        } 
       else {
              $value=array(
                            'id'    =>$po_id,
                            'status'=> 'A',
                            );
                            $this->change_po_status($value);
           
            $response = array(
                'message' => "Center data has been populated.",
            );                                                              
            return $this->success($response);
        }         
    }
   
  
    public function get_databack_2($params)
         {
            $dir = (dirname(dirname(__DIR__)));
            $dir = str_replace('\\', "/", $dir);
            $cmd = "select po_no from po where id ={$params['po_id']}";
            $rs = $this->db->fetchRow($cmd);
            $file_name =$rs["po_no"]."_DataBack_".time().".csv";
            $dir_file_name = $dir."/public/{$file_name}";
          

            
            $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);
            $no_of_q = $row_no_of_question['number_of_question'];
            $q_fields="";
            $q_fields_empty="";
            
            for($x =1; $x<=$no_of_q; $x++)
            {
                $q_fields.="'Q{$x}'," ; 
                $q_fields_empty.="''," ; 
            }
            
            $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
            $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
            
            
            
            //databack column
            $get_data_ans_col = "select remarks from po_transaction_data_ans_2_column where is_column =1 and  po_id =". $params['po_id'] ." limit 1";
            

            $row_data_ans_col = $this->db->fetchRow($get_data_ans_col);   
            $data_ans_col = explode(";",($row_data_ans_col['remarks']),-1);
            $ans_col_db ="";
            $temp_fields="";
            $phone_col="";
            $customer_response_map = array();
            $arr_response_map = array(); 
            //Get First name column
             $first_name_col = $this->db->fetchRow("SELECT fname_field FROM po_transaction_data_2_column  WHERE po_id = ".$params['po_id'] ." AND is_column=1 LIMIT 1");
             //Get First name column
            $proc_opt = $this->db->fetchRow("SELECT process_option_id FROM po_transaction_data_2_column  WHERE po_id = ".$params['po_id'] ." AND is_column=1 LIMIT 1");
            //Get Custom Status Code
            $custom_status_code = $this->db->fetchRow("select custom_status from po_management  WHERE po_id = ".$params['po_id']);
            //Get Customer Response Map
            $response_map = $this->db->fetchRow("select customer_response_map from po_management  WHERE po_id = ".$params['po_id']);
            if($response_map['customer_response_map']!=""){
               $customer_response_map = explode(",",$response_map['customer_response_map']); 
            } 
       
            $status_code_position=0;   
            foreach($data_ans_col as $r)
            {
                $column_name = explode("|",$r);  
                
                if(trim($column_name[1])=='spoke_to'||$column_name[1]=='call_date'||$column_name[1]=='call_time'||$column_name[1]=='duration'||
                    $column_name[1]=='TSR'||$column_name[1]=='status_code'||$column_name[1]=='remarks'
                    ||$column_name[1]=='cap1'||$column_name[1]=='cap2'||$column_name[1]=='cap3'||$column_name[1]=='cap4'){
                    $col1 = $column_name[0];
                    $col = $column_name[1];
                    if(trim($column_name[1])=='spoke_to'){
                        $fname = "tpda.{$col1}";
                        $ans_col_db.="'{$col}' AS {$col}Ans,";
                        if($proc_opt['process_option_id']==4){
                            $temp_fields .=  " tpda.{$col1} AS spoke_to, ";         
                        }else{
                            $temp_fields .=  "CASE WHEN tpda.{$col1}>=1  THEN ptd.{$first_name_col['fname_field']} ELSE '' END AS spoke_to,";         
                        }
                        
                        
                    }else{ 
                        //For custom Status code
                        if(trim($column_name[1])=='status_code'){
                            $status_code_position = "{$col1}tpdaa";
                          }  
                        $ans_col_db.="'{$col}' AS {$col}Ans,";      
                        $temp_fields .= "tpda.{$col1} as {$col1}tpdaa ,"; 
                    }
                }
             
                 if(trim($column_name[1])=='phone'){
                        $phone_col = $column_name[0];    
                    }
                 
                for($x =1; $x<=$no_of_q; $x++){
                    $is_has_custom_response = 0;
                    if(trim($column_name[1])=="Q".$x){
                        $col1 = $column_name[0];
                        $col = $column_name[1];
                        $ans_col_db.="'{$col}',";
                        //Response Map
                        $str_case = "";
                        
                         foreach($customer_response_map as $res){
                                 $res1= explode(':',$res);
                                 if($res1[0]==$x){
                                    $str_case="CASE tpda.{$col1} "; 
                                 }                     
                         }
                        
                        
                        if(count($customer_response_map)>0){
                            
                           $temp_fields.=  $str_case; 
                             foreach($customer_response_map as $res){
                                 
                                 $res1= explode(':',$res);
                                 if($res1[0]==$x){
                                    $is_has_custom_response = 1;
                                    $res2= explode('=',$res1[1]);
                                    $temp_fields.="WHEN {$res2[0]} THEN '{$res2[1]}' ";   
                                 }
                                 
                             }
                                if($is_has_custom_response==1){
                                    $temp_fields.=  "END AS {$col1}Ans, "; 
                                }else{
                                   $temp_fields .=  "tpda.{$col1} AS {$col1}Ans," ; 
                                }
                        }else{
                             $temp_fields .=  "tpda.{$col1} AS {$col1}Ans," ;            
                        }   
                     }
                    
                }           
            }
            
           
             $temp_fields =substr($temp_fields, 0, strlen($temp_fields)-1); 
             $ans_col_db = substr($ans_col_db, 0, strlen($ans_col_db)-1);
          
             if(!(isset($row_data_ans_col['remarks'])))
             {
                    $ans_col_db ="'spoke_to','call_date','call_time','duration','TSR','status_code',";
                    $temp_fields = "'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'' status_code,";
                    for($x =1; $x<=$no_of_q; $x++){
                        $ans_col_db.="'Q{$x}',";       
                        $temp_fields.="'' Q{$x},";       
                    }
                    $ans_col_db = substr($ans_col_db, 0, strlen($ans_col_db)-1); 
             }
              
             
             $temp_fields  = substr($temp_fields, 0, strlen($temp_fields)-1);
              
           /*   print_r($temp_fields);
              die(); */
             
            $get_column_cmd="select  col_fields,misc_fields, remarks from po_transaction_data_2_column where is_column =1 and  po_id =". $params['po_id'] ." limit 1";
            $row = $this->db->fetchRow($get_column_cmd);
                     
            
            //Col
            $col_fields = explode("|",unserialize($row['col_fields']));
           
            $col_fields_db ="";     
            foreach($col_fields as $r)
            {
                $col_fields_db.="ptd.{$r}," ;   

            }
            $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);

             //Misc
             $misc_field="";
            if($row['misc_fields']!="")
            {
                $misc_field = unserialize($row['misc_fields']);
            }
            $other_fields ="";           
            $other_fields_rows="";
    //        foreach($misc_field as $r)
    //        {
    //            $column_name = explode("|",$r);
    //            $col = $column_name[1];
    //            $other_fields.="'ptd.{$col}'," ;    
    //            $other_fields_rows.="ptd.{$col} AS {$col}tpda," ;    
    //        }
    //        
    //        $other_fields = substr($other_fields, 0, strlen($other_fields)-1);
    //        $other_fields_rows = substr($other_fields_rows, 0, strlen($other_fields_rows)-1);
            
             
           /*  //Remarks
            $remarks = unserialize($row['remarks']);
            $remarks = explode("|",$remarks);
            $remarks_field ="";
            $remarks_data_field = "";
            foreach($remarks as $r)
            {
                $col = $r;
                
                foreach($data_ans_col as $r1){
                    
                    $column_name = explode("|",$r1);  
                
                     if(trim($column_name[1])==trim($col))  {
                          $remarks_data_field.="tpda.{$column_name[0]}," ;
                      
                     }
                   
                 }
                 
                 if(!(isset($row_data_ans_col['remarks'])))
                 {
                    $remarks_data_field .="'',";
                 }
     
                
                $remarks_field.="'{$col}'," ;
                //$remarks_data_field.="''," ;
            } */
    //        Array ( [0] => col1|crID [1] => col2|phone [2] => col3|last_name [3] => col4|first_name [4] => col5|Firstname [5] => col6|Midname [6] => col7|Lastname [7] => col8|State [8] => col9|Email [9] => col10|misc [10] => col11|misc2 [11] => col12|spoke_to [12] => col13|call_date [13] => col14|call_time [14] => col15|duration [15] => col16|TSR [16] => col17|status_code [17] => col18|Q1 [18] => col19|Q2 [19] => col20|Q3 ) 
            
            
    //        print_r($remarks);
    //        die();
            
    //        print_r(count($remarks));
    //        die();
       /*     $remarks_field = substr($remarks_field, 0, strlen($remarks_field)-1);   
            $remarks_data_field = substr($remarks_data_field, 0, strlen($remarks_data_field)-1);  */
            
            $other_fields = str_replace("'", "", $other_fields);
           /* if(trim($remarks_field!=""))
            {
                $cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec',{$remarks_field},'phone_clean',{$ans_col_db} from po_transaction_data_2 ptd";     
            }else{
                $cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2 ptd"; 
            } */
             
              
            //$cmd="select {$col_fields_db},{$other_fields},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2_column ptd"; 
            $cmd="select {$col_fields_db},'centerID','master_rec','phone_clean',{$ans_col_db} from po_transaction_data_2_column ptd"; 
            $cmd .=" where po_id={$params['po_id']} and is_column=1 limit 1";     
              
               
            $res = $this->db->fetchRow($cmd);
            
           $fp = fopen('php://output', 'w');
            
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
            header('Pragma: no-cache');
            header('Expires: 0');
            
            fputcsv($fp, array_values($res)); 
                      
             //$cmd UNION ALL"; 
           /* if(trim($remarks_field!=""))
            {
              $cmd .=" select {$col_fields_db},{$other_fields},ptd.center_id,'Y',{$remarks_data_field},ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            }else{
                $cmd .=" select {$col_fields_db},{$other_fields},ptd.center_id,'Y',ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            }  */    
    //        $cmd =" select {$col_fields_db},{$other_fields_rows},ptd.center_id, CASE WHEN LENGTH(TRIM(ptd.phone_no))=10 THEN 'Y' ELSE 'N'  END as master_rec,ptd.phone_no,{$temp_fields}  from  po_transaction_data_2 ptd";
            $cmd =" select {$col_fields_db},ptd.center_id, CASE WHEN LENGTH(TRIM(ptd.phone_no))=10 THEN 'Y' ELSE 'N'  END as master_rec,ptd.phone_no,{$temp_fields}  from  po_transaction_data_2_{$params['po_id']} ptd";
            
                                                                                   
            //$cmd .=" INNER JOIN   po_transaction_data_ans_2 tpda on tpda.crid_permanent = ptd.id where tpda.po_id={$params['po_id']}";    
            /*$cmd .=" LEFT JOIN   po_transaction_data_ans_2 tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id where  ptd.po_id={$params['po_id']} AND ptd.is_column=0 AND ptd.center_id>=1"; */   
            if($phone_col!=""){
                $cmd .=" LEFT JOIN   po_transaction_data_ans_2_{$params['po_id']} tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id AND ptd.phone_no  = tpda.{$phone_col} where  ptd.po_id={$params['po_id']} AND ptd.is_column=0";    
            }else{
                $cmd .=" LEFT JOIN   po_transaction_data_ans_2_{$params['po_id']} tpda on  ptd.id  = tpda.crid_permanent AND ptd.po_id  = tpda.po_id  where  ptd.po_id={$params['po_id']} AND ptd.is_column=0";    
            }
         
            
            $status_codes = $this->get_status_codes($params['po_id']);
            $status_codes_callable="";
            foreach($status_codes as $s)
            {
                $status = $s['status_code'];
                foreach($_GET['coded_types'] as $c)
                {
                    if($s['status_type'] == $c)
                    {
                        $status_codes_callable .= "'{$status}',";        
                    }
                }
            }
            if($status_codes_callable != "") {
                $status_codes_callable = substr($status_codes_callable, 0, strlen($status_codes_callable)-1); 
            }
            
            if($_GET['record_type'] == 0) // this means that the user is downloading a databack depending on the param selected
            {
                //add filter parameters
                $cmd .=" AND  " . 'replace(tpda.status_code,"\r","")'." IN({$status_codes_callable})";
                if(!($_GET['txtCalled_From'] =="" AND $_GET['txtCalled_To'] == ""))
                {
                    $cmd .= " AND str_to_date(tpda.call_date,'%m/%d/%Y')>='{$_GET['txtCalled_From']}' AND str_to_date(tpda.call_date,'%m/%d/%Y')<='{$_GET['txtCalled_To']}'";
                } 
                
            }
            
    //        print_r($cmd);
    //        die();
          /*   print_r($cmd);
                die();*/
           
            //$res = $this->db->fetchAllTOFile($cmd, $file_name);      
                $res = $this->db->fetchAllTOFile($cmd, $fp,$status_code_position,$custom_status_code['custom_status']);
            /*$cmd .=" INTO OUTFILE '{$dir_file_name}'";
            $cmd .=" FIELDS TERMINATED BY ','";
            $cmd .=" LINES TERMINATED BY '\n';"; 
            */
            /*    $this->db->query($cmd);
                return $this->success($file_name);  */ 
            
    //        print_r($cmd);
    //        die();
    //        $res = $this->db->fetchAllTOFile($cmd, $file_name);
            //print_r($cmd);
          die();      
         }


 
     public function get_databack($params)
     {
        $cmd="select ptd.misc_fields, ptd.phone_no,tpda.* from po_transaction_data_ans tpda
        LEFT JOIN  po_transaction_data ptd on tpda.po_transaction_data_id = ptd.id where tpda.po_id =". $params['id']; 
      
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
 
      public function get_calling_list($params)
     {  
        $sql="select ccl.is_reassign, ccl.list_cnt,ccl.id,  cc.center_code,cc.name as center_name,ptd.batch_no,ptd.po_id,ptd.create_date,  ccl.center_id,ccl.record_cnt_from,ccl.record_cnt_to, ptd.total_count as record_count, ptd.process_option_id, ccl.assign_record_cnt ";
        //$sql .="     from po_transaction_data_2 ptd";
        //$sql .="     INNER JOIN center_calling_list ccl on ccl.po_id = ptd.po_id and ccl.batch_no = ptd.batch_no";
         $sql .="    ,householded_count, badphone_count, total_count, final_count, dedup_count";
        $sql .="     FROM center_calling_list ccl";
        //$sql .="     INNER JOIN call_center cc on cc.id = ccl.center_id ";
        $sql .="     LEFT JOIN call_center cc on cc.id = ccl.center_id ";
        $sql .="     INNER JOIN po_transaction_data_2_summary ptd on ccl.po_id = ptd.po_id and ccl.batch_no = ptd.batch_no";
        $sql .=" LEFT JOIN users u on u.user_center=ccl.center_id  ";
        //$sql .=" WHERE ptd.po_id =". $params['id'];
        $sql .=" WHERE ccl.po_id ={$params['id']}";
        $user_type = $this->get_user_type_by_username($_SESSION['username']);
        if(!in_array($user_type,array('Admin','System Admin','Data'))){
          $sql .= " and is_center = 1 and  u.username = '{$_SESSION['username']}'";  
        }
        $sql .=" GROUP BY ccl.id, center_id,cc.id ORDER BY ccl.center_id, ccl.id,ccl.is_reassign";
        
//        print_r($sql);
//        die();
        $result = $this->db->fetchAll($sql);
    
//          $out_calling_list = array(); 
//          foreach ($result as $row) {
            //   $sql="select center_id, count(id) as record_count   from po_transaction_data where batch_no = ".$row['batch_no']." and po_id =".$row['po_id'];
              // $record_count = $this->db->fetchRow($sql);
              // $record_count = $this->db->fetchRow($sql);
//                $out_calling_list[] = array(
//                    'id'  =>$row['id'], 
//                    'batch_no' =>$row['batch_no'],
//                    'center_name'=>$row['center_name'],
//                    'center_code'=> $row['center_code'],
//                    'po_id' => sprintf('%06d',$row['po_id']),
//                    'record_cnt' =>$row['record_cnt_from'] ." - ". $row['record_cnt_to'],
//                    'record_cnt_from' =>$row['record_cnt_from'],
//                    'record_cnt_to' =>$row['record_cnt_to'],
//                    'Date_uploaded' =>$row['create_date'],
//                    'button' =>'',
//                );
//        }
        
        
        //return $this->success( $out_calling_list);   
        return $this->success( $result);   
    }
 
 
    public function get_uploadbatchno($id)
    {
        if($id>0){
            $sql="select  max(batch_no)+1 as batch_no_cnt   from po_transaction_data_2_column where  po_id =".$id;
            //$sql="select  max(batch_no)+1 as batch_no_cnt   from po_transaction_data_2 where  po_id =".$id;
           
            $po_data_trans=$this->db->fetchRow($sql);  
            if($po_data_trans['batch_no_cnt']>0){
                 return $po_data_trans['batch_no_cnt'];
            }else{
                 return 1;
            }
        }
    
    }
    
 public function check_if_has_po_transaction_data($params)
  {
            
        $sql="select * from po_transaction_data_ans_2_column where  po_id =". $params['id']." LIMIT 1";
         
        $po_data_trans=$this->db->fetchAll($sql);  
        
        if(count($po_data_trans)>0){
            return TRUE;
            
        }else{
            return FALSE;
        }
  }
    
     public function isexist_po_transaction_data($params)
     {
        $cmd="select * from po_transaction_data_{$params['id']} where  po_id =".$this->hideit->decrypt(sha1(md5($this->salt)), $params['id'], sha1(md5($this->salt)))." LIMIT 1";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
    
     public function get_poid_by_encrypted_id($params)
     {
        $cmd="select id from po where  id =".$this->hideit->decrypt(sha1(md5($this->salt)), $params['id'], sha1(md5($this->salt)));
        
        $result = $this->db->fetchRow($cmd);                
        return $this->success( $result['id']);
     } 
    public function get_po_transaction_data($params)
     {
         $process_list_cmd="select ptd.process_option_id FROM po_transaction_data_2_{$params['id']} ptd
         inner join process_list_option plo on(plo.id=ptd.process_option_id) where  po_id =". $params['id'] ." limit 1";
         
         $result = $this->db->fetchRow($process_list_cmd);
         if($result['process_option_id']==1){ //Non-Household List AS-IS
                $cmd="select * from po_transaction_data_2_{$params['id']} where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['id'];
                $cmd .=" UNION ALL";
                $cmd .=" (select * from po_transaction_data_2_{$params['id']} where batch_no =". $params['batch_no'] ." and po_id =". $params['id'];
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
              
         }elseif($result['process_option_id']==2){ //Non-Household List DE-DUP
                $cmd="SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']}";
                $cmd .=" where is_column =1 and batch_no= " .$params['batch_no']. "  and po_id =". $params['id']; 
                $cmd .=" UNION ALL";
                $cmd .=" (SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']} where batch_no =". $params['batch_no'] ." and  po_id =". $params['id'] ." GROUP BY phone_no  order by id";
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
         }elseif($result['process_option_id']==3){//Already-Householded List Load
                 
         }else{ //4 - Already-Householded List Load
             
         }
         
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
     
      public function export_calling_list_2($params)
     {             
     
        $center_calling_list_id = $params['id'];
        
        $cmd ="SELECT record_cnt_from as min_id, record_cnt_to as max_id  FROM center_calling_list WHERE list_cnt ={$center_calling_list_id} and po_id ={$params['po_id']} and batch_no ={$params['batch_no']} AND center_id={$params['center_id']}";

           $r_id = $this->db->fetchRow($cmd);
           $min_id = $r_id['min_id'];
           $max_id = $r_id['max_id'];
         
        $dir = (dirname(dirname(__DIR__)));
        $dir = str_replace('\\', "/", $dir);
        $cmd = "select po_no from po where id ={$params['po_id']}";
        $rs = $this->db->fetchRow($cmd);
        $file_name =$rs["po_no"]."_Calling_List_".$params['center_code'].'_'.$params['batch_no'].'_'.time().".csv";
        $dir_file_name = $dir."/public/{$file_name}";
        
        
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);
        $no_of_q = $row_no_of_question['number_of_question'];
      
        $get_column_cmd="select lname_field, fname_field, misc_fields, remarks from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
  
        $row = $this->db->fetchRow($get_column_cmd);
        
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=". $params['po_id']);

        $no_of_q = $row_no_of_question['number_of_question'];

        $q_fields="";
        $q_fields_empty="";
        
        for($x =1; $x<=$no_of_q; $x++)
        {
            $q_fields.="'Q{$x}'," ; 
            $q_fields_empty.="'' Q{$x}," ; 
        }
        
        $q_fields = substr($q_fields, 0, strlen($q_fields)-1);
        
        $q_fields_empty = substr($q_fields_empty, 0, strlen($q_fields_empty)-1);
        
        $last_name = $row['lname_field'];
        $first_name = $row['fname_field'];
        
        $misc_field="";
        if($row['misc_fields']!="")
        {
            $misc_field = unserialize($row['misc_fields']);
        }
        $other_fields ="";
        if($misc_field!="")
        {
            foreach($misc_field as $r)
            {
                $column_name = explode("|",$r);
                $col = $column_name[1];
                $other_fields.="'{$col}'," ;
            }
        }
        $other_fields = substr($other_fields, 0, strlen($other_fields)-1);
    
        $remarks = unserialize($row['remarks']);
        /*$remarks = unserialize($row['remarks']);
        $remarks = explode("|",$remarks);
        $remarks_field ="";
        $remarks_empty_field = "";
        foreach($remarks as $r)
        {
            $col = $r;
            $remarks_field.="'{$col}'," ;
            $remarks_empty_field.="''," ;
        }
//        print_r(count($remarks));
//        die();
        $remarks_field = substr($remarks_field, 0, strlen($remarks_field)-1);
        
        $remarks_empty_field = substr($remarks_empty_field, 0, strlen($remarks_empty_field)-1);*/
        $other_fields = str_replace("'", "", $other_fields);
            
         if($params['process_option_id']==1){ //Non-Household List AS-IS
         
                if(!(trim($other_fields)==""))
                {
                    $other_fields = ",{$other_fields}";
                }
                
        if($q_fields==""){
            $cmd="select 'crID','phone','last_name','first_name' {$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time', 'duration','TSR','status_code' from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
   
        }else{
            $cmd="select 'crID','phone','last_name','first_name' {$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time', 'duration','TSR','status_code',
{$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
     
        } 
        
        
                $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');

                
                fputcsv($fp, array_values($res));


                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name}, {$other_fields},'','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
                if($q_fields_empty==""){
                    $cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code from po_transaction_data_2_{$params['po_id']} ptd  where   ptd.po_id =". $params['po_id'];    
                }else{
                    $cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code,{$q_fields_empty} from po_transaction_data_2_{$params['po_id']} ptd  where   ptd.po_id =". $params['po_id'];
                }
                
                //$cmd .=" and ccl.list_cnt = ".$params['id']."  and  ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.list_cnt = ".$params['id']."  and  ptd.batch_no=".$params['batch_no'] ;
                $cmd .=" and  ptd.batch_no=".$params['batch_no'] ;
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND ptd.is_column=0 AND  LENGTH(TRIM(ptd.phone_no))=10 order by  CAST(crID AS UNSIGNED)";
                $cmd .=" and id>={$min_id} AND id<={$max_id} AND ptd.is_column=0 AND  LENGTH(TRIM(ptd.phone_no))=10 ";
                //order by  CAST(crID AS UNSIGNED)";
           
//                print_r($cmd);
//                die();
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";                
         }
         
         elseif($params['process_option_id']==2 || $params['process_option_id']==4){ //Non-Household List DE-DUP    //Already-Householded List Load
                
             if($q_fields==""){
                 $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code' from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
             }else{
                 $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code',
    {$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];
             }
                
                //$cmd .=" UNION ALL";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name}, {$other_fields}, '','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
                
                $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, array_values($res));

                
                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select ptd.id,ptd.phone_no,{$last_name},{$first_name},{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'' status_code,{$q_fields_empty} from po_transaction_data_2 ptd where   ptd.po_id =". $params['po_id'];
                if($q_fields_empty==""){
                    $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name}, ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
                }else{
                    $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name}, ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4, '{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time,'' duration,'' TSR,'NC' status_code,{$q_fields_empty} from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
                }
                
                
                
                
                //$cmd .=" and  ccl.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and  ptd.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'];
                $cmd .="  and ptd.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 order by  CAST(crID AS UNSIGNED)";
                $cmd .=" and id>={$min_id} AND id<={$max_id} AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 "; //"order by  CAST(crID AS UNSIGNED)";
                
                
                
                //"GROUP BY phone_no HAVING COUNT(phone_no)=1";
//                $cmd .=" UNION ALL";
//                $cmd .=" select id,phone_no,{$last_name},{$first_name}, {$other_fields}, {$remarks_empty_field},'','','','','','',{$q_fields_empty} from po_transaction_data_2 where   po_id =". $params['po_id'];
//                $cmd .=" and batch_no=".$params['batch_no'];
//                $cmd .=" and center_id=".$params['center_id']. "  AND  is_column=0";//" GROUP BY phone_no HAVING COUNT(phone_no)>1";
//               print_r($cmd);
//               die();
                     
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";  
                
         }elseif($params['process_option_id']==3){      //Load then Household list
              if($q_fields==""){
                    $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code'
              from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];  
              }else{
                $cmd="select 'crID','phone','last_name','first_name',{$other_fields},'cap1','cap2','cap3','cap4','remarks','spoke_to','call_date','call_time',    'duration','TSR','status_code',
              {$q_fields} from po_transaction_data_2_column where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['po_id'];          
              } 
                
              
              $cmd = str_replace(",,",",", $cmd); // to handle empty other fields
                $res = $this->db->fetchRow($cmd);
                
                $fp = fopen('php://output', 'w');
                
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Pragma: no-cache');
                header('Expires: 0');

                
                fputcsv($fp, array_values($res));
                                                   

                //$cmd .=" UNION ALL";
                $cmd = "";
                //$cmd .=" select GROUP_CONCAT(ptd.id SEPARATOR '|'),ptd.phone_no,{$last_name},GROUP_CONCAT({$first_name} SEPARATOR '|'), {$other_fields}, '','','','', '{$remarks}','','','','','','',{$q_fields_empty} from po_transaction_data_2 ptd INNER JOIN center_calling_list ccl on(ptd.list_cnt = ccl.list_cnt) where   ptd.po_id =". $params['po_id'];
//                $cmd .=" select GROUP_CONCAT(ptd.id SEPARATOR '|'),ptd.phone_no,{$last_name},GROUP_CONCAT({$first_name} SEPARATOR '|'),{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'' status_code,
//              {$q_fields_empty}  from po_transaction_data_2 ptd where   ptd.po_id =". $params['po_id'];
        
            if($q_fields_empty==""){
                $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name},ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'NC' status_code
              from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
            }else{
                $cmd .=" select ptd.cr_ids,ptd.phone_no,{$last_name},ptd.first_names as first_name,{$other_fields},'' cap1,'' cap2,'' cap3,'' cap4,'{$remarks}' as remarks,'' spoke_to,'' call_date,'' call_time, ''    duration,'' TSR,'NC' status_code,
              {$q_fields_empty}  from po_transaction_data_2_calling_list_{$params['po_id']} ptd where   ptd.po_id =". $params['po_id'];
            }
            
              
                //$cmd .=" and  ccl.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'] ." AND ccl.batch_no=".$params['batch_no'];
                //$cmd .=" and  ptd.list_cnt = ".$params['id']."  and ptd.batch_no=".$params['batch_no'];
                $cmd .=" and ptd.batch_no=".$params['batch_no'];
                //$cmd .=" and ptd.center_id=".$params['center_id']. " AND  LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 GROUP BY ptd.phone_no order by  CAST(crID AS UNSIGNED)";
                //$cmd .="  and id>={$min_id} AND id<={$max_id} AND LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0 GROUP BY ptd.phone_no "; //" order by  CAST(crID AS UNSIGNED)";
                
                $cmd .="  and id>={$min_id} AND id<={$max_id} AND LENGTH(TRIM(ptd.phone_no))=10 AND ptd.is_column=0";
                  
                //$cmd .=" INTO OUTFILE '{$dir_file_name}'";
                //$cmd .=" FIELDS TERMINATED BY ','";
                //$cmd .=" LINES TERMINATED BY '\n';";
         }
         
         //print_r(phpinfo());
         //die();
       
        /* print_r($cmd);
           die();  */   
         
         //$res = $this->db->query($cmd);
        // $res = $this->db->fetchAll($cmd);
//        print_r($cmd);
//        die();
        
          //$res = $this->db->fetchAllTOFile($cmd, $file_name);
          
          $cmd = str_replace(",,",",", $cmd); // to handle empty other fields

          
          
          $datetime=$this->get_server_date();
            $this->save_to_action_logs('Download calling list.' ,'PO',$datetime,$_SESSION['username'],$params['po_id']);                                                            
          $res = $this->db->fetchAllTOFile($cmd, $fp);
           
            die();
        /*$fp = fopen('php://output', 'w');
        
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
        foreach($res as $row)
        {
            fputcsv($fp, array_values($row));
        }
        die();
         */   
         
         
         //$cmd = escapeshellcmd('mysql -u lemmuelc_webpol -p %3uLw(g06T}J -h localhost -e "'.$cmd.'"');

        
         //$test = shell_exec('/usr/bin/mysql -ulemmuelc_webpol -p %3uLw(g06T}J -h localhost -e' .'"'.$cmd);
         //var_dump($test);
         //die(mysql_error());
         //return $this->success($file_name);
         
     }

      
  
     public function export_calling_list($params)
     {
         $process_list_cmd="select ptd.process_option_id FROM po_transaction_data_2 ptd
         inner join process_list_option plo on(plo.id=ptd.process_option_id) where  po_id =". $params['id'] ." limit 1";
         
         $result = $this->db->fetchRow($process_list_cmd);
         if($result['process_option_id']==1){ //Non-Household List AS-IS
                $cmd="select col1,col2,col3 from po_transaction_data_2_{$params['id']} where is_column =1  and batch_no= " .$params['batch_no']. "   and  po_id =". $params['id'];
                $cmd .=" UNION ALL";
                $cmd .=" select  col1,col2,col3 from po_transaction_data_2_{$params['id']} where   po_id =". $params['id'];
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] ;
                $cmd .=" INTO OUTFILE 'C:/file.csv'";
                $cmd .=" FIELDS TERMINATED BY ','";
                $cmd .=" LINES TERMINATED BY '\n';";   
         }elseif($result['process_option_id']==2){ //Non-Household List DE-DUP
                $cmd="SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']}";
                $cmd .=" where is_column =1 and batch_no= " .$params['batch_no']. "  and po_id =". $params['id']; 
                $cmd .=" UNION ALL";
                $cmd .=" (SELECT *, count(phone_no) as cnt FROM po_transaction_data_2_{$params['id']} where  po_id =". $params['id'] ." GROUP BY phone_no  order by id";
                $cmd .=" LIMIT ".$params['limit']." OFFSET " . $params['offset'] .")";
         }elseif($result['process_option_id']==3){//Already-Householded List Load
              //"select phone_no from po_transaction_data_2  where center_id=1 and  po_id =1 group by phone_no having count(phone_no)>1";
         } 
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
     
     public function get_po_transaction_data_col($params)
     {
        $cmd="select * from po_transaction_data_2_{$params['id']} where  po_id =". $params['id']." LIMIT 1";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
     } 
    
     public function get_column_data($params)
     {

               $cmd="select row_data
                from po_transaction_data_{$params['id']} 
                where is_column = 1 and  po_id =". $params['id'];
                $result = $this->db->fetchRow($cmd);
             
                
            return $this->success( $result['row_data']);
     } 
     public function get_customer_databack_fields($params)
     {
        $cmd="select customer_databack_fields from po_management  where po_id =". $params['id'];
       
        $result = $this->db->fetchRow($cmd);
        return $this->success( $result['customer_databack_fields']);
     } 
     public function get_upload_summary($params)
     {
         
          if($params['type']=='AS-IS'){ //Non-Household List AS-IS
             
                $cmd =" select id,phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0";
                $result = $this->db->fetchAll($cmd);
                return count($result);
         }elseif($params['type']=='NON-H' || $params['type']=='AL-H'){ //Non-Household List DE-DUP    //Already-Householded List Load
             
                $cmd =" select phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0 GROUP BY phone_no HAVING COUNT(phone_no)=1";
                $result = $this->db->fetchAll($cmd);
                return count($result);
         }elseif($params['type']=='LD-H'){//Load then Household list
                $cmd =" select id,phone_no from po_transaction_data_2_{$params['po_id']} where";
                $cmd .="  batch_no=".$params['batch_no'];
                $cmd .=" and po_id=".$params['po_id']. " AND is_column=0 GROUP BY phone_no ";
                 $result = $this->db->fetchAll($cmd);
                 return count($result);
         }elseif ($params['type']=='bad_phone'){
             $cmd="select count(ptd.id) return_value from po_transaction_data_2_{$params['po_id']} ptd 
                 where ptd.is_column = 0 and ptd.batch_no = ". $params['batch_no'] ." and  ptd.po_id =". $params['po_id']. 
             " AND LENGTH(TRIM(phone_no))<>10";
           
             $result = $this->db->fetchRow($cmd);
             return $result['return_value'];
         }
  
        /* if($params['type']=='householded_records'){
        
           $cmd="select phone_no from po_transaction_data_2  where  is_column is null and batch_no = ". $params['batch_no'] ." and  po_id =". $params['po_id'] . 
          " group by phone_no having count(phone_no)>1";
          $result = $this->db->fetchRow($cmd);
          return $result;
         }elseif ($params['type']=='bad_phone'){
             $cmd="select count(ptd.id) return_value from po_transaction_data_2 ptd 
                 where ptd.batch_no = ". $params['batch_no'] ." and  ptd.po_id =". $params['po_id']. 
             " AND LENGTH(phone_no)<>10";
             $result = $this->db->fetchRow($cmd);
        return $result['return_value'];
         }else{}   */
         
        
     } 
     public function get_process_list_option()
    {
        $cmd="select id,name from process_list_option  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
     
  /*  public function upload_call_list($params)
    {  
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 1000, ",");
            $check_col =$data;
            $num = count($data); 
           fclose($handle); 
        } 
       
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\n");  
       //$file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
                        $phone_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                            if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .="@col".$c.","."'|',";

                          } 
                          
       $file_query .= substr($col, 0, -1);   
       $file_query .=" )";
       $file_query .=" set row_data=CONCAT(".substr($row_data, 0, -5)."),\n";
       $file_query .="  phone_no=@col".$phone_cnt.",po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       $file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
        
       $res= $this->db->query($file_query);

       if($res==1)
       {                     
           $update_po=array(
              'is_column'=>1,
           );
           if(!$this->db->update('po_transaction_data', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
              return false; 
           }else{
              return true; 
           }
       }else{
           return false;
       }
    }   */
    
    public function validate_column_format($po_id)
    {
       
        
    }
    
         
         
     public function upload_call_list_2_temp($params)
    {  
        
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $file_col="";
        $wrong_format = "";
        $wrong_format_str="";
        if (($handle = fopen($file, "r")) !== FALSE) {
          
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data); 
            
            
           fclose($handle); 
        } 
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            $file_col.="{$check_col[$x]}|" ;
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
       $file_col = explode("|",substr($file_col, 0, strlen($file_col)-1));
       $cmd="SELECT col_fields FROM po_transaction_data_2_column WHERE po_id={$po_id} and is_column = 1 limit 1";
        
       $row = $this->db->fetchAll($cmd);
        if(count($row)>0){
            
        
          foreach($row as $r_col){
           $col_fields = unserialize($r_col['col_fields']);
           $col_fields = explode("|",$col_fields);
           $format_col_fields="";
            foreach($col_fields as $r){
                $format_col_fields.="{$r}," ;
            }
               
            $format_col_fields = substr($format_col_fields, 0, strlen($format_col_fields)-1);
            
             $cmd="SELECT {$format_col_fields} FROM po_transaction_data_2_column WHERE po_id={$po_id} and is_column = 1 limit 1";
             $row_actual_col = $this->db->fetchRow($cmd);
           
              $x=1;
             
              foreach($file_col as $r){
                    if(trim($row_actual_col['col'.($x)]) != trim($r)){
                        $wrong_format=$wrong_format.trim($r)."|";
                    }
                     
                    $x++;
              }
           }
         $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
      
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>$row_actual_col,
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
         
    }else{
        
          foreach($file_col as $r){
                  
                  if( trim($r)=='spoke_to'|| trim($r)=='call_date'|| trim($r)=='call_time'|| trim($r)=='duration'||
                      trim($r)=='TSR'|| trim($r)=='status_code'){
                       $wrong_format=$wrong_format.trim($r)."|";
                }
          }
          
          $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
     
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>'',
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
          
    }
    
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $file_query  ="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_2_temp_{$po_id}";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
      // $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");  
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape(NEWLINE);  
       $file_query .=" IGNORE 1 LINES";
        
     
       $file_query .=" (";
                        $phone_cnt=-1;
                        $columns1 = "";
                        for ($c=1; $c <=$num; $c++) {
                             if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .= "col".$c."="."@col".$c.",";
                           $row_data_col .="col".$c."|";       
                           $columns1 .= ",col{$c}";
                          } 
                         
       $file_query .= substr($col, 0, -1);   
       //$file_query .=" ) set col_fields= '".serialize(substr($row_data_col, 0, -1))."',".$row_data;
       $file_query .=" ) set col_fields= '',".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .="  ,phone_no=trim(@col".$phone_cnt."),po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       //$file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .= "misc_fields='',remarks='',";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       

  
       $res= $this->db->query($file_query);
       if($res!=1)
       {     
          $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "Error in temp",
                );
           return $response;  
       }
     
       //insert to permanent table
       $insert_query="";
       $fixed_columns ="pdt.po_id,pdt.batch_no,is_column,center_id,pdt.phone_no,phone_field,lname_field,fname_field,process_option_id,remarks,col_fields,misc_fields,status,list_cnt,pdt.create_by,pdt.create_date";
       
       
       $fixed_columns_insert = str_replace("pdt.", "", $fixed_columns);
       $columns1_insert = str_replace("pdt.", "", $columns1);
       
       //$insert_query.=" INSERT INTO po_transaction_data_2 ({$fixed_columns}{$columns1}) SELECT {$fixed_columns}{$columns1}"; 
       $insert_query.=" INSERT INTO po_transaction_data_2_{$po_id} ({$fixed_columns_insert}{$columns1_insert}) SELECT {$fixed_columns}{$columns1}"; 
       //$insert_query.=" FROM po_transaction_data_2_temp WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id} AND batch_no={$params['batch_no']}";
       $insert_query.=" FROM po_transaction_data_2_temp_{$po_id} pdt 
LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null";
       
       $res= $this->db->query($insert_query);
       
       if($params['process_option_id']==3) //Load Then Household List Dedup
       {
           //$insert_query =" INSERT INTO po_transaction_data_2_calling_list (cr_ids,first_names,{$fixed_columns}{$columns1}) SELECT GROUP_CONCAT(id SEPARATOR '|') crids,GROUP_CONCAT({$params['fname_field']} SEPARATOR '|') first_names,{$fixed_columns}{$columns1}";
           $insert_query =" INSERT INTO po_transaction_data_2_calling_list_{$po_id} (cr_ids,first_names,{$fixed_columns_insert}{$columns1_insert}) SELECT GROUP_CONCAT(pdt.id SEPARATOR '|') crids,GROUP_CONCAT({$params['fname_field']} SEPARATOR '|') first_names,{$fixed_columns}{$columns1}";
           //$insert_query.=" FROM po_transaction_data_2 WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id}  AND batch_no={$params['batch_no']} GROUP BY phone_no ORDER BY po_id,id";
           $insert_query.=" FROM po_transaction_data_2_{$po_id} pdt 
            LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no 
            WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null GROUP BY pdt.phone_no  ORDER BY pdt.po_id,pdt.id";
           $res= $this->db->query($insert_query);
       }
       
       if($params['process_option_id']==2 || $params['process_option_id']==4) //Load Then Household List Dedup
       {
            
            //$insert_query =" INSERT INTO po_transaction_data_2_calling_list (cr_ids,first_names,{$fixed_columns}{$columns1}) SELECT id,{$params['fname_field']} first_names,{$fixed_columns}{$columns1}";
            $insert_query =" INSERT INTO po_transaction_data_2_calling_list_{$po_id} (cr_ids,first_names,{$fixed_columns_insert}{$columns1_insert}) SELECT pdt.id,{$params['fname_field']} first_names,{$fixed_columns}{$columns1}";
           //$insert_query.=" FROM po_transaction_data_2 WHERE phone_no NOT IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND po_id={$po_id} AND batch_no={$params['batch_no']} GROUP BY phone_no  ORDER BY po_id,id";
           $insert_query.=" FROM po_transaction_data_2_{$po_id} pdt 
LEFT JOIN po_transaction_data2_nos_{$po_id} pno ON pdt.po_id=pno.po_id AND pdt.phone_no=pno.phone_no 
WHERE pdt.po_id={$po_id} AND pdt.batch_no={$params['batch_no']} AND pno.phone_no is null GROUP BY pdt.phone_no  ORDER BY pdt.po_id,pdt.id";
           $res= $this->db->query($insert_query);
       }
       
       if($res==1)
       {    
            $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $ctr++; 
            }
            
            //$dedup_prev = $this->db->fetchRow("SELECT count(id) as total_dedup FROM po_transaction_data_2_temp WHERE phone_no IN(SELECT phone_no FROM po_transaction_data2_nos WHERE po_id={$po_id}) AND batch_no={$params['batch_no']}");
            $dedup_prev = $this->db->fetchRow("SELECT count(id) as total_dedup FROM po_transaction_data_2_temp_{$po_id} WHERE phone_no IN(SELECT phone_no FROM po_transaction_data2_nos_{$po_id} WHERE po_id={$po_id}) AND batch_no={$params['batch_no']} AND po_id={$po_id}");
            

            
            $cmd ="INSERT INTO po_transaction_data_2_column (". substr($column_name, 0, strlen($column_name)-1) .",col_fields,po_id,is_column,process_option_id,batch_no,phone_field,fname_field,lname_field,misc_fields,remarks,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",'".serialize(substr($row_data_col, 0, -1))."'";   
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['process_option_id'];   
            $cmd .= ",".$params['batch_no'];   
            $cmd .= ",".$this->db->db_escape($params['phone_field']); 
            $cmd .= ",".$this->db->db_escape($params['fname_field']); 
            $cmd .= ",".$this->db->db_escape($params['lname_field']); 
            $cmd .= ",".$this->db->db_escape($params['misc_fields']); 
            $cmd .= ",".$this->db->db_escape($params['remarks']); 
            
            if(isset($dedup_prev['total_dedup']))
            {
                $cmd .= ",'{$dedup_prev['total_dedup']}'";    
            }else
            {
                $cmd .= ",'0'";    
            }
            //$cmd .= ",'N'";
            
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
            

            $this->db->query($cmd);

            
            $cmd ="INSERT INTO po_transaction_data2_nos_{$po_id}" ;
            $cmd .="(phone_no, po_id, batch_no) SELECT phone_no, po_id, batch_no FROM po_transaction_data_2_temp_{$po_id} WHERE po_id={$po_id} AND batch_no={$params['batch_no']}";
            
            $this->db->query($cmd);
            
            
   //         $this->db->query("DELETE FROM po_transaction_data_2_temp_{$po_id} WHERE po_id={$po_id} AND batch_no={$params['batch_no']}");
            
            
//           $update_po=array(
//              'is_column'=>1,
//           );
//           if(!$this->db->update('po_transaction_data_2', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
               $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                ); 
//           }else{
                $value=array(
                            'id'    =>$po_id,
                            'status'=> 'DR',
                            );
                            $this->change_po_status($value);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              $response = array(
                     'data'   =>"",
                    'err_num'=>"0000",
                    'message' => "",
                   
                );
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Upload data row','PO',$datetime,$_SESSION['username'],$po_id); 
              return $response; 
//           }
       }else{
           $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "{$insert_query}",
                );
           return $response;  
       }
    }
    
     
   
    public function drop_client_data($id, $batch_no =0)
    {
         
        $data_result =$this->check_data_if_result_exist($id);
       
        if($data_result>0){
            return $this->failed("Unable to drop data if the result already uploaded.");
        }else{
            
           
            
            if($batch_no == 0)
            {
               
//                 if (! $this->db->delete('po_transaction_data_2', 'po_id=' .  $id)) {
//                                    return $this->failed("Unable to drop client data.1");
//                 }    
                $values = array(
                                    'status'=>'PD', //Once data dropped need to change the status to Pending Data
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                $this->db->delete('center_calling_list', 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_summary', 'po_id=' .  $id);
                /*$this->db->delete('po_transaction_data2_nos_'.$id, 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_temp_'.$id, 'po_id=' .  $id);
                $this->db->delete('po_transaction_data_2_calling_list_'.$id, 'po_id=' .  $id);*/
                $cmd_delete= " TRUNCATE TABLE po_transaction_data_2_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data2_nos_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data_2_temp_{$id};";
                $this->db->query($cmd_delete);
                $cmd_delete=" TRUNCATE TABLE po_transaction_data_2_calling_list_{$id};";
                $this->db->query($cmd_delete);
                
                $this->db->query($cmd_delete);
                $this->db->update('po', $values, 'id='.$id);
                $values_u = array(
                                    'universe'=>0,
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                                $this->db->update('po_parameter', $values_u, 'po_id='.$id);
                
                
            }else
            {
//                if (! $this->db->delete('po_transaction_data_2', 'po_id=' .  $id . " AND batch_no={$batch_no}")) {
//                    return $this->failed("Unable to drop client data.");
//                }   
                $this->db->delete("po_transaction_data_2_".$id, 'po_id=' .  $id . " AND batch_no={$batch_no}");
                $universe_drop_cnt = $this->db->fetchRow("select (final_count-dedup_count) as final_universe from po_transaction_data_2_summary where po_id= {$id} AND batch_no={$batch_no}");
                $po_universe = $this->db->fetchRow("select universe from po_parameter where po_id={$id}");
                 
                $this->db->delete('center_calling_list', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_column', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_summary', 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data2_nos_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_temp_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                $this->db->delete('po_transaction_data_2_calling_list_'.$id, 'po_id=' .  $id. " AND batch_no={$batch_no}");
                
                $cmd="SELECT id FROM po_transaction_data_2_column WHERE po_id={$id}";
               
                  $values_u = array(
                                    'universe'=>$po_universe['universe']-$universe_drop_cnt['final_universe'],
                                    'update_by'=>$_SESSION['username'],
                                    'update_date'=>date('Y-m-d H:i:s'), 
                                );
                                $this->db->update('po_parameter', $values_u, 'po_id='.$id);
                
                $row = $this->db->fetchRow($cmd);
                if(!isset($row['id']))
                {
                    $values = array(
                        'status'=>'PD', //Once data dropped need to change the status to Pending Data
                        'update_by'=>$_SESSION['username'],
                        'update_date'=>date('Y-m-d H:i:s'), 
                    );
                    $this->db->update('po', $values, 'id='.$id);
                }
                    
            }
            return $this->success("Client data has been dropped");
        }
        
    }
    
    public function upload_call_list($params)
    {  
        ini_set('max_execution_time', 0);
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $file_col="";
        $wrong_format = "";
        $wrong_format_str="";
        if (($handle = fopen($file, "r")) !== FALSE) {
          
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data); 
            
            
           fclose($handle); 
        } 
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            $file_col.="{$check_col[$x]}|" ;
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
       $file_col = explode("|",substr($file_col, 0, strlen($file_col)-1));
       $cmd="SELECT col_fields FROM po_transaction_data_2 WHERE po_id={$po_id} and is_column = 1 limit 1";
        
       $row = $this->db->fetchAll($cmd);
        if(count($row)>0){
            
        
          foreach($row as $r_col){
           $col_fields = unserialize($r_col['col_fields']);
           $col_fields = explode("|",$col_fields);
           $format_col_fields="";
            foreach($col_fields as $r){
                $format_col_fields.="{$r}," ;
            }
               
            $format_col_fields = substr($format_col_fields, 0, strlen($format_col_fields)-1);
            
             $cmd="SELECT {$format_col_fields} FROM po_transaction_data_2 WHERE po_id={$po_id} and is_column = 1 limit 1";
             $row_actual_col = $this->db->fetchRow($cmd);
              $x=0;
            
              foreach($file_col as $r){
                    if(trim($row_actual_col['col'.($x+1)]) != trim($r)){
                        
                        $wrong_format=$wrong_format.trim($r)."|";
                         
                    }
                    $x++;
              }
           }
         $wrong_format_str = substr($wrong_format, 0, strlen($wrong_format)-1);
         $wrong_format=explode("|",$wrong_format_str);
           
         if(strlen($wrong_format_str)>0){
              $response = array(
                    'data'   =>$row_actual_col,
                    'err_num'=>"0001",
                    'message' => $wrong_format,
                ); 
            
             return $response;
         }
         
    }
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_2";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");   
       $file_query .=" IGNORE 1 LINES";
     
       $file_query .=" (";
                        $phone_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                             if ($check_col[$c-1]==$params['phone_field']) {
                              $phone_cnt = $c;  
                            }
                           $col .="@col".$c.","; 
                           $row_data .= "col".$c."="."@col".$c.",";
                           $row_data_col .="col".$c."|";       
                          } 
                         
       $file_query .= substr($col, 0, -1);   
       //$file_query .=" ) set col_fields= '".serialize(substr($row_data_col, 0, -1))."',".$row_data;
       $file_query .=" ) set col_fields= '',".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .="  ,phone_no=@col".$phone_cnt.",po_id=".$po_id.",is_column=0,process_option_id=".$params['process_option_id'].",\n";
       $file_query .="batch_no = ".$params['batch_no'].", phone_field=". $this->db->db_escape($params['phone_field']).",fname_field=".$this->db->db_escape($params['fname_field']).",lname_field=".$this->db->db_escape($params['lname_field']).",\n";     
       //$file_query .= "misc_fields=".$this->db->db_escape($params['misc_fields']).",remarks=".$this->db->db_escape($params['remarks']).",\n";
       $file_query .= "misc_fields='',remarks='',";
       $file_query .=" status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       

       $res= $this->db->query($file_query);
     
       if($res==1)
       {                  
            
            
            $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $ctr++; 
            }
            
            $cmd ="INSERT INTO po_transaction_data_2_column (". substr($column_name, 0, strlen($column_name)-1) .",col_fields,po_id,is_column,process_option_id,batch_no,phone_field,fname_field,lname_field,misc_fields,remarks,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",'".serialize(substr($row_data_col, 0, -1))."'";   
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['process_option_id'];   
            $cmd .= ",".$params['batch_no'];   
            $cmd .= ",".$this->db->db_escape($params['phone_field']); 
            $cmd .= ",".$this->db->db_escape($params['fname_field']); 
            $cmd .= ",".$this->db->db_escape($params['lname_field']); 
            $cmd .= ",".$this->db->db_escape($params['misc_fields']); 
            $cmd .= ",".$this->db->db_escape($params['remarks']); 
            $cmd .= ",'N'";
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
            

            $this->db->query($cmd);
//           $update_po=array(
//              'is_column'=>1,
//           );
//           if(!$this->db->update('po_transaction_data_2', $update_po, 'po_id='.$po_id.' AND phone_no='.$this->db->db_escape($params['phone_field']))){
               $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                ); 
//           }else{
                $value=array(
                            'id'    =>$po_id,
                            'status'=> 'DR',
                            );
                            $this->change_po_status($value);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              $response = array(
                     'data'   =>"",
                    'err_num'=>"0000",
                    'message' => "",
                   
                ); 
              return $response; 
//           }
       }else{
           $response = array(
                    'data'   =>"",
                    'err_num'=>"0002",
                    'message' => "",
                );
           return $response;  
       }
    }
   public function get_responses($po_id)
   {
        $cmd ="select psq.sequence as question,psr.row_no as response_no,psr.response,psr.go_to From  po_script_q_response psr
            INNER JOIN po_script_question psq ON(psr.po_script_question_id=psq.id) WHERE psq.po_id={$po_id} order by psq.sequence" ; 
         
        $result=$this->db->fetchAll($cmd); 
        return $result;  
   } 
    public function check_skip_logic($params)
   {
        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        
        //Check Skip Logic
        $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id={$po_id}");
        $max_res = $this->db->fetchRow("select max(psr.row_no) as max_res From  po_script_q_response psr
            INNER JOIN po_script_question psq ON(psr.po_script_question_id=psq.id) WHERE psq.po_id={$po_id}");
      /*
         print_r($max_res['max_res']);
         exit;
               */

          $status_codes = $this->db->fetchAll("select ptc.status_code,sct.name from project_template_code ptc
                                                INNER JOIN status_code_type sct on  ptc.type = sct.id 
                                                INNER JOIN project_template pt on pt.id = ptc.project_template_id
                                                INNER JOIN po p on p.project_template_id=pt.id
                                                where p.id ={$po_id}");
   
        
           $status_type = array();
           foreach($status_codes as $res_q){
                   $status_type[strtoupper($res_q['status_code'])] = strtoupper($res_q['name']);
                    
           }
         /*  print_r($status_type['T']);
           exit; 
                 */
        $no_of_q = $row_no_of_question['number_of_question'];
        $closing_no = ($no_of_q+1);
        $qestion_response=$this->get_responses($po_id);
       /* print_r($qestion_response);
        exit;     */
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 20000, ","); 
            $check_col =$data;
            $num = count($data);
            $col_num="";
            $status_code_col="";
            //Get column position
             for ($c=0; $c < $num; $c++) {
                for ($x=1; $x <=$no_of_q; $x++) {
                    if($data[$c]=='Q'.$x){
                               $col_num=$col_num. ($c).',';
                    } 
                }  
                if(trim($data[$c])=='status_code'){
                               $status_code_col=$c;
                    } 
             }
             
             $row = 0;
             $col_num = substr($col_num, 0, strlen($col_num)-1);
             $col_num = explode(",",$col_num);
              
            
              $script_res = array();
               $skip_logic=array();
               $str = "";
              $setof_res = array();
              $skip_num = 2;
              //$x=$max_res['max_res']; 
               // $x=1;
                 
//               print_r($qestion_response);
//               exit;  
                 for ($y=1; $y <=$no_of_q; $y++) {
                     
                     foreach($qestion_response as $res_q){ 
                         
                           if($res_q['question']==$y){
//                            $skip_logic=$skip_logic+array(
//                             /*$res_q['go_to']=>$res_q['response_no'],*/
//                             
//                             $res_q['response_no']=>$res_q['go_to'],
//                            );
                             
                             $resp[$res_q['response_no']] = $res_q['go_to'];
//                             $resp[] = array($res_q['go_to']);
                            $skip_logic[$res_q['response_no']] = $res_q['go_to'];
                           }
                           $resp ="";
                     } 
                        $setof_res[]=$skip_logic;
                        $skip_logic=array();
                        $resp ="";
                 }
                 /*print_r($setof_res);
                 exit;  */
            $wrong_crID ="";   
             while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                $num = count($data);
                /*print_r($col_num[0]);
                exit;  */
                  /*  // print_r($data[$status_code_col]);
                  */     
                      
                      //Check if the status codes are valid based on the project template
                        if(!array_key_exists(strtoupper($data[$status_code_col]),$status_type))
                        {
                             $wrong_crID=$data[0]; //Retrun the crID with incorrect skip logic.
                              $response = array(
                                     'success'=>false,
                                     'message' => "Unable to upload result. Invalid status code ".$data[$status_code_col]. ".  (CRID# ".$wrong_crID.")" ,
                                   );  
                              return $response;
                            
                        }else{
                            $type =  $status_type[strtoupper($data[$status_code_col])];
                         
                        }
                     
                         //Check all Complete answers
                    if($type=='COMPLETE' && $no_of_q>0){
                         if(trim($data[$col_num[0]])=="0" || trim($data[$col_num[0]])==""){
                              $wrong_crID=$data[0]; //Retrun the crID with incorrect skip logic.
                              $response = array(
                                     'success'=>false,
                                     'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                                   ); 
                              return $response;
                        }  
                       
                          
                      
                            if(!$this->validate_response($col_num,$data,$setof_res)){
                               $wrong_crID=$data[0]; 
                               $response = array(
                                  'success'=>false,
                                  'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                               ); 
                            return $response;
                            }   
                    }else{
                             if($type!="LEAVE MESSAGE(COMPLETE)" && $no_of_q>0)
                               {
                                //Check if non Complete status had a value.
                                    foreach($col_num as $r){
                                        if(trim($data[$r])!="" && trim($data[$r])!="0"){
                                           $wrong_crID=$data[0]; 
                                           $response = array(
                                              'success'=>false,
                                              'message' => "Unable to upload result. Please check your skip logic. (CRID# ".$wrong_crID.")" ,
                                           );
                                           return $response;  
                                        }
                                    }
                               }

                    }
                }
                  
           fclose($handle);  
           $response = array(
                 'success'=>true,
                 'message' => "CSV file has been uploaded." ,
           ); 
           
           return $response;
    }
         
      
       
}
    public function alertMessage($message){
      echo '<script type="text/javascript">alert("' . $message . '")"</script>';
    } 
   public function upload_finished_call_list($params)
    {  

        $file =$params['file'];
        $po_id =  $params['id'];
        $num = 0;
        $str='';
        $check_col="";
        $check_crid="";
        $cmd="select allow_disable_logic from po_management where po_id={$po_id}";
        $result = $this->db->fetchRow($cmd);
        if($result['allow_disable_logic']==2)
        {
           $skip_logic=$this->check_skip_logic($params);
           if(!$skip_logic['success']){
             return $this->failed($skip_logic);
            }   
       } 
       $cmd ="SELECT process_option_id FROM po_transaction_data_2_column WHERE po_id =".$params['id']." and center_id is null AND is_column=1 LIMIT 1";
       $proc_opt = $this->db->fetchRow($cmd);
       
        if (($handle = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($handle, 20000, ",");
            $check_col =$data;
            $num = count($data);
            $col_num="";      
            $call_date_position=""; 
            $crID_position=0;
            //Validation for Call date
            $call_date_position="";
            $call_date_temp="";
            $status_code_position="";
            $status_code_temp="";
             for ($c=0; $c <$num; $c++) {
                    if ($check_col[$c]=="crID") {
                              $crID_position = $c;   
                     }
                     if ($check_col[$c]=="call_date") {
                              $call_date_position = $c;   
                     }
                     if ($check_col[$c]=="status_code") {
                              $status_code_position = $c;   
                     }
                  }
                  
                    while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                        $temp_crID=$data[$crID_position];
                        $call_date_temp=$data[$call_date_position];
                        $status_code_temp=$data[$status_code_position];
                            if ($call_date_temp=="" ){
                                 return $this->failed("Unable to upload result. Please check your call_date value. CRID# ({$temp_crID})"); 
                                 die(); 
                            }  
                             if ($call_date_temp!="" && $status_code_temp=='NC'){
                                 return $this->failed("Unable to upload result. Please check your call_date value. CRID# ({$temp_crID})"); 
                                 die(); 
                            } 
                        }
           
             
             
            
          //Validation of spoke_to   for Option 3
           
           $spoke_to_position=0;
          if($proc_opt['process_option_id']==3) {
                  for ($c=0; $c <$num; $c++) {
                     if ($check_col[$c]=="crID") {
                              $crID_position = $c;   
                     }
                     if($check_col[$c]=="spoke_to"){
                                $spoke_to_position = $c;  
                     }
                  }  
              
                while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                       $temp_crID=$data[$crID_position];
                       $temp_spoke=$data[$spoke_to_position];
                        if (strpos($temp_crID, '|') !== FALSE){
                             $ids = explode("|",$temp_crID);  
                             if($temp_spoke>count($ids)){
                                 return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                                 die();
                             }
                        }else{
                             if($temp_spoke>1){
                                 return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                                 die();
                             }    
                        }
                   
                }
       
                
          }
          
          //Validation of spoke_to   for Option 4 First name
           if($proc_opt['process_option_id']==4) {
               
           $first_name_col = $this->db->fetchRow("SELECT fname_field FROM po_transaction_data_2_column  WHERE po_id = ".$params['id'] ." AND is_column=1 LIMIT 1");
           $first_name = $this->db->fetchRow("SELECT  {$first_name_col['fname_field']} as col  FROM po_transaction_data_2_column  WHERE po_id = ".$params['id'] ." AND is_column=1 LIMIT 1");
           $first_name  =$first_name['col'];
           $first_name = substr($first_name, 0, 5);
        
                $get_column_cmd="select  misc_fields from po_transaction_data_2_column where is_column =1 and  po_id =". $params['id'] ." limit 1";
                $row = $this->db->fetchRow($get_column_cmd);
                //Misc fields and get the firstname prefix
                $misc_field="";
                $count_firstname=1; //Set default value for firstname #1
                if($row['misc_fields']!=""){
                        $misc_field = unserialize($row['misc_fields']);
                   
                    foreach($misc_field as $r){
                        $column_name = explode("|",$r);
                        if(strtolower($first_name)==strtolower(substr($column_name[0], 0, 5))){
                              $count_firstname++;
                        }
                    }
                     for ($c=0; $c <$num; $c++) {
                         if($check_col[$c]=="spoke_to"){
                                    $spoke_to_position = $c;  
                         }
                      }  
                  
                     while (($data = fgetcsv($handle, 20000, ",")) !== FALSE) {
                           $temp_crID=$data[$crID_position];
                           $temp_spoke=$data[$spoke_to_position];
                           
                     } 
                 if($temp_spoke>$count_firstname){
                     return $this->failed("Unable to upload result. Please check your spoke_to value. CRID# ({$temp_crID})"); 
                     die();
                 }
             } 
                 
           }
          
            fclose($handle);  
          }
        //Get the column format if has exixting raw data and validate if it's identicaly 
        $columns = array();
        $columns = array();
        for($x =0; $x<$num; $x++){    //Get the columns of upload file
            
            $col_no = $x+1;
            $columns[] = array(
                '1' => "col{$col_no}",
                '2' => $check_col[$x]
            );
        }
      
       
       $file_query="";
       $clean_file= str_replace('\\','/',$file);
       $col="";
       $row_data="";
       $row_data_col="";
       $row_data_col_2="";
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE po_transaction_data_ans_2_{$po_id}";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" ENCLOSED BY '\"'" ; 
       //$file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\r\n");  
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape(NEWLINE);  
       //$file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
                        $crID_cnt=-1;
                        $spoke_to_cnt=-1;
                        $call_date_cnt = -1;
                        $call_time_cnt=-1;
                        $question_cnt=-1;
                        $status_code_cnt=-1;
                        for ($c=1; $c <=$num; $c++) {
                            if ($check_col[$c-1]=="crID") {
                              $crID_cnt = $c;  
                              
                              
                            }
                            if($check_col[$c-1]=="spoke_to"){
                                $spoke_to_cnt = $c;  
                            }
                             if($check_col[$c-1]=="call_date"){
                                $call_date_cnt = $c;  
                            }
                            if($check_col[$c-1]=="call_time"){
                                $call_time_cnt = $c;  
                            }
                            if($check_col[$c-1]=="status_code"){
                                $status_code_cnt = $c;  
                            }
                            
                        
                          /*  //Get the actual column of all the question
                                for($x =1; $x<=$no_of_q; $x++){
                                     if($check_col[$c-1]=="Q{$x}"){
                                        $q_fields.="@col".$c.","; 
                                        $q_fields_data .="concat('col{$c}','|',@col{$c},';'),";
                                        $question_cnt=$c;
                                     }    
                                }   */ 
                        
                            
                            $col .="@col".$c.","; 
                            $row_data .= "col".$c."="."@col".$c.","; 
                            $row_data_col .="concat('col{$c}','|',@col{$c},';'),";         
                          }                                
       $file_query .= substr($col, 0, -1);      
       $file_query .=" ) set remarks=concat(".substr($row_data_col, 0, -1)."),".$row_data;
       $file_query = substr($file_query, 0, -1);
       $file_query .=", crid_permanent=@col".$crID_cnt.",po_transaction_data_2_id=@col".$crID_cnt.",po_id=".$po_id.",\n";
       $file_query .=" batch_no=-1,record_no=".$params['record_no'].",status='N',create_by=".$this->db->db_escape($_SESSION['username']);
       $file_query .=",status_code =TRIM(@col".$status_code_cnt."), call_date =@col".$call_date_cnt.",call_time=@col".$call_time_cnt."\n";
       
      /* //For Billing
       $file_query="LOAD DATA LOCAL INFILE ". $this->db->db_escape($clean_file);
       $file_query .=" INTO TABLE billing_record";
       $file_query .=" FIELDS TERMINATED BY " .  $this->db->db_escape(','); 
       $file_query .=" LINES TERMINATED BY  " .  $this->db->db_escape("\n");  
      // $file_query .=" IGNORE 1 LINES";
       $file_query .=" (";
       $file_query .= substr($q_fields, 0, -1);   
       $file_query .=" )";
       $file_query .=" set row_data=CONCAT(".substr($q_fields_data, 0, -1).")\n";
       $file_query = substr($file_query, 0, -1);   */
     
       $res= $this->db->query($file_query);
        
       if($res==1)
       {
          
           $ctr=1;
            $column_name="";
            $column_values="";
            foreach($columns as $col)
            {
                  $column_name .="col{$ctr},";
                  $column_values .="'{$col['2']}',";
                  $row_data_col_2 .="concat('col{$ctr}','|','{$col['2']}',';'),";   
                  $ctr++; 
            }
              
            $cmd ="INSERT INTO po_transaction_data_ans_2_column (". substr($column_name, 0, strlen($column_name)-1) .",po_id,is_column,batch_no,status,create_by)"; 
            $cmd .= "VALUES(".substr($column_values, 0, strlen($column_values)-1);
            $cmd .= ",{$po_id}";   
            $cmd .= ",1";   
            $cmd .= ",".$params['batch_no'];               
            $cmd .= ",'N'";
            $cmd .= ",".$this->db->db_escape($_SESSION['username']).")"; 
           if(!$this->db->query($cmd)){
              return false; 
           }else{
               $this->db->query("UPDATE po_transaction_data_ans_2_column set remarks=concat(".substr($row_data_col_2, 0, -1).") WHERE id=". $this->db->lastInsertId());
               $cmd="select id,po_transaction_data_2_id,col{$spoke_to_cnt} as spoke_to from po_transaction_data_ans_2_{$po_id} where col{$spoke_to_cnt} >0 and  LOCATE('|',po_transaction_data_2_id)>0";
               $result=$this->db->fetchAll($cmd); 
               foreach($result as $res_row) {
                    $crids = explode("|",$res_row['po_transaction_data_2_id']); 
                    $spoke_to_permanent_id = $crids[$res_row['spoke_to']-1];
                    $this->db->query("update po_transaction_data_ans_2_{$po_id} set crid_permanent = {$spoke_to_permanent_id} where id = {$res_row['id']}");
               }
               $this->db->delete('po_transaction_data_ans_2'."_{$po_id}", ' crid_permanent= 0 and po_id=' .  $params['id']);
                 $datetime=$this->get_server_date();
                 $this->save_to_action_logs('Upload data row','PO',$datetime,$_SESSION['username'],$params['id']);
              //Delete file after upload
              if(file_exists($file)){
                 unlink($file);       
              }
              
              //Update number of Completes
              $number_complete = $this->get_number_complete($params['id']);
              $this->db->query("UPDATE po_parameter set complete=complete+{$number_complete} WHERE po_id=". $params['id']);
               
               return $this->success("CSV file has been uploaded.");
           }  
           return $this->success("CSV file has been uploaded.");
           
       }else{
          return $this->failed("Unable to upload result.");
       }
     
       
    }
    
  public function check_if_has_script($po_id)
  {
       
        $sql="select * from po_script_question where po_id=".$po_id;
         
        $po_que=$this->db->fetchAll($sql);  
        
        if(count($po_que)>0){
            return TRUE;
            
        }else{
            return FALSE;
        }
  }
       public function edit_user($params)
    {
        $params = array_map('trim', $params);
        if (!(isset($params['mobile_number'],$params['fname'], $params['lname'], $params['email_address'], $params['username'], $params['user_type']))) {
            return $this->failed('Missing required fields');
            die();
        } elseif (($params['mobile_number'] == '' && $params['fname'] == '' && $params['lname'] == '' && $params['email_address'] == '' && $params['username'] == '' && $params['user_type'] == '')) {
            return $this->failed('Missing required fields');
            die();
        }
         elseif (!strstr($params['email_address'], '@')) {
             return $this->failed('Invalid email address');
             die();
        }
        else 
        {        
            $id = $params['id'];
            $update_data = array(
                'username'      => $params['username'],
               // 'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))),
                'fname'         => $params['fname'],
                'lname'         => $params['lname'],
                'email_address' => $params['email_address'],
                'mobile_number' => $params['mobile_number'],                
                'user_type'     => $params['user_type'],                      
                'is_center'     =>  $params['is_center'],
                'user_center'     =>  $params['user_center'],
                'update_by'=>$_SESSION['username'],
                'update_date'=>date('Y-m-d H:i:s'));          
               
                
             if (!$this->db->update('users', $update_data,'id=' . $id)) 
             { return $this->failed("Unable to update user."); } 
             else 
             {
                $response = array('message' => "User record has been updated.",);
                return $this->success($response);
            }              
        }                   
    }
  public function add_po_script($params){
                     
      $num_of_q = $params['txtQuestion_index'];
      
     
         // $num_of_q= $num_of_q-2; //minus 2 to eliminate na start and end
          $po_id =  $params['txtID'];
          //For Editing
          $sql="select * from po_script_question where po_id=".$po_id;
          $po_que=$this->db->fetchAll($sql);
          foreach ($po_que as $row) {
                  $this->db->delete('po_script_q_response', 'po_script_question_id=' . $row['id']);
           }
           $this->db->delete('po_script_question', 'po_id=' . $po_id);
          //End
       for ($i = 0; $i < $num_of_q; $i++) {
           if($i==0)
           {
               //For Start   
                $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$i, 
                    'question'=>$this->replace_schar($params['txtOpening']), 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );  
           }elseif($i==($num_of_q-1))
           {
               //For End
              $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$num_of_q-1, 
                    'question'=>$this->replace_schar($params['txtEnd']), 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );  
           }else{
               //For Questions
             $insert_po_script = array(     
                    'po_id'=>$po_id,
                    'sequence'=>$i, 
                    'question'=>$this->replace_schar($params['txtScript'.$i]), 
                //    'rate'=> $params['txtRate_'.$i], 
                    'status'=>'A', 
                    'create_by'=>$_SESSION['username'],
                    );
           }
       
      
      
     

        if (!$this->db->insert('po_script_question', $insert_po_script)) {
            return $this->failed("Unable to add new PO Script");
        } else {
            
            if($i==0)
           {
             
           }elseif($i==($num_of_q-1))
           {
             
           }else{
               
               $temp = array();  
               $temp1 = array();
                       $insert_po_response = array();
                        if($params['txtResponse_'.$i]!="")
                        {                     
                              $row=explode("|",substr($params['txtResponse_'.$i],0,-1));
                                foreach($row as $res_row) {
                                    $temp[] = explode(",",$res_row);;
                                }
                                foreach($temp as $val) {
                                    if($val[2]=="Closing"){
                                        $goto=$num_of_q-1; 
                                    }else{
                                        $goto=$val[2]; 
                                    }

                                    
                                   
                                $insert_po_response[] = array(     
                                        'po_script_question_id'=>$this->db->lastInsertId(), 
                                        'row_no'=>$val[0],  
                                        'response'=>$val[1],  
                                        'rate'=>$val[3],  
                                        'go_to'=>$goto,  
                                        'create_by'=>$_SESSION['username'],
                                         );
                             }
                          
                        if (!$this->db->batchInsert('po_script_q_response', $insert_po_response)) {
                            return $this->failed("Unable to add new PO Script");
                        } else {
                                $value=array(
                                        'id'    =>$po_id,
                                        'status'=> 'PD',
                                );
                            
                            $this->change_po_status($value);
                        }
                            
                            
                        }  
               
               
           }
                    
       
     }
           
    }
    $datetime=$this->get_server_date();
    $this->save_to_action_logs('Added new PO Script.','PO Script',$datetime,$_SESSION['username'],$po_id);
    if(count($po_que)>0){
        //Email notification   
        //Send mail if the script has been modified
        $cmd = "select po_no from po where id ={$po_id}";
        $rs = $this->db->fetchRow($cmd);
        $email = $this->get_email_by_username($_SESSION['username']);
        $body="<br><br>Greetings!<br><br>
        PO Script for PO number {$rs['po_no']} has been modified by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
        foreach($email as $s){
            $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");      
        }
        }
    $response = array(
                    'message' => "PO Script has beed added.",
                );                                                              
            return $this->success($response);    
  }
  
   public function edit_project($params){
       
           $id =$params['txtID'];
            $update_po = array( 
            'name'=>$params['txtPOName'],  
            'project_id_old'=>isset($params['po_list'])? $params['po_list']: -1,  
            'project_template_id'=>$params['po_template_list'],  
            //'client_id'=>$params['client'], 
            'status'   => $params['status'],
            'update_by'=>$_SESSION['username'],
            'update_date'=>date('Y-m-d H:i:s'), 
        ); 
        
       /* $row_no_of_question = $this->db->fetchRow("SELECT number_of_question FROM po_parameter WHERE po_id=".$id);
        $no_of_q = $row_no_of_question['number_of_question'];
        if($no_of_q!=$params['number_of_question']){
            return $this->failed("Warning, PBS detected that you've modified the # of questions."); 
        }
              */
        if (!$this->db->update('po', $update_po, 'id='.$id)) {
            return $this->failed("Unable to update project.");
        } else {
            
            $update_po_param = array( 
           //  'name'=>$params['name'], 
             //'client_id'=>$params['client'], 
           //  'contact_id'=>$params['contact_id'],  
             'start_date'=>$params['start_date'],  
             'start_time'=>$params['start_time'],  
             'end_date'=>$params['end_date'], 
             'end_time'=>$params['end_time'], 
             'state'=>$params['state'], 
             'universe'=>$params['universe'],  
             'total_contact'=>$params['total_contact'],  
             'caller_id'=>$params['caller_id'],  
             'goal_type'=>$params['goal_type'],  
             'goal_note'=>$params['goal_note'], 
             'number_of_question'=>$params['number_of_question'], 
             'update_by'=>$_SESSION['username'],
             'update_date'=>date('Y-m-d H:i:s'), 
        ); 
        
            if (!$this->db->update('po_parameter', $update_po_param, 'po_id='.$id)) {         
                return $this->failed("Unable to update project.");
            }else {
                  $update_po_manage = array( 
                        'campaign_note'=>$params['campaign_note'], 
                        'center_note'=>$params['center_note'], 
                        'manager_note'=>$params['manager_note'], 
                        'email_list'=>serialize($params['email_list']), 
                        'allow_download'=>$params['allow_download'], 
                        'allow_report'=>$params['allow_report'], 
                        'allow_grab_report'=>$params['allow_grab_report'], 
                        'allow_blank_question_zero'=>$params['allow_blank_question_zero'], 
                        'allow_disable_logic'=>$params['allow_disable_logic'], 
                        'custom_status'=>$params['custom_status'], 
                        'custom_databack'=>$params['custom_databack'], 
                        'customer_response_map'=>$params['customer_response_map'], 
                        'customer_databack_fields'=> serialize($params['customer_databack_fields']),
                        //'misc_field_databack'=>$params['misc_field_databack'],
                        'status'=>$params['status'], 
                      //  'report_template'=>$params['report_template'], 
                        'manager_id'=>$params['manager_id'], 
                        'update_by'=>$_SESSION['username'],
                        'update_date'=>date('Y-m-d H:i:s'), 
                    ); 
                    
                if (!$this->db->update('po_management', $update_po_manage, 'po_id='.$id)) {
                return $this->failed("Unable to update project.");
                }else {
                
                         //Abby 03-20 Inserting Goal information on po_goal
                    /*$arr_day=array(isset($params['day_checked_1']),isset($params['day_checked_2']),isset($params['day_checked_3']),isset($params['day_checked_4']),isset($params['day_checked_5']),isset($params['day_checked_6']),isset($params['day_checked_7'])) ; 
                    */ 
                    $arr_day = array();       
                    for($x =1; $x<=7; $x++){
                        isset($params['day_checked_'.$x]) ? $arr_day=array_merge($arr_day,array($x)) :  ''; 
                    }
                            
                                             $insert_po_goal = array(
                                                 'po_id'=>$id,
                                                 'goal_type'=>$params['goal_type'],
                                                 'goal_days_of_week'=>implode("-",$arr_day),
                                                 'goal_value'=>$params['goal_value'],
                                                 'update_date'=>date('Y-m-d H:i:s'),
                                                  );    
                                            //Abby 03-20 Inserting Goal information on po_goal
                                        
                        if (!$this->db->update('po_goal', $insert_po_goal,'po_id='.$id)) {
                            return $this->failed("Unable to update project.");
                            //Abby 03-20 Inserting Goal information on po_goal
                   }else{
                       
                   }
                
                  
                    
                }      
            } 
           
            
            $response = array(
                'message' => "PO has been updated.",
            );   
            $datetime=$this->get_server_date();
            $this->save_to_action_logs('Edit project order ' .$_POST['txtPOName'] ,'PO',$datetime,$_SESSION['username'],$id);    
            $email = $this->get_email_by_username($_SESSION['username']);
            $allow_report = "";
            if($params['allow_report']==1){
                $allow_report="-View report has been changed to Y";
            }
            $body="<br><br>Greetings!<br><br>
                                PO {$params['txtPOName']} has been modified by {$_SESSION['username']}.<br> {$allow_report} <br> <br> Thanks,<br> PBS Admin";    
                                $cmd = "select po_no from po where id ={$id}";
                                $rs = $this->db->fetchRow($cmd);
                                
                                 foreach($email as $s){
                                     $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]"); 
                                 }
                                
                                                      
            return $this->success($response);
        }         
    }
  
  
   public function get_po_number_of_question($params)
   {
     
      $cmd="select number_of_question
            from po_parameter 
            where  po_id =". $params['id'];
            $result = $this->db->fetchRow($cmd);
        return $this->success( $result['number_of_question']);
   } 
   
    public function get_client_databack_fields($params)
   {
        $cmd="select databack_fields from client c
        inner join po p on c.id = p.client_id
        where p.id = ".$params['id'];
        $result = $this->db->fetchRow($cmd);
        return $this->success( $result['databack_fields']);
   } 
   
    public function get_po_byid($params)
  {
      //Abby 3-31 added goal_value and goal_days_of_week
      $cmd="select p.id, p.name po_name, p.client_id, p.project_id_old, p.project_template_id, 
            p.status as po_status,pp.po_id, pp.name, pp.client_id, pp.contact_id, pp.start_date, pp.start_time, 
            pp.end_date, pp.end_time, pp.state,pp.universe, pp.total_contact, pp.caller_id, pp.goal_type, 
            pp.goal_note, pp.number_of_question,pm.po_id, pm.campaign_note, pm.center_note, pm.manager_note, pm.email_list, pm.allow_download, 
            pm.allow_report, pm.allow_grab_report, pm.allow_blank_question_zero, pm.allow_disable_logic, pm.custom_status,
            pm.custom_databack, pm.customer_response_map, pm.customer_databack_fields,pm.misc_field_databack, 
            pm.status, pm.report_template, pm.manager_id,pg.goal_value,pg.goal_days_of_week
            from po p 
            inner join po_parameter pp on(pp.po_id = p.id)
            inner join po_management  pm on(pm.po_id=p.id)
            left join po_goal pg on(pg.po_id=p.id)
            where  p.status<>'D' and p.id =".$params['id'];
            $record = $this->db->fetchAll($cmd); 
            return $this->success($record);
  }  
    public function delete_po($params)
  {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('po', $values, 'id='.$id);
        $response = array(
            'message' => "PO has been deleted.",
        );
        $datetime=$this->get_server_date();
        $this->save_to_action_logs('PO ID# '. sprintf('%06d',$id) . ' was successfully deleted' ,'PO',$datetime,$_SESSION['username'],$id);
 
        return $this->success($response);
    }   
   public function get_po($status=false)
   {
       	
       
       $cmd="select pp.complete, pp.universe, pm.allow_report,  p.po_no,DATE_FORMAT(pp.start_date,'%Y/%m/%d') as start_date,DATE_FORMAT(pp.end_date,'%Y/%m/%d') as end_date,   p.id, p.name as po_name,c.client_code,c.name as client_name,pt.name as template_name , p.project_id_old, p.project_template_id,
        DATE_FORMAT(p.create_date,'%Y/%m/%d') create_date, p.create_by, p.update_date, p.update_by, 
        
        ps.name as status
        from po p
        left join po_parameter as pp on(p.id=pp.po_id)
        left join client c on (c.id=p.client_id)
        left join project_status ps on (ps.code = p.status)
        left join project_template pt on (pt.id=p.project_template_id)
        left join po_management pm on(p.id=pm.po_id)
        where p.status <> 'D' and c.status='A'";
        if($_SESSION['client_id']>0){
           $cmd .=" AND p.client_id = {$_SESSION['client_id']}"; 
        }
        if($status=='true'){
          $cmd .=" AND pm.status<>'C'";  
        }
       
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
  
 public function add_project($params){
        $client_code = $this->get_client_code($params['client']);
        $po_no= $this->get_po_counter($params['client']);     
        $po_name =  $params['txtPOName'];
      if($params['po_list']>0){
          $cmd=" INSERT INTO po (SELECT NULL,'".$client_code.sprintf('%06d',$po_no)."', '".$params['txtPOName']."', p.project_id_old, p.project_id, p.project_template_id, p.client_id, 'PD', NULL, '{$_SESSION['username']}', p.update_date, ";
          $cmd .=" '{$_SESSION['username']}' FROM po AS p WHERE p.id={$params['po_list']});";
          if(!$this->db->query($cmd)){
             return $this->failed("Unable to add new project.");   
          }else{
              $new_po_id = $this->db->lastInsertId();
        $cmd  =" INSERT INTO po_parameter"; 
        $cmd .=" (SELECT NULL,{$new_po_id}, name, client_id, contact_id, start_date, start_time, end_date, end_time, ";
        $cmd .=" state, universe, total_contact, caller_id, goal_type, goal_note, number_of_question, status, create_date, ";
        $cmd .=" '{$_SESSION['username']}', update_date, '{$_SESSION['username']}' from po_parameter WHERE po_id={$params['po_list']});";
        if(!$this->db->query($cmd)){
             $this->db->delete('po', 'id=' . $new_po_id);
             return $this->failed("Unable to add new project.");   
          }else{ 
                $cmd =" INSERT INTO po_management"; 
                $cmd .=" (SELECT NULL, {$new_po_id}, campaign_note, center_note,manager_note, email_list, allow_download, allow_report, allow_grab_report, ";
                $cmd .=" allow_blank_question_zero, allow_disable_logic,";
                $cmd .=" custom_status, custom_databack, customer_response_map,"; 
                $cmd .=" customer_databack_fields, misc_field_databack, status, report_template, manager_id, create_date, create_by, update_date, update_by";
                $cmd .=" from po_management where  po_id={$params['po_list']});";
                
             if(!$this->db->query($cmd)){
                $this->db->delete('po', 'id=' . $new_po_id);
                $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                return $this->failed("Unable to add new project.");   
             }else{
                     $old_response_id = $this->db->fetchRow("SELECT id FROM po_script_question WHERE sequence>0  and  po_id={$params['po_list']}");
                  
                      if($old_response_id['id']!=""){
                            //For Scipt
                            $cmd =" INSERT INTO po_script_question";  
                            $cmd .=" (SELECT NULL, {$new_po_id}, sequence, question, rate, status, NULL, create_by, update_date,";  
                            $cmd .=" update_by from po_script_question where po_id={$params['po_list']})"; 
                             if(!$this->db->query($cmd)){
                                 $this->db->delete('po', 'id=' . $new_po_id);
                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                return $this->failed("Unable to add new project.");   
                             }else{
                                                              
                                    $list_of_questions =  $this->db->fetchAll("SELECT * FROM po_script_question WHERE po_id={$new_po_id}");
                                      
                                     foreach( $list_of_questions as $r){
                                     //
                                  
                                       $new_po_script_question_id = $r['id'];   
                                       $cmd =" INSERT INTO po_script_q_response"; 
                                       $cmd .=" (SELECT NULL, {$new_po_script_question_id}, row_no, response, go_to, NULL, create_by, update_date, update_by,rate from po_script_q_response where po_script_question_id={$old_response_id['id']})";
                                     
                                        if(!$this->db->query($cmd)){
                                             $this->db->delete('po', 'id=' . $new_po_id);
                                             $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                             $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                             $this->db->delete('po_script_question', 'po_id=' . $new_po_id);
                                            return $this->failed("Unable to add new project.");   
                                        }else{
                                            $cmd="insert into po_goal"; 
                                            $cmd .=" ( id,po_id, goal_type, goal_value,";
                                            $cmd .="  goal_days_of_week, status, create_by)";
                                            $cmd .=" (select NULL, {$new_po_id}, goal_type, goal_value,"; 
                                            $cmd .=" goal_days_of_week, status, '{$_SESSION['username']}'";
                                            $cmd .=" from po_goal where po_id={$params['po_list']})";
                                            if(!$this->db->query($cmd)){
                                                 $this->db->delete('po', 'id=' . $new_po_id);
                                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_question', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id);
                                               
                                            return $this->failed("Unable to add new project."); 
                                            }
                                        }
                                        
                                        
                                     } 
                                   
                                    //Copy Data
                                       if(isset($params['copy_data'])){
                                           //Get columns white values
                                            $get_column_cmd="select  col_fields from po_transaction_data_2_column where is_column =1 and  po_id =". $params['po_list'] ." limit 1";
                                            $row = $this->db->fetchRow($get_column_cmd);
                                            //Col
                                            $col_fields = explode("|",unserialize($row['col_fields']));
                                            $col_fields_db ="";     
                                            foreach($col_fields as $r){
                                                $col_fields_db.="{$r}," ;   
                                            }
                                            $col_fields_db = substr($col_fields_db, 0, strlen($col_fields_db)-1);
                                           //
                                           
                                            $cmd =" INSERT INTO po_transaction_data_2(id,po_id,batch_no, is_column, center_id, phone_no, phone_field
, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields, status, list_cnt, create_by
, create_date,{$col_fields_db})";  
                                            $cmd .=" (SELECT NULL, {$new_po_id}, batch_no,"; 
                                            $cmd .=" is_column, center_id, phone_no, phone_field, lname_field,"; 
                                            $cmd .=" fname_field, process_option_id, remarks, col_fields,"; 
                                            $cmd .=" misc_fields, status, list_cnt, '{$_SESSION['username']}', NULL,{$col_fields_db}  ";
                                            $cmd .=" from po_transaction_data_2 where po_id={$params['po_list']})";
                                           
                                            if(!$this->db->query($cmd)){
                                                 $this->db->delete('po', 'id=' . $new_po_id);
                                                 $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                 $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                 $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                return $this->failed("Unable to add new project.");   
                                            }else{
                                                    $cmd =" INSERT INTO po_transaction_data_2_column(id,po_id,batch_no, is_column, center_id, phone_no, phone_field
, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields, status, list_cnt, create_by
, create_date,{$col_fields_db})";  
                                                    $cmd .=" (SELECT NULL, {$new_po_id}, batch_no, is_column, center_id,"; 
                                                    $cmd .=" phone_no, phone_field, lname_field, fname_field, process_option_id, remarks, col_fields, misc_fields,"; 
                                                    $cmd .=" status, list_cnt, '{$_SESSION['username']}', NULL,{$col_fields_db}  ";
                                                    $cmd .=" from po_transaction_data_2_column where po_id={$new_po_id})";
                                                    if(!$this->db->query($cmd)){
                                                         $this->db->delete('po', 'id=' . $new_po_id);
                                                         $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                         $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                         $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                         $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                         $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                        return $this->failed("Unable to add new project.");   
                                                    }else{
                                                        $cmd =" insert into po_transaction_data_2_summary(id,process_option_id, po_id, batch_no, householded_count, badphone_count, total_count, final_count, dedup_count, create_date) ";  
                                                        $cmd .=" (SELECT NULL, process_option_id, {$new_po_id}, batch_no, householded_count, "; 
                                                        $cmd .=" badphone_count, total_count, final_count, dedup_count,NULL"; 
                                                        $cmd .=" from po_transaction_data_2_summary where po_id={$new_po_id})";
                                                        if(!$this->db->query($cmd)){
                                                             $this->db->delete('po', 'id=' . $new_po_id);
                                                             $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                             $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                             $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                             $this->db->delete('po_transaction_data_2_column', 'po_id=' . $new_po_id);
                                                            return $this->failed("Unable to add new project.");   
                                                        }else{
                                                             
                                                                $cmd ="  insert into po_transaction_data2_nos";  
                                                                $cmd .=" (SELECT NULL, phone_no, NULL, {$new_po_id}, batch_no "; 
                                                                $cmd .=" from po_transaction_data2_nos where po_id={$params['po_list']})";
                                                                if(!$this->db->query($cmd)){
                                                                     $this->db->delete('po', 'id=' . $new_po_id);
                                                                     $this->db->delete('po_parameter', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_management', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_script_question', 'po_id=' . $new_po_id); 
                                                                     $this->db->delete('po_script_q_response', 'po_script_question_id=' . $new_po_script_question_id); 
                                                                     $this->db->delete('po_transaction_data_2', 'po_id=' . $new_po_id);
                                                                     $this->db->delete('po_transaction_data_2_summary', 'po_id=' . $new_po_id);
                                                                    return $this->failed("Unable to add new project.");   
                                                                }else{
                                                                    $value=array(
                                                                    'id'    =>$po_id,
                                                                    'status'=> 'A',
                                                                    );

                                                                    $this->change_po_status($value);
                                                                }
                                                            
                                                        }
                                                          
                                                    }
                                                    
                                                
                                            }
                                           
                                       }
                                     //End Copy data
                                     
                                      
                                   
                                   
                                     $params= array(
                                            'client_id'=>$params['client'],
                                            'value'=> $po_no,
                                        );
                                     $this->update_po_counter($params);  
                                     
                                $response = array(
                                    'message' => "New project has been added.",
                                ); 
                                
                            $datetime=$this->get_server_date();
                            $this->save_to_action_logs('Registered new project order ' . $client_code.sprintf('%06d',$po_no) ,'PO',$datetime,$_SESSION['username'],$new_po_id);  
                                           
                        //Email notification           
                        $email = $this->get_email_by_username($_SESSION['username']);
                        $body="<br><br>Greetings!<br><br>
                        PO {$po_name} has been created by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin";    
                        foreach($email as $s){
                            $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#".$client_code.sprintf('%06d',$po_no)."]");
                        }
                        $this->create_po_transaction_tables($new_po_id);
                                return $this->success($response);
                                     
                                    // 
                                    }
                      }else{
                           $params= array(
                                            'client_id'=>$params['client'],
                                            'value'=> $po_no,
                                        );
                                     $this->update_po_counter($params);  
                                $response = array(
                                    'message' => "New project has been added, but an error occured while copying the script.",
                                );                                                              
                                return $this->success($response); 
                        }
                        
             }
                
          }
        
       
       }  
      }else{
        $insert_po = array(
            'po_no'=>$client_code.sprintf('%06d',$po_no), 
            'name'=>$params['txtPOName'],  
            'project_id_old'=>isset($params['po_list'])? $params['po_list']: -1,  
            'project_template_id'=>$params['po_template_list'],  
            'client_id'=>$params['client'], 
            'create_by'=>$_SESSION['username'],
        ); 
         $po_id = -1; 
        if (!$this->db->insert('po', $insert_po)) {
            return $this->failed("Unable to add new project.");
        } else {
              $po_id = $this->db->lastInsertId(); 
            $insert_po_param = array( 
             'po_id'=>$po_id,  
           //  'name'=>$params['name'], 
             'client_id'=>$params['client'], 
            // 'contact_id'=>$params['contact_id'],
             'start_date'=>date('Y-m-d',strtotime($params['start_date'])),  
             'start_time'=>$params['start_time'],  
             'end_date'=>  date('Y-m-d',strtotime($params['end_date'])),
             'end_time'=>$params['end_time'], 
             'state'=>$params['state'], 
             'universe'=>$params['universe'],  
             'total_contact'=>$params['total_contact'],  
             'caller_id'=>$params['caller_id'],  
             'goal_type'=>$params['goal_type'],  
             'goal_note'=>$params['goal_note'], 
             'number_of_question'=>$params['number_of_question'], 
             'create_by'=>$_SESSION['username'],
        ); 
            if (!$this->db->insert('po_parameter', $insert_po_param)) {
                $this->db->delete('po', 'id=' . $po_id);
                return $this->failed("Unable to add new project.");
            }else {
                  $insert_po_manage = array( 
                        'po_id'=>$po_id,
                        'campaign_note'=>$params['campaign_note'], 
                        'center_note'=>$params['center_note'], 
                        'manager_note'=>$params['manager_note'], 
                        'email_list'=>serialize($params['email_list']), 
                        'allow_download'=>isset($params['allow_download'])?$params['allow_download']:1, 
                        'allow_report'=>isset($params['allow_report'])?$params['allow_report']:2, 
                        'allow_grab_report'=>isset($params['allow_grab_report'])?$params['allow_grab_report']:1, 
                        'allow_blank_question_zero'=>isset($params['allow_blank_question_zero'])?$params['allow_blank_question_zero']:1,
                        'allow_disable_logic'=>isset($params['allow_disable_logic'])?$params['allow_disable_logic']:1, 
                        'custom_status'=>isset($params['custom_status'])?$params['custom_status']:"", 
                        'custom_databack'=>isset($params['custom_databack'])?$params['custom_databack']:"", 
                        'customer_response_map'=>isset($params['customer_response_map'])?$params['customer_response_map']:"", 
                        'customer_databack_fields'=>isset($params['customer_databack_fields'])? serialize($params['customer_databack_fields']):serialize(""),
                        //'misc_field_databack'=>$params['misc_field_databack'],
                        'status'=>isset($params['status'])?$params['status']:'N', 
                      //  'report_template'=>$params['report_template'], 
                        'manager_id'=>isset($params['manager_id'])?$params['manager_id']:1, 
                        'create_by'=>$_SESSION['username'],
                    ); 
                    
                if (!$this->db->insert('po_management', $insert_po_manage)) {
                $this->db->delete('po_parameter', 'po_id=' .$po_id);
                return $this->failed("Unable to add new project.");
                }else {
                    //Abby 03-20 Inserting Goal information on po_goal
                   /* $arr_day=array($params['day_checked_1'],$params['day_checked_2'],$params['day_checked_3'],$params['day_checked_4'],$params['day_checked_5'],$params['day_checked_6'],$params['day_checked_7']) ; */
                    
                      $arr_day = array();       
                    for($x =1; $x<=7; $x++){
                        isset($params['day_checked_'.$x]) ? $arr_day=array_merge($arr_day,array($x)) :  ''; 
                    }
                   
                    /*print_r($arr_day);
                    die();*/
                     $insert_po_goal = array(
                         'po_id'=>$po_id,
                         'goal_type'=>$params['goal_type'],
                         'goal_days_of_week'=>implode("-",$arr_day),
                         'goal_value'=>$params['goal_value'],
                          );    
                    //Abby 03-20 Inserting Goal information on po_goal
                        if (!$this->db->insert('po_goal', $insert_po_goal)) {
                            $this->db->delete('po_management', 'po_id=' . $po_id);
                            $this->db->delete('po_parameter', 'po_id=' .$po_id);
                            return $this->failed("Unable to add new project.");
                            //Abby 03-20 Inserting Goal information on po_goal
                   }else{ 
                     $params= array(
                                    'client_id'=>$params['client'],
                                    'value'=> $po_no,
                     );
                     $this->update_po_counter($params);   
                     $datetime=$this->get_server_date();
                    // $this->save_to_action_logs('Registered new project order ' . $client_code.sprintf('%06d',$po_no) ,'PO',$datetime,$_SESSION['username'],$po_id);
               
                    $this->create_po_transaction_tables($po_id);
                }      
            } 
        }
        
             //Email notification           
             $cmd = "select po_no from po where id ={$po_id}";
             $rs = $this->db->fetchRow($cmd);
            $email = $this->get_email_by_username($_SESSION['username']);
            $body="<br><br>Greetings!<br><br>
            PO {$po_name} has been created by {$_SESSION['username']}. <br> <br> Thanks,<br> PBS Admin"; 
            foreach($email as $s){   
                $this->send_mail_notification($body,$s['email_address'],"PBS Notification [PO#{$rs['po_no']}]");                                                           }
            $response = array(
                'message' => "New project has been added.",
            );                                                              
            return $this->success($response);
        }         
      }
    }
 public function delete_projecttemplate($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('project_template', $values, 'id='.$id);
        $response = array(
            'message' => "Project Template has been deleted.",
        );
        return $this->success($response);
    }   
 public function get_project_template_byid($params)
  {
        
        $record = $this->db->fetchAll("select id, name, client_id, report_type_id, is_auto_call, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status , create_date, create_by, update_date, update_by from project_template  WHERE  id=".$params['id']);
        return $this->success($record);
  }  
  public function get_project_template_code_byid($params)
  {
      
        $record = $this->db->fetchAll("select     id, project_template_id, type, status_code, pay_rate, status, create_date, create_by, update_date, update_by from project_template_code   WHERE project_template_id=".$params['id']);
        return $this->success($record);
  }     
    
public function edit_projecttemplate($params)
{
    $id =$params['txtID'];
    $update_data = array(
        'name'=>$params['txtTemplateName'], 
        'client_id'=>$params['client_name_temp'],
        'report_type_id'=>$params['report_type'],
        'is_auto_call' =>  isset($params['is_auto_call'])=='on'? 1: 0,
        'update_by'                   =>$_SESSION['username'],
        'update_date'                 =>date('Y-m-d H:i:s'), 
    );                      

     if (!$this->db->update('project_template', $update_data,'id=' . $id)) {
        return $this->failed("Unable to update project template.");
    } else {
       $this->db->delete('project_template_code', 'project_template_id=' . $id);
        $cnt=count($_POST['entity_type']);
        $project_template_codes = array(); 
        for ($i = 0; $i < $cnt; $i++) {
        $project_template_codes[] = array(
                    'project_template_id'=>$id,
                    'status_code'=>$_POST['txtStatus'][$i],
                    'type'=>$_POST['entity_type'][$i],
                    'pay_rate'=>$_POST['txtPayRate'][$i],
                    'create_by'=>$_SESSION['username'],
                );
        }
       
               if (!$this->db->batchInsert('project_template_code', $project_template_codes)) {
                    return $this->failed('Unable to save project template code.');
               }
        
        
        $response = array(
            'message' => "Project Template has been updated.",
        );

        return $this->success($response);
    }           
} 

 public function add_projecttemplate($params){
        
      
        $insert_data = array(
            'name'=>$params['txtTemplateName'], 
            'client_id'=>$params['client_name_temp'],
            'report_type_id'=>$params['report_type'],
            'is_auto_call' =>  isset($params['is_auto_call'])=='on'? 1: 0,

             
        );                     
      
        if (!$this->db->insert('project_template', $insert_data)) {
            return $this->failed("Unable to add new project template.");
        } else {
           
            
            
            
        $template_id=$this->db->lastInsertId();
        $cnt=count($_POST['entity_type']);
        $project_template_codes = array(); 
        for ($i = 0; $i < $cnt; $i++) {
        $project_template_codes[] = array(
                    'project_template_id'=>$template_id,
                    'status_code'=>$_POST['txtStatus'][$i],
                    'type'=>$_POST['entity_type'][$i],
                    'pay_rate'=>$_POST['txtPayRate'][$i],
                    'create_by'=>$_SESSION['username'],
                );
        }
       
               if (!$this->db->batchInsert('project_template_code', $project_template_codes)) {
                    return $this->failed('Unable to save project template code.');
               }
            $response = array(
                'message' => "New project template has been added.",
            );                                                              
            return $this->success($response);
        }         
    }
    
  public function get_project_template()
    {
        $cmd="select pt.id, pt.name as template_name, pt.client_id, pt.report_type_id,rt.name as report_type,CASE pt.is_auto_call WHEN 1 THEN 'YES' WHEN '0' THEN 'NO'  END as is_auto_call, 
            CASE pt.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,pt.create_date, pt.create_by, pt.update_date, pt.update_by,
            c.name as client_name
            from project_template pt
            inner join client  c on pt.client_id = c.id
            inner join report_type rt on pt.report_type_id = rt.id 
            where pt.status='A' AND pt.client_id={$_SESSION['client_id']}";
  
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
   
        
   public function delete_client($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('client', $values, 'id='.$id);
        $response = array(
            'message' => "Client has been deleted.",
        );
        return $this->success($response);
    }
    
    public function delete_user($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
                        
                      
                        
        $this->db->update('users', $values, 'id='.$id);
        $response = array(
            'message' => "User has been deleted.",
        );
        return $this->success($response);
    }
    
    
   public function get_client()
    {
        $cmd="select id,client_code,name,last_po_no,street,city,state,zip,phone,fax,notes,default_report_status,
        databack_status,basket_notification_status,databack_fields, 
        allow_co_fields,co_notification_status,schedule_notification_status,schedule_access,show_discount_code,
        client_logo,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,create_date,create_by,update_date,update_by
        from client  where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    public function get_client_list($id=0)
    {
        if($id==0){
            $cmd="select id,name from client where status = 'A'";
        }else{
                $cmd="select id,name from client where status = 'A' and id = {$id}";       
           
        }
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_center_list()
    {
        $cmd="select id,name from call_center  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);    
        return $this->success($result);
    }
    public function get_center_list_user()
    {
        $cmd="select id,name from call_center  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);    
        return $result;
    }
     public function get_contact_list()
    {
        $cmd="select id, concat(first_name,last_name) as name from contact   where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_goal_type()
    {
        $cmd="select  id,name, create_date, create_by,update_date,update_by from goal_type ";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
     public function get_states_list()
    {
        $cmd="select id, name from states";
       
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    public function get_po_list()
    {
        $cmd="select id,name from po  where status = 'A'";
       
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
    public function get_po_template_list()
    {
        $cmd="select id,name from project_template  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
     public function get_report_type_list()
    {
        $cmd="select id,name from report_type  where status = 'A'";
        $result = $this->db->fetchPairs($cmd);
        return $this->success($result);
    }
    
  public function get_client_byid($params)
  {
        $record = $this->db->fetchAll("select id,client_code,name,last_po_no,street,city,state,zip,phone,fax,notes,default_report_status,
                databack_status,basket_notification_status,databack_fields, 
                allow_co_fields,co_notification_status,schedule_notification_status,schedule_access,show_discount_code,
                client_logo,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status,create_date,create_by,update_date,update_by
                from client  WHERE id=".$params['id']);
        return $this->success($record);
  }  
   public function edit_client($params)
   {
        $id =$params['txtID'];
        $update_data = array(
            'name'                        =>$params['txtClientName'], 
            'client_code'                 =>$params['txtClientCode'],
            'last_po_no'                  =>$params['txtLastPO'], 
            'street'                      =>$params['txtAddress'], 
            'city'                        =>$params['txtCity'], 
            'state'                       =>$params['txtState'], 
            'zip'                         =>$params['txtZipCode'], 
            'phone'                       =>$params['txtPhone'], 
            'fax'                         =>$params['txtFax'], 
            'notes'                       =>$params['txtNote'], 
            'default_report_status'       =>$params['report_ready'], 
            'databack_status'             =>$params['data_back'], 
            'basket_notification_status'  =>$params['data_basket_notif'], 
            'databack_fields'             =>serialize($params['data_fields']), 
            'allow_co_fields'             =>serialize($params['allow_fields']),
            'co_notification_status'      =>$params['co_notif'],
            'schedule_notification_status'=>$params['sched_notif'], 
            'schedule_access'             =>$params['sched_access'], 
            'show_discount_code'          =>$params['discount_code'],
            'client_logo'                 =>'C:', 
            'status'                      =>'A', 
            'update_by'                   =>$_SESSION['username'],
            'update_date'                 =>date('Y-m-d H:i:s'), 
    );                      
       
         if (!$this->db->update('client', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update client.");
        } else {
            $response = array(
                'message' => "Client has been updated.",
            );

            return $this->success($response);
        }           
    } 
   public function register_client($params){
       $u_param = array(
            'username'=>$params['txtClientUsername'],
       );
        if ($this->check_user_if_exist($u_param)) {
                                                             
                return $this->failed('Username already exist.');
                die();
            } else{
                
            
     
       
        $insert_data = array(
            'name'                        =>$params['txtClientName'], 
            'client_code'                 =>$params['txtClientCode'],
            'last_po_no'                  =>$params['txtLastPO'], 
            'street'                      =>$params['txtAddress'], 
            'city'                        =>$params['txtCity'], 
            'state'                       =>$params['txtState'], 
            'zip'                         =>$params['txtZipCode'], 
            'phone'                       =>$params['txtPhone'], 
            'fax'                         =>$params['txtFax'], 
            'notes'                        =>$params['txtNote'], 
            'default_report_status'       =>$params['report_ready'], 
            'databack_status'             =>$params['data_back'], 
            'basket_notification_status'  =>$params['data_basket_notif'], 
            'databack_fields'             =>serialize($params['data_fields']), 
            'allow_co_fields'             =>serialize($params['allow_fields']),
            'co_notification_status'      =>$params['co_notif'],
            'schedule_notification_status'=>$params['sched_notif'], 
            'schedule_access'             =>$params['sched_access'], 
            'show_discount_code'          =>$params['discount_code'],
            'client_logo'                 =>'C:', 
            'status'                      =>'A', 
            'create_by'                   =>$_SESSION['username'], 
        );                     
       
        if (!$this->db->insert('client', $insert_data)) {
            return $this->failed("Unable to add new client.");
        } else {
                $client_id = $this->db->lastInsertId();
                 $insert_data = array(
                    'fname'         => $params['txtClientName'],
                    'lname'         => $params['txtClientName'],
                    'username'      => $params['txtClientUsername'],
                    'user_type'     => 3,
                    'is_client'     => 1,
                    'client_id'     => $client_id,
                    'created_by'    => $_SESSION['username'],
                    'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['txtClientPassword'], sha1(md5($this->salt))),  
                );
           $this->db->insert('users', $insert_data);
           $response = array(
                'message' => "New client has been added.",
            );                                                              
            return $this->success($response);
        } 
       }        
    }
    
   public function delete_callcenter($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('call_center', $values, 'id='.$id);
        $response = array(
            'message' => "Contact has been deleted.",
        );
        return $this->success($response);
    }   
    
 
  public function delete_contact($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array(
                            'status'=>'D',
                            'update_by'=>$_SESSION['username'],
                            'update_date'=>date('Y-m-d H:i:s'), 
                        );
        $this->db->update('contact', $values, 'id='.$id);
        $response = array(
            'message' => "Contact has been deleted.",
        );
        return $this->success($response);
    }   
    
  
    public function edit_contact($params){
        $params = array_map('trim', $params);
        $id =$params['txtID'];
        $params = array_map('trim', $params);
             $update_data = array(
                    'first_name'=>$params['txtFirstName'], 
                    'last_name' =>$params['txtLastName'],
                    'city'=>$params['txtCity'],
                    'state'=>$params['txtState'],
                    'zip'=>$params['txtZipCode'],  
                    'phone'=>$params['txtPhone'],
                    'ext'=>$params['txtExt'],
                    'fax'=>$params['txtFax'],
                    'email'=>$params['txtEmail'],
                    'login_id'=>$params['txtUsername'],
                    'password' =>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtPassword'], sha1(md5($this->salt))),
                    'notes'=>$params['txtNote'],
                    'status'=>'A',
                    'update_by'=>$_SESSION['username'],
                    'update_date'=>date('Y-m-d H:i:s'), 
        );                      
       
         if (!$this->db->update('contact', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update contact.");
        } else {
            $response = array(
                'message' => "Contact has been updated.",
            );

            return $this->success($response);
        }           
    }
    
 
     public function get_contact()
    {
        $cmd="select id, client_id, login_id, password, first_name, last_name, street, city, state, zip, phone, ext, fax, email, 
              notes,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status, create_date, create_by, update_date, update_by from contact where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }

  public function get_contact_byid($params)
  {
        
        $record = $this->db->fetchAll("select     id, client_id, login_id, password, first_name, last_name, street, city, state, zip, phone, ext, fax, email, 
                                        notes,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status, create_date, create_by, update_date, update_by from contact  WHERE id=".$params['id']);
        
        return $this->success($record);
  } 
 
   public function register_contact($params){
        
        $params = array_map('trim', $params);
        $insert_data = array(
            'first_name'=>$params['txtFirstName'], 
            'last_name' =>$params['txtLastName'],
            'city'=>$params['txtCity'],
            'state'=>$params['txtState'],
            'zip'=>$params['txtZipCode'],  
            'phone'=>$params['txtPhone'],
            'ext'=>$params['txtExt'],
            'fax'=>$params['txtFax'],
            'email'=>$params['txtEmail'],
            'login_id'=>$params['txtUsername'],
            'password' =>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtPassword'], sha1(md5($this->salt))),
            'notes'=>$params['txtNote'],
            'status'=>'A',
            'create_by'=>$_SESSION['username'],
        );                     
       
        if (!$this->db->insert('contact', $insert_data)) {
            return $this->failed("Unable to add new contact.");
        } else {
           
            $response = array(
                'message' => "New contact has been added.",
            );                                                              
           
            
            return $this->success($response);
        }         
    }
    
  
    
   public function register_callcanter($params){
        
        $params = array_map('trim', $params);
        $insert_data = array(
            'name'=>$params['txtCallCenterName'], 
            'center_code'=>$params['txtCallCenterCode'],  
            'ftp_site'=>$params['txtFTPSite'],   
            'ftp_login'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPLogin'], sha1(md5($this->salt))),  
            'ftp_password'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPPassword'], sha1(md5($this->salt))),   
            'status'=>'A', 
            'create_by'=>$_SESSION['username'], 

        );                     
       
        if (!$this->db->insert('call_center', $insert_data)) {
            return $this->failed("Unable to add new call center.");
        } else {
           
            $response = array(
                'message' => "New call center has been added.",
            );                                                              
            return $this->success($response);
        }         
    }
   public function edit_callcanter($params){
        $params = array_map('trim', $params);
        $id =$params['txtID'];
        $params = array_map('trim', $params);
            $update_data = array(
                'name'=>$params['txtCallCenterName'], 
                'center_code'=>$params['txtCallCenterCode'],  
                'ftp_site'=>$params['txtFTPSite'],   
                'ftp_login'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPLogin'], sha1(md5($this->salt))),  
                'ftp_password'=>$this->hideit->encrypt(sha1(md5($this->salt)), $params['txtFTPPassword'], sha1(md5($this->salt))),   
                'status'=>'A', 
                'update_by'=>$_SESSION['username'],
                'update_date'=>date('Y-m-d H:i:s'), 

        
            );                     
       
         if (!$this->db->update('call_center', $update_data,'id=' . $id)) {
            return $this->failed("Unable to update call center.");
        } else {
            $response = array(
                'message' => "Call Center has been updated.",
            );
/*         Add Action Logs*/
            return $this->success($response);
        }           
    }
    //PP 2015/07/27
    public function get_callcenter()
    {
        $cmd="select id, name, center_code, ftp_site,ftp_login,ftp_password, create_date, create_by, update_date, update_by, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status from call_center  where status = 'A'";
        
        $result = $this->db->fetchAll($cmd);
        return $this->success($result);
    }
    public function get_callcenter_byid($params)
  {
        
        $record = $this->db->fetchAll("select id, name, center_code, ftp_site,ftp_login,ftp_password, create_date, create_by, update_date, update_by, CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status from call_center WHERE id=".$params['id']);
        return $this->success($record);
  }  
    public function get_user_types() {
        $user_types = $this->db->fetchPairs('select id,description from user_types');
        return $user_types;
    }
    public function get_user_types_exclude_client() {
        $user_types = $this->db->fetchPairs('select id,description from user_types where id<>3');
        return $user_types;
    }
    
    public function get_user_types_selected($user_type_id) {
        $user_types = $this->db->fetchPairs('select id,description from user_types where created_by='.$user_type_id);
        return $user_types;
    }
    
    public function get_user_type_by_username($username){
        $sql = "SELECT ut.* FROm users u LEFT JOIN user_types ut ON u.user_type=ut.id WHERE u.username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchAll($sql);
        return $this->success($result);  
    }
    
    public function get_user_type_by_username_ex($username){
        
        $result = $this->db->fetchRow("SELECT ut.description FROm users u LEFT JOIN user_types ut ON u.user_type=ut.id WHERE u.username='{$username}'");
        return $result['description'];  
    }
    
    public function get_user_info($params){
        $username = $params['username'];
        $return_value = $params['return_value'];
        
        $sql = "select * from users  WHERE username='{$username}'";
        //print($sql);
        //die();
        $result = $this->db->fetchRow($sql);
        return $result[$return_value];  
    }


    
    
    
    //=============LOOKUP FUNCTIONS=====================    
    
    public function validate_session($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']) && strlen($params['session_id']) == 32)) {
            return $this->failed('Invalid or expired session id');
        } else {
            $chk = $this->db->fetchRow('select * from sessions where session_id='. $this->db->db_escape($params['session_id']));
            if (!(isset($chk['session_id']))) {
                return $this->failed('Invalid or expired session id');
            } elseif (isset($chk['logout_date']) && $chk['logout_date'] != '') {
                return $this->failed('Invalid or expired session id');
            //} elseif (strtotime($chk['last_seen']) + (3600) < time()) {
            } elseif (strtotime($chk['last_seen']) + (600) < time()) {
                return $this->failed('Invalid or expired session id');
            } else {
                $values = array("last_seen" => date('Y-m-d H:i:s'));
                $this->db->update('sessions', $values, 'session_id=' . $this->db->db_escape($params['session_id']));
                return $this->success('session id valid');
            }
        }
    }
    //params: user_id
    public function change_password($params) {
        $params = array_map('trim', $params);
        if (!isset($params['username'], $params['old_pw'], $params['new_pw'])) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['username'] != '' && $params['old_pw'] !='' && $params['new_pw'] != '')) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['old_pw'] != $params['new_pw'])) {
            return $this->failed('Old and new password should not be the same');
        } else {
            $user_check = $this->db->fetchRow('select * from users where username='. $this->db->db_escape($params['username']) .' and password=' . $this->db->db_escape(md5($this->salt . $params['old_pw'] . $this->salt )));
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid old password');
            } else {
                $update_data = array(
                    'password' => md5($this->salt . $params['new_pw'] . $this->salt),
                );
                if (!($this->db->update('users', $update_data, 'id=' . $user_check['id']))) {
                    return $this->failed('Unable to change user password');
                } else {
                    //return $this->failed('Successfully changed user password');
                    return $this->success('Successfully changed user password');
                }
            }
        }
    }
    //params: user_id
    public function update_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['user_id'],$params['fname'],$params['lname'], $params['email_address'], $params['mobile_number'], $params['address'], $params['user_type']))) {
            return $this->failed('Missing required fields');
        }else if($params['mobile_number']==""){
            return $this->failed('Missing required mobile number');
        } else {
            $user_check = $this->db->fetchRow('select * from users where id='. $this->db->db_escape($params['user_id']));
            if (!(isset($user_check['id'])))  {
                return $this->failed('User does not exist');
            } else {
                //echo $params['user_id'];
                $update_data = array(
                    'fname' => $params['fname'],
                    'lname' => $params['lname'],
                    'email_address' => $params['email_address'],
                    'mobile_number' => $params['mobile_number'],
                    'address' => $params['address'],
                    'user_type' => $params['user_type'],
                    'status' => $params['status'],
                );
                
                if (!($this->db->update('users', $update_data, 'id=' . $user_check['id']))) {
                    return $this->failed('Unable to change user information');
                } else {
                    //return $this->failed('Successfully changed user password');
                    return $this->success('Successfully changed user information');
                }
                
            }
        }
    
    }
    public function get_user_by_id($params) 
    {
        $query = "select u.user_center, u.is_center, CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type WHERE u.id = ".$params['id'];

        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
 /*   //params: user_id
    public function delete_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['user_id']) && is_numeric($params['user_id']) && $params['user_id'] > 0)) {
            return $this->failed('Invalid user ID');
        } else {
            $user_check = $this->db->fetchRow('select * from users where id='. $this->db->db_escape($params['user_id']));
            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
               // if ($this->get_users_customers($user_check['username'])) {
                 //   return $this->failed('There are existing customers registered under this agent/user.');
               // } else {
                    $this->db->delete('users', 'id=' . $params['user_id']);
                    return $this->success('Successfully deleted user');
                //}
            }
        }
    }  */
    
    

    
    
    
      

    
    public function get_users_detailed() {
        $query = "select CASE u.status WHEN 'A' THEN 'ACTIVE' WHEN 'D' THEN 'DELETED'  END as status_text ,u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description " ;             
        $query .= "user_type_desc from users u ";
        $query .= "inner join user_types ut on ut.id=u.user_type where u.status='A'";
       /* print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_detailed_selected($user_type_id) {
        $query = 'select u.status, u.id,fname,lname,username,u.email_address,mobile_number,address,user_type,ut.description ' ;                $query .= 'user_type_desc from users u ';
        $query .= 'inner join user_types ut on ut.id=u.user_type ';
        $query .= 'where ut.created_by=' . $user_type_id;
/*        print_r($query);
        die();*/
        $users = $this->db->fetchAll($query);
        
        return $this->success($users);
    }
    
    public function get_users_list()
    {
        $query="SELECT id,username,password,CASE status WHEN 'A' THEN 'ACTIVE' WHEN 'I' THEN 'INACTIVE' ELSE 'PENDING' END as status";
        $query.=",date_created,created_by,fname";
        $query.=",lname,email_address,mobile_number,address,user_type,customers_id,agent_id FROM users";
        $users= $this->db->fetchAll($query);
        $this->success($users); 
        $response = $this->get_response(); 
        return    $response;
    }
    
    

   
    public function generate_password() {
        return substr(md5(uniqid(time(), true)), 5,8);
    }

    
    
    public function reset_password_notification($params)
    {
        require_once 'class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->isSendmail();//Set who the message is to be sent from
        $mail->From = 'flashcash@omniflashcash.com';
        $mail->FromName = 'FlashCash Reset Password';
        $mail->addAddress($params['email_address']);
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject = 'FlashCash: Your auto-generated password';
        $mail->Body    = 'Username: <b>' . $params['username'] . '</b>, New Password: <b>' . $params['generated_password'].'</b>';
        if (!$mail->send()) {
            return $this->failed('Unable to send email notification');
        } else {
            return $this->success('Message sent');
        } 
    }

    public function reset_user_password($params)
    {   
        $params = array_map('trim', $params);
        if (!(isset($params['user_id'],$params['username'],$params['email_address']))) {
            return $this->failed('Missing required fields');
        }elseif (!($params['user_id'] != ''))
        {
            return $this->failed('Missing required fields');
        }else{
            $generated_password = $this->generate_password();
            $update_data = array( 'password' => md5($this->salt . $generated_password . $this->salt));
            if (!($this->db->update('users', $update_data, 'id=' . $params['user_id']))) {
                return $this->failed('Unable to reset your password');
            }else
            {
               $notify_param =array('email_address' => $params['email_address'],'username' => $params['username'],'generated_password' => $generated_password);
               $this->reset_password_notification($notify_param);
               return $this->success('Successfully reset password');
            }
        }
    }
    
                                 
    
     
    protected function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } else {
                    if (file_exists($dst . '/' . $file)) @unlink($dst . '/' . $file);
                    copy($src . '/' . $file, $dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }
         
    
                                         

    //params: username, password
    public function login($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['username'], $params['password']))) {
            return $this->failed('Invalid username or password');
        } elseif (!($params['username'] != '' && $params['password'] != '')) {
            return $this->failed('Invalid username or password');
        } else {
            $user_check = $this->db->fetchRow("select  u.*,ut.description as user_type_desc from users u inner join user_types ut on u.user_type=ut.id where u.username=". $this->db->db_escape($params['username']) ." and password=" . $this->db->db_escape($this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt)))) . " and status='A'");
            

            if (!(isset($user_check['id']))) {
                return $this->failed('Invalid username or password');
            } else {
                $permissions = $this->get_user_permissions($user_check['id'], $user_check['user_type']);
                $session_id = $this->create_session($user_check['id']);
                $response = array(
                    'SessionID' => $session_id,
                    'Permissions' => implode(';', $permissions),
                    'user_type' => $user_check['user_type'],
                    'user_type_desc' => $user_check['user_type_desc'],
                    'fname' => $user_check['fname'],
                    'lname'  => $user_check['lname'],
                    'user_center'=> $user_check['user_center'],
                    'client_id'=>$user_check['client_id'],
                );
                //print_r($response);
                //die();
                return $this->success($response);
            }
        }
    }
    

    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = $this->db->fetchRow('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
    
    protected function create_session($user_id) {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
        );
        $this->db->insert('sessions', $insert_data);
        return $session_id;        
    }
    

    
    //user_id
    public function get_user_permissions($user_id, $user_type_id) {
        $permissions = $this->db->fetchPairs('select p.id,r.resource from permissions p inner join resources r on p.resource_id=r.id where p.user_id=' . $this->db->db_escape($user_id));
        $permissions2 = $this->db->fetchPairs('select ut.id,r.resource from user_templates ut inner join resources r on ut.resource_id=r.id where ut.user_type_id=' . $this->db->db_escape($user_type_id));
        $permissions = array_flip(array_merge(array_flip($permissions), array_flip($permissions2)));
          
        if (is_array($permissions) && count($permissions) > 0) {
            return $permissions;
        }
    }

    

    
    

    //params: session_id
    public function logout($params) {//note: session maintenance is not yet implemented in backend.php, client must implement its own
        $params = array_map('trim', $params);
        if (!(isset($params['session_id']))) {
            return $this->failed('Invalid session ID');
        } else {
            $check = $this->db->fetchRow('select * from sessions where logout_date is null and session_id=' . $this->db->db_escape($params['session_id']));
            if (!(isset($check['id']))) {
                return $this->failed('Invalid session ID');
            } else {
                $update_data = array(
                    'logout_date' => date('Y-m-d H:i:s'),
                );
                if (!$this->db->update('sessions', $update_data, 'id='. $check['id'])) {
                    return $this->failed('Unable to logout');
                } else {
                    return $this->success('Successfully logged out user.');
                }
            }
        }
    }

    
    protected function failed($message) {
        $this->response = array(
            'ResponseCode' => '0001',
            'ResponseMessage' => $message,
        );
        return false;
    }
    
    protected function success($message) {
        $this->response = array(
            'ResponseCode' => '0000',
            'ResponseMessage' => $message,
        );
        return true;
    }
    
    public function get_response() {
        return $this->response;
    }
    

    
    
    //params: username
     public function get_user_id($username) {
            $record = $this->db->fetchRow('select agent_id from users where username=' . $this->db->db_escape($username));
            if (!isset($record['agent_id'])) {
                return $this->failed('Unable to retrieve userId');
            } else {
                return $record['agent_id'];
            }
        
    }
    
   
     //params: description(user type)
       public function get_user_types_ByDesc($desc) {
            $record = $this->db->fetchRow('select id from user_types where description=' . $this->db->db_escape($desc));
            if (!isset($record['id'])) {
                return $this->failed('Unable to retrieve id');
            } else {
                return $record['id'];
            }
        
    }


      

    //params: payroll_cycle, loan_terms
    public function get_loan_term_calendar_value($payroll_cycle, $loan_term) {
            $sql = 'select value from loan_term_calendar where payroll_cycle=' . $this->db->db_escape($payroll_cycle);
            $sql .= " and loan_terms=" . $loan_term;
            $record = $this->db->fetchRow($sql);
            if (!isset($record['value'])) {
                return $this->failed('Unable to retrieve id');
            } else {
                return $record['value'];
            }
        
    }

      
//params: action_taken, action_module, action_date, action_by, remark
    public function save_to_action_logs($action_taken, $action_module, $action_date, $action_by, $remark) {
        if (!($action_taken != '' && $action_module != '' && $action_date != '' && $action_by != '')) {
            return $this->failed('Invalid parameters');
        } else {
            $insert_data = array(
                'action_taken'  => $action_taken,
                'action_module'            => $action_module,
                'action_date'              => $action_date,
                'action_by'         => $action_by,
                'remark'      => $remark,            
    );
            if (!$this->db->insert('action_logs', $insert_data)) {
                return $this->failed('Unable to save action_logs');
            } else {
                return $this->success('Successfully saved new action_logs');

            }
        }
    }
    

    
    public function get_server_date() {
        $result = $this->db->fetchRow('SELECT NOW() AS server_date');
        return $result['server_date'];
    } 
    
  

    
    public function delete_module($params)
    {
        $params = array_map('trim', $params);
        $id = $params['id'];     
        $values = array('deleted'=>1);
        $this->db->update('resources', $values, 'id='.$id);
        $response = array(
            'message' => "Successfully deleted module",
        );
        return $this->success($response);
    }   
    
    
    public function add_module($params)
    {
         $params = array_map('trim', $params);
         //print_r($params);
         //die();
         
         $insert_data = array(
              'resource' => $params['resource'],
              'description' => $params['description'],
              'date_created' => date('Y-m-d H:i:s'),
         );           
         
         
         if (!$this->db->insert('resources', $insert_data)) 
         {
            return $this->failed("Error creating module.");
         }else {
            $response = array(
                'message' => "Successfully created module.",
            );
            return $this->success($response);
         }   
    }

    
    //params: fname, lname, email_address, mobile_number, address, username, user_type
    public function register_user($params) {
        $params = array_map('trim', $params);
        if (!(isset($params['mobile_number'],$params['fname'], $params['lname'], $params['email_address'], $params['username'], $params['user_type'], $params['created_by']))) {
            return $this->failed('Missing required fields');
            die();
        } elseif (($params['mobile_number'] == '' && $params['fname'] == '' && $params['lname'] == '' && $params['email_address'] == '' && $params['username'] == '' && $params['user_type'] == '' && $params['created_by'] == '')) {
            return $this->failed('Missing required fields');
            die();
        }
         elseif (!strstr($params['email_address'], '@')) {
             return $this->failed('Invalid email address');
             die();
        }
       /* elseif (!($this->get_userid_by_email($params['email_address'])==-1)) {
            return $this->failed('Email has already been used.');
            die();
       
        }*/
        else {
            if ($this->get_user_details($params)) {

                return $this->failed('Username already exist');
                die();
            } else {
                
     
                $insert_data = array(
                    'fname'         => $params['fname'],
                    'lname'         => $params['lname'],
                    'email_address' => $params['email_address'],
                    'mobile_number' => $params['mobile_number'],
                    'username'      => $params['username'],
                    'user_type'     => $params['user_type'],
                    'created_by'    => $params['created_by'],
                    'is_center'     =>  $params['is_center'],
                    'password'      => $this->hideit->encrypt(sha1(md5($this->salt)),$params['password'], sha1(md5($this->salt))),  
                );
               
               
                if (!$this->db->insert('users', $insert_data)) {
                    return $this->failed('Unable to register new user');
                } else {
                    $response = array(
                        'message' => 'Successfully registered new user record',
                    );
                    
                    return $this->success($response);
                }
            }
        }
    }
    
    public function get_user_details($params) {
        $params = array_map('trim', $params);
        
        if (!(isset($params['username']) && trim($params['username']) != '')) {
         
            return $this->failed('Missing required parameter');
        } else {
            $row = $this->db->fetchRow("select * from users where status = 'A'  and  username=" . $this->db->db_escape($params['username']));
            if (!(isset($row['id']))) {
                return $this->failed('User account not found');
            } else {
                 
                return $this->success($row);
            }
        }
    }
    
    public function get_user_information($sessionid) 
    {
        $query = "SELECT u.* FROM users u
        LEFT JOIN sessions s ON u.id = s.user_id
        WHERE s.session_id = ".$this->db->db_escape($sessionid);
        $merchant = $this->db->fetchAll($query);
        return $this->success($merchant);
    }
    
     // ACL
    public function get_module_name($module_list = array(), $include_action = false)
    {
        if(count($module_list) > 0) {
            $return_arr = array();
            $query = "SELECT * FROM acl_modules WHERE id IN(".implode(',', $module_list).") AND main = 1";
            $module_list = $this->db->fetchAll($query);

            foreach($module_list as $module) {
                $return_arr[] = $module['module_class'];
                $return_arr[] = $module['main_module_index'];
                if($include_action)
                    $return_arr[] = $module['module_action'];
            }

            $result = array_unique($return_arr);
            return $result;
        } else {
            return FALSE;
        }
    }

    public function remove_module_buttons($class)
    {
        $module_btn_return = array();

        $all_module_btn = $this->db->fetchAll('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND with_button = 1');
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];
        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_access = $this->db->fetchRow($query);
        $module_access = explode(',', $module_access['module_access']);

        if($all_module_btn && $module_access) {
            foreach($all_module_btn as $btn) {
                if(in_array($btn['id'], $module_access)) {
                    $module_btn_return[$btn['module_action']] = '';
                } else {
                    $module_btn_return[$btn['module_action']] = 'display: none;';
                }
            }
        }

        return $module_btn_return;
    }

    public function get_all_modules()
    {
        $return_arr = array();
        $query = "SELECT * FROM acl_modules"; // check if it will be a 1d array if i just get the id instead of all
        $all_modules = $this->db->fetchAll($query);

        foreach($all_modules as $module)
            $return_arr[] = $module['id'];

        return $return_arr;
    }

    public function get_user_modules()
    {
        $user_info = $this->get_user_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['user_type'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            $module_list = explode(',', $module_list['module_access']);
            $all_modules = $this->get_all_modules();

            $unset_modules = array_diff($all_modules, $module_list);

            if(count($unset_modules) > 0)
                return $unset_modules;
            else
                return FALSE;
        }
    }

    public function get_user_access_list()
    {
        $user_info = $this->get_merchant_information($_SESSION['sessionid'], TRUE);
        $profile_id = $this->response['ResponseMessage'][0]['profile_id'];

        $query = "SELECT * FROM acl_profile WHERE deleted = 0 AND id = ".$profile_id;
        $module_list = $this->db->fetchRow($query);

        if($module_list) {
            return explode(',', $module_list['module_access']);
        } else {
            return array();
        }
    }

    public function get_module_id($class, $action)
    {
        $module_info = $this->db->fetchRow('SELECT * FROM acl_modules WHERE module_class = '.$this->db->db_escape($class).' AND module_action = '.$this->db->db_escape($action));

        if(!$module_info)
            return FALSE;

        $module_id = $module_info['id'];
        $user_list = $this->get_user_access_list();

        if(in_array($module_id, $user_list)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // ACL PROFILE
    public function get_all_profile($where = null)
    {
        $query = 'SELECT * FROM user_types WHERE deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $profile_list = $this->db->fetchAll($query);

        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }
    
    public function get_all_profile_selected($user_type_id)
    {
        $query = 'SELECT * FROM user_types WHERE deleted = 0 AND created_by ='. $user_type_id;
        $profile_list = $this->db->fetchAll($query);
        //print($query);
        //die();
        if(!$profile_list)
            return array();
        else
            return $profile_list;
    }    
    
    public function get_all_profile_access($id)
    {
        $query = 'SELECT resource_id FROM user_templates WHERE user_type_id =' . $id;
        $access_list = $this->db->fetchAll($query);

        if(!$access_list)
            return array();
        else
            return $access_list;
    }
    

    public function get_all_modules_list($where = null)
    {
        $query = 'SELECT * FROM resources where deleted = 0';

        if(!is_null($where))
            $query .= ' AND '.$where;

        $query .= ' ORDER BY description';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    public function get_all_modules_list_selected($user_type_id)
    {
        //$query = 'SELECT * FROM resources where deleted = 0';
        $query = 'SELECT r.* FROM resources r inner join user_templates ut on ut.resource_id = r.id';
        $query .= ' WHERE deleted = 0 and user_type_id = ' . $this->db->db_escape($user_type_id);         
        $query .= ' ORDER BY resource';

        $module_list = $this->db->fetchAll($query);

        if(!$module_list)
            return array();
        else
            return $module_list;
    }
    
    protected function is_profile_existing($profile_name)
    {
        $query = 'select * from user_types where description ='.$this->db->db_escape($profile_name);
        $check_profile = $this->db->fetchAll($query);
        if (count($check_profile)>0) {
           return true;
        } else {
           return false;
        }
           
    }

    public function add_profile($params)
    {
        if(count($params['module_access']) == 0 || !isset($params['module_access']))
            return $this->failed('Select a module');
            
        //print_r($params['module_access']);
        //die();

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }
        
        if($this->is_profile_existing($params['description'])){
            return $this->failed('Profile already exist!');
        }

        $data = array(
            'description'   => $params['description'],
            'created_by'    => $params['created_by']
        );

       if(!$this->db->insert('user_types', $data)) {
            return $this->failed("Unable to add profile");
        } else {
            //get user type id
            $id=$this->db->lastInsertId(); 
            //print_r($id);
            // delete all access from user_templates
            $this->db->delete('user_templates', 'user_type_id=' . $id);
            // insert new access rights
            foreach($params['module_access'] as $data) {
                $insert_data = array(
                    'resource_id'   => $data,
                    'user_type_id'  => $id,
                );
                $this->db->insert('user_templates', $insert_data);
            }
            return $this->success('Profile Saved');
        }
    }

    public function edit_profile($params)
    {
        if(!isset($params['module_access']) || count($params['module_access']) == 0)
            return $this->failed('You cant add a profile with no module');

        foreach($params['module_access'] as $data) {
            if(!is_numeric($data))
                return FALSE;
        }

        if(!is_numeric($params['id']))
            return FALSE;
        
        $id = $params['id'];
        // delete all access from user_templates
        $this->db->delete('user_templates', 'user_type_id=' . $id);
        // insert new access rights
        foreach($params['module_access'] as $data) {
            $insert_data = array(
                'resource_id'   => $data,
                'user_type_id'  => $id,
            );
            $this->db->insert('user_templates', $insert_data);
        }
        return $this->success('Profile updated');
    }

    public function delete_profile($profile_id = null)
    {
        if(is_null($profile_id) || !is_numeric($profile_id))
            return $this->failed('Invalid ID');

        if($this->db->update('acl_profile', array('deleted' => 1), 'id='.$this->db->db_escape($profile_id))){
            $response = array(
                'message' => "Successfully deleted module",
            );
            return $this->success($response);
        }
        else{
            $response = array(
                'message' => "Unable to delete profile",
            );
            return $this->failed($response);
        }
    }
    
    public function get_all_resources()
    {
        $query = 'SELECT * FROM resources where deleted = 0';
        $query .= ' ORDER BY id';
        //print_r($query);
        //die();
        $module_list = $this->db->fetchAll($query);
        return $this->success($module_list);
    }
    
    public function update_module($params)
    {
        $params = array_map('trim', $params);
        $update_data = array(
           'resource'      => $params['value'], 
           'description'    => $params['description'],
        );
        if (!($this->db->update('resources', $update_data, 'id=' . $params['id']))) {
            $response = array(
                'success' => false,
                'message' => 'Unable to update module.',
            );
            return $this->failed($response);    
        } else {
            $response = array(
                'success' => true,
                'message' => 'Successfully updated module',
            );
            return $this->success($response);
        }
    }
    
     public function get_pan_key() {                                                     
        //$query = $this->db->select('key')->from('keys')->order_by('timestamp', 'desc')->get();
        $query = $this->db->fetchRow("SELECT `key` FROM `keys` ORDER by `timestamp` desc");
        if (!$query) {
            //$key = $query->result_array();
            $dek_enc = $query['key'];
            $iv = sha1('aes-256-cbc');
            //$kek = file_get_contents('http://192.168.2.16/web/i.php?token=cfbe176207b80774e8911c10893f5a0f');
            $kek="cfbe176207b80774e8911c10893f5a0f";
            $dek_plain = $this->hideit->decrypt($kek, $dek_enc, $iv);
            return $dek_plain;
        } else {
            return '';
        }
    }  
                    
    
    
}
