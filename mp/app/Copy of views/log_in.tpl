<!-- Main content -->
 <div class="container">  
    <section class="content">
        <div class="row">
            <div class="login-position">
                <div class="col-lg-8 col-sm-8">
                    <img src="{$webroot_resources}/images/logo-login.png" />
                    <div class="login-logo-subtitle">Your online reservation solution for better service to make <br />your business grow</div>
                </div>
             
                 <div class="col-lg-4 col-sm-4">
                       <div class="login-box-header"> Sign in</div>
                       <div class="login-box-body"> 
                           
                        
                            <form action="" method="post">
                                <div class="form-group has-feedback">
                                    <input type="text" id="username" name="username" class="form-control login-input" placeholder="Username">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password"name="password" id="password" class="form-control login-input" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                     <div class="col-xs-12">
                                        <div class="checkbox icheck">
                                            <label>
                                                <input type="checkbox"> Remember Me
                                            </label>
                                        </div>
                                     </div>

                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat"> Sign In</button>
                                        
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-xs-8">
                                        <div class="ta-right">
                                            <a href="forgotpassword.php">I forgot my password</a>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                         </div>
                     <!-- /.login-box-body -->
                </div>
            </div>
        </div>
    </section>
 </div>

