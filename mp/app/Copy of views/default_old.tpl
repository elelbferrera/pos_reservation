<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rewards | Registration</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{$webroot_resources}/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{$webroot_resources}/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{$webroot_resources}/css/ionicons.min.css">
    <link rel="stylesheet" href="{$webroot_resources}/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{$webroot_resources}/dist/css/AdminLTE.css">
    <!-- AdminLTE Skin -->
    <link rel="stylesheet" href="{$webroot_resources}/dist/css/skins/skin-blue.css">
        <!-- DataTables -->
  	<link rel="stylesheet" href="{$webroot_resources}/plugins/datatables/dataTables.bootstrap.css">
    
    <!-- CSS files needed for dashboard sample -->
    
    <!-- Morris chart -->
  	<!-- <link rel="stylesheet" href="{$webroot_resources}/plugins/morris/morris.css"> -->
    <!-- jvectormap -->
    <link rel="stylesheet" href="{$webroot_resources}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{$webroot_resources}/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{$webroot_resources}/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{$webroot_resources}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    
    <!-- iCheck -->
  	<link rel="stylesheet" href="{$webroot_resources}/plugins/iCheck/flat/blue.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{$webroot_resources}/assets/css/custom.css">
    
    
    <!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="{$webroot_resources}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<!-- <script src="{$webroot_resources}/js/jquery-ui.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
{literal}
  $.widget.bridge('uibutton', $.ui.button);
{/literal}
</script>


	<script src="{$webroot_resources}/plugins/jQueryUI/jquery-ui.min.js"></script>
 	<!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/less.min.js"></script>  -->
    
    <!-- JQuery -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/jquery.min.js"></script> -->
    
    <!-- JQueryUI -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/jquery-ui/js/jquery-ui-1.9.2.custom.min.js"></script> -->
    
    <!-- JQueryUI Touch Punch -->
    <!-- small hack that enables the use of touch events on sites using the jQuery UI user interface library -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script> -->
    
    
    <!-- Modernizr -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/modernizr.js"></script> -->
    
    <!-- Bootstrap -->
    <!-- <script src="{$webroot_resources}/common/bootstrap/js/bootstrap.min.js"></script> -->
    <!-- <script src="{$webroot_resources}/bootstrap/js/bootstrap.min.js"></script> -->
    
    <!-- SlimScroll Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/other/jquery-slimScroll/jquery.slimscroll.min.js"></script> -->
    
    <!-- Common Demo Script -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/demo/common.js?1369753442"></script>
    <script src="{$webroot_resources}/common/theme/scripts/demo/form_wizards.js"></script>
    <script src="{$webroot_resources}/common/theme/scripts/demo/notifications.js"></script> -->
    
    <!-- Holder Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/other/holder/holder.js"></script> -->
    
    <!-- Uniform Forms Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min.js"></script> -->

    <!-- Bootstrap Extended -->
    <!-- <script src="{$webroot_resources}/common/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>   
    <script src="{$webroot_resources}/common/bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/jasny-bootstrap/js/bootstrap-fileupload.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/bootbox.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/alertify.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/bootstrap-wysihtml5/js/wysihtml5-0.3.0_rc2.min.js"></script>
    <script src="{$webroot_resources}/common/bootstrap/extend/bootstrap-wysihtml5/js/bootstrap-wysihtml5-0.0.2.js"></script> -->
    
    <!-- Google Code Prettify -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/other/google-code-prettify/prettify.js"></script> -->
    
    <!-- Gritter Notifications Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/notifications/Gritter/js/jquery.gritter.min.js"></script> -->
    
    <!-- Notyfy Notifications Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/notifications/notyfy/jquery.notyfy.js"></script> -->
    
    <!-- MiniColors Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/color/jquery-miniColors/jquery.miniColors.js"></script> -->
    
    <!-- DateTimePicker Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/forms/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script> -->

    <!-- Cookie Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/system/jquery.cookie.js"></script> -->
    
        <!-- Colors -->
    <script>
    var primaryColor = '#8ec657',
        dangerColor = '#b55151',
        successColor = '#609450',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    </script>
    
    <!-- Themer -->
    <script>
    var themerPrimaryColor = primaryColor;
    </script>
    

    <!-- <script src="{$webroot_resources}/common/theme/scripts/demo/themer.js"></script> -->
    
    <!-- Twitter Feed -->
    <!--<script src="{$webroot_resources}/common/theme/scripts/demo/twitter.js"></script>          -->
    
    <!-- Easy-pie Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/charts/easy-pie/jquery.easy-pie-chart.js"></script> -->
    
    <!-- Sparkline Charts Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/charts/sparkline/jquery.sparkline.min.js"></script> -->
    
    <!-- Ba-Resize Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/other/jquery.ba-resize.js"></script>                  -->
    <!-- DataTables Tables Plugin -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/plugins/tables/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="{$webroot_resources}/common/theme/scripts/plugins/tables/DataTables/media/js/DT_bootstrap.js"></script> -->
    
    <script src="{$webroot_resources}/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="{$webroot_resources}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Tables Demo Script -->
    <!-- <script src="{$webroot_resources}/common/theme/scripts/demo/tables.js"></script> -->
    
        
    <!-- Bootstrap Form Wizard Plugin -->
    <!-- <script src="{$webroot_resources}/common/bootstrap/extend/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js"></script>            -->
    
    <!-- Form Wizards Page Demo Script -->
    <!--<script src="{$webroot_resources}/common/theme/scripts/demo/form_wizards.js"></script>          -->
    
    <!-- <link href="{$webroot_resources}/css/style.css" rel="stylesheet" /> -->
    
    <script src="{$webroot_resources}/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="{$webroot_resources}/dist/js/app.min.js"></script>
	
	
	<script src="{$webroot_resources}/plugins/morris/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="{$webroot_resources}/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script src="{$webroot_resources}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="{$webroot_resources}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="{$webroot_resources}/plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="{$webroot_resources}/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script src="{$webroot_resources}/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{$webroot_resources}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="{$webroot_resources}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="{$webroot_resources}/plugins/fastclick/fastclick.js"></script>
	
	<link rel="stylesheet" href="{$webroot_resources}/plugins/pace/pace.min.css">
	<!-- PACE -->
	<script src="{$webroot_resources}/plugins/pace/pace.min.js"></script>

	
    <!-- Select2 -->
  	
	<script src="{$webroot_resources}/plugins/select2/select2.full.min.js"></script>
	<script src="{$webroot_resources}/plugins/iCheck/icheck.min.js"></script>
	
  
    <script type="text/javascript" src="{$webroot_resources}/js/jquery-migrate-1.0.0.js"></script>
    <script type="text/javascript" src="{$webroot_resources}/js/script.js"></script>
    <script type="text/javascript" src="{$webroot_resources}/js/jquery.maskedinput-1.2.2.js"></script>
     <script type="text/javascript" src="{$webroot_resources}/js/jquery.searchabledropdown-1.0.8.min.js"></script>
     <script type="text/javascript" src="{$webroot_resources}/js/dropzone.js"></script>
    
    
    
    <!-- <script type="text/javascript" src="{$webroot_resources}/js/jquery.custom-file-input.js"></script> -->
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">

  	<!-- Main Header -->
<header class="main-header">

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="container">
            
            <div id="logo">
                <a href="{$webroot}"><h1>GO<span>ETU</span></h1></a>
            </div>
        
            
            <ul class="nav navbar-nav navigation">
            	{foreach name=topmenu from=$meta.menu item=item key=key}
            	
            		{if $item|is_array}
            			
            			<li class="wsubnav"><a href="#">{$meta.menu_label.$key} <i class="fa fa-caret-down"></i></a>
            			<ul class="nav navbar-nav subnavigation">
            				{foreach name=submenu from=$item item=subitem key=subkey}
            					<li><a href="{$webroot}/{$key}/{$subkey}">{$subitem}</a></li>
            				{/foreach}	
            			</ul>
            			</li>
            		{else}
            			
            		{/if}
	            	<!-- <li class="wsubnav"><a href="#">Merchants <i class="fa fa-caret-down"></i></a>
		            	<ul class="nav navbar-nav subnavigation">
		            		<li><a href="{$webroot}/merchant/add">Add New Merchant</a></li>
		            		<li><a href="{$webroot}/merchant/management">Merchants</a></li>
		            		<li><a href="{$webroot}/product/management">Products</a></li>
		            	</ul>
		            </li>
		            <li class="wsubnav"><a href="#">Agents <i class="fa fa-caret-down"></i></a>
		            	<ul class="nav navbar-nav subnavigation">
		            		<li><a href="{$webroot}/agent/add">Add New Agent</a></li>
		            		<li><a href="{$webroot}/agent/management">Agents</a></li>
		            	</ul>
		            </li>
		            <li><a href="{$webroot}/admin/acl">ACL</a></li> -->
		            
		            
	                <!-- <li class="wsubnav"><a href="#">Entity <i class="fa fa-caret-down"></i></a>
	                    <ul class="nav navbar-nav subnavigation">
	                        <li><a href="{$webroot}/partner/management">Partner</a></li>
	                        <li><a href="merchant.php">Merchant</a></li>
	                        <li><a href="#">Agent</a></li>
	                        
	                    </ul>	
	                </li>
	                <li><a href="#">Orders</a></li>
	                <li><a href="#">Billing</a></li>
	                <li><a href="#">Report</a></li>
	                <li><a href="#">Setup</a></li> -->
	                
	                
	                
                
                {/foreach}
            </ul>
            
            <!-- Sidebar toggle button // Ready for Responsive
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a> -->
            
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                
                <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{$webroot_resources}/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                            		{php}
                                    	print_r($_SESSION['username']);
                                    {/php}
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{$webroot_resources}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                <p>
                                    {php}
                                    	print_r($_SESSION['username']);
                                    {/php}
                                    <!--<small>Member since Nov. 2012</small>-->
                                </p>
                            </li>
                        <!-- Menu Footer-->
                            <li class="user-footer">
                                <!-- <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div> -->
                                <div class="pull-right">
                                    <a href="{$webroot}/log/out" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    
        <div class="clearfix"></div>
        
        <div class="sub-nav-wrapper">
            <div class="container">
                <!-- Navbar Search Right -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="nav-search">
                            <form action="#" method="get" class="sidebar-form">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
    </nav>
</header>


<!-- Content Wrapper. Contains page content -->


            {assign var="class" value=$meta.class}
                {if $action_tpl != ''}
                    {include file="$action_tpl"}
            {/if}

 <!-- /.content-wrapper -->

<div id="modalConfirm" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body</p>
              </div>
              <div class="modal-footer">
              	<button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>

<div id="modalConfirm" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  <input type="hidden" name="txtConfirmMessage" name="txtConfirmMessage" />
                <h4 class="modal-title">GoETU Billing</h4>
              </div>
              <div class="modal-body">
                <p id="modalMessageConfirm">One fine body</p>
              </div>
              <div class="modal-footer">
              	<button type="button" id="confirmOk" class="btn btn-primary">Ok</button>
                <button type="button" id="confirmCancel" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>



<div id="modalLoading" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
                  <!-- <span aria-hidden="true">&times;</span></button> -->
                	<h4 class="modal-title" id="modal-title-message">GoETU Billing</h4>
              </div>
              <div class="modal-body">
                <p id="modalLoadingMessage">Modal Message</p><i class="fa fa-spin fa-refresh"></i>
              </div>
              <div class="modal-footer">
                
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>


<div id="modalCustomField" class="modal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  <input type="hidden" name="txtConfirmMessage" name="txtConfirmMessage" />
                <h4 class="modal-title">GoETU Billing</h4>
              </div>
              <div class="modal-body">
					<div class="box-body">
                    	<div class="row">
                    		<div class="col-md-12">
                                <div class="form-group">
                                    <label for="compname">Field Name:</label>
                                    <input type="text" class="form-control" id="txtFieldName" name="txtFieldName" placeholder="Enter Field name">
                                </div>
                            </div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-12">
                                <div class="form-group">
                                    <label for="compname">Field Type:</label>
                                    <select class="form-control select2" style="width: 100%;" id="txtFieldType" name="txtFieldType">
					                  
					                  	<option value="Text">Text</option>
					                  	<option value="Dropdown">Dropdown</option>
					                  	<option value="Checkbox">Checkbox</option>
					                  	<option value="Textarea">Textarea</option>
					                  
					                </select>
                                </div>
                            </div>
                    	</div>
                    	<div class="row" id="rowOption" name="rowOption" style="display: none">
                    		<div class="col-md-12">
                                <div class="form-group">
                                    <label for="compname">Options:</label>
                                    <textarea class="form-control" id="txtOption" name="txtOption" placeholder="Enter Options">
                                    	
                                    </textarea>
                                </div>
                            </div>
                    	</div>
                    	
                    </div>    
              </div>
              <div class="modal-footer">
              	<button type="button" id="btnAddField" name="btnAddField" class="btn btn-primary">Add Field</button>
                <button type="button" id="btnCancelField" name="btnCancelField" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>    

<div id="modalGeneric" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                	<h4 class="modal-title" id="modal-title-message">GoETU Billing</h4>
              </div>
              <div class="modal-body">
                <p id="modalMessage">Modal Message</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>



{php}
  $this->assign("exempt", array('merchant_application.tpl','admin_users.tpl'));
{/php}


{if  !in_array($action_tpl , $exempt)}
  <!-- Main Footer -->
  <footer class="main-footer">
	<div class="container">
    	<div class="row">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                <!--Anything you want-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2016 <a href="./">GOETU</a>.</strong> All rights reserved.
            {$action_tpl}
        </div>
    </div>
</footer>
{/if}

</div>

</body>
</html>
