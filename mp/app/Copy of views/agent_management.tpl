	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Agents
                    <!-- <a href="{$webroot}/partner/add">
                    	<button class="btn btn-primary" type="button">Add New Partner</button>
                    </a> -->
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">List Of Agents</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Agent List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Agent ID</th>
                                                <th>Partner</th>
                                                <th>Email</th>
                                                <th>Phone 1</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach from=$agents item=item}
                                            
	                                            <tr>
	                                            	<td><a href="{$webroot}/agent/edit/{$item.id}">{$item.partner_id_reference}</a></td>
	                                                <td>{$item.agent_partner}</td>
	                                                <td>{$item.email}</td>
	                                                <td>+{$item.mobile_number}</td>
	                                                <td>{$item.first_name}</td>
	                                                <td>{$item.last_name}</td>
	                                            </tr>
                                            
                                            {/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->