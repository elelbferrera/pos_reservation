<script type="text/javascript" src="{$webroot_resources}/js/partner.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div class="container">
    	<div class="row">
            <!-- Content Header (Page header / Title / Breadcrumbs) -->
            <section class="content-header">
                <h1>
                Partner
                <small>Profile</small>
                </h1> 
                <ol class="breadcrumb">
                    <li><a href="{$webroot}/partner/management"><i class="fa fa-dashboard"></i>Partner</a></li>
                    <li class="active">Profile</li>
                </ol>
            </section>
            
            <!-- Main content -->
            <section class="content">
            	<div class="row">
                <!-- Your Page Content Here -->
                    <div class="col-sm-3">
                        <div class="sidelink">
                        	<!-- <h3 class="sidelink-title">Partner Profile</h3> -->
                            <ul class="sidelink-nav">
                            	<!-- <li><a href="summary.php">Summary</a></li> -->
                                <li><a href="{$webroot}/partner/edit/{$partner_id}" class="selected">Profile</a></li>
                                <li><a href="{$webroot}/partner/contact/{$partner_id}">Contacts</a></li>
                                <li><a href="#">Payment Info</a></li>
                                <li><a href="#">Products</a></li>
                                <li><a href="invoice.php">Invoices</a></li>
                                <li><a href="#">Attachments</a></li>
                                <li><a href="#">Notes</a></li>
                                <li><a href="#">Communication</a></li>
                                <li><a href="#">Log</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="box">
                            <div class="box-header with-border">
                            	<!-- <h3 class="box-title">Profile</h3> -->
                            </div>
                        	<!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" id="frmUpdatePartner" name="frmUpdatePartner">
                                <div class="box-body">
                                	<input type="hidden" id="txtPartnerID" name="txtPartnerID" value="{$partner_id}">
                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Company Name:</label>
                                                    <input type="text" class="form-control" id="txtCompanyName" name="txtCompanyName" placeholder="Enter company name" value="{$partner_info.company_name}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">DBA:</label>
                                                    <input type="text" class="form-control" id="txtDBA" name="txtDBA" placeholder="Enter DBA" value="{$partner_info.dba}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1"  value="{$partner_info.address1}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2"  value="{$partner_info.address2}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city" value="{$partner_info.city}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text" class="form-control" id="txtState" name="txtState" placeholder="Enter state" value="{$partner_info.state}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip" value="{$partner_info.zip}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
									                  {foreach from=$country item=item}
									                  	<option value="{$item.id}" {if $item.id eq $partner_info.country_id} selected="selected"  {/if}>{$item.name}</option>
									                  {/foreach}
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email" value="{$partner_info.email}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1" value="{$partner_info.phone1}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2"  value="{$partner_info.phone2}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  {foreach from=$partner_type item=item}
									                  	<option value="{$item.id}" {if $item.id eq $partner_info.partner_type_id} selected="selected"  {/if}>{$item.name}</option>
									                  {/foreach}
									                </select>

                                                </div>
                                            </div>
                                        </div> 
                                </div>
                                <!-- /.box-body -->
                            
                                <div class="box-footer">
                                    <button type="submit" id="btnUpdatePartner" name="btnUpdatePartner" class="btn btn-primary">Update Partner</button>
                                </div>
                            </form>
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
    </div>
</div>
<!-- /.content-wrapper -->