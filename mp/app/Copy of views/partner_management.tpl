	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Partners
                    <a href="{$webroot}/partner/add">
                    	<button class="btn btn-primary" type="button">Add New Partner</button>
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Entity</li>
                        <li class="active">Partner</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Partner List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>MBA</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                                <th>Mobile Phone</th>
                                                <th>State</th>
                                                <th>Country</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach from=$partners item=item}
                                            
	                                            <tr>
	                                            	<td><a href="{$webroot}/partner/edit/{$item.partner_id}">{$item.company_name}</a></td>
	                                                <td>{$item.dba}</td>
	                                                <td>{$item.email}</td>
	                                                <td>+{$item.phone1}</td>
	                                                <td>{$item.state}</td>
	                                                <td>{$item.country_name}</td>
	                                            </tr>
                                            
                                            {/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->