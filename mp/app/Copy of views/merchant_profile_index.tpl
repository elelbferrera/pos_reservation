<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/merchant_profile.js"></script>



<!-- <link href="{$webroot_resources}/css/jcrop.css" rel="stylesheet" /> -->
<link href="{$webroot_resources}/css/jquery.jcrop.css" rel="stylesheet" />
<script type="text/javascript" src="{$webroot_resources}/js/jquery.jCrop.js"></script>


<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/merchants.png" style="height: 30px;width: 30px;" />Merchant Information</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <!-- <div class="widget-head"> -->
        <!-- <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
            My Mechant Information</h4>
             
        </a>     -->
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <!-- <a href="javascript:void(0)" onclick="RegisterMerchant(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New Merchant </h4>
            </a>   -->      
        <!-- </div>                                                                                        -->
        
        <!-- <div class="widget-body">  -->
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
           

        <!-- </div> -->
        <div  id="dlgUploadLogo" title="Upload Logo">
		   <form id="upload_form" name ="upload_form" enctype="multipart/form-data"  method="post">
			        <!-- hidden crop params -->
			        <input type="hidden" id="x1" name="x1" />
			        <input type="hidden" id="y1" name="y1" />
			        <input type="hidden" id="x2" name="x2" />
			        <input type="hidden" id="y2" name="y2" />
			        <input type="hidden" id="file_name" name="file_name">
			        <input type="hidden" id="txt_upload_merchant_id" name="txt_upload_merchant_id" value ="{$my_info.id}">
			
			        
			        <div><input type="file" name="image_file" id="image_file"  /></div>
			<!-- onchange="fileSelectHandler()" -->
			
			        <div class="error"></div>
			
			        <div class="step2">
			            <!-- <h2>Step2: Please select a crop region</h2> -->
			            <img id="preview" name="preview"/>
			
			            <div class="info" style="display: none">
			                <label>File size</label> <input type="text" id="filesize" name="filesize" />
			                <label>Type</label> <input type="text" id="filetype" name="filetype" />
			                <label>Image dimension</label> <input type="text" id="filedim" name="filedim" />
			                <label>W</label> <input type="text" id="w" name="w" />
			                <label>H</label> <input type="text" id="h" name="h" />
			            </div>
						<br />
			            <input type="button" value="Crop" id="btnCrop" name="btnCrop" class="btn btn-info" />
			        </div>
			 </form>
		</div>

                                                
        <div  id="dlgMerchant1" title="New Merchant">
        <div class="widget widget-tabs">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
            <li id="tabMerchant1" class="active"><a href="#tab1" class="glyphicons notes" data-toggle="tab"><i></i>Basic Information</a></li>
            <li id="tabMerchant2"><a href="#tab2" class="glyphicons notes_2" data-toggle="tab"><i></i>Merchant Address</a></li>
   
                </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           	
           	
        <form id="frmRegisterMerchant" name="frmRegisterMerchant" enctype="multipart/form-data">
        <input type="hidden"  id = "txtMerchantId" name = "txtMerchantId" value ="{$my_info.id}"/>  
        <input type="hidden"  id = "txtLogo" name = "txtLogo"/> 
        <div id ="divMerchantStep1">
        			
<div class="bbody">

    <!-- upload form -->
    <img id="img_src" name="img_src" src="{$my_info.merchant_logo}" height="100px" width="100px">
    <br>
    <br>
    <input type="button" name="btnUpload" id="btnUpload" value="Upload Logo" onclick="upload_new_logo()" class="btn btn-info">
    
</div>

        <table width="50%">
            <tbody>
            	<tr>	
            	</tr>
               <tr>
                    <td class="tbl_label">Merchant Code:</td>
                    <td>
                        <input type="text" name="txtMerchantCode" id="txtMerchantCode" size="29" maxlength="20" value='{$my_info.merchant_code}' />
                     </td>
                </tr>
               
               <tr>
                    <td class="tbl_label">Merchant Name:</td>
                    <td>
                        <input type="text" name="txtMerchantName" id="txtMerchantName" size="29" maxlength="60" value='{$my_info.merchant_name}' />
                     </td>
                </tr>
                <tr>
                    <td class="tbl_label">Contact Last Name:</td>
                    <td>
                        <input type="text" name="txtLastName" id="txtLastName" size="29" maxlength="40" value='{$my_info.last_name}' />
                     </td>
                </tr> 
                <tr>
                    <td class="tbl_label">Contact First Name:</td>
                    <td>
                        <input type="text" name="txtFirstName" id="txtFirstName" size="29" maxlength="40" value='{$my_info.first_name}' />
                     </td>
                </tr> 
                <!-- <tr>
                    <td class="tbl_label">Username:</td>
                    <td>
                        <input type="text" name="txtUsername" id="txtUsername" size="29" maxlength="30" value='{$my_info.username}' />
                     </td>
                </tr>  -->
                <!-- <tr>
                    <td class="tbl_label">Password:</td>
                    <td>
                        <input type="text" name="txtPassword" id="txtPassword" size="29" maxlength="30" />
                     </td>
                </tr>  -->
 <!--                
                <tr>
                    <td class="tbl_label">Contact Person:</td>
                    <td>
                        <input type="text" name="txtContactPerson" id="txtContactPerson" size="29" maxlength="60" />
                     </td>
                </tr>
         
               <tr>
                    <td class="tbl_label">Merchant Username:</td>
                    <td>
                        <input type="text" name="txtUsername" id="txtUsername" size="29" maxlength="60" />
                     </td>
                </tr>
                <tr>
                    <td class="tbl_label">Merchant Password:</td>
                    <td>
                        <input type="text" name="txtPassword" id="txtPassword" size="29" maxlength="60" />
                     </td>
                </tr>

                 <tr>
                    <td class="tbl_label">Business Type</td>
                    <td>
                        <input type="text" name="txtBusinessType" id="txtBusinessType" size="29" maxlength="60" />
                     </td>
                </tr>-->
               <tr>
                    <td class="tbl_label">Email Address:</td>
                    <td>
                        <input type="text" name="txtEmailAddress" id="txtEmailAddress" size="29" maxlength="60" value='{$my_info.email}' />
                     </td>
                     
               </tr> 

               <tr>
                    <td class="tbl_label">Telephone No:</td>
                    <td>
                        <input type="text" name="txtPhone" id="txtPhone" size="29" maxlength="20" value='{$my_info.telephone}' />
                     </td>
                     
               </tr> 
               <tr>
                    <td class="tbl_label">Mobile Phone:</td>
                    <td>
                        <input type="text" name="txtMobile" id="txtMobile" size="29" maxlength="20" value='{$my_info.mobile}'/>
                     </td>      
               </tr> 
               <tr>
                    <td class="tbl_label">Fax:</td>
                    <td>
                        <input type="text" name="txtFax" id="txtFax" size="29" maxlength="20" value='{$my_info.fax}'/>
                     </td>
                </tr> 

            </tbody>
        </table>
        
        </div>
        </div>
        <div id ="divMerchantStep2" style="display: none;">
        
            <table width="100%">
            <tbody>

                <tr>
                    <td class="tbl_label">Country:</td>
                    <td>
                    	<select id="txtCountry" name="txtCountry">
	                    	{foreach from=$countries item=item}
	                        	<option value="{$item}" label="{$item}" {if $item eq $my_info.country} selected {/if}>
	                    {$item}
	                        	</option>
	                        	
	                        	
	                        {/foreach}
                    	</select>
                    </td>
                </tr> 
                
                <tr>
                    <td class="tbl_label">State:</td>
                    <td>
                        <input type="text" name="txtState" id="txtState" size="29" maxlength="20" value ="{$my_info.state}" />
                     </td>
                </tr> 
                <tr>
                    <td class="tbl_label">City:</td>
                    <td>
                        <input type="text" name="txtCity" id="txtCity" size="29" maxlength="20" value ="{$my_info.city}"/>
                     </td>
                     
                </tr> 
                <tr>
                    <td class="tbl_label">Zip Code:</td>
                    <td>
                        <input type="text" name="txtZipCode" id="txtZipCode" size="29" maxlength="20" value ="{$my_info.zip}"/>
                     </td>
                </tr> 

            </tbody>
        </table>        
        </div>

        <hr>          
        <!-- <div id="divNew" align="right">
            <input type="button"  name="btnRegisterMerchant" id="btnRegisterMerchant" value="Register Merchant" class="btn btn-info"/>
            <input type="button"  name="btnUpdateMerchant" style="display:none" id="btnUpdateMerchant" value="Update Merchant" class="btn btn-info"/>
            
            <input type="button"  name="btnCancelNewMerchant" id="btnCancelNewMerchant" value="Cancel" class="btn btn-danger btn-small"/>
        </div> -->
        
          <div id="divEdit" align="left">
            <input type="button"  name="btnUpdateMyMerchantProfile" id="btnUpdateMyMerchantProfile" value="Update My Profile" class="btn btn-info"/>
            
        </div>
          
        </form> 
        </div>   
        </div> 
        </div>
        </div>
        

        
        
        
        
        {$_messages_}
    
    
    </div>
</div>
</div>


        
