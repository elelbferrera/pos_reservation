<script type="text/javascript" src="{$webroot_resources}/js/agent.js"></script>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Update Agent
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}/agent/management"><i class="fa fa-dashboard"></i> Agents</a></li>
                        <li class="active">Profile</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <!-- <div class="col-sm-3">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="summary.php">Summary</a></li>
                                    <li><a href="profile.php">Profile</a></li>
                                    <li><a href="#">Contacts</a></li>
                                    <li><a href="#">Payment Info</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">Invoices</a></li>
                                    <li><a href="#">Attachments</a></li>
                                    <li><a href="#">Notes</a></li>
                                    <li><a href="#">Communication</a></li>
                                    <li><a href="#">Log</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <div class="col-sm-12">
                            <div class="box">
                                <!-- <div class="box-header with-border">
                                	<h3 class="box-title">Company Profile</h3>
                                </div> -->
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frmRegisterAgent" id="frmRegisterAgent">
                                	<input type="hidden" name="txtProfileId" id="txtProfileId" value="{$agent_id}" >
                                    <div class="box-body">
                                    	<div class="row">
                                            <!-- <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label for="processor">Processor:</label>
	                                                <input type="text" class="form-control" id="processor" placeholder="Enter processor">
	                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="merchantID">Merchant ID:</label>
                                                    <input type="text" class="form-control" id="merchantID" placeholder="Enter merchant id">
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12"><h4>Company Profile:</h4></div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Agent ID:</label>
                                                    <input type="text" class="form-control" id="txtAgentId" name="txtAgentId" placeholder="Enter Agent Id" value="{$agent.partner_id_reference}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">Partner:</label>
                                                    <input value="{$agent.agent_partner}" type="text" class="form-control" id="txtPartner" name="txtPartner" placeholder="Enter Partner">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Firstname:</label>
                                                    <input type="text" value="{$agent.first_name}"  class="form-control" id="txtFirstName" name="txtFirstName" placeholder="Enter First Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Last Name:</label>
                                                    <input type="text"  value="{$agent.last_name}" class="form-control" id="txtLastName" name="txtLastName" placeholder="Enter Last Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text"  value="{$agent.address1}" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" value="{$agent.address2}" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" value="{$agent.city}" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text"  value="{$agent.state}" class="form-control" id="txtState" name="txtState" placeholder="Enter state">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" value="{$agent.zip}" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
									                  {foreach from=$country item=item}
									                  	<option value="{$item.name}" {if $item.name eq $agent.country} selected="selected" {/if}>{$item.name}</option>
									                  {/foreach}
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" value="{$agent.email}" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" value="{$agent.mobile_number}" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" value="{$agent.other_number}" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: none">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <!-- <option selected="selected">Alabama</option>
									                  <option>Alaska</option>
									                  <option>California</option>
									                  <option>Delaware</option>
									                  <option>Tennessee</option>
									                  <option>Texas</option>
									                  <option>Washington</option> -->
									                  {foreach from=$partner_type item=item}
									                  	<option value="{$item.id}"{if $item.id eq $agent.partner_type_id} selected="selected" {/if}>{$item.name}</option>
									                  {/foreach}
									                </select>

                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer right">
                                        <button type="submit" class="btn btn-primary" id="btnUpdateAgent" name="btnUpdateAgent">
                                        	Update
                                        </button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->