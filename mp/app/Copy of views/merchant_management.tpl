	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchants
                    <a href="{$webroot}/partner/add">
                    	<!-- <button class="btn btn-primary" type="button">Add New Partner</button> -->
                    </a>
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{$webroot}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Merchant</li>
                        <li class="active">Merchants</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Merchant List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Merchant ID</th>
                                                <th>Processor</th>
                                                <th>Company Name</th>
                                                <th>Address</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach from=$merchants item=item}
                                            
	                                            <tr>
	                                            	<td><a href="{$webroot}/merchant/summary/{$item.partner_id}">{$item.merchant_id}</a></td>
	                                                <td>{$item.processor}</td>
	                                                <td>{$item.company_name}</td>
	                                                <td>{$item.address1}</td>
	                                                <td>{$item.email}</td>
	                                                <td>{$item.phone1}</td>
	                                            </tr>
                                            
                                            {/foreach}
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->