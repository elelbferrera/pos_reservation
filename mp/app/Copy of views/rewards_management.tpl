<script type="text/javascript" src="{$webroot_resources}/js/jquery.upload-1.0.2.min.js"></script>
<script type="text/javascript" src="{$webroot_resources}/js/rewards.js"></script>

<div id="content">
<h3> <img  style="padding-right:10px;height: 40px;width: 40px" src="{$webroot_resources}/images/rewards.png" alt="Click to hide" style="height: 30px;width: 30px;" />List of Rewards</h3>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget">
    <!-- Widget heading -->
        <div class="widget-head">
        <a href="{$webroot}">
            <h4 class="heading">
            <input  type="image" style="padding-right:1px;height: 20px;width: 20px" src="{$webroot_resources}/images/home.png" title="home"/>
             Rewards</h4>
             
        </a>    
            <!--<a href="{$webroot}/customers/management">
                <h4 class="heading">Customer Maintenance</h4>
            </a>--> 
            <a href="javascript:void(0)" onclick="RegisterReward(false);return false;">
                <h4 class="floatright">
                <input  type="image" style="padding-right:1px;height: 30px;width: 30px" src="{$webroot_resources}/images/new.png" title="New"/>
                Register New Rewards </h4>
            </a>        
        </div>                                                                                       
        
        <div class="widget-body">
          <!-- Table -->               
            <!--<div>
            <br/>
            </div>-->
            <!-- // Table END -->
            
            
            <table id="tblSearchResult" width="100%"  class="dynamicTable table table-striped table-bordered table-condensed">
            <thead>
                <tr>       
                    <th width="10%">Reward Code</th>
                    <th width="10%">Rewards Name</th>
                    <th width="2%">Level</th>
                    <th width="15%">Points - $ Conversion</th>
                    <!-- <th width="10%">Point Amount Conversion</th> -->
                    <!-- <th width="10%">Required Amount</th> -->
                    <th width="15%">$Amount - Point Conversion</th>
                    <th width="10%">Goal Points</th>
                    <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$rewards item=item}
                    <tr>
                        <td>{$item.reward_code}</td> 
                        <td>{$item.reward_name}</td> 
                        <td>{$item.level}</td> 
                        <td>{$item.point|string_format:"%d"} pt(s) - ${$item.point_amount_conversion|string_format:"%.2f"}</td> 
                        <!-- <td></td> -->
                        <td>${$item.amount|string_format:"%.2f"} - {$item.amount_point_conversion|string_format:"%d"} pt(s)</td>
                        
                        <!-- <td></td> -->
                        <td>{$item.goal_point|string_format:"%d"} pt(s)</td>
                        <td>
                            <a onclick="EditReward('{$item.id}','{$item.merchant_id}','{$item.reward_code}','{$item.reward_name}',
                            '{$item.start_date}','{$item.end_date}','{$item.is_expire_on_day}','{$item.number_of_days}','{$item.is_expire_on_create_date}','{$item.start_date_on_day}',
                            '{$item.point}','{$item.point_amount_conversion}','{$item.amount}','{$item.amount_point_conversion}','{$item.goal_point}','{$item.automatically_claim_reward}',
                            '{$item.day_of_month}','{$item.description}')">Edit</a>
                            <a href="#" onclick="DeleteReward('{$item.id}')">Delete</a>
                        </td>
                    </tr>
                {/foreach}    
        
            </tbody>
            </table>
            </fieldset>

        </div>
                                                              
        <div  id="dlgReward" title="New Reward" style="display:none" >
        <div class="widget widget-tabs widget-tabs-double">  
        <!-- Widget heading -->
            <div class="widget-head">
              <ul>
                <li id="tabReward1" class="active"><a href="#tab1" class="glyphicons user" data-toggle="tab"><i></i>Reward Information</a></li>
           <!--     <li id="tabAgent2"><a href="#tab2" class="glyphicons adress_book" data-toggle="tab"><i></i>DBA Address</a></li>
                <li id="tabAgent3"><a href="#tab3" class="glyphicons home" data-toggle="tab"><i></i>Mailing Address</a></li>
               -->
              </ul>
            </div>    
           <div class="widget-body">   
           <div class="tab-content">   
           
        <form id="frmRegisterReward" name="frmRegisterReward">
            <input type="hidden"  id = "txtMerchantId" name = "txtMerchantId"  value="-1"/>  
            <input type="hidden"  id = "txtRewardId" name = "txtRewardId"/>
            
            <div id ="divRewardStep1"> 
                <table width="100%">
                <tbody>
                   <tr>
                        <td class="tbl_label">Reward Code:</td>
                        <td>
                            <input type="text" name="txtRewardCode" id="txtRewardCode" size="29" maxlength="30" />
                         </td>
                    </tr>
                   <tr>
                        <td class="tbl_label">Reward Name:</td>
                        <td>
                            <input type="text" name="txtRewardName" id="txtRewardName" size="29" maxlength="60" />
                         </td>
                    </tr>  
                    
					<tr>
                        <td class="tbl_label">Description:</td>
                        <td>
                            <textarea name="txtDescription" id="txtDescription" size="35" maxlength="100"> </textarea>
                         </td>
                    </tr>  
                    
                    
                    <!-- <tr>
                    	<td></td>
                    	<td>
                    		<h4>Point Expiration</h4>
                    	</td>
                    </tr> -->
                    
                    <!-- <tr>
                        <td class="tbl_label"></td>
                        <td>
                        	<label>
                        		
                        		<input type="checkbox" name="chkNoOfDay" id="chkNoOfDay" checked/>
                        		Specify Expiry on No. Of Days
                        	</label>
                    			     
                         </td>
                    </tr> -->
                    
                    
                    <!-- <tr id="trNoOfDays" name="trNoOfDays" >
                        <td class="tbl_label">No. of Days:</td>
                        <td>
                            <input type="text" name="txtNoOfDays" id="txtNoOfDays" size="29" maxlength="60" onkeypress="return isNumberKey(event)"/>
                         </td>
                    </tr>   
                    
                    <!-- can be specify the date when the customer registered or specify a starting date   --> 
                    <!-- <tr id="trSpecifyDate" name="trSpecifyDate" >
                        <td class="tbl_label"></td>
                        <td>
                        	<label>
                        		<input type="checkbox" name="chkSpecifyStartDate" id="chkSpecifyStartDate" />
                        		Do you want to specify start date?
                        	</label>
                        	<br><br>
                    		Remaining Uncheck will use Customer Registration Date as Default	     
                         </td>
                    </tr>    -->
                    
                    <!-- <tr id="trSpecifyDateField" name="trSpecifyDateField" >
                        <td class="tbl_label">Start Date:</td>
                        <td>
                            <input type="text" name="txtSpecifiedStartDate" id="txtSpecifiedStartDate" size="29" maxlength="60" />
                         </td>
                    </tr>   -->
                    
                    <!-- <tr id="trStartDate" name="trStartDate" style="display:none;">
                        <td class="tbl_label">Start Date:</td>
                        <td>
                            <input type="text" name="txtStartDate" id="txtStartDate" size="29" maxlength="60" />
                         </td>
                    </tr>    -->
                    <!-- <tr id="trEndDate" name="trEndDate" style="display:none;"> -->
                    <tr id="trEndDate" name="trEndDate">
                        <td class="tbl_label">Points Reset After:</td>
                        <td>
                            <input type="text" name="txtEndDate" id="txtEndDate" size="29" maxlength="60" />
                         </td>
                    </tr> 
                    
                    
                    <tr>
                        <td class="tbl_label"></td>
                        <td>
                        	<br>
                            <h4>Conversions</h4>
                         </td>
                    </tr> 
                    
                    <tr>
                        <td class="tbl_label">Points Conversion:</td>
                        <td>
                            $<input type="text" name="txtAmount1" id="txtAmount1" size="5" maxlength="80" onkeypress="return isNumberKey(event)" />
                            =
                            <input type="text" name="txtPoint1" id="txtPoint1" size="5" maxlength="80" onkeypress="return isNumberKey(event)"/>Points
                         </td>
                    </tr>   
                    <tr>
                        <td class="tbl_label">Amount Conversion:</td>
                        <td>
                            Points<input type="text" name="txtPoint2" id="txtPoint2" size="5" maxlength="80" onkeypress="return isNumberKey(event)"/>
                            =
                            <input type="text" name="txtAmount2" id="txtAmount2" size="5" maxlength="80" onkeypress="return isNumberKey(event)"/>$
                         </td>
                    </tr>   
                    
                    <tr>
                    	<td class="tbl_label">Goal Points:</td>
                    	<td>
                    		<input type="text" name="txtGoalPoint" id="txtGoalPoint" size="29" maxlength="20" onkeypress="return isNumberKey(event)"/>
                    	</td>
                    </tr>
                    
                    <!-- <tr>
                    	<td class="tbl_label"> </td>
                    	<td>
                    		<input type="checkbox" name="chkRewardClaimAuto" id="chkRewardClaimAuto" value="">
                    		Claim Rewards Automatically?
                    	</td>
                    </tr>
                    
                    <tr id="trDayOfMonth" name="trDayOfMonth">
                    	<td class="tbl_label">Every nth day of the month:</td>
                    	<td>
                    		<input type="text" name="txtDayOfMonth" id="txtDayOfMonth" value="" onkeypress="return isNumberKey(event)">
                    	</td>
                    </tr> -->
                    
                </tbody>
            </table>
            </div>

            
            </div>
            

            <hr>          
            <div id="divNew" align="right">
                <input type="button"  name="btnRegisterReward" id="btnRegisterReward" value="Register Reward" class="btn btn-info"/>
                <input type="button"  name="btnUpdateReward" style="display:none" id="btnUpdateReward" value="Update Reward" class="btn btn-info"/>
                <input type="button"  name="btnCancelNewReward" id="btnCancelNewReward" value="Cancel" class="btn btn-danger btn-small"/>
            </div>

          
        </form> 
        </div>   
        </div> 
        </div>
        </div>

        {$_messages_}

    </div>
</div>
</div>


        
