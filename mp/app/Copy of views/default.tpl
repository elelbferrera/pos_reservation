<!DOCTYPE html>
<!-- saved from url=(0042)http://flatfull.com/themes/note/index.html -->
<html lang="en" class="app js no-touch no-android chrome no-firefox no-iemobile no-ie no-ie10 no-ie11 no-ios no-ios7 ipad">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>GO3 Reservation</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="{$webroot_resources}/images/favicon.ico">
    <link rel="stylesheet" href="{$webroot_resources}/css/font.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/bootstrap_calendar.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/go3reservation.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/fuelux.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/custom.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/custom.min.css" type="text/css">
    
    <link rel="stylesheet" href="{$webroot_resources}/css/jquery.paginate.css" type="text/css">
    <link rel="stylesheet" href="{$webroot_resources}/css/bootstrap-timepicker.min.css">
    <link href="{$webroot_resources}/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    
    <link href="{$webroot_resources}/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{$webroot_resources}/css/calendar/fullcalendar_002.css" rel="stylesheet" media="print">
    <link href="{$webroot_resources}/css/calendar/scheduler.css" rel="stylesheet">
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    <style type="text/css">

    </style>
    
    <!-- <script src="{$webroot_resources}/js/jquery-2.2.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{$webroot_resources}/js/app.v1.js"></script>
    <script src="{$webroot_resources}/js/fuelux.js"></script>
    <script src="{$webroot_resources}/js/parsley.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.easy-pie-chart.js"></script>
    <script src="{$webroot_resources}/js/jquery.sparkline.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.tooltip.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.resize.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.grow.js"></script>
    <script src="{$webroot_resources}/js/demo.js"></script>
    <script src="{$webroot_resources}/js/bootstrap_calendar.js"></script>
    <script src="{$webroot_resources}/js/jquery.sortable.js"></script>
    <script src="{$webroot_resources}/js/app.plugin.js"></script>
    <script src="{$webroot_resources}/js/jquery.smartWizard.js"></script>
    <script src="{$webroot_resources}/js/custom.min.js"></script>
    <script src="{$webroot_resources}/js/bootstrap-datepicker.js"></script>
    <script src="{$webroot_resources}/js/bootstrap-timepicker.min.js"></script>
    <script src="{$webroot_resources}/js/fullcalendar.min.js"></script>
     -->
     
    <script src="{$webroot_resources}/js/jquery-2.2.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="{$webroot_resources}/js/app.v1.js"></script>
    <script src="{$webroot_resources}/js/fuelux.js"></script>
    <script src="{$webroot_resources}/js/parsley.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.easy-pie-chart.js"></script>
    <script src="{$webroot_resources}/js/jquery.sparkline.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.tooltip.min.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.resize.js"></script>
    <script src="{$webroot_resources}/js/jquery.flot.grow.js"></script>
    <script src="{$webroot_resources}/js/demo.js"></script>
    <script src="{$webroot_resources}/js/bootstrap_calendar.js"></script>
    <script src="{$webroot_resources}/js/jquery.sortable.js"></script>
    <script src="{$webroot_resources}/js/app.plugin.js"></script>
    <script src="{$webroot_resources}/js/jquery.smartWizard.js"></script>
    <script src="{$webroot_resources}/js/custom.min.js"></script>
    <script src="{$webroot_resources}/js/bootstrap-datepicker.js"></script>
    <script src="{$webroot_resources}/js/bootstrap-timepicker.min.js"></script>
    <script src="{$webroot_resources}/js/bootstrap-colorpicker.min.js"></script>
    <script src="{$webroot_resources}/js/fullcalendar.min.js"></script>

	<script src="{$webroot_resources}/js/jquery.tinycarousel.js"></script>
	<script src="{$webroot_resources}/js/jquery.paginate.js"></script>
	<script src="{$webroot_resources}/js/daterangepicker.js"></script>
	
	
	<script src="{$webroot_resources}/js/calendar/fullcalendar.js"></script>
    <script src="{$webroot_resources}/js/calendar/scheduler.js"></script>
	
	<script src="{$webroot_resources}/js/public.js"></script>
	
</head>

<body class="" style="">
    <section class="vbox">
        <!-- .header -->
         <header class="bg-dark dk header navbar navbar-fixed-top-xs">
            <div class="navbar-header aside-lg">
                <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html"> <i class="fa fa-bars"></i> </a>
                <a href="#" class="navbar-brand" ><img src="{$webroot_resources}/images/logo.png" class="m-r-sm">Reservation</a>
                <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> <i class="fa fa-cog"></i> </a>
            </div>
            <ul class="nav navbar-nav hidden-xs">
                <li>  <a href="index.php" class="{if strpos($action_tpl, "wait") !== false} dker {/if}"> <i class="fa fa-book"></i> Wait List </a> </li>
                <li>  <a href="{$webroot}/reservation/management" class="{if strpos($action_tpl, "reservation") !== false} dker {/if}"> <i class="fa fa-calendar"></i> Reservation</a> </li>
                <li>  <a href="{$webroot}/service/management" class="{if strpos($action_tpl, "service") !== false} dker {/if}"> <i class="fa fa-list-alt"></i> Product and Services </a> </li>
                <!-- <li>  <a href="{$webroot}/branch/management" class="{if strpos($action_tpl, "branch") !== false} dker {/if}"> <i class="fa fa-list-alt"></i> Branch </a> </li> -->
                <li>  <a href="{$webroot}/specialist/management" class="{if strpos($action_tpl, "specialist") !== false} dker {/if}"> <i class="fa fa-users"></i> Specialist</a> </li>
                <!-- <li>  <a href="{$webroot}/merchant_user/management" class="{if strpos($action_tpl, "merchant_user") !== false} dker {/if}"> <i class="fa fa-users"></i> Users</a> </li> -->
                <li>  <a href="#" class="{if strpos($action_tpl, "history") !== false} dker {/if}"> <i class="fa fa-clock-o"></i> History</a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                <li class="hidden-xs">
                    <!-- <a href="#" class="dropdown-toggle dk" data-toggle="dropdown"> <i class="fa fa-bell"></i> <span class="badge badge-sm up bg-stanadard m-l-n-sm count" style="display: inline-block;">3</span> </a> -->
                    <section class="dropdown-menu aside-xl">
                        <section class="panel bg-white">
                            <!-- <header class="panel-heading b-light bg-light"> <strong>You have <span class="count" style="display: inline;">3</span> notifications</strong> </header>
                            <div class="list-group list-group-alt animated fadeInRight">
                                <a href="#" class="media list-group-item" style="display: block;"><span class="pull-left thumb-sm text-center"><i class="fa fa-envelope-o fa-2x text-success"></i></span><span class="media-body block m-b-none">Sophi sent you a email<br><small class="text-muted">1 minutes ago</small></span></a>
                                <a href="#" class="media list-group-item"> <span class="pull-left thumb-sm"> <img src="{$webroot_resources}/images/avatar.jpg" alt="John said" class="img-circle"> </span> <span class="media-body block m-b-none"> Use awesome animate.css<br> <small class="text-muted">10 minutes ago</small> </span> </a>
                                <a href="#" class="media list-group-item"> <span class="media-body block m-b-none"> 1.0 initial released<br> <small class="text-muted">1 hour ago</small> </span> </a>
                            </div>
                            <footer class="panel-footer text-sm"> <a href="#" class="pull-right"><i class="fa fa-cog"></i></a> 
                            <a href="">See all the notifications</a> </footer> -->
                        </section>
                    </section>
                </li>
             <!--    <li class="dropdown hidden-xs"> <a href="http://flatfull.com/themes/note/index.html#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
                    <section class="dropdown-menu aside-xl animated fadeInUp">
                        <section class="panel bg-white">
                            <form role="search">
                                <div class="form-group wrapper m-b-none">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button> </span> </div>
                                </div>
                            </form>
                        </section>
                    </section>
                </li> -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="{$webroot_resources}/images/avatar.jpg"> </span> 
					{php}
						print_r($_SESSION['fname']." ". $_SESSION['lname']);
					{/php} <b class="caret"></b> </a>
                    <ul class="dropdown-menu animated fadeInRight"> <span class="arrow top"></span>
                        <li> <a  href="{$webroot}/setting/management">Settings</a> </li>
                        <!-- <li> <a href="#">Profile</a> </li> -->
                        <!-- <li><a href="#"> <span class="badge bg-stanadard pull-right">3</span> Notifications </a></li> -->
                        <!-- <li> <a href="">Help</a> </li> -->
                        <!-- <li> <a href="lockpage.php" data-toggle="ajaxModal">Lock</a> </li> -->
                        <li class="divider"></li>
                        <li> <a href="{$webroot}/log/out">Logout</a> </li>
                    </ul>
                </li>
            </ul>
        </header>
        <!-- /.header -->
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <aside class="bg-dark lter aside-lg hidden-print hidden-xs" id="nav">
                    <section class="vbox">
                        <header class="header bg-primary lter text-center clearfix">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-dark btn-icon" onclick="add_service()" title="New project"><i class="fa fa-plus"></i></button>
                                <div class="btn-group hidden-nav-xs">
                                    <button type="button" class="btn btn-sm btn-primary" onclick="add_service()"> Add New Reservation </button>
                                </div>
                            </div>
                        </header>
                        <section>
                            <div class="sidebar-titleholder">
                                <div class="sidebar-title ta-center">Analytics</div>
                            </div>
                            <div class="sidebar-module">
                                <div class="sidebar-waitingtitle ta-center">Total Waiting</div>
                                <div class="sidebar-numbers ta-center">14</div>
                            </div>
                            <div class="sidebar-module">
                                <div class="sidebar-waitingtitle ta-center">Total Reserved</div>
                                <div class="sidebar-numbers ta-center">28</div>
                            </div>
                            <div class="sidebar-module">
                                <div class="sidebar-waitingtitle ta-center">In Service</div>
                                <div class="sidebar-numbers ta-center">35</div>
                            </div>
                        </section>
                     
                       
                    </section>
                </aside>                <!-- /.aside -->

                 <!-- .content -->
                
                	<!-- contents should be in here  -->
                	
                	<!-- <div> -->
                		 {assign var="class" value=$meta.class}
                {if $action_tpl != ''}
                    {include file="$action_tpl"}
            {/if}
                	<!-- </div> -->
                
                <!-- /.content -->
               
            </section>
        </section>
    </section>



  <!-- .footer -->
        <!-- Bootstrap -->
    <!-- App -->

    









<!-- Modal -->
<div id="dlgViewService" name="dlgViewService" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="row">
            <div class="col-md-4">
                <img src="{$webroot_resources}/images/servicebg-modal.jpg" class="servicesbg-modal" />
            </div>
            <div class="col-md-8">
                <div class="service-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row">
                         <div class="col-md-6">
                            <div class="title-services" id="view_product_name" name="view_product_name">Deluxe Pedicure</div>
                            <div class="description-services-modal" id="view_description" name="view_description">The same application is applied as the European Pedicure with a soft sugar scrub in place of the light sea salt for the same application is applied as the European Pedicure with a soft sugar scrub in place of the light sea salt for</div><br />
                            <p><span>Price</span>: <label id="view_price" name="view_price">45.00</label></p>
                            <p><span>Cost</span>: <label id="view_cost" name="view_cost">5.00</label></p>
                            <p><span>Type</span>: <label id="view_product_type" name="view_product_type">Inventory</label></p>
                            <p id="inventory_quantity" name="inventory_quantity"><span>Quantity</span>: <label id="view_quantity" name="view_quantity">5</label></p>
                            <p id="minutes_of_service" name="minutes_of_service"><span>Minutes of Service</span>: <label id="view_minutes" name="view_minutes">5</label> min.</p>
                            
                         </div>
                         <div class="col-md-6 ta-right">
                            <div class="price-services" id="view_big_price" name="view_big_price">$45</div>
                         </div>
                         <div class="clear"></div>
                         <div class="ta-right">
                              <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='servicesprocess.php'">Edit Service</button> -->
                              <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /. Modal content-->

  </div>
</div>




<!-- Modal -->
<div id="dlgEditService" name="dlgEditService" class="modal fade" role="dialog">
<form id="frmEditService" name="frmEditService">	
	<input type="hidden" name="txtProductId" id="txtProductId">
  <div class="modal-dialog">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Service/Product Information</h4>
                <p>Enter Information for you to save new service/product</p>
            </div>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="row">
                    
                         <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <label>Service/Product Name</label>
                                <input type="text" name="txtName" id="txtName" class="form-control" placeholder="Services"> 
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                 <textarea rows="5" cols="50" id="txtDescription" name="txtDescription"  class="form-control" style="width:100%;"></textarea>
                            </div>
                         </div>
                         <div class="col-md-3 col-xs-3">
                            <div class="form-group">
                                <label>Cost</label>
                                <input type="text" name="txtCost" id="txtCost" class="form-control" placeholder="Cost"> 
                            </div>
                            
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" id="txtPrice" name="txtPrice" class="form-control" placeholder="Price"> 
                            </div>
                            
                            <div class="form-group">
                                <label>Product Type</label>
								<select id="txtProductType" name="txtProductType" class="form-control m-b">
								  <option value="SERVICE">SERVICE</option>
								  <option value="NON-INVENTORY">NON-INVENTORY</option>
								  <option value="INVENTORY">INVENTORY</option>
								  
								</select>
                            </div>
                            
                             
                         </div>
                         <div class="col-md-3 col-xs-3">
                            
                            <div class="form-group" id="divMinutes" name="divMinutes" style="display:none;">
                                <label>Minutes of Service</label>
                                <div id="MySpinner" class="spinner input-group" data-min="1" data-max="10">
                                    <input type="text" class="form-control spinner-input" name="txtMinutesOfService" id="txtMinutesOfService" value="1" name="spinner" maxlength="4">
                                    <div class="btn-group btn-group-vertical input-group-btn">
                                        <button type="button" class="btn btn-default spinner-up"> <i class="fa fa-chevron-up text-muted"></i> </button>
                                        <button type="button" class="btn btn-default spinner-down"> <i class="fa fa-chevron-down text-muted"></i> </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="divQuantity" name="divQuantity" style="display:none;">
                                <label>Available Quantity</label>
                                <input class="form-control" type="text" placeholder="Quantity" name="txtQuantity" id="txtQuantity" value="0">
                            </div>

                         </div>
                   
                </div>
            </div>
            <!-- /. Modal content-->
           <div class="modal-footer">
                <button class="btn btn-primary" id="btnUpdateService" name="btnUpdateService">Save Service</button>
                <!-- <button type="button" class="btn btn-md btn-primary" onClick="location.href='serviceslist.php'">Save Service</button> -->
                <button type="button" class="btn btn-md btn-dark" data-dismiss="modal">Close</button>
            </div>
  </div>
</form>
</div>

<!-- make it dynamic content sir lems :p Modal -->
<div id="dlg_message" name="dlg_message" class="modal fade" role="dialog">
  <div class="modal-dialog2">
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-4">
                <!-- <img src="{$webroot_resources}/images/update.png" class="servicesbg-modal" /> -->
                <img src="{$webroot_resources}/images/success.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-8">
                <div class="service-content">
                     <div class="success-title" id="lbltitle" name="lbltitle">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessage" name="dlg_lblMessage">
                    		Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>

<!-- make it dynamic content sir lems :p Modal -->
<div id="dlg_messageerror" name="dlg_messageerror" class="modal fade" role="dialog">
  <div class="modal-dialog2">
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
            <div class="col-md-4">
                <!-- <img src="{$webroot_resources}/images/update.png" class="servicesbg-modal" /> -->
                <img src="{$webroot_resources}/images/error.png" class="servicesbg-modal" />
            </div>
            <div class="col-md-8">
                <div class="service-content">
                     <div class="success-title" id="lbltitleerror" name="lbltitleerror">Successfully Save</div>
                    <!-- <div class="success-title">Successfully Added</div> -->
                    <p id="dlg_lblMessageerror" name="dlg_lblMessagerrror">
                    		Services Information Successfully added to list. Please check it on the list. Thank you
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>



<div id="dlgLoading" name="dlgLoading" class="modal fade" role="dialog">
  <div class="modal-dialog3" style="width:430px;">
         <!-- Modal content-->
    <div class="modal-content" style="border-radius:6px;">
        <div class="row">
           <div class="col-md-4 col-xs-4">
               <img src="{$webroot_resources}/images/loading.gif" class="servicesbg-modal" />
            </div>
            <div class="col-md-8 col-xs-8">
                <div class="service-content">
                    <div class="success-title">Go3Reservation</div>
                    <p>Please wait...</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /. Modal content-->
  </div>
</div>


 <!-- /.footer -->

 
</body>

</html>