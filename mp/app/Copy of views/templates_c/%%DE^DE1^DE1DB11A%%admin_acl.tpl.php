<?php /* Smarty version 2.6.26, created on 2016-08-29 23:10:04
         compiled from admin_acl.tpl */ ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    User Type
                    <!--<small>Optional description</small>-->
                    <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/new_profile">
                    	<button class="btn btn-primary" type="button">Add New Profile</button>
                    </a>
					<a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/new_module">
                    	<button class="btn btn-primary" type="button">Add New Module</button>
                    </a>
                    </h1>
                    
                    
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Admin</li>
                        <li class="active">ACL</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                	
                    <!-- Your Page Content Here -->
                    	
                        <br>
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">User Type Profile</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                	
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th width="70%">Profile Name</th>
												<th width="30%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									        <?php $_from = $this->_tpl_vars['profile_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                <tr>
									                    <td><?php echo $this->_tpl_vars['item']['description']; ?>
</td>
									                    <td>
									                        <input class="btn btn-warning" type="button" onClick="window.location.href='edit_profile/<?php echo $this->_tpl_vars['item']['id']; ?>
'" value="Edit" />
									                        <!--<input style="<?php echo $this->_tpl_vars['rem_btn']['delete_partner']; ?>
" rel="<?php echo $this->_tpl_vars['item']['id']; ?>
" class="ordBtn delBtnProfile" type="button" value="Delete" />-->
									                        <?php if ($this->_tpl_vars['item']['create_by'] != 'DEF'): ?>
									                            <input type="button" class="btn btn-danger" value="Delete" id="<?php echo $this->_tpl_vars['item']['id']; ?>
" onClick="deleteProfile('<?php echo $this->_tpl_vars['item']['id']; ?>
')" />       
									                        <?php endif; ?>
									                    </td>
									                </tr>
									        <?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    