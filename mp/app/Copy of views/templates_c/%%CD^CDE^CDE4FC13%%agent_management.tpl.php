<?php /* Smarty version 2.6.26, created on 2016-09-26 03:17:00
         compiled from agent_management.tpl */ ?>
	<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Agents
                    <!-- <a href="<?php echo $this->_tpl_vars['webroot']; ?>
/partner/add">
                    	<button class="btn btn-primary" type="button">Add New Partner</button>
                    </a> -->
					
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">List Of Agents</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                	<h3 class="box-title">Agent List</h3>
                                </div>
                            	<!-- /.box-header -->
                                <div class="box-body">
                                    <table id="merchantlist" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Agent ID</th>
                                                <th>Partner</th>
                                                <th>Email</th>
                                                <th>Phone 1</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $_from = $this->_tpl_vars['agents']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                            
	                                            <tr>
	                                            	<td><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/agent/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['partner_id_reference']; ?>
</a></td>
	                                                <td><?php echo $this->_tpl_vars['item']['agent_partner']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
	                                                <td>+<?php echo $this->_tpl_vars['item']['mobile_number']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['first_name']; ?>
</td>
	                                                <td><?php echo $this->_tpl_vars['item']['last_name']; ?>
</td>
	                                            </tr>
                                            
                                            <?php endforeach; endif; unset($_from); ?>
                                        </tbody>

                                    </table>
                                </div>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->               
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->