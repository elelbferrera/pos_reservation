<?php /* Smarty version 2.6.26, created on 2016-10-18 22:54:23
         compiled from agent_add.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/agent.js"></script>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Agent
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/agent/management"><i class="fa fa-dashboard"></i> Agents</a></li>
                        <li class="active">Profile</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <!-- <div class="col-sm-3">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="summary.php">Summary</a></li>
                                    <li><a href="profile.php">Profile</a></li>
                                    <li><a href="#">Contacts</a></li>
                                    <li><a href="#">Payment Info</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">Invoices</a></li>
                                    <li><a href="#">Attachments</a></li>
                                    <li><a href="#">Notes</a></li>
                                    <li><a href="#">Communication</a></li>
                                    <li><a href="#">Log</a></li>
                                </ul>
                            </div>
                        </div> -->
                        <div class="col-sm-12">
                            <div class="box">
                                <!-- <div class="box-header with-border">
                                	<h3 class="box-title">Company Profile</h3>
                                </div> -->
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frmRegisterAgent" id="frmRegisterAgent">
                                    <div class="box-body">
                                    	<div class="row">
                                            <!-- <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label for="processor">Processor:</label>
	                                                <input type="text" class="form-control" id="processor" placeholder="Enter processor">
	                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="merchantID">Merchant ID:</label>
                                                    <input type="text" class="form-control" id="merchantID" placeholder="Enter merchant id">
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12"><h4>Company Profile:</h4></div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Agent ID:</label>
                                                    <input type="text" class="form-control" id="txtAgentId" name="txtAgentId" placeholder="Enter Agent Id">
                                                </div>
                                                
                                                
                                                
                                                <div class="form-group">
                                                    <label for="dba">Partner:</label>
                                                    <input type="text" class="form-control" id="txtPartner" name="txtPartner" placeholder="Enter Partner">
                                                </div>
                                                
                                                <div class="box-header with-border">
			                                			<h3 class="box-title">Office</h3>
			                                	</div>
			                                	
                                                <div class="form-group">
                                                    <label for="address1">Firstname:</label>
                                                    <input type="text" class="form-control" id="txtFirstName" name="txtFirstName" placeholder="Enter First Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="address2">Last Name:</label>
                                                    <input type="text" class="form-control" id="txtLastName" name="txtLastName" placeholder="Enter Last Name">
                                                </div>
                                                
                                                <div class="box-header with-border">
			                                			<h3 class="box-title">Business Address</h3>
			                                	</div>
			                                	 <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountry" name="txtCountry">
									                  <?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
				                                
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
                                                </div>
                                            
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
                                                </div>
	                                            
	                                            <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <!-- <input type="text" class="form-control" id="txtState" name="txtState" placeholder="Enter state"> -->
                                                    <select class="form-control select2" style="width: 100%;" id="txtState" name="txtState">
									                  <?php $_from = $this->_tpl_vars['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['code']; ?>
"><?php echo $this->_tpl_vars['item']['code']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
                                                </div>
                                                
                                                <div class="form-group" style="display: none">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <!-- <option selected="selected">Alabama</option>
									                  <option>Alaska</option>
									                  <option>California</option>
									                  <option>Delaware</option>
									                  <option>Tennessee</option>
									                  <option>Texas</option>
									                  <option>Washington</option> -->
									                  <?php $_from = $this->_tpl_vars['partner_type']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>

                                                </div>
                                                
                                                <div class="box-header with-border">
			                                			<h3 class="box-title">Mailing Address</h3>
			                                	</div>
			                                	<div class="form-group">
                                                    <label>
                                                    	<input type="checkbox" id="chkSameAsBusiness" name="chkSameAsBusiness" placeholder="Enter Fax">
                                                    	Same as Business Address
                                                    </label>
                                                </div>
                                               
                                               
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountryBusiness" name="txtCountryBusiness">
									                  <?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
				                                
                                                <div class="form-group">
                                                    <label for="address1">Address:</label>
                                                    <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="Enter address">
                                                </div>
                                            
	                                            <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" class="form-control" id="txtCityBusiness" name="txtCityBusiness" placeholder="Enter city">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <!-- <input type="text" class="form-control" id="txtStateBusiness" name="txtStateBusiness" placeholder="Enter state"> -->
                                                    <select class="form-control select2" style="width: 100%;" id="txtStateBusiness" name="txtStateBusiness">
									                  <?php $_from = $this->_tpl_vars['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['code']; ?>
"><?php echo $this->_tpl_vars['item']['code']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text" class="form-control" id="txtZipBusiness" name="txtZipBusiness" placeholder="Enter zip">
                                                </div>
                                                
                                            
                                                
                                               
                                            
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">SSN:</label>
                                                    <input type="password" class="form-control" id="txtSSN" name="txtSSN" placeholder="Enter SSN" maxlength="9">
                                                </div>
                                                <div class="form-group">
                                                    <label for="compname">Date of Birth:</label>
                                                    <input type="text" class="form-control datepicker" id="txtDOB" name="txtDOB" placeholder="Date of Birth">
                                                </div>
                                                
                                                <div class="box-header with-border">
			                                			<h3 class="box-title">Contact Information</h3>
			                                	</div>
			                                	 <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="phone2">Mobile:</label>
                                                    <input type="text" class="form-control" id="txtMobileNumber" name="txtMobileNumber" placeholder="Enter phone 2">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="phone2">Fax:</label>
                                                    <input type="text" class="form-control" id="txtFax" name="txtFax" placeholder="Enter phone 2">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email 2:</label>
                                                    <input type="email" class="form-control" id="txtEmail2" name="txtEmail2" placeholder="Enter email">
                                                </div>
                                                
												<div class="form-group">
                                                    <label for="email">Website:</label>
                                                    <input type="email" class="form-control" id="txtWebsite" name="txtWebsite" placeholder="Enter website">
                                                </div>
                                                
                                                <div class="box-header with-border">
			                                			<h3 class="box-title">Payment Information</h3>
			                                		</div>
			                                		
			                                		<div class="form-group">
	                                                    <label for="phone2">Bank Name:</label>
	                                                    <input type="text" class="form-control" id="txtBankName" name="txtBankName" placeholder="Enter Bank Name">
	                                                </div>
	                                                <div class="form-group">
	                                                    <label for="phone2">Account Name:</label>
	                                                    <input type="text" class="form-control" id="txtAccountName" name="txtAccountName" placeholder="Enter Account Name">
	                                                </div>
	                                                <div class="form-group">
	                                                    <label for="phone2">Account Number:</label>
	                                                    <input type="text" class="form-control" id="txtAccountNumber" name="txtAccountNumber" placeholder="Enter Account Number">
	                                                </div>
                                            		<div class="form-group">
	                                                    <label for="phone2">Routing Number:</label>
	                                                    <input type="text" class="form-control" id="txtRoutingNumber" name="txtRoutingNumber" placeholder="Enter Routing Number">
	                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <div class="col-md-6">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <div class="col-md-6">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <div class="col-md-6">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                        </div>
                                        <div class="row">
                                            
                                        </div> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer right">
                                        <button type="submit" class="btn btn-primary" id="btnCreateAgent" name="btnCreateAgent">
                                        	Save
                                        </button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->