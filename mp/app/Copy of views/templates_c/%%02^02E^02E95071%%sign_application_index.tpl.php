<?php /* Smarty version 2.6.26, created on 2016-10-20 22:44:22
         compiled from sign_application_index.tpl */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GOETU | Billing</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/dist/css/AdminLTE.css">
    <!-- iCheck -->
  	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/iCheck/square/blue.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/assets/css/custom.css">
    	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/pace/pace.min.css">
	<!-- PACE -->
	<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/pace/pace.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--  -->
	
</head>    
<style type="text/css">
<?php echo '
	#signature {
		border: 2px dotted black;
		/*background-color:lightgrey;*/
	}
'; ?>

</style>
<script type="javascript">
<?php echo '
	$(document).ajaxStart(function() {
		Pace.restart();
	});
	$(\'.ajax\').click(function() {
		$.ajax({
			url : \'#\',
			success : function(result) {
				// $(\'.ajax-content\').html(\'<hr>Ajax Request Completed !\');
			}
		});
	});
'; ?>

	
</script>
<body>
	<form id="frmSignature" name="frmSignature" id="frmSignature">
	<div class="container" style="">
		<input type="hidden" name="txtImage" id="txtImage" value="<?php echo $this->_tpl_vars['signature']; ?>
" />
		<input type="hidden" name="txtPartnerId" id="txtPartnerId" value="<?php echo $this->_tpl_vars['partner_id']; ?>
" />
		<input type="hidden" name="txtOrderId" id="txtOrderId" value="<?php echo $this->_tpl_vars['order_id']; ?>
" />
		<iframe width="100%" height="500px" src="<?php echo $this->_tpl_vars['webroot']; ?>
/sign_application/generate_pdf_doc?partner_id=<?php echo $this->_tpl_vars['partner_id']; ?>
&order_id=<?php echo $this->_tpl_vars['order_id']; ?>
"></iframe>
	
		<div class="jSignature" id="signature"></div>
		<hr>
		<!-- <div id="displayarea"></div> -->
 		<input type="button" class="btn btn-primary btn-block btn-flat" name="btnReset" value="Reset My Signature" id="btnReset" />
 		<input type="button" class="btn btn-primary btn-block btn-flat" name="btnDisplay" value="Submit" id="btnDisplay" />
	</div>
	<img id="imgSign" name="imgSign" />	
	</form>
 	


<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/plugins/iCheck/icheck.min.js"></script>

<script  type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/libs/jSignature.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/sign_application.js"></script>
<script src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/libs/jSignature.SignHere.js"></script> 

<script>
<?php echo '
	$(function () {
		$(\'input\').iCheck({
			checkboxClass: \'icheckbox_square-blue\',
			radioClass: \'iradio_square-blue\',
			increaseArea: \'20%\' // optional
		});
		
	});
'; ?>

</script>
</body>
</html>