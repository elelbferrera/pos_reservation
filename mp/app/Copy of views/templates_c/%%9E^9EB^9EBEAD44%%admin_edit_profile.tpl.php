<?php /* Smarty version 2.6.26, created on 2016-08-30 01:02:36
         compiled from admin_edit_profile.tpl */ ?>
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    ACL
                    <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/admin/acl"><i class="fa fa-dashboard"></i> Profiles</a></li>
                        <li class="active">ACL</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-header with-border">
                                	<h3 class="box-title"><?php echo $this->_tpl_vars['profile_info']['profile_name']; ?>
 Profile</h3>
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="frmAdminEditProfile" method="post">
                                	<input type="hidden" name="id" id="id" value="<?php echo $this->_tpl_vars['profile_info']['id']; ?>
" />
                                	<input value="<?php echo $this->_tpl_vars['profile_info']['profile_name']; ?>
" class="inputbox" id="profile_name" name="profile_name" size="31" type="hidden"/>
                                    <div class="box-body">
                                    	<?php $this->assign('ctr', 0); ?>
                                    	<?php $this->assign('first', 'first'); ?>
                                    	<?php $_from = $this->_tpl_vars['module_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['mods']):
?>
	                                    		<div class="col-md-3">
		                                    	  <div class="form-group">
									                <label>
									                  <input type="checkbox" class="minimal-red" name="module_access[]" value="<?php echo $this->_tpl_vars['mods']['id']; ?>
" class="minimal-red" <?php if (in_array ( $this->_tpl_vars['mods']['id'] , $this->_tpl_vars['profile_info']['module_access'] )): ?> checked <?php endif; ?>>
									                  <?php echo $this->_tpl_vars['mods']['description']; ?>

									                </label>
									              </div>
									            </div>
	                                    
	                                    <?php endforeach; endif; unset($_from); ?> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <div class="box-footer">
                                        <button id="btnSaveEditProfile" type="submit" class="btn btn-primary">Save Profile</button>
                                    </div>
                                </form>
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>