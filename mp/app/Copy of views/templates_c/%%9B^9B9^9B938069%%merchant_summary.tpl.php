<?php /* Smarty version 2.6.26, created on 2016-10-21 00:53:42
         compiled from merchant_summary.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'merchant_summary.tpl', 228, false),)), $this); ?>
      <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/merchant.js">
    	
    	
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchant
                    <small>Summary</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/management"><i class="fa fa-dashboard"></i> Merchant</a></li>
                        <li class="active">Merchants</li>
                        <li class="active">Summary</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-sm-3 sidelink-wrap">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/summary/<?php echo $this->_tpl_vars['partner_id']; ?>
" class="selected"><i class="fa fa-list-alt" aria-hidden="true"></i> Summary</a></li>
                                    <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['partner_id']; ?>
"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                    <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/contact/<?php echo $this->_tpl_vars['partner_id']; ?>
"><i class="fa fa-users" aria-hidden="true"></i> Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-9 merchantinfo-wrap">
                            <div class="row">
                            	<div class="col-sm-6">
                                	<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Profile</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                            	<div class="col-sm-4 summary-title">
                                                	First Name
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['first_name']; ?>

                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Last Name
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['last_name']; ?>

                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Company Name
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['company_name']; ?>

                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	DBA
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['dba']; ?>

                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Contact No.
                                                </div>
                                                <div class="col-sm-8">
                                                	+<?php echo $this->_tpl_vars['merchant']['phone1']; ?>

                                                </div>
                                            </div>
                                            <div class="row">    
                                                <div class="col-sm-4 summary-title">
                                                	Email
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['email']; ?>
	
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                	Address
                                                </div>
                                                <div class="col-sm-8">
                                                	<?php echo $this->_tpl_vars['merchant']['address1']; ?>

                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                	<!--<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Invoices</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-4 summary-title">
                                                    Paid
                                                </div>
                                                <div class="col-sm-8">
                                                    1 ($100.50 USD)
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                    Unpaid
                                                </div>
                                                <div class="col-sm-8">
                                                    2 ($10.48 USD)
                                                </div>
                                            </div>
                                            <div class="row">   
                                                <div class="col-sm-4 summary-title">
                                                    Income
                                                </div>
                                                <div class="col-sm-8">
                                                    $80.02 USD
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                        		<a href="#"><i class="fa fa-file-text"></i> Create Invoice</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Attachments</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-md-6 col-lg-4">
                                                	<a href="#" class="summary-attachment"><i class="fa fa-file"></i>application.pdf</a>
                                                </div>
                                                <div class="col-md-6 col-lg-4">
                                                	<a href="#" class="summary-attachment"><i class="fa fa-file"></i>sample.doc</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                            	<a href="#"><i class="fa fa-plus-circle"></i> Add File</a>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="col-sm-4">
                                	<!--<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Products</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-4 summary-title">
                                                    Products
                                                </div>
                                                <div class="col-sm-8">
                                                    0 (Total)
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                        	<div>
                                            	<a href="product-add.php"><i class="fa fa-plus-circle"></i> Add New Product</a>
                                            </div>
                                            <div>
                                            	<a href="#"><i class="fa fa-search"></i> View Products</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Notes</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-12">
                                                    N/A
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="col-sm-12">
                                	<div class="box">
                                    	<div class="box-header with-border">
                                            <h3 class="box-title">Products</h3>
                                        </div>
                                        <div class="box-body summary-info">
                                        	<div class="row">
                                                <div class="col-sm-12">
                                                    <table id="merchantlist" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Application ID.</th>
                                                                <th>Product</th>
                                                                
                                                                <th>Status</th>
                                                                <th class="no-sort"></th>
                                                                <th class="no-sort"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        	
                                                        	<?php $_from = $this->_tpl_vars['histories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                                            <tr>
                                                                <td><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['create_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
</td>
                                                                <td><?php echo $this->_tpl_vars['item']['order_id']; ?>
</td>
                                                                <td><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
                                                                
                                                                <td><?php echo $this->_tpl_vars['item']['status']; ?>
</td>
                                                                <td class="icon-td">
                                                                	<?php if ($this->_tpl_vars['item']['status'] == 'Pending'): ?>
                                                                		<a target="_blank" href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/generate_pdf_doc/<?php echo $this->_tpl_vars['partner_id']; ?>
&order_id=<?php echo $this->_tpl_vars['item']['order_id']; ?>
"><i class="fa fa-file-pdf-o"></i></a>
                                                                	<?php endif; ?>
                                                                </td>
                                                                <td class="icon-td">
	                                                                <?php if ($this->_tpl_vars['item']['status'] == 'Pending'): ?>
	                                                                	<a href="#" class="sendmodal" onclick="SendPDFDoc('<?php echo $this->_tpl_vars['partner_id']; ?>
','<?php echo $this->_tpl_vars['item']['order_id']; ?>
');"><i class="fa fa-send"></i></a>
	                                                                <?php else: ?>
	                                                                	
	                                                                <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; endif; unset($_from); ?>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->