<?php /* Smarty version 2.6.26, created on 2016-10-06 01:21:47
         compiled from merchant_profile.tpl */ ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['webroot_resources']; ?>
/js/merchant.js">
    	
    	
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	<div class="container">
        	<div class="row">
                <!-- Content Header (Page header / Title / Breadcrumbs) -->
                <section class="content-header">
                    <h1>
                    Merchant
                    <small>Summary</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/management"><i class="fa fa-dashboard"></i> Merchant</a></li>
                        <li class="active">Merchants</li>
                        <li class="active">Summary</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                	<div class="row">
                    <!-- Your Page Content Here -->
                        <div class="col-sm-3 sidelink-wrap">
                            <div class="sidelink">
                            	<h3 class="sidelink-title">Merchant Tabs</h3>
                                <ul class="sidelink-nav">
                                	<li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/summary/<?php echo $this->_tpl_vars['partner_id']; ?>
"><i class="fa fa-list-alt" aria-hidden="true"></i> Summary</a></li>
                                    <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/profile/<?php echo $this->_tpl_vars['partner_id']; ?>
" class="selected"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                    <li><a href="<?php echo $this->_tpl_vars['webroot']; ?>
/merchant/contact/<?php echo $this->_tpl_vars['partner_id']; ?>
"><i class="fa fa-users" aria-hidden="true"></i> Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-9">
                        	<form role="form" method="post" name="frmUpdateMerchant" id="frmUpdateMerchant" enctype="multipart/form-data">
                            <div class="box" id="merchant-info-wrap" name="merchant-info-wrap">
                                <div class="box-header with-border" >
                                	<!-- <h3 class="box-title">Company Profile</h3> -->
                                </div>
                            	<!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                    	<div class="row">
                                            <!-- <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label for="processor">Processor:</label>
	                                                <input type="text" class="form-control" id="processor" placeholder="Enter processor">
	                                            </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="merchantID">Merchant ID:</label>
                                                    <input type="text" class="form-control" id="merchantID" placeholder="Enter merchant id">
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12"><h4>Company Profile:</h4></div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="compname">Processor:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['merchant_processor']; ?>
" class="form-control" id="txtProcessor" name="txtProcessor" placeholder="Enter processor">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dba">Merchant ID:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['merchant_id']; ?>
" class="form-control" id="txtMerchantID" name="txtMerchantID" placeholder="Enter Merchant ID">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-header with-border">
                                			<h3 class="box-title">Company Profile</h3>
                                		</div>
                                		
                                		<div class ="row">
                                			<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Company Name:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['company_name']; ?>
" class="form-control" id="txtCompanyName" name="txtCompanyName" placeholder="Enter Company Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">DBA:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['dba']; ?>
" class="form-control" id="txtDBA" name="txtDBA" placeholder="Enter DBA">
                                                </div>
                                            </div>	
                                		</div>
                                		
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">Address 1:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['address1']; ?>
" class="form-control" id="txtAddress1" name="txtAddress1" placeholder="Enter address 1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address2">Address 2:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['address2']; ?>
" class="form-control" id="txtAddress2" name="txtAddress2" placeholder="Enter address 2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['city']; ?>
" class="form-control" id="txtCity" name="txtCity" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['state']; ?>
" class="form-control" id="txtState" name="txtState" placeholder="Enter state">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zip">Zip:</label>
                                                    <input type="text"  value="<?php echo $this->_tpl_vars['merchant']['zip']; ?>
"class="form-control" id="txtZip" name="txtZip" placeholder="Enter zip">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country:</label>
													<select class="form-control select2" style="width: 100%;" id="txtCountryUpdate" name="txtCountryUpdate">
									                  <?php $_from = $this->_tpl_vars['country']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['name']; ?>
" <?php if ($this->_tpl_vars['item']['name'] == $this->_tpl_vars['merchant']['country_name']): ?>  selected="selected"   <?php endif; ?>  ><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" value="<?php echo $this->_tpl_vars['merchant']['email']; ?>
" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone1">Phone 1:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['phone1']; ?>
" class="form-control" id="txtPhone1" name="txtPhone1" placeholder="Enter phone 1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone 2:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['phone2']; ?>
" class="form-control" id="txtPhone2" name="txtPhone2" placeholder="Enter phone 2">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Fax:</label>
                                                    <input type="text" value="<?php echo $this->_tpl_vars['merchant']['fax']; ?>
" class="form-control" id="txtFax" name="txtFax" placeholder="Enter Fax">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: none">
                                                    <label>Partner Type:</label>
													<select class="form-control select2" style="width: 100%;" id="txtPartnerTypeId" name="txtPartnerTypeId">
									                  <!-- <option selected="selected">Alabama</option>
									                  <option>Alaska</option>
									                  <option>California</option>
									                  <option>Delaware</option>
									                  <option>Tennessee</option>
									                  <option>Texas</option>
									                  <option>Washington</option> -->
									                  <?php $_from = $this->_tpl_vars['partner_type']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									                  	<option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"<?php if ($this->_tpl_vars['item']['name'] == 'MERCHANT'): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
									                  <?php endforeach; endif; unset($_from); ?>
									                </select>

                                                </div>
                                            </div>
                                            <?php $_from = $this->_tpl_vars['documents']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['d']):
?>
	                                        <div class="col-md-12">
	                                        	
	                                            <div class="form-group">
	                                                <label><?php echo $this->_tpl_vars['d']['name']; ?>
:</label>
	                                                <!-- <input type="file" id="fileUploadVoidCheck" name="fileUploadVoidCheck"> -->
	                                               	<div>
	                                               		<a href="<?php echo $this->_tpl_vars['d']['document_image']; ?>
" target="_blank"> <img style="max-width: 250px;" src="<?php echo $this->_tpl_vars['d']['document_image']; ?>
" />
	                                               		</a>
	                                               	</div>
	                                            </div>
	                                        </div>
	                                        <?php endforeach; endif; unset($_from); ?>
                                        </div> 
                                    </div>
                                    <!-- /.box-body -->
                                
                                    <!-- <div class="box-footer ta-center">
                                        
                                        <button type="submit" class="btn btn-primary" id="btnUpdateMerchant" name="btnUpdateMerchant">	
                                        	Update Merchant Info
                                        </button>
                                    </div> -->
                                
                            <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                            
                            </form>
                        </div>
                        
                        
                        
                        
                    </div>
                </section>
                <!-- /.content -->
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->